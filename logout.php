<?php
	ob_start();
	session_start();
	require_once("config.inc.php");
	require_once("classes/cms.php");
	require_once("classes/student.php");

	if (array_key_exists("LogoutState", $_REQUEST)) {
		$state = \SimpleSAML\Auth\State::loadState((string)$_REQUEST['LogoutState'], 'MyLogoutState');
		session_destroy();
		header("location: index.php");
		exit();
	}

	$member = new Student();
	if ($member->isLogined()) {
		$member->logout();
	}
	
	
	header("location: index.php");
	exit();
?>