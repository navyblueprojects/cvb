<?php
	ob_start();
	require_once("config.inc.php");
	include_once("classes/system.php");

	include_once("header.php");

	$pageName = ["Preview & Save"];
	$id = base64_decode($_GET["id"]);

	$cvform = $member->getCVFormByHash($id);

	if (!$cvform["personal_cv"]["id"]) {
		header("location: ../home");
		exit();
	}

	

	include_once("breadcrumb.php");

?><main id="choose_content">
	<div class="wrapper">
		<ul class="nav nav-step hidden-xs">
			<li>Application Form</li>
			<li>Templates</li>
			<li class="active">Preview & Save</li>
		</ul>
		<ul class="nav nav-pills visible-xs">
			<li role="presentation" class="disabled"><a>Application Form</a></li>
			<li role="presentation" class="disabled"><a>Templates</a></li>
			<li role="presentation" class="active"><a>Preview & Save</a></li>
		</ul>
	</div>
	<div class="container">
		<h1 class="page_header">Preview &amp; Save</h1>
		<section id="cv_wrapper" >
			<div class="row">
				<div class="col-md-12"><div data-cms-title="previewcv-tipbox"><?=$cmsObj->getPageArea("previewcv-tipbox")?></div></div>
				<div class="col-md-12 tab-content">
					<h3 class="pull-left"><i class="far fa-window-maximize"></i> Template <?=$_GET["t"]?></h3>
					<ul class="btn-group pull-right btn-group-right" role="group" >
						<li class="dropdown" role="presentation">
							<a class="btn btn-default dropdown-toggle" id="downloadMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-envelope"></i></a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="downloadMenu2">
								<li><a data-toggle="modal" data-target="#modalSendFSCC"> Send To FSCC</a></li>
								<li><a data-toggle="modal" data-target="#modalSendCV"> Send To Personal Email</a></li>
							</ul>
						</li>
						<li role="presentation"><a href="cvgenerate-<?=base64_encode(sha1("cv-".$cvform["personal_cv"]["id"]))?>.print?t=<?=$_GET["t"]?>" target="_print" class="btn btn-default"><i class="fa fa-print"></i></a></li>
						<li class="dropdown" role="presentation">
							<a class="btn btn-default dropdown-toggle" id="downloadMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-download"></i></a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="downloadMenu">
								<li><a href="cvgenerate-<?=trim(base64_encode(sha1("cv-".$cvform["personal_cv"]["id"])), "=")?>.pdf?t=<?=$_GET["t"]?>" target="_blank"><i class="fa fa-file-pdf"></i> PDF File (.pdf)</a></li>
								<li><a href="cvgenerate-<?=trim(base64_encode(sha1("cv-".$cvform["personal_cv"]["id"])), "=")?>.docx?t=<?=$_GET["t"]?>"><i class="fa fa-file-word"></i> Word File (.docx)</a></li>
							</ul>
						</li>
					</ul>
					<div id="template1" class="tab-pane fade in active" data-tabid="1">
						<iframe src="preview-cv/<?=$_GET["id"]?>?t=<?=$_GET["t"]?>" id="previewWindow" data-generate="cvgenerate-<?=trim(base64_encode(sha1("cv-".$cvform["personal_cv"]["id"])), "=")?>.preview?t=<?=$_GET["t"]?>"></iframe>
					</div>
				</div>
				<div class="saveRow">
					<div class="col-md-12">
						<a href="choose-template/<?=trim(base64_encode(sha1("app-".$cvform["personal_cv"]["id"])), "=")?>" class="btn btn-default pull-left" > Back</a>
						<a class="btn btn-default pull-right" onclick="goSave()" >Next </a>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="modalSendCV" aria-labelledby="lblModalSendCV">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form class="form" method="post" id="frmSendCV" action="cvgenerate-<?=base64_encode(sha1("cv-".$cvform["personal_cv"]["id"]))?>.email?t=<?=$_GET["t"]?>">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="lblModalSendCV">Send the CV to Personal Email</h4>
				</div>
				<div class="modal-body">
					<div class="row"><div class="form-group col-md-12">
						<label for="modalEmail">Send To: </label>
						<input type="text" name="email" id="modalEmail_1" class="req email form-control" autocomplete="off" placeholder="Email" />
					</div></div>
					<div class="row"><div class="form-group col-md-12">
						<label for="modalSubject">Subject: </label>
						<input type="text" name="subject" id="modalSubject_1" class="req form-control" autocomplete="off" placeholder="Subject" />
					</div></div>
					<div class="row"><div class="form-group col-md-12">
						<label for="content">Content: </label>
						<textarea name="content" id="content_1" class="ckeditor" ></textarea>
					</div></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-default btnSendEmail">Send</button>
				</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="modalSendFSCC" aria-labelledby="lblModalSendFSCC">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form class="form" method="post" id="frmSendFSCC" action="cvgenerate-<?=base64_encode(sha1("cv-".$cvform["personal_cv"]["id"]))?>.email?t=<?=$_GET["t"]?>">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="lblModalSendFSCC">Send the CV to FSCC</h4>
				</div>
				<div class="modal-body">
					<div class="row"><div class="form-group col-md-12">
						<label for="modalEmail">Send To: </label>
						<input type="text" name="email" id="modalEmail_2" value="fscd@cpce-polyu.edu.hk" readonly="readonly" class="req form-control" autocomplete="off" placeholder="Email"  />
					</div></div>
					<div class="row"><div class="form-group col-md-12">
						<label for="modalSubject">Subject: </label>
						<input type="text" name="subject" id="modalSubject_2" class="req form-control" autocomplete="off" placeholder="Subject" />
					</div></div>
					<div class="row"><div class="form-group col-md-12">
						<label for="content">Content: </label>
						<textarea name="content" id="content_2" class="ckeditor" ></textarea>
					</div></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-default btnSendEmail">Send</button>
				</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<script src="scripts/cvform.js"></script>
	<script >
	
	function goSave(){

		window.location = '<?=$appPath."/"?>cvgenerate-<?=base64_encode(sha1("cv-".$cvform["personal_cv"]["id"]))?>.next?t=<?=$_GET["t"]?>';
	}
	</script>
	<script src="<?=$appPath?>/lib_common/ckeditor/ckeditor.js" async></script>
	<script src="<?=$appPath?>/lib_common/ckeditor/ckfinder/ckfinder.js" async></script>
</main>
<?php
	include_once("footer.php");
	ob_end_flush();
?>