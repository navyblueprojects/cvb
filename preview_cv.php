<?php
	include_once("config.inc.php");
	include_once("classes/cvform.php");

	$html = (file_get_contents("cvtemplate/cvtemplate".(int)$_GET["t"].".php"));

	$template = array();

	$sections = array("BASIC", "EDUCATION", "RESULT", "PRESULT", "WORK", "INTERNSHIP", "EXACTIVITIES", "PROFESSIONAL", "OTHER_QUALI", "ACHIEVEMENTS", "SKILLS", "REFERENCE", "INTERVIEW", "OTHERS", "ETC");

	foreach ($sections as $section) {
		$ttt = explode('<!-- ['.$section.'] -->', $html, 3); 
		$template[$section]["TEMPLATE"] = $ttt[1];
		$rows = explode('{'.$section.'-ROW}', $ttt[1], 3);
		
		if (count($rows) > 1){
			$template[$section]["ROW"] = $rows[1];
		}
	}
	$student = new Student();                                               
    if ($student->isLogined()) {                                            
        $cvForm = new CVForm();
		$cvForm = $cvForm->getCVByHash($_GET["id"], $student);
		$html = $cvForm->preview($template, $html);
		echo ($html);
	}
	exit();
?>