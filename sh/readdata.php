#!/usr/bin/env php

<?php 
    include_once("../config.inc.php");
    $pdo = new PDO(sprintf("mysql:dbname=%s;host=%s", $dbDatabase, $dbHost), $dbUser, $dbPassword);
    
    $insert_prog = 0;
    $insert_stud = 0;
    $update_stud = 0;
    $record_prog = 0;
    $record_stud = 0;

    $program_file_path = "/home/cvbdata/cvbprog.dat";
    $student_file_path = "/home/cvbdata/cvbstud.dat";

    foreach (file($program_file_path) as $row) {

        list($code, $code2, $program_name) = explode("|", $row);

        $code .= (trim($code2) == "") ? "" : "-".trim($code2);

        if ($pp = $pdo->query("Select * from program where code = '$code'")->fetch()) {
        
        } else {
    		echo $code . " " . $program_name;
    		$stmt = $pdo->prepare("INSERT INTO `program`( `node_id`, `code`, `name`, `max_job`, `status`, `createDate`) VALUES (1, :program_code, :program_name, 10, 'E', NOW())");
    		$tmp = $stmt->execute(array(":program_code"=>$code, ":program_name"=>$program_name));
            $insert_prog++;
    	}
        $record_prog++;

    }
    echo "[".date("YmdHis")."] (".$program_file_path.") Program: ".$record_prog. " Rows. ". $insert_prog . " inserted.\n";


    foreach (file($student_file_path) as $row) {
        list($stud_code, $prog_code, $prog_code2) = explode("|", $row);
        $stud_code = trim($stud_code);
        $prog_code = trim($prog_code);  
        $prog_code2 = trim($prog_code2);

        $study_yr = "Sept 1, " . substr($stud_code, 0, 2);
        $study_yr = ceil((time() - strtotime($study_yr)) / 31536000);

        $c_email = "";
        $prog_code .= ($prog_code2 == "") ? "" : "-".$prog_code2;

        $prog = $pdo->query("Select * from program where code = '".$prog_code."'")->fetch();

        //For the college email, pls determine by “A” /”S”n in student no.
        // 18032111A=HKCC student= @hkcc-polyu.edu.hk
        // 17032111S= SPEED student= @speed-polyu.edu.hk 

        switch (substr($stud_code, -1, 1)) {
            case 'A':
                $c_email = $stud_code."@student.hkcc-polyu.edu.hk";
                break;
            case 'S':
                $c_email = $stud_code."@student.speed-polyu.edu.hk";
                break;
            default:
                break;
        }
        if ($c_email) {
            if ($pp = $pdo->query("Select * from student where student_id = '$stud_code'")->fetch()) {
                $pdo->prepare("Update student set yrofstudy = '".$study_yr."', lastFeedDate = NOW(), `prog_code` = '".$prog["id"]."', `prog_title` = '".$prog["name"]."' where id = '".$pp["id"]."'")->execute();
                $update_stud++;
            } else {
                $sql = sprintf("INSERT INTO `student`( `student_id`, `wie_no`, `is_wie`, `en_name`, `en_lastname`, `ch_name`, `ch_lastname`, `address1`, `address2`, `address3`, `district`, `work_district`, `hkid`, `gender`, `age`, `dob`, `c_email`, `p_email`, `h_phone`, `m_phone`, `prog_code`, `prog_title`, `photo`, `subj_code`, `subj_title`, `em_name`, `em_name2`, `em_relation`, `em_phone`, `em_contact`, `yrofstudy`, `status`, `createDate`, `lastModDate`, `lastFeedDate`) VALUES ( '%s', '', 0, '', '', '', '', '', '', '', 0, '', '', '', 0, '1970-01-01', '%s', '', '', '', '%s', '%s', '', '', '', '', '', '', '', '', ".$study_yr.", 'E', now(), now(), now());", $stud_code, $c_email, $prog["id"], $prog["name"]);

                $pdo->prepare($sql)->execute();
                $insert_stud++;
            }    
        }
        $record_stud++;

    }

    echo "[".date("YmdHis")."] (".$student_file_path.") Students: ".$record_stud. " Rows. ". $insert_stud . " inserted.". $update_stud . " updated.\n";

    $pdo->query("UPDATE student SET status = 'D' where lastFeedDate  < NOW() - INTERVAL 1 WEEK ");
    
    $wie = $pdo->query("SELECT * FROM `wie_program` where now() BETWEEN `start_date` AND `end_date`");

    $pdo->prepare("UPDATE student SET is_wie = 0")->execute();

    while ($row = $wie->fetch()) {
        $studyyr = explode(";", trim($row["studyyear"], ";"));
        $pdo->prepare("UPDATE student set is_wie = 1 where prog_code = '".$row["program_id"]."' and yrofstudy in (".implode(",", $studyyr).")")->execute();
    }