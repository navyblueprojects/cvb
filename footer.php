<?php
	require_once("config.inc.php");

	$footerPage = $cmsObj->getPageList();
	
?><footer>
	
	<div  id="top-footer">
		<div class="wrapper">
			<div class="row" >
				<div class="col-sm-5"><img src="images/qess.png" alt="Funded by Quality Enhancement Support Scheme (QESS)"/></div>
				<div class="col-sm-6 col-sm-offset-1 text-right">
					<a href="https://sao.cpce-polyu.edu.hk/en/home/index.html" rel="noreferrer noopener" target="_blank"><img src="images/logo_csao.png" alt="PolyU CPCE Student Affairs Office" /></a>
					<a href="https://sao.cpce-polyu.edu.hk/en/fscd" rel="noreferrer noopener" target="_blank"><img src="images/logo_fscc.png" alt="PolyU Further Study & Career Centre" /></a>
				</div>
			</div>
		</div>
	</div>
	<div  id="bottom-footer">
		<div class="wrapper">
			<nav>
				<ul>
					<?php
						foreach ($footerPage as $page ){
							?><li><a href="<?=$page["tag"]?>"><?=$page["pageName"]?></a></li><?php
						}
					?>
				</ul>
			</nav>
			<div ><div class="cms" data-cms-title="footer_copyright"><?=$cmsObj->getPageArea("footer_copyright")?></div></div>
		</div>
	</div>
</footer>
<script>
	var viewCms = <?=($cmsObj->checkLogin()) ? "true" : "false"?>;
	!function(t){"use strict";t.loadCSS||(t.loadCSS=function(){});var e=loadCSS.relpreload={};if(e.support=function(){var e;try{e=t.document.createElement("link").relList.supports("preload")}catch(t){e=!1}return function(){return e}}(),e.bindMediaToggle=function(t){var e=t.media||"all";function a(){t.addEventListener?t.removeEventListener("load",a):t.attachEvent&&t.detachEvent("onload",a),t.setAttribute("onload",null),t.media=e}t.addEventListener?t.addEventListener("load",a):t.attachEvent&&t.attachEvent("onload",a),setTimeout(function(){t.rel="stylesheet",t.media="only x"}),setTimeout(a,3e3)},e.poly=function(){if(!e.support())for(var a=t.document.getElementsByTagName("link"),n=0;n<a.length;n++){var o=a[n];"preload"!==o.rel||"style"!==o.getAttribute("as")||o.getAttribute("data-loadcss")||(o.setAttribute("data-loadcss",!0),e.bindMediaToggle(o))}},!e.support()){e.poly();var a=t.setInterval(e.poly,500);t.addEventListener?t.addEventListener("load",function(){e.poly(),t.clearInterval(a)}):t.attachEvent&&t.attachEvent("onload",function(){e.poly(),t.clearInterval(a)})}"undefined"!=typeof exports?exports.loadCSS=loadCSS:t.loadCSS=loadCSS}("undefined"!=typeof global?global:this);
</script>
</body>
</html>
<?php
	session_write_close();
?>