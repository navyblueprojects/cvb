<?php

include($adoDBPath.'/adodb.inc.php'); # load code common to ADOdb

global $conn;
$conn = &ADONewConnection($platForm);  # create a connection
$conn->connect($dbHost,$dbUser,$dbPassword,$dbDatabase);# connect to MySQL
$conn->debug = $debugMode;
$conn->Execute("SET NAMES 'utf8'");

global $pdo;
try {
	$pdo = new PDO("mysql:host=$dbHost;dbname=$dbDatabase", $dbUser, $dbPassword);
	$pdo->exec("set names utf8");
}catch(PDOException $e) {
    echo $e->getMessage();
}

?>