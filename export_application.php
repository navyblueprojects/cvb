<?php
	require_once("config.inc.php");
	include_once("classes/application.php");
	require_once("classes/student.php");
	
	$t = base64_decode($_GET["t"]);

	$id = substr($_GET["id"], 0, -13);

	if ($id != $t) {
		header('HTTP/1.0 404 Not Found');
		exit();
	}

	$lecturer = Lecturer::retain();

	$application = new Application();

	$application = $application->getFromHashId($id);
	
	header("Content-type:application/pdf");
	header("Content-Disposition:inline;filename='$filename");

	readfile($application->generateApplicationPDF($lecturer)); 


	
?>

