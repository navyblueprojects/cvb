<?php
	require_once("config.inc.php");
	require_once("./classes/student.php");
	include_once("./classes/lecturer.php");

	$lecturer = Lecturer::retain();
	$student = new Student();
	//print_r($_GET);

	
	if ($lecturer->validateToken($_GET["token"]) || $lecturer->isLogined()) {
		$lecturer->getFileById($_GET["t"], (int)$_GET["id"], "");
	} else {
		if ($student->isLogined()) {
			$student->getFileById($_GET["t"], (int)$_GET["id"], "");
		} else {
			header('HTTP/1.0 403 Forbidden');
        	exit();	
		}
	}
	
?>