<?php
    require_once("config.inc.php");
    require_once("classes/cvform.php");
    require_once("classes/student.php");
    require_once("classes/nqfileobj.php");
    require_once("send_email.php");
    
    set_time_limit(0);
    ini_set('memory_limit', '-1');

    $member = new Student();
    
    if ($_GET["docx"] == 1) {
        $html = (file_get_contents("cvtemplate/cvtemplate".(int)$_GET["t"]."_docx.php"));
    } else {
        $html = (file_get_contents("cvtemplate/cvtemplate".(int)$_GET["t"].".php"));
    }
    $template = array();

    $sections = array("BASIC", "EDUCATION", "RESULT", "PRESULT", "WORK", "INTERNSHIP", "EXACTIVITIES", "PROFESSIONAL", "OTHER_QUALI", "ACHIEVEMENTS", "SKILLS", "OTHERS", "REFERENCE", "ETC");

    foreach ($sections as $section) {
        $ttt = explode('<!-- ['.$section.'] -->', $html, 3); 
        $template[$section]["TEMPLATE"] = $ttt[1];
        $rows = explode('{'.$section.'-ROW}', $ttt[1], 3);
        
        if (count($rows) > 1){
            $template[$section]["ROW"] = $rows[1];
        }
    }
    
    //if ($_GET["email"] == 1) {
    $action = "FI";
    //}
    if ($_GET["email"] == 1 || $_GET["next"] == 1 || $_GET["preview"] == 1) {
        $action = "F";
    }
    if ($_GET["print"] == 1 ) {
        $action = "FI";
    }

    if ($_GET["docx"] == 1) {
        $action = "docx";
    }
    $cvForm = new CVForm();
    if ($member->isLogined()) {    
        
        $cvForm = $cvForm->getObjByHashId($_GET["id"], $member);
    }
    
    if (!$cvForm->id) {
        header('HTTP/1.0 403 Forbidden');
        exit();
    }
 
    $filename = $cvForm->genpdf($template, (int)$_GET["t"], $action);

    if ($_GET["preview"] == 1) {
        $filename = explode("-", $filename);
        $filename = $filename[count($filename) - 1];
        
        $fileObj = new NqFileObj($member->getId());
        $tmpfile = $fileObj->generatefile($filename, ".pdf");
        
        ?><!DOCTYPE html>
<html lang="xmlns">
<head>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
</head>
<body bgcolor="#808080">

<canvas id="the-canvas" width="100%"></canvas>  
<script type="text/javascript">
    // If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = '<?=$appPath."/".$tmpfile ?>';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

// Asynchronous download of PDF
var loadingTask = pdfjsLib.getDocument(url);
loadingTask.promise.then(function(pdf) {
    
  // Fetch the first page
  var pageNumber = 1;
  pdf.getPage(pageNumber).then(function(page) {
    
    var desiredWidth = window.innerWidth;
    var viewport = page.getViewport({ scale: 1.05, });
    var scale = desiredWidth / viewport.width;
    var scaledViewport = page.getViewport({ scale: scale, });

    // Prepare canvas using PDF page dimensions
    var canvas = document.getElementById('the-canvas');
    var context = canvas.getContext('2d');
    canvas.height = scaledViewport.height;
    canvas.width = scaledViewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: context,
      viewport: scaledViewport
    };
    var renderTask = page.render(renderContext);
    renderTask.promise.then(function () {
      console.log('Page rendered');
    });
  });
}, function (reason) {
  // PDF loading error
  console.error(reason);
});
</script>
</body>
</html><?php
        exit();
    }


    if ($_GET["email"] == 1) {
        $filename = explode("-", $filename);
        $filename = $filename[count($filename) - 1];
        
        $fileObj = new NqFileObj($member->getId());
        $tmpfile = $fileObj->generatefile($filename, ".pdf");
        
        if (sendMail( $_POST["subject"],  $_POST["content"], $_POST["email"], $cvForm->getEmail(), array(), $rootPath.$appPath."/".$tmpfile) === TRUE) {
            @unlink($rootPath.$appPath."/".$tmpfile);
        } 
        if ($member->isWIE()) {
            header("location: joblist");
        } else {
            header("location: home");
        }
        exit();
    }

    if ($_GET["next"] == 1) {
        if ($member->isWIE()) {
            header("location: joblist");
        } else {
            header("location: home");
        }
        exit();
    }
?>