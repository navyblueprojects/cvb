<?php
	require_once("../config.inc.php");
	require_once("../classes/student.php");
	require_once("../classes/application.php");
	require_once("../classes/jobs.php");

	$student = new Student();
	if (!$student->isLogined()) {
		header("HTTP/1.0 404 Not Found");
		break;
	}
	$application = $student->getApplication();
	$application = new Application($application["id"]);

	switch ($_POST["action"]) {
		case "add":
			if ($_POST["id"]) {
				echo json_encode($application->add($_POST["id"], $student->getId()));
			}
			break;
		case "remove":
			if ($_POST["id"]) {
				echo json_encode($application->remove($_POST["id"], $student->getId()));
			}
			break;
		case "order":
			if ($_POST["priority"]) {
				echo json_encode($application->reorder($_POST["id"], $_POST["priority"]));
			}
			break;
		case "selfassess":
			echo json_encode($application->update($_POST));
			break;

		case "submit":
			echo json_encode($application->submit());
			break;

		default:
			header("HTTP/1.0 404 Not Found");
			break;
	}
	session_write_close();