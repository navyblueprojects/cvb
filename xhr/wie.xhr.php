<?php
	require_once("../config.inc.php");
	require_once("../classes/lecturer.php");
	require_once("../classes/application.php");
	require_once("../classes/jobs.php");


	if ($_POST["action"] != "login") {
		$member = Lecturer::retain();
		if (!$member->isLogined()){
			header("HTTP/1.0 404 Not Found");
			break;
		}	
	}
	

	switch ($_POST["action"]) {
		case 'login':
			$lecturer = new Lecturer();
			echo $lecturer->login($_POST["username"], $_POST["password"], $_POST["captcha"]);
			break;
		case "offerApplication":
			$lecturer = new Lecturer();
			if ($lecturer->isLogined()) {
				echo json_encode($lecturer->offerApplication($_POST["item_id"]));
			}
			exit();
		case "sendOfferEmail":
			$lecturer = new Lecturer();
			if ($lecturer->isLogined()) {
				echo json_encode($lecturer->sendOfferEmail($_POST["item_id"]));
			}
			exit();
		case "deleteJob":
			$jobs = new Jobs($_POST["pid"]);
			$lecturer = new Lecturer();
			if ($lecturer->isLogined()) {
				echo json_encode($jobs->delete($lecturer));
			}
			break;
		case "uploadFile":
			$jobs = new Jobs($_POST["id"]);
			$lecturer = new Lecturer();
			if ($lecturer->isLogined()) {
				echo json_encode($jobs->uploadFile($_POST["type"]));
			}
			break;
		case "removeFile":
			$jobs = new Jobs($_POST["job_id"]);
			$lecturer = new Lecturer();
			if ($lecturer->isLogined()) {
				echo json_encode($jobs->removeFile($_POST["type"]));
			}
			break;
		case "cloneJob":
			$jobs = new Jobs($_POST["pid"]);
			$lecturer = new Lecturer();
			if ($lecturer->isLogined()) {
				echo json_encode($jobs->cloneJob($lecturer));
			}
			break;
		case "getApplicationByStudent":
			$student = new Student($_POST["student_id"]);
			$list = array();

			foreach ($student->getApplicationHistory() as $application) {
				if ($application["application"]["status"] != 'NEW') {
					$list[] = array("value"=>$application["application"]["id"], "label"=>$application["application"]["app_no"]);
				}
			}

			echo json_encode($list);
			break;

		default:
			header("HTTP/1.0 404 Not Found");
			break;
	}

	session_write_close();