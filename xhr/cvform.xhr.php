<?php
	require_once("../config.inc.php");
	require_once("../classes/student.php");

	$student = new Student();
	if (!$student->isLogined()) {
		header("HTTP/1.0 404 Not Found");
		break;
	}
	switch ($_POST["action"]) {
		case "removeMeta":
			if ($_POST["id"]) {
				echo json_encode($student->removeMeta($_POST["application_id"], $_POST["id"]));
			}
			break;

		case "#modalEmergencyContact": 
			$postdata = array();
			
			$postdata["name"] = trim($_POST["name"]);
			$postdata["relationship"] = trim($_POST["relationship"]);
			$postdata["home"] = trim($_POST["home"]);
			$postdata["contact"] = trim($_POST["contact"]);

			if ($postdata["name"] != "") {
				echo json_encode(["html"=>$student->saveEmergencyContact( $postdata)]);
			}
			break;
		case '#modalEducation':

			$postdata = array();
			
			$postdata["fromdate"] = trim($_POST["fromdate"]);
			$postdata["todate"] = trim($_POST["todate"]);
			if ($_POST["topresent"] == "1") {
				$postdata["todate"] = "Present";
			}
			$postdata["school"] = trim($_POST["school"]);
			$postdata["class"] = trim($_POST["class"]);
			$postdata["year"] = trim($_POST["year"]);

			if ($postdata["class"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "education", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "education", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;

		case '#modalProfQuali':

			$postdata = array();
			
			$postdata["name"] = trim($_POST["name"]);
			$postdata["organization"] = trim($_POST["organization"]);
			$postdata["date"] = trim($_POST["date"]);
			$postdata["grade"] = trim($_POST["grade"]);

			if ($postdata["name"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "prof_qualification", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "prof_qualification", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;
		case '#modalExperience':

			$postdata = array();
			
			$postdata["fromdate"] = trim($_POST["fromdate"]);
			$postdata["todate"] = trim($_POST["todate"]);
			if ($_POST["topresent"] == "1") {
				$postdata["todate"] = "Present";
			}
			$postdata["employment"] = trim($_POST["employment"]);
			$postdata["organization"] = trim($_POST["organization"]);
			$postdata["position"] = trim($_POST["position"]);
			$postdata["duties"] = trim($_POST["duties"]);

			if ($postdata["position"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "work_experience", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "work_experience", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;
		case '#modalOthExperience':

			$postdata = array();
			
			$postdata["fromdate"] = trim($_POST["fromdate"]);
			$postdata["todate"] = trim($_POST["todate"]);
			if ($_POST["topresent"] == "1") {
				$postdata["todate"] = "Present";
			}
			$postdata["employment"] = trim($_POST["employment"]);
			$postdata["organization"] = trim($_POST["organization"]);
			$postdata["position"] = trim($_POST["position"]);
			$postdata["duties"] = trim($_POST["duties"]);

			if ($postdata["position"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "internship_experience", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "internship_experience", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;

		case '#modalOthQuali':

			$postdata = array();
			
			$postdata["name"] = trim($_POST["name"]);
			$postdata["organization"] = trim($_POST["organization"]);
			$postdata["date"] = trim($_POST["date"]);
			$postdata["grade"] = trim($_POST["grade"]);

			if ($postdata["name"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "oth_qualification", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "oth_qualification", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;
		case "#modalReference":
			$postdata = array();
			
			$postdata["name"] = trim($_POST["name"]);
			$postdata["organization"] = trim($_POST["organization"]);
			$postdata["position"] = trim($_POST["position"]);
			$postdata["contact"] = trim($_POST["contact"]);
			$postdata["email"] = trim($_POST["email"]);


			if ($postdata["name"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "reference", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "reference", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;

		case "#modalAchievement":
			$postdata = array();
			
			$postdata["name"] = trim($_POST["name"]);
			$postdata["organization"] = trim($_POST["organization"]);
			$postdata["date"] = trim($_POST["date"]);

			if ($postdata["name"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "achievement", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "achievement", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;

		case "#modalExActivity":
			$postdata = array();
			
			$postdata["fromdate"] = trim($_POST["fromdate"]);
			$postdata["todate"] = trim($_POST["todate"]);
			if ($_POST["topresent"] == "1") {
				$postdata["todate"] = "Present";
			}
			$postdata["organization"] = trim($_POST["organization"]);
			$postdata["position"] = trim($_POST["position"]);
			$postdata["duties"] = trim($_POST["duties"]);


			if ($postdata["organization"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "exactivity", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "exactivity", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;
		
		case "#modalSkills":
			$postdata = array();
			$postdata = json_decode($_POST["language"], true);

			if ($postdata["language"] != "") {
				echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "skill", $postdata, "")]);
			}
			break;

		case "#modalExam":
			$postdata = array();
			$postdata["exam"] = $student->getExamType($_POST["exam"]);
			$postdata["exam"] = $postdata["exam"]["node_name"];
			
			$postdata["year"] = $_POST["year"];
			$postdata["exam_result"] = json_decode($_POST["exam_result"], true);
			
			if ($postdata["exam"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "exam_result", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "exam_result", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;
		case "#modalOtherSkill":

			$postdata = array();
			$postdata["skill"] = $_POST["skill"];
			$postdata["description"] = htmlentities($_POST["description"], ENT_QUOTES, "UTF-8");
			
			if ($postdata["skill"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "other_skill", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "other_skill", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}

		case "#modalPublicExam": 
			$postdata = array();
			$postdata["date"] = $_POST["date"];
			$postdata["name"] = $_POST["name"];
			$postdata["organization"] = $_POST["organization"];
			$postdata["grade"] = $_POST["grade"];
			
			if ($postdata["name"] != "") {
				if (!array_key_exists("update", $_POST)) {
					echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "public_exam", $postdata, $_POST["template"])]);
				} else {
					echo json_encode(["html"=>$student->updateMeta($_POST["application_id"], "public_exam", $postdata, $_POST["template"], $_POST["update"])]);
				}
			}
			break;

		case "#addDateTravel":
			$postdata = array();
			$postdata["date_travel"] = $_POST["date_travel"];
			if ($postdata["date_travel"] != "") {
				echo json_encode(["html"=>$student->saveMeta($_POST["application_id"], "date_travel", $postdata, "")]);
			}
			break;
		case "tips":
			echo json_encode(array("html" => $student->getTips($_POST["tag"])));

			break;
		case "uploadFile":
			echo json_encode($student->uploadFile($_POST["application_id"], $_POST["type"]));
			break;
		case "updateFile":
			echo json_encode($student->updateFile($_POST["application_id"], $_POST["type"]));
			break;

		case "getEditExamForm":
			echo json_encode($student->getExamForm($_POST["pid"]));
			break;
		case "removeCVAttachment":
			echo json_encode($student->removeCVAttachment($_POST["sess_id"], $_POST["sess_key"]));
			break;
		case "autosave" :
			$student->saveCVForm($_POST["application_id"], $_POST);
			echo true;
			break;
		default:
			header("HTTP/1.0 404 Not Found");
			break;
	}
	session_write_close();
