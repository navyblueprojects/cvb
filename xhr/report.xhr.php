<?php
	include_once("../config.inc.php");
	require_once("../classes/lecturer.php");

	$member = Lecturer::retain();
	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}

	switch ($_GET["field"]) {
		case 'jobs.job_no':
		case 'jobs.dept':
		case 'jobs.name':
			require_once("../classes/jobs.php");
			$cms = new Jobs();
			echo json_encode($cms->getXhrList($_GET["field"], $_GET["q"]));
			break;
		case 'std.prog_code':
			require_once("../classes/system.php");
			$cms = new Program();
			echo json_encode($cms->getXhrList($_GET["q"]));

			break;
		case 'student_id':
			require_once("../classes/student.php");
			$cms = new Student();
			echo json_encode($cms->getXhrList($_GET["q"]));

			break;
		case "wie_app_no":
			require_once("../classes/application.php");
			$cms = new Application();
			echo json_encode($cms->getXhrList($_GET["q"]));
			break;
		default:
			header("HTTP/1.0 404 Not Found");
			break;
	}
	
	session_write_close();
?>	