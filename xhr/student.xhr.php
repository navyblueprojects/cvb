<?php
	require_once("../config.inc.php");
	require_once("../classes/student.php");

	$student = new Student();

	switch ($_POST["action"]) {
		case 'login':
			echo $student->login($_POST["student_id"], $_POST["password"], $_POST["captcha"]);
			break;
		default:
			header("HTTP/1.0 404 Not Found");
			break;
	}