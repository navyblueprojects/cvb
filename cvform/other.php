<h2><span class="fa fa-star"></span> Others</h2>
<div class="panel-group" id="other_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-other">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#other_accordion" href="#interview1" aria-expanded="true" aria-controls="interview1">
          Job Request
        </a>
      </h4>
    </div>
    <div id="interview1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12"><label>Availability</label>
            <input type="text" name="interview[availability]" class="form-control" data-tid="availability" id="availability" value="<?=$member->cvform["application"]["availability"]?>" maxlength="100" /></div>
        </div>
        <div class="row">
          <div class="col-md-12"><label>Expected Salary</label>
            <input type="text" name="interview[expected_salary]" class="form-control" data-tid="expected_salary" id="expected_salary" value="<?=$member->cvform["application"]["expected_salary"]?>" maxlength="100" /></div>
        </div>

      </div>
    </div>
  </div>
  
 


  <div class="panel panel-other">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#other_accordion" href="#interview4" aria-expanded="false" aria-controls="interview4">
          Additional information
        </a>
      </h4>
    </div>
    <div id="interview4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <textarea  name="interview[remarks]" data-tid="remarks" id="remarks" class="form-control"><?=$member->cvform["application"]["remarks"]?></textarea>
      </div>
    </div>
  </div>
  <div class="panel panel-other">
    <?php
    $other = json_decode($member->cvform["other"][0]["content"], true);
    $other_id = ($member->cvform["other"][0]["id"]) ? '[id]['.$member->cvform["other"][0]["id"].']' : '[new][]';
    $new = ($member->cvform["other"][0]["id"]) ? false : true;
    ?>
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#other_accordion" href="#other1" data-default-title="<i>Create subtitle 1</i>" aria-expanded="true" aria-controls="other1">
          <?= ($new || trim($other["title"])=="") ? "<i>Create subtitle 1</i>" :$other["title"]?>
        </a>
      </h4>
    </div>
    <div id="other1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <label>Subtitle</label>
        <input type="text" name="other[title]<?=$other_id?>" value="<?=$other["title"]?>" class="form-control" data-section="other1" data-tid="other1" />
        <label>Content</label>
        <textarea name="other[content]<?=$other_id?>" id="other1-textarea" class="ckeditor"><?=html_entity_decode($other["html"], ENT_QUOTES, "UTF-8"); ?></textarea>
        <div class="hide strip_html"><?=strip_tags(html_entity_decode($other["html"], ENT_QUOTES, "UTF-8")); ?></div>
      </div>
    </div>
  </div>
  <?php
    $other = json_decode($member->cvform["other"][1]["content"], true);
    $other_id = ($member->cvform["other"][1]["id"]) ? '[id]['.$member->cvform["other"][1]["id"].']' : '[new][]';
    $new = ($member->cvform["other"][1]["id"]) ? false : true;
    ?>
  <div class="panel panel-other">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#other_accordion" href="#other2" data-default-title="<i>Create subtitle 2</i>" aria-expanded="false" aria-controls="other2">
          <?= ($new || trim($other["title"])=="") ? "<i>Create subtitle 2</i>" :$other["title"]?>
        </a>
      </h4>
    </div>
    <div id="other2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <label>Subtitle</label>
        <input type="text" name="other[title]<?=$other_id?>" value="<?=$other["title"]?>" class="form-control" data-section="other2" data-tid="other2"/>
        <label>Content</label>
        <textarea name="other[content]<?=$other_id?>" id="other2-textarea" class="ckeditor"><?=html_entity_decode($other["html"], ENT_QUOTES, "UTF-8"); ?></textarea>
        <div class="hide strip_html"><?=strip_tags(html_entity_decode($other["html"], ENT_QUOTES, "UTF-8")); ?></div>
      </div>
    </div>
  </div>
  <?php
    $other = json_decode($member->cvform["other"][2]["content"], true);
    $other_id = ($member->cvform["other"][2]["id"]) ? '[id]['.$member->cvform["other"][2]["id"].']' : '[new][]';
    $new = ($member->cvform["other"][2]["id"]) ? false : true;
    ?>
    <div class="panel panel-other">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#other_accordion" href="#other3"  data-default-title="<i>Create subtitle 3</i>" aria-expanded="false" aria-controls="other3">
          <?= ($new || trim($other["title"])=="") ? "<i>Create subtitle 3</i>" :$other["title"]?>
        </a>
      </h4>
    </div>
    <div id="other3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <label>Subtitle</label>
        <input type="text" name="other[title]<?=$other_id?>" value="<?=$other["title"]?>" class="form-control" data-section="other3" data-tid="other3"/>
        <label>Content</label>
        <textarea name="other[content]<?=$other_id?>" id="other3-textarea" class="ckeditor"><?=html_entity_decode($other["html"], ENT_QUOTES, "UTF-8"); ?></textarea>
        <div class="hide strip_html"><?=strip_tags(html_entity_decode($other["html"], ENT_QUOTES, "UTF-8")); ?></div>
      </div>
    </div>
  </div>
   <div class="row rowbtn">
    <div class=" hidden-xs col-sm-6"><a class="btn " data-active="skills"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right hidden-xs col-sm-6"><a  class="btn " data-active="attachment">Next <i class="fa fa-caret-right"></i></a></div>
     <div class="visible-xs col-xs-5"><a  data-active="skills"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right visible-xs col-xs-7"><a  data-active="attachment">Next <i class="fa fa-caret-right"></i></a></div>
  </div>

</div>    