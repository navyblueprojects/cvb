<h2 ><span class="fa fa-graduation-cap"></span> Education</h2>
<div class="panel-group" id="education_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-education">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#education_accordion" href="#education1" aria-expanded="true" aria-controls="education1">
          Education
        </a>
      </h4>
    </div>
    <div id="education1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <ul class="list-group <?=$member->cvform["require_data"]["modalEducation_list"]?>" id="modalEducation_list"><?php
          foreach ($member->cvform["education"] as $edd) {
            $ed = json_decode($edd["content"], true);
        ?><li class="list-group-item"  data-id='<?=$edd["id"]?>' data-content='<?=str_replace("'", "&#39;", $edd["content"])?>'>
            <div><span class="education-date"><?=$ed["fromdate"]?> - <?=$ed["todate"]?></span><?=$ed["school"]?></div>
            <div><?=$ed["class"]?></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>
        </ul>
        <p><a href="#" data-tid="addEducation" data-toggle="modal" data-target="#modalEducation">Add Education</a></p>
        <h3>GPA</h3>
          <div class="row">
            <div class="col-sm-2">CGPA: </div>
            <div class="col-sm-2"><select data-tid="gpa" name="basic[gpa][school]" id="gpa_school" class="form-control">
                <option value="HKCC" <?=($member->cvform["application"]["gpa"]["school"] == "HKCC") ? 'selected="selected"' : ''?>>HKCC</option>
                <option value="SPEED" <?=($member->cvform["application"]["gpa"]["school"] == "SPEED") ? 'selected="selected"' : ''?>>SPEED</option>
              </select></div>
            <div class="col-sm-2"><input type="text" class="form-control <?=$member->cvform["require_data"]["gpa"]?>" data-tid="gpa" name="basic[gpa][point]" id="gpa_point" value="<?=number_format($member->cvform["application"]["gpa"]["point"],  2)?>" placeholder="" /></div>
            <div class="col-sm-2 ">Date Obtained:</div>
            <div class="col-sm-3">
              <?php
                if ($member->cvform["application"]["gpa"]["date"]) {
                  $dateobtained = date("M Y", strtotime($member->cvform["application"]["gpa"]["date"]));
                }
              ?>
              <label class="input"><input type="text" data-tid="gpa" name="basic[gpa][date]" value="<?=$dateobtained?>" placeholder="Click to choose" id="gpa_date" class="form-control <?=$member->cvform["require_data"]["gpa"]?> monthpicker" /></label>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">CGPA of <br>Pre-Associate Degree:</div>
            <div class="col-sm-2"><label class="input"><input type="text" data-tid="cgpa"   name="basic[cgpa][point]" id="cgpa_point" value="<?=number_format($member->cvform["application"]["cgpa"]["point"],  2)?>" placeholder="" class="form-control <?=($member->cvform["application"]["cgpa"]["point"] == 0) ? '' : 'req'?>"  <?=($member->cvform["application"]["cgpa"]["point"] == 0) ? 'readonly="readonly"' : ''?>  /></label></div>
            <?php
                if ($member->cvform["application"]["cgpa"]["date"]) {
                  $dateobtained = date("M Y", strtotime($member->cvform["application"]["cgpa"]["date"]));
                }

            ?><div class="col-sm-2 ">Date Obtained:</div>
            <div class="col-sm-3"><label class="input"><input type="text" data-tid="cgpa" placeholder="Click to choose" name="basic[cgpa][date]" id="cgpa_date" value="<?=$dateobtained?>"  class="form-control <?=($member->cvform["application"]["cgpa"]["point"] == 0) ? '' : 'req'?> monthpicker" <?=($member->cvform["application"]["cgpa"]["point"] == 0) ? 'readonly="readonly"' : ''?>  /></label></div>
            <div class="col-sm-1 text-right"  ><label for="na_cgpa"><input type="checkbox" id="na_cgpa" data-removereq="cgpa_point, #cgpa_date, #cgpa_college" 
              <?=($member->cvform["application"]["cgpa"]["point"] == 0) ? 'checked="checked"' : ''?> /> N/A </label></div>
          </div>
          <div class="row">
            <div class="col-sm-4">College:</div>
            <div class="col-sm-8"><label class="input"><input type="text" data-tid="cgpa"   name="basic[cgpa][college]" id="cgpa_college" value="<?=$member->cvform["application"]["cgpa"]["college"]?>" placeholder="" class="form-control <?=($member->cvform["application"]["cgpa"]["point"] == 0) ? '' : 'req'?>"  <?=($member->cvform["application"]["cgpa"]["point"] == 0) ? 'readonly="readonly"' : ''?>  /></label></div>
            
          </div>
      </div>
    </div>
  </div>
  <div class="panel panel-education">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#education_accordion" href="#education2" aria-expanded="false" aria-controls="education2">
          Academic Attainment
        </a>
      </h4>
    </div>
    <div id="education2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
          
        
        <h3>DSE/HKAL/HKCE Results</h3>
        <ul class="<?=$member->cvform["require_data"]["modalExam_list"]?> list-group" id="modalExam_list" >
        <?php
        
        foreach ($member->cvform["exam_result"] as $iq) {
          $q = json_decode($iq["content"], true);
          //$q["exam"] = $member->getExamType($q["exam"]);

          ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>' data-id='<?=$iq["id"]?>' >
            <div><span class="education-date"><?=$q["year"]?></span><?=$q["exam"]?></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
        }
        ?>
        </ul>
        <p><a href="#" data-tid="addExam" data-toggle="modal" data-target="#modalExam">Add DSE/HKAL/HKCE Examination Result</a></p>
        <?php
          if (count($member->cvform["public_exam"]) == 0) {
            $selected = ($member->cvform["application"]["status"] == 'new') ? "" : 'checked="checked"';
          }
        ?><div class="pull-right na-wrapper <?=(count($member->cvform["public_exam"]) > 0) ? "hide": ""?>" data-na="modalPublicExam_list"><label for="na_modalPublicExam"><input type="checkbox" id="na_modalPublicExam" data-remove-req-row="modalPublicExam_list" <?=$selected?> /> N/A </label></div><?php
          
        ?>
        <h3>Other Public Examination Results</h3>
        <ul class="<?=($selected == "") ? "req_row" : ""?> list-group" id="modalPublicExam_list" >
        <?php
        
        foreach ($member->cvform["public_exam"] as $iq) {
          $q = json_decode($iq["content"], true);

          $q["grade"] = json_decode($q["grade"], true);
          $array = array();
          foreach ($q["grade"] as $i=>$g) {
            $array[] = $i . "(".$g.")";
          }
          $q["grade"] = implode(", ", $array);
          ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>' data-id='<?=$iq["id"]?>' >
            <div><span class="education-date"><?=$q["date"]?></span><?=$q["name"]?><br /> Result: <?=$q["grade"]?></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
        }
        ?>
        </ul>
        <p><a href="#" data-tid="addPublicExam" data-toggle="modal" data-target="#modalPublicExam">Add Other Public Examination Result</a></p>
      </div>
    </div>
  </div>
  <div class="row rowbtn">
    <div class="hidden-xs col-sm-6"><a  class="btn " data-active="basic"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right hidden-xs col-sm-6"><a  class="btn " data-active="qualification">Next <i class="fa fa-caret-right"></i></a></div>
    <div class="visible-xs col-xs-5"><a  data-active="basic"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right visible-xs col-xs-7"><a  data-active="qualification"> Next<i class="fa fa-caret-right"></i></a></div>
  </div>
</div>
<div class="modal fade" role="dialog" id="modalExam" aria-labelledby="lblModalExam">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalExam">Add Examination Result</h4>
      </div>
      <div class="modal-body">
        <form class="form">
            <div class="row">
              <div class="col-md-2 col-xs-4"><label>Examination: </label></div>
              <div class="col-md-3 col-xs-8"><select class="form-control" name="exam" id="exam_list"><?php
              $exam_object = array();
              foreach ($exam_result as $cat) {
              ?><option value="<?=$cat["node_id"]?>"><?=$cat["node_name"]?></option><?php
                $exam_object[$cat["node_id"]] = $cat["node_name"];
              }
              ?></select><label id="lbl_exam_list"></label></div>
              <div class="col-md-2 col-xs-4"><label>Year: </label></div>
              <div class="col-md-3 col-xs-4"><input type="text" class="form-control number" name="year" id="exam_date" placeholder="(E.g. 2010)" maxlength="4"   /><label id="lbl_exam_date"></label></div>
              <div class="col-md-2 col-xs-4"><a class="btn btn-default" id="add_exam_row">Enter</a></div>
            </div>
            <div class="row">
              <div class="col-md-2 col-xs-12"><label>Result: </label></div><?php
              foreach ($exam_result as $cat) {
                ?><ul class="col-md-10 col-xs-12 hide exam_ul" id="exam_<?=$cat["node_id"]?>">
                  <li class="row compulsory"><div class="col-sm-8 col-xs-6"><label>Subject</label></div>
                                  <div class="col-sm-2 col-xs-6 text-right"><label>Grade</label></div></li>
                  <?php
                  foreach ($cat["compulsory"] as $subject) {
                    
                    ?><li class="row compulsory">
                      <div class="col-sm-8 col-xs-6"><?=$subject["name"]?></div>
                      <div class="col-sm-2 col-xs-6 text-right"><?php
                        switch($subject["grade"]) {
                          case "1" :
                            ?><select  data-grade="<?=$subject["name"]?>">
                              <option value="">- Select -</option>
                              <option value="5**">5**</option>
                              <option value="5*">5*</option>
                              <option value="5">5</option>
                              <option value="4">4</option>
                              <option value="3">3</option>
                              <option value="2">2</option>
                              <option value="1">1</option>
                            </select><?php
                            break;
                          case "2" :
                            ?><select  data-grade="<?=$subject["name"]?>">
                              <option value="">- Select -</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                              <option value="E">E</option>
                              <option value="F">F</option>
                              <option value="U">U</option>
                            </select><?php
                            break;
                          case "3" :
                            ?><select  data-grade="<?=$subject["name"]?>">
                              <option value="">- Select -</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                              <option value="E">E</option>
                              <option value="U">U</option>
                            </select><?php
                            break;

                        }
                      ?></div>
                      <div class="addon"></div></li><?
                  }
                ?></ul><?php
              }
            ?></div>
            <textarea class="hide" name="exam_result" id="exam_result"></textarea>
            <div class="row hide" id="row_add_subject">
              <div class="col-md-2"><label>Subject: </label></div>
              <div class="col-md-5 col-xs-6 "><select class="select2" id="exam_subject"></select></div>
              <div class="col-md-3 col-xs-4">
                <select class=" hide" data-name="grade" id="exam_subject-grade1">
                  <option value="">- Select -</option>
                  <option value="5**">5**</option>
                  <option value="5*">5*</option>
                  <option value="5">5</option>
                  <option value="4">4</option>
                  <option value="3">3</option>
                  <option value="2">2</option>
                  <option value="1">1</option>
                </select>
                <select class=" hide" data-name="grade" id="exam_subject-grade2">
                  <option value="">- Select -</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="C">C</option>
                  <option value="D">D</option>
                  <option value="E">E</option>
                  <option value="F">F</option>
                  <option value="U">U</option>
                </select>
                <select class=" hide" data-name="grade" id="exam_subject-grade3">
                  <option value="">- Select -</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="C">C</option>
                  <option value="D">D</option>
                  <option value="E">E</option>
                  <option value="U">U</option>
                </select>
              </div>
              <div class="col-md-2 col-xs-2"><a class="btn btn-default" id="add_subject_result">Add</a></div>
            </div>
        </form>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalExam" data-validate="chkModalExam()">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  var exam_object = <?=json_encode($exam_object)?>;

</script>
<div class="modal fade" tabindex="-1" role="dialog" id="modalEducation" aria-labelledby="lblModalEducation">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalEducation">Add Education</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-4">
            <label for="frmedu_fromdate">From: </label>
            <input type="text" name="fromdate" placeholder="Click to choose" id="frmedu_fromdate" class="reqModal monthpicker form-control" autocomplete="off" />
          </div>
          <div class="form-group col-md-4">
            <label for="frmedu_todate">To: </label>
            <input type="text" name="todate" placeholder="Click to choose" id="frmedu_todate" class="reqModal monthpicker form-control" autocomplete="off"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmedu_topresent"><input type="checkbox" name="topresent" id="frmedu_topresent" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmedu_school">School/ College </label>
            <input type="text" name="school" id="frmedu_school" maxlength="200" spellcheck="true" class="reqModal form-control" placeholder="(Max 200 chars)" />
          </div>
          <div class="form-group col-md-12">
            <label for="frmedu_class">Class/Course of Study: </label>
            <input type="text" name="class" id="frmedu_class" maxlength="200" spellcheck="true" class="reqModal form-control" placeholder="(Max 200 chars)"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmedu_class">Study Year: </label>
            <input type="text" name="year" id="frmedu_year" maxlength="100" placeholder="not default have 'Year', need key in" spellcheck="true" class="reqModal form-control"/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalEducation" data-validate="chkModal('modalEducation')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" tabindex="-1" role="dialog" id="modalPublicExam" aria-labelledby="lblModalPublicExam">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalPublicExam">Add Public Examination</h4>
      </div>
      <div class="modal-body">
          <form class="form">

          <div class="form-group ">
            <label for="frmpe_name">Examination/Certification: </label>
            <input type="text" name="name" id="frmpe_name" maxlength="200" spellcheck="true" class="reqModal form-control" placeholder="(Maximum 200 characters)" />
          </div>
          <div class="form-group ">
            <label for="frmpe_organization">Name of Issuing Organization: </label>
            <input type="text" name="organization" id="frmpe_organization" maxlength="200" spellcheck="true" class="reqModal form-control" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group ">
            <label for="frmpe_date">Date obtained: </label>
            <input type="text" name="date" id="frmpe_date" placeholder="Click to choose" class="monthpicker reqModal form-control"/>
          </div>
          <div class="form-group ">
            <label for="frmpe_grade">Grade/Level Obtained: </label>
            <textarea type="text" name="grade" id="frmpe_grade" class="reqModal hide"></textarea>
            
            <ul class="list-group" id="public_exam_grades" >
              <li class="list-group-item hide">
                <div class="row"> <div class="col-md-4"><span data-span="subject"></span></div><div data-span="grade" class="col-md-4 text-right"></div></div>
                <div class="addon"><a class="btn btn-remove-row" title="Delete"><span class="fa fa-times-circle"></span></a></div>
              </li>
            </ul>
            <div class="row"> 
              <div class="col-xs-6"><input type="text" class="form-control" id="subject" maxlength="100" placeholder="Subject/Level Name (Max. 100 Chars)" /></div>
              <div class="col-xs-4 "><input type="text" class="form-control" id="grade" maxlength="100" placeholder="Result (E.g. Merit)" /></div>
              <div class="col-xs-12 col-sm-2"><a class="btn btn-default btn-add-grade pull-right"  title="Add subject">Add</a></div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalPublicExam" data-validate="chkModal('modalPublicExam')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" role="dialog" id="modalExam_edit" aria-labelledby="lblModalExam">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalExam">Edit Examination Result</h4>
      </div>
      <div class="modal-body">
        <form class="form">
            <div class="row">
              <div class="col-md-2"><label>Examination: </label><a data-toggle="tooltip" data-placement="top" title="You cannot change the exam here. "><i class="fa fa-question-circle"></i></a></div>
              <div class="col-md-3"><label id="lbl_exam_list"><input type="text" class="form-control" name="exam" readonly="true" id="exam_name" /></label></div>
                  
              <div class="col-md-2"><label>Year: </label></div>
              <div class="col-md-3"><input type="text" class="form-control" name="year" id="exam_date_edit" placeholder="(E.g. 2012)" maxlength="4" /><label id="lbl_exam_date"></label></div>
              
            </div>
            <div class="row">
              <div class="col-md-2"><label>Result: </label></div>
              <ul class="col-md-10 hide exam_ul" >
                <li class="row"><div class="col-sm-8"><label>Subject</label></div>
                                <div class="col-sm-2 text-right"><label>Grade</label></div></li>
              </ul>

              </div>
            <textarea class="hide" name="exam_result" id="exam_result_edit"></textarea>
            <div class="row " id="row_add_subject">
              <div class="col-md-2"><label>Subject: </label></div>
              <div class="col-md-5"><select class="select2" id="exam_subject_edit"></select></div>
              <div class="col-md-3">
                <select class=" hide" data-name="grade" id="exam_subject_edit-grade1">
                  <option value="">- Select -</option>
                  <option value="5**">5**</option>
                  <option value="5*">5*</option>
                  <option value="5">5</option>
                  <option value="4">4</option>
                  <option value="3">3</option>
                  <option value="2">2</option>
                  <option value="1">1</option>
                </select>
                <select class=" hide" data-name="grade" id="exam_subject_edit-grade2">
                  <option value="">- Select -</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="C">C</option>
                  <option value="D">D</option>
                  <option value="E">E</option>
                  <option value="F">F</option>
                  <option value="U">U</option>
                </select>
                <select class="hide" data-name="grade" id="exam_subject_edit-grade3">
                  <option value="">- Select -</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="C">C</option>
                  <option value="D">D</option>
                  <option value="E">E</option>
                  <option value="U">U</option>
                </select>
              </div>
              <div class="col-md-2"><a class="btn btn-default" id="edit_subject_result_add">Add</a></div>
            </div>
        </form>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalExam" data-validate="chkModalExam_edit()">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalEducation_edit" aria-labelledby="lblModalEducation">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalEducation">Edit Education</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-4">
            <label for="frmedu_fromdate">From: </label>
            <input type="text" name="fromdate" placeholder="Click to choose" id="frmedu_fromdate_edit" class="reqModal monthpicker form-control" autocomplete="off" />
          </div>
          <div class="form-group col-md-4">
            <label for="frmedu_todate">To: </label>
            <input type="text" name="todate" placeholder="Click to choose" id="frmedu_todate_edit" class="reqModal monthpicker form-control" autocomplete="off"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmedu_topresent"><input type="checkbox" name="topresent" id="frmedu_topresent_edit" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmedu_school">School/ College </label>
            <input type="text" name="school" id="frmedu_school_edit" maxlength="200" spellcheck="true" placeholder="(Maximum: 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmedu_class">Class/Course of Study: </label>
            <input type="text" name="class" id="frmedu_class_edit" maxlength="200" spellcheck="true" placeholder="(Maximum: 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmedu_class">Study Year: </label>
            <input type="text" name="year" maxlength="100" placeholder="not default have 'Year', need key in" id="frmedu_year_edit" class="reqModal form-control"/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalEducation" data-validate="chkModal('modalEducation_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalPublicExam_edit" aria-labelledby="lblModalPublicExam">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalPublicExam">Edit Public Examination</h4>
      </div>
      <div class="modal-body">
          <form class="form"><div class="form-group col-md-12">
            <label for="frmpe_name_edit">Examination/Certification: </label>
            <input type="text" name="name" id="frmpe_name_edit" maxlength="200" spellcheck="true" class="reqModal form-control" placeholder="(Maximum 200 characters)" />
          </div>
          <div class="form-group col-md-12">
            <label for="frmpe_organization_edit">Name of Issuing Organization: </label>
            <input type="text" name="organization" id="frmpe_organization_edit" maxlength="200" spellcheck="true" class="reqModal form-control" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmpe_date_edit">Date obtained: </label>
            <input type="text" name="date" id="frmpe_date_edit" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmpe_grade_edit">Grade/Level Obtained: </label>
            <textarea type="text" name="grade" id="frmpe_grade_edit" class="reqModal hide"></textarea>
            <ul class="list-group exam_ul" id="public_exam_grades_edit">
            </ul>
            <div class="row"> 
              <div class="col-md-6"><input type="text" class="form-control" id="subject" maxlength="100" placeholder="Subject/Level Name (Max. 100 Chars)" /></div>
              <div class="col-md-4 text-right"><input type="text" class="form-control" maxlength="100" id="grade" placeholder="Result (E.g. Merit)" /></div>
              <div class="addon col-md-2"><a class="btn btn-default btn-edit-grade"  title="Add subject">Add</a></div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalPublicExam" data-validate="chkModal('modalPublicExam_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="hide">
  <ul id="modalEducation_template">
    <li class="list-group-item" data-content="[content]" data-id="[id]">
      <div><span class="education-date">[fromdate] - [todate]</span>[school]</div>
      <div>[class]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
  <ul id="modalProfQuali_template">
    <li class="list-group-item" data-content="[content]" data-id="[id]">
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
      <div><span class="education-date">[date]</span>[name]</div>
      <div><span>Grade: [grade]</span>
          <span>Organization: [organization]</span></div>
    </li>
  </ul>
  <ul id="modalOthQuali_template">
    <li class="list-group-item" data-content="[content]" data-id="[id]">
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
      <div><span class="education-date">[date]</span>[name]</div>
      <div><span>Grade: [grade]</span>
      <span>Organization: [organization]</span></div>
    </li>
  </ul>
  <ul id="modalExam_template">
    <li class="list-group-item" data-content="[content]" data-id="[id]">
      <div><span class="education-date">[year]</span>[exam]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
  <ul id="modalPublicExam_template">
    <li class="list-group-item" data-content="[content]" data-id="[id]" >
      <div><span class="education-date">[date]</span>[name]<br>[grade]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
</div>
