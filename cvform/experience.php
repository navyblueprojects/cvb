  <h2><span class="fa fa-briefcase"></span> Experience(s)</h2>
<div class="panel-group" id="experience_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-experience">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#experience_accordion" href="#experience1" data-tid="addExperience" aria-expanded="true" aria-controls="experience1">
          Work Experience(s)
        </a>
        <?php
          if (count($member->cvform["experience"]["work"]) == 0) {
            $selected = ($member->cvform["application"]["status"] == 'new') ? "" : 'checked="checked"';
          }
        
         if ($member->cvform["require_data"]["modalExperience_list"] == '') {
          ?><label for="na_modalExperience" class="pull-right <?=count($member->cvform["experience"]["work"]) == 0 ? "" : "hide"?>"><input type="checkbox" id="na_modalExperience" data-remove-req-row="modalExperience_list" <?=$selected?>/> N/A </label><?php
        }
          ?>
      </h4>
    </div>
    <div id="experience1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <ul class="list-group  " id="modalExperience_list"><?php
          foreach ($member->cvform["experience"]["work"] as $iq) {
            $q = json_decode($iq["content"], true);
        ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>' data-id="<?=$iq["id"]?>">
            <div><span class="education-date"><?=$q["fromdate"]?> - <?=$q["todate"]?></span><?=$q["position"]?></div>
            <div>Organization: <?=$q["organization"]?> Employment: <?=$q["employment"]?></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>
        </ul>
          <p><a href="#" data-tid="addExperience" data-toggle="modal" data-target="#modalExperience">Add Work Experience</a></p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-experience">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#experience_accordion" href="#experience2" data-tid="addIntExperience" aria-expanded="false" aria-controls="experience2">
          Internship Experience(s)
        </a>
         <?php 
          if (count($member->cvform["experience"]["internship"]) == 0) {
            $selected = ($member->cvform["application"]["status"] == 'new') ? "" : 'checked="checked"';
          }

          if ($member->cvform["require_data"]["modalOthExperience_list"] == '') {
          ?><label for="na_modalOthExperience" class="pull-right <?=count($member->cvform["experience"]["internship"]) == 0 ? "" : "hide"?>"><input type="checkbox" id="na_modalOthExperience" data-remove-req-row="modalOthExperience_list" <?=$selected?>/> N/A </label><?php
        }
          ?>
      </h4>
    </div>
    <div id="experience2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <ul class="list-group <?=$member->cvform["require_data"]["modalOthExperience_list"]?>" id="modalOthExperience_list"><?php
          foreach ($member->cvform["experience"]["internship"] as $iq) {
            $q = json_decode($iq["content"], true);
        ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>'  data-id="<?=$iq["id"]?>">
            <div><span class="education-date"><?=$q["fromdate"]?> - <?=$q["todate"]?></span><?=$q["position"]?></div>
            <div>Organization: <?=$q["organization"]?> Employment: <?=$q["employment"]?></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>
        </ul>
          <p><a href="#" data-tid="addIntExperience" data-toggle="modal" data-target="#modalOthExperience">Add Internship Experience</a></p>
      </div>
    </div>
  </div>

  <div class="panel panel-experience">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#experience_accordion" href="#experience3" data-tid="addExActivity" aria-expanded="false" aria-controls="experience3">
          Extra-Curricular Activity(ies)
        </a><?php        
          if (count($member->cvform["achievement"]["exactivity"]) == 0) {
            $selected = ($member->cvform["application"]["status"] == 'new') ? "" : 'checked="checked"';
          }
          if ($member->cvform["require_data"]["modalExActivity_list"] == '') {
            ?><label for="na_modalExActivity" class="pull-right <?=count($member->cvform["achievement"]["exactivity"]) == 0 ? "" : "hide"?>"><input type="checkbox" id="na_modalExActivity" data-remove-req-row="modalExActivity_list" <?=$selected?>/> N/A </label><?php
          }
        ?></h4>
    </div>
    <div id="experience3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        
        <ul class="list-group <?=$member->cvform["require_data"]["modalExActivity_list"]?>" id="modalExActivity_list"><?php
          foreach ($member->cvform["achievement"]["exactivity"] as $iq) {
            $q = json_decode($iq["content"], true);
        ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>' data-id='<?=$iq["id"]?>'>
            <div><span class="education-date"><?=$q["fromdate"]?> - <?=$q["todate"]?></span><?=$q["position"]?></div>
            <div>Organization: <?=$q["organization"]?> </div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>
        </ul>
          <p><a href="#" data-tid="addExActivity" data-toggle="modal" data-target="#modalExActivity">Add Extra-Curricular Activity</a></p>


      </div>
    </div>
  </div>

  <div class="panel panel-experience">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#experience_accordion" href="#experience4" data-tid="addReference" aria-expanded="false" aria-controls="experience4">
          Reference(s)
        </a><?php        
          if (count($member->cvform["experience"]["reference"]) == 0) {
            $selected = ($member->cvform["application"]["status"] == 'new') ? "" : 'checked="checked"';
          }
          if ($member->cvform["require_data"]["reference"] == '') {
            ?><label for="na_modalReference" class="pull-right <?=count($member->cvform["experience"]["reference"]) == 0 ? "" : "hide"?>"><input type="checkbox" id="na_modalReference" data-remove-req-row="modalReference_list" <?=$selected?>/> N/A </label><?php
          }
        ?></h4>
    </div>
    <div id="experience4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
         <ul class="list-group  <?=$member->cvform["require_data"]["modalReference_list"]?>" id="modalReference_list"><?php
          foreach ($member->cvform["experience"]["reference"] as $iq) {
            $q = json_decode($iq["content"], true);
        ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>' data-id="<?=$iq["id"]?>">
            <div><?=$q["name"]?> (<?=$q["position"]?>) </div>
            <div>Organization: <?=$q["organization"]?> </div>
            <div>Contact: <?=$q["contact"]?> Email: <?=$q["email"]?></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>
        </ul>
          <p><a href="#" data-tid="addReference"  data-toggle="modal" data-target="#modalReference">Add Work Reference</a></p>
      </div>
    </div>
  </div>
  
</div>

<div class="row rowbtn">
    <div class="hidden-xs col-xs-6"><a  class="btn " data-active="qualification"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right hidden-xs col-xs-6"><a  class="btn " data-active="achievements">Next <i class="fa fa-caret-right"></i></a></div>

    <div class="visible-xs col-xs-6"><a  data-active="qualification" style="white-space: nowrap"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right visible-xs col-xs-6"><a data-active="achievements" style="white-space: nowrap">Next <i class="fa fa-caret-right"></i></a></div>
</div>

<p>&nbsp;</p>
<div class="modal fade" tabindex="-1" role="dialog" id="modalExperience" aria-labelledby="lblModalExperience">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalExperience">Add Work Experience</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-4">
            <label for="frmexp_fromdate">From: </label>
            <input type="text" name="fromdate" id="frmexp_fromdate" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmexp_todate">To: </label>
            <input type="text" name="todate" id="frmexp_todate" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmexp_topresent"><input type="checkbox" name="topresent" id="frmexp_topresent" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-8">
            <label for="frmexp_organization">Name of Organization: </label>
            <input type="text" name="organization" spellcheck="true" id="frmexp_organization" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label class="w-xs-100">Type of Employment: </label>
            <label for="frmexp_employment_fulltime"><input type="radio" name="employment" id="frmexp_employment_fulltime" value="Full-time" checked="checked" /> Full-time</label>
            <label for="frmexp_employment_parttime"><input type="radio" name="employment" id="frmexp_employment_parttime" value="Part-time" /> Part-time </label>
            <label for="frmexp_employment_temporary"><input type="radio" name="employment" id="frmexp_employment_temporary" value="Temporary" /> Temporary </label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexp_position">Position Held: </label>
            <input type="text" name="position" spellcheck="true" id="frmexp_position" maxlength="200" placeholder="(Maximum 200 characters)"  class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexp_duties">Major Duties: </label>
            <textarea  name="duties" id="frmexp_duties" spellcheck="true" maxlength="1000" placeholder="Check out aside for the writing tips"  class="reqModal form-control"></textarea> 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalExperience" data-validate="chkModal('modalExperience')"  >Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalOthExperience" aria-labelledby="lblModalOthExperience">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalOthExperience">Add Internship Experience</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-4">
            <label for="frmoexp_fromdate">From: </label>
            <input type="text" name="fromdate" id="frmoexp_fromdate" placeholder="Click to choose" autocomplete="off" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmoexp_todate">To: </label>
            <input type="text" name="todate" id="frmoexp_todate" placeholder="Click to choose" autocomplete="off" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmoexp_topresent"><input type="checkbox" name="topresent" id="frmoexp_topresent" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-8">
            <label for="frmoexp_organization">Name of Organization: </label>
            <input type="text" name="organization" spellcheck="true" id="frmoexp_organization" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)" />
          </div>
          <div class="form-group col-md-4">
            <label class="w-xs-100">Type of Employment: </label>
            <label for="frmoexp_employment_fulltime"><input type="radio" name="employment" id="frmoexp_employment_fulltime" value="Full-time" checked="checked" /> Full-time</label>
            <label for="frmoexp_employment_parttime"><input type="radio" name="employment" id="frmoexp_employment_parttime" value="Part-time" /> Part-time </label>
            <label for="frmexp_employment_temporary"><input type="radio" name="employment" id="frmexp_employment_temporary" value="Temporary" /> Temporary </label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmoexp_position">Position Held: </label>
            <input type="text" name="position" spellcheck="true" id="frmoexp_position" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmoexp_duties">Major Duties: </label>
            <textarea  name="duties" id="frmoexp_duties" spellcheck="true" class="reqModal form-control" maxlength="1000" placeholder="Check out aside for the writing tips" ></textarea> 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalOthExperience" data-validate="chkModal('modalOthExperience')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalReference" aria-labelledby="lblModalReference">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalReference">Add Work Referee</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-12">
            <label for="frmref_name">Name of Referee: </label>
            <input type="text" name="name" id="frmref_name" spellcheck="true" maxlength="200" placeholder="(Maximum 200 characters)" class="form-control reqModal"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmref_position">Position : </label>
            <input type="text" name="position" id="frmref_position" spellcheck="true" maxlength="200" placeholder="(Maximum 200 characters)" class="form-control reqModal"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmref_organization">Name of Organization: </label>
            <input type="text"  name="organization" id="frmref_organization" spellcheck="true" maxlength="200" placeholder="(Maximum 200 characters)" class="form-control reqModal" /> 
          </div>
          <div class="form-group col-md-6">
            <label for="frmref_contact">Contact No: </label>
            <input type="text" name="contact" id="frmref_contact" maxlength="50"  class="form-control phone reqModal"/>
          </div>
         
          <div class="form-group col-md-6">
            <label for="frmref_email">Email: </label>
            <input type="text" name="email" id="frmref_email" maxlength="100" placeholder="(Maximum 200 characters)" class="form-control email reqModal"/>
          </div>

          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalReference" data-validate="chkModal('modalReference')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalExperience_edit" aria-labelledby="lblModalExperience">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalExperience">Edit Work Experience</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-4">
            <label for="frmexp_fromdate">From: </label>
            <input type="text" name="fromdate" id="frmexp_fromdate_edit" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmexp_todate">To: </label>
            <input type="text" name="todate" id="frmexp_todate_edit" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmexp_topresent"><input type="checkbox" name="topresent" id="frmexp_topresent_edit" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-8">
            <label for="frmexp_organization">Name of Organization: </label>
            <input type="text" name="organization" id="frmexp_organization_edit" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label >Type of Employment </label>
            <label for="frmexp_employment_fulltime"><input type="radio" name="employment" id="frmexp_employment_fulltime" value="Full-time" checked="checked" /> Full-time</label>
            <label for="frmexp_employment_parttime"><input type="radio" name="employment" id="frmexp_employment_parttime" value="Part-time" /> Part-time </label>
            <label for="frmexp_employment_temporary"><input type="radio" name="employment" id="frmexp_employment_temporary" value="Temporary" /> Temporary </label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexp_position">Position Held: </label>
            <input type="text" name="position" id="frmexp_position_edit" spellcheck="true" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexp_duties">Major Duties: </label>
            <textarea  name="duties" id="frmexp_duties_edit" spellcheck="true" maxlength="1000" placeholder="Check out aside for the writing tips" class="reqModal form-control"></textarea> 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalExperience" data-validate="chkModal('modalExperience_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalOthExperience_edit" aria-labelledby="lblModalOthExperience">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalOthExperience">Edit Internship Experience</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-4">
            <label for="frmoexp_fromdate">From: </label>
            <input type="text" name="fromdate" id="frmoexp_fromdate_edit" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmoexp_todate">To: </label>
            <input type="text" name="todate" id="frmoexp_todate_edit" autocomplete="off" placeholder="Click to choose"  class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmoexp_topresent"><input type="checkbox" name="topresent" id="frmoexp_topresent_edit" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-8">
            <label for="frmoexp_organization">Name of Organization: </label>
            <input type="text" name="organization" id="frmoexp_organization_edit" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label >Type of Employment </label>
            <label for="frmoexp_employment_fulltime"><input type="radio" name="employment" id="frmoexp_employment_fulltime" value="Full-time" /> Full-time</label>
            <label for="frmoexp_employment_parttime"><input type="radio" name="employment" id="frmoexp_employment_parttime" value="Part-time" /> Part-time </label>
            <label for="frmexp_employment_temporary"><input type="radio" name="employment" id="frmexp_employment_temporary" value="Temporary" /> Temporary </label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmoexp_position">Position Held: </label>
            <input type="text" name="position" id="frmoexp_position_edit" maxlength="200" placeholder="(Maximum 200 characters)"  class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmoexp_duties">Major Duties: </label>
            <textarea  name="duties" id="frmoexp_duties_edit" class="reqModal form-control" maxlength="1000" placeholder="Check out aside for the writing tips"></textarea> 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalOthExperience" data-validate="chkModal('modalOthExperience_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalExActivity" aria-labelledby="lblModalExActivity">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalExActivity">Add Extra-Curricular Activity</h4>
      </div>
      <div class="modal-body">
        <form class="form">
            <div class="form-group col-md-4">
            <label for="frmexact_fromdate">From: </label>
            <input type="text" name="fromdate" id="frmexact_fromdate" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmexact_todate">To: </label>
            <input type="text" name="todate" id="frmexact_todate" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmexact_topresent"><input type="checkbox" name="topresent" id="frmexact_topresent" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexact_organization">Name of Organization: </label>
            <input type="text" name="organization" id="frmexact_organization" spellcheck="true" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexact_position">Position Held: </label>
            <input type="text" name="position" id="frmexact_position" spellcheck="true" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexact_duties">Major Duties: </label>
            <textarea  name="duties" id="frmexact_duties" spellcheck="true" class="reqModal form-control" maxlength="1000" placeholder="Check out aside for the writing tips"></textarea> 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalExActivity" data-validate="chkModal('modalExActivity')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalExActivity_edit" aria-labelledby="lblModalExActivity">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalExActivity">Edit Extra-Curricular Activity</h4>
      </div>
      <div class="modal-body">
        <form class="form">
            <div class="form-group col-md-4">
            <label for="frmexact_fromdate">From: </label>
            <input type="text" name="fromdate" id="frmexact_fromdate_edit" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmexact_todate">To: </label>
            <input type="text" name="todate" id="frmexact_todate_edit" autocomplete="off" placeholder="Click to choose" class="reqModal monthpicker form-control"/>
          </div>
          <div class="form-group col-md-4"><BR />
            <label for="frmexact_topresent"><input type="checkbox" name="topresent" id="frmexact_topresent_edit" value="1"/> Present</label>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexact_organization">Name of Organization: </label>
            <input type="text" name="organization" id="frmexact_organization_edit" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexact_position">Position Held: </label>
            <input type="text" name="position" id="frmexact_position_edit" maxlength="200" placeholder="(Maximum 200 characters)" class="reqModal form-control"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmexact_duties">Major Duties: </label>
            <textarea  name="duties" id="frmexact_duties_edit" class="form-control reqModal" maxlength="1000" placeholder="Check out aside for the writing tips"></textarea> 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalExActivity" data-validate="chkModal('modalExActivity_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalReference_edit" aria-labelledby="lblModalReference">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalReference">Edit Work Referee</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-12">
            <label for="frmref_name_edit">Name of Referee: </label>
            <input type="text" name="name" id="frmref_name_edit" maxlength="200" placeholder="(Maximum 200 characters)" class="form-control"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmref_position_edit">Position : </label>
            <input type="text" name="position" id="frmref_position_edit" maxlength="200" placeholder="(Maximum 200 characters)" class="form-control"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmref_organization_edit">Name of Organization: </label>
            <input type="text"  name="organization" id="frmref_organization_edit" maxlength="200" placeholder="(Maximum 200 characters)" class="form-control" /> 
          </div>
          <div class="form-group col-md-6">
            <label for="frmref_contact_edit">Contact No: </label>
            <input type="text" name="contact" id="frmref_contact_edit" maxlength="20" class="form-control phone"/>
          </div>
         
          <div class="form-group col-md-6">
            <label for="frmref_email_edit">Email: </label>
            <input type="text" name="email" id="frmref_email_edit" maxlength="100" placeholder="(Maximum 100 characters)" class="form-control email"/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalReference" data-validate="chkModal('modalReference_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="hide">
  <ul id="modalExperience_template">
    <li class="list-group-item" data-content='[content]' data-id='[id]'>
      <div><span class="education-date">[fromdate] - [todate]</span>[position]</div>
      <div>Organization: [organization] Employment: [employment]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
  <ul id="modalOthExperience_template">
    <li class="list-group-item" data-content='[content]' data-id='[id]'>
      <div><span class="education-date">[fromdate] - [todate]</span>[position]</div>
      <div>Organization: [organization] Employment: [employment]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
  <ul id="modalReference_template">
    <li class="list-group-item" data-content='[content]' data-id='[id]'>
      <div>[name] ([position]) </div>
      <div>Organization: [organization]</div>
      <div>Contact: [contact] Email: [email]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
</div>
