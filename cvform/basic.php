<h2><span class="fa fa-user"></span> Personal Particulars</h2>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-basic">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a role="button"  data-parent="#accordion" aria-expanded="true" aria-controls="personal1">
          Mandatory Field
        </a>
        <a class="pull-right"  data-parent="#accordion" aria-expanded="false" aria-controls="personal1">
          
        </a>
      </h4>
    </div>
    <div id="personal1" aria-labelledby="headingOne">
      <div class="panel-body">
          <div class="form-group row">
            <div class="col-md-6">
              <label for="student_no">Student Number: </label>
              <div><?=$member->cvform["personal"]["student_id"]?></div></div>
            <div class="col-md-6">
              <label for="programme_study">Programme of Study: </label>
              <div><?=$member->cvform["personal"]["program"]["code"] ?></div></div>
            </div>
            <div class="form-group row">
            <div class="col-md-6"><label for="programme_title">Programme Title: </label>
                                  <div><?=$member->cvform["personal"]["program"]["name"]?></div></div>
            <div class="col-md-6"><label for="study_year">Study Year: </label>
                                  <div>Year <?=$member->cvform["personal"]["yrofstudy"]?></div></div>
            </div><div class="form-group row">
            <div class="col-md-6"><label for="c_email">College Email: </label>
                                            <div><?=$member->cvform["personal"]["c_email"]?></div></div>
          </div>
      </div>
    </div>
  </div>
  
  <div class="panel panel-basic">
    <div class="panel-heading" role="tab" >

      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#personal2" aria-expanded="false" aria-controls="personal2">
          Personal Particulars
        </a>
        <a class="pull-right" data-toggle="collapse" data-parent="#accordion" href="#personal2" aria-expanded="false" aria-controls="personal2">
          
        </a>
      </h4>
    </div>
    <div id="personal2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
          <div class="row">
          <div class="form-group col-md-3">
            <label for="firstname_en" >First Name (English): </label>
            <input type="text" name="basic[firstname_en]" id="firstname_en" data-tid="firstname_en" placeholder="(e.g.: Tai Man, Andrew)" class="<?=$member->cvform["require_data"]["firstname_en"]?> form-control" value="<?=$member->cvform["personal"]["en_name"]?>"/>
          </div>
          <div class="form-group col-md-3">
            <label for="lastname_en">Last Name (English): </label>
            <input type="text" name="basic[lastname_en]" id="lastname_en" data-tid="lastname_en" placeholder="(e.g.: Chan)" class="<?=$member->cvform["require_data"]["lastname_en"]?> form-control" value="<?=$member->cvform["personal"]["en_lastname"]?>"/>
          </div>
          <div class="form-group col-md-3">
            <label for="firstname_zh">First Name (Chinese): </label>
            <input type="text" name="basic[firstname_zh]" id="firstname_zh" data-tid="firstname_zh" placeholder="(e.g.: 大文)" class="<?=$member->cvform["require_data"]["firstname_zh"]?> form-control" value="<?=$member->cvform["personal"]["ch_name"]?>"/>
          </div>
          <div class="form-group col-md-3">
            <label for="lastname_zh">Last Name (Chinese): </label>
            <input type="text" name="basic[lastname_zh]" id="lastname_zh" data-tid="lastname_zh" placeholder="(e.g.: 陳)" class="<?=$member->cvform["require_data"]["lastname_zh"]?> form-control" value="<?=$member->cvform["personal"]["ch_lastname"]?>"/>
          </div>
          <div class="form-group col-md-6">
            <label for="email">Personal Email: </label>
            <input type="text" name="basic[email]" id="email" class="<?=$member->cvform["require_data"]["email"]?> email form-control" placeholder="(e.g. : 12345678A@student.cpce.edu.hk)"  data-tid="email" value="<?=$member->cvform["personal"]["p_email"]?>"/>
          </div>
          <div class="form-group col-md-6">
            <label for="contact">Contact Number: </label>
            <input type="text" name="basic[contact]" id="contact"  maxlength="8" class="<?=$member->cvform["require_data"]["contact"]?> phone number form-control" placeholder="(e.g. : 12345678)" data-tid="contact" value="<?=$member->cvform["personal"]["contact"]?>"/>
          </div>
          <div class="form-group col-md-12">
            <label for="address1">Address: </label>
            <input type="text" name="basic[address1]" id="address1" class="<?=$member->cvform["require_data"]["address1"]?> form-control" data-tid="address1" value="<?=$member->cvform["personal"]["address1"]?>"/>
            <input type="text" name="basic[address2]" id="address2" class="<?=$member->cvform["require_data"]["address2"]?> form-control" data-tid="address2" value="<?=$member->cvform["personal"]["address2"]?>"/>
            <input type="text" name="basic[address3]" id="address3" class="<?=$member->cvform["require_data"]["address3"]?> form-control" data-tid="address3" value="<?=$member->cvform["personal"]["address3"]?>"/>
          </div>
          <div class="form-group col-md-4">
            <label>Gender: </label>
            <div class="row">
              <label for="gender_M" class="col-xs-6">
                <input type="radio" name="basic[gender]" id="gender_M" value="M" data-tid="gender" class="<?=$member->cvform["require_data"]["gender"]?>" <?=($member->cvform["personal"]["gender"] == "M") ?  'checked="checked"' : ''?>/> Male 
              </label>
              <label for="gender_F" class="col-xs-6">
                <input type="radio" name="basic[gender]" id="gender_F" value="F" data-tid="gender" class="<?=$member->cvform["require_data"]["gender"]?>" <?=($member->cvform["personal"]["gender"] == "F") ? 'checked="checked"' : ""?>/> Female 
              </label>
            </div>
          </div>
           <div class="form-group col-md-2">
            <label for="age">Age: </label>
            <input type="text" name="basic[age]" id="age" class="<?=$member->cvform["require_data"]["age"]?> form-control number" data-tid="age" value="<?=$member->cvform["personal"]["age"]?>"/>
          </div>
          <div class="form-group col-md-3">
            <label for="hkid">HKID No.: </label>
            <input type="text" name="basic[hkid]" id="hkid" placeholder="(E.g. : A123456(7))" class="<?=$member->cvform["require_data"]["hkid"]?> form-control"  data-tid="hkid" value="<?=$member->cvform["personal"]["hkid"]?>"/>
          </div>
           <div class="form-group col-md-3">
            <label for="dob">Date of Birth: </label>
            <input type="text" name="basic[dob]" id="dob" placeholder="Click to choose" class="<?=$member->cvform["require_data"]["dob"]?> datepicker form-control " data-tid="dob" value="<?=$member->cvform["personal"]["dob"]?>"/>
          </div>
          <?php 
           
              ?><div class="form-group col-md-6">
            <label for="district">Home District: </label>
            <select id="district" name="basic[district]" data-tid="district" placeholder="Click to choose" class="<?=$member->cvform["require_data"]["district"]?> select2 district col-md-12">
            <?php
              if ($member->cvform["personal"]["district"]["id"] != "") {
            ?><option selected="selected" value="<?=$member->cvform["personal"]["district"]["id"]?>"><?=$member->cvform["personal"]["district"]["name"]?></option><?php
              }
            ?>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label for="work_district">Preferred Work Location: </label>
            <select id="work_district" name="basic[work_district]" data-tid="work_district" placeholder="Click to choose"  multiple="multiple" class="<?=$member->cvform["require_data"]["work_district"]?> select2 district col-md-12">
            <?php
              foreach ($member->cvform["personal"]["work_district"] as $work_district){
                ?><option selected="selected" value="<?=$work_district["id"]?>"><?=$work_district["name"]?></option><?
              }
            ?>
            </select>
          </div>
           
          <div class="form-group col-md-12">
            <label for="religion">Religion: </label>
            <input type="text" name="basic[religion]" id="religion" data-tid="religion" spellcheck="true" placeholder="(e.g. Buddhism)" class="form-control" value="<?=$member->cvform["personal"]["religion"]?>"/>
          </div>
          <?php
          if ($member->isWIE()) {
          ?>
          <div class="form-group col-md-12">
            <label>Emergency Contact:</label>
          </div>
          <div class="form-group col-md-6">
            <label class="">First Name</label>
            <input type="text" name="basic[em_name]" id="em_name" data-tid="em_name" maxlength="100" placeholder="(e.g. Siu Ming)" class="form-control <?=$member->cvform["require_data"]["em_name"]?>" value="<?=$member->cvform["personal"]["em_name"]?>"/>
          </div>
          <div class="form-group col-md-6">
            <label class="">Last Name</label>
            <input type="text" name="basic[em_name2]" id="em_name2" data-tid="em_name2" maxlength="100" placeholder="(e.g. Wong)" class="form-control  <?=$member->cvform["require_data"]["em_name2"]?>" value="<?=$member->cvform["personal"]["em_name2"]?>"/>
          </div><div class="form-group col-md-4">
            <label class="">Relationship</label>
            <input type="text" name="basic[em_relation]" id="em_relation" spellcheck="true" maxlength="100" placeholder="(e.g. Father)"  data-tid="em_relation" class="form-control  <?=$member->cvform["require_data"]["em_relation"]?>" value="<?=$member->cvform["personal"]["em_relation"]?>"/>
          </div><div class="form-group col-md-4">
            <label class="">Phone no. (Home)</label>
            <input type="text" name="basic[em_phone]" maxlength="8" id="em_phone" data-tid="em_phone" placeholder="(e.g. 12345678)" class="phone number form-control  <?=$member->cvform["require_data"]["em_phone"]?>" value="<?=$member->cvform["personal"]["em_phone"]?>"/>
          </div><div class="form-group col-md-4">
            <label class="">Contact Number</label>
            <input type="text" name="basic[em_contact]" maxlength="8" id="em_contact" data-tid="em_contact" placeholder="(e.g. 98765432)" class="phone number form-control  <?=$member->cvform["require_data"]["em_contact"]?>" value="<?=$member->cvform["personal"]["em_contact"]?>"/>
          </div><?php
            }
          ?>
           
           <?php /* div class="form-group col-md-12">
            <label class="">Preferred Work Schedule: </label>
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-10"><?php
              for ($i = 1; $i <= 6; $i++) {
                ?><label class="col-sm-1"><?=date("D", strtotime("+".$i." day", strtotime("Sunday"))); ?></label><?
              }
              ?></div><?php
            ?></div>
            <?php
              foreach (["AM", "PM"] as $p) {
                echo '<div class="row"><label class="col-md-2">'.$p.'</label><div class="col-md-10">';
                
                for ($i = 1; $i <= 6; $i++) {
                  ?><label class="col-sm-1"><input type="checkbox" value="<?=date("D", strtotime("+".$i." day", strtotime("Sunday"))); ?>_<?=$p?>" /></label><?
                } 
                echo "</div></div>";
              }
            ?>
          </div */ ?>
        </div>         
      </div>
    </div>

  </div>
   <div class="panel panel-basic">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#interview_accordion" href="#interview2" aria-expanded="false" aria-controls="interview2">
           Photo
        </a>
      </h4>
    </div>
    <div id="interview2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12">
            <label>Personal Photo (max 300KB, .JPG file):</label>
            <div class="ajax-upload row <?=($member->cvform["application"]["photo"]) ? "hide" : ""?>" data-placement="top" title="" data-validfiletype="image/jpeg" >
              <input type="file" name="photo" class="form-control <?=$member->cvform["require_data"]["photo"]?>" value="" id="photo" data-value="<?=$member->cvform["application"]["photo"]?>" accept="image/*"  />
              <div class="col-sm-10">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
              </div>
              <div class="col-sm-1"><button class="btn btn-upload btn-default" data-tid="photo">Upload</button></div>
            </div>
            <div class="link-wrapper row <?=($member->cvform["application"]["photo"]) ? "" : "hide"?>">
              <div class="link col-sm-9"><?php
                $filename = explode("/", $member->cvform["application"]["photo"]);
                $filename = $filename[count($filename) - 1];
                if ($filename != "") {
              ?><a href="<?=$appPath . "/". $member->cvform["application"]["photo"]?>?<?=time()?>" rel="noreferrer noopener" data-tid="photo" target="_blank"  >
                <i class="fa fa-paperclip"></i> <?=$filename?></a><?php
                }
              ?></div>
              <div class="text-right col-sm-3"><a class="btn btn-remove" data-tid="remove-photo" data-remove-attach="photo" ><i class="fa fa-ban"></i> Delete</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   <div class="row rowbtn">
    
    <div class="text-right hidden-xs col-sm-12"><a  class="btn " data-active="education">Next <i class="fa fa-caret-right"></i></a></div>
    <div class="text-right visible-xs col-xs-12"><a  class="" data-active="education">Next <i class="fa fa-caret-right"></i></a></div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalEmergencyContact" aria-labelledby="lblModalEContact">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalEContact">Add Emergency Contact</h4>
      </div>
      <div class="modal-body">
          <form class="form"><div class="form-group col-md-12">
            <label for="frmec_name">Name: </label>
            <input type="text" name="name" id="frmec_name" class="form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmec_relationship">Relationship: </label>
            <input type="text" name="relationship" id="frmec_relationship" class="form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmec_home">Home number: </label>
            <input type="text" name="home" id="frmec_home" class="form-control"/>
          </div>
          <div class="form-group col-md-4">
            <label for="frmec_contact">Contact: </label>
            <input type="text" name="contact" id="frmec_contact" class="form-control"/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalEmergencyContact">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->