<h2><span class="fa fa-wrench"></span> Skill(s)</h2>
<div class="panel-group" id="skills_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-skills">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#skills_accordion" href="#skills1" aria-expanded="false" aria-controls="skills1">
          Language
        </a>
      </h4>
    </div>
    <div id="skills1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        
        <div id="language_list">
        <?php
        $skills = json_decode($member->cvform["achievement"]["skill"][0]["content"], true);


        if (!array_key_exists("language", $skills) || empty($skills["language"])) {
            $skills["language"] = array();
            $skills["language"]["__Chinese"] = array("Written"=>"", "Oral"=>"");
            $skills["language"]["__English"] = array("Written"=>"", "Oral"=>"");
            $skills["language"]["__Putonghua"] = array("Oral"=>"");
        }

        if (!array_key_exists("computer", $skills) || empty($skills["computer"])) {
            $skills["computer"] = array("__Excel"=>"", "__Word"=>"", "__PowerPoint"=>"");
        }


        foreach ($skills["language"] as $language => $skill) {
        ?>
        <div class="language_skill_list">
          <h4><?=ltrim($language, "_")?>
          <?php if (strpos($language, "__") !== 0) { ?><a class="btn btn-delete-language btn-xs" data-language="<?=ltrim($language, "_")?>"> Remove <i class="fa fa-times"></i> </a><?php } ?></h4>
          <?php
            foreach ($skill as $sub=>$grade) {
          ?><div class="row">
            <div class="col-md-5"><label><?=$sub?></label></div>
            <div class="col-md-7">
              <div class="btn-group grade" role="group" aria-label="..." data-language="<?=$language?>" data-sub="<?=$sub?>">
                <button type="button" class="btn btn-achievements <?=($grade == 'Excellent') ? "active" : ""?>" >Excellent</button>
                <button type="button" class="btn btn-achievements <?=($grade == 'Good') ? "active" : ""?>">Good</button>
                <button type="button" class="btn btn-achievements <?=($grade == 'Average') ? "active" : ""?>">Average</button>
                <button type="button" class="btn btn-achievements <?=($grade == 'N/A') ? "active" : ""?>">N/A</button>
              </div>
            </div>
          </div><?php 
            } 
        ?></div><?php
        } 
        ?>
      </div>
          <div class="row">
            <div class="col-md-5">Add Language</div>
            <div class="col-md-4"><input type="text" id="new_language"  data-tid="new_language" value="" placeholder="Type Language here"  class="form-control"/></div>
            <div class="col-md-2"><button id="add_language" class="btn btn-default">Add</button></div>
          </div>
        
      
      </div>
    </div>
  </div>

  <div class="panel panel-skills">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#skills_accordion" href="#skills2" aria-expanded="false" aria-controls="skills2">
          Computer
        </a>
      </h4>
    </div>
    <div id="skills2" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        
        <div id="computer_list">
        <?php
        foreach ($skills["computer"] as $language => $grade) {
        ?>
        <div class="computer_skill_list">
          <div class="row">
            <div class="col-md-5">
              <h4><?=ltrim($language, "_")?><?php if (strpos($language, "__") !== 0) { ?><a class="btn btn-delete-skill btn-xs" data-language="<?=ltrim($language, "_")?>"> Remove <i class="fa fa-times"></i> </a><?php } ?></h4>
            </div>
            <div class="col-md-7">
              <div class="btn-group computer" role="group" aria-label="..." data-computer="<?=$language?>" >
                <button type="button" class="btn btn-achievements <?=($grade == 'Excellent') ? "active" : ""?>" >Excellent</button>
                <button type="button" class="btn btn-achievements <?=($grade == 'Good') ? "active" : ""?>">Good</button>
                <button type="button" class="btn btn-achievements <?=($grade == 'Average') ? "active" : ""?>">Average</button>
                <button type="button" class="btn btn-achievements <?=($grade == 'N/A') ? "active" : ""?>">N/A</button>
              </div>
            </div>
          </div>
        </div><?php
        } 
        ?>
      </div>
          <div class="row">
            <div class="col-md-5">Add Computer Skill</div>
            <div class="col-md-4"><input type="text" id="new_computer" data-tid="new_computer" value="" maxlength="100" placeholder="Type Computer Skill here" class="form-control"/></div>
            <div class="col-md-2"><button id="add_computer" class="btn btn-default">Add</button></div>
          </div>
        
      
      </div>
    </div>
  </div>

  <div class="panel panel-skills">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#skills_accordion" href="#skills3" aria-expanded="false" aria-controls="skills3">
          Others
        </a>
      </h4>
    </div>
    <div id="skills3" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <div >
          <ul id="modalOtherSkill_list" class="list-group"><?php
          $skillOthers = array();

           foreach ($member->cvform["achievement"]["skill_other"] as $skillOther) {

            $skillOther["skill"] = json_decode($skillOther["content"], true);
       
            ?><li class="list-group-item" data-content="" data-id='<?=$skillOther["id"]?>'>
              <div>Skill: <?=$skillOther["skill"]["skill"]?> </div>
              <div class="addon">
                <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
                <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
              </div> 
            </li><?

            $skillOthers[$skillOther["id"]] = array("skill"=>$skillOther["skill"]["skill"], "description"=>html_entity_decode($skillOther["skill"]["description"], ENT_QUOTES, "UTF-8"));
           }


          ?></ul>
        </div>
        <p><a data-tid="addOtherSkill" data-toggle="modal" data-target="#modalOtherSkill">Add Other Skill</a></p>
      </div>
    </div>
  </div>


  <div class="row rowbtn">
    <div class="hidden-xs col-sm-6"><a  class="btn " data-active="achievements"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right hidden-xs col-sm-6"><a  class="btn " data-active="other">Next <i class="fa fa-caret-right"></i></a></div>
    <div class="visible-xs col-xs-7"><a data-active="achievements"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="text-right visible-xs col-xs-5"><a data-active="other"> Next <i class="fa fa-caret-right"></i></a></div>
  </div>
</div>  


<div class="modal fade" tabindex="-1" role="dialog" id="modalOtherSkill" aria-labelledby="lblOtherSkill">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblOtherSkill">Add Other Skill</h4>
      </div>
      <div class="modal-body">
        <form class="form row">
          <div class="form-group col-md-12">
            <label for="frmskill_name">Skill Name: </label>
            <input type="text" name="skill" id="frmskill_name" class="form-control" maxlength="200" placeholder="(Maximum 200 characters)" />
          </div>
          <div class="form-group col-md-12">
            <label for="frmskill_description">Description: </label>
            <textarea  name="description" id="frmskill_description" class="ckeditor" ></textarea> 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalOtherSkill">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalOtherSkill_edit" aria-labelledby="lblOtherSkill_edit">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblOtherSkill_edit">Edit Other Skill</h4>
      </div>
      <div class="modal-body">
          <form class="form row">
            <div class="form-group col-md-12">
              <label for="frmskill_name">Skill Name: </label>
              <input type="text" name="skill" id="frmskill_name" maxlength="200" placeholder="(Maximum 200 characters)" class="form-control"/>
            </div>
            <div class="form-group col-md-12">
              <label for="frmskill_description_edit">Description: </label>
              <textarea name="description" id="frmskill_description_edit" class="ckeditor" ></textarea>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalOtherSkill">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="hide">
  <ul id="modalOtherSkill_template">
    <li class="list-group-item" data-content='[content]' data-id='[id]'>
      <div>Skill: [skill] </div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
</div>
<script>
  var skill_language = <?=json_encode($skills["language"])?>;
  var skill_computer = <?=json_encode($skills["computer"])?>;
  var skill_other = <?=json_encode($skillOthers)?>;
</script>