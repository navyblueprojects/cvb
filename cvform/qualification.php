<h2><span class="fa fa-award"></span> Qualifications</h2>
<div class="panel-group" id="qualification_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-qualification">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#qualification_accordion" href="#qualification1" aria-expanded="false" aria-controls="qualification1">
          Professional Qualification(s)
        </a>
        <?php
          if (count($member->cvform["qualification"]["professional"]) == 0) {
            $selected = ($member->cvform["application"]["status"] == 'new') ? "" : 'checked="checked"';
          }
        ?><label for="na_modalProfQuali" class="pull-right <?=count($member->cvform["qualification"]["professional"]) == 0 ? "" : "hide"?>"><input type="checkbox" id="na_modalProfQuali" data-remove-req-row="modalProfQuali_list" <?=$selected?>/> N/A </label>
      </h4>
    </div>
    <div id="qualification1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <div>
          <ul class="<?=($selected == "") ? $member->cvform["require_data"]["modalProfQuali_list"] : ""?> list-group" id="modalProfQuali_list"><?php
          foreach ($member->cvform["qualification"]["professional"] as $iq) {
            $q = json_decode($iq["content"], true);
        ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>' data-id='<?=$iq["id"]?>' >
            <div class="ellipsis"><span class="education-date"><?=$q["date"]?></span><?=$q["name"]?></div>
            <div class="ellipsis"><span><label>Grade: </label> <?=$q["grade"]?></span>
                <span><label>Organization: </label> <?=$q["organization"]?></span></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>
        </ul>
          <p><a href="#" data-tid="addProfQuali" data-toggle="modal" data-target="#modalProfQuali">Add Professional Qualification</a></p>
        </div>
        
      </div>
    </div>

  </div>
  <div class="panel panel-qualification">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#qualification_accordion" href="#qualification1" aria-expanded="false" aria-controls="qualification1">
          Other Qualification(s)
        </a>
        <?php
          if (count($member->cvform["qualification"]["other"]) == 0) {
            $selected = ($member->cvform["application"]["status"] == 'new') ? "" : 'checked="checked"';
          }
        ?><label for="na_modalOthQuali" class="pull-right <?=count($member->cvform["qualification"]["other"]) == 0 ? "" : "hide"?>"><input type="checkbox" id="na_modalOthQuali" data-remove-req-row="modalOthQuali_list" <?=$selected?>/> N/A </label>
      </h4>
    </div>
    <div id="qualification2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <div>
           <ul class="<?=($selected == "") ? $member->cvform["require_data"]["modalOthQuali_list"] : ""?> list-group" id="modalOthQuali_list"><?php
          foreach ($member->cvform["qualification"]["other"] as $iq) {
            $q = json_decode($iq["content"], true);
        ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"]) ?>' data-id='<?=$iq["id"]?>' >
           
            <div class="ellipsis"><span class="education-date"><?=$q["date"]?></span><?=$q["name"]?></div>
            <div class="ellipsis"><span ><label>Grade:</label> <?=$q["grade"]?></span>
                 <span><label>Organization:</label> <?=$q["organization"]?></span></div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>
        </ul>
           <p><a href="#" data-tid="addOthQuali"  data-toggle="modal" data-target="#modalOthQuali">Add Other Qualification</a></p>
        </div>
      </div>
    </div>
    
  </div>
  <div class="row rowbtn">
      <div class="hidden-xs col-sm-6"><a  class="btn " data-active="education"><i class="fa fa-caret-left"></i> Previous</a></div>
      <div class="hidden-xs text-right col-sm-6"><a  class="btn " data-active="experience">Next <i class="fa fa-caret-right"></i></a></div>
       <div class="visible-xs col-xs-5"><a  data-active="education"  style="white-space: nowrap"><i class="fa fa-caret-left"></i> Previous</a></div>
      <div class="text-right visible-xs col-xs-7"><a data-active="experience"  style="white-space: nowrap"> Next <i class="fa fa-caret-right"></i></a></div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalProfQuali_edit" aria-labelledby="lblModalProfQuali">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalProfQuali">Edit Professional Qualification</h4>
      </div>
      <div class="modal-body">
          <form class="form">
          <div class="row"><div class="form-group col-md-12">
            <label for="frmpq_name_edit">Examination / Certification: </label>
            <input type="text" name="name" id="frmpq_name_edit" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)" />
          </div>
          <div class="form-group col-md-12">
            <label for="frmpq_organization_edit">Name of Issuing Organization: </label>
            <input type="text" name="organization" id="frmpq_organization_edit" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmpq_date_edit">Date issued: </label>
            <input type="text" name="date" id="frmpq_date_edit"  class="reqModal monthpicker form-control" placeholder="Click to choose" />
          </div>
          <div class="form-group col-md-6">
            <label for="frmpq_grade_edit">Grade/Level Obtained: </label>
            <input type="text" name="grade" id="frmpq_grade_edit" class="reqModal form-control" maxlength="100" placeholder="(E.g. Merit)"/>
          </div></div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalProfQuali" data-validate="chkModal('modalProfQuali_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalOthQuali_edit" aria-labelledby="lblModalOthQuali">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalOthQuali">Edit Other Qualification</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="row">
            <div class="form-group col-md-12">
              <label for="frmoq_name_edit">Examination/Certification: </label>
              <input type="text" name="name" id="frmoq_name_edit" class="reqModal form-control"  maxlength="200" placeholder="(Maximum 200 characters)"/>
            </div>
            <div class="form-group col-md-12">
              <label for="frmoq_organization_edit">Name of Issuing Organization: </label>
              <input type="text" name="organization" id="frmoq_organization_edit" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)"/>
            </div>
            <div class="form-group col-md-6">
              <label for="frmoq_date_edit">Date issued: </label>
              <input type="text" name="date" id="frmoq_date_edit"  class="monthpicker form-control" placeholder="Click to choose"/>
            </div>
            <div class="form-group col-md-6">
              <label for="frmoq_grade_edit">Grade/Level Obtained: </label>
              <input type="text" name="grade" id="frmoq_grade_edit" class="reqModal form-control" placeholder="(E.g. Merit)"/>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalOthQuali" data-validate="chkModal('modalOthQuali_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalProfQuali" aria-labelledby="lblModalProfQuali">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalProfQuali">Add Professional Qualification</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="row">
              <div class="form-group col-md-12">
            <label for="frmpq_name">Examination/Certification: </label>
            <input type="text" name="name" id="frmpq_name" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmpq_organization">Name of Issuing Organization: </label>
            <input type="text" name="organization" id="frmpq_organization" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmpq_date">Date issued: </label>
            <input type="text" name="date" id="frmpq_date"  class="monthpicker reqModal form-control"  placeholder="Click to choose"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmpq_grade">Grade/Level Obtained: </label>
            <input type="text" name="grade" id="frmpq_grade" class="reqModal form-control" placeholder="(E.g. Merit)"/>
          </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalProfQuali" data-validate="chkModal('modalProfQuali')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalOthQuali" aria-labelledby="lblModalOthQuali">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalOthQuali">Add Other Qualification</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="row">
            <div class="form-group col-md-12">
            <label for="frmoq_name">Examination/Certification: </label>
            <input type="text" name="name" id="frmoq_name" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group col-md-12">
            <label for="frmoq_organization">Name of Issuing Organization: </label>
            <input type="text" name="organization" id="frmoq_organization" class="reqModal form-control" maxlength="200" placeholder="(Maximum 200 characters)"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmoq_date">Date issued: </label>
            <input type="text" name="date" id="frmoq_date" class="reqModal monthpicker form-control" placeholder="Click to choose"/>
          </div>
          <div class="form-group col-md-6">
            <label for="frmoq_grade">Grade/Level Obtained: </label>
            <input type="text" name="grade" id="frmoq_grade" class="reqModal form-control" placeholder="(E.g. Merit)"/>
          </div></div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalOthQuali" data-validate="chkModal('modalOthQuali')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="hide">
<ul id="modalProfQuali_template">
    <li class="list-group-item" data-content="[content]" data-id="[id]">
      
      <div class="ellipsis"><span class="education-date">[date]</span>[name]</div>
      <div class="ellipsis"><span><label>Grade:</label> [grade]</span>
          <span><label>Organization:</label> [organization]</span></div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
  <ul id="modalOthQuali_template">
    <li class="list-group-item" data-content="[content]" data-id="[id]">
      
      <div class="ellipsis"><span class="education-date">[date]</span>[name]</div>
      <div class="ellipsis"><span><label>Grade:</label> [grade]</span>
      <span><label>Organization:</label> [organization]</span></div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
</div>