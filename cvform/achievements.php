<h2><span class="fa fa-trophy"></span> Achievement(s)</h2>
<div class="panel-group" id="achievements_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-achievements">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#achievements_accordion" href="#achievements1" aria-expanded="true" aria-controls="achievements1">
          Achievements
        </a>
        <?php if ($member->cvform["require_data"]["modalAchievement_list"] == '') {
          ?><label for="na_modalAchievement" class="pull-right <?=count($member->cvform["achievement"]["achievement"]) == 0 ? "" : "hide"?>"><input type="checkbox" id="na_modalAchievement" data-remove-req-row="modalAchievement_list"/> N/A </label><?php
        }
        ?>
      </h4>
    </div>
    <div id="achievements1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <ul class="list-group <?=$member->cvform["require_data"]["modalAchievement_list"]?>" id="modalAchievement_list"><?php
          foreach ($member->cvform["achievement"]["achievement"] as $iq) {
            $q = json_decode($iq["content"], true);
        ?><li class="list-group-item" data-content='<?=str_replace("'", "&#39;", $iq["content"])?>' data-id='<?=$iq["id"]?>'>
            <div><span class="education-date"><?=$q["date"]?> </span><?=$q["name"]?></div>
            <div>Organization: <?=$q["organization"]?> </div>
            <div class="addon">
              <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
              <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
            </div> 
          </li><?
          }
        ?>    
        </ul>
        <p><a href="#" data-tid="addAchievement" data-toggle="modal" data-target="#modalAchievement">Add Achievement</a></p>
      </div>
    </div>
  </div>
  <div class="row rowbtn">
    <div class="hidden-xs col-sm-6"><a  class="btn " data-active="experience"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="hidden-xs text-right col-sm-6"><a  class="btn " data-active="skills">Next <i class="fa fa-caret-right"></i></a></div>
    <div class="visible-xs col-xs-7"><a  data-active="experience"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="visible-xs text-right col-xs-5"><a  data-active="skills">Next <i class="fa fa-caret-right"></i></a></div>
  </div>
</div>  

<div class="modal fade" tabindex="-1" role="dialog" id="modalAchievement" aria-labelledby="lblModalAchievement">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalAchievement">Add Achievement</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-12">
            <label for="frmachv_name">Name of Scholarship/Awards: </label>
            <input type="text" name="name" id="frmachv_name" class="reqModal form-control" maxlength="200" placeholder="(Max 200 characters)" spellcheck="true" />
          </div>
          <div class="form-group col-md-6">
            <label for="frmachv_organization">Name of Issuing Organization: </label>
            <input type="text"  name="organization" id="frmachv_organization" class="form-control reqModal" maxlength="200" placeholder="(Max 200 characters)" spellcheck="true" /> 
          </div>
          <div class="form-group col-md-6">
            <label for="frmachv_date">Date obtained: </label>
            <input type="text" name="date" id="frmachv_date" autocomplete="off" spellcheck="true" placeholder="Click to choose" class="reqModal monthpicker form-control "/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-save="#modalAchievement" data-validate="chkModal('modalAchievement')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<div class="modal fade" tabindex="-1" role="dialog" id="modalAchievement_edit" aria-labelledby="lblModalAchievement">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lblModalAchievement">Edit Achievement</h4>
      </div>
      <div class="modal-body">
          <form class="form">
            <div class="form-group col-md-12">
            <label for="frmachv_name">Name of Scholarship/Awards: </label>
            <input type="text" name="name" id="frmachv_name_edit" class="form-control reqModal" maxlength="200" placeholder="(Max 200 characters)" spellcheck="true" />
          </div>
          <div class="form-group col-md-6">
            <label for="frmachv_organization">Name of Issuing Organization: </label>
            <input type="text"  name="organization" id="frmachv_organization_edit" class="form-control reqModal" maxlength="200" placeholder="(Max 200 characters)" spellcheck="true" /> 
          </div>
          <div class="form-group col-md-6">
            <label for="frmachv_date">Date obtained: </label>
            <input type="text" name="date" id="frmachv_date_edit" autocomplete="off" class="monthpicker form-control reqModal" placeholder="Click to choose" spellcheck="true" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" data-update="#modalAchievement" data-validate="chkModal('modalAchievement_edit')">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="hide">
  <ul id="modalAchievement_template">
    <li class="list-group-item" data-content='[content]' data-id='[id]'>
      <div><span class="education-date">[date]</span>[name]</div>
      <div>Organization: [organization] </div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
  <ul id="modalExActivity_template">
    <li class="list-group-item" data-content='[content]' data-id='[id]'>
      <div><span class="education-date">[fromdate] - [todate]</span></div>
      <div>Organization: [organization] Position: [position]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
  <ul id="modalSkills_template">
    <li class="list-group-item" data-content='[content]' >
      <div>[name] ([position]) </div>
      <div>Organization: [organization]</div>
      <div>Contact: [contact] Email: [email]</div>
      <div class="addon">
        <a class="btn btn-edit" title="Edit"><span class="fa fa-pencil-alt"></span></a>
        <a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>
      </div> 
    </li>
  </ul>
</div>
<script>
  var skill_language = <?=json_encode($skills["language"])?>;
  var skill_computer = <?=json_encode($skills["computer"])?>;
</script>