<h2><span class="fa fa-paperclip"></span> Attachment(s)</h2>
<div class="panel-group" id="attachment_accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" >
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#attachment_accordion" href="#attachment1" aria-expanded="true" aria-controls="attachment1">
          Attachment(s)
        </a>
      </h4>
    </div>
    <div id="attachment1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12">
            <label>DSE Results (max 300KB, .JPG /.PDF format):</label>
            <div class="ajax-upload row <?=($member->cvform["application"]["dse"]) ? "hide" : ""?>" data-placement="top" title="" data-validfiletype="image/jpeg,application/pdf">
              <input type="file" name="dse" class="form-control" id="dse" accept=".jpg,.pdf" />
              <div class="col-sm-10">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
              </div>
              <div class="col-sm-1"><button type="button" class="btn btn-upload btn-default">Upload</button></div>              
            </div>
            <div class="link-wrapper row <?=($member->cvform["application"]["dse"]) ? "" : "hide"?>">
              <div class="link col-sm-9"><?php
                $filename = explode("/", $member->cvform["application"]["dse"]);
                $filename = $filename[count($filename) - 1];
                if ($filename != "") {
              ?><a href="<?=$appPath . "/" . $member->cvform["application"]["dse"] ?>" rel="noreferrer noopener" target="_blank"><i class="fa fa-paperclip"></i> <?=$filename?></a><?php
                }
              ?>
              </div>
              <div class="text-right col-sm-3"><a class="btn btn-remove" data-remove-attach="dse"><i class="fa fa-ban"></i> Delete</a></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label>HKCC Academic Results (max 300KB, .JPG /.PDF format):</label>
            <div class="ajax-upload row <?=($member->cvform["application"]["hkcc"]) ? "hide" : ""?>" data-placement="top" title="" data-validfiletype="image/jpeg,application/pdf">
              <input type="file" name="hkcc" class="form-control" id="hkcc" accept=".jpg,.pdf" />
              <div class="col-sm-10">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
              </div>
              <div class="col-sm-1"><button type="button" class="btn btn-upload btn-default">Upload</button></div>
            </div>
            <div class="link-wrapper row <?=($member->cvform["application"]["hkcc"]) ? "" : "hide"?>">
              <div class="link col-sm-9"><?php
                $filename = explode("/", $member->cvform["application"]["hkcc"]);
                $filename = $filename[count($filename) - 1];
                if ($filename != "") {
              ?><a href="<?=$appPath . "/" . $member->cvform["application"]["hkcc"] ?>" rel="noreferrer noopener" target="_blank"><i class="fa fa-paperclip"></i> <?=$filename?></a><?php
                }
              ?></div>
              <div class="text-right col-sm-3"><a  class="btn btn-remove" data-remove-attach="hkcc"><i class="fa fa-ban"></i> Delete</a></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label>Others 1  (max 300KB, .JPG /.PDF format):</label>
            <div class="ajax-upload row <?=($member->cvform["application"]["other1"]) ? "hide" : ""?>" data-placement="top" title="" data-validfiletype="image/jpeg,application/pdf">
              <input type="file" name="other1" class="form-control" id="other1" accept=".jpg,.pdf" />
              <div class="col-sm-10">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
              </div>
              <div class="col-sm-1"><button type="button" class="btn btn-upload btn-default">Upload</button></div>
            </div>
            <div class="link-wrapper row <?=($member->cvform["application"]["other1"]) ? "" : "hide"?>">
              <div class="link col-sm-9"><?php
                $filename = explode("/", $member->cvform["application"]["other1"]);
                $filename = $filename[count($filename) - 1];
                if ($filename != "") {
              ?><a href="<?=$appPath . "/" . $member->cvform["application"]["other1"] ?>" rel="noreferrer noopener" target="_blank"><i class="fa fa-paperclip"></i> <?=$filename?></a><?php
                }
              ?></div>
              <div class="text-right col-sm-3"><a  class="btn btn-remove" data-remove-attach="other1"><i class="fa fa-ban"></i> Delete</a></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label>Others 2 (max 300KB, .JPG /.PDF format):</label>
            <div class="ajax-upload row <?=($member->cvform["application"]["other2"]) ? "hide" : ""?>" data-placement="top" title="" data-validfiletype="image/jpeg,application/pdf">
              <input type="file" name="other2" class="form-control" id="other2" accept=".jpg,.pdf"/>
              <div class="col-sm-10">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
              </div>
              <div class="col-sm-1"><button  type="button" class="btn btn-upload btn-default">Upload</button></div>
            </div>
            <div class="link-wrapper row <?=($member->cvform["application"]["other2"]) ? "" : "hide"?>">
              <div class="link col-sm-9"><?php
                $filename = explode("/", $member->cvform["application"]["other2"]);
                $filename = $filename[count($filename) - 1];
                if ($filename != "") {
              ?><a href="<?=$appPath . "/" . $member->cvform["application"]["other2"] ?>" rel="noreferrer noopener" target="_blank"><i class="fa fa-paperclip"></i> <?=$filename?></a><?php
                }
              ?></div>
              <div class="text-right col-sm-3"><a  class="btn btn-remove" data-remove-attach="other2"><i class="fa fa-ban"></i> Delete</a></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label>Others 3 (max 300KB, .JPG /.PDF format):</label>
            <div class="ajax-upload row <?=($member->cvform["application"]["other3"]) ? "hide" : ""?>"  data-placement="top" title="" data-validfiletype="image/jpeg,application/pdf">
              <input type="file" name="other3" class="form-control" id="other3" accept=".jpg,.pdf"/>
              <div class="col-sm-10">
                  <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
              </div>
              <div class="col-sm-1"><button  type="button" class="btn btn-upload btn-default">Upload</button></div>
            </div>
            <div class="link-wrapper row  <?=($member->cvform["application"]["other3"]) ? "" : "hide"?>">
              <div class="link col-sm-9"><?php
                $filename = explode("/", $member->cvform["application"]["other3"]);
                $filename = $filename[count($filename) - 1];
                if ($filename != "") {
              ?><a href="<?=$appPath . "/" . $member->cvform["application"]["other3"] ?>" rel="noreferrer noopener" target="_blank"><i class="fa fa-paperclip"></i> <?=$filename?></a><?php
                }
              ?></div>
              <div class="text-right col-sm-3"><a  class="btn btn-remove" data-remove-attach="other3"><i class="fa fa-ban"></i> Delete</a></div>
            </div>
          </div>
          <input type="hidden" name="application_id" value="<?=$member->cvform["application"]["id"]?>" />
        </div>
      </div>
    </div>
  </div>
   <div class="row rowbtn">
    <div class="hidden-xs col-sm-6"><a class="btn " data-active="other"><i class="fa fa-caret-left"></i> Previous</a></div>
    <div class="visible-xs col-xs-6"><a data-active="other"><i class="fa fa-caret-left"></i> Previous</a></div>
    
  </div>
  
  
</div>