<?php
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["Jobs"];
	
	include_once("breadcrumb.php");
	include_once("classes/application.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	

	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	$limit = 10;
	$offset = ($page - 1) * $limit;

	$application = $member->getApplication();
	
	if ($application["status"] != "NEW") {
		header("location: job_preferences");
		exit();
	}
	$application_id = $application["id"];

	$application = new Application($application["id"]);
	$joblist = $application->getItems();

?><main id="job_selfassess" class="joblist">
		<div class="wrapper">
			<ul class="nav nav-step hidden-xs">
				<li><a href="joblist.php">Job List</a></li>
				<li><a href="job_preferences.php">Job Preference <span class="badge"><?=count($joblist)?></span></a></li>
				<li>Confirmation</li>
				<li class="active">Submission</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation"><a href="joblist.php">Job List</a></li>
				<li role="presentation"><a href="job_preferences">Job Preference (<?=count($joblist)?>)</a></li>
				<li role="presentation" class="active"><a>Confirm / Submit</a></li>
			</ul>
			<div class="container">
				<h1 class="page_header">Self Assessment Form </h1>
				<section id="joblist_wrapper">
					<div class="row">
						<data data-cms-title="wie_app_selfassess">
							<?=$cmsObj->getPageArea("wie_app_selfassess");?>
							<textarea class="form-control" name="selfassess" id="selfassess"></textarea>
						</data>
					</div>
					<div class="text-center saveRow"><a class="btn btn-default" >Agree and Preview Application Form</a></div>
				</section>
			</div>
		</div>
		<script type="text/javascript" src="scripts/wie_app.js"></script>
	</main>
	
<?php
	include_once("footer.php");
?>