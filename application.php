<?php
	require_once("config.inc.php");
	include_once("classes/application.php");

	$t = base64_decode($_GET["t"]);
	$id = substr($_GET["id"], 0, -13);

	if ($id != $t) {
		header('HTTP/1.0 404 Not Found');
		exit();
	}

	$application = new Application();
	$application = $application->getFromHashId($id)->getDetail();

	$student = new Student($application["member_id"]);	
	$member = Lecturer::retain();
	if (!$member->isLogined()) {
		$member = new Student();

		if (!$member->isLogined()) {
			header('HTTP/1.0 403 Forbidden');
			exit();
		}

		if ($member->getId() != $application["member_id"]) {
			header('HTTP/1.0 403 Forbidden');
			exit();
		}
	}

	$data = $student->getCVForm($application["id"]);

	$student = $student->getDetail();
	



?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Curriculum Vitae Builder - College of Professional and Continuing Education</title>
	<meta name='robots' content='noindex,follow' />	
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css"  />
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" />
</head>
<body class="font16">
	<main id="application_form" >
		<div class="wrapper">
		<table width="100%" border="1" align="center">
			<tr><th colspan="5"><h3>Personal Particulars</h3></th></tr>
			<tr><th width="25%">English Name:</th><td width="25%"><?=$data["personal"]["en_name"]?> <?=$data["personal"]["en_lastname"]?></td>
				<th width="25%">Chinese Name:</th><td width="25%"><?=$data["personal"]["ch_lastname"]?><?=$data["personal"]["ch_name"]?></td>
				<td rowspan="4"><?php
				if ($application["photo"]) {
				?><img src="<?=$appPath ."/". $application["photo"]?>" height="200"><?php
				}
				?></td></tr>
			<tr><th>Home District:</th><td><?=($data["personal"]["district"]["name"]);?></td>
				<th>Gender:</th><td><?=($data["personal"]["gender"]);?></td></tr>
			<tr><th>College Email:</th><td colspan="3"><?=($student["c_email"])?></td></tr>
			<tr><th>Contact Number:</th><td colspan="3"><?=($application["contact"]);?></td></tr>
		</table>
		<p></p>

		<table width="100%" border="1" align="center">
			<tr><th colspan="4"><h3>Education</h3></th></tr>
			<tr><th>From<br>(MM/YY):</th>
				<th>To<br>(MM/YY):</th>
				<th>School/College:</th>
				<th>Class/Course of Study (Study Year)</th></tr>
			<?php
				foreach ($data["education"] as $edu) {
					$content = json_decode($edu["content"], true);
			?><tr><td><?=($content["fromdate"])?></td>
				<td><?=($content["todate"])?></td>
				<td><?=($content["school"])?></td>
				<td><?=($content["class"] . " (".$content["year"].")")?></td></tr><?php
				}
				?>
		</table>
		<p></p>

		<table width="100%" border="1" align="center">
			<tr><th colspan="4" align="center"><h3>Academic Attainment </h3></th></tr>
			<tr><th>Date Obtained</th>
				<th>School/College</th>
				<th>GPA/ Public Exam Results</th></tr>
			<?php
				if ($data["application"]["gpa"]) {
					?><tr><td><?=$data["application"]["gpa"]["date"]?></td>
				  <td><?=$data["application"]["gpa"]["school"]?></td>
				  <td>CGPA: <?=$data["application"]["gpa"]["point"]?></td>
			 	  <tr><?
				}
				if ($data["application"]["cgpa"]["date"]) {

					?><tr><td><?=$data["application"]["cgpa"]["date"]?></td>
				  <td><?=$data["application"]["cgpa"]["college"]?></td>	
				  <td>CGPA of Pre-Associate Degree: <?=$data["application"]["cgpa"]["point"]?></td>
			 	  <tr><?
				} else {
					?><tr><td></td>
				  <td></td>	
				  <td>CGPA of Pre-Associate Degree: N/A</td>
			 	  <tr><?
				}
				foreach ($data["exam_result"] as $edu) {
					$content = json_decode($edu["content"], true);
			?><tr><td><?=$content["year"]?></td>
				  <td><?=$content["exam"]?></td>
			 	  <td><?php 

			 	  foreach ($content["exam_result"] as $key => $value) {
			 	  	echo "$key ($value) <BR> ";
			 	  }?></td></tr><?php
				}
				foreach ($data["public_exam"] as $edu) {
					$content = json_decode($edu["content"], true);
			?><tr><td><?=$content["date"]?></td>
				<td><?=$content["name"]?> <?=($content["organization"] == "") ? "" : "(".$content["organization"].")"?></td>
				<td><?php

					$content["grade"] = json_decode($content["grade"], true);
					foreach ($content["grade"] as $nnn=>$obj) {
						echo "$nnn ($obj) <BR> ";

					} ?></td></tr><?php
				}
			?>
		</table>
		<p></p>

		<table width="100%" border="1" align="center">
			<tr><th colspan="6"><h3>Work Experience</h3></th></tr>
			<tr><th width="100">From<br>(MM/YY):</th>
				<th width="100">To<br>(MM/YY):</th>
				<th width="300">Name of Organization</th>
				<th width="180">Type of Employment</th>
				<th width="180">Position </th>
				<th>Major Duties</th></tr>
			<?php
				if (count($data["experience"]["work"]) == 0) {
					?><tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr><?
				}
				foreach ($data["experience"]["work"] as $work) {
					$work["content"] = json_decode($work["content"], true);
			?><tr><td><?=$work["content"]["fromdate"]?></td>
				<td><?=$work["content"]["todate"]?></td>
				<td><?=$work["content"]["organization"]?></td>
				<td><?=$work["content"]["employment"]?></td>
				<td><?=$work["content"]["position"]?> </td>
				<td valign="top"><?=nl2br($work["content"]["duties"])?></td></tr><?
			}
				?>
		</table>
		<p></p>

		<table width="100%" border="1" align="center">
			<tr><th colspan="6"><h3>Internship Experience</h3></th></tr>
			<tr><th width="100">From<br>(MM/YY):</th>
				<th width="100">To<br>(MM/YY):</th>
				<th width="300">Name of Organization</th>
				<th width="180">Type of Employment</th>
				<th width="180">Position </th>
				<th>Major Duties</th></tr>
			<?php
				if (count($data["experience"]["internship"]) == 0) {
					?><tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr><?
				}
				foreach ($data["experience"]["internship"] as $work) {
					$work["content"] = json_decode($work["content"], true);
			?><tr><td><?=$work["content"]["fromdate"]?></td>
				<td><?=$work["content"]["todate"]?></td>
				<td><?=$work["content"]["organization"]?></td>
				<td><?=$work["content"]["employment"]?></td>
				<td><?=$work["content"]["position"]?> </td>
				<td valign="top"><?=nl2br($work["content"]["duties"])?></td></tr><?
				}


				?>
		</table>
		<p></p>

		<table width="100%" border="1" align="center">
			<tr><th colspan="5"><h3>Extra-Curricular Activit(ies)</h3></th></tr>
			<tr><th width="10%">From<br>(MM/YY):</th>
				<th width="10%">To<br>(MM/YY):</th>
				<th width="30%">Name of Organization</th>
				<th width="25%">Position </th>
				<th width="25%">Major Duties</th></tr>
			<?php
			if (count($data["achievement"]["exactivity"]) == 0) {
				?><tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr><?
			}
			foreach ($data["achievement"]["exactivity"] as $work) {
				$work["content"] = json_decode($work["content"], true);
			?><tr><td><?=$work["content"]["fromdate"]?></td>
				<td><?=$work["content"]["todate"]?></td>
				<td><?=$work["content"]["organization"]?></td>
				<td><?=$work["content"]["position"]?> </td>
				<td valign="top"><?=$work["content"]["duties"]?></td></tr><?
			}
				?>
		</table>
		<p></p>

		<table width="100%" border="1" align="center">
			<tr><th colspan="4"><h3>Professional Qualification(s) </h3></th></tr>
			<tr><th>Date Issued:</th>
				<th>Examination/ Certification:</th>
				<th>Name of Issuing Organization</th>
				<th>Grade/ Level obtained</th></tr>
			<?php
			if (count($data["qualification"]["professional"]) == 0) {
				?><tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr><?
			}
			foreach ($data["qualification"]["professional"] as $work) {
				$work["content"] = json_decode($work["content"], true);
			?><tr><td><?=$work["content"]["date"]?></td>
				<td><?=($work["content"]["name"])?></td>
				<td><?=$work["content"]["organization"]?></td>
				<td valign="top"><?=$work["content"]["grade"]?></td></tr><?
			}
				?>
		</table>	
		<p></p>

		<table width="100%" border="1" align="center">
			<tr><th colspan="4"><h3>Other Qualification(s) </h3></th></tr>
			<tr><th>Date Issued:</th>
				<th>Examination/ Certification:</th>
				<th>Name of Issuing Organization</th>
				<th>Grade/ Level obtained</th></tr>			
			<?php
			if (count($data["qualification"]["other"]) == 0) {
				?><tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr><?
			}
			foreach ($data["qualification"]["other"] as $work) {
				$work["content"] = json_decode($work["content"], true);
			?><tr><td><?=$work["content"]["date"]?></td>
				<td><?=($work["content"]["name"])?></td>
				<td><?=$work["content"]["organization"]?></td>
				<td valign="top"><?=$work["content"]["grade"]?></td></tr><?
			}
				?>
		</table>	
		<p></p>


		<table width="100%" border="1" align="center">
			<tr><th colspan="4"><h3>Achievement(s)</h3></th></tr>
			<tr><th>Date Issued:</th>
				<th>Name of Issuing Organization</th>
				<th>Grade/ Level obtained</th></tr>

			<?php
			if (count($data["achievement"]["achievement"]) == 0) {
				?><tr><td>N/A</td><td>N/A</td><td>N/A</td></tr><?
			}
			foreach ($data["achievement"]["achievement"] as $work) {
				$work["content"] = json_decode($work["content"], true);

			?><tr><td><?=$work["content"]["date"]?></td>
				<td><?=$work["content"]["organization"]?></td>

				<td valign="top"><?=$work["content"]["name"]?></td></tr><?
			}
				?>
		</table>

		<p></p>
		<table width="100%" border="1" align="center">
			<tr><th colspan="6"><h3>Skill(s)</h3></th></tr>
			<tr><td colspan="6"><strong>Language Proficiency</strong></td></tr>
			<?php
				$skill = json_decode($data["achievement"]["skill"][0]["content"], true);
				foreach ($skill["language"] as $language=>$kkk) {
			?>
			<tr><td width="25%"><?=ltrim($language, "_")?></td>
				<td width="75%" valign="top" colspan="5">
					<table  border="0" align="left">
						<tr><?php
						foreach ($kkk as $key => $value) {
							?><td width="25%"><STRONG><?=$key?>: </STRONG></td>
							  <td width="25%"><?=$value?></td><?php
						}
						?></tr>
					</table>
				</td></tr><?php
				}
			?>
			<tr><td colspan="6"><strong>Technical Skills</strong></td></tr>
			<tr><?php
				$count = count($skill["computer"]);
				foreach ($skill["computer"] as $language=>$kkk) {

					
					?><td><?=ltrim($language, "_")?></td>
					  <td <?=(($count -1) == $i) ? 'colspan="'. (5 - (($i % 3) * 2)) .'"' : ""?>><?=$kkk?></td><?php
					if ($i++ % 3 == 2) {
						?></tr><tr><?
					}
				}
			?></tr>
			<tr><td colspan="6"><strong>Other Skills</strong></td></tr>
			<?php

				$other_skill = ($data["achievement"]["skill_other"]);
				if (count($other_skill) == 0) {
					?><tr><td>N/A</td><td colspan="5" valign="top">N/A</td></tr><?
				}
				foreach ($other_skill as $ooo) {
					$ooo = json_decode($ooo["content"], true);
					?><tr><td><?=$ooo["skill"]?></td><td colspan="5" valign="top"><?=html_entity_decode($ooo["description"], ENT_QUOTES, "UTF-8")?></td></tr><?
				}
			?>
		</table>	
		<p></p>


		<table width="100%" border="1" align="center">
			<tr><th colspan="4"><h3>Emergency Contact</h3></th></tr>
			<tr><th>Name:</th>
				<th>Contact: </th>
				<th>Relationship:</th></tr>

			<?php
			
			//foreach ($data["achievement"]["achievement"] as $work) {
		

			?><tr><td><?=$data["personal"]["em_name"] . " " . $data["personal"]["em_name2"]?></td>
				<td><?=$data["personal"]["em_contact"]?></td>

				<td valign="top"><?=$data["personal"]["em_relation"]?></td></tr><?
			//}
				?>
		</table>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
	</main>
</body>
</html>

