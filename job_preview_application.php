<?php
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["Jobs"];
	
	include_once("breadcrumb.php");
	include_once("classes/application.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	

	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	$limit = 10;
	$offset = ($page - 1) * $limit;

	$application = $member->getApplication();

?><main id="job_codeofpractice" class="joblist">
		<div class="wrapper">
			<ul class="nav nav-step">
				<li><a href="joblist">Job List</a></li>
				<li><a href="job_preferences">Job Preference <span class="badge"><?=count($joblist)?></span></a></li>
				<li><a href="job_confirm">Confirmation</a></li>
				<li class="active">Submission</li>
			</ul>
			<div class="container">
				<h1 class="page_header">Preview Application Form </h1>
				<section id="joblist_wrapper">
						<div class="row">
							<iframe src="app_form-<?=uniqid(sha1("application-".$application["id"]))?>-<?=base64_encode(sha1("application-".$application["id"]))?>" frameborder="0" width="100%" height="600" ></iframe>
						</div>
					<div class="text-center saveRow">
						<a href="job_confirm" class="btn btn-default">Back</a>
						<a class="btn btn-default btn-submit-app" data-id="<?=$application_id?>">Agree and Submit</a>
					</div>
				</section>
				<div class="hide">
					<div id="dialog-confirm" title="submit WIE application?">
					  <p>You cannot edit this WIE Application Form after submission. Do you wish to proceed with your application?</p>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="scripts/wie_app.js" async></script>		
	</main>
	
<?php
	include_once("footer.php");
?>