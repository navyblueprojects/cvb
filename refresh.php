<?php
	require_once("config.inc.php");
	require_once("classes/student.php");
	require_once("classes/lecturer.php");

	session_start();
	$key = array_key_exists(sha1("cvbuilder_keepalive"), $_SESSION) ? $_SESSION[sha1("cvbuilder_keepalive")] : false;
	session_write_close();

	if ($key) {
		if ($key < time()) {
			Student::logout();
			Lecturer::logout();
		}
	}
	
	session_start();
	$_SESSION[sha1("cvbuilder_keepalive")] = time() + 300;
	session_write_close();
