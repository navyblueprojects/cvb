<?php
	require_once("config.inc.php");
	require_once("classes/jobs.php");

	include_once("header.php");
	$pageName = ["Jobs"];
	include_once("breadcrumb.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	
	if (!array_key_exists("id", $_GET)) {
		header("location: joblist.php");
		exit();
	}
	$job = new Jobs();

	//(int)$_GET["id"]);
	$job->getJobByJobNo($_GET["id"]);
	$job = $job->getDetails();

	session_start();
	$joblistpage = $_SESSION[sha1($dbDatabase."_page_joblist")];
	session_write_close();
	
	$limit = 10;
	$offset = ($joblistpage - 1) * $limit;

	$joblist = $member->getJobList($offset, $limit);
	$prev = $next = false;
	foreach ($joblist as $idx=>$jj) {
	
		if ($job["id"] == $jj["id"]) {
			$next = $joblist[$idx+1];
			break;
		} else {
			$prev = $joblist[$idx];
		}
	}
	
	$application = $member->getApplication();
	$application = new Application($application["id"]);
	
	$joblist = $application->getItems();
	
?><main id="jobdetails" class="joblist">
		<div class="wrapper">
			<ul class="nav nav-step hidden-xs">
				<li class="active"><a  href="joblist">Job List</a></li>
				<li><a href="job_preferences">Job Preference <span class="badge"><?=count($joblist)?></span></a></li>
				<li>Confirmation</li>
				<li>Submit</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation" class="active"><a href="joblist">Job List</a></li>
				<li role="presentation" ><a href="job_preferences">Job Preference (<?=count($joblist)?>)</a></li>
				<li role="presentation" class="disabled"><a>Confirm / Submit</a></li>
			</ul>
			
			<div class="container">
				
				<div class="height0_wrapper back text-right"><a href="joblist.php" class="btn btn-default">Back to List</a></div>
				<h1 class="page_header">Jobs</h1>
				<div >
					<section id="jobdetails_left" class="col-md-2"><div><var data-cms-title="job_app_details"><?=$cmsObj->getPageArea("job_app_details");?></var>
						</div>
						
					</section>
					
					<section id="jobdetails_wrapper" class="col-md-10">
						<h2><?=$job["name"]?></h2>
						<?php
							$fields = array("job_no"=>"Job No.",
							"name"=>"Position",
							"company"=>"Company",
							"dept"=>"Department",
							"vacancy"=>"Vacancy",
							"post"=>"Type",
							"duty"=>"Duty",
							"period"=>"Period",
							"salary"=>"Salary/Allowance",
							"work_hour"=>"Working Hours",
							"work_day"=>"Working Days",
							"open_date"=>"Application Start Date",
							"close_date"=>"Application End Date",
							"remarks"=>"Remarks",
							"attachment"=>"Attachment",
							"attachment02"=>"Attachment 2",
							"attachment03"=>"Attachment 3");
						?>
						<ul id="details">
							<?php
							$jj = $job;
							$jj["attachment"] = "<a href='".UFILE_PATH . $jj["attachment"]."' target='_blank'>" . $jj["attachment"] . "</a>";
							foreach ($fields as $key => $value) {
								if ($job[$key] != "") {
									if ($key == "open_date" OR $key == "close_date") {
										?><li><label><?=$value?></label> <div><?=date("Y-m-d H:i", strtotime($jj[$key]))?></div></li><?php
									} else {
										?><li><label><?=$value?></label> <div><?=nl2br($jj[$key])?></div></li><?php
									}								
								}
							}
							
							?>
						</ul>

						<div class="action">
							<button class="btn btn-default" id="goAddJob" data-id="<?=$job["id"]?>">Add to My Preference</button>
						</div>
						<div class="bottomRow">
							<div class="col-md-12">
								<?php
									if ($prev) {
								?><a href="./job/<?=$prev["job_no"]?>-<?=str_replace("-", "", urlencode($prev["name"]."@".$prev["dept"]."@".$prev["company"]))?>" class=" pull-left" ><i class="fa fa-caret-left" ></i> Previous Job</a><?php
									}
									if ($next) {
								?><a href="./job/<?=$next["job_no"]?>-<?=str_replace("-", "", urlencode($next["name"]."@".$next["dept"]."@".$next["company"]))?>" class="pull-right"> Next Job <i class="fa fa-caret-right" ></i> </a><?php
									}
								?>
							</div>
						</div>
					</section>
				</div>

			</div>
		</div>
		<script type="text/javascript" src="scripts/wie_app.js"></script>
	</main>
	<div class="modal fade" tabindex="-1" role="dialog" id="errorModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Error</h4>
	      </div>
	      <div class="modal-body">
	        <h3 id="errMsg"></h3>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php
	include_once("footer.php");
?>