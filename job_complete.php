<?php
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["WIE Application Number"];
	
	include_once("breadcrumb.php");
	include_once("classes/application.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	
	
	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	$limit = 10;
	$offset = ($page - 1) * $limit;

	$application = $member->getLastApplication();
	$application = new Application($application["id"]);

	
?><main id="job_complete" class="joblist">
		<div class="wrapper">
			
			<div class="container">
				<h1 class="page_header">WIE Application Number</h1>
				<div class="row text-center"><var data-cms-title="job_app_submitted"><?=$cmsObj->getPageArea("job_app_submitted");?></var></div>
				<div class="text-center app_no row">
					<div class="col-md-4 col-md-offset-4">
						<label>WIE Application Number</label>
						<span><?=$application->getApplicationNumber();?></span>
					</div>
				</div>
				<div class="row text-center saveRow">
					<a href="home" class="btn btn-default">Home</a>
					<a href="logout" class="btn btn-default">Logout</a>
				</div>
				
			</div>
		</div>
		<script type="text/javascript" src="scripts/wie_app.js"></script>
	</main>
<?php
	include_once("footer.php");
	
	
?>