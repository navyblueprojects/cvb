<?php
	require_once("config.inc.php");
	require_once("classes/cms.php");
	require_once("classes/student.php");

	$member = new Student();
	$member = $member::retain();

	$cmsObj = new CMS();

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Curriculum Vitae Builder - College of Professional and Continuing Education</title>
	<meta name='robots' content='noindex,follow' />

	<base href="https://<?=$domain.$appPath?>/" >

	<link rel="preload" href="css/fontawesome-all.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
	<link rel="stylesheet" href="css/style.css?v=<?=time()?>" />
	<link rel="preload" href="css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" />
	<link rel="preload" href="css/bootstrap-theme.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'"  />
	<link rel="preload" href="css/jquery.datetimepicker.css" as="style" onload="this.onload=null;this.rel='stylesheet'" />	
	<link rel="preload" href="js/daterangepicker.css" as="style" onload="this.onload=null;this.rel='stylesheet'" />
	<link rel="preload" href="js/jquery-ui.min.css"  as="style" onload="this.onload=null;this.rel='stylesheet'" />
	<?php
		if ($preload) {

			foreach ($preload as $preload_obj) {
    ?><link rel="preload" href="<?=$preload_obj[0]?>" as="<?=$preload_obj[1]?>" type="<?=$preload_obj[2]?>" />
    <?php
   			}
		}
	?>


	<link rel="shortcut icon" type="image/png" href="favicon.png"/>
	<script src="js/jquery-2.2.5.min.js" ></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/bootstrap.min.js" async></script>
	<script src="js/moment.min.js" async></script>
	<script src="js/jquery.datetimepicker.js" async></script>
	<script src="js/daterangepicker.js" async></script>
	<script src="scripts/member.js" async></script>
	<script src="js/bootstrap-filestyle.min.js" async> </script>
</head>
<body class="font12">
	<header>
		<div class=" row1">
			<div class="wrapper">
				<div class="row">
					<div class="col-xs-6 row">
						
						<div class="col-sm-12">
							<div class="dropdown">
								<a class="dropdown-toggle" id="ddFontSize" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<i class="fas fa-font"></i>
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" aria-labelledby="ddFontSize">
									<li><a data-font-size="font12">Small</a></li>
									<li><a data-font-size="font14">Medium</a></li>
									<li><a data-font-size="font16">Large</a></li>
								</ul>
							</div>
							

						</div>
					</div>
					<div class="col-md-4 hidden-sm hidden-xs pull-right">
						<?php
							if (!$member->isLogined()) {
							?><ul id="header_portal_nav"><li><a href="./wie/">Staff</a></li><li>Students</li></ul><?php
							}
						?>
					</div>
					<div class="visible-sm visible-xs pull-right">
						<div class="dropdown">
							<a class="dropdown-toggle" id="ddUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="fas fa-user"></i>
								<span class="caret"></span>
							</a>
							<?php
								if (!$member->isLogined()) 
								{
							?><ul class="dropdown-menu dropdown-menu-right" aria-labelledby="ddUser">
								<li><a href="./wie/">Staff</a></li>
								<li><a href="login.php">Students</a></li></ul><?php
								}
							?>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class=" row2">
			<div class="wrapper">
				<div class="row">
					<div class="col-md-8" id="logo">
						<a href="https://www.polyu.edu.hk/"><img src="images/logo_polyu.svg" alt="PolyU" /></a>
						<a href="https://www.cpce-polyu.edu.hk/"><img src="images/logo_cpce.svg" alt="PolyU CPCE" /></a>
					</div>
					<div class="col-md-4 hidden-sm hidden-xs text-right" ><a href="http://www.hkcc-polyu.edu.hk/"><img src="images/logo_03.svg" alt="PolyU HKCC"/></a></div>
					<div class=" visible-sm visible-xs text-right college-link" >
						<ul>
							<li><a href="http://www.hkcc-polyu.edu.hk/">HKCC <i class="fa fa-angle-right"></i></a></li>
							<li><a href="https://www.speed-polyu.edu.hk/" rel="noreferrer noopener"  target="_blank" title="This link will pop up in a new window">SPEED <i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>