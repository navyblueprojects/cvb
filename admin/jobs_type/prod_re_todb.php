<?php
ob_start();
include_once("../../config.inc.php");
require_once($rootPath . $adminPath."/config.inc.php");
include_once("../../connDB.php");
include_once("config.inc.php");

$id = (array_key_exists("id", $_POST)) ? $_POST['id'] : 0;
$prodOriId = (array_key_exists("prodOriId", $_POST)) ? $_POST['prodOriId'] : "";
$prodId = (array_key_exists("prodId", $_POST)) ? $_POST['prodId'] : "";
$keyword = (array_key_exists("keyword", $_POST)) ? $_POST['keyword'] : "";

if ($id == 0) {
	header("location: list.php");
	exit;
}

if(count($prodOriId)>0){
	$removeList=implode(',',$prodOriId);
	$conn->Execute("DELETE FROM `".$relTableName."` WHERE (`prodId_1`='".$id."' AND `prodId_2` IN (".$removeList.")) OR (`prodId_2`='".$id."' AND `prodId_1` IN (".$removeList."))");
}

foreach ($prodId as $eachProdId) {
	$fields['prodId_1'] = $id;
	$fields['prodId_2'] = $eachProdId;
	$fields['lastModDate'] = date("Y-m-d H:i:s");
	$conn->AutoExecute("`".$relTableName."`", $fields, 'INSERT');
}

$conn->Execute("OPTIMIZE TABLE `".$relTableName."`");
$conn->close();

header("location: ./prod_re.php?id=".$id.(($keyword!="")?"&keyword=".$keyword:""));
ob_end_flush();
exit;
?>