<style type="text/css">

a.navLink:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
	text-decoration: none;
	font-weight:bold;
}
a.navLink:visited {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
	text-decoration: none;
	font-weight:bold;

}
a.navLink:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #E6524E;
	text-decoration: none;
	font-weight:bold;

}

</style>
<table border="0"  width="100%"cellspacing="0" cellpadding="4" >
<tr>
	<td height="20" bgcolor="<?=$navBgColor ?>" colspan="10" style="font-family:Arial, Helvetica, sans-serif; font-size: 13; font-weight: bold; font-style:italic; color:#FFFFFF"><?= $navHeadText ?></td>
	</tr>
	</table>
<table border="0" cellspacing="0" cellpadding="0" style="padding-right: 20px; padding-top: 5px">
	
  <tr>
  	<td style="padding-right: 4px;"></td>
	<td><a href=$adminPath."/category" class="navLink">Category</a></td>
	<td><a href=$adminPath."/product" class="navLink">Product</a></td>
	<td><a href=$adminPath."/prodfeature" class="navLink">Product Feature</a></td>
	<td><a href=$adminPath."/news" class="navLink">News</a></td>
	<td><a href=$adminPath."/banner/add_edit.php?id=1" class="navLink">Banner</a></td>
	<td><a href=$adminPath."/logout.php" class="navLink">Logout</a></td>
  </tr>
</table>
