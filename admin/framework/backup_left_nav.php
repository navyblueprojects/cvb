<div id="leftPanel">
 
 		<a href="<?=ADMIN_LOGO_HREF?>" target="_blank">
		 	<div id="topSection">
			 		<img src="<?=ADMIN_LOGO?>"/>
		 			<?=ADMIN_LOGO_NAME?> 
		 	</div>
 		</a>
 
 	<div id="welcome_login">
	 	Welcome back, <?=ucfirst($_SESSION[$dbDatabase]["loginName"])?>
 	</div>
 	<br/>
 	<div id="contentSection" class="leftmenu_section">
 	 	<div class="leftmenu_sectionTitle">
 			Site Content
 		</div>
 		
 		 <? 
 		 	
 		 	foreach($leftNavContentItems["SECTIONS"] as $eachItem) { 
 		 		if(empty($eachItem[3]) || !empty($_SESSION[$dbDatabase][$eachItem[3]]) ){
					
					if(!$eachItem["isSubMenu"]){
				
 		 ?>
					    <a href="<?=$adminPath?>/<?=trim($eachItem[1])?>/<?=trim($eachItem[2])?>">
					 		<div class="leftmenu_item" onmouseover="onMenuHover('<?=trim($eachItem[1])?>');" >
					 			 <?=trim($eachItem[0])?>
					 		</div>
				 		</a>
				 		<hr/>   
	 		  
        <?		
        			}else{
		?>
						 <a href="<?=$adminPath?>/<?=trim($eachItem[1])?>/<?=(empty($eachItem["url"]))?  trim($eachItem[2]) : $eachItem["url"]?>" onmouseover="onMenuHover('<?=trim($eachItem[1])?>');"  class="submenu_<?=trim($eachItem["parent"])?>" <?=( strrpos($_SESSION[$dbDatabase]["currentView"],$eachItem["parent"]) !== false) ? "":"style='display:none'"?>>
					 		<div class="leftmenu_item submenu ">
					 			 <?=trim($eachItem[0])?>
					 		</div>
				 		</a>
		
		<?php 
        			}
        			
        			
        			
        			
				}
        		
 		 	}
        ?>
 		
 		
 		
 	
 		
 	</div>
 	<script>
		
		function onMenuHover(menu){
// 			$(".submenu_"+menu).stop().slideDown(300);
		}
		
		function onMenuOut(menu){
// 			$(".submenu_"+menu).stop().slideUp(300);
		}
		
 	</script>
 	<br/>
 	
 	<div id="configSection" class="leftmenu_section">
 		<div class="leftmenu_sectionTitle">
 			Site Configuration
 		</div>
 		
 		 <? foreach($leftNavSettingItems["SECTIONS"] as $eachItem) {
 		 		if(empty($eachItem[3]) || !empty($_SESSION[$dbDatabase][$eachItem[3]]) ){
 		 ?>
				   <a href="<?=$adminPath?>/<?=trim($eachItem[1])?>/<?=trim($eachItem[2])?>">
				 		<div class="leftmenu_item">
				 			 <?=trim($eachItem[0])?>
				 		</div>
			 		</a>
			 		<hr/>     
        <? 
        		}
 		 	} 
 		?>
 	</div>
 	
 	
 		
 	<div class="leftmenu_btnContainer">
 		<a href="<?=$appPath?>/admin/hp.php" target="_self">
		 	<div class="btnItem">
		 		Admin Home
		 	</div>
	 	</a>
	 	<a href="<?=$appPath?>" target="_blank">
		 	<div class="btnItem">
		 		Back to Site
		 	</div>
	 	</a>
 		<a href="<?=$adminPath?>/logout.php">
		 	<div class="btnItem">
		 		Logout
		 	</div>
	 	</a>
 	</div>
 </div>