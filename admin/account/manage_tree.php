<?php
ob_start();
require_once("../../config.inc.php"); # common config
require_once("config.inc.php"); # config
require_once($rootPath . $adminPath."/config.inc.php"); # admin config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");
if( !isset( $_SESSION ) ){
	session_start();
}

/* ------------------------ NEW OBJECT ------------------------ */
require_once($pathClassLib.'/dbtree.class.php');

// Create new object
$dbtree = new dbtree(DB_TABLE, 'node', $conn);
?>

    <?php include( $rootPath . $adminPath."/framework/overall_header.php" );?>
<link href="<?=$adminPath?>/css/slide.css" type="text/css" rel="stylesheet" />
    <body>
<div id="body" style=" width:100%;height:100%;overflow:hidden">

<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>




<?


/* ------------------------ MOVE ------------------------ */

/* ------------------------ MOVE 2 ------------------------ */

// Method 2: Assigns a node with all its children to another parent.
if (!empty($_GET['action']) && 'move_2' == $_GET['action']) {

    // Move node ($_GET['node_id']) and its children to new parent ($_POST['section2_id'])
    $dbtree->MoveAll((int)$_GET['node_id'], (int)$_POST['section2_id']);

    // Check errors
    if (!empty($dbtree->ERRORS_MES)) {
        echo 'DB Tree Error!';
        echo '<pre>';
        print_r($dbtree->ERRORS_MES);
        if (!empty($dbtree->ERRORS)) {
            print_r($dbtree->ERRORS);
        }
        echo '</pre>';
        exit;
    }

    
    $sql = "UPDATE `".DB_TABLE."` SET `node_parent`= ".(int)$_POST['section2_id']." WHERE `".DB_TABLE."`.`node_id` = '".(int)$_GET['node_id']."' LIMIT 1 ;";
    $res = $conn->Execute($sql);
    
    header('Location:'.$_SERVER['PHP_SELF']);
    exit;
}
/* ------------------------ MOVE 1 ------------------------ */

// Method 1: Swapping nodes within the same level and limits of one parent with all its children.
if (!empty($_GET['action']) && 'move_1' == $_GET['action']) {

    // Change node ($_GET['node_id']) position and all its childrens to
    // before or after ($_POST['position']) node 2 ($_POST['section2_id'])
    $dbtree->ChangePositionAll((int)$_GET['node_id'], (int)$_POST['section2_id'], $_POST['position']);

    // Check class errors
    if (!empty($dbtree->ERRORS_MES)) {
        echo 'DB Tree Error!';
        echo '<pre>';
        print_r($dbtree->ERRORS_MES);
        if (!empty($dbtree->ERRORS)) {
            print_r($dbtree->ERRORS);
        }
        echo '</pre>';
        exit;
    }

    header('Location:'.$_SERVER['PHP_SELF']);
    exit;
}

/* ------------------------ MOVE FORM------------------------ */

// Move section form
if (! @empty($_GET['action']) && 'move' == @$_GET['action']) {
    // Prepare the restrictive data for the first method:
    // Swapping nodes within the same level and limits of one parent with all its children
    $current_section = $dbtree->GetNodeInfo((int)$_GET['node_id']);
    
    
    $selectedNodeId = (int)$_GET['node_id'];
    
    //only allow leave moving
    $dbtree->Branch((int)$_GET['node_id'], array('node_id'));
    $isLeave = $dbtree->RecordCount() == 1? true : false;
    
    
    
    
    
    $dbtree->Parents((int)$_GET['node_id'], array('node_id'), array('and' => array('node_level = ' . ($current_section[2] - 1))));
    
    
    // Check class errors
    if (!empty($dbtree->ERRORS_MES)) {
        echo 'DB Tree Error!';
        echo '<pre>';
        print_r($dbtree->ERRORS_MES);
        if (!empty($dbtree->ERRORS)) {
            print_r($dbtree->ERRORS);
        }
        echo '</pre>';
        exit;
    }

    $item = $dbtree->NextRow();
    $r = $dbtree->Branch($item['node_id'], array('node_id', 'node_name'), array('and' => array('node_level = ' . $current_section[2])));
   

    // Create form
    ?>

    
    
     <div id="mainFrame" >
	<div id="listbody" >
	<div>
    
    	<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$pageName?></span>
		 	 -> Move Category
		</div>
   		
    
    
    
    
    
    
    
    
    
    
     <!-- start -->
      <table  id="editTable"  border="0" cellpadding="5" align="center" width="570">
        <tr>
            <td>
                <strong>Move Node</strong>
            </td>
        </tr>
        <tr>
            <td>
                <form action="<?=$_SERVER['PHP_SELF']?>?action=move_1&node_id=<?=$_GET['node_id']?>" method="post" onSubmit="this.submit.disabled=true;">
                <div style="padding:12px;font-weight:bold;"><strong>1) Swapping nodes within the same level and limits of one parent with all its children.</strong><br></div>
                
                <div style="padding:12px;">
                <span style="padding:12px;">Choose second section:</span>
                <select name="section2_id">
    <?php

    while ($item = $dbtree->NextRow()) {

        ?>
                    <option value="<?=$item['node_id']?>"><?=$item['node_name']?> <?php echo $item['node_id'] == (int)$_GET['node_id'] ? '<<<' : ''?></option>
        <?php

    }

    
    
  
    
    
    ?>
                </select><br>
                </div>
                
                <div style="padding:12px;">
                 <span style="padding:12px;">Choose position:</span>
                <select name="position">
                    <option value="after">After</option>
                    <option value="before">Before</option>
                </select><br>
                </div>
                <center>    <input name="submit" class="btnItem"  type="submit" value="Apply"><br>OR</center><br>
                </form>
                
                
      <?php 
      
      	if($isLeave){
      ?>          
                <form action="<?=$_SERVER['PHP_SELF']?>?action=move_2&node_id=<?=$_GET['node_id']?>" method="post" onSubmit="this.submit.disabled=true;">
                <div style="padding:12px;font-weight:bold;">2) Assigns node to another parent.<br></div>
               	<div style="padding:12px;">
               	<span style="padding:12px;"> Choose another node:</span>
                <select name="section2_id">
    <?php

  
    
    
    
    // Prepare the data for the second method:
    // Assigns a node with all its children to another parent
    $dbtree->Full(array('node_id', 'node_level', 'node_name'), array('or' => array('node_left <= ' . $current_section[0], 'node_right >= ' . $current_section[1])));

    // Check class errors
    if (!empty($dbtree->ERRORS_MES)) {
        echo 'DB Tree Error!';
        echo '<pre>';
        print_r($dbtree->ERRORS_MES);
        if (!empty($dbtree->ERRORS)) {
            print_r($dbtree->ERRORS);
        }
        echo '</pre>';
        exit;
    }

    while ($item = $dbtree->NextRow()) {

        ?>
                    <option value="<?=$item['node_id']?>"><?=str_repeat('&nbsp;', 6 * $item['node_level'])?><?=$item['node_name']?> <?php echo $item['node_id'] == (int)$_GET['node_id'] ? '<<<' : ''?></option>
        <?php

    }

    ?>
                </select><br>
                </div>
    <?php 
}
?>
                <center>
                <a href="<?=$_SERVER['PHP_SELF']?>"><input type="button" name="btnReset" value="Back"  class="btnItem" style="margin:0px 8px"/></a>
                <input name="submit" class="btnItem"  type="submit" value="Apply">
                </center>
                
                </form>
                
            </td>
        </tr>
    </table>
    
    
    
     </div>
    </div>
    </div>
    
    
    
    
    
    
    
    
    <?php
	exit();
}

/* ------------------------ DELETE ------------------------ */

// Delete node ($_GET['node_id']) from the tree wihtout deleting it's children
// All children apps to one level
if (!empty($_GET['action']) && 'delete' == $_GET['action']) {

	$sql = "SELECT * FROM `".DB_TABLE."` WHERE node_id = " . (int)$_GET['node_id'];
    $res = $conn->Execute($sql);

    // Check adodb errors
    if (FALSE === $res) {
        echo 'internal_error';
        echo '<pre>';
        print_r(array(2, 'SQL query error.', __FILE__ . '::' . __CLASS__ . '::' . __FUNCTION__ . '::' . __LINE__, 'SQL QUERY: ' . $sql, 'SQL ERROR: ' . $conn->ErrorMsg()));
        echo '</pre>';
        exit;
    }

    if (0 == $res->RecordCount()) {
        echo 'node_not_found';
        exit;
    } else {
// 		while (!$res->EOF) {		
// 			@unlink($uploadedFilePath."/".$res->fields["banner"]);
// 			@unlink($uploadedFilePath."/".$res->fields["sub_banner_left"]);
// 			@unlink($uploadedFilePath."/".$res->fields["sub_banner_right"]);
// 			$res->MoveNext();
// 		}
		// delete all product under this node
		$rs = $conn->Execute("SELECT * FROM ".$tableName." WHERE `node_id` = '".(int)$_GET['node_id']."'");
		$totalRecord = $rs->RecordCount();
		if($totalRecord > 0) {
			if (!$rs->EOF) {
				foreach ( $fixedField as $eachColumn ) {
					if ($eachColumn[2] == "FILE") {
						@unlink($uploadedFilePath."/".$rs->fields[$eachColumn[1]]);
					}
				}
			}
		}
		$conn->Execute("DELETE FROM ".$tableName." WHERE `node_id` = '".(int)$_GET['node_id']."'");
	}

    $dbtree->Delete((int)$_GET['node_id']);

    // Check class errors
    if (!empty($dbtree->ERRORS_MES)) {
        echo 'DB Tree Error!';
        echo '<pre>';
        print_r($dbtree->ERRORS_MES);
        if (!empty($dbtree->ERRORS)) {
            print_r($dbtree->ERRORS);
        }
        echo '</pre>';
        exit;
    }

    header('Location:'.$_SERVER['PHP_SELF']);
    exit;
}

/* ------------------------ ADD ------------------------ */

/* ------------------------ ADD OK ------------------------ */

// Add new node as children to selected node ($_GET['node_id'])
if (!empty($_GET['action']) && 'add_ok' == $_GET['action']) {
	

	$postArr = $_POST['section'];
	foreach($postArr as $name=>$value) {
		$tmpValue = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $value);
		$postArr[$name] = htmlspecialchars($tmpValue, ENT_QUOTES);
	}
	
	$parentId = (int)$_GET['node_id'];
	
    // Add new node
    $returnValue = $dbtree->Insert((int)$_GET['node_id'], '', $postArr);
	
    // Check class errors
    if (!empty($dbtree->ERRORS_MES)) {
        echo 'DB Tree Error!';
        echo '<pre>';
        print_r($dbtree->ERRORS_MES);
        if (!empty($dbtree->ERRORS)) {
            print_r($dbtree->ERRORS);
        }
        echo '</pre>';
        exit;
    } else {
		require_once($pathClassLib."/class.uploadfile.php");

		$additionalSQL ="";
		
		
		$sql = "UPDATE `".DB_TABLE."` SET ".$additionalSQL."`node_parent`= ".$parentId." WHERE `".DB_TABLE."`.`node_id` = '".(int)$returnValue."' LIMIT 1 ;";
		$res = $conn->Execute($sql);
		
		// Check adodb errors
		if (FALSE === $res) {
			echo 'internal_error';
			echo '<pre>';
			print_r(array(2, 'SQL query error.', __FILE__ . '::' . __CLASS__ . '::' . __FUNCTION__ . '::' . __LINE__, 'SQL QUERY: ' . $sql, 'SQL ERROR: ' . $conn->ErrorMsg()));
			echo '</pre>';
			exit;
		}
	}

    header('Location:'.$_SERVER['PHP_SELF']);
    exit;
}

/* ------------------------ ADD FORM ------------------------ */

// Add new node form
if (!empty($_GET['action']) && 'add' == $_GET['action']) {
    ?>
    <div id="mainFrame" >
<div id="listbody" >
<div>
<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$pageName?></span>
		 	 -> Add Category
		</div>
    <table id="listTable" class="formline"  border="0" cellpadding="0" align="center">
        <tr>
            <td align="left">
                <form action="<?=$_SERVER['PHP_SELF']?>?action=add_ok&node_id=<?=$_GET['node_id']?>" method="post" onSubmit="this.submit.disabled=true;" enctype="multipart/form-data">
                Node name:<br>
                <input type="text" name="section[node_name]" value="" style="width:450px"><br> 
                
                *Details of the node can be edit after added. 
                
                
                
                
                <div style="width:100%;padding:16px 0px">
				
				  	<div style="float:left">
		                <a href="<?=$_SERVER['PHP_SELF']?>"><input type="button" name="btnReset" value="Back"  class="btnItem" style="margin:0px 8px"/></a>
					</div>
				 	<div style="float:right">
		                <input type="submit" class="btnItem" name="submit" value="Submit"></center>
					</div>	
				
				</div>
                
                <center>
                </form>
            </td>
        </tr>
    </table>
    </div>
    </div>
    </div>
    <?php

}

/* ------------------------ LIST ------------------------ */

// Prepare data to view all tree
$dbtree->Full('');

// Check class errors
if (!empty($dbtree->ERRORS_MES)) {
    echo 'DB Tree Error!';
    echo '<pre>';
    print_r($dbtree->ERRORS_MES);
    if (!empty($dbtree->ERRORS)) {
        print_r($dbtree->ERRORS);
    }
    echo '</pre>';
    exit;
}

    ?>
    
<script type="text/javascript"><!--
function goDel(obj, node_id) {
	if (confirm('All the product(s) under this category will be also deleted.\n Are you sure to delete this category?')) {
		obj.disabled=true;
		window.location='<?=$_SERVER['PHP_SELF']?>?action=delete&node_id='+node_id;
	}
}
//--></script>	
<div id="mainFrame" >
<div id="listbody" >
	<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$pageName?></span>
		 	
		 	<?php 
		 		if($hasEditPermission){
		 			echo " -> Manage Category";
		 		}else{
					echo " -> View Category";
		 		}
		 	
		 	?>
		 	
		</div>
   		
    <table  id="listTable" class="formline" border="0" cellpadding="3" cellspacing="1" width="100%" style="margin-top:30px">
        <tr>
            <th width="100%" bgcolor="#618dc5" style="font-family:Verdana, Arial, Helvetica, sans-serif; color:#FFFFFF; font-size:12px; font-weight:bold;">Name</th>
            <th colspan="4" bgcolor="#618dc5" style="font-family:Verdana, Arial, Helvetica, sans-serif; color:#FFFFFF; font-size:12px; font-weight:bold;" align="center">Control</th>
        </tr>
    <?php
	$defaultNodeId = array();
	$disableAddFX_NodeId = array();
	$disableNodeLevel = array();
    $counter = 1;
    while ($item = $dbtree->NextRow()) {



        if ($counter % 2) {
            $class = 'row1';
        } else {
            $class = 'row2';
        }
        
        
        ?>
        <tr class="rowContainer">
            <td class="<?=$class?>" style="padding:18px;">
                <?=str_repeat('&nbsp;', 6 * $item['node_level']) . '<strong>' . html_entity_decode($item['node_name'])?></strong>
            </td>
            <td class="<?=$class?>"><input type="button" name="btnAdd" value="Add" class="btnItem" onClick="this.disabled=true;window.location='<?=$_SERVER['PHP_SELF']?>?action=add&node_id=<?=$item['node_id']?>'" 
        		<?= ( $hasEditPermission && !in_array($item['node_level'], !empty($addLevelDisabled)?$addLevelDisabled : array()) && $item['node_level'] < $level - 1 && !in_array($item['node_id'], $disableAddFX_NodeId)  ) ? "" : "disabled='disabled'"?>/></td>
        		
            <td class="<?=$class?>"><input type="button" name="btnEdit" value="Edit" class="btnItem" onClick="this.disabled=true;window.location='<?=EDIT_PAGE?>?action=edit&id=<?=$item['node_id']?>'"  
            	<?= !$hasEditPermission || (in_array($item['node_level'], !empty($editLevelDisabled)?$editLevelDisabled:array()) )|| ( $item['node_level'] == 0 || in_array($item['node_id'], $defaultNodeId) || in_array($item['node_level'], $disableNodeLevel)) ? "disabled='disabled'" : ""?>/></td>
            <td class="<?=$class?>">
            		
            <?php
            if (!$hasEditPermission || 0 == $item['node_level'] || in_array($item['node_id'], $defaultNodeId) || in_array($item['node_level'], $disableNodeLevel)) {
				echo "<input type=\"button\" name=\"btnDelete\" class=\"btnItem\" value=\"Delete\" disabled=\"disabled\">";
            } else {
				echo "<input type=\"button\" name=\"btnDelete\" value=\"Delete\" class=\"btnItem\" onClick=\"goDel(this, '".$item['node_id']."')\">";
            }
            ?>
            
            </td>
            <td class="<?=$class?>">
            
            <?php
            if (!$hasEditPermission || 0 == $item['node_level'] || in_array($item['node_id'], $defaultNodeId) || in_array($item['node_level'], $disableNodeLevel)) {
				echo "<input type=\"button\" name=\"btnMove\" class=\"btnItem\" value=\"Move\" disabled=\"disabled\">";
            } else {
				echo "<input type=\"button\" name=\"btnMove\" value=\"Move\" class=\"btnItem\" onClick=\"this.disabled=true;window.location='".$_SERVER['PHP_SELF']."?action=move&node_id=".$item['node_id']."'\">";
            }
            ?>

            </td>
        </tr>
        <?php
        $counter++;
    }

    ?>
		<tr>
			<td colspan="5" align="left">
				<a href="./list.php"><input type="button" name="btnReset" value="Back to List Page"  class="btnItem" style="margin:0px 8px"/></a>
			</td>
		</tr>
    </table>
    <!--  end -->
    
	  <br>
      <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>

</div>
</div>

</div>
</body>
</html>
<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>