<?php
$pageName = "User Account";
$tableName = "account";
$keyField = "id";
$level = "1";
$perPage = 20;
$pageLinks = 10;
define("PARENT_NODE_SELECT", FALSE);
define("MULTI_CATEGORY", FALSE);
define('DB_CACHE', FALSE);
define("DB_TABLE", $tableName."_node");
define("EXPORT_TO_EXCEL", TRUE);
define("ORDER_BY_VALUE", " `id` asc");

//edit page - error message
define("DEFAULT_ERROR_MSG", "Please enter a valid value.");

//items category 
$addLevelDisabled = array();
$editLevelDisabled = array();
define("EDIT_PAGE", "edit_node.php");

//user permission
define("CREATE_PERMISSION", "ACCOUNT_CREATE");
define("VIEW_PERMISSION", "");
define("EDIT_PERMISSION", "ACCOUNT_EDIT");
define("DELETE_PERMISSION", "ACCOUNT_DELETE");
define("EXPORT_PERMISSION", "");
if( !isset( $_SESSION ) ){
	session_start();
}
$hasCreatePermission = (CREATE_PERMISSION==""  || CREATE_PERMISSION!="" && $_SESSION[$dbDatabase][CREATE_PERMISSION]) ? true:false;
$hasViewPermission = (VIEW_PERMISSION==""  || VIEW_PERMISSION!="" && $_SESSION[$dbDatabase][VIEW_PERMISSION]) ? true:false;
$hasEditPermission = (EDIT_PERMISSION==""  || EDIT_PERMISSION!="" && $_SESSION[$dbDatabase][EDIT_PERMISSION]) ? true:false;
$hasDeletePermission = (DELETE_PERMISSION==""  || DELETE_PERMISSION!="" && $_SESSION[$dbDatabase][DELETE_PERMISSION]) ? true:false;
$hasExportPermission = (EXPORT_PERMISSION==""  || EXPORT_PERMISSION!="" && $_SESSION[$dbDatabase][EXPORT_PERMISSION]) ? true:false;

//list & edit page button 
define("INVENTORY_NEED", FALSE);
define("INVENTORY_VALUE", "Image");
define("NEW_NEED", $hasCreatePermission);
define("EDIT_NEED", $hasEditPermission);
define("DELETE_NEED", $hasDeletePermission);
define("EXPORT_NEED", $hasExportPermission);

//language
define("SHOW_LANG_BTN", FALSE);

$fixedField = array (
	# array( Name, DB Field Name, type, array(list status, width, sortable, searchable), array(edit form status, update, insert, default selection/value))

	array( "ID", "id", "INT", array("HIDDEN", "40", false, false), array("HIDDEN", false, false, array()) ),
	array( "Username", "username", "VARCHAR", array("SHOW", "300", true, true), array("SHOW", true, true, array()) ),
	array( "Password", "password", "PASSWORD_MD5", array("HIDDEN", "0", true, true), array("SHOW", true, true, array()), "tips"=>"Leave it blank if the password is unchanged"),
	array( "Role", "role_id", "FOREIGN", array("SHOW", "300", true, true), array("SHOW", true, true, array("role", "id", "name" )) ),
		
	array( "Create Date", "createDate", "VARCHAR", array("HIDDEN", "100", true, false), array("HIDDEN", false, true, array()) ),
	array( "Last Updated", "lastModDate", "DATETIME", array("SHOW", "180", true, false), array("HIDDEN", true, true, array()) ), 
);

$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>