<?php
ob_start();
require_once("../../config.inc.php"); # common config
require_once("inv_config.inc.php"); # config
require_once($rootPath . $adminPath."/config.inc.php"); # admin config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.pageiterator.php");
header('Cache-Control: no-cache');
header('Pragma: no-cache');
if( !isset( $_SESSION ) ) { ob_start(); session_start(); ob_end_clean(); }

$prodId = array_key_exists($tableName."_prodId", $_SESSION) ? $_SESSION[$tableName."_prodId"] : "";
if(!function_exists("getProdInfo")) {
	function getProdInfo($id, $requestField) {
		global $conn, $parentTable;
		
		$thissql = "SELECT * FROM `".$parentTable."` WHERE `id`='".$id."' LIMIT 0, 1";
		$rs = $conn->Execute($thissql);
		$recordFound = ($rs->RecordCount() > 0) ? true : false;
		if ($recordFound) {
			return (empty($requestField)) ? $rs->fields : $rs->fields[$requestField];
		} else return false;
	}
}

// Set global variables if not already set
$node_id = "";
$refresh = (array_key_exists("refresh", $_GET)) ? $_GET['refresh'] : "";
if($refresh == "1") echo "<script language=\"javascript\">window.location.replace('".$_SERVER['PHP_SELF']."');</script>";
$arrVar = $conn->MetaColumns($tableName);
$arrVar = $conn->MetaColumnNames($tableName);
foreach ($arrVar as $var) {
	$$var = "";
}
if ($level == 0)
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : 1;
else
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : "";


if($id != "") {
	$thissql = "SELECT * FROM `".$tableName."` WHERE `".$keyField."`='".$id."'";
	$rs = $conn->SelectLimit($thissql, 1, 0);
	$totalRecord = $rs->RecordCount();
	if($totalRecord > 0) {
		while (!$rs->EOF) {	
			foreach ($rs->fields as $key=>$value) {
				$$key = $value;
			}					
			$rs->MoveNext();
		}
	}
}

function hasChild($node_id) {
	global $conn;
	
	$thissql = "SELECT n2.* FROM `".DB_TABLE."` n1, `".DB_TABLE."` n2 WHERE n1.`node_id`='".$node_id."' AND n1.`node_left`<n2.`node_left` AND n1.`node_right`>n2.`node_right`";
	$rs = $conn->Execute($thissql);
	return ($rs->RecordCount() > 0);
}

$node_id = (array_key_exists("node_id", $_GET)) ? $_GET['node_id'] : $node_id;

$printForm = "";
$printHidden = "";
$printCreateFCKScript = "var fcklist = new Array(";
$printCreateCalendarScript = "";
foreach ( $fixedField as $eachColumn ) {
	if ( $eachColumn[4][0] != "HIDDEN" ) {
		if($eachColumn[2] == "CUSTOM")
			$printForm .= "<tr><td style=\"".$eachColumn[4][3][0]."\" colspan=\"2\">".$eachColumn[4][3][1]."</td>";
		else
			$printForm .= '<tr><th width="150" style="text-align:left; vertical-align:top; padding-top:2px; padding-left:2px;">'.$eachColumn[0].': </th>';
		switch ($eachColumn[2]) {
		case "CATEGORY":
		
			require_once($pathClassLib.'/dbtree.class.php');		
			// Create new object
			$dbtree = new dbtree(DB_TABLE, 'node', $conn);		
			$dbtree->Full(array('node_id', 'node_level', 'node_name'));
			
			$printForm .= "<td><select id=\"".$eachColumn[1]."\" name=\"".$eachColumn[1]."\" style=\"width:300px;\">";	
	
			while ($item = $dbtree->NextRow()) {
				if($item['node_level'] > 0) {
					$selected = ($$eachColumn[1] == $item['node_id']) ? "selected" : "";
					$arrow = ($$eachColumn[1] == $item['node_id']) ? "&lt;&lt;&lt;&lt;" : "";
					if (hasChild($item['node_id']) && !PARENT_NODE_SELECT)
						$printForm .= "<optgroup LABEL=\" ".str_repeat('&nbsp;', 6 * $item['node_level']).html_entity_decode($item['node_name'])." ".$arrow." \"> ".str_repeat('&nbsp;', 6 * $item['node_level']).html_entity_decode($item['node_name'])." ".$arrow." </optgroup>\n";
					else
						$printForm .= "<option value=\"".$item['node_id']."\" ".$selected."> ".str_repeat('&nbsp;', 6 * $item['node_level']).html_entity_decode($item['node_name'])." ".$arrow." </option>\n";
				}
			}
			$printForm .= "</select>";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
			
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
			
			$printForm .= '</td>';
			
			break;
		case "CATEGORY_MULTI":
		
			require_once($pathClassLib.'/dbtree.class.php');		
			$printForm .= "<td><select id=\"".$eachColumn[1]."[]\" name=\"".$eachColumn[1]."[]\" style=\"width:300px;height:250px;\" multiple><option value=\"1\"> --請選擇-- </option>";	
			$tmpArr = split(";", $$eachColumn[1]);
			// Create new object
			$dbtree = new dbtree(DB_TABLE, 'node', $conn);		
			$dbtree->Full(array('node_id', 'node_level', 'node_name'));	
			while ($item = $dbtree->NextRow()) {
				if($item['node_level'] > 0) {
					$selected = (in_array_multi($item['node_id'],$tmpArr)) ? "selected" : "";
					$arrow = (in_array_multi($item['node_id'],$tmpArr)) ? "&lt;&lt;&lt;&lt;" : "";
					if (hasChild($item['node_id']) && !PARENT_NODE_SELECT)
						$printForm .= "<optgroup LABEL=\" ".str_repeat('&nbsp;', 6 * $item['node_level']).html_entity_decode($item['node_name'])." ".$arrow." \"> ".str_repeat('&nbsp;', 6 * $item['node_level']).html_entity_decode($item['node_name'])." ".$arrow." </optgroup>\n";
					else
						$printForm .= "<option value=\"".$item['node_id']."\" ".$selected."> ".str_repeat('&nbsp;', 6 * $item['node_level']).html_entity_decode($item['node_name'])." ".$arrow." </option>\n";
				}
			}
			
			$printForm .= "</select>";


			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			break;
		case "FOREIGN":
			$printForm .= "<td><select id=\"".$eachColumn[1]."\" name=\"".$eachColumn[1]."\" style=\"width:300px;\"><option value=\"\"> --Please Select-- </option>";
			$thissql = "SELECT `".$eachColumn[4][3][1]."`, `".$eachColumn[4][3][2]."` FROM `".$eachColumn[4][3][0]."` ORDER BY `".$eachColumn[4][3][2]."`";
			$rs = $conn->Execute($thissql);
			$recordFound = ($rs->RecordCount() > 0) ? true : false;
			while (!$rs->EOF && $recordFound) {
				$selected = ($$eachColumn[1] == $rs->fields[$eachColumn[4][3][1]]) ? "selected" : "";
				$printForm .= "<option value=\"".$rs->fields[$eachColumn[4][3][1]]."\" ".$selected."> ".html_entity_decode($rs->fields[$eachColumn[4][3][2]])." </option>";
				$rs->MoveNext();
			}
			$printForm .= "</select>";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			break;
		case "FOREIGN2":
			$printForm .= "<td><select id=\"".$eachColumn[1]."\" name=\"".$eachColumn[1]."\" ><option value=\"\"> --Please Select-- </option>";
			$thissql = "SELECT `".$eachColumn[4][3][0]."`.`".$eachColumn[4][3][1]."`, `".$eachColumn[4][3][3]."`.`".$eachColumn[4][3][5]."`".(($eachColumn[4][3][6]!="")?", `".$eachColumn[4][3][6]."`":"")." FROM `".$eachColumn[4][3][0]."`, `".$eachColumn[4][3][3]."` WHERE `".$eachColumn[4][3][0]."`.`".$eachColumn[4][3][2]."`=`".$eachColumn[4][3][3]."`.`".$eachColumn[4][3][4]."` ORDER BY `".$eachColumn[4][3][3]."`.`".$eachColumn[4][3][5]."`";
			$rs = $conn->Execute($thissql);
			$recordFound = ($rs->RecordCount() > 0) ? true : false;
			while (!$rs->EOF && $recordFound) {
				$selected = ($$eachColumn[1] == $rs->fields[$eachColumn[4][3][1]]) ? "selected" : "";
				$printForm .= "<option value=\"".$rs->fields[$eachColumn[4][3][1]]."\" ".$selected."> ".html_entity_decode($rs->fields[$eachColumn[4][3][5]])." -> ".html_entity_decode($rs->fields[$eachColumn[4][3][6]])." </option>";
				$rs->MoveNext();
			}
			$printForm .= "</select>";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			
			break;
		case "FOREIGN_MULTI":
			$printForm .= "<td><select id=\"".$eachColumn[1]."[]\" name=\"".$eachColumn[1]."[]\" style=\"width:300px; height:100px;\" multiple>";
			$tmpArr = split(";", $$eachColumn[1]);
			$thissql = "SELECT `".$eachColumn[4][3][1]."`, `".$eachColumn[4][3][2]."` FROM `".$eachColumn[4][3][0]."` ORDER BY `".$eachColumn[4][3][2]."`";
			$rs = $conn->Execute($thissql);
			$recordFound = ($rs->RecordCount() > 0) ? true : false;
			while (!$rs->EOF && $recordFound) {
				$selected = (in_array_multi($rs->fields[$eachColumn[4][3][1]],$tmpArr)) ? "selected" : "";
				$printForm .= "<option value=\"".$rs->fields[$eachColumn[4][3][1]]."\" ".$selected."> ".html_entity_decode($rs->fields[$eachColumn[4][3][2]])." </option>";
				$rs->MoveNext();
			}
			$printForm .= "</select>";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			break;
		case "READONLY":
			if ($id != ""){
					$printForm .= '<td><input type="text" id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" value="'.$$eachColumn[1].'" style="width:350px" readonly />';
					

					if(!empty($eachColumn["error"])){
						$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
					}else{
						$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
					}
						
					if(!empty($eachColumn["tips"])){
						$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
					}
						
					$printForm .= '</td>';
						
			}else{
				$printForm .= '<td><input type="text" id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" value="'.$$eachColumn[1].'" style="width:350px" />';

				if(!empty($eachColumn["error"])){
					$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
				}else{
					$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
				}
					
				if(!empty($eachColumn["tips"])){
					$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
				}
					
				$printForm .= '</td>';
					
			}
			break;
		case "INT":
		case "VARCHAR":
		case "HYPERLINK":
			$printForm .= '<td><input type="text" id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" value="'.str_replace('"', "''", html_entity_decode($$eachColumn[1])).'" style="width:350px" />';
			

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
			
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
			
			$printForm .= '</td>';
			
			break;
		case "FILE":
			$printForm .= '<td><input type="file" id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" />';
			if ($$eachColumn[1] != "") {
				/*$newsize = imgVirtualResize ($pathDocRoot, $$eachColumn[1], 80, 60);
				if($newsize != 0)
					$printimg = "<img src=\"".$$eachColumn[1]."\" border=\"0\" width=\"".$newsize[0]."\" height=\"".$newsize[1]."\" align=\"left\" alt=\"".$$eachColumn[1]."\" hspace=\"5\" />";
				else $printimg = "";*/
				$printForm .= "<br /><a href=\"".$appPath.$uploadedFileDir."/".$$eachColumn[1]."\" target=\"_blank\">".$appPath.$uploadedFileDir."/".$$eachColumn[1]."</a> <input type=\"hidden\" name=\"filename_".$eachColumn[1]."\" value=\"".$$eachColumn[1]."\" /><input type=\"checkbox\" name=\"fileDel_".$eachColumn[1]."\" value=\"".$$eachColumn[1]."\" /> Delete";
			}
			
			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
			
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
			
			
			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
			
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
			
			$printForm .= '</td>';
			
			
			break;
		case "PHOTO":
		case "THUMBNAIL":
			$printForm .= '<td><input type="file" id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" />';
			if ($$eachColumn[1] != "") {
				$path = $appPath.$uploadedFileDir."/";
				$filename = $$eachColumn[1];
				$newsize = imgVirtualResize ($path, $filename, 80, 60);	
				if($newsize != 0)
					$printimg = "<img src=\"".$path.$filename."\" border=\"0\" width=\"".$newsize[0]."\" height=\"".$newsize[1]."\" align=\"left\" alt=\"".$$eachColumn[1]."\" hspace=\"5\" />";
				else $printimg = "";
				$printForm .= "<br /><a href=\"".$appPath.$uploadedFileDir."/".$$eachColumn[1]."\" target=\"_blank\">".$printimg."</a> <input type=\"hidden\" name=\"filename_".$eachColumn[1]."\" value=\"".$$eachColumn[1]."\" /><input type=\"checkbox\" name=\"fileDel_".$eachColumn[1]."\" value=\"".$$eachColumn[1]."\" /> Delete";
			}
			
			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
			
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
			
			$printForm .= '</td>';
			
			break;
		case "DATE":
			$thisValue = ($$eachColumn[1] != "") ? $$eachColumn[1] : date("Y-m-d");
			$printForm .= "<td><input id=\"".$eachColumn[1]."\" type=\"text\" size=\"12\" onBlur=\"if (! checkdate(this) ) {this.value = '';this.select();}\" value=\"".$thisValue."\" name=\"".$eachColumn[1]."\" /> <img id=\"btn".$eachColumn[1]."\" title=\"Date Selector\" src=\"".$adminPath."/images/calendar.gif\" style=\"cursor:pointer;vertical-align:text-bottom;\" />";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			$printCreateCalendarScript .= "Calendar.setup({inputField:\"".$eachColumn[1]."\", button:\"btn".$eachColumn[1]."\", ifFormat:\"%Y-%m-%d\"});\n";
			break;
		case "DATETIME":
			//$printForm .= '<td><input type="hidden" id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" value="'.$$eachColumn[1].'" /></td>';
			$thisValue = ($$eachColumn[1] != "") ? $$eachColumn[1] : date("Y-m-d H:i:s");
			$printForm .= "<td><input id=\"".$eachColumn[1]."\" type=\"text\" size=\"18\" onBlur=\"if (! checkdate(this) ) {this.value = '';this.select();}\" value=\"".$thisValue."\" name=\"".$eachColumn[1]."\" /> <img id=\"btn".$eachColumn[1]."\" title=\"Date Selector\" src=\"".$adminPath."/images/calendar.gif\" style=\"cursor:pointer;vertical-align:text-bottom;\" />";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			$printCreateCalendarScript .= "Calendar.setup({inputField:\"".$eachColumn[1]."\", button:\"btn".$eachColumn[1]."\", ifFormat:\"%Y-%m-%d %H:%M:00\", showsTime:\"true\"});\n";
			break;
		case "TEXT":
			$printForm .= '<td><textarea id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" style="width:350px; height:200px">'.$$eachColumn[1].'</textarea></td>';
			//$printCreateFCKScript .= "var oFCKeditor1 = new FCKeditor( '".$eachColumn[1]."', 470, 200 );\n";
			$printCreateFCKScript .= ($printCreateFCKScript == "var fcklist = new Array(" ) ? "'".$eachColumn[1]."'" : ", '".$eachColumn[1]."'";
			break;
		case "TEXTAREA":
			$printForm .= '<td><textarea id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" style="width:350px; height:200px">'.str_replace(array("\'", '\"'), array("'", '"'), $$eachColumn[1]).'</textarea>';

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			break;
		case "SELECT":
			$printForm .= "<td><select id=\"".$eachColumn[1]."\" name=\"".$eachColumn[1]."\" style=\"width:350px; height:100px\">";
			foreach($eachColumn[4][3] as $eachDefaultName=>$eachDefaultValue) {
				$mySelected = ($eachDefaultValue == $$eachColumn[1]) ? "selected" : "";
				$printForm .= "<option value=\"".$eachDefaultValue."\" ".$mySelected."> ".$eachDefaultName." </option>";
			}
			$printForm .= "</select>";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			break;
		case "MULTISELECT":
			$printForm .= "<td><select id=\"".$eachColumn[1]."[]\" name=\"".$eachColumn[1]."[]\" multiple style=\"width:350px; height:100px\">";
			$tmpArr = split(";", $$eachColumn[1]);
			foreach($eachColumn[4][3] as $eachDefaultName=>$eachDefaultValue) {
				$mySelected = (in_array_multi($eachDefaultValue,$tmpArr)) ? "selected" : "";
				$printForm .= "<option value=\"".$eachDefaultValue."\" ".$mySelected."> ".$eachDefaultName." </option>";
			}
			$printForm .= "</select>";

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
				
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
				
			$printForm .= '</td>';
				
			break;
		case "CHECKBOX":
			$printForm .= "<td>";
			$tmpArr = split(";", $$eachColumn[1]);
			foreach($eachColumn[4][3] as $eachDefaultName=>$eachDefaultValue) {
				$mySelected = (in_array_multi($eachDefaultName,$tmpArr)) ? "checked" : "";
				$printForm .= "<input type=\"checkbox\" id=\"".$eachColumn[1]."[]\" name=\"".$eachColumn[1]."[]\" value=\"".$eachDefaultValue."\" ".$mySelected."> ".getDBContent($eachDefaultName)." ";
			}
			
			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
			
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
			
			$printForm .= '</td>';
			
			break;
		case "RADIO":
			$printForm .= "<td>";
			$i = 0;
			foreach($eachColumn[4][3] as $eachDefaultName=>$eachDefaultValue) {
				$mySelected = ($eachDefaultValue == $$eachColumn[1]) ? "checked" : ($i == 0) ? "checked" : "";
				$printForm .= getDBContent($eachDefaultName)."<input type=\"radio\" name=\"".$eachColumn[1]."\" value=\"".$eachDefaultValue."\" ".$mySelected." style=\"border:0\" />";
				$i++;
			}

			if(!empty($eachColumn["error"])){
				$printForm .= '<div class="errorContainer">'.$eachColumn["error"].'</div>';
			}else{
				$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
			}
			
			if(!empty($eachColumn["tips"])){
				$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn["tips"].'</span>';
			}
			
			$printForm .= '</td>';

			break;
		}
		$printForm .= "</tr>\n";
	} else {
		$printHidden .= '<input type="hidden" id="'.$eachColumn[1].'" name="'.$eachColumn[1].'" value="'.$$eachColumn[1].'" />';
	}
}
$printCreateFCKScript .= ");\n";
?>
<?php require_once( $rootPath . $adminPath."/framework/overall_header.php" );?>
<link rel="stylesheet" href="<?=$adminPath?>/css/slide.css" type="text/css">
<script type="text/javascript" src="<?=$adminPath?>/js/core.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/events.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/css.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/coordinates.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/drag.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/dragsort.js"></script>
<script type='text/javascript' src='<?=$adminPath?>/js/timepicker.js'></script>
<script type="text/javascript" src="<?=$adminPath?>/js/calendar.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/calendar-en.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/calendar-setup.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckeditor.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckfinder/ckfinder.js"></script>
<link href="<?=$adminPath?>/css/calendar-tas.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
<!--
var sBasePath = "<?=$fckEditorPath?>/";
<?=$printCreateFCKScript?>

function checkdatetime(input) {
	var validformat=/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/; //Basic check for format validity
	var returnval=false;
	if(input.value != "") {
		if (!validformat.test(input.value))
			alert("Invalid Date Format. Please correct and submit again.");
		else{ //Detailed check for valid date ranges
			var dateArr = input.value.split(" ")[0];
			var yearfield=dateArr.split("-")[0];
			var monthfield=dateArr.split("-")[1];
			var dayfield=dateArr.split("-")[2];
			var dayobj = new Date(yearfield, monthfield-1, dayfield);
			if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
				alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
			else
				returnval=true;
		}
		return returnval;
	} else {
		return true;
	}
}
		 
function checkdate(input) {
	var validformat=/^\d{4}\-\d{2}\-\d{2}$/; //Basic check for format validity
	var returnval=false;
	if(input.value != "") {
		if (!validformat.test(input.value))
			alert("Invalid Date Format. Please correct and submit again.");
		else{ //Detailed check for valid date ranges
			var yearfield=input.value.split("-")[0];
			var monthfield=input.value.split("-")[1];
			var dayfield=input.value.split("-")[2];
			var dayobj = new Date(yearfield, monthfield-1, dayfield);
			if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
				alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
			else
				returnval=true;
		}
		return returnval;
	} else {
		return true;
	}
}

function getFckEditorTextLength(fckName) {
	// This functions shows that you can interact directly with the editor area
	// DOM. In this way you have the freedom to do anything you want with it.
	
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance(fckName) ;

	// Get the Editor Area DOM (Document object).
	var oDOM = oEditor.EditorDocument ;

	var iLength ;

	// The are two diffent ways to get the text (without HTML markups).
	// It is browser specific.

	if ( document.all ) { // If Internet Explorer.
		var innerText = oDOM.body.innerText;
		iLength = innerText.length ;
	} else {// If Gecko.
		var r = oDOM.createRange() ;
		r.selectNodeContents( oDOM.body ) ;
		iLength = r.toString().length ;
	}

	return iLength;
}

function setEditorValue( instanceName, text )
{  
  /* Old Version Coding
  // Get the editor instance that we want to interact with.
  //var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
  
  // Set the editor contents.
  //oEditor.SetHTML( text ) ;
  */
  CKEDITOR.instances[instanceName].setData(text);
} 

function getEditorValue( instanceName ) 
{  
  /* Old Version Coding
  // Get the editor instance that we want to interact with.
  //var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
  
  // Get the editor contents as XHTML.
  //return oEditor.GetXHTML( true ) ;  // "true" means you want it formatted.
  */
  CKEDITOR.instances[instanceName].getData();
}

function goUpdate(myForm) {
	//var name = myForm.name.value;
	//var description = getEditorValue("desc");
	var errorFlag = false;
	var errorMsg = "";
	/*	
	if (name == "") {
		errorMsg += "Please input the name\n";
		errorFlag = true;
	}
	if (description == "") {
		errorMsg += "Please input the description\n";
		errorFlag = true;
	}
	*/
	if(errorFlag) {
		alert(errorMsg);
		myForm.btnUpdate.disabled = false;
		return false;
	} else {
		myForm.method = "post";
		myForm.action = "inv_todb.php";
		myForm.submit();
	}
}

function resetForm( myForm ) {
	myForm.reset();
	/*document.getElementById("calendar-container").innerHTML = "";
	cal = new Zapatec.Calendar.setup({
		date		: document.getElementById("newsDate").value.replace(/-/g, "/"),
		flat         : "calendar-container", // ID of the parent element
		flatCallback : dateChanged,          // our callback function
		dateStatusFunc : function(date, y, m, d) {
		if (dateIsSpecial(y, m, d)) return "zpCalSpecialDay";
		else return false; // other dates are enabled
		// return true if you want to disable other dates
	}
	});*/
	for (var i = 0; i < fcklist.length; i++) {
		/*
		Used For Old Version
		//setEditorValue(fcklist[i], document.getElementById(fcklist[i]).value);
		*/
		CKEDITOR.instances[fcklist[i]].setData(document.getElementById(fcklist[i]).value);
	}
}

window.onload = function() {
	for (var i = 0; i < fcklist.length; i++) {
		eval("var oFCKeditor" + (i+1) + " = CKEDITOR.replace('"+fcklist[i]+"')");	
		eval("CKFinder.setupCKEditor(oFCKeditor" + (i+1) + ", '<?=$fckEditorPath?>/ckfinder/' )");
	}	
}
-->
</script>
<SCRIPT language="javascript">
function addbr() { 
    //Create an input type dynamically.
	var fxloat = document.createElement("div"); 
	fxloat.style.width = '640px';	
	fxloat.style.clear = 'both';	
	return fxloat;
}

function golang(lang) {
	if (confirm('Any unsaved data will be lost. Are you sure?')) {
		window.location = '?id=<?=$id?>&lang='+lang;
	}
}

function add2(target, obj, css, attri){
	var obj = $(target).find('div').eq(0).clone().css('background-color','#CCCCFF');
	obj.find('input').val('');
	$(target).append(obj);
}

function add(fx, name,type,value) { 
    //Create an input type dynamically.
	var element = document.createElement("input"); 
	
    //Assign different attributes to the element.
    element.setAttribute("type", type);
    element.setAttribute("value", value);
    element.setAttribute("name", name);
	element.setAttribute("id", name);	
 	element.style.cssFloat = 'left';
	element.style.width = '127px';	
	//element.style.marginRight = '19px';	
	
    fx.appendChild(element);
}

function removeThis(obj) {
	if (confirm('Are you sure to remove this row?')) {
		obj.parent().remove();
	}
}
</SCRIPT>
<body>
<div id="body" style=" width:100%;height:100%;overflow: hidden;">

<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" >
<div id="listbody">
	<div style="">
		<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$pageName?></span>
		 	 -> <?=($id=="") ? "Add" : "Edit"?>
		</div>
   		
		<table id="editTable" >
		  <tbody>
		    <tr>
		    <td>
		      <form action="" method="post" id="myform" enctype="multipart/form-data">
		      <div><input type="hidden" value="ok" name="refresh" /><?=$printHidden?></div>
		      <div style="padding:24px 6px;">
		  	
				  <?
						if(SHOW_LANG_BTN){					 	 
					 		foreach ($_version as $k=>$v) {
			        ?>		
				    		<div style="padding:24px 6px;">
			        			<a class="langbtn btnItem <?=($v == $currversion) ? "curr" : ""?>" onClick="golang('<?=$v?>')">Switch to <?=$k?></a>
					        </div>
			        <?
					  		}
					  	}
			        ?>
			  	
	        </div>
		      <table class="formline_edit" cellspacing="0" cellpadding="6" width="100%" style="padding-bottom:5px;">
		        <tbody>
				<?=$printForm?>
		        <?php if ($level > 0) { ?>
				<tr>
					<td colspan="3" align="center"></td>
				</tr>
		        <?php } ?>
			  </tbody></table>
		
			  	<div style="float:left">
						<a href="./list.php?node_id=<?=$node_id?>"  class="btnItem" style="margin:0px 8px">Back to List Page</a>
					</div>
				 	<div style="float:right">
						<input type="button" name="btnReset" value=" Reset "  class="btnItem" onClick="resetForm(this.form)" style="margin:0px 8px"/>
				  		<input type="button" id="btnUpdate" name="btnUpdate" value=" <?=($id=="") ? "Add" : "Update"?> " class="btnItem" onClick="this.disabled=true;goUpdate(this.form)"  style="padding-left:32px;padding-right:32px;margin:0px 8px;"/> 
					</div>	
			  </form>
		</td></tr></tbody>
		
		</table>
	
	      <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>
	</div></div>
</div>
</div>


</body>
</html>
<script type="text/javascript">
<!--
<?=$printCreateCalendarScript?>	
-->

	$(function(){

		$(".formline_edit td").on("click", function(){
			$(this).find("input, select").focus();
		});
	});


</script>
<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>