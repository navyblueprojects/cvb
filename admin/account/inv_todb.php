<?php
ob_start();
include_once("../../config.inc.php"); # common config
require_once($rootPath . $adminPath."/config.inc.php");
require_once("inv_config.inc.php"); # config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/class.uploadfile.php");
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.file.php");
header('Cache-Control: no-cache');
header('Pragma: no-cache');
if( !isset( $_SESSION ) ) { ob_start(); session_start(); ob_end_clean(); }

$prodId = array_key_exists($tableName."_prodId", $_SESSION) ? $_SESSION[$tableName."_prodId"] : "";

$fileUpload = new file_upload;
$fileUpload->upload_dir = $uploadedFilePath . "/"; // "files" is the folder for the uploaded files (you have to create this folder)
//$fileUpload->extensions = array(".png", ".zip", ".pdf", ".jpg"); // specify the allowed extensions here
$fileUpload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
$fileUpload->rename_file = true;

foreach ( $fixedField as $eachColumn ) {
	if ( $eachColumn[4][0] != "HIDDEN" ) {	
		switch ($eachColumn[2]) {
		case "FILE":
		case "PHOTO":
			$$eachColumn[1] = (array_key_exists($eachColumn[1], $_FILES)) ? $_FILES[$eachColumn[1]] : "";
			$varName = "fileDel_".$eachColumn[1];
			$$varName = (array_key_exists($varName, $_POST)) ? $_POST[$varName] : "";
			break;
		case "CATEGORY_MULTI":
		case "FOREIGN_MULTI":
		case "MULTISELECT":
		case "CHECKBOX":
			$$eachColumn[1] = $_POST[$eachColumn[1]];
			break;
		default:
			$value = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $_POST[$eachColumn[1]]);
			$$eachColumn[1] = array_key_exists($eachColumn[1], $_POST) ? htmlspecialchars($value, ENT_QUOTES) : "";
			break;
		}
	} else {
		$value = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $_POST[$eachColumn[1]]);
		$$eachColumn[1] = array_key_exists($eachColumn[1], $_POST) ? htmlspecialchars($value, ENT_QUOTES) : "";
	}
}
$thissql = "SELECT `".$keyField."` FROM `".$tableName."` WHERE 1 AND `".$keyField."`='".$$keyField."'";
$rs = $conn->Execute($thissql);
$recordFound = ($rs->RecordCount() > 0) ? true : false;

$conn->debug = true;
if (!$recordFound) {
	$thumbnail = "";
	foreach ( $fixedField as $eachColumn )
		if ( $eachColumn[4][2] ) {
			switch ($eachColumn[2]) {
			case "FILE":
				$fileUpload->the_temp_file = "";
				$fileUpload->the_file = "";
				$fileUpload->http_error = "";
				$fileUpload->file_copy = "";
				foreach ( $_FILES[$eachColumn[1]] as $varName => $varValue ) {
					if ($varName == "tmp_name")
						$fileUpload->the_temp_file = $varValue;
					if ($varName == "name")
						$fileUpload->the_file = $varValue;
					if ($varName == "error")
						$fileUpload->http_error = $varValue;
				}
				$fileUpload->replace = "n";
				$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
				$fileUpload->upload();
				$fields[$eachColumn[1]] = $fileUpload->file_copy;				
				break;
			case "PHOTO":
			case "THUMBNAIL":
				$fileUpload->the_temp_file = "";
				$fileUpload->the_file = "";
				$fileUpload->http_error = "";
				$fileUpload->file_copy = "";
				foreach ( $_FILES[$eachColumn[1]] as $varName => $varValue ) {
					if ($varName == "tmp_name")
						$fileUpload->the_temp_file = $varValue;
					if ($varName == "name")
						$fileUpload->the_file = $varValue;
					if ($varName == "error")
						$fileUpload->http_error = $varValue;
				}
				$fileUpload->replace = "n";
				$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
				$fileUpload->upload();
				$fields[$eachColumn[1]] = $fileUpload->file_copy;
				if (!empty($eachColumn[4][3])) {
					imgActualResize($uploadedFilePath,$fields[$eachColumn[1]],$uploadedFilePath,$fields[$eachColumn[1]],$eachColumn[4][3][0],$eachColumn[4][3][1]);
					if (!empty($eachColumn[4][3][2])) {
						if (!empty($eachColumn[4][3][3])) {
							addWaterMark($uploadedFilePath."/".$fields[$eachColumn[1]], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0, 25, $eachColumn[4][3][3]);
						} else
							addWaterMark($uploadedFilePath."/".$fields[$eachColumn[1]], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0);
					}
				}
				break;
			case "CATEGORY_MULTI":
			case "FOREIGN_MULTI":
			case "MULTISELECT":
			case "CHECKBOX":
				$tmpStr = "";
				foreach($$eachColumn[1] as $eachValue)
					if(trim($eachValue) != "")
						$tmpStr .= $eachValue.";";
				$fields[$eachColumn[1]] = ($tmpStr!="") ? $tmpStr : "1;";
				break;
			default:
				$fields[$eachColumn[1]] = $$eachColumn[1];
				break;
			}
		}
	$fields[$foreignKey] = $prodId;
	$fields['createDate'] = date("Y-m-d H:i:s");
	$fields['lastModDate'] = date("Y-m-d H:i:s");
	$conn->AutoExecute("`".$tableName."`", $fields, 'INSERT');
		
} elseif ($$keyField != "") {
	foreach ( $fixedField as $eachColumn )
		if ( $eachColumn[4][1] ) {
			switch ($eachColumn[2]) {
			case "FILE":
				$varName = "fileDel_".$eachColumn[1];
				$varFileName = "filename_".$eachColumn[1];
				if($$varName != "") {
					@unlink($uploadedFilePath."/".$$varName);
					$fields[$eachColumn[1]] = "";					
				} else {
					$fileUpload->the_temp_file = "";
					$fileUpload->the_file = "";
					$fileUpload->http_error = "";
					$fileUpload->file_copy = "";	
					foreach ( $$eachColumn[1] as $varName => $varValue ) {
						if ($varName == "tmp_name")
							$fileUpload->the_temp_file = $varValue;
						if ($varName == "name")
							$fileUpload->the_file = $varValue;
						if ($varName == "error")
							$fileUpload->http_error = $varValue;
					}
					if ($fileUpload->the_temp_file != "") {
						@unlink($uploadedFilePath."/".$$varFileName);
						$fileUpload->replace = "n";
						$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
						$fileUpload->upload();		
						$fields[$eachColumn[1]] = $fileUpload->file_copy;
					}
				}
				break;
			case "PHOTO":
			case "THUMBNAIL":
				$varName = "fileDel_".$eachColumn[1];
				$varFileName = "filename_".$eachColumn[1];
				if($$varName != "") {
					@unlink($uploadedFilePath."/".$$varName);
					$fields[$eachColumn[1]] = "";					
				} else {
					$fileUpload->the_temp_file = "";
					$fileUpload->the_file = "";
					$fileUpload->http_error = "";
					$fileUpload->file_copy = "";	
					foreach ( $$eachColumn[1] as $varName => $varValue ) {
						if ($varName == "tmp_name")
							$fileUpload->the_temp_file = $varValue;
						if ($varName == "name")
							$fileUpload->the_file = $varValue;
						if ($varName == "error")
							$fileUpload->http_error = $varValue;
					}
					if ($fileUpload->the_temp_file != "") {
						@unlink($uploadedFilePath."/".$$varFileName);
						$fileUpload->replace = "n";
						$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
						$fileUpload->upload();		
						$fields[$eachColumn[1]] = $fileUpload->file_copy;
						
						if (!empty($eachColumn[4][3])) {
							imgActualResize($uploadedFilePath,$fields[$eachColumn[1]],$uploadedFilePath,$fields[$eachColumn[1]],$eachColumn[4][3][0],$eachColumn[4][3][1]);
							if (!empty($eachColumn[4][3][2])) {
								if (!empty($eachColumn[4][3][3])) {
									addWaterMark($uploadedFilePath."/".$fields[$eachColumn[1]], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0, 25, $eachColumn[4][3][3]);
								} else
									addWaterMark($uploadedFilePath."/".$fields[$eachColumn[1]], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0);
							}
						}
					}
				}
				break;
			case "CATEGORY_MULTI":
			case "FOREIGN_MULTI":
			case "MULTISELECT":
			case "CHECKBOX":
				$tmpStr = "";
				foreach($$eachColumn[1] as $eachValue)
					if(trim($eachValue) != "")
						$tmpStr .= $eachValue.";";
				$fields[$eachColumn[1]] = ($tmpStr!="") ? $tmpStr : "1;";
				break;
			default:
				$fields[$eachColumn[1]] = $$eachColumn[1];
				break;
			}
		}
		
	$fields[$foreignKey] = $prodId;
	$fields['lastModDate'] = date("Y-m-d H:i:s");
	$conn->AutoExecute("`".$tableName."`", $fields, 'UPDATE', "`".$keyField."` = '" . $$keyField ."'");
} else {
	header("location: inv_list.php");
	exit;
}
$conn->Execute("OPTIMIZE TABLE `".$tableName."`");

header("location: inv_list.php");
ob_end_flush();
exit;
?>