<?php
//$relTableName = "ProductsRelationship";
$keyField = "node_id";
$level = "1";
$perPage = 20;
$pageLinks = 10;
define("PARENT_NODE_SELECT", FALSE);
define("MULTI_CATEGORY", FALSE);
define('DB_CACHE', FALSE);
define("DB_TABLE", $tableName."_node");
define("DB_CATITEM_ID", "id");
define("DB_CATITEM_NAME", "name");
define("EXPORT_TO_EXCEL", FALSE);


//custom setting
$pageName = "Product Category";
$tableName = "product_node";

//language
define("SHOW_LANG_BTN", FALSE);

//node setting(edit it only for categories)
define("RETURN_MAIN_EDIT", "product/manage_tree.php");


$fixedField = array (
	# array( Name, DB Field Name, type, array(list status, width, sortable, searchable), array(edit form status, update, insert, default selection/value), array(language version))

	array( "ID", "node_id", "INT", array("HIDDEN", "0", true, false), array("HIDDEN", false, false, array()) ),
	array( "Category Name(en)", "node_name", "VARCHAR", array("SHOW", "180", true, false), array("SHOW", true, true, array()) ),
	array( "Photo(265px * 85px) ", "photo", "PHOTO", array("HIDDEN", "0", true, false), array("SHOW", true, true, array(), array()) ),
		array( "Caption", "Caption", "TEXT", array("SHOW", "180", false, true), array("SHOW", true, true, array()), array("en") ),
	
);

$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>