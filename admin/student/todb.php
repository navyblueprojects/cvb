<?php
	ob_start();
	include_once("../../config.inc.php"); # common config
	require_once($rootPath . $adminPath."/config.inc.php");
	require_once("config.inc.php"); # config
	include_once($pathDocRoot . "/connDB.php"); # common connect db
	require_once($pathClassLib."/class.uploadfile.php");
	require_once($pathClassLib."/function.common.php");
	require_once($pathClassLib."/class.file.php");
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');

	if (!isset($_SESSION)){
		session_start();	
	}
	$currversion = "";
	if (isset($_version)) {
		$currversion = (array_key_exists($dbDatabase."_VERSION_LANG", $_SESSION)) ? $_SESSION[$dbDatabase.'_VERSION_LANG'] : $_version[0];
	}
	$id = array_key_exists($keyField, $_POST) ? $_POST[$keyField] : false;

	$thissql = "SELECT `".$keyField."` FROM `".$tableName."` WHERE 1 AND `".$keyField."`='".$id."'";
	$rs = $conn->Execute($thissql);
	$recordFound = ($rs->RecordCount() > 0) ? true : false;

	$fields = array();
	foreach ( $cms->__cms_fields as $cmsobj ) {
		$cmsobj->save($recordFound);
	}
	$fields['lastModDate'] = date("Y-m-d H:i:s");
	
	if (!$recordFound) {
		$fields['createDate'] = date("Y-m-d H:i:s");
		$conn->AutoExecute("`".$tableName."`", $fields, 'INSERT');



		header("location: list.php?$keyField=".$conn->Insert_ID());
	} else {
		$conn->AutoExecute("`".$tableName."`", $fields, 'UPDATE', "`".$keyField."` = '" . $_POST[$keyField] ."'");
		header("location: list.php?$keyField=".$id);
	}


	$conn->Execute("OPTIMIZE TABLE `".$tableName."`");


	ob_end_flush();
	exit;
?>