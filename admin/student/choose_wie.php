<?php

require_once("../../config.inc.php"); # common config

require_once("config.inc.php"); # config
require_once($rootPath . $adminPath."/config.inc.php"); # admin config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.pageiterator.php");
header('Cache-Control: no-cache');
header('Pragma: no-cache');
if (!isset($_SESSION)) {
	session_start();
}

// Set global variables if not already set

$node_id = (array_key_exists("node_id", $_GET)) ? $_GET['node_id'] : $node_id;

if (array_key_exists("goUpdate", $_GET)) {

	$wie = array();
	foreach ($_POST["program"] as $program) {
		$wie["program_id"] = $program;
		$wie["studyyear"] = implode(";", $_POST["studyyear"][$program]).";";
		$wie["start_date"] = $_POST["start_date"][$program];
		$wie["end_date"] = $_POST["end_date"][$program];

		$conn->AutoExecute("wie_program", $wie, "UPDATE", "program_id = '".$program."'");
	}

	foreach ($_POST["program_new"] as $idx=>$program) {
		$wie = array();

		$wie["program_id"] = $program;
		$wie["studyyear"] = implode(";", $_POST["studyyear_new"][$idx]).";";
		$wie["start_date"] = $_POST["start_date_new"][$idx];
		$wie["end_date"] = $_POST["end_date_new"][$idx];

		$n = $conn->AutoExecute("wie_program", $wie, "INSERT", "program_id = '".$program."'");

		if (!$n) {
			echo "<p>Insertion Error!</p>";
		}
	}
}

if (array_key_exists("goDelete", $_GET)) {
	$conn->Execute("DELETE from wie_program where program_id = '".(int)$_GET["goDelete"]."'");

}

$printForm = "";
$printHidden = "";
$printCreateCalendarScript = "";

$program = $conn->GetAll("Select id, concat(code,' ',name) as course_name from program where status = 'E'");
$wie_program = $conn->GetAll("Select * from wie_program order by start_date");

?>
<?php require_once( $rootPath . $adminPath."/framework/overall_header.php" );?>

<script type="text/javascript" src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery.datetimepicker.js"></script>
<link href="<?=$adminPath?>/css/calendar-tas.css" rel="stylesheet" type="text/css">
<link href="<?=$adminPath?>/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">

<script src="/js/select2.min.js"></script>
<link href="/css/select2.min.css" rel="stylesheet">

<?=(array_key_exists("gmap", $cms->admin_js)) ? implode("\n", $cms->admin_js["gmap"]) : ""?>


<body>
<div id="body" style=" width:100%;height:100%;overflow: hidden;">
<script>
	function goUpdate(thisform){
		thisform.action = '?goUpdate';
		thisform.submit();
	}

	function removeRow(idx, thisform) {
		if (confirm("All the unsaved changes will be discarded. Are you sure to delete row?")) {
			thisform.action = '?goDelete='+idx;
			thisform.submit();
		}
	}
</script>
<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" >
<div id="listbody">
	<div style="">
		<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$cms->pageName?></span>
		 	 -> Select WIE Programme
		</div>
   		<h3><i>Reminder: The WIE status will be updated every day @ 4:00am .</i></h3>
		<table id="editTable" >
		  <tbody>
		    <tr>
		    <td>
		      <form action="" method="post" id="myform" enctype="multipart/form-data" >
		  	
				  	
			  	
		      <table class="formline" cellspacing="0" cellpadding="6" width="100%" style="padding-bottom:5px;">
		        <tbody>
					<tr>
						<th width="20"><input type="checkbox" class="checkAllToggle"/></th>
						<th width="200">Programme</th><th width="500">Study Year</th><th width="200">Date</th><th width="200">Action</th></tr>
					
					<?php
					foreach ($wie_program as $row) {
						$studyyear = explode(";", $row["studyyear"]);

					?><tr><td><i class="checkbox_container"><input type="checkbox" /></i></td>
						<td width="200"><select class="select2" name="program[]" ><?php
						foreach ($program as $value) {
							?><option value="<?=$value["id"]?>" <?=($value["id"] == $row["program_id"]) ? 'selected="selected"' : ''?>><?=$value["course_name"]?></option><?
						}

						?></select></td>
						<td>
							<label><input type="checkbox" name="studyyear[<?=$row["program_id"]?>][]" value="1" <?=in_array("1", $studyyear) ? 'checked="checked"' : ''?> />Year 1</label>
							<label><input type="checkbox" name="studyyear[<?=$row["program_id"]?>][]" value="2" <?=in_array("2", $studyyear) ? 'checked="checked"' : ''?>/>Year 2</label>
							<label><input type="checkbox" name="studyyear[<?=$row["program_id"]?>][]" value="3" <?=in_array("3", $studyyear) ? 'checked="checked"' : ''?>/>Year 3</label>
							<label><input type="checkbox" name="studyyear[<?=$row["program_id"]?>][]" value="4" <?=in_array("4", $studyyear) ? 'checked="checked"' : ''?>/>Year 4</label>
						</td>
						<td>
							<label><input type="date" name="start_date[<?=$row["program_id"]?>]" value="<?=$row["start_date"]?>" /></label>
							<label><input type="date" name="end_date[<?=$row["program_id"]?>]" value="<?=$row["end_date"]?>"/></label>
						</td>
						<td><input type="button" class="btnItem" onclick="removeRow(<?=$row["program_id"]?>, this.form)" value=" Remove "></td></tr><?php
					}
					?>

			  	</tbody>
				</table>
				<div style="width:100%;">
				
				  	<div style="float:left">
						<input type="button" name="btnAdd" value=" Add New Row " class="btnItem" onclick="addRow()">
					</div>
				 	<div style="float:right">
						<input type="button" name="btnReset" value=" Reset "  class="btnItem" onClick="resetForm(this.form)" style="margin:0px 8px"/>
				  		<input type="button" id="btnUpdate" name="btnUpdate" value=" Update " class="btnItem submitItem" onClick="this.disabled=true;goUpdate(this.form)"  style="padding-left:32px;padding-right:32px;margin:0px 8px;"/>  
		 		
				 	</div>	
				
				</div>
			  </form>
		</td></tr></tbody>
		
		</table>
	      <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>
	</div></div>
</div>
</div>
<script type="text/javascript">
	var i = 0;

	$(function(){
		<?=(array_key_exists("calendar", $cms->admin_js)) ? implode("\n", $cms->admin_js["calendar"]) : ""?>	
		$('.select2').select2();
	});
			        

	$(function(){
		$("input.checkAllToggle").on("click", function(e){
			var currentCheckedStatus = $(this).prop("checked");
			$(this).parent().parent().parent().find(".checkbox_container").find("input[type=checkbox]").prop("checked", currentCheckedStatus);
		});

	});
	var addRow = function() {
		$('.formline tbody').append(newrow(i++));

		$('.select2').select2();
	}
	var newrow = (i)=>{
		return '<tr><td><i class="checkbox_container"><input type="checkbox" /></i></td>'+
					'<td width="200"><select class="select2" name="program_new[]" >'+
					'<?php
						foreach ($program as $value) {
							?><option value="<?=$value["id"]?>"><?=trim($value["course_name"])?></option><?
						}
						?>'
					+'</select></td>'+
					'<td><label><input type="checkbox" name="studyyear_new['+i+'][]" value="1" />Year 1</label> '+
						'<label><input type="checkbox" name="studyyear_new['+i+'][]" value="2" />Year 2</label> '+
						'<label><input type="checkbox" name="studyyear_new['+i+'][]" value="3" />Year 3</label> '+
						'<label><input type="checkbox" name="studyyear_new['+i+'][]" value="4" />Year 4</label></td>'+
					'<td><label><input type="date" name="start_date_new['+i+']" value="" /></label>'+
						'<label><input type="date" name="end_date_new['+i+']" value=""/></label></td>'+
					'<td><input type="button" class="btnItem" value=" Remove "></td></tr>'
	};
</script>

</body>
</html>

<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>