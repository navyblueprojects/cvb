<?php
ob_start();

require_once("../../config.inc.php"); # common config

$currversion = "";
if (isset($_version) && $_version) {
	$currversion = (array_key_exists($dbDatabase."_VERSION_LANG", $_SESSION)) ? $_SESSION[$dbDatabase.'_VERSION_LANG'] : $_version[0];
	$currversion = (array_key_exists("lang", $_POST)) ? $_POST['lang'] : $currversion;
	$currversion = (array_key_exists("lang", $_GET)) ? $_GET['lang'] : $currversion;
	$_SESSION[$dbDatabase. '_VERSION_LANG'] = $currversion;	
}

require_once("config.inc.php"); # config
require_once($rootPath . $adminPath."/config.inc.php"); # admin config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.pageiterator.php");
header('Cache-Control: no-cache');
header('Pragma: no-cache');
if (!isset($_SESSION)) {
	session_start();
}

// Set global variables if not already set
$node_id = "";
$refresh = (array_key_exists("refresh", $_GET)) ? $_GET['refresh'] : "";
if($refresh == "1") echo "<script language=\"javascript\">window.location.replace('".$_SERVER['PHP_SELF']."');</script>";
$arrVar = $conn->MetaColumns($tableName);
$arrVar = $conn->MetaColumnNames($tableName);
foreach ($arrVar as $var) {
	$$var = "";
}
if ($level == 0)
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : 1;
else
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : "";


if($id != "") {
	$thissql = "SELECT * FROM `".$tableName."` WHERE `".$keyField."`='".$id."'";
	$rs = $conn->SelectLimit($thissql, 1, 0);
	$totalRecord = $rs->RecordCount();

	if($totalRecord > 0) {
		while (!$rs->EOF) {	
			foreach ($rs->fields as $key=>$value) {
				$$key = $value;
			}					
			$rs->MoveNext();
		}
	}
	
}

function hasChild($node_id) {
	global $conn;
	
	$thissql = "SELECT n2.* FROM `".DB_TABLE."` n1, `".DB_TABLE."` n2 WHERE n1.`node_id`='".$node_id."' AND n1.`node_left`<n2.`node_left` AND n1.`node_right`>n2.`node_right`";
	$rs = $conn->Execute($thissql);
	return ($rs->RecordCount() > 0);
}
$node_id = (array_key_exists("node_id", $_GET)) ? $_GET['node_id'] : $node_id;


$printForm = "";
$printHidden = "";

$printCreateCalendarScript = "";

foreach ( $cms->__cms_fields as $eachColumn ) {

	if ( $eachColumn->edit["show"] ) {


		if($eachColumn->type == "CUSTOM")
			$printForm .= "<tr><td style=\"".$eachColumn->options["custom"]["style"]."\" colspan=\"2\">".$eachColumn->options["custom"]["label"]."</td>";
		else
			$printForm .= '<tr><th width="150" >'.$eachColumn->label . (	!empty($eachColumn->lang) ? ' ('.$currversion.')' : '' ).': </th>';	
					
		$eachFieldName =  $eachColumn->fieldName . (!(empty($eachColumn->lang)) ? '_'.$currversion : "");
		$printForm .= '<td>' . $eachColumn->renderControls();

		if(!empty($eachColumn->error)){
			$printForm .= '<div class="errorContainer">'.$eachColumn->error.'</div>';
		}else{
			$printForm .= '<div class="errorContainer">'.DEFAULT_ERROR_MSG.'</div>';
		}
			
		if(!empty($eachColumn->options["tips"])){
			$printForm .= '		<span class="tipsContent"> <div>Tips:</div>'.$eachColumn->options["tips"].'</span>';
		}
		$printForm .= "</td></tr>\n";


	} else {

		$eachFieldName = (!empty($eachColumn->lang)) ? $eachColumn->fieldName.'_'.$currversion : $eachColumn->fieldName;
		$printHidden .= '<input type="hidden" id="'.$eachFieldName.'" name="'.$eachFieldName.'" value="'.$$eachFieldName.'" />';
	}
}
?>
<?php require_once( $rootPath . $adminPath."/framework/overall_header.php" );?>
<script type="text/javascript" src="<?=$adminPath?>/js/core.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/events.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/css.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/coordinates.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/drag.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/dragsort.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckeditor.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckfinder/ckfinder.js"></script>
<link href="<?=$adminPath?>/css/calendar-tas.css" rel="stylesheet" type="text/css">
<link href="<?=$adminPath?>/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">



<?=(array_key_exists("gmap", $cms->admin_js)) ? implode("\n", $cms->admin_js["gmap"]) : ""?>

<script type="text/javascript">
	var sBasePath = "<?=$fckEditorPath?>/";
	var fcklist = new Array('<?=array_key_exists("ckeditor", $cms->admin_js) ? implode("','", $cms->admin_js["ckeditor"]) : ""?>');

	function checkdatetime(input) {
		var validformat=/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/; //Basic check for format validity
		var returnval=false;
		if(input.value != "") {
			if (!validformat.test(input.value))
				alert("Invalid Date Format. Please correct and submit again.");
			else{ //Detailed check for valid date ranges
				var dateArr = input.value.split(" ")[0];
				var yearfield=dateArr.split("-")[0];
				var monthfield=dateArr.split("-")[1];
				var dayfield=dateArr.split("-")[2];
				var dayobj = new Date(yearfield, monthfield-1, dayfield);
				if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
					alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
				else
					returnval=true;
			}
			return returnval;
		} else {
			return true;
		}
	}
			 
	function checkdate(input) {
		var validformat=/^\d{4}\-\d{2}\-\d{2}$/; //Basic check for format validity
		var returnval=false;
		if(input.value != "") {
			if (!validformat.test(input.value))
				alert("Invalid Date Format. Please correct and submit again.");
			else{ //Detailed check for valid date ranges
				var yearfield=input.value.split("-")[0];
				var monthfield=input.value.split("-")[1];
				var dayfield=input.value.split("-")[2];
				var dayobj = new Date(yearfield, monthfield-1, dayfield);
				if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
					alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
				else
					returnval=true;
			}
			return returnval;
		} else {
			return true;
		}
	}
	
	function getFckEditorTextLength(fckName) {
		// This functions shows that you can interact directly with the editor area
		// DOM. In this way you have the freedom to do anything you want with it.
		
		// Get the editor instance that we want to interact with.
		var oEditor = FCKeditorAPI.GetInstance(fckName) ;
	
		// Get the Editor Area DOM (Document object).
		var oDOM = oEditor.EditorDocument ;
	
		var iLength ;
	
		// The are two diffent ways to get the text (without HTML markups).
		// It is browser specific.
	
		if ( document.all ) { // If Internet Explorer.
			var innerText = oDOM.body.innerText;
			iLength = innerText.length ;
		} else {// If Gecko.
			var r = oDOM.createRange() ;
			r.selectNodeContents( oDOM.body ) ;
			iLength = r.toString().length ;
		}
	
		return iLength;
	}
	
	function setEditorValue( instanceName, text )
	{  
	  /* Old Version Coding
	  // Get the editor instance that we want to interact with.
	  //var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	  
	  // Set the editor contents.
	  //oEditor.SetHTML( text ) ;
	  */
	  CKEDITOR.instances[instanceName].setData(text);
	} 
	
	function getEditorValue( instanceName ) 
	{  
	  /* Old Version Coding
	  // Get the editor instance that we want to interact with.
	  //var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	  
	  // Get the editor contents as XHTML.
	  //return oEditor.GetXHTML( true ) ;  // "true" means you want it formatted.
	  */
	  CKEDITOR.instances[instanceName].getData();
	}
	
	function goUpdate(myForm) {
		//var name = myForm.name.value;
		//var description = getEditorValue("desc");
		var errorFlag = false;
		var errorMsg = "";
		/*	
		if (name == "") {
			errorMsg += "Please input the name\n";
			errorFlag = true;
		}
		if (description == "") {
			errorMsg += "Please input the description\n";
			errorFlag = true;
		}
		*/
		if(errorFlag) {
			alert(errorMsg);
			myForm.btnUpdate.disabled = false;
			return false;
		} else {
			myForm.method = "post";
			myForm.action = "todb.php";
			myForm.submit();
		}
	}
	
	function resetForm( myForm ) {
		myForm.reset();
		for (var i = 0; i < fcklist.length; i++) {
			CKEDITOR.instances[fcklist[i]].setData(document.getElementById(fcklist[i]).value);
		}
	}
	
	window.onload = function() {
		for (var i = 0; i < fcklist.length; i++) {
			eval("var oFCKeditor" + (i+1) + " = CKEDITOR.replace('"+fcklist[i]+"')");	
			eval("CKFinder.setupCKEditor(oFCKeditor" + (i+1) + ", '<?=$fckEditorPath?>/ckfinder/' )");
		}
				
	}
	function addbr() { 
	    //Create an input type dynamically.
		var fxloat = document.createElement("div"); 
		fxloat.style.width = '640px';	
		fxloat.style.clear = 'both';	
		return fxloat;
	}
	
	function golang(lang) {
		if (confirm('Any unsaved data will be lost. Are you sure?')) {
			window.location = '?id=<?=$id?>&lang='+lang;
		}
	}
	
	function add2(target, obj, css, attri){
		var obj = $(target).find('div').eq(0).clone().css('background-color','#CCCCFF');
		obj.find('input').val('');
		$(target).append(obj);
	}
	
	function add(fx, name,type,value) { 
	    //Create an input type dynamically.
		var element = document.createElement("input"); 
		
	    //Assign different attributes to the element.
	    element.setAttribute("type", type);
	    element.setAttribute("value", value);
	    element.setAttribute("name", name);
		element.setAttribute("id", name);	
	 	element.style.cssFloat = 'left';
		element.style.width = '127px';	
		//element.style.marginRight = '19px';	
		
	    fx.appendChild(element);
	}
	
	function removeThis(obj) {
		if (confirm('Are you sure to remove this row?')) {
			$(obj).parentsUntil('.json_row').parent().remove();
		}
	}



	function toggleCheckBox(id){

		var current = document.getElementById(id).disabled;
		document.getElementById(id).disabled = !current;

		if(current == true){
			document.getElementById(id).style.visibility = "visible";
		}else{
			document.getElementById(id).style.visibility = "hidden";
		}
		
	}
		
</SCRIPT>
<link href="/css/select2.min.css" rel="stylesheet" />
<script src="/js/select2.min.js"></script>
<body>
<div id="body" style=" width:100%;height:100%;overflow: hidden;">

<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" >
<div id="listbody">
	<div style="">
		<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$cms->pageName?></span>
		 	 -> <?=($id=="") ? "Add" : "Edit"?>
		</div>
   		
		<table id="editTable" >
		  <tbody>
		    <tr>
		    <td>
		      <form action="" method="post" id="myform" enctype="multipart/form-data" >
		      	<div align="right" style="position:relative; z-index:1;height:0px;"><?
						if (isset($_version)) { 	 
					 		foreach ($_version as $v) {
			        ?><a class="langbtn btnItem <?=($v == $currversion) ? "curr" : ""?>" onClick="golang('<?=$v?>')">Switch to <?=$v?></a><?
					  		}
					  	}
			        ?></div>
		      <div><input type="hidden" value="ok" name="refresh" /><?=$printHidden?></div>
		  	
				  	
			  	
		      <table class="formline_edit" cellspacing="0" cellpadding="6" width="100%" style="padding-bottom:5px;">
		        <tbody>
				<?=$printForm?>
		        <?php if ($level > 0) { ?>
				<tr>
					<td colspan="3" align="center"></td>
				</tr>
		        <?php } ?>
			  </tbody></table>
				<div style="width:100%;">
				
				  	<div style="float:left">
						<a href="./list.php?node_id=<?=$node_id?>"  class="btnItem" style="margin:0px 8px">Back to List Page</a>
					</div>
				 	<div style="float:right">
						<?php 
				 			if(EDIT_NEED){
				 		?>
								<input type="button" name="btnReset" value=" Reset "  class="btnItem" onClick="resetForm(this.form)" style="margin:0px 8px"/>
						  		<input type="button" id="btnUpdate" name="btnUpdate" value=" <?=($id=="") ? "Add" : "Update"?> " class="btnItem submitItem" onClick="this.disabled=true;goUpdate(this.form)"  style="padding-left:32px;padding-right:32px;margin:0px 8px;"/> 
				 		<?php 
				 			}
				 		?>
				 	</div>	
				
				</div>
			  </form>
		</td></tr></tbody>
		
		</table>
	      <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>
	</div></div>
</div>
</div>
<script type='text/javascript' src='<?=$adminPath?>/js/calendar.js'></script>
<script type='text/javascript' src='<?=$adminPath?>/js/calendar-en.js'></script>
<script type='text/javascript' src='<?=$adminPath?>/js/calendar-setup.js'></script>";
<script type="text/javascript">
	$(function(){
		$('.select2').select2();

		<?=(array_key_exists("calendar", $cms->admin_js)) ? implode("\n", $cms->admin_js["calendar"]) : ""?>	
	});
			        

	$(function(){
		$("input.checkAllToggle").on("click", function(e){
			var currentCheckedStatus = $(this).prop("checked");
			$(this).parent().parent().parent().find(".checkboxContainer").find("input[type=checkbox]").prop("checked", currentCheckedStatus);
		});
	});


</script>

</body>
</html>

<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>