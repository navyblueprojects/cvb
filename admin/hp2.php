<?php
	include_once("../config.inc.php");
	include_once("../connDB.php");
	include_once("./config.inc.php");
	include_once($rootPath . $adminPath."/framework/overall_header.php");
?>
<head>
	<link rel="stylesheet" href="<?=$adminPath?>/css/jquery-ui.min.css" type="text/css">
	<link rel="stylesheet" href="<?=$adminPath?>/css/jquery-ui.theme.min.css" type="text/css">
	<link rel="stylesheet" href="<?=$adminPath?>/css/cms.css" type="text/css">
	<script type="text/javascript"
	src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?=$adminPath?>/js/jquery-ui.min.js"></script>

	<script type="text/javascript" src="<?=$fckEditorPath?>/ckeditor.js"></script>
	<script type="text/javascript" src="<?=$fckEditorPath?>/ckfinder/ckfinder.js"></script>
	<style>
		.adminPreview {

			display: inline-block;
			min-height: 200px;
			height:360px;
			vertical-align: middle;
			-webkit-box-shadow: 2px 2px 2px 2px #CCCCCC;
			box-shadow: 2px 2px 2px 2px #CCCCCC;
			-webkit-border-radius: 4px 4px 4px 4px;
			border-radius: 4px 4px 4px 4px;
			padding: 8px;
			margin: 8px;
			
			background: #ffffff; /* Old browsers */
			background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5)); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* IE10+ */
			background: linear-gradient(to bottom,  #ffffff 0%,#e5e5e5 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 ); /* IE6-9 */
						
		}

		.adminPreview th{
			background-color:#DDD;
		}
		
		.adminPreview table td {
			height: 32px;
			padding: 2px;
			text-align: center;
		}

		.adminPreview a {
			text-decoration: none;
			color: initial;
		}
		.adminPreview a:hover {
			opacity: 0.6;
		}

		.adminPreview .title {
			font-weight: bold;
			padding:8px;
			text-align:right;
			font-size: 200%;
			color:#666;
			text-shadow: 3px 3px 3px #CCC;
		}

		.adminPreview .contentList {

			padding: 8px;
		}

		.adminPreview tr:hover>td{
			background-color:#BEE9FF;
		}
		.adminPreview th{
		
			background: #0675ba;
			background: -moz-linear-gradient(top, #0675ba 0%, #5da0c7 69%, #80b7d0 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0675ba), color-stop(69%,#5da0c7), color-stop(100%,#80b7d0));
			background: -webkit-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -o-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -ms-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: linear-gradient(to bottom, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0675ba', endColorstr='#80b7d0',GradientType=0 );
				
		}
	</style>
</head>

<body>
	<div id="body" style=" width:100%;height:100%">

		<div id="leftNav" style="min-width:200px;height:100%;">
			<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
		</div>

		<div id="mainFrame" style="">

			<div>
				<span class="pageTitle" >Order Summary</span>
			</div>
			<div style="padding:16px;width:90%;height:90%">

				

				
				
				
				

				
				<div class="adminPreview"  style="width:100%;height:100%">
					<div id="tabs" style="height:100%;">
						<ul>
							<li>
								<a href="#tabs-orderSummary">Order Summary</a>
							</li>
							<li>
								<a href="#tabs-normalOrder">Pending Order</a>
							</li>
							<li>
								<a href="#tabs-returnOrder">Refund/Exchange Order</a>
							</li>
						</ul>
						<div id="tabs-orderSummary">
							<?php
								//total
								$totalOrderSQL = "select count(*) from orders";
								$totalReturnSQL = "select count(*) from return_record";
								$totalRs = $pdo->prepare($totalOrderSQL);
								$totalRs->execute();
								$totalRs_row = $totalRs->fetch();
								$totalOrder = $totalRs_row["count(*)"];
	
								$totalReturnRs = $pdo->prepare($totalReturnSQL);
								$totalReturnRs->execute();
								$totalReturnRs_row = $totalReturnRs->fetch();
								$totalReturnOrder = $totalReturnRs_row["count(*)"];
	
								//thismonth
								$thisMonthOrderSQL = "select count(*) from orders where MONTH(createDate)= :m and YEAR(createDate) =:y;";
								$thisMonthReturnSQL = "select count(*) from return_record where MONTH(createDate)= :m and YEAR(createDate) =:y;";
								$thisMonthRs = $pdo->prepare($thisMonthOrderSQL);
								$thisMonthRs->bindValue("m", date("n"), PDO::PARAM_INT);
								$thisMonthRs->bindValue("y", date("Y"), PDO::PARAM_INT);
								$thisMonthRs->execute();
								$thisMonthRs_row = $thisMonthRs->fetch();
								$thisMonthOrder = $thisMonthRs_row["count(*)"];
								
								$thisMonthReturnRs = $pdo->prepare($thisMonthReturnSQL);
								$thisMonthReturnRs->bindValue("m", date("n"), PDO::PARAM_INT);
								$thisMonthReturnRs->bindValue("y", date("Y"), PDO::PARAM_INT);
								$thisMonthReturnRs->execute();
								$thisMonthReturnRs_row = $thisMonthReturnRs->fetch();
								$thisMonthReturnOrder = $thisMonthReturnRs_row["count(*)"];
								
								//today
								$todayOrderSQL = "select count(*) from orders where DATE(createDate)=CURDATE();";
								$todayReturnSQL = "select count(*) from return_record where DATE(createDate)=CURDATE();";
								$todayRs = $pdo->prepare($todayOrderSQL);
								$todayRs->execute();
								$todayRs_row = $todayRs->fetch();
								$todayOrder = $todayRs_row["count(*)"];
	
								$todayReturnRs = $pdo->prepare($todayReturnSQL);
								$todayReturnRs->execute();
								$todayReturnRs_row = $todayReturnRs->fetch();
								$todayReturnOrder = $todayReturnRs_row["count(*)"];
							?>

							<div class="contentList" >
								<table cellpadding="8" cellspacing="0" width="100%">
									<tr>
										<th width="30%" style="background-color:#A8D324;color:#fff;padding:16px;"></th>
										<th width="40%" style="background-color:#A8D324;color:#fff;padding:16px;">Normal Order</th>
										<th width="40%" style="background-color:#A8D324;color:#fff;padding:16px;">Refund Order</th>
									</tr>
									<tr>
										<td style="padding:24px;">Today</td>
										<td style="padding:24px;"><?=$todayOrder?></td>
										<td style="padding:24px;"><?=$todayReturnOrder?></td>
									</tr>
									<tr>
										<td style="padding:24px;">This month<br/>(<?=date("F")?>, <?=date("Y")?>)</td>
										<td style="padding:24px;"><?=$thisMonthOrder?></td>
										<td style="padding:24px;"><?=$thisMonthReturnOrder?></td>
									</tr>
									<tr>
										<td style="padding:24px;">Overall</td>
										<td style="padding:24px;"><?=$totalOrder?></td>
										<td style="padding:24px;"><?=$totalReturnOrder?></td>
									</tr>

								</table>
							</div>
						</div>
						<div id="tabs-normalOrder" >

							<div id="orderStatus">
								<h3>Pending (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
	
										$pendingOrderSQL  = "
											select * from orders where order_status='PAID' order by id desc limit 20;
										";
										$pendingOrderRs = $pdo->prepare($pendingOrderSQL);
										$pendingOrderRs -> execute();
					
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable " cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Invoice ID </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Order Total </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($pendingOrderRs_row = $pendingOrderRs->fetch()){
														$orderId = $pendingOrderRs_row["id"];
														$invoiceId = $pendingOrderRs_row["orderNumber"];
														$totalCharge = $pendingOrderRs_row["total_charge"];
														$createDate = date('M j Y', strtotime($pendingOrderRs_row["createDate"]));
													?>
													<tr>
														<td align="center"><a href="order/edit.php?id=<?=$orderId?>"> <?=$invoiceId?> </a></td>
														<td align="center">USD <?=number_format($totalCharge,2)?></td>
														<td align="center"><?=$createDate?> </td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<h3>Success (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
	
										$successOrderSQL  = "
											select * from orders where order_status in('SUCCESS', 'SHIPPED') order by id desc limit 20;
										";
										$successOrderRs = $pdo->prepare($successOrderSQL);
										$successOrderRs -> execute();
					
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Invoice ID </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Order Total </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($successOrderRs_row = $successOrderRs->fetch()){
														$orderId = $successOrderRs_row["id"];
														$invoiceId = $successOrderRs_row["orderNumber"];
														$totalCharge = $successOrderRs_row["total_charge"];
														$createDate = date('M j Y', strtotime($successOrderRs_row["createDate"]));
													?>
													<tr>
														<td align="center"><a href="order/edit.php?id=<?=$orderId?>"> <?=$invoiceId?> </a></td>
														<td align="center">USD <?=number_format($totalCharge,2)?></td>
														<td align="center"><?=$createDate?> </td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="tabs-returnOrder">
							
						
							<div id="returnOrderStatus">
								<h3>Refund Pending (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
	
										$refundPendingOrderSQL  = "
											select *,rr.id as rid, sum(qty) as returnItems
											from return_record rr
											join return_item ri on ri.return_id = rr.id
											where rr.status='WAITING_APPROVE'
											and ri.return_type='REFUND'
											group by ra
											order by rr.id desc
											limit 20;
										";
										$refundPendingOrderRs = $pdo->prepare($refundPendingOrderSQL);
										$refundPendingOrderRs -> execute();
					
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> RA Number </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Refund Items</th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($refundPendingOrderRs_row = $refundPendingOrderRs->fetch()){
														$orderId = $refundPendingOrderRs_row["rid"];
														$orderRa = $refundPendingOrderRs_row["ra"];
														$totalReturnItems = $refundPendingOrderRs_row["returnItems"];
														$createDate = date('M j Y', strtotime($refundPendingOrderRs_row["createDate"]));
														if(empty($orderRa))continue;
													?>
													<tr>
														<td align="center"><a href="return/edit.php?id=<?=$orderId?>"> <?=$orderRa?> </a></td>
														<td align="center"><?=$totalReturnItems?> </td>
														<td align="center"><?=$createDate?> </td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
								
								
								
								
								<h3>Refund All Received (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
	
										$refundCompletedOrderSQL  = "
											select *,rr.id as rid, sum(qty) as returnItems
											from return_record rr
											join return_item ri on ri.return_id = rr.id
											where rr.status='ALL_RECEIVED'
											and ri.return_type='REFUND'
											group by ra
											order by rr.id desc
											limit 20;
										";
										$refundCompletedOrderRs = $pdo->prepare($refundCompletedOrderSQL);
										$refundCompletedOrderRs -> execute();
					
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> RA Number </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Refund Items</th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($refundCompletedOrderRs_row = $refundCompletedOrderRs->fetch()){
														$orderId = $refundCompletedOrderRs_row["rid"];
														$orderRa = $refundCompletedOrderRs_row["ra"];
														$totalReturnItems = $refundCompletedOrderRs_row["returnItems"];
														$createDate = date('M j Y', strtotime($refundCompletedOrderRs_row["createDate"]));
														if(empty($orderRa))continue;
													?>
													<tr>
														<td align="center"><a href="return/edit.php?id=<?=$orderId?>"> <?=$orderRa?> </a></td>
														<td align="center"><?=$totalReturnItems?> </td>
														<td align="center"><?=$createDate?> </td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
								
								
								
								
								
								
								
								
								
								
								
								
								<h3>Refund Completed (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
	
										$refundCompletedOrderSQL  = "
											select *,rr.id as rid, sum(qty) as returnItems
											from return_record rr
											join return_item ri on ri.return_id = rr.id
											where rr.status='COMPLETED'
											and ri.return_type='REFUND'
											group by ra
											order by rr.id desc
											limit 20;
										";
										$refundCompletedOrderRs = $pdo->prepare($refundCompletedOrderSQL);
										$refundCompletedOrderRs -> execute();
					
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> RA Number </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Refund Items</th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($refundCompletedOrderRs_row = $refundCompletedOrderRs->fetch()){
														$orderId = $refundCompletedOrderRs_row["rid"];
														$orderRa = $refundCompletedOrderRs_row["ra"];
														$totalReturnItems = $refundCompletedOrderRs_row["returnItems"];
														$createDate = date('M j Y', strtotime($refundCompletedOrderRs_row["createDate"]));
														if(empty($orderRa))continue;
													?>
													<tr>
														<td align="center"><a href="return/edit.php?id=<?=$orderId?>"> <?=$orderRa?> </a></td>
														<td align="center"><?=$totalReturnItems?> </td>
														<td align="center"><?=$createDate?> </td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
								
								<h3>Exchange Pending (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
									
										$exchangePendingOrderSQL  = "
											select *,rr.id as rid, sum(qty) as returnItems
											from return_record rr
											join return_item ri on ri.return_id = rr.id
											where rr.status='WAITING_APPROVE'
											and ri.return_type='EXCHANGE'
											group by ra
											order by rr.id desc
											limit 20;
										";
										$exchangePendingOrderRs = $pdo->prepare($exchangePendingOrderSQL);
										$exchangePendingOrderRs -> execute();
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> RA Number </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Exchange Items</th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($exchangePendingOrderRs_row = $exchangePendingOrderRs->fetch()){
														$orderId = $exchangePendingOrderRs_row["rid"];
														$orderRa = $exchangePendingOrderRs_row["ra"];
														$totalReturnItems = $exchangePendingOrderRs_row["returnItems"];
														$createDate = date('M j Y', strtotime($exchangePendingOrderRs_row["createDate"]));
														if(empty($orderRa))continue;
													?>

													<tr>
														<td align="center"><a href="return/edit.php?id=<?=$orderId?>"> <?=$orderRa?> </a></td>
														<td align="center"><?=$totalReturnItems?> </td>
														<td align="center"><?=$createDate?> <?php ?></td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
								
								
								
								
								
								
								
								
								<h3>Exchange All Received (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
									
										$exchangePendingOrderSQL  = "
											select *,rr.id as rid, sum(qty) as returnItems
											from return_record rr
											join return_item ri on ri.return_id = rr.id
											where rr.status='ALL_RECEIVED'
											and ri.return_type='EXCHANGE'
											group by ra
											order by rr.id desc
											limit 20;
										";
										$exchangePendingOrderRs = $pdo->prepare($exchangePendingOrderSQL);
										$exchangePendingOrderRs -> execute();
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> RA Number </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Exchange Items</th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($exchangePendingOrderRs_row = $exchangePendingOrderRs->fetch()){
														$orderId = $exchangePendingOrderRs_row["rid"];
														$orderRa = $exchangePendingOrderRs_row["ra"];
														$totalReturnItems = $exchangePendingOrderRs_row["returnItems"];
														$createDate = date('M j Y', strtotime($exchangePendingOrderRs_row["createDate"]));
														if(empty($orderRa))continue;
													?>

													<tr>
														<td align="center"><a href="return/edit.php?id=<?=$orderId?>"> <?=$orderRa?> </a></td>
														<td align="center"><?=$totalReturnItems?> </td>
														<td align="center"><?=$createDate?> <?php ?></td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								<h3>Exchange Completed (Last 20 Records)</h3>
								<div style="height:100%;">
									<?php
	
										$exchangeCompletedOrderSQL  = "
											select *,rr.id as rid, sum(qty) as returnItems
											from return_record rr
											join return_item ri on ri.return_id = rr.id
											where rr.status='COMPLETED'
											and ri.return_type='EXCHANGE'
											group by ra
											order by rr.id desc
											limit 20;
										";
										$exchangeCompletedOrderRs = $pdo->prepare($exchangeCompletedOrderSQL);
										$exchangeCompletedOrderRs -> execute();
					
									?>
					
									<div style="position: relative">
										<div style="overflow:auto; height:540px ">
				
											<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
												<tbody>
													<tr>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> RA Number </th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Exchange Items</th>
														<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="150" align="center"> Create Date </th>
													</tr>
													<?php
													while($exchangeCompletedOrderRs_row = $exchangeCompletedOrderRs->fetch()){
														$orderId = $exchangeCompletedOrderRs_row["rid"];
														$orderRa = $exchangeCompletedOrderRs_row["ra"];
														$totalReturnItems = $exchangeCompletedOrderRs_row["returnItems"];
														$createDate = date('M j Y', strtotime($exchangeCompletedOrderRs_row["createDate"]));
														if(empty($orderRa))continue;
													?>
													<tr>
														<td align="center"><a href="return/edit.php?id=<?=$orderId?>"> <?=$orderRa?> </a></td>
														<td align="center"><?=$totalReturnItems?> </td>
														<td align="center"><?=$createDate?> </td>
													</tr>
													<?php
													}
													?>
				
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				</div>
				
			</div>

			<?php
			/*
			<table border="0" cellspacing="0" cellpadding="0" style="width:800px; margin-top:13px; margin-left:3px; ">
				<tr>
					<td valign="top" >
					<table cellpadding="0" cellspacing="0" style="width:100%; padding-left:30px; padding-top:50px; margin-bottom:20px" class="formline">
						<tr>
							<td class="rowHeader" colspan="5" style="padding-top:10px"><span style="font-style: italic"><u>Admin Home</u></span></td>
						</tr>
						<tr>
							<td>
							<p>
								<span style="font-family:Arial, Helvetica, sans-serif; font-size:14px"> Welcome to <?=$navHeadText?>
									<br />
									<br />
									You may perform the following task in this area: </span>
							</p><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
								<ol>
									<?php foreach($leftNavItems["SECTIONS"] as $eachItem) { ?>
									<li style="margin-bottom:5px;">
										<a href="<?=$adminPath."/".trim($eachItem[1])."/".trim($eachItem[2])?>"><?=trim($eachItem[0])?> Administration</a>
									</li>
									<?php } ?>
								</ol></span></td>
						</tr>
					</table> <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?> </td>
				</tr>
			</table>

			*/

			?>

		</div>

	</div>

	<script>
		$(function() {
			$("#orderStatus").accordion({
				heightStyle : "content",
				collapsible: true
			});

			$("#returnOrderStatus").accordion({
				heightStyle : "content",
				collapsible: true
			});
			
			$("#tabs").tabs();
		});
	</script>
</body>
</html>
<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>