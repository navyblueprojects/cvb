<?php
    ob_start();

    require_once("../../config.inc.php"); # common config
    require_once("config.inc.php"); # common config

    if (array_key_exists("student_id", $_POST)) {
        $mywhere = "member_id in (".implode(",",$_POST["student_id"]).") ";
        $session = $cms->export(["file_obj"], $_POST["password"], ["file_obj"=> $mywhere], true);
        echo json_encode(array("session"=>$session));
        $cms->removeFiles($_POST["student_id"]);
        exit(); 
    }

    //check for permission if needed
    if(!$hasExportPermission){
        include_once("../framework/no_permission.php");
    }

    $student = array();
    foreach ($cms->getStudent() as $row) {
        $student[$row["yrofstudy"]][] = $row;
    }

?>
<?php require_once( $rootPath . $adminPath."/framework/overall_header.php" );?>

<body>
    <script type="text/javascript" src="<?=$adminPath?>/js/core.js"></script>
    <script type="text/javascript" src="<?=$adminPath?>/js/events.js"></script>
    <script type="text/javascript" src="<?=$adminPath?>/js/css.js"></script>
    <script type="text/javascript" src="<?=$adminPath?>/js/coordinates.js"></script>
    <script type="text/javascript" src="<?=$adminPath?>/js/drag.js"></script>
    <script type="text/javascript" src="<?=$adminPath?>/js/dragsort.js"></script>
    <script type="text/javascript" src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?=$adminPath?>/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="<?=$fckEditorPath?>/ckeditor.js"></script>
    <script type="text/javascript" src="<?=$fckEditorPath?>/ckfinder/ckfinder.js"></script>
    <link href="<?=$adminPath?>/css/font-awesome.min.css" type="text/css" />

<div id="body" style=" width:100%;height:100%;overflow: hidden;">

<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" >
<div id="listbody">
    <div style="">
        <div>
            <span style="cursor:pointer" class="pageTitle" onClick="window.location='./list.php'"> File Housekeeping</span>
        </div>
        
        <table id="editTable" >
          <tbody>
            <tr>
            <td>
              <form action="" method="post" id="myform" enctype="multipart/form-data" >
                <table class="formline_edit" cellspacing="0" cellpadding="6" width="100%" style="padding-bottom:5px;">
                <tbody>
                    <tr>
                        <td></td>
                        <td>
                            <div>
                            <?php
                            foreach ($student as $idx=>$stud) {
                            ?><fieldset class="">
                                <legend><label><input type="checkbox" class="checkAllToggle"> Year <?=$idx?></label></legend>
                                <?php
                                    foreach ($stud as $row) {
                                    ?><div class="checkboxContainer"> <label><input type="checkbox" name="student_id[]" value="<?=$row["id"]?>" ><?=$row["student_id"]?></label> </div><?php
                                    }
                                ?>
                            </fieldset>
                            <?php
                            }                            
                        ?></td>
                    </tr>
                <?php if ($level > 0) { ?>
                <tr>
                    <td colspan="3" align="center"></td>
                </tr>
                <?php } ?>
              </tbody></table>

              <div style="width:100%;">
                
                    <div style="float:left">
                        <a href="./list.php?node_id=<?=$node_id?>"  class="btnItem" style="margin:0px 8px">Back to List Page</a>
                    </div>
                    <div style="float:right">
                        <?php 
                            if($hasExportPermission){
                        ?>
                            <input type="button" id="btnUpdate" name="btnUpdate" value=" Export Data " class="btnItem submitItem" onClick="goExport(this.form)"  style="padding-left:32px;padding-right:32px;margin:0px 8px;"/> 
                        <?php 
                            }
                        ?>
                    </div>  
                
                </div>

              </form>
        </td></tr></tbody>
        
        </table>
          <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>
    </div></div>
</div>
</div>
<!-- Modal HTML embedded directly into document -->
<div id="passwordWrapper" class="modal">
  <p><strong>WARNING:</strong> After you click 'Export Now', all the selected students' personal files stored in this system WILL BE removed from the database. </p>
  <p>Please add a password onto the zip file.</p>
  <div><input type="text" name="password" id="password" /></div><br clear="all" />
  <div><a href="#" id="btnExportX" class="btnItem">Export Now</a></div>
</div>

<div id="loadingWrapper" class="modal">
  <p>Now Loading ... <i class="fa fa-spinner fa-spin"></i></p>
</div>


<script type='text/javascript' src='<?=$adminPath?>/js/jquery.modal.min.js'></script>
<link href="<?=$adminPath?>/css/jquery.modal.min.css" rel="stylesheet"/>

<script type="text/javascript" src="<?=$adminPath?>/js/data.js"></script>
</body>
</html>

<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>