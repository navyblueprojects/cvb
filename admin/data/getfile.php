<?php
	include_once("config.inc.php");

	$filename = "cvb-data-".$_REQUEST["session"].".zip";
	$filepath = $rootPath.$appPath."/dataexport/";

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: Export Data");
	header("Content-type: application/zip");
	header("Content-Disposition:attachment;filename=cvbdata-".date("YmdHis").".zip");
	header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".filesize($filepath.$filename));
	ob_end_flush();
	@readfile($filepath.$filename);

	unlink($filepath.$filename);
?>