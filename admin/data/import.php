<?php
	ob_start();

	require_once("../../config.inc.php"); # common config
	require_once("config.inc.php"); # common config


	//check for permission if needed
	if(!$hasImportPermission){
		include_once("../framework/no_permission.php");
	}

	if (array_key_exists("importzip", $_FILES)) {
		echo json_encode($cms->upload($_FILES["importzip"]));
		exit();
	}

	if (array_key_exists("session", $_POST)) {
		echo json_encode($cms->process($_POST["session"]));
		exit();
	}
?>
<?php require_once( $rootPath . $adminPath."/framework/overall_header.php" );?>

<body>
	<script type="text/javascript" src="<?=$adminPath?>/js/core.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/events.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/css.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/coordinates.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/drag.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/dragsort.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckeditor.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckfinder/ckfinder.js"></script>


<div id="body" style=" width:100%;height:100%;overflow: hidden;">

<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" >
<div id="listbody">
	<div style="">
		<div>
			<span style="cursor:pointer" class="pageTitle" onClick="window.location='./list.php'"><?=$cms->pageName?> -&gt; Import</span>
		</div>
   		
		<table id="editTable" >
		  <tbody>
		    <tr>
		    <td>
		      <form action="" method="post" id="myform" enctype="multipart/form-data" >
		      	<div align="right" style="position:relative; z-index:1;height:0px;"><?
					
			        ?></div>
		  	
		      <table class="formline_edit" cellspacing="0" cellpadding="6" width="100%" style="padding-bottom:5px;">
		        <tbody>
		        	<tr>
		        		<td width="300">Choose a non-encrypted zip file to import: </td>
		        		<td>
		        			<input type="file" name="importzip" id="importzip" /><input type="button" class="btnItem" id="btnUpload" value=" Upload "/>
		        		</td>
		        	</tr>

				<tr>
					<td colspan="3" align="center">
						<textarea id="panelMsg"></textarea> 
					</td>
				</tr>
			  </tbody></table>

			  <div style="width:100%;">
				
				  	<div style="float:left">
						<a href="./list.php?node_id=<?=$node_id?>"  class="btnItem" style="margin:0px 8px">Back to List Page</a>
					</div>
				 	<div style="float:right">
						<?php 
				 			if($hasExportPermission){
				 		?>
							<input type="button" id="btnUpdate" name="btnUpdate" value=" Export Data " class="btnItem submitItem" onClick="goExport(this.form)"  style="padding-left:32px;padding-right:32px;margin:0px 8px;"/> 
				 		<?php 
				 			}
				 		?>
				 	</div>	
				
				</div>

			  </form>
		</td></tr></tbody>
		
		</table>
	      <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>
	</div></div>
</div>
</div>
<!-- Modal HTML embedded directly into document -->
<div id="passwordWrapper" class="modal">
  <p>Please add a password onto the zip file.</p>
  <div><input type="text" name="password" id="password" /></div><br clear="all" />
  <div><a href="#" id="btnExport" class="btnItem">Export Now</a></div>
</div>


<script type='text/javascript' src='<?=$adminPath?>/js/jquery.modal.min.js'></script>
<link href="<?=$adminPath?>/css/jquery.modal.min.css" rel="stylesheet"/>

<script type="text/javascript" src="<?=$adminPath?>/js/data.js?<?=time()?>"></script>
</body>
</html>

<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>