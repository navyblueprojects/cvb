<?php
include_once("../../config.inc.php");
include_once("../../classes/system.php");

$cms = new DataManipulator();


//user permission
define("CREATE_PERMISSION", "DATA_IMPORT");
define("DELETE_PERMISSION", "DATA_ARCHIVE");
define("EXPORT_PERMISSION", "DATA_EXPORT");

if( !isset( $_SESSION ) ){
	session_start();
}
$hasImportPermission = (CREATE_PERMISSION==""  || CREATE_PERMISSION!="" && $_SESSION[$dbDatabase][CREATE_PERMISSION]) ? true:false;
$hasDeletePermission = (DELETE_PERMISSION==""  || DELETE_PERMISSION!="" && $_SESSION[$dbDatabase][DELETE_PERMISSION]) ? true:false;
$hasExportPermission = (EXPORT_PERMISSION==""  || EXPORT_PERMISSION!="" && $_SESSION[$dbDatabase][EXPORT_PERMISSION]) ? true:false;


$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>