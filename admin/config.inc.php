<?php
session_start();
$navHeadText = "Administration Area for website of ".$docTitle;
$navBgColor = "#D4003B";

//$adminDirName = "/admin";


$defaultLogo = "<img src=\"".$adminPath."/images/logo.gif\" width=\"120\" alt=\"\" />";

//array(display name, folder name, index page, required permission value) 
$leftNavContentItems = array("SECTIONS"=>array(
	array("Site Statistic", "", "hp.php", "", 		"isMenu"=>true),
	array("padding"=>true),
	

	array("Student", "", "", "STUDENT_VIEW", "menuTitle"=>true),
		array("Create Student", 		"student",		 	"index.php", 		"STUDENT_VIEW", 		"isSubMenu"=>true, 		 "url"=>"edit.php?id="),
		//array("Import Excel", 			"student",		 	"index.php", 		"STUDENT_CREATE", 		"isSubMenu"=>true, 		 "url"=>"import.php?node_id="),
		array("Select WIE", 				"student",		 	"index.php", 		"STUDENT_CREATE", 		"isSubMenu"=>true, 		 "url"=>"choose_wie.php?node_id="),
		//array("Student Application", 		"student",		 	"index.php", 		"STUDENT_VIEW", 		"isSubMenu"=>true, 		 "url"=>"list.php?node_id="),
		array("Student List", 			"student", 			"index.php", 		"STUDENT_VIEW", 		"isSubMenu"=>true),
	array("padding"=>true),

	array("Lecturer", "", "", "LECTURER_VIEW", "menuTitle"=>true),
		array("Create Lecturer", 		"lecturer",		 	"index.php", 		"LECTURER_VIEW", 		"isSubMenu"=>true, 		 "url"=>"edit.php?node_id="),
		//array("Import Excel", 			"lecturer",		 	"index.php", 		"LECTURER_CREATE", 		"isSubMenu"=>true, 		 "url"=>"import.php?node_id="),
		//array("Student Application", 		"student",		 	"index.php", 		"STUDENT_VIEW", 		"isSubMenu"=>true, 		 "url"=>"list.php?node_id="),
		array("Lecturer List", "lecturer", 			"index.php", 		"LECTURER_VIEW", 		"isSubMenu"=>true),
	array("padding"=>true),
	
	array("Job", "", "", "JOBS_VIEW",  "menuTitle"=>true),
		array("Add Job", "jobs", "index.php", "JOBS_VIEW",  "isSubMenu"=>true, "url"=>"edit.php"),
		array("Job Type", "jobs_type", "index.php", "JOBS_EDIT",  "isSubMenu"=>true, "url"=>"list.php"),
		// array("Job Applications", "application", "index.php", "JOBS_VIEW",  "isSubMenu"=>true, "url"=>"edit.php"),
		array("Job List", "jobs", "index.php", "JOBS_VIEW",  "isSubMenu"=>true, "url"=>"list.php"),
	array("padding"=>true),
	
	
	array("Application", "", "", "APPLICATION_VIEW",  "menuTitle"=>true),
		array("Unprocess Application ", "application", "index.php", "APPLICATION_VIEW",  "isSubMenu"=>true, "url"=>"list.php?status=OPEN"),
	array("padding"=>true),
	
	array("Web CMS", "", "", "PAGE_VIEW", "menuTitle"=>true), 
		//array("Navigation Menu", 			"navi",		"index.php", 	"BANNER_VIEW", 			"isSubMenu"=>true),
		array("Page", 			"page",		"", 	"PAGE_VIEW", 			"isSubMenu"=>true, "url"=>"list.php?type=page"),
		array("Area Text", 		"page",		"", 	"PAGE_VIEW", 			"isSubMenu"=>true, "url"=>"list.php?type=area"),
		array("CV Form Tips", 	"page",		"", 	"PAGE_VIEW", 			"isSubMenu"=>true, "url"=>"list.php?type=tips"),
	array("padding"=>true),		

	array("System Log", "", "", "LOG_VIEW", "menuTitle"=>true),	
		array("Student Login ", 			"log",		"", 	"LOG_VIEW", 			"isSubMenu"=>true, "url"=>"list.php?action=login&type=1"),
		array("Lecturer Login ", 			"log",		"", 	"LOG_VIEW", 			"isSubMenu"=>true, "url"=>"list.php?action=login&type=2"),
		array("Lecturer Job Approval", 		"log",		"", 	"LOG_VIEW", 			"isSubMenu"=>true, "url"=>"list.php?action=approval&type=2"),
		array("CV Form Generate", 			"log",		"", 	"LOG_VIEW", 			"isSubMenu"=>true, "url"=>"list.php?action=cvform&type=1"),
	array("padding"=>true),		



	array("System Configuration", "", "", "", "menuTitle"=>true),	
		array("Programme",					"programme", 		"index.php", 	"PROGRAM_VIEW",				"isSubMenu"=>true),
		array("Exam Subjects",				"subject", 			"index.php", 	"SUBJECT_VIEW",				"isSubMenu"=>true),
		array("Districts",					"district", 		"index.php", 	"DISTRICT_VIEW",			"isSubMenu"=>true),
		array("User Account",				"account", 			"index.php", 	"ACCOUNT_VIEW",				"isSubMenu"=>true),
		array("Email Template",				"email", 			"index.php", 	"EMAIL_VIEW",				"isSubMenu"=>true),
		array("Data Export",				"data", 			"index.php", 	"DATA_EXPORT",				"isSubMenu"=>true),
		array("Data Import",				"data", 			"import.php", 	"DATA_EXPORT",				"isSubMenu"=>true),
		array("File Housekeeping",          "data", 			"export_student.php",	"DATA_EXPORT",		"isSubMenu"=>true),

		array("Account Password",			"password", 		"index.php", 	"", 						"isSubMenu"=>true),
	array("padding"=>true),
		
));

if(isset($_SESSION[$dbDatabase])){
	$loginFlag = array_key_exists("loginName", $_SESSION[$dbDatabase]) ? true : false;
}else{
	$loginFlag = false;
}

if (!$loginFlag && !$IS_PUBLIC) {
	header("location: ".$adminPath."/login.php");
 	exit;
}

// $leftNavSettingItems = array("SECTIONS"=>array(
// 	array("Currency", "currency", "index.php", "CURRENCY_VIEW"),
// 	array("Home Banner", "banner", "index.php", "BANNER_VIEW"),
// 	array("Payment Setting", "payment", "index.php", "PAYMENT_VIEW"),
// 	array("Site Email", "contactus", "index.php","CONTACTUS_VIEW"),
// 	array("Site Heading", "site_heading", "index.php", "SITEHEADING_VIEW"),
// 	array("Social Media", "socialmedia", "index.php", "SOCIALMEDIA_VIEW"),
// 	array("User Accounts", "account", "index.php", ""),
// 	array("Reset Password", "password", "index.php", ""),
		
// ));


define("PERMISSION_TABLE"  	,	"role_permission"	);
define("ACCOUNT_ROLE_TABLE"	,	"account_role"		);


?>