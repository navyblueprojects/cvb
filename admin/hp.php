<?php
	
	include_once("../config.inc.php");
	include_once("../connDB.php");
	include_once("./config.inc.php");
	include_once($rootPath . $adminPath."/framework/overall_header.php");
	
?>
<head>
	<link rel="stylesheet" href="<?=$adminPath?>/css/jquery-ui.min.css" type="text/css">
	<link rel="stylesheet" href="<?=$adminPath?>/css/jquery-ui.theme.min.css" type="text/css">
	<link rel="stylesheet" href="<?=$adminPath?>/css/cms.css" type="text/css">
	<script type="text/javascript"
	src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?=$adminPath?>/js/jquery-ui.min.js"></script>

	<script type="text/javascript" src="<?=$fckEditorPath?>/ckeditor.js"></script>
	<script type="text/javascript" src="<?=$fckEditorPath?>/ckfinder/ckfinder.js"></script>
	<style>
		.adminPreview {

			display: inline-block;
			min-height: 200px;
			height:380px;
			vertical-align: middle;
			-webkit-box-shadow: 2px 2px 2px 2px #CCCCCC;
			box-shadow: 2px 2px 2px 2px #CCCCCC;
			-webkit-border-radius: 4px 4px 4px 4px;
			border-radius: 4px 4px 4px 4px;
			padding: 8px;
			margin: 8px;
			
			background: #ffffff; /* Old browsers */
			background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5)); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* IE10+ */
			background: linear-gradient(to bottom,  #ffffff 0%,#e5e5e5 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 ); /* IE6-9 */
						
		}

		.adminPreview th{
			background-color:#DDD;
		}
		
		.adminPreview table td {
			height: 18px;
			padding: 4px 0px;
			text-align: center;
		}

		.adminPreview a {
			text-decoration: none;
			color: initial;
		}
		.adminPreview a:hover {
			opacity: 0.6;
		}

		.adminPreview .title {
			font-weight: bold;
			padding:8px;
			text-align:right;
			font-size: 200%;
			color:#666;
			text-shadow: 3px 3px 3px #CCC;
		}

		.adminPreview .contentList {

			padding: 8px;
		}

		.adminPreview tr:hover>td, .adminPreview tr:hover>td a {
			background-color:#C9EDFF;
/* 			font-weight:bold; */
		}
		
		
		.adminPreview:nth-child(1) th{
		
/* 			background: #a9e4f7; /* Old browsers */ */
/* 			/* IE9 SVG, needs conditional override of 'filter' to 'none' */ */
/* 			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2E5ZTRmNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwZmI0ZTciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+); */
/* 			background: -moz-linear-gradient(top,  #a9e4f7 0%, #0fb4e7 100%); /* FF3.6+ */ */
/* 			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a9e4f7), color-stop(100%,#0fb4e7)); /* Chrome,Safari4+ */ */
/* 			background: -webkit-linear-gradient(top,  #a9e4f7 0%,#0fb4e7 100%); /* Chrome10+,Safari5.1+ */ */
/* 			background: -o-linear-gradient(top,  #a9e4f7 0%,#0fb4e7 100%); /* Opera 11.10+ */ */
/* 			background: -ms-linear-gradient(top,  #a9e4f7 0%,#0fb4e7 100%); /* IE10+ */ */
/* 			background: linear-gradient(to bottom,  #a9e4f7 0%,#0fb4e7 100%); /* W3C */ */
/* 			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9e4f7', endColorstr='#0fb4e7',GradientType=0 ); /* IE6-8 */ */
					
			background: #0675ba;
			background: -moz-linear-gradient(top, #0675ba 0%, #5da0c7 69%, #80b7d0 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0675ba), color-stop(69%,#5da0c7), color-stop(100%,#80b7d0));
			background: -webkit-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -o-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -ms-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: linear-gradient(to bottom, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0675ba', endColorstr='#80b7d0',GradientType=0 );

					
		}
		
		.adminPreview:nth-child(2) th{
		
/* 			background: #ffa84c; /* Old browsers */ */
/* 			/* IE9 SVG, needs conditional override of 'filter' to 'none' */ */
/* 			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYTg0YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjdiMGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+); */
/* 			background: -moz-linear-gradient(top,  #ffa84c 0%, #ff7b0d 100%); /* FF3.6+ */ */
/* 			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffa84c), color-stop(100%,#ff7b0d)); /* Chrome,Safari4+ */ */
/* 			background: -webkit-linear-gradient(top,  #ffa84c 0%,#ff7b0d 100%); /* Chrome10+,Safari5.1+ */ */
/* 			background: -o-linear-gradient(top,  #ffa84c 0%,#ff7b0d 100%); /* Opera 11.10+ */ */
/* 			background: -ms-linear-gradient(top,  #ffa84c 0%,#ff7b0d 100%); /* IE10+ */ */
/* 			background: linear-gradient(to bottom,  #ffa84c 0%,#ff7b0d 100%); /* W3C */ */
/* 			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffa84c', endColorstr='#ff7b0d',GradientType=0 ); /* IE6-8 */ */
							
			background: #0675ba;
			background: -moz-linear-gradient(top, #0675ba 0%, #5da0c7 69%, #80b7d0 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0675ba), color-stop(69%,#5da0c7), color-stop(100%,#80b7d0));
			background: -webkit-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -o-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -ms-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: linear-gradient(to bottom, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0675ba', endColorstr='#80b7d0',GradientType=0 );
			
		}
		
		.adminPreview:nth-child(3) th{
		
/* 			background: #fc7171; /* Old browsers */ */
/* 			/* IE9 SVG, needs conditional override of 'filter' to 'none' */ */
/* 			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjNzE3MSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQ1JSIgc3RvcC1jb2xvcj0iI2ZjNmM2YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjNkM2QiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+); */
/* 			background: -moz-linear-gradient(top,  #fc7171 0%, #fc6c6c 45%, #ff3d3d 100%); /* FF3.6+ */ */
/* 			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fc7171), color-stop(45%,#fc6c6c), color-stop(100%,#ff3d3d)); /* Chrome,Safari4+ */ */
/* 			background: -webkit-linear-gradient(top,  #fc7171 0%,#fc6c6c 45%,#ff3d3d 100%); /* Chrome10+,Safari5.1+ */ */
/* 			background: -o-linear-gradient(top,  #fc7171 0%,#fc6c6c 45%,#ff3d3d 100%); /* Opera 11.10+ */ */
/* 			background: -ms-linear-gradient(top,  #fc7171 0%,#fc6c6c 45%,#ff3d3d 100%); /* IE10+ */ */
/* 			background: linear-gradient(to bottom,  #fc7171 0%,#fc6c6c 45%,#ff3d3d 100%); /* W3C */ */
/* 			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fc7171', endColorstr='#ff3d3d',GradientType=0 ); /* IE6-8 */ */
			background: #0675ba;
			background: -moz-linear-gradient(top, #0675ba 0%, #5da0c7 69%, #80b7d0 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0675ba), color-stop(69%,#5da0c7), color-stop(100%,#80b7d0));
			background: -webkit-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -o-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -ms-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: linear-gradient(to bottom, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0675ba', endColorstr='#80b7d0',GradientType=0 );
	
		}
		
		.adminPreview:nth-child(4) th{
		
/* 			background: #98c128; /* Old browsers */ */
/* 			/* IE9 SVG, needs conditional override of 'filter' to 'none' */ */
/* 			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzk4YzEyOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM2ZmM0MDAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+); */
/* 			background: -moz-linear-gradient(top,  #98c128 0%, #6fc400 100%); /* FF3.6+ */ */
/* 			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#98c128), color-stop(100%,#6fc400)); /* Chrome,Safari4+ */ */
/* 			background: -webkit-linear-gradient(top,  #98c128 0%,#6fc400 100%); /* Chrome10+,Safari5.1+ */ */
/* 			background: -o-linear-gradient(top,  #98c128 0%,#6fc400 100%); /* Opera 11.10+ */ */
/* 			background: -ms-linear-gradient(top,  #98c128 0%,#6fc400 100%); /* IE10+ */ */
/* 			background: linear-gradient(to bottom,  #98c128 0%,#6fc400 100%); /* W3C */ */
/* 			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#98c128', endColorstr='#6fc400',GradientType=0 ); /* IE6-8 */ */
			background: #0675ba;
			background: -moz-linear-gradient(top, #0675ba 0%, #5da0c7 69%, #80b7d0 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0675ba), color-stop(69%,#5da0c7), color-stop(100%,#80b7d0));
			background: -webkit-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -o-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: -ms-linear-gradient(top, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			background: linear-gradient(to bottom, #0675ba 0%,#5da0c7 69%,#80b7d0 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0675ba', endColorstr='#80b7d0',GradientType=0 );
		
		}
		
	</style>
</head>

<body>
	<div id="body" style=" width:100%;height:100%">

		<div id="leftNav" style="min-width:200px;height:100%;">
			<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
		</div>

		<div id="mainFrame" style="">

			<div>
				<span class="pageTitle" style="cursor: pointer">Site Statistic</span>
			</div>
			<div style="padding:12px;">

				
				
				
                <div class="adminPreview" style="width:45%">
					<div class="title">
						New Job Applications
					</div>
					<?php
						$top10CountrySQL = "
							SELECT product_id, code, qty, qty_held
							FROM product_inv 
                            WHERE qty <= qty_held
							order by qty desc, code
						";
	
						$top10CountryRs = $pdo->prepare($top10CountrySQL);
						$top10CountryRs->execute();

					?>
					<div style="position: relative">
						<div style="overflow:auto; height:340px ">

							<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center">
								<tbody>
									<tr>
										<th style="padding:4px;height:40px;background-color:#FF9494;color:#fff" width="20" align="center"> # </th>
										<th style="padding:4px;height:40px;background-color:#FF9494;color:#fff" width="60" align="center"> Student ID </th>
										<th style="padding:4px;height:40px;background-color:#FF9494;color:#fff" width="120" align="center"> Job Title </th>
										<th style="padding:4px;height:40px;background-color:#FF9494;color:#fff" width="120" align="center"> Submission Time  </th>
									</tr>
									<?php
										$itemCounter=1;
										while($top10CountryRs_row = $top10CountryRs->fetch()){
											$product_id = $top10CountryRs_row["product_id"];
                                            $product_code = $top10CountryRs_row["code"];
											$qty = $top10CountryRs_row["qty"];
											$qty_held = $top10CountryRs_row["qty_held"];
											
											
												$style1 = "";
												$style2 = "";
											
									?>
										<tr>
											<td align="center" <?=$style1?>> <?=$itemCounter?> </td>
											<td align="center"> <a href="./product/edit.php?id=<?=$product_id?>" target="mainF"><?=$product_code?></a> </td>
											<td align="center"> <?=$qty?> </td>
											<td align="center"> <?=$qty_held?> </td>
										</tr>
									<?php
											$itemCounter++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				

                
                
                <? /*
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				

			
				<div class="adminPreview tall"  style="width:45%">
					<div id="tabs" style="height:100%;">
						<ul>
							<li>
								<a href="#tabs-mostcolor">Most Ordered Color</a>
							</li>
							<li>
								<a href="#tabs-mostsize">Most Ordered Size</a>
							</li>
						</ul>
						<div id="tabs-mostcolor">
							<?php

								$topColorSQL = "
									select pc.id,name, code, sum(color_id) as totalSold
									from order_item oi
									join product_color pc on pc.id = oi.color_id
									where color_id is not null
									group by color_id order by totalSold desc;
								";
			
								$topColorRs = $pdo->prepare($topColorSQL);
								$topColorRs->execute();
		
							?>
		
							<div style="position: relative">
								<div style="overflow:auto; height:320px;overflow-x:hidden ">
		
									<table width="100%" class=" orderHistoryTable " cellpadding="0" cellspacing="0" align="center" >
										<tbody>
											<tr>
												<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="20" align="center"> # </th>
												<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="120" align="center"> Color Name </th>
												<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="80" align="center"> Total Order Amount </th>
											</tr>
											<?php
											$itemCounter = 1;
											while($topColorRs_row = $topColorRs->fetch()){
												$colorId = $topColorRs_row["id"];
												$colorName = $topColorRs_row["name"];
												$colorCode = $topColorRs_row["code"];
												
												$totalSold = $topColorRs_row["totalSold"];
												
												
		
// 												if($itemCounter == 1){
// 													$style1 = " style='font-size:500%;font-weight:bold;color:#FFB047'";
// 													$style2 = " style='font-weight:bold;color:#FFB047'";
// 												}else if($itemCounter == 2){
// 													$style1 = " style='font-size:400%;font-weight:bold;color:#FFB047'";
// 													$style2 = " style='font-weight:bold;color:#FFB047'";
// 												}else if($itemCounter == 3){
// 													$style1 = " style='font-size:300%;font-weight:bold;color:#FFB047'";
// 													$style2 = " style='font-weight:bold;color:#FFB047'";
// 												}else{
// 													$style1 = "";
// 													$style2 = "";
// 												}
											?>
												<tr>
													<td align="center" <?=$style1?>><?=$itemCounter?></td>
													<td align="center"><a href="product_color/edit.php?id=<?=$colorId?>"> <?=$colorName?> (<?=$colorCode?>) </a></td>
													<td align="center"> <?=$totalSold?> </td>
												</tr>
											
											<?php 
												
													$itemCounter++;
												}
											?>
		
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div id="tabs-mostsize" >
							<?php

								$topSizeSQL = "
									select ps.id,name, sum(size_id) as totalSold
									from order_item oi
									join product_size ps on ps.id = oi.size_id
									where size_id is not null
									group by size_id
			
									order by totalSold desc;
								";
			
								$topSizeRs = $pdo->prepare($topSizeSQL);
								$topSizeRs->execute();
		
							?>
		
							<div style="position: relative">
								<div style="overflow:auto; height:320px ;overflow-x:hidden">
		
									<table width="100%" class=" orderHistoryTable" cellpadding="0" cellspacing="0" align="center" >
										<tbody>
											<tr>
												<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="20" align="center"> # </th>
												<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="120" align="center"> Size Name </th>
												<th style="padding:4px;height:40px;background-color:#A8D324;color:#fff" width="80" align="center"> Total Order Amount </th>
											</tr>
											<?php
											$itemCounter = 1;
											while($topSizeRs_row = $topSizeRs->fetch()){
												$sizeId = $topSizeRs_row["id"];
												$sizeName = $topSizeRs_row["name"];
												
												$totalSold = $topSizeRs_row["totalSold"];
												
												
		
// 												if($itemCounter == 1){
// 													$style1 = " style='font-size:500%;font-weight:bold;color:#FFB047'";
// 													$style2 = " style='font-weight:bold;color:#FFB047'";
// 												}else if($itemCounter == 2){
// 													$style1 = " style='font-size:400%;font-weight:bold;color:#FFB047'";
// 													$style2 = " style='font-weight:bold;color:#FFB047'";
// 												}else if($itemCounter == 3){
// 													$style1 = " style='font-size:300%;font-weight:bold;color:#FFB047'";
// 													$style2 = " style='font-weight:bold;color:#FFB047'";
// 												}else{
// 													$style1 = "";
// 													$style2 = "";
// 												}
											?>
												<tr>
													<td align="center" <?=$style1?>><?=$itemCounter?></td>
													<td align="center"><a href="product_size/edit.php?id=<?=$sizeId?>"> <?=$sizeName?> </a></td>
													<td align="center"> <?=$totalSold?> </td>
												</tr>
											
											<?php 
												
													$itemCounter++;
												}
											?>
		
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>*/ ?>
				</div>
				
			</div>

		</div>

	</div>

	<script>
		$(function() {
			$("#orderStatus").accordion({
				heightStyle : "content",
				collapsible: true
			});

			$("#returnOrderStatus").accordion({
				heightStyle : "content",
				collapsible: true
			});
			
			$("#tabs, #tabs2").tabs();
		});
	</script>
</body>
</html>
<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>