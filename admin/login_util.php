<?php
$IS_PUBLIC = true;
include_once("../config.inc.php");
include_once("./config.inc.php");
include_once("../connDB.php"); # common connect db

$loginName = array_key_exists('loginName', $_POST) ? $_POST["loginName"] : "";
$password = array_key_exists('password', $_POST) ? $_POST["password"] : "";

if ( ! array_key_exists('loginName', $_POST) || ! array_key_exists('password', $_POST) )
	header("location: ".$adminPath."/index.php");


if (!empty($_POST['session'])) {
    if (!hash_equals($_SESSION['token'], $_POST['session'])) {
        header("location: ".$adminPath."/index.php");
    }
}
$thissql = "SELECT `id`, `password`, `password_salt` FROM `account` WHERE sha1(`username`)=?";

$rs = $conn->prepare($thissql);
$rs = $conn->Execute($rs, [sha1($loginName)]);

$totalRecord = $rs->RecordCount();
$thisPass = "";
if($totalRecord > 0) {
	while (!$rs->EOF) {
		$thisloginName = $loginName;
		$thisPass = $rs->fields["password"];
		$thisSalt = $rs->fields["password_salt"];
		$thisId = $rs->fields["id"];
		$rs->MoveNext();
	}
} else {
// 	header("location: ".$adminPath."/index.php?e=Invalid+User+Name.&loginName=" . $loginName);
	echo "ERROR"; 
	exit;
}
$rs->close();

if  (sha1(sha1($password).$thisSalt) != $thisPass) {
// 	header("location: ".$adminPath."/index.php?e=Invalid+Password.&loginName=" . $loginName);
	echo "ERROR";
	exit;
}else {
	setcookie('loginName', $loginName, 0, '/');	
	if (!isset($_SESSION)) {
		session_start();
	}
	$_SESSION[$dbDatabase]['loginName']=$loginName;
	$_SESSION[$dbDatabase]['loginId']=$thisId;
	
	

	$accountPermRs = $pdo->prepare("SELECT rp.perm_value FROM `account_role` ar join role_permission rp on ar.perm_id = rp.id join account a on ar.role_id = a.role_id where a.id=:accountId");
	$accountPermRs->bindValue("accountId", $thisId, PDO::PARAM_INT);
	$accountPermRs->execute();
	
	while($accountPermRs_row = $accountPermRs->fetch()) {
		$permValue = $accountPermRs_row['perm_value'];
	
		$_SESSION[$dbDatabase][$permValue]=true;
	}
	
	
	
	
	
// 	header("location: ".$adminPath."/hp.php");
	echo "TRUE";
}
exit;
?>