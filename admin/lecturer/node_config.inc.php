<?php
//$relTableName = "ProductsRelationship";
$keyField = "node_id";
$level = "1";
$perPage = 20;
$pageLinks = 10;

define("DB_CATITEM_ID", "id");
define("DB_CATITEM_NAME", "name");


//custom setting
$pageName = "Member Grade";
$tableName = "member_node";

//node setting(edit it only for categories)
define("RETURN_MAIN_EDIT", "product/manage_tree.php");


$fixedField = array (
	# array( Name, DB Field Name, type, array(list status, width, sortable, searchable), array(edit form status, update, insert, default selection/value), array(language version))

	array( "ID", "node_id", "INT", array("HIDDEN", "0", true, false), array("HIDDEN", false, false, array()) ),
	array( "Category Name", "node_name", "VARCHAR", array("SHOW", "180", true, false), array("SHOW", true, true, array()) ),
	
	
);

$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>