<?php
ob_start();

require_once("../../config.inc.php"); # common config

$currversion = "";
if (isset($_version) && $_version) {
	$currversion = (array_key_exists($dbDatabase."_VERSION_LANG", $_SESSION)) ? $_SESSION[$dbDatabase.'_VERSION_LANG'] : $_version[0];
	$currversion = (array_key_exists("lang", $_POST)) ? $_POST['lang'] : $currversion;
	$currversion = (array_key_exists("lang", $_GET)) ? $_GET['lang'] : $currversion;
	$_SESSION[$dbDatabase. '_VERSION_LANG'] = $currversion;	
}

require_once("config.inc.php"); # config
require_once($rootPath . $adminPath."/config.inc.php"); # admin config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.pageiterator.php");
header('Cache-Control: no-cache');
header('Pragma: no-cache');
if (!isset($_SESSION)) {
	session_start();
}

// Set global variables if not already set
$node_id = "";
$refresh = (array_key_exists("refresh", $_GET)) ? $_GET['refresh'] : "";
if($refresh == "1") echo "<script language=\"javascript\">window.location.replace('".$_SERVER['PHP_SELF']."');</script>";
$arrVar = $conn->MetaColumns($tableName);
$arrVar = $conn->MetaColumnNames($tableName);
foreach ($arrVar as $var) {
	$$var = "";
}
if ($level == 0)
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : 1; 
else
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : "";


if($id != "") {
	$thissql = "SELECT * FROM `".$tableName."` WHERE `".$keyField."`='".$id."'";
	$rs = $conn->SelectLimit($thissql, 1, 0);
	$totalRecord = $rs->RecordCount();

	if($totalRecord > 0) {
		while (!$rs->EOF) {	
			foreach ($rs->fields as $key=>$value) {
				$$key = $value;
			}					
			$rs->MoveNext();
		}
	}
	
}

function hasChild($node_id) {
	global $conn;
	
	$thissql = "SELECT n2.* FROM `".DB_TABLE."` n1, `".DB_TABLE."` n2 WHERE n1.`node_id`='".$node_id."' AND n1.`node_left`<n2.`node_left` AND n1.`node_right`>n2.`node_right`";
	$rs = $conn->Execute($thissql);
	return ($rs->RecordCount() > 0);
}
$node_id = (array_key_exists("node_id", $_GET)) ? $_GET['node_id'] : $node_id;


$printForm = "";
$printHidden = "";

$printCreateCalendarScript = "";

if (array_key_exists("goUpload", $_GET)) {
	$cms->importXLS($_FILES["excelfile"]);
}

?>
<?php require_once( $rootPath . $adminPath."/framework/overall_header.php" );?>

<script type="text/javascript" src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery.datetimepicker.js"></script>
<link href="<?=$adminPath?>/css/calendar-tas.css" rel="stylesheet" type="text/css">
<link href="<?=$adminPath?>/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">



<?=(array_key_exists("gmap", $cms->admin_js)) ? implode("\n", $cms->admin_js["gmap"]) : ""?>


<body>
<div id="body" style=" width:100%;height:100%;overflow: hidden;">
<script>
	function goUpload(thisform){
		thisform.action = '?goUpload';
		thisform.submit();
	}
</script>
<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" >
<div id="listbody">
	<div style="">
		<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$cms->pageName?></span>
		 	 -> Import Excel
		</div>
   		
		<table id="editTable" >
		  <tbody>
		    <tr>
		    <td>
		      <form action="" method="post" id="myform" enctype="multipart/form-data" >
		      	<div align="right" style="position:relative; z-index:1;height:0px;"><?
						if (isset($_version)) { 	 
					 		foreach ($_version as $v) {
			        ?><a class="langbtn btnItem <?=($v == $currversion) ? "curr" : ""?>" onClick="golang('<?=$v?>')">Switch to <?=$v?></a><?
					  		}
					  	}
			        ?></div>
		      <div><input type="hidden" value="ok" name="refresh" /><?=$printHidden?></div>
		  	
				  	
			  	
		      <table class="formline_edit" cellspacing="0" cellpadding="6" width="100%" style="padding-bottom:5px;">
		        <tbody>
					<tr><th width="200">Upload Excel File:</th><td><input type="file" name="excelfile"/></td></tr>
					<tr><td>&nbsp;</td></tr>
			  </tbody>
				</table>
				<div style="width:100%;">
				
				  	<div style="float:left">
						<a href="./genxlt.php"  class="btnItem" style="margin:0px 8px">Download Excel Template</a>
					</div>
				 	<div style="float:right">
						<?php 
				 			if(CREATE_NEED){
				 		?>
								<input type="button" name="btnReset" value=" Reset "  class="btnItem" onClick="resetForm(this.form)" style="margin:0px 8px"/>
						  		<input type="button" id="btnUpdate" name="btnUpdate" value=" Upload " class="btnItem submitItem" onClick="this.disabled=true;goUpload(this.form)"  style="padding-left:32px;padding-right:32px;margin:0px 8px;"/> 
				 		<?php 
				 			}
				 		?>
				 	</div>	
				
				</div>
			  </form>
		</td></tr></tbody>
		
		</table>
	      <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>
	</div></div>
</div>
</div>
<script type="text/javascript">
	$(function(){

		<?=(array_key_exists("calendar", $cms->admin_js)) ? implode("\n", $cms->admin_js["calendar"]) : ""?>	
	});
			        

	$(function(){
		$("input.checkAllToggle").on("click", function(e){
			var currentCheckedStatus = $(this).prop("checked");
			$(this).parent().parent().parent().find(".checkboxContainer").find("input[type=checkbox]").prop("checked", currentCheckedStatus);
		});
	});


</script>

</body>
</html>

<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>