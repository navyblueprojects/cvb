<?php
$pageName = "User Account - >Role";
$tableName = "role";
$keyField = "id";
$level = "1";
$perPage = 20;
$pageLinks = 10;
define("PARENT_NODE_SELECT", FALSE);
define("MULTI_CATEGORY", FALSE);
define('DB_CACHE', FALSE);
define("DB_TABLE", $tableName."_node");
define("EXPORT_TO_EXCEL", FALSE);

define("EDIT_PAGE", "edit_node.php");
define("ORDER_BY_VALUE", " `name` asc");

define("DEFAULT_ERROR_MSG", "Please enter a valid value.");

$addLevelDisabled = array();
$editLevelDisabled = array();


//user permission
define("CREATE_PERMISSION", "ROLE_CREATE");
define("VIEW_PERMISSION", "ROLE_VIEW");
define("EDIT_PERMISSION", "ROLE_EDIT");
define("DELETE_PERMISSION", "ROLE_DELETE");
define("EXPORT_PERMISSION", "");
if( !isset( $_SESSION ) ){
	session_start();
}
$hasCreatePermission = (CREATE_PERMISSION==""  || CREATE_PERMISSION!="" && $_SESSION[$dbDatabase][CREATE_PERMISSION]) ? true:false;
$hasViewPermission = (VIEW_PERMISSION==""  || VIEW_PERMISSION!="" && $_SESSION[$dbDatabase][VIEW_PERMISSION]) ? true:false;
$hasEditPermission = (EDIT_PERMISSION==""  || EDIT_PERMISSION!="" && $_SESSION[$dbDatabase][EDIT_PERMISSION]) ? true:false;
$hasDeletePermission = (DELETE_PERMISSION==""  || DELETE_PERMISSION!="" && $_SESSION[$dbDatabase][DELETE_PERMISSION]) ? true:false;
$hasExportPermission = (EXPORT_PERMISSION==""  || EXPORT_PERMISSION!="" && $_SESSION[$dbDatabase][EXPORT_PERMISSION]) ? true:false;

//list & edit page button 
define("INVENTORY_NEED", FALSE);
define("INVENTORY_VALUE", "Image");
define("NEW_NEED", $hasCreatePermission);
define("EDIT_NEED", $hasEditPermission);
define("DELETE_NEED", $hasDeletePermission);
define("EXPORT_NEED", $hasExportPermission);





//language
define("SHOW_LANG_BTN", FALSE);


$fixedField = array (
	# array( Name, DB Field Name, type, array(list status, width, sortable, searchable), array(edit form status, update, insert, default selection/value))
	array( "ID", "id", "INT", array("HIDDEN", "40", false, false), array("HIDDEN", false, false, array()) ),
	array( "Name", "name", "VARCHAR", array("SHOW", "400", true, true), array("SHOW", true, true, array()) ),
	array( "Create Date", "createDate", "VARCHAR", array("HIDDEN", "100", true, false), array("HIDDEN", false, true, array()) ),
	array( "Last Updated", "lastModDate", "DATETIME", array("SHOW", "300", true, false), array("HIDDEN", true, true, array()) ), 
	array( "Permission", "", "PERMISSION", array("HIDDEN", "300", true, false), array("SHOW", true, true, array())),
);

$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>