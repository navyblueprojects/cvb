<?php
ob_start();
include_once("../../config.inc.php");
require_once($rootPath . $adminPath."/config.inc.php");
include_once("../../connDB.php");
include_once("inv_config.inc.php");

$all = (array_key_exists("all", $_POST)) ? $_POST['all'] : "";
$id = (array_key_exists("id", $_GET)) ? $_GET['id'] : "";
$node_id = (array_key_exists("node_id", $_GET)) ? $_GET['node_id'] : "";
$tableName = (array_key_exists('tableName', $_POST)) ? $_POST['tableName'] : "";
$keyField = (array_key_exists('keyField', $_POST)) ? $_POST['keyField'] : "";
$pageNo = (array_key_exists("pageNo", $_POST)) ? $_POST['pageNo'] : "";
$keyword = (array_key_exists("keyword_h", $_POST)) ? $_POST['keyword_h'] : "";
$thissql = (array_key_exists('thissql', $_POST)) ? stripslashes($_POST['thissql']) : "";

$conn->BeginTrans();

if ($id != "") {
	$rs = $conn->Execute("SELECT * FROM ".$tableName." WHERE `".$keyField."` = '".$id."'");
	$totalRecord = $rs->RecordCount();
	if($totalRecord > 0) {
		if (!$rs->EOF) {
			foreach ( $fixedField as $eachColumn ) {
				if ($eachColumn[2] == "FILE" || $eachColumn[2] == "PHOTO") {
					@unlink($uploadedFilePath."/".$rs->fields[$eachColumn[1]]);
				}
			}
		}
	}
	$conn->Execute("DELETE FROM ".$tableName." WHERE `".$keyField."` = '".$id."'");
} else {
	if ($all != 1)
		$rs = $conn->Execute($thissql." LIMIT ?, ?", array(($_POST['pageNo']-1)*$_POST['perPage'], $_POST['perPage']*1));
	else
		$rs = $conn->Execute($thissql);
	
	$i = 0;
	$totalRecord = $rs->RecordCount();
	if($totalRecord > 0) {
		while (!$rs->EOF) {	
			$thisAlbumCatId = $rs->fields[$keyField];
			$tmpdel = "del$i";
			$thisdel = (array_key_exists($tmpdel, $_POST)) ? $_POST[$tmpdel] : "";
			if(trim($thisdel) != "") {
				foreach ( $fixedField as $eachColumn ) {
					if ($eachColumn[2] == "FILE" || $eachColumn[2] == "PHOTO") {
						@unlink($uploadedFilePath."/".$rs->fields[$eachColumn[1]]);
					}
				}
				$conn->Execute("DELETE FROM ".$tableName." WHERE `".$keyField."` = '".$rs->fields[$keyField]."'");
			}
			$tmpdel = "";
			$thisdel = "";
			$i++;
			$rs->MoveNext();
		}
	}
}

$conn->Execute("OPTIMIZE TABLE `".$tableName."`");
$conn->CommitTrans();
$conn->close();

header("location: inv_list.php?pageNo=".$pageNo."&keyword=".$keyword);
ob_end_flush();
exit;
?>