<?php
$pageName = "Product Image";
$tableName = "product_img";
$keyField = "id";
$level = "1";
$perPage = 20;
$pageLinks = 10;
define("PARENT_NODE_SELECT", FALSE);
define("MULTI_CATEGORY", FALSE);
define('DB_CACHE', FALSE);
define("DB_TABLE", $tableName."Node");
define("EXPORT_TO_EXCEL", FALSE);

$parentTable = "product";
$foreignKey = "product_id";

//language
define("SHOW_LANG_BTN", FALSE);


define("ORDER_BY_VALUE", "`rank` desc, `id` desc ");
define("DEFAULT_ERROR_MSG", "Please enter a valid value.");
$fixedField = array (
	# array( Name, DB Field Name, type, array(list status, width, sortable, searchable), array(edit form status, update, insert, default selection/value))


	array( "ID", "id", "INT", array("HIDDEN", "0", false, false), array("HIDDEN", false, false, array()) ),
	array( "Rank", "rank", "INT", array("SHOW", "160", true, false), array("SHOW", true, true, array()) ),
	array( "Image(400px * 400px)", "image", "THUMBNAIL", array("SHOW", "250", true, true), array("SHOW", true, true, array()) ),
	array( "price", "price", "TEXT", array("SHOW", "200", true, true), array("SHOW", true, true, array()), array()),
	
);

$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>