/*=============================================================================
|| ##################################################################
||	phpFoX Portal
|| ##################################################################
||	
||	Copyright		: (C) 2005 The phpFoX Group a Reality Fox Creation
||	Contact		: support@phpfox.com
||	Support Forum	: http://www.phpfox.com/forum/
||
||	- phpFoX and all of its source code and files are protected by Copyright Laws. 
||
||	- The license for phpFoX permits you to install this software on a single domain (.com, .org, .net, etc.). 
||
||	- You may also not remove the "Powered By phpFoX" or any copyright screen which shows the copyright information and credits for phpFoX (RealityFOX Creations). 
||
||	- phpFoX is NOT a FREE software - http://www.phpfox.com/license
||
|| ##################################################################
=============================================================================*/

function showIt()
{	
	var item = document.getElementById('location');

	if (item.value == "United_States")
	{
		

		document.getElementById('state').disabled=false;
		document.getElementById('zip').disabled=false;	
	}
	else
	{
		document.getElementById('state').disabled=true;
		document.getElementById('zip').disabled=true;	
	}
}

function bookmark(url, description)
	{
	netscape="Gecko User's hit CTRL+D to add a bookmark to this site."
	if (navigator.appName=='Microsoft Internet Explorer')
		{
		window.external.AddFavorite(url, description);
		}
	else if (navigator.appName=='Netscape')
		{
		alert(netscape);
		}
	}


function popUp2(URL,WIDTH,HEIGHT,SCROLL,LEFT,TOP) 
	{
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "','scrollbars="+ SCROLL +",width="+ WIDTH +",height="+ HEIGHT +",left = "+ LEFT +",top = "+ TOP +"');");
	}

/*###############################
	PROGRESS BAR
###############################*/

var progressEnd;		
var progressInterval;	
var progressAt;
var progressTimer;

function do_prog(item1,item2) 
{
	progressEnd = item1;		
	progressInterval = item2;	
	progressAt = progressEnd;
	progress_update();
}

function progress_clear() 
{
	for (var i = 1; i <= progressEnd; i++) document.getElementById('progress'+i).className = 'pbar1';
	progressAt = 0;
}

function progress_update() 
{
	progressAt++;
	if (progressAt > progressEnd) progress_clear();
	else document.getElementById('progress'+progressAt).className = 'pbar2';
	progressTimer = setTimeout('progress_update()',progressInterval);
}

function progress_stop() 
{
	clearTimeout(progressTimer);
	progress_clear();
}

/*###############################
	MENU MOUSEOVER
###############################*/

function linkon1(id1)
{
	document.getElementById(id1).className='menu5b';

}

function linkon2(id1)
{
	document.getElementById(id1).className='menu5a';

}


/*###############################
	COOKIES
###############################*/


function SetCookie(cookieName,cookieValue,nDays) 
{
	var today = new Date();
	var expire = new Date();
 	if (nDays==null || nDays==0) nDays=1;
 	expire.setTime(today.getTime() + 3600000*24*nDays);
 	document.cookie = cookieName+"="+escape(cookieValue)+";expires="+expire.toGMTString();
}

function deleteCookie(name, path, domain)
{
    if (getCookie(name))
    {
        document.cookie = name + "=" + 
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            "; expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
}


function getCookie(name)
{
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1)
    {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1)
    {
        end = dc.length;
    }
	alert(dc);
    return unescape(dc.substring(begin + prefix.length, end));
}



/*###############################
	HIDE MENUS
###############################*/

if (document.getElementById) { 
	document.write('<style type="text/css">\n')
	document.write('.submenu{display:;}\n')
	document.write('</style>\n')
}


function SwitchMenu(obj,id2)
{
	if(document.getElementById) {
	var el = document.getElementById(obj);
	
		if(el.style.display == "none") { 
			document.getElementById(id2).innerHTML='[-]';
			el.style.display = "";
			SetCookie(obj,"","-1"); 
		} else { 
			document.getElementById(id2).innerHTML='[+]';
			el.style.display = "none";
			SetCookie(obj,1,0); 
		}
	}
}


function SwitchMenu2(obj,id2,id3,id4,id5,id6,id7,id8)
{
	if(document.getElementById) {
	var el = document.getElementById(obj);
	
		if(el.style.display == "none") { 
			document.getElementById(id6).innerHTML=id7;
			document.getElementById(id2).src=id5;
			el.style.display = "";
			SetCookie(obj,"","-1"); 
			document.getElementById(id3).style.width='70%';
		} else { 
			document.getElementById(id6).innerHTML=id8;
			document.getElementById(id2).src=id4;
			el.style.display = "none";
			SetCookie(obj,1,0); 
			document.getElementById(id3).style.width='100%';
		}
	}
}




/*###############################
	GO TO LOCATION / FRAMES
###############################*/
function go_home(link)
{
	if (top.frames.length == 0) {
    		document.cookie = "newURL=" + escape(document.URL) + "; path=/;"
    		ver = parseInt(navigator.appVersion, 10);
    		if ( ((navigator.appName == "Netscape") && (ver >= 3)) ||
         		((navigator.appName == "Microsoft Internet Explorer") && (ver >= 4)) )
      			location.replace(link);
    		else
	      location = link;
    		};
}



/*###############################
	NO RIGHT CLICK
###############################*/
function clickIE() 
	{
	if (document.all) 
		{
		return false;
		}
	}

function clickNS(e) 
	{
	if (document.layers||(document.getElementById&&!document.all)) 
		{
		if (e.which==2||e.which==3) 
			{
			return false;
			}
		}
	}


function noCLICK()
{
	
	if (document.layers) {
		document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;
	} else {
	document.onmouseup=clickNS;document.oncontextmenu=clickIE;
	}
	document.oncontextmenu=new Function("return false")
}



function jumpTo(list)
{
	var IDS = document.getElementById(list);
	
	if (IDS.value != "") {
		window.location.href=IDS.value;
	} 
	
	return false;
}






function image1(img,height,width)
{
	if (getCookie('FOX_EN')) { showInfo3(img,width,height); }
	return false;
}


function image2()
{
	if (getCookie('FOX_EN')) { nd(); }
	return false;
}


function image3(type)
{
	if (getCookie('FOX_EN')) {
		SetCookie("FOX_EN","","-1");
	} else {
		SetCookie("FOX_EN",1,0);
	}	
}




/*###############################
	IMAGE FADER
###############################*/
function initImage(ids) {
  imageId = ids;
  image = document.getElementById(imageId);
  setOpacity(image, 0);
  image.style.visibility = 'visible';
  fadeIn(imageId,0);
}


function setOpacity(obj, opacity) {
  opacity = (opacity == 100)?99.999:opacity;
  
  // IE/Win
  obj.style.filter = "alpha(opacity:"+opacity+")";
  
  // Safari<1.2, Konqueror
  obj.style.KHTMLOpacity = opacity/100;
  
  // Older Mozilla and Firefox
  obj.style.MozOpacity = opacity/100;
  
  // Safari 1.2, newer Firefox and Mozilla, CSS3
  obj.style.opacity = opacity/100;
}


function fadeIn(objId,opacity) {
  if (document.getElementById) {
    obj = document.getElementById(objId);
    if (opacity <= 100) {
      setOpacity(obj, opacity);
      opacity += 10;
      window.setTimeout("fadeIn('"+objId+"',"+opacity+")", 100);
    }
  }
}



/*###############################
	INSERT TEXT AT CURSOR
###############################*/
function insertAtCursor(myField, myValue, type) 
{
	if ( displayIt == "True" )
	{
		var newValue = myValue.replace(":","");
		var mainValue = newValue.replace(":","");
		var html = '<img src="'+ SciptHome +'/images/smile/emo/'+ mainValue +'.gif">';
		tinyMCE.execCommand('mceInsertContent', false, html);
		return;
	}
	else
	{
		if (document.selection) 
		{
			myField.focus();
			if (type == 1) {
			sel = opener.document.selection.createRange();
		} else {
			sel = document.selection.createRange();
		}
		sel.text = myValue;
	}
	else if (myField.selectionStart || myField.selectionStart == '0') 
	{
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)
		+ myValue
		+ myField.value.substring(endPos, myField.value.length);
		myField.focus();
	} 
	else 
	{
		myField.value += myValue;
		}
	}
}

function checkAll(formName, leading) {
	var firstClick = "";
	var leadLen = leading.length;
	with(document.forms[formName]) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,leadLen) == leading) {
				firstClick = (firstClick == "") ? !thiselm.checked : firstClick;
				thiselm.checked = firstClick;
			}
		}
	}
}

function checkInverse(formName, leading) {
	var leadLen = leading.length;
	with(document.forms[formName]) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,leadLen) == leading) {
				thiselm.checked = !thiselm.checked;
			}
		}
	}	
}

function showInfo5(name) 
{
	var newhtml = ''+name+'';

	return overlib(newhtml,WRAP,BGCOLOR,"#00004D",FGCOLOR,"#F6F6F6",TEXTSIZE,"10px",BORDER,2,CELLPAD,5)
}

function showInfo4(name,location,mood) 
{
	var newhtml = '<img src="'+location+'" alt="" width="18" height="12" style="vertical-align:middle;" /> '+name+' '+mood+'';

	return overlib(newhtml,WRAP,BGCOLOR,"#00004D",FGCOLOR,"#F6F6F6",TEXTSIZE,"10px",BORDER,2,CELLPAD,5)
}
	
function showInfo2(name) 
{
	var newhtml = ''+name+'';

	return overlib(newhtml,WRAP,BGCOLOR,"#00004D",FGCOLOR,"#F6F6F6",TEXTSIZE,"10px",BORDER,2,CELLPAD,5)
}

$(function(){
	$('.showlog').on('click', function(){
		if ($('.showlog').text() == 'Show') {
			$('.logs').show();
			$('.showlog').text('Hide');
		} else {
			$('.logs').hide();
			$('.showlog').text('Show');
		}
	});
});
