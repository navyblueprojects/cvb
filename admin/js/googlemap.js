$(document).ready(function(){
	var map = $('#map'), ms = []
	map.tinyMap({
		'center' : [ '22.319321466742593', '114.16933178901672' ],
		'zoom' : 17,
		// 綁定地圖事件
		'event' : {
			// 滑鼠右鍵點擊
			'rightclick' : function(e) {

				// 限制建立兩個位置
				if (1 > ms.length) {
					// 將點擊位置儲存至陣列
					ms.push(e.latLng);
					$(".GOOGLEMAP").val(e.latLng);
					// 並建立標記
					map.tinyMap('modify', {
						'marker' : [ {
							'addr' : e.latLng
						} ]
					});

				}
			}
		}
	});
	if($(".GOOGLEMAP").val() != ""){
		var Arrleg = $(".GOOGLEMAP").val().replace('(','').replace(')','').split(",")
		var x = Arrleg[0];
		var y = Arrleg[1];
		$('#map').tinyMap('panto', {lat: x, lng: y});
		$('#map').tinyMap('modify',{
		    marker: [{
		    	addr: {lat: x, lng: y}
		    }]
		});	
	}
	$('#clear').on('click', function() {
		ms = [];
		draw = false;
		map.tinyMap('clear');
		$('#info').empty();
	});
})

