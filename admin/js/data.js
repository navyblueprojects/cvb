$(function(){
	$("input.checkAllToggle").on("click", function(e){
		var currentCheckedStatus = $(this).prop("checked");
		$(this).parent().parent().parent().find(".checkboxContainer").find("input[type=checkbox]").prop("checked", currentCheckedStatus);
	});
	$('input#checkAll').on("click", function(e){
		$(".checkAllToggle").prop('checked', e.target.checked);	
		$(".checkboxContainer").find("input[type=checkbox]").prop('checked', e.target.checked);
	});

	$('#btnExport').on('click', function(e){
		data = $('#myform').serializeArray();
		data.push({name: 'password', value: $('#password').val()});

		$('#loadingWrapper').modal();

		$.ajax({
			url: 'export.php',
			method: 'post',
			data: data,
			dataType: 'json'
		}).done(function(data){
			$.modal.close();
			window.location = 'getfile.php?session='+data.session;
		}); 
	});

	$('#btnExportX').on('click', function(e){
		data = $('#myform').serializeArray();
		data.push({name: 'password', value: $('#password').val()});

		$('#loadingWrapper').modal();

		$.ajax({
			url: 'export_student.php',
			method: 'post',
			data: data,
			dataType: 'json'
		}).done(function(data){
			$.modal.close();
			window.location = 'getfile.php?session='+data.session;

		}); 
	});

	$('#btnUpload').on('click', function(){
		return uploadFile();
	});
});

function uploadFile() {
	var file;

	if ($('#importzip')[0].files.length != 1) {
		alert('Please choose a zip file');
		return;
	} else {
		var file = $('#importzip')[0].files[0];
		if (file.type != 'application/zip'){
			alert('Please choose a zip file');
			return;
		}
	}

	var formData = new FormData();
	formData.append("importzip", file);
	formData.append('action', 'uploadFile');
	$('#panelMsg').append('Uploading '+file.name+'...');
	$.ajax({
		url: './import.php',
		type: 'POST',
		cache: false,
		data: formData,
		dataType: 'JSON',
		processData: false,
		contentType: false,
		xhr: function(){
			var xhr = $.ajaxSettings.xhr();
			xhr.addEventListener('progress', function(e) { }, false);
			if (xhr.upload) {
				xhr.upload.onprogress = function(evt) {
					var percentage = parseInt(evt.loaded / evt.total * 100);
					
				};
			}
			return xhr;
		}
	}).done(function(data) {
		if (data.status != 1) {
			$('#panelMsg').append('Error! '+ data.msg);

		} else {
			$('#panelMsg').append('Done! ' + "\n");
			setTimeout("progressFile('"+data.session+"')", 1000);
		}
	});	
}

function progressFile(session_id) {
	$.ajax({
		url: './import.php',
		type: 'POST',
		cache: false,
		data: {session: session_id},
		dataType: 'JSON'
	}).done(function(data) {
		$('#panelMsg').append("["+new Date().toUTCString()+"] "+ data.msg + "\n");
		if (data.status != 1) {
			setTimeout("progressFile('"+session_id+"')", 5000);
		}
	});	
}

function goExport(thisform) {
	var password = '';
	$('#passwordWrapper').on($.modal.BEFORE_CLOSE, function(){
	}).modal();
}



