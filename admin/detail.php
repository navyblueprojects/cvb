<?php
  include_once("config.inc.php");
  include_once("connDB.php");
  require_once("classes/product.php");

  if (!array_key_exists("ns", $_GET)){
    header("location: /index.php");
    exit;
  }

  $rsP = new Product();
  $prod = $rsP->getDetails(array("namespace='".$_GET["ns"]."'"));
  $plist = $rsP->getRelatedProducts($prod["id"], 5);
  
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="http://<?=$domain?>/"></base>
<title><?=$prod["name"]?> - 金多榴槤貓山王|香港極品榴槤及榴槤產品|網上訂講</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/style-responsivator.css" rel="stylesheet" type="text/css" />
<meta name="description" content="貓山王是榴槤中的極品之一，其特點是果核細小和擁有豐富果肉，果肉金黃，味道香清，甘甜幼滑，部份果肉有甜中帶苦的餘韻，那種回甘的味道像XO干邑的甘香口感特佳，叫人一試難忘，令你愛不釋手！" />
<meta name="keywords" content="durian hong kong hk hongkong 貓山王 榴槤 香港" />
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/cart.js"></script>

<script src="z/asset/aigens.js"></script>
</head>

<body data-product-id="<?=$prod["id"]?>">
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-558244eb296393d6" async="async"></script>
  <div id="wrapper">

		<?php include_once("include_header.php"); ?>    	
            
        <!--header-->
        <div class="header-space"></div>
        <div id="location-path">
                    <ul class="list-inline">
                      <li><a href="./">主頁</a></li>
                      <li class="sp-arrow"> > </li>
                      <li><a href="../list/<?=$prod["cat"]["node_name"]?>"><?=$prod["cat"]["node_name"]?></a></li>
                      <li class="sp-arrow"> > </li>
                      <li><?=$prod["name"]?></li>
                    </ul>
                </div>
        <div id="detail">
            <div class="counter">
                <div class="detail-image">
                    <?php
                    $class = 'show';
                    $i = 0;
                    if ($prod['photos']) {
                      foreach ($prod["photos"] as $pp) {
                    ?><div class="img img-<?=$i++?> <?=$class?>" ><img src="<?=$pp["path"]?>" width="<?=$pp["size"][0]?>" height="<?=$pp["size"][1]?>" alt="<?=$prod["name"]?>" title="<?=$prod["name"]?>"/></div><?
                      $class="";
                      }
                    }
                    ?>
                    <div class="img-album">
                        <ul>
                            <?
                              $i = 0;
                              if ($prod['photos']) {
                              foreach ($prod["photos"] as $pp) {
                            ?><li><a href="#" rel="<?=$i++?>" style="background-image:url(<?=$pp["path"]?>)"><div class="border"></div></a></li><?
                              }
                            }
                            ?>
                        </ul>    
                    </div>
                    <!--img-album-->
                </div>
                <!--product-image-->
                <div class="detail-content">
                    <h1 ><?=$prod["name"]?></h1>
                    <div class="addthis_native_toolbox"></div>
                      <p class="content">
                        <?=nl2br($prod["description"])?>	
                    </p>
                    <div class="add-to-cart-section">
                      <?
                      if ($prod["price_caption"]) {
                      ?><div class="row detail-row">
                          <div class="col-md-3 col-sm-3 col-xs-3" >
                            <div class="detail-discount-tag">價錢</div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-3">
                            <div class="detail-discount-price"  style="white-space:nowrap;"><?
                            if ($prod["discount_caption"]) {
                            ?><span class="hidden-xs" ><del><?=$prod["price_caption"]?></del>&nbsp;&nbsp;</span><?
                            } else {
                            ?><span class="hidden-xs" ><?=$prod["price_caption"]?>&nbsp;&nbsp;</span><?
                              
                            }
                            ?><?=$prod["discount_caption"]?></div>
                          </div>
                      </div>
                      <?
                      }
                      if ($prod["inventory"] > 0 && $prod["salesFlag"]) {
                      ?><div class="row detail-row">
                          <div class="col-md-3 col-sm-3 col-xs-3" >
                            <div class="detail-discount-tag">數量</div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-3">
                            <select class="form-control" id="qty">
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                            </select>
                          </div>
                      </div><?
                      }
                      ?>
                    </div>

                    <?
                      if ($prod["inventory"] > 0 && $prod["salesFlag"]) {
                      ?><div class="detail-btn-wrap">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="add-to-cart-btn pull-right" id="addToCart" >
                          加入購物籃
                        </div>
                      </div>
                       <div class="col-md-6 col-sm-6 col-xs-6">
                        <a class="back-btn" href="/list/<?=$prod["cat"]["node_name"]?>">回上頁</a>
                      </div>
                    </div><?
                    }
                  ?>
                   
                </div>
                <!--detail-content-->

            </div>
            <div align="left" id="list-wrap">
             <div class="container">
                <h2>其他<?=$prod["cat"]["node_name"]?></h2>
                <div class="row list-item-row-padding" style="margin:0px; padding-bottom:60px">
                        <?php
                        foreach ($plist as $p) {
                        ?><div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="item-wrap">
                                <div class="mini-image"><a href="../product/<?=$p["namespace"]?>" style="background-image:url(<?=$p["photo1"]["path"]?>);" class="img-responsive"></a></div>
                            </div>
                            <div>
                                <div class="col-md-12 white-bg-color">
                                    <div class="list-item-content-wrap">
                                        <a href="../product/<?=$p["namespace"]?>"><div class="list-item-title"><?=$p["name"]?></div></a>
                                    </div>
                                    <div class="list-item-price-wrap">
                                        <div class="list-item-price">
                                            <ul class="list-inline">
                                              <?
                                              if ($p["price_caption"]) {
                                              if ($p["discount"] == 0 || $p["discount"] == $p["price"]) {
                                                ?><li class="normal-tag">零售價</li><li class="normal-price"><?=$p["price_caption"]?></li><?
                                              } else {
                                                ?><li class="discount-tag">會員價</li><li class="discount-price"><del ><?=$p["price_caption"]?></del> <?=$p["discount_caption"]?></li><?
                                              }
                                              }
                                              ?>
                                              
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><?php
                        }
                        ?>
                    </div>
                   </div>
            </div>
        </div>
        <!--/detail-->
        <div class="detail-sub-title">
        	<div class="counter">
            	<img src="images/test_detail_02.jpg" alt="金多榴槤產品" title="金多榴槤產品" />
                <h3>換領辦法：</h3>
                <ol>
                	<li>
                    	自取：火炭坳背灣街 61-63號盈力工業中心 7樓12-15室<br/>
						電話：2243 5678<br/>
                        <strong>營業時間</strong><br/>
                        星期一至五：早上 10時至下午5時<br/>
                        星期六：早上 10時至下午１時<br/>
                        注意：敬請自備保溫購物袋
					</li>
                    <li>
                    	送貨安排：<br/>
                            <p>購買貨品滿$800，可享有免費送貨服務 (不包括離島及偏遠地區) 。</p>
                            <p>如未滿$800，需另加$100運輸費，或可親臨本公司自取。</p>
                            <p>送貨日期為星期一至星期六 (9:30am - 5:00pm)</p>
                    </li>           
                </ol>
                <h3>付款方法：</h3>
                <ol>
                	<li style="list-style:none;">
                    	~ 購買榴槤產品，可使用網上訂購，以paypal付款<br/>
						~ 購買榴槤，需先電話訂購，於銀行預付。請付款至：<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;銀行： Bank of China<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;帳戶名稱： luck triumph ltd<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;帳號： 012-688-00091133
					</li>
        
                </ol>
            </div>
        </div>
        <div id="footer">
        	<div class="btn-email"><a href="mailto:info@navyblue.hk"></a></div>
            <div class="phone-no">詳細查詢請至電 2243 - 5678</div>
            <div class="web-site"><a href="http://www.kamdor.com.hk/">http://www.kamdor.com.hk</a></div> 
        
        </div>
        <!--footer-->
    </div>
    <!--wrapper-->

<script type="text/javascript">
/* <![CDATA[ */
(function(){try{var s,a,i,j,r,c,l=document.getElementsByTagName("a"),t=document.createElement("textarea");for(i=0;l.length-i;i++){try{a=l[i].getAttribute("href");if(a&&a.indexOf("/cdn-cgi/l/email-protection") > -1  && (a.length > 28)){s='';j=27+ 1 + a.indexOf("/cdn-cgi/l/email-protection");if (a.length > j) {r=parseInt(a.substr(j,2),16);for(j+=2;a.length>j&&a.substr(j,1)!='X';j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}j+=1;s+=a.substr(j,a.length-j);}t.innerHTML=s.replace(/</g,"&lt;").replace(/>/g,"&gt;");l[i].setAttribute("href","mailto:"+t.value);}}catch(e){}}}catch(e){}})();
/* ]]> */
</script>
</body>
</html>