<?php
include_once("../config.inc.php");
include_once("./config.inc.php");

if( !isset( $_SESSION ) ) { ob_start(); session_start(); ob_end_clean(); }
include_once($pathDocRoot."/connDB.php"); 
$_SESSION = array();
session_destroy();
unset($_SESSION);
header("location: ".$adminPath."/index.php");
exit;
?>