<?php
include_once("../../config.inc.php");
include_once("../../classes/application.php");
$cms = new Application();
$tableName = "application";
// $relTableName = "ProductsRelationship";
$keyField = "id";
$level = "1";
$perPage = 20;
$pageLinks = 10;
define("PARENT_NODE_SELECT", FALSE);
define("MULTI_CATEGORY", FALSE);
define('DB_CACHE', FALSE);
define("DB_TABLE", $tableName."_node");
define("EXPORT_TO_EXCEL", TRUE);

define("EDIT_PAGE", "edit_node.php");
define("ORDER_BY_VALUE", " `createDate` desc");

//edit page - error message
define("DEFAULT_ERROR_MSG", "Please enter a valid value.");

//items category 
$addLevelDisabled = array();
$editLevelDisabled = array();

//user permission
define("CREATE_PERMISSION", "APPLICATION_CREATE");
define("VIEW_PERMISSION", "APPLICATION_VIEW");
define("EDIT_PERMISSION", "APPLICATION_EDIT");
define("DELETE_PERMISSION", "APPLICATION_DELETE");
define("EXPORT_PERMISSION", "APPLICATION_EXPORT");
if( !isset( $_SESSION ) ){
	session_start();
}
$hasCreatePermission = (CREATE_PERMISSION==""  || CREATE_PERMISSION!="" && $_SESSION[$dbDatabase][CREATE_PERMISSION]) ? true:false;
$hasViewPermission = (VIEW_PERMISSION==""  || VIEW_PERMISSION!="" && $_SESSION[$dbDatabase][VIEW_PERMISSION]) ? true:false;
$hasEditPermission = (EDIT_PERMISSION==""  || EDIT_PERMISSION!="" && $_SESSION[$dbDatabase][EDIT_PERMISSION]) ? true:false;
$hasDeletePermission = (DELETE_PERMISSION==""  || DELETE_PERMISSION!="" && $_SESSION[$dbDatabase][DELETE_PERMISSION]) ? true:false;
$hasExportPermission = (EXPORT_PERMISSION==""  || EXPORT_PERMISSION!="" && $_SESSION[$dbDatabase][EXPORT_PERMISSION]) ? true:false;

//list & edit page button 
define("INVENTORY_NEED", FALSE);
define("INVENTORY_VALUE", "Image");
define("NEW_NEED", $hasCreatePermission);
define("EDIT_NEED", $hasEditPermission);
define("DELETE_NEED", $hasDeletePermission);
define("EXPORT_NEED", $hasExportPermission);

//language
define("SHOW_LANG_BTN", TRUE);



$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>