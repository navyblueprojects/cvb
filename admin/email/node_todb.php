<?php
ob_start();
include_once("../../config.inc.php"); # common config
require_once($rootPath . $adminPath."/config.inc.php");
require_once("node_config.inc.php"); # config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/class.uploadfile.php");
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.file.php");
header('Cache-Control: no-cache');
header('Pragma: no-cache');
session_start();
$currversion = "";
if ($_version) {
	$currversion = (array_key_exists($dbDatabase."_VERSION_LANG", $_SESSION)) ? $_SESSION[$dbDatabase.'_VERSION_LANG'] : $_version[0];
}



$fileUpload = new file_upload;
$fileUpload->upload_dir = $uploadedFilePath . "/"; // "files" is the folder for the uploaded files (you have to create this folder)
//$fileUpload->extensions = array(".png", ".zip", ".pdf", ".jpg"); // specify the allowed extensions here
$fileUpload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
$fileUpload->rename_file = true;

foreach ( $fixedField as $eachColumn ) {
	$eachField = ($eachColumn[5] && $currversion != "") ? $eachColumn[1].'_'.$currversion : $eachColumn[1];
	
	if ( $eachColumn[4][0] != "HIDDEN" ) {
		$currversion = (array_key_exists($dbDatabase."_VERSION_LANG", $_SESSION)) ? $_SESSION[$dbDatabase.'_VERSION_LANG'] : $_version[0];
		switch ($eachColumn[2]) {
		case "FILE":
		case "PHOTO":
		case "THUMBNAIL":
			$$eachField = (array_key_exists($eachField, $_FILES)) ? $_FILES[$eachField] : "";
			$varName = "fileDel_".$eachField;
			$varFileName = "filename_".$eachField;
			$$varName = (array_key_exists($varName, $_POST)) ? $_POST[$varName] : "";
			$$varFileName = (array_key_exists($varFileName, $_POST)) ? $_POST[$varFileName] : "";
			break;
		case "CATEGORY_MULTI":
		case "FOREIGN_MULTI":
		case "MULTISELECT":
		case "CHECKBOX":
		case "CHECKBOX_FOREIGN":
			$$eachField = $_POST[$eachField];
			break;
		case "JSON":  
			$$eachField = array(); 
			$aArray = $_POST[$eachField.'_'.array_shift(array_values($eachColumn[5])) ]; 
			for($i=0 ;$i< sizeof($aArray); $i++){
				$fieldArray= array();
				$haveValue = false;
				foreach($eachColumn[5]  as $eachFieldName){  
					$value = str_replace(array("\\'", "\\\""), array("'", "\""), $_POST[$eachField.'_'.$eachFieldName][$i]);	
					$value = str_replace(array("\\"), array('\\\\'), $value);	
					$fieldArray[$eachFieldName] =   htmlspecialchars($value, ENT_QUOTES,'UTF-8') ;
					if($_POST[$eachField.'_'.$eachFieldName][$i]!=""){
						$haveValue=true;
					}
				} 
				if($haveValue){
					array_push($$eachField,$fieldArray);
				}
			} 
			 
			break;
		default:
			$value = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $_POST[$eachField]);		 
			$$eachField = array_key_exists($eachField, $_POST) ? htmlspecialchars($value, ENT_QUOTES,'UTF-8') : "";
			break;
		}
	} else {
		$value = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $_POST[$eachField]);
		$$eachField = array_key_exists($eachField, $_POST) ? htmlspecialchars($value, ENT_QUOTES,'UTF-8') : "";
	}
}

$thissql = "SELECT `".$keyField."` FROM `".$tableName."` WHERE 1 AND `".$keyField."`='".$$keyField."'";
$rs = $conn->Execute($thissql);
$recordFound = ($rs->RecordCount() > 0) ? true : false;

$conn->debug = true;
if (!$recordFound) {
	$thumbnail = "";
	$isiid = 0;
	foreach ( $fixedField as $eachColumn ) {
		$eachField = ($eachColumn[5] && $currversion != "") ? $eachColumn[1].'_'.$currversion : $eachColumn[1];	
		
		if ( $eachColumn[4][2] ) {			
			switch ($eachColumn[2]) {
				case "FILE":
					$fileUpload->the_temp_file = "";
					$fileUpload->the_file = "";
					$fileUpload->http_error = "";
					$fileUpload->file_copy = "";
					foreach ( $_FILES[$eachField] as $varName => $varValue ) {
						if ($varName == "tmp_name")
							$fileUpload->the_temp_file = $varValue;
						if ($varName == "name")
							$fileUpload->the_file = $varValue;
						if ($varName == "error")
							$fileUpload->http_error = $varValue;
					}
					$fileUpload->replace = "n";
					$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
					$fileUpload->upload();
					$fields[$eachField] = $fileUpload->file_copy;				
					break;
				case "PHOTO":
				case "THUMBNAIL":
					$fileUpload->the_temp_file = "";
					$fileUpload->the_file = "";
					$fileUpload->http_error = "";
					$fileUpload->file_copy = "";
					foreach ( $_FILES[$eachField] as $varName => $varValue ) {
						if ($varName == "tmp_name")
							$fileUpload->the_temp_file = $varValue;
						if ($varName == "name")
							$fileUpload->the_file = $varValue;
						if ($varName == "error")
							$fileUpload->http_error = $varValue;
					}
					$fileUpload->replace = "n";
					$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
					$fileUpload->upload();
					$fields[$eachField] = $fileUpload->file_copy;
					if (!empty($eachColumn[4][3])) {
						imgActualResize($uploadedFilePath,$fields[$eachField],$uploadedFilePath,$fields[$eachField],$eachColumn[4][3][0],$eachColumn[4][3][1]);
						if (!empty($eachColumn[4][3][2])) {
							if (!empty($eachColumn[4][3][3])) {
								addWaterMark($uploadedFilePath."/".$fields[$eachField], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0, 25, $eachColumn[4][3][3]);
							} else
								addWaterMark($uploadedFilePath."/".$fields[$eachField], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0);
						}
					}
					if (!empty($eachColumn[4][4]) and $fields[$eachField] != "") 
					{
						$sizeIndex = 0;
						foreach ($eachColumn[4][4] as $sizeImg)
						{
							$thumbnailFileName = split("[.]", $fields[$eachField]);
							$thumbnail = $thumbnailFileName[0]."_t".$sizeIndex++.".".$thumbnailFileName[1];
							$file = new File();
							$file->copy_file($uploadedFilePath."/".$fields[$eachField], $uploadedFilePath."/".$thumbnail);
							$photoPath1 = $thumbnail;
							imgActualResize($uploadedFilePath, $photoPath1, $uploadedFilePath, $photoPath1, $sizeImg[1], $sizeImg[2], 100);
							$fields[$sizeImg[0]] = $thumbnail;
						}
					}
					break;
				case "CATEGORY_MULTI":
				case "FOREIGN_MULTI":
					$tmpStr = "";
					foreach($$eachField as $eachValue)
						if(trim($eachValue) != "")
							$tmpStr .= $eachValue.";";
					$fields[$eachField] = ($tmpStr!="") ? $tmpStr : "1;";
					break;
				case "MULTISELECT":
				case "CHECKBOX_FOREIGN":
				case "CHECKBOX":
					$tmpStr = "";
					foreach($$eachField as $eachValue)
						if(trim($eachValue) != "")
							$tmpStr .= $eachValue.";";
					$fields[$eachField] = ($tmpStr!="") ? $tmpStr : "";
					break;
				case "JSON": 
					$tmpStr='[';  
					foreach($$eachField as $eachItem)
					{
						
						$tmpStr.=	($tmpStr!="[") ? "," : ""; ;
						$thisObjStr = '{';
						foreach($eachItem  as $eachFieldName =>$eachFieldValue)
						{   
							$thisObjStr.= ($thisObjStr!="{") ? "," : ""; 
							$thisObjStr .='"'.$eachFieldName.'"  : "'.$eachFieldValue.'"';
						}
						$thisObjStr .='}';
						$tmpStr .=$thisObjStr; 
					}
					$tmpStr.=']';
					$fields[$eachField] = ($tmpStr!="") ? ($tmpStr) : ""; 
					//echo $fields[$eachField];
					break;
					
				case "PASSWORD_MD5":
				
					if(!empty($$eachField)){
						$salt = rand(0,999);
						$fields[$eachField] = md5(md5($$eachField).$salt);
						$fields[$eachField."_salt"] = $salt;
					}else{
					}
					break;
				case "PASSWORD":
					if(!empty($$eachField)){
						$fields[$eachField] = md5($$eachField);
					}
					break;
				case "PERMISSION":
				
					$account_role_table = ACCOUNT_ROLE_TABLE;
				
					$submittedRoles = $_REQUEST["role"];
				
					//delete all
					$deleteRoleRs = $pdo->prepare("delete from `".$account_role_table."` where role_id = :role_id;");
					$deleteRoleRs->bindValue("role_id", $id, PDO::PARAM_INT);
					$deleteRoleRs->execute();
						
					//add all
					foreach($submittedRoles as $submittedRole){
						$insertSQL = '
							INSERT INTO '.$account_role_table.' ( `perm_id`, `role_id`) VALUES
																(:perm_id, :role_id)
							';
						$insertRoleRs = $pdo->prepare($insertSQL);
						$insertRoleRs->bindValue("role_id", $id, PDO::PARAM_INT);
						$insertRoleRs->bindValue("perm_id", $submittedRole, PDO::PARAM_INT);
						$insertRoleRs->execute();
					}
					break;
				default:
					$fields[$eachField] = $$eachField;
					break;
			}
			// FOR CREATE NEW, FILL ALL LANGUAGE
			if ($eachColumn[5]) {
				foreach ($eachColumn[5] as $ff) {
					$eField = ($eachColumn[5] && $ff != "") ? $eachColumn[1].'_'.$ff : $eachColumn[1];	
					$fields[$eField] = $fields[$eachField];
				}
			}

		}
	}
	$fields['createDate'] = date("Y-m-d H:i:s");
	$fields['lastModDate'] = date("Y-m-d H:i:s");
	$conn->AutoExecute("`".$tableName."`", $fields, 'INSERT');
	header("location: manage_tree.php");
} elseif ($$keyField != "") {
	foreach ( $fixedField as $eachColumn ){
		$eachField = ($eachColumn[5]) ? $eachColumn[1].'_'.$currversion : $eachColumn[1];
		if ( $eachColumn[4][1] ) {
			switch ($eachColumn[2]) {
			case "FILE":
				$varName = "fileDel_".$eachField;
				$varFileName = "filename_".$eachField;
				if($$varName != "") {
					@unlink($uploadedFilePath."/".$$varName);
					$fields[$eachField] = "";					
					if (!empty($eachColumn[4][4])) 
					{
						$sizeIndex = 0;
						foreach ($eachColumn[4][4] as $sizeImg)
						{
							@unlink($uploadedFilePath."/".$$sizeImg[0]);
							$fields[$sizeImg[0]]= "";
						}
					}
				} else {
					$fileUpload->the_temp_file = "";
					$fileUpload->the_file = "";
					$fileUpload->http_error = "";
					$fileUpload->file_copy = "";	
					foreach ( $$eachField as $varName => $varValue ) {
						if ($varName == "tmp_name")
							$fileUpload->the_temp_file = $varValue;
						if ($varName == "name")
							$fileUpload->the_file = $varValue;
						if ($varName == "error")
							$fileUpload->http_error = $varValue;
					}
					if ($fileUpload->the_temp_file != "") {
						@unlink($uploadedFilePath."/".$$varFileName);
						$fileUpload->replace = "n";
						$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
						$fileUpload->upload();		
						$fields[$eachField] = $fileUpload->file_copy;
					}
				}
				break;
			case "PHOTO":
			case "THUMBNAIL":
				$varName = "fileDel_".$eachField;
				$varFileName = "filename_".$eachField;
				if($$varName != "") {
					@unlink($uploadedFilePath."/".$$varName);
					$fields[$eachField] = "";					
				} else {
					$fileUpload->the_temp_file = "";
					$fileUpload->the_file = "";
					$fileUpload->http_error = "";
					$fileUpload->file_copy = "";	
					foreach ( $$eachField as $varName => $varValue ) {
						if ($varName == "tmp_name")
							$fileUpload->the_temp_file = $varValue;
						if ($varName == "name")
							$fileUpload->the_file = $varValue;
						if ($varName == "error")
							$fileUpload->http_error = $varValue;
					}
					if ($fileUpload->the_temp_file != "") {
						@unlink($uploadedFilePath."/".$$varFileName);
						$fileUpload->replace = "n";
						$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
						$fileUpload->upload();		
						$fields[$eachField] = $fileUpload->file_copy;
						
						if (!empty($eachColumn[4][3])) {
							imgActualResize($uploadedFilePath,$fields[$eachField],$uploadedFilePath,$fields[$eachField],$eachColumn[4][3][0],$eachColumn[4][3][1]);
							if (!empty($eachColumn[4][3][2])) {
								if (!empty($eachColumn[4][3][3])) {
									addWaterMark($uploadedFilePath."/".$fields[$eachField], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0, 25, $eachColumn[4][3][3]);
								} else
									addWaterMark($uploadedFilePath."/".$fields[$eachField], $rootPath.$appPath.trim($eachColumn[4][3][2]), 0, 0);
							}
						}
						if (!empty($eachColumn[4][4]) and $fields[$eachField] != "") 
						{
							$sizeIndex = 0;
							foreach ($eachColumn[4][4] as $sizeImg)
							{
								$thumbnailFileName = split("[.]", $fields[$eachField]);
								$thumbnail = $thumbnailFileName[0]."_t".$sizeIndex++.".".$thumbnailFileName[1];
								$file = new File();
								$file->copy_file($uploadedFilePath."/".$fields[$eachField], $uploadedFilePath."/".$thumbnail);
								$photoPath1 = $thumbnail;
								imgActualResize($uploadedFilePath, $photoPath1, $uploadedFilePath, $photoPath1, $sizeImg[1], $sizeImg[2], 100);
								$fields[$sizeImg[0]] = $thumbnail;
							}
						}
					}
				}
				break;
			case "CATEGORY_MULTI":
			case "FOREIGN_MULTI":
				$tmpStr = "";
				foreach($$eachField as $eachValue)
					if(trim($eachValue) != "")
						$tmpStr .= $eachValue.";";
				$fields[$eachField] = ($tmpStr!="") ? $tmpStr : "1;";
				break;
			case "MULTISELECT":
			case "CHECKBOX":
			case "CHECKBOX_FOREIGN":
				$tmpStr = "";
				foreach($$eachField as $eachValue)
					if(trim($eachValue) != "")
						$tmpStr .= $eachValue.";";
				$fields[$eachField] = ($tmpStr!="") ? $tmpStr : "";
				break;
			case "JSON": 
				$tmpStr='[';  
				foreach($$eachField as $eachItem)
				{
					
					//print_r($eachItem);
					$tmpStr.=	($tmpStr!="[") ? "," : ""; ;
					$thisObjStr = '{';
					foreach($eachItem  as $eachFieldName =>$eachFieldValue)
					{   
					
						$thisObjStr.= ($thisObjStr!="{") ? "," : ""; 
						$thisObjStr .='"'.$eachFieldName.'"  : "'.$eachFieldValue.'"';
					}
					$thisObjStr .='}';
					$tmpStr .=$thisObjStr; 
				}
				$tmpStr.=']';
				$fields[$eachField] = ($tmpStr!="") ? ($tmpStr) : ""; 
				//echo $fields[$eachField];
				break;
			case "PASSWORD_MD5":
				
				if(!empty($$eachField)){
					$salt = rand(0,999);
					$fields[$eachField] = md5(md5($$eachField).$salt);
					$fields[$eachField."_salt"] = $salt;
				}else{
				}
				break;
			case "PASSWORD":
				if(!empty($$eachField)){
					$fields[$eachField] = md5($$eachField);
				}
				break;
			case "PERMISSION":
			
				$account_role_table = ACCOUNT_ROLE_TABLE;
			
				$submittedRoles = $_REQUEST["role"];
			
				//delete all
				$deleteRoleRs = $pdo->prepare("delete from `".$account_role_table."` where role_id = :role_id;");
				$deleteRoleRs->bindValue("role_id", $id, PDO::PARAM_INT);
				$deleteRoleRs->execute();
					
				//add all
				foreach($submittedRoles as $submittedRole){
					$insertSQL = '
							INSERT INTO '.$account_role_table.' ( `perm_id`, `role_id`) VALUES
																(:perm_id, :role_id)
							';
					$insertRoleRs = $pdo->prepare($insertSQL);
					$insertRoleRs->bindValue("role_id", $id, PDO::PARAM_INT);
					$insertRoleRs->bindValue("perm_id", $submittedRole, PDO::PARAM_INT);
					$insertRoleRs->execute();
				}
				break;
			default:
				$fields[$eachField] = $$eachField;
				break;
			}
		}
	}
	$fields['lastModDate'] = date("Y-m-d H:i:s");
	$conn->AutoExecute("`".$tableName."`", $fields, 'UPDATE', "`".$keyField."` = '" . $$keyField ."'");
	header("location: manage_tree.php");
} else {
	header("location: index.php");
	exit;
}
$conn->Execute("OPTIMIZE TABLE `".$tableName."`");


ob_end_flush();
exit;
?>