<?php
set_time_limit(10);
require_once("../../config.inc.php");
require_once($rootPath . $adminPath."/config.inc.php");
require_once("../../connDB.php");
require_once("config.inc.php");
require_once($pathClassLib."/class.writeexcel_workbook.inc.php");
require_once($pathClassLib."/class.writeexcel_worksheet.inc.php");

function make_seed() {
  list($usec, $sec) = explode(' ', microtime());
  return (float) $sec + ((float) $usec * 100000);
}

srand(make_seed());
$randval1 = rand(0, 9898989);
$randval2 = rand(0, 9898989);

$fname = tempnam("/tmp", "tmp".$randval1.$randval2.".xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

$text_format01 =& $workbook->addformat();
$text_format01->set_size(12);
$text_format01->set_bold();
$text_format01->set_font('Times New Roman');
$text_format01->set_align('left');
$text_format01->set_align('top');

$text_format02 =& $workbook->addformat();
$text_format02->set_size(12);
$text_format02->set_font('Times New Roman');
$text_format02->set_align('left');
$text_format02->set_align('top');

$startRow = $thisRow = 0;
$thisCol = 0;
foreach ( $cms->__cms_fields as $eachColumn ) {
	$thisColName = split("<br>", $eachColumn[0]);
	$worksheet->write($thisRow, $thisCol++, iconv("UTF-8", "Big5", strip_tags($thisColName[0])), $text_format01);
}

$thissql = (array_key_exists('thissql', $_POST)) ? stripslashes($_POST['thissql']) : "";
$rs = $conn->Execute($thissql);

$thisRow++;
$totalRecord = $rs->RecordCount();
if($totalRecord > 0) {
	while (!$rs->EOF) {	
		$$keyField = $rs->fields[$keyField];
		
		$thisCol = 0;
		foreach ( $cms->__cms_fields as $eachColumn ) {
			if($eachColumn[2] == "CATEGORY") {
				if( $level > 1 ) {
					if ($rs->fields["node_level"] > 1) {
						$dbtree = new dbtree(DB_TABLE, 'node', $conn);
						$thisNodeId = $rs->fields["node_id"];
						$$tempVarName = "";
						do {
							$data1 = $dbtree->GetNodeInfo($thisNodeId, false, "node_name");
							$thisNodeLevel = $data1[2];
							$thisNodeName = $data1[3];
							$$tempVarName = ($$tempVarName != "") ? $thisNodeName."->".$$tempVarName : $thisNodeName;
							
							$data2 = $dbtree->GetParentInfo($rs->fields["node_id"]);
							$thisNodeId = $data2[0];
							$thisParentNodeLevel = $data2[3];
						} while ($thisNodeLevel > $rs->fields["node_level"]);
					} else
						$$tempVarName = (str_replace(array("\n", "\r"), "", $rs->fields["node_name"]));
				} else {
					$$tempVarName = "-";
				}
			} elseif($eachColumn[2] == "FOREIGN") {
				$$tempVarName = "";
				$thissql2 = "SELECT `".$eachColumn[4][3][2]."` FROM `".$eachColumn[4][3][0]."` WHERE `".$eachColumn[4][3][1]."`='".$rs->fields[$eachColumn[1]]."' LIMIT 0, 1";
				$rsExtra = $conn->Execute($thissql2);
				$recordFound = ($rsExtra->RecordCount() > 0) ? true : false;
				while (!$rsExtra->EOF && $recordFound) {
					$$tempVarName = $rsExtra->fields[$eachColumn[4][3][2]];
					$rsExtra->MoveNext();
				}
			} elseif($eachColumn[2] == "FOREIGN2") {
				$$tempVarName = "";
				$thissql2 = "SELECT ".$eachColumn[4][3][3].".`".$eachColumn[4][3][5]."` FROM `".$eachColumn[4][3][0]."`, ".$eachColumn[4][3][3]." WHERE `".$eachColumn[4][3][0]."`.`".$eachColumn[4][3][2]."`=`".$eachColumn[4][3][3]."`.`".$eachColumn[4][3][4]."` AND `".$eachColumn[4][3][1]."`='".$rs->fields[$eachColumn[1]]."' LIMIT 0, 1";
				$rsExtra = $conn->Execute($thissql2);
				$recordFound = ($rsExtra->RecordCount() > 0) ? true : false;
				while (!$rsExtra->EOF && $recordFound) {
					$$tempVarName = $rsExtra->fields[$eachColumn[4][3][5]];
					$rsExtra->MoveNext();
				}
			} elseif($eachColumn[2] == "RADIO") {
				$strStatus = array();
				foreach( $eachColumn[4][3] as $eachName=>$eachValue)
					$strStatus[trim($eachValue)] = trim($eachName);	
				$$tempVarName = ($strStatus[$rs->fields[$eachColumn[1]]] != "") ? $strStatus[$rs->fields[$eachColumn[1]]] : $rs->fields[$eachColumn[1]];
			} elseif($eachColumn[2] == "FILE" || $eachColumn[2] == "PHOTO") {
				if ($rs->fields[$eachColumn[1]] != "")
					$$tempVarName = "<a href=\"".$appPath.$uploadedFileDir."/".$rs->fields[$eachColumn[1]]."\" target=\"_blank\"><img src=\"".$adminPath."/images/icondownload.gif\" alt=\"Click to Open\" border=\"0\" /></a>";
				else
					$$tempVarName = "";
			} elseif($eachColumn[2] == "HYPERLINK") {
				$$tempVarName = "<a href=\"".(str_replace(array("\n", "\r"), "", $rs->fields[$eachColumn[1]]))."\" target=\"_blank\">".(str_replace(array("\n", "\r"), "", $rs->fields[$eachColumn[1]]))."</a>";
			} else {
				$$tempVarName = (str_replace(array("\n", "\r"), "", $rs->fields[$eachColumn[1]]));
			}
			
			$worksheet->write($thisRow, $thisCol++, iconv("UTF-8", "Big5", strip_tags($$tempVarName)), $text_format02);
			//$worksheet->write($thisRow, $thisCol++, $$tempVarName, $text_format02);
		}
		
		$thisRow++;
		$rs->MoveNext();
	}
}

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"data_".$tableName."_".date("Ymd_His").".xls\"");
header("Content-Disposition: inline; filename=\"data_".$tableName."_".date("Ymd_His").".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
