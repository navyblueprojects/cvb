<?php
ob_start();

require_once("config.inc.php"); # config
if( !isset( $_SESSION ) ) { ob_start(); session_start(); ob_end_clean(); }

foreach($_SESSION as $eachSessionName=>$eachSessionValue) {
	if (strpos($eachSessionName, $tableName) !== false)
		unset($_SESSION[$eachSessionName]);
}
include_once("./config.inc.php");
if ($level == 0) {
	header("location: edit.php");
	exit;
} else {
	header("location: list.php");
	exit;
}
ob_end_flush();
?>