<?php
$IS_PUBLIC = true;
include_once("../config.inc.php");
include ("./config.inc.php");
$e = array_key_exists ( "e", $_GET ) ? $_GET ["e"] : "";
$loginName = array_key_exists ( "loginName", $_GET ) ? $_GET ["loginName"] : "";

if (empty($_SESSION['token'])) {
    if (function_exists('mcrypt_create_iv')) {
        $_SESSION['token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
    } else {
        $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
    }
}
$token = $_SESSION['token'];

?><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Administrative Area</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<link href="css/cms.css" type="text/css" rel="stylesheet" />

<style type="text/css">

body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	vertical-align: middle;
}

body {
	background: url("images/blur_bg.png");
	background-repeat: no-repeat;
	background-size: cover;
}

.errorContainer {
	visibility: hidden;
	color: red;
	padding: 4px 0px;
}

#loginIncorrect {
	opacity: 0;
	color: red;
	transition: all .3s;
}

#formContainer .formInputWrapper label {
	width: 20%;
	
	text-align: left;
	display: inline-block;
}

#formContainer .formInputWrapper input {
	height:30px;
	width:80%;
	display: inline-block;
}

#formContainer {
	min-width:600px;
	max-width:900px;
	width: 40%;
	margin: auto;
	border: 1px solid #DDD;
	background-color: #F5F5F5;
	padding: 2%;
	-webkit-border-radius: 4px 4px 4px 4px;
	border-radius: 4px 4px 4px 4px;
}

#formContainer #submitBtn {
	width: 100%;
}
</style>

</head>
<body>

	<div style="display: table; width: 100%; height: 100%;">
		<div style="display: table-cell; vertical-align: middle;">
			<div id="formContainer">

					<div style="width:60%;display:inline-block;vertical-align:middle">
					
						<div class="formInputWrapper">
							<label  for="username">
								Login ID :
							</label><input type="text" name="username" id="username"
								class="form-control" placeholder="Username" autocomplete="off" />
							<div class="errorContainer">Please input your username</div>
						</div>
	
						<div class="formInputWrapper">
							<label  for="password">
								Password :
							</label><input type="password" name="password" id="password"
								class="form-control" placeholder="Password" autocomplete="off" />
							<div class="errorContainer">Please enter your password</div>
						</div>
	
						<div class="formInputWrapper">
							<input type="button" id="submitBtn" class="btnItem"
								value="Login"></input>
						</div>
	
						<div id="loginIncorrect">Login incorrect. Please try again.</div>
					</div><!-- 
					 --> 
			</div>
		</div>
	</div>

</body>



	<script>
		function bodyFadeIn(){
			$("html").fadeIn(300, function(){
				$("html").show();
			});
			
		}
		$('#submitBtn').click(function(){
			$('#formContainer').wrapInner('<form id="loginForm"></form>');


			$("#loginForm").validate({
				rules:{
	   				username: "required",
					password:"required",
				}
				,submitHandler:function(form,e) {
					
					e.preventDefault();
					$.post("login_util.php",{
						loginName: $("[name=username]").val(),
		   				password: $("[name=password]").val(),
		   				session: '<?=$token?>'
					}, function(data){
						if(data == "TRUE"){
							window.location="<?=$adminPath?>/";
						}else{
							$("#loginIncorrect").css({opacity:1});
							setTimeout(function(){
								$("#loginIncorrect").css({opacity:0});
							}, 2000);
						}

					}); 
	   			}
	   			,errorPlacement: function(error, element) {
			   		$('#loginForm').replaceWith($('#loginForm').find('div').eq(0));
			   		$(element).parent().find(".errorContainer").css("visibility","visible");
			    },
			    success: function (error, element) {
		    		$(element).parent().find(".errorContainer").css("visibility","hidden");
	            }
			});

			$('#loginForm').submit();

		});
		    
				
				
	
	</script>

</html>