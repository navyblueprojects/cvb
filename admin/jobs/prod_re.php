<?php
ob_start();
require_once("config.inc.php"); # config
require_once("../../config.inc.php"); # common config
require_once("../config.inc.php"); # admin config
include_once("../../connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");

$thissql = "SELECT p.* FROM `".$tableName."` p WHERE p.`id`='".$id."' LIMIT 0, 1";
$rs = $conn->Execute($thissql);
$recordFound = ($rs->RecordCount() > 0) ? true : false;
if ($recordFound) {
	while (!$rs->EOF) {
		$thisProdId = $rs->fields["prodId"];
		$thisName = $rs->fields["prodName"];
		$rs->MoveNext();
	}
}

$id = array_key_exists("id", $_GET) ? $_GET["id"] : "";
$keyword = array_key_exists("keyword", $_GET) ? trim($_GET["keyword"]) : "";
$joinTableList = $extraFields = "";

$mywhere = "";
if($keyword!="") {
	$mywhere .= " AND (";
	foreach ( $fixedField as $eachColumn ) {	
		if ( $eachColumn[3][3] ) {
			if ($mywhere != " AND (")
				$mywhere .= " OR ";
			if ($eachColumn[1] == "node_id" && $level > 1) {
				$mywhere .= "n1.`node_name` like '%".htmlspecialchars($keyword, ENT_QUOTES)."%' OR n2.`node_name` like '%".htmlspecialchars($keyword, ENT_QUOTES)."%'";
			} else { 
				$mywhere .= "`".$eachColumn[1]."` like '%".htmlspecialchars($keyword, ENT_QUOTES)."%'";
			}
		}
	}
	$mywhere .= ")";
	
	if($level > 1) {
		$joinTableList = ", `".DB_TABLE."` n1, `".DB_TABLE."` n2";
		$extraFields = ", n2.`node_name`, n2.`node_level`";
		if (MULTI_CATEGORY)
			$mywhere .= " AND (`".$tableName."`.`node_id` LIKE CONCAT('%;',n2.`node_id`,';%') OR `".$tableName."`.`node_id` LIKE CONCAT(n2.`node_id`,';%') ) AND n1.`node_left`<=n2.`node_left` AND n1.`node_right`>=n2.`node_right`";
		else
			$mywhere .= " AND (`".$tableName."`.`node_id` = n2.`node_id` ) AND n1.`node_left`<=n2.`node_left` AND n1.`node_right`>=n2.`node_right`";
	}
}

if($level > 1) {
	$joinTableList = ", `".DB_TABLE."` n1, `".DB_TABLE."` n2";
	$extraFields = ", n2.`node_name`, n2.`node_level`";
	if (MULTI_CATEGORY)
		$mywhere .= " AND (`".$tableName."`.`node_id` LIKE CONCAT('%;',n2.`node_id`,';%') OR `".$tableName."`.`node_id` LIKE CONCAT(n2.`node_id`,';%') ) AND n1.`node_left`<=n2.`node_left` AND n1.`node_right`>=n2.`node_right`";
	else
		$mywhere .= " AND (`".$tableName."`.`node_id` = n2.`node_id` ) AND n1.`node_left`<=n2.`node_left` AND n1.`node_right`>=n2.`node_right`";
}

function isRelated($prodId) {
	global $conn, $id;	
	$thissql = "SELECT * FROM `".$relTableName."` WHERE (`prodId_1`='".$id."' AND `prodId_2`='".$prodId."') OR (`prodId_2`='".$id."' AND `prodId_1`='".$prodId."')";
	$rs = $conn->Execute($thissql);
	$recordFound = ($rs->RecordCount() > 0) ? true : false;
	$rs->close();
	return $recordFound;
}

$thissql = "SELECT `".$tableName."`.* ".$extraFields." FROM `".$tableName."` ".$joinTableList." WHERE 1 $mywhere GROUP BY `".$tableName."`.`".$keyField."`";
$rs = $conn->Execute($thissql);
$recordFound = ($rs->RecordCount() > 0) ? true : false;
$printContent = "";
$i = 1;
$perLine = 4;
while (!$rs->EOF && $recordFound) {	
	// head
	if ($i % $perLine - 1 == 0)
		$printContent .= "<tr style=\"padding:5px;".((($i / $perLine )%2==0)?"background-color:#cfcfcf":"")."\">";
	
	$thisChecked = isRelated($rs->fields["id"]) ? "checked" : "";
	$hiddenField = isRelated($rs->fields["id"]) ? "<input type=\"hidden\" name=\"prodOriId[]\" value=\"".$rs->fields["id"]."\" >" : "";
	// main content
	$printContent .= "<td align=\"left\" width=\"167\">".$hiddenField."<input type=\"checkbox\" name=\"prodId[]\" value=\"".$rs->fields["id"]."\" ".$thisChecked.">".$rs->fields["prodId"]." - ".$rs->fields["prodName"]."<br>
 (".$rs->fields["node_name"].")</td>";
	  
	// tail
	if ($i++ % $perLine == 0)
		$printContent .= "</tr>";
	$rs->MoveNext();
}
if (($perLine - (($i-1)%$perLine))<$perLine)
	$printContent .= "</tr>";
$rs->close();
?>
<? require_once("../framework/overall_header.php")?>
<link href="<?=$adminPath?>/css/slide.css" type="text/css" rel="stylesheet" />
<body>
<div id="body" style="position:absolute; left:0px; top:0px; width:100%">

<div id="leftNav" style="position:absolute; left:0px; top:0px; width:180px;">
<? include_once($pathDocRoot . "/admin/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" style="position:absolute;left:180px; top:0px;">
<table id="listbody" style="margin-top:10px;" >
  <tbody>
    <tr>
    <td>
      
			<div id="inputFields" style="position:static"></div>
      <table class="formline" cellspacing="0" cellpadding="0" width="700">
        <tr>
          <td class="rowHeader" colspan="5" style="padding-top:10px"><span style="font-style: italic"><u><span style="cursor:pointer" onClick="window.location='./list.php'">Products</span></u> -&gt; Relationship</span></td>
		</tr>
        <tr>
			<td colspan="5" style="padding:5px;">Products &gt; <?=$thisProdId." - ".$thisName?></td>
		</tr>
        <form name="frmSearch" action="" method="get"><input type="hidden" name="id" value="<?=$id?>" />
        <tr>
        	<td colspan="5" style="padding:5px;">Keyword: <input type="text" name="keyword" value="<?=$keyword?>" /> <input type="button" name="btnSearch" value=" Search " onClick="window.location='prod_re.php?id=<?=$id?>&keyword='+this.form.keyword.value" /></td>
        </tr>
        </form>
        <form action="prod_re_todb.php" method="post" id="myform" name="myform" onSubmit="return confirm('Are you sure to link the selected item(s) to the specific product?')">
      <input type="hidden" name="id" value="<?=$id?>" /><input type="hidden" name="keyword" value="<?=$keyword?>" />
		<tr>
          <td class="catBottom" colspan="5">
		  	[ <a onClick="checkAll('myform', 'prodId');" href="javascript:void(0);">Check All</a> ] 
		  	[ <a onClick="checkInverse('myform', 'prodId');" href="javascript:void(0);">Check Inverse</a> ] 
			</td></tr>
		<tr>
			<td colspan="5" style="overflow:hidden">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr><td width="25%" height="0"></td><td width="25%" height="0"></td><td width="25%" height="0"></td><td width="25%" height="0"></td></tr>
            <?=$printContent?>
            </table>
            </td>
        </tr>
		<tr>
          <td class="catBottom" colspan="5">
		  	[ <a onClick="checkAll('myform', 'prodId');" href="javascript:void(0);">Check All</a> ] 
		  	[ <a onClick="checkInverse('myform', 'prodId');" href="javascript:void(0);">Check Inverse</a> ] 
			</td></tr>
        <tr>
        	<td colspan="5" align="center" height="30"><input type="submit" name="btnSubmit" value=" Submit " /></td>
        </tr>        
	  </form>
        <tr>
        	<td colspan="5" height="30" align="center" valign="middle"><a href="./list.php">&lt;- Back to Product List Page</a></td>
        </tr>
	  </table>
      <? include_once($pathDocRoot . "/admin/framework/overall_footer.php");?>
</td></tr></tbody></table>
</div>

</div>
<div id="extendsDiv"></div>
</body>
</html>
<? include_once($pathDocRoot . "/admin/framework/end.php");?>
