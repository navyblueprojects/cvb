<?php
ob_start();
require_once("../../config.inc.php"); # common config
require_once("inv_config.inc.php"); # config
require_once($rootPath.$adminPath."/config.inc.php"); # admin config
include_once($pathDocRoot."/connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.pageiterator.php");
require_once($pathClassLib.'/dbtree.class.php');
if( !isset( $_SESSION ) ) { ob_start(); session_start(); ob_end_clean(); }
header('Cache-Control: no-cache');
header('Pragma: no-cache');

// Set global variables if not already set
$totalWidth = 0;
$tableColumn = array(array("", "", "false", "30"));
$totalWidth += 30;
$tableColumn[] = array("", "Control", "false", "180");
$totalWidth += 115;
foreach ( $fixedField as $eachColumn ) {
	if ( $eachColumn[3][0] != "HIDDEN" ) {
		$thisColName = split("<br>", $eachColumn[0]);
		$tableColumn[] = array($eachColumn[1], $thisColName[0], "\"".$eachColumn[3][2]."\"", $eachColumn[3][1]);
		$totalWidth += $eachColumn[3][1];
	}	
}

$prodId = array_key_exists($tableName."_prodId", $_SESSION) ? $_SESSION[$tableName."_prodId"] : "";
$prodId = array_key_exists("prodId", $_GET) ? $_GET['prodId'] : $prodId;
$_SESSION[$tableName."_prodId"] = $prodId;

if(!function_exists("getProdInfo")) {
	function getProdInfo($id, $requestField="") {
		global $conn, $parentTable;
		$thissql = "SELECT * FROM `".$parentTable."` WHERE `id`='".$id."' LIMIT 0, 1";
		$rs = $conn->Execute($thissql);
		$recordFound = ($rs->RecordCount() > 0) ? true : false;
		if ($recordFound) {
			return ($requestField == "") ? $rs->fields : $rs->fields[$requestField];
		} else return false;
	}
}


if (!empty($_POST['node_id']) || !empty($_POST['keyword']) || !empty($_GET['node_id']) || !empty($_GET['keyword'])) {
	unset($_SESSION[$tableName."_pageNo"]);
}

$joinTableList = $extraFields = "";
$pageNo = array_key_exists($tableName."_pageNo", $_SESSION) ? $_SESSION[$tableName."_pageNo"] : 1;
$pageNo = array_key_exists("pageNo", $_GET) ? $_GET['pageNo'] : $pageNo;
$pageNo = array_key_exists("pageNo", $_POST) ? $_POST['pageNo'] : $pageNo;
$_SESSION[$tableName."_pageNo"] = $pageNo;

$keyword = array_key_exists($tableName."_keyword", $_SESSION) ? $_SESSION[$tableName."_keyword"] : "";
$keyword = array_key_exists("keyword", $_GET) ? $_GET['keyword'] : $keyword;
$keyword = array_key_exists("keyword", $_POST) ? $_POST['keyword'] : $keyword;
$keyword = str_replace(array("\'", '\"'), array("'", '"'), trim($keyword));
$_SESSION[$tableName."_keyword"] = $keyword;

$node_id = array_key_exists($tableName."_node_id", $_SESSION) ? $_SESSION[$tableName."_node_id"] : "";
$node_id = array_key_exists("node_id", $_GET) ? $_GET['node_id'] : $node_id;
$node_id = array_key_exists("node_id", $_POST) ? $_POST['node_id'] : $node_id;
$_SESSION[$tableName."_node_id"] = $node_id;

$refresh = (array_key_exists("refresh", $_GET)) ? $_GET['refresh'] : "";
if($refresh == "1") echo "<script language=\"javascript\">window.location.replace('".$_SERVER['PHP_SELF']."');</script>";

$mywhere = " AND `".$tableName."`.`".$foreignKey."`='".$prodId."'";
if($keyword!="") {
	$mywhere .= " AND (";
	foreach ( $fixedField as $eachColumn ) {	
		if ( $eachColumn[3][3] ) {
			if ($mywhere != " AND (")
				$mywhere .= " OR ";
			$mywhere .= "`".$eachColumn[1]."` like '%".htmlspecialchars($keyword, ENT_QUOTES)."%'";
		}
	}
	$mywhere .= ")";
}

if($level > 1) {
	$joinTableList = ", `".DB_TABLE."`";
	$extraFields = ", `".DB_TABLE."`.`node_name`, `".DB_TABLE."`.`node_level`";
	if (MULTI_CATEGORY)
		$mywhere .= " AND (`".$tableName."`.`node_id` LIKE CONCAT('%',`".DB_TABLE."`.`node_id`,';%') )";
	else
		$mywhere .= " AND (`".$tableName."`.`node_id` = `".DB_TABLE."`.`node_id` )";
}

if($node_id != "" && $node_id != "0" && $node_id != "1") {
	if (strpos($node_id, "@") === false) {
		$thissql = "SELECT * FROM `".DB_TABLE."` WHERE 1 AND `node_id`='".$node_id."'";
		$rs = $conn->Execute($thissql);
		$recordFound = ($rs->RecordCount() > 0) ? true : false;
		if ($recordFound) {
			while (!$rs->EOF) {
				$node_id = array($rs->fields["node_id"], $rs->fields["node_left"], $rs->fields["node_right"]);
				$rs->MoveNext();
			}
		}
	} else {		
		$node_id = split("@", $node_id);
	}
	
	$mywhere .= " AND (`".DB_TABLE."`.`node_left` >= '".$node_id[1]."' AND `".DB_TABLE."`.`node_right` <= '".$node_id[2]."')";
}

$scol = ( array_key_exists("scol", $_GET) ) ? $_GET['scol'] : "2";
$sdir = ( array_key_exists("sdir", $_GET) ) ? $_GET['sdir'] : "1";

$sqlFieldList = "";
foreach ($tableColumn as $eachColumn) {
	if ($eachColumn[0] != "")
		$sqlFieldList .= ($sqlFieldList == "") ? $eachColumn[0] : ", ".$eachColumn[0];
}

$thissql = "SELECT `".$tableName."`.* ".$extraFields." FROM `".$tableName."` ".$joinTableList." WHERE 1 $mywhere GROUP BY `".$tableName."`.`".$keyField."` ORDER BY `".$tableName."`.`".$tableColumn[$scol][0]."` " . $sortDir[$sdir];
$rs = $conn->SelectLimit($thissql, $perPage, ($pageNo-1)*$perPage);
$rss = $conn->Execute($thissql);

$totalRecord = $rs->RecordCount();

$printScript = "";
$printIDScript = "";
$printNameScript = "";
$printCDScript = "";
$printLMDScript = "";
$printThumbScript = "";
$printStatusScript  = "";
$printDateScript = "";
if($totalRecord > 0) {
	while (!$rs->EOF) {	
		$$keyField = $rs->fields[$keyField];
		
		$i = 1;
		foreach ( $fixedField as $eachColumn ) {	
			if ( $eachColumn[3][0] != "HIDDEN" ) {
				$tempVarName = "col".$i;
				$tempScriptVarName = "printCol".$i."Script";
				if($eachColumn[2] == "CATEGORY") {
					if ($rs->fields["node_level"] > 1) {
						$dbtree = new dbtree(DB_TABLE, 'node', $conn);
						$thisNodeId = $rs->fields["node_id"];
						$$tempVarName = "";
						do {
							$data1 = $dbtree->GetNodeInfo($thisNodeId, false, "node_name");
							$thisNodeLevel = $data1[2];
							$thisNodeName = $data1[3];
							$$tempVarName = ($$tempVarName != "") ? html_entity_decode($thisNodeName)." -> ".html_entity_decode($$tempVarName) : html_entity_decode($thisNodeName);
							
							$data2 = $dbtree->GetParentInfo($rs->fields["node_id"]);
							$thisNodeId = $data2[0];
							$thisParentNodeLevel = $data2[3];
						} while ($thisNodeLevel > $rs->fields["node_level"]-1);
					} else
						$$tempVarName = (str_replace(array("\n", "\r"), "", html_entity_decode($rs->fields["node_name"])));		
				} elseif($eachColumn[2] == "CATEGORY_MULTI") {
					$dbtree = new dbtree(DB_TABLE, 'node', $conn);
					$$tempVarName = "";
					$tmpArr = array();
					$tmpArr = split(";", $rs->fields[$eachColumn[1]]);
					foreach($tmpArr as $eachId) {
						if($eachId != "") {
							$data1 = $dbtree->GetNodeInfo($eachId, false, "node_name");
							$thisNodeName = html_entity_decode($data1[3]);
							$$tempVarName .= ($$tempVarName!="")? ", ".$thisNodeName : $thisNodeName;
						}
					}
				} elseif($eachColumn[2] == "FOREIGN") {
					$$tempVarName = "";
					$thissql2 = "SELECT `".$eachColumn[4][3][2]."` FROM `".$eachColumn[4][3][0]."` WHERE `".$eachColumn[4][3][1]."`='".$rs->fields[$eachColumn[1]]."' LIMIT 0, 1";
					$rsExtra = $conn->Execute($thissql2);
					$recordFound = ($rsExtra->RecordCount() > 0) ? true : false;
					while (!$rsExtra->EOF && $recordFound) {
						$$tempVarName = html_entity_decode($rsExtra->fields[$eachColumn[4][3][2]]);
						$rsExtra->MoveNext();
					}
					$rsExtra->close();
				} elseif($eachColumn[2] == "FOREIGN2") {
					$$tempVarName = "";
					$thissql2 = "SELECT ".$eachColumn[4][3][3].".`".$eachColumn[4][3][5]."` FROM `".$eachColumn[4][3][0]."`, ".$eachColumn[4][3][3]." WHERE `".$eachColumn[4][3][0]."`.`".$eachColumn[4][3][2]."`=`".$eachColumn[4][3][3]."`.`".$eachColumn[4][3][4]."` AND `".$eachColumn[4][3][1]."`='".$rs->fields[$eachColumn[1]]."' LIMIT 0, 1";
					$rsExtra = $conn->Execute($thissql2);
					$recordFound = ($rsExtra->RecordCount() > 0) ? true : false;
					while (!$rsExtra->EOF && $recordFound) {
						$$tempVarName = html_entity_decode($rsExtra->fields[$eachColumn[4][3][5]]);
						$rsExtra->MoveNext();
					}
					$rsExtra->close();
				} elseif($eachColumn[2] == "FOREIGN_MULTI") {
					$$tempVarName = "";
					$tmpArr = array();
					$tmpArr = split(";", $rs->fields[$eachColumn[1]]);
					foreach($tmpArr as $eachId) {
						if($eachId != "") {
							$thissql2 = "SELECT `".$eachColumn[4][3][2]."` FROM `".$eachColumn[4][3][0]."` WHERE `".$eachColumn[4][3][1]."`='".$eachId."' LIMIT 0, 1";
							$rsExtra = $conn->Execute($thissql2);
							$recordFound = ($rsExtra->RecordCount() > 0) ? true : false;
							if( $recordFound) {
								$$tempVarName .= ($$tempVarName!="")? ", ".html_entity_decode($rsExtra->fields[$eachColumn[4][3][2]]) : html_entity_decode($rsExtra->fields[$eachColumn[4][3][2]]);
							}
							$rsExtra->close();
						}
					}
				} elseif($eachColumn[2] == "CHECKBOX") {
					$$tempVarName = "";
					$tmpArr = array();
					$tmpArr = split(";", $rs->fields[$eachColumn[1]]);
					foreach($tmpArr as $eachId) {
						if($eachId != "") {
							$$tempVarName .= ($$tempVarName!="")? ", ".$eachColumn[4][3][$eachId] : $eachColumn[4][3][$eachId];
						}
					}
				} elseif($eachColumn[2] == "RADIO") {	
					$strStatus = array();
					foreach( $eachColumn[4][3] as $eachName=>$eachValue)
						$strStatus[trim($eachValue)] = trim($eachName);	
					$$tempVarName = ($strStatus[$rs->fields[$eachColumn[1]]] != "") ? $strStatus[$rs->fields[$eachColumn[1]]] : $rs->fields[$eachColumn[1]];
				} elseif($eachColumn[2] == "FILE" || $eachColumn[2] == "PHOTO") {
					if ($rs->fields[$eachColumn[1]] != "")			
						$$tempVarName = "<a href=\"".$appPath.$uploadedFileDir."/".$rs->fields[$eachColumn[1]]."\" target=\"_blank\"><img src=\"".$adminPath."/images/icondownload.gif\" alt=\"Click to Open\" border=\"0\" /></a>";
					else
						$$tempVarName = "";
				} elseif($eachColumn[2] == "THUMBNAIL") {
					if ($rs->fields[$eachColumn[1]] != "")	{					
						$path = $appPath.$uploadedFileDir."/";
						$filename = $rs->fields[$eachColumn[1]];
						$newsize = imgVirtualResize ($path, $filename, 30, 30);	
						if($newsize != 0)
							$$tempVarName = "<a href=\"".$path.$rs->fields[$eachColumn[1]]."\" target=\"_blank\"><img src=\"".$path.$filename."\" border=\"0\" width=\"".$newsize[0]."\" height=\"".$newsize[1]."\" alt=\"".$$eachColumn[1]."\" /></a>";
						else $$tempVarName = "";
					} else
						$$tempVarName = "";
				} elseif($eachColumn[2] == "HYPERLINK") {
					$$tempVarName = "<a href=\"".(str_replace(array("\n", "\r"), "", $rs->fields[$eachColumn[1]]))."\" target=\"_blank\">".(($rs->fields[$eachColumn[1]]!="")?"Link":"")."</a>";
				} elseif($eachColumn[2] == "DOUBLE") {
					$$tempVarName = number_format(str_replace(array("\n", "\r"), "", $rs->fields[$eachColumn[1]]), 2);
				} elseif($eachColumn[2] == "INT") {
					$$tempVarName = number_format(str_replace(array("\n", "\r"), "", $rs->fields[$eachColumn[1]]), 0);
				} else {
					$$tempVarName = (str_replace(array("\n", "\r"), "", $rs->fields[$eachColumn[1]]));
				}
				
				$$tempScriptVarName .= ($$tempScriptVarName!="") ? ", '".str_replace("'", "&#039;", $$tempVarName)."'" : "'".str_replace("'", "&#039;", $$tempVarName)."'";
				$i++;
			}
		}
		
		$printKeyScript .= ($printKeyScript!="") ? ", '".$$keyField."'" : "'".$$keyField."'";
		$rs->MoveNext();
	}
	$printScript = "var slideKey = new Array(".$printKeyScript.");\n";
	$i = 1;
	foreach ( $fixedField as $eachColumn ) {	
		if ( $eachColumn[3][0] != "HIDDEN" ) {
			$tempScriptVarName = "printCol".$i."Script";
			$printScript .= "var col".$i." = new Array(".$$tempScriptVarName.");\n";
			$i++;
		}
	}
}

$paginator = new PageIterator($rss->RecordCount(), $pageNo, $perPage, $pageLinks);

$pageList = "";
$pageList .= ($pageList == "") ? "Pages: " : "";

// add first page button
if ($pageNo > 1)
	$pageList .= "<span style=\"cursor:pointer\" onclick=\"pageChange('myform', '1')\"><img src=\"".$adminPath."/images/btn_first.jpg\" alt\"First Page\" border=\"0\" /></span> ";

// add previous page button
if ($pageNo > 1)
	$pageList .= "<span style=\"cursor:pointer\" onclick=\"pageChange('myform', '".($pageNo-1)."')\"><img src=\"".$adminPath."/images/btn_prev.jpg\" alt\"Previous Page\" border=\"0\" /></span> ";


// add page no selection
while ($paginator -> hasNextPage()) {
    $ndx = $paginator -> nextPage();
    if ($ndx == $pageNo) {
        $pageList .= "<span class='btnItem pageSelected' style='padding: 2px 4px;'>$ndx</span>";
    } else {
        $pageList .= '<span class="btnItem paging" style="padding: 2px 4px;" onclick="window.location=\''.$_SERVER['PHP_SELF'].'?pageNo='.$ndx.'\'">';
        $pageList .= $ndx . '</span> ';
    }
}

// add next page button
if ($pageNo < $paginator->getPageCount())
	$pageList .= "<span style=\"cursor:pointer\" onclick=\"pageChange('myform', '".($pageNo+1)."')\"><img src=\"".$adminPath."/images/btn_next.jpg\" alt\"Next Page\" border=\"0\" /></span> ";

// add last page button
if ($pageNo < $paginator->getPageCount())
	$pageList .= "<span style=\"cursor:pointer\" onclick=\"pageChange('myform', '".$paginator->getPageCount()."')\"><img src=\"".$adminPath."/images/btn_last.jpg\" alt\"Last Page\" border=\"0\" /></span> ";

$rs->close();
$rss->close();
?>
<? require_once("../framework/overall_header.php")?>
<link href="<?=$adminPath?>/css/slide.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?=$adminPath?>/js/core.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/events.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/css.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/coordinates.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/drag.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/dragsort.js"></script>
<script type="text/javascript" language="javascript">
<!--
var ESCAPE = 27;
var ENTER = 13;
var TAB = 9;
var noOfSortBox = <?=$totalRecord?>;
var coordinates = ToolMan.coordinates();
var dragsort = ToolMan.dragsort();
var dragSwitch = <?=($keyword!="") ? 'false' : 'true'?>;
var editSwitch = false;
dragSwitch = false;
<?=$printScript?>

window.onload = function() {
	for (var i=0; i < noOfSortBox && editSwitch; i++) {
		eval('join("'+slideId[i]+'@'+i+'sortBox", true);');
	}
	if(dragSwitch && <?=$dragSort?>) {
		dragsort.makeListSortable(document.getElementById("slideshow"));
	}
}

function setHandle(item) {
	item.toolManDragGroup.setHandle(findHandle(item));
}

function findHandle(item) {
	var children = item.getElementsByTagName("div");
	for (var i = 0; i < children.length; i++) {
		var child = children[i];

		if (child.getAttribute("class") == null) continue;

		if (child.getAttribute("class").indexOf("handle") >= 0)
			return child;
	}
	return item;
}

function join(name, isDoubleClick) {
	var view = document.getElementById(name + "View");
	view.editor = document.getElementById(name + "Edit");

	var showEditor = function(event) {
		event = fixEvent(event);

		var view = this;
		var editor = view.editor;

		if (!editor) return true;

		if (editor.currentView != null) {
			editor.blur();
		}
		editor.currentView = view;

		var topLeft = coordinates.topLeftOffset(view);
		topLeft['x'] = topLeft['x']-180;
		topLeft.reposition(editor);
		if (editor.nodeName == 'TEXTAREA') {
			editor.style['width'] = view.offsetWidth + "px";
			editor.style['height'] = view.offsetHeight + "px";
		} else {
			editor.style['overflow'] = "visible";
		}
		editor.value = view.innerHTML;
		editor.style['visibility'] = 'visible';
		view.style['visibility'] = 'hidden';
		editor.focus();
		return false;
	}

	if (isDoubleClick) {
		view.ondblclick = showEditor;
	} else {
		view.onclick = showEditor;
	}

	view.editor.onblur = function(event) {
		event = fixEvent(event);

		var editor = event.target;
		var view = editor.currentView;

		if (!editor.abandonChanges) view.innerHTML = editor.value;
		editor.abandonChanges = false;
		editor.style['overflow'] = "hidden";
		editor.style['visibility'] = 'hidden';
		editor.value = ''; // fixes firefox 1.0 bug
		view.style['visibility'] = 'visible';
		editor.currentView = null;

		return true;
	}
	
	view.editor.onkeydown = function(event) {
		event = fixEvent(event);
		
		var editor = event.target;
		if (event.keyCode == TAB) {
			editor.blur();
			return false;
		} else if (event.keyCode == ENTER) {
			editor.blur();
			return false;
		}
	}

	view.editor.onkeyup = function(event) {
		event = fixEvent(event);

		var editor = event.target;
		if (event.keyCode == ESCAPE) {
			editor.abandonChanges = true;
			editor.blur();
			return false;
		} else {
			return true;
		}
	}

	// TODO: this method is duplicated elsewhere
	function fixEvent(event) {
		if (!event) event = window.event;
		if (event.target) {
			if (event.target.nodeType == 3) event.target = event.target.parentNode;
		} else if (event.srcElement) {
			event.target = event.srcElement;
		}

		return event;
	}
}

function goUpdate(thisForm) {

	var children = document.getElementsByTagName("span");
	for (var i = 0; i < children.length; i++) {
		var child = children[i];

		if (child.className == null) continue;
		if (child.className.indexOf("view") >= 0) {
			document.getElementById("inputFields").innerHTML += "<input type='hidden' name='sort[]' value='"+child.id.replace(/sortBoxView/, "")+"@"+child.innerHTML+"' />";
		}
	}
	document.getElementById("slideEditors").innerHTML = "";
	
	thisForm.action = "inv_goupdate.php";
	thisForm.submit();
}

function goDelete(thisForm) {
	if(confirm("Are you sure to delete all the selected data?")) {
		thisForm.method = "POST";
		thisForm.action = "inv_godel.php?node_id=<?=$node_id?>";
		thisForm.submit();
	}
}

function goDeleteOne(thisForm, id, btn) {
	if(confirm("Are you sure to delete the selected data?")) {
		thisForm.method = "POST";
		thisForm.action = "inv_godel.php?node_id=<?=$node_id?>&id="+id;
		thisForm.submit();
	} else {
		btn.disabled = false;
	}
}

function confirmdelall() {
	if(confirm("Are you sure to delete all the selected data?"))
		return true;
	else
		return false;
}

function genCSV(thisForm) {
	if(confirm("Are you sure to generate the data into EXCEL format?")) {			
		thisForm.action = "gencsv.php?node_id=<?=$node_id?>";
		thisForm.submit();
	}
}

function genCSV2(thisForm) {
	if(confirm("Are you sure to generate the data into EXCEL format?")) {			
		thisForm.action = "gencsv_details.php?node_id=<?=$node_id?>";
		thisForm.submit();
	}
}

function pageChange(formName, page) {
	myForm = document.getElementById(formName);
	myForm.action = "<?=$_SERVER['PHP_SELF']?>";
	myForm.pageNo.value = page;
	myForm.submit();
}

function goEdit(slideId) {
	window.location = 'inv_edit.php?id='+slideId;
}

function goInventory(id) {
	window.location = 'inv_inventory_list.php?id='+id;
}

function goAdd() {
	window.location = 'inv_edit.php?node_id=<?=$node_id[0]?>';
}

function goSearch() {
	var keyword = document.getElementById("tmpKeyword").value.replace(/(^\s*)|(\s*$)/g, "");
	<? if ($level > 1) { ?>
	var node_id = document.getElementById("node_id").value.replace(/(^\s*)|(\s*$)/g, "");
	<? } ?>
	document.getElementById("extendsDiv").innerHTML = '<form id="tmpForm" method="get" action="<?=$_SERVER['PHP_SELF']?>"><input type="hidden" name="keyword" value="'+keyword+'" /><?=($level>1) ? "<input type=\"hidden\" name=\"node_id\" value=\"'+node_id+'\" />" : ""?></form>';
	document.getElementById("tmpForm").submit();
}

-->
</script>
<body>
<div id="body" style=" width:100%;height:100%;overflow:hidden">

<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame">
<div id="listbody">
	<div>
		<span style="cursor:pointer" class="pageTitle" onClick="window.location='./list.php'"><?=$inv_cms->pageName?></span>-&gt; List (<?=getProdInfo($prodId, "name")?>)
	</div>
      <form action="" method="get" id="myform" name="myform">
      <div><input type="hidden" value="ok" name="refresh" />
	  		<input type="hidden" name="pageNo" value="<?=$pageNo?>" />
			<input type="hidden" name="thissql" value="<?=$thissql?>" />
			<input type="hidden" name="keyword_h" value="<?=$keyword?>" />
			<input type="hidden" name="rankStartForm" value="<?=(($pageNo-1)*$perPage+1)?>" />
			<input type="hidden" name="perPage" value="<?=$perPage?>" />
			<input type="hidden" name="tableName" value="<?=$tableName?>" />
			<input type="hidden" name="keyField" value="<?=$keyField?>" /></div>
			<div id="inputFields" style="position:static"></div>
      <table id="listTable" class="formline" cellspacing="0" cellpadding="0" <?=($totalWidth>0) ? "width=\"".$totalWidth."px\"" : ""?>>
        <tbody>
		<tr>
			<td style="height:30px; text-align:right; padding-left:5px; padding-right:5px">
				<div style="float:left;"><?= ($level > 1) ? "<input type=\"button\" name=\"manageCat\" value=\"Manage Categories\" class=\"btnItem\" onClick=\"this.disabled=true;window.location='manage_tree.php'\"/>" : ""?> <input type="button" name="addNew" value="Add New" class="btnItem" onClick="this.disabled=true;goAdd()"/></div>
				<div style="float:right;"><?= ($level > 1) ? "<input type=\"button\" name=\"manageCat\" value=\"Manage Categories\" class=\"btnItem\" onClick=\"this.disabled=true;window.location='manage_tree.php'\"/>" : ""?> <input type="button" name="addNew" value="Add New" class="btnItem" onClick="this.disabled=true;goAdd()"/></div>
            </td>
		</tr>
        <tr>
          <td colspan="5" style="padding-left:10px"><span>Keyword Search: <input id="tmpKeyword" type="text" name="keyword" value="" onKeyPress="if(event.keyCode==13) goSearch();" onFocus="this.select()" /> <input type="button" name="btnGoSearch" value="Go" style="CURSOR: pointer; width:40px" class="btnItem"  onClick="this.disabled=true;goSearch()" /></span></td>
		</tr>
        <? if ($level > 1) { ?>
        <tr>
          <td colspan="5" style="padding-left:10px"><span>Category Select: <select id="node_id" name="node_id" style="width:300px;" onChange="this.disabled=true;goSearch();">
    <?php
	require_once($pathClassLib.'/dbtree.class.php');

	// Create new object
	$dbtree = new dbtree(DB_TABLE, 'node', $conn);

    // Prepare the data for the second method:
    // Assigns a node with all its children to another parent
    $dbtree->Full(array('node_id', 'node_level', 'node_name', 'node_left', 'node_right'));

    while ($item = $dbtree->NextRow()) { ?>
                    <option value="<?=$item['node_id']."@".$item['node_left']."@".$item['node_right']?>" <?php echo $item['node_id'] == (int)$node_id[0] ? 'selected' : ''?> ><?=str_repeat('&nbsp;', 6 * $item['node_level'])?><?=html_entity_decode($item['node_name'])?> <?php echo $item['node_id'] == (int)$node_id[0] ? '<<<' : ''?></option>
        <?php
    }
    ?>
            </select></span></td>
        </tr>
        <? } ?>
        <?php if($keyword != "") { ?>
        <tr>
          <td colspan="5" style="padding-left:10px" align="left">Current Keyword: <?=$keyword?> <a href="list.php?keyword=">CLEAR SEARCH</a></td>
		</tr>        
        <?php } ?>
        <tr>
          <td colspan="5" style="padding-left:10px;padding-top:10px;" align="left"><?=$pageList?></td>      		
		</tr>
        <tr >
			<td colspan="5">
			
			<script type="text/javascript" language="javascript">
			  <!--
			  	<? 
					$widthList = "";
					foreach ( $tableColumn as $eachColumn )
						$widthList .= ($widthList == "") ? $eachColumn[3] : ", ".$eachColumn[3];
					echo "var colWidth = new Array(".$widthList.");\n";
				?>
				var colData = new Array(<? 
								$i=0;
								foreach ($tableColumn as $eachCol) {
									if($i++ > 0)
										echo ", ";
									echo "new Array('".$eachCol[1]."', ".$eachCol[2].")";
								}
								?>);
				var sortImg = new Array('arrow-none.gif', 'arrow-up.gif', 'arrow-down.gif');
				var defaultSortCol = 1;
				var defalutSortDir = sortImg[1];
				
				function calLeft ( inPos ) {
					var resultLeft = 0;
					for ( var calLeftCount = 0; calLeftCount < inPos; calLeftCount++ )
						resultLeft += this.colWidth[calLeftCount];
					return resultLeft;
				}
				
				function changeSort ( obj ) {
					var id = obj.id;
					var src = obj.src;
					
					id = id.replace("sortImg", "");
					
					if ( src.indexOf(sortImg[1]) < 0 ) {
						window.location='list.php?scol='+id+'&sdir=1';
					} else {					
						window.location='list.php?scol='+id+'&sdir=2';
					}
				}
				
				document.write('<li class="slide">\n<table cellpadding="0" cellspacing="0" border="0" ><tr style="height:20px;">\n');
				var thisSortImg;
				var thisSortCol = <?=$scol?>;
				var thisSortDir = <?=$sdir?>;
				
				if( thisSortDir != 1 && thisSortDir != 2 )
					thisSortDir = 1;
				
				for (var thCount = 0; thCount < colData.length; thCount++) {
					document.write( '<th style="font-size: 9pt;width:'+colWidth[thCount]+'px;">'+colData[thCount][0] );
					if( colData[thCount][1] ) {
						if( thisSortCol == thCount ) {
							thisSortImg = sortImg[thisSortDir];		// thisSortDir : 0 = up(asc) , 1 = down(desc)
						} else
							thisSortImg = sortImg[0];
						document.write( ' <img id="sortImg'+thCount+'" src="../images/'+thisSortImg+'" border="0" alt="" onclick="changeSort(this);" style="cursor:pointer" />' );
					}
					document.write( '</th>\n' );
				}
				document.write('</tr></table></li>\n');
			  -->
			</script>
			</td>
		</tr>
		
		<tr>
          <td class="catBottom" colspan="5">
          
          
            <script type="text/javascript" language="javascript">
			<!--
				if (dragSwitch || editSwitch)
					document.write('<input style="CURSOR: pointer" type="button" value="Update Ranking / Title" onClick="goUpdate(this.form)" />');
			-->
			</script>
          </td></tr>
		<tr>
			<td colspan="5" style="overflow:hidden">
		<div id="slideEditors">
			<script type="text/javascript" language="javascript">
			  <!--
			  	function changeUpdatedStyle(id) {
					var o_objName = id.replace("Edit", "View");
					var o_value = document.getElementById(o_objName).innerHTML;
					var n_value = document.getElementById(id).value;
					
					if(o_value != n_value)
						eval("document.getElementById('" + o_objName + "').style.color='#FF0000';");
				}
			  
				for (var i=0; i < noOfSortBox && editSwitch; i++) {
					document.write('<input id="'+slideId[i]+'@'+i+'sortBoxEdit" name="'+slideId[i]+'@'+i+'sortBoxEdit" class="inplace" onfocus="this.select();" onchange="changeUpdatedStyle(this.id)" />');
				}
			  -->
			</script>
		</div>
		<ul id="slideshow" class="slideshow">
			<script type="text/javascript" language="javascript">
			  <!--
				for (var i=0; i < noOfSortBox; i++) {
					document.write('<li class="slide">\n<table cellpadding="0" cellspacing="0" border="0" ><tr style="height:30px;">\n');
					document.write('<td style="width:'+colWidth[0]+'px;text-align:center;overflow:hidden" class="row'+(i%2+1)+'"><input style="BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px" type="checkbox" value="'+slideKey[i]+'" name="del'+i+'" /></td>\n');
					document.write('<td style="width:'+colWidth[1]+'px;text-align:center;overflow:hidden" class="row'+(i%2+1)+'"><input class="btnItem" type="button" value=" Edit " onclick="this.disabled=true;goEdit(\''+slideKey[i]+'\')" /> <input class="btnItem" type="button" value=" Delete " onclick="this.disabled=true;goDeleteOne(this.form, \''+slideKey[i]+'\', this)" /></td>\n');
					
					for (var thCount = 1; thCount < colWidth.length - 1; thCount++)
						document.write('<td style="width:'+colWidth[thCount+1]+'px;text-align:center;overflow:hidden" class="row'+(i%2+1)+'">'+eval("col"+thCount)[i]+'&nbsp;</td>\n');
					
					document.write('</tr></table></li>\n');
				}
				
			 -->
			</script>
		</ul>
			</td></tr>
		<tr>
          <td class="catBottom" colspan="5">
		  	<input class="btnItem" type="button" onClick="checkAll('myform', 'del');" value="Check All"/>
		  	<input class="btnItem" type="button" onClick="checkInverse('myform', 'del');" value="Check Inverse"/> <br/>
			<input class="btnItem warnBtn" type="button" value="Delete Selected Record(s)" onClick="this.disabled=true;goDelete(this.form)" />
			 
            <?php if (EXPORT_TO_EXCEL) { ?><input class="btnItem excelItem"  type="button" value="Export Excel" onClick="genCSV(this.form)" /><?php } ?>
            <script type="text/javascript" language="javascript">
			<!--
				if (dragSwitch || editSwitch)
					document.write('<input style="CURSOR: pointer" type="button" value="Update Ranking / Title" onClick="goUpdate(this.form)" />');
			-->
			</script>
          </td></tr>
		<tr>
			<td colspan="5" align="left" height="30">
				<br/>
				<a href="./list.php?node_id=<?=$node_id?>""><input type="button" name="btnReset" value="Back to List Page"  class="btnItem" style="margin:0px 8px"/></a>
			</td>
		</tr>
	  </tbody></table>
	  </form>
      <? include_once($rootPath . $adminPath."/framework/overall_footer.php");?>
</div>

</div>
<div id="extendsDiv"></div>
</body>
</html>
<? include_once($rootPath . $adminPath."/framework/end.php");?>
