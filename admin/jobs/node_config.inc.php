<?php
include_once("../../config.inc.php");
include_once("../../classes/product.php");

//$relTableName = "ProductsRelationship";
$keyField = "node_id";
$level = "1";
$perPage = 20;
$pageLinks = 10;


$cms = new Product();

//custom setting
$pageName = "Product Category";
$tableName = "product_node";

define("PARENT_NODE_SELECT", FALSE);
define("MULTI_CATEGORY", FALSE);
define('DB_CACHE', FALSE);
define("DB_TABLE", $tableName."_node");
define("DB_CATITEM_ID", "id");
define("DB_CATITEM_NAME", "name");
define("EXPORT_TO_EXCEL", FALSE);
define("DEFAULT_ERROR_MSG", FALSE);



//language
define("SHOW_LANG_BTN", FALSE);

//node setting(edit it only for categories)
define("RETURN_MAIN_EDIT", "product/manage_tree.php");


$fixedField = array (
	# array( Name, DB Field Name, type, array(list status, width, sortable, searchable), array(edit form status, update, insert, default selection/value), array(language version))

	array( "ID", "node_id", "INT", array("HIDDEN", "0", true, false), array("HIDDEN", false, false, array()) ),
	
	array( "Category Name", "node_name", "VARCHAR", array("SHOW", "180", true, false), array("SHOW", true, true, array()),array("", "zh", "cn")  ),
	array( "Category Description", "node_description", "TEXT", array("SHOW", "180", true, false), array("SHOW", true, true, array()),array("", "zh", "cn")  ),

	
);

$sortDir = array("1"=>"ASC" , "2"=>"DESC");
$dragSort = "true";
?>