<?php
ob_start();

require_once("../../config.inc.php"); # common config

$currversion = "";
if (isset($_version) && $_version) {
	$currversion = (array_key_exists($dbDatabase."_VERSION_LANG", $_SESSION)) ? $_SESSION[$dbDatabase.'_VERSION_LANG'] : $_version[0];
	$currversion = (array_key_exists("lang", $_POST)) ? $_POST['lang'] : $currversion;
	$currversion = (array_key_exists("lang", $_GET)) ? $_GET['lang'] : $currversion;
	$_SESSION[$dbDatabase. '_VERSION_LANG'] = $currversion;	
}
require_once("config.inc.php"); # config
require_once($rootPath . $adminPath."/config.inc.php"); # admin config
include_once($pathDocRoot . "/connDB.php"); # common connect db
require_once($pathClassLib."/function.common.php");
require_once($pathClassLib."/class.pageiterator.php");

if (array_key_exists("goAdd", $_GET)) {

	if (array_key_exists("lecturer", $_POST)) {
		$data = array();
		$data["job_id"] = (int)$_GET["goAdd"];
		
		foreach ($_POST["lecturer"] as $idx => $lecturer_id) {
			$data["lecturer_id"] = (int)$lecturer_id;
			$permission = array();
			foreach (["READ", "EDIT", "DELETE"] as $right) {
				$permission[$right] = in_array($right, $_POST["permission"][$idx]) ? 1 : 0;
			}
			$data["permission"] = json_encode($permission, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);

			if ($data["lecturer_id"]) {
				$conn->AutoExecute("jobs_lecturer", $data, "INSERT");
			}
		}
	}

	if (array_key_exists("n_permission", $_POST)) {
		$data = array();
		$data["job_id"] = (int)$_GET["goAdd"];
		
		foreach ($_POST["n_lecturer"] as $idx => $lecturer_id) {
			$data["lecturer_id"] = (int)$lecturer_id;

			$permission = array();
			foreach (["READ", "EDIT", "DELETE"] as $right) {
				$permission[$right] = in_array($right, $_POST["n_permission"][$idx]) ? 1 : 0;
			}
			$data["permission"] = json_encode($permission, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);

			if ($data["lecturer_id"]) {
				$conn->AutoExecute("jobs_lecturer", $data, "UPDATE", "id='".$idx."'");
			}
		}
	}
	
	if (array_key_exists("delRow", $_POST)) {
		foreach ($_POST["delRow"] as $idx => $program_id) {
			$conn->Execute("DELETE FROM jobs_lecturer where id='".$idx."'");
		}
	}

	header("location: permission.php?id=".$_GET["goAdd"]);
	exit();

}

header('Cache-Control: no-cache');
header('Pragma: no-cache');
if (!isset($_SESSION)) {
	session_start();
}

// Set global variables if not already set
$node_id = "";
$refresh = (array_key_exists("refresh", $_GET)) ? $_GET['refresh'] : "";
if($refresh == "1") echo "<script language=\"javascript\">window.location.replace('".$_SERVER['PHP_SELF']."');</script>";
$arrVar = $conn->MetaColumns($tableName);
$arrVar = $conn->MetaColumnNames($tableName);
foreach ($arrVar as $var) {
	$$var = "";
}
if ($level == 0)
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : 1;
else
	$id = array_key_exists("id", $_GET) ? $_GET['id'] : "";


if($id != "") {
	$thissql = "SELECT * FROM `".$tableName."` WHERE `".$keyField."`='".$id."'";
	$rs = $conn->SelectLimit($thissql, 1, 0);
	$totalRecord = $rs->RecordCount();

	if($totalRecord > 0) {
		while (!$rs->EOF) {	
			foreach ($rs->fields as $key=>$value) {
				$$key = $value;
			}					
			$rs->MoveNext();
		}
	}
	
}


$rsPermission = $conn->GetAll("Select * from jobs_lecturer where job_id = '".$id."'");

$printForm = "";
$printHidden = "";

$printCreateCalendarScript = "";



?>
<?php require_once( $rootPath . $adminPath."/framework/overall_header.php" );?>
<script type="text/javascript" src="<?=$adminPath?>/js/core.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/events.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/css.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/coordinates.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/drag.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/dragsort.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?=$adminPath?>/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckeditor.js"></script>
<script type="text/javascript" src="<?=$fckEditorPath?>/ckfinder/ckfinder.js"></script>
<link href="<?=$adminPath?>/css/calendar-tas.css" rel="stylesheet" type="text/css">
<link href="<?=$adminPath?>/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">



<?=(array_key_exists("gmap", $cms->admin_js)) ? implode("\n", $cms->admin_js["gmap"]) : ""?>

<script type="text/javascript">
	var sBasePath = "<?=$fckEditorPath?>/";
	var fcklist = <?=(array_key_exists("ckeditor", $cms->admin_js)) ? "['".implode("','", $cms->admin_js["ckeditor"]). "']" : "[]" ?>;

	function checkdatetime(input) {
		var validformat=/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/; //Basic check for format validity
		var returnval=false;
		if(input.value != "") {
			if (!validformat.test(input.value))
				alert("Invalid Date Format. Please correct and submit again.");
			else{ //Detailed check for valid date ranges
				var dateArr = input.value.split(" ")[0];
				var yearfield=dateArr.split("-")[0];
				var monthfield=dateArr.split("-")[1];
				var dayfield=dateArr.split("-")[2];
				var dayobj = new Date(yearfield, monthfield-1, dayfield);
				if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
					alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
				else
					returnval=true;
			}
			return returnval;
		} else {
			return true;
		}
	}
			 
	function checkdate(input) {
		var validformat=/^\d{4}\-\d{2}\-\d{2}$/; //Basic check for format validity
		var returnval=false;
		if(input.value != "") {
			if (!validformat.test(input.value))
				alert("Invalid Date Format. Please correct and submit again.");
			else{ //Detailed check for valid date ranges
				var yearfield=input.value.split("-")[0];
				var monthfield=input.value.split("-")[1];
				var dayfield=input.value.split("-")[2];
				var dayobj = new Date(yearfield, monthfield-1, dayfield);
				if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
					alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
				else
					returnval=true;
			}
			return returnval;
		} else {
			return true;
		}
	}
	
	function getFckEditorTextLength(fckName) {
		// This functions shows that you can interact directly with the editor area
		// DOM. In this way you have the freedom to do anything you want with it.
		
		// Get the editor instance that we want to interact with.
		var oEditor = FCKeditorAPI.GetInstance(fckName) ;
	
		// Get the Editor Area DOM (Document object).
		var oDOM = oEditor.EditorDocument ;
	
		var iLength ;
	
		// The are two diffent ways to get the text (without HTML markups).
		// It is browser specific.
	
		if ( document.all ) { // If Internet Explorer.
			var innerText = oDOM.body.innerText;
			iLength = innerText.length ;
		} else {// If Gecko.
			var r = oDOM.createRange() ;
			r.selectNodeContents( oDOM.body ) ;
			iLength = r.toString().length ;
		}
	
		return iLength;
	}
	
	function setEditorValue( instanceName, text )
	{  
	  /* Old Version Coding
	  // Get the editor instance that we want to interact with.
	  //var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	  
	  // Set the editor contents.
	  //oEditor.SetHTML( text ) ;
	  */
	  CKEDITOR.instances[instanceName].setData(text);
	} 
	
	function getEditorValue( instanceName ) 
	{  
	  /* Old Version Coding
	  // Get the editor instance that we want to interact with.
	  //var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	  
	  // Get the editor contents as XHTML.
	  //return oEditor.GetXHTML( true ) ;  // "true" means you want it formatted.
	  */
	  CKEDITOR.instances[instanceName].getData();
	}
	
	function goUpdate(myForm) {
		//var name = myForm.name.value;
		//var description = getEditorValue("desc");
		var errorFlag = false;
		var errorMsg = "";
		/*	
		if (name == "") {
			errorMsg += "Please input the name\n";
			errorFlag = true;
		}
		if (description == "") {
			errorMsg += "Please input the description\n";
			errorFlag = true;
		}
		*/
		if(errorFlag) {
			alert(errorMsg);
			myForm.btnUpdate.disabled = false;
			return false;
		} else {
			myForm.method = "post";
			myForm.action = "?goAdd=<?=$id?>";
			myForm.submit();
		}
	}
	
	function resetForm( myForm ) {
		myForm.reset();
		for (var i = 0; i < fcklist.length; i++) {
			CKEDITOR.instances[fcklist[i]].setData(document.getElementById(fcklist[i]).value);
		}
	}
	
	window.onload = function() {
		for (var i = 0; i < fcklist.length; i++) {
			eval("var oFCKeditor" + (i+1) + " = CKEDITOR.replace('"+fcklist[i]+"')");	
			eval("CKFinder.setupCKEditor(oFCKeditor" + (i+1) + ", '<?=$fckEditorPath?>/ckfinder/' )");
		}
				
	}
	function addbr() { 
	    //Create an input type dynamically.
		var fxloat = document.createElement("div"); 
		fxloat.style.width = '640px';	
		fxloat.style.clear = 'both';	
		return fxloat;
	}
	
	function golang(lang) {
		if (confirm('Any unsaved data will be lost. Are you sure?')) {
			window.location = '?id=<?=$id?>&lang='+lang;
		}
	}
	
	function add2(target, obj, css, attri){
		var obj = $(target).find('div').eq(0).clone().css('background-color','#CCCCFF');
		obj.find('input').val('');
		$(target).append(obj);
	}
	
	function add(fx, name,type,value) { 
	    //Create an input type dynamically.
		var element = document.createElement("input"); 
		
	    //Assign different attributes to the element.
	    element.setAttribute("type", type);
	    element.setAttribute("value", value);
	    element.setAttribute("name", name);
		element.setAttribute("id", name);	
	 	element.style.cssFloat = 'left';
		element.style.width = '127px';	
		//element.style.marginRight = '19px';	
		
	    fx.appendChild(element);
	}
	
	function removeThis(obj) {
		if (confirm('Are you sure to remove this row?')) {
			$(obj).parentsUntil('.json_row').parent().remove();
		}
	}



	function toggleCheckBox(id){

		var current = document.getElementById(id).disabled;
		document.getElementById(id).disabled = !current;

		if(current == true){
			document.getElementById(id).style.visibility = "visible";
		}else{
			document.getElementById(id).style.visibility = "hidden";
		}
		
	}
		
</SCRIPT>
<style>
	ul.permission { margin: 0; padding: 0; list-style: none; }
	ul.permission li { margin: 0; padding: 0 }
</style>
<link href="/css/select2.min.css" rel="stylesheet" />
<script src="/js/select2.min.js"></script>
<body>
<div id="body" style=" width:100%;height:100%;overflow: hidden;">

<div id="leftNav">
<?php include_once($rootPath . $adminPath."/framework/left_nav.php");  ?>
</div>

<div id="mainFrame" >
<div id="listbody">
	<div style="">
		<div >
		 	<span class="pageTitle" style="cursor:pointer" onClick="window.location='./list.php'"><?=$cms->pageName?></span>
		 	 -> <?=($id=="") ? "Add" : "Edit"?>
		</div>
		
   		
		<table id="editTable" >
		  <tbody>
		    <tr>
		    <td>
		    	<?php if ($id != "") { ?><div class="tab">
			<ul>
				<li><a href="edit.php?id=<?=$id?>" >Record</a></li>
				<li><a href="target.php?id=<?=$id?>" >Target Student</a></li>
				<li class="active">Permission</a></li>
			</ul>
		</div><?php
					}
				?>
		      <form action="" method="post" id="myform" enctype="multipart/form-data" >
		      	<input type="hidden" value="ok" name="refresh" /><?=$printHidden?></div>
		  	  <table class="formline_edit" cellspacing="0" cellpadding="6" width="100%" style="padding-bottom:5px;">
		        <tbody>
				<tr><th width="250">Job No: </th><td><?=$job_no ?></td></tr>
				<tr><th width="250">Position: </th><td><?=$name ?></td></tr>
				<tr><th width="250">Department / Company:</th><td><?=$dept ?></td></tr>
				<tr><th width="250">Permission:</th>
					<td>
						<div >
							<div class="col-xs-4 " style="padding:1em 0em">Lecturer</div>
							<div class="col-xs-6 " style="padding:1em 0em">Permission</div>
						</div>
						<ul class="permission">
							<?php
							foreach ($rsPermission as $data) {
								$lecturer = $conn->GetRow("Select * from lecturer where id = '".$data["lecturer_id"]."'");

								$data["permission"] = json_decode( $data["permission"], true);
								?><li class="row" data-id="" style="clear:both">
									<div class="col-xs-4"><select class="select2 col-xs-10" name="n_lecturer[<?=$data["id"]?>]">
										<option value="<?=$lecturer["id"]?>" selected="selected"><?=$lecturer["username"]?></option>
									</select></div>
									<div class="col-xs-6  ">
										<div class="col-xs-3"><label ><input type="checkbox" name="n_permission[<?=$data["id"]?>][]" value="READ" <?=($data["permission"]["READ"] == 1) ? 'checked="checked"' : ""?> /> READ </label></div>
										<div class="col-xs-3"><label ><input type="checkbox" name="n_permission[<?=$data["id"]?>][]" value="EDIT" <?=($data["permission"]["EDIT"] == 1) ? 'checked="checked"' : ""?> /> EDIT </label></div>
										<div class="col-xs-3"><label ><input type="checkbox" name="n_permission[<?=$data["id"]?>][]" value="DELETE" <?=($data["permission"]["DELETE"] == 1) ? 'checked="checked"' : ""?> /> DELETE </label></div>
										
									</div>
									<div class="col-xs-2"><input type="checkbox" name="delRow[<?=$data["id"]?>]" /> Delete</div>
								</li><?
							}
							?>
							<li class="row new" data-id="" style="clear:both">
								<div class="col-xs-4"><select class="select2 col-xs-10" name="lecturer[]">
									<option></option>
								</select></div>
								<div class="col-xs-6  col-xs-offset-2">
									<div class="col-xs-3"><label ><input type="checkbox" name="permission[0][]" value="READ" /> READ </label></div>
									<div class="col-xs-3"><label ><input type="checkbox" name="permission[0][]" value="EDIT" /> EDIT </label></div>
									<div class="col-xs-3"><label ><input type="checkbox" name="permission[0][]" value="DELETE" /> DELETE </label></div>
									
								</div>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td >&nbsp;</td>
					<td><button class="btn" onclick="return addNew()" >Add New</button></td></tr>
		        <?php if ($level > 0) { ?>
				<tr>
					<td colspan="3" align="center">&nbsp;</td>
				</tr>
		        <?php } ?>
			  </tbody></table>
				<div style="width:100%;">
				
				  	<div style="float:left">
						<a href="./list.php?node_id=<?=$node_id?>"  class="btnItem" style="margin:0px 8px">Back to List Page</a>
					</div>
				 	<div style="float:right">
						<?php 
				 			if(EDIT_NEED){
				 			?><input type="button" name="btnReset" value=" Reset "  class="btnItem" onClick="resetForm(this.form)" style="margin:0px 8px"/>
						  	<input type="button" id="btnUpdate" name="btnUpdate" value=" <?=($id=="") ? "Add" : "Update"?> " class="btnItem submitItem" onClick="this.disabled=true;goUpdate(this.form)"  style="padding-left:32px;padding-right:32px;margin:0px 8px;"/> 
				 		<?php 
				 			}
				 		?>
				 	</div>	
				
				</div>
			  </form>
		</td></tr></tbody>
		
		</table>
	      <?php include_once($rootPath . $adminPath."/framework/overall_footer.php");  ?>
	</div></div>
</div>
</div>
<script type='text/javascript' src='<?=$adminPath?>/js/calendar.js'></script>
<script type='text/javascript' src='<?=$adminPath?>/js/calendar-en.js'></script>
<script type='text/javascript' src='<?=$adminPath?>/js/calendar-setup.js'></script>";
<script type="text/javascript">
	var new_row = '';
	$(function(){
		new_row = $('.permission .row.new').clone();
		$('.select2').select2({
			ajax: {
		    url: '<?=$appPath ?>/xhr/lecturer.xhr.php',
		    dataType: 'json',
		}});

		<?=(array_key_exists("calendar", $cms->admin_js)) ? implode("\n", $cms->admin_js["calendar"]) : ""?>	
	
		


	});
		
	var addNew = function(){
		var nrow = new_row.clone();
		nrow.find("input[name='year[0][]']").prop("name", "year["+$('.permission li.new').length+"][]")
		$('ul.permission').append(nrow);
		nrow.find('.select2').select2({
			ajax: {
		    	url: '<?=$appPath ?>/xhr/lecturer.xhr.php',
		    	dataType: 'json',
		    }
		});

		return false;
	}	        

	$(function(){
		$("input.checkAllToggle").on("click", function(e){
			var currentCheckedStatus = $(this).prop("checked");
			$(this).parent().parent().parent().find(".checkboxContainer").find("input[type=checkbox]").prop("checked", currentCheckedStatus);
		});
	});


</script>

</body>
</html>

<?php include_once($rootPath . $adminPath."/framework/end.php"); ?>