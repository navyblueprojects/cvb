<?php
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = [];
?>
	<main id="home">
		<div >
			<section id="home_banner" class="col-md-12">
				<div class="wrapper">
					<div class="col-md-6" id="banner">
						<div class="cms"  data-cms-title="home_banner" ><?=$cmsObj->getPageArea("home_banner")?></div>
						<button class="btn btn-default" data-toggle="modal" data-target="#loginReminderModal"><span class="fa fa-lock"></span> Login</button>
					</div>
				</div>
			</section>
			<section id="home_menu" class="col-md-12">
				<div class="wrapper">
					<div class="cms"  data-cms-title="home_how_it_works"><?=$cmsObj->getPageArea("home_how_it_works")?></div>
					<ul>
						<li class="container col-md-4" >
							<div><a href="login"><i class="fas fa-edit fa-8x"></i></a></div>
								 <a href="login"><div class="cms" data-cms-title="home_how_it_works_col_1">
								<?=$cmsObj->getPageArea("home_how_it_works_col_1")?>
							</div></a>
						</li>
						<li class="container col-md-4">
							<div><a href="login"><i class="fas fa-user fa-8x"></i></a></div>
								 <a href="login"><div class="cms" data-cms-title="home_how_it_works_col_2">
								<?=$cmsObj->getPageArea("home_how_it_works_col_2")?>
							</div></a>
						</li>
						<li class="container col-md-4" >
							<div><a href="login"><i class="fas fa-list-alt fa-8x"></i></a></div>
							<a href="login"><div class="cms" data-cms-title="home_how_it_works_col_3">
								<?=$cmsObj->getPageArea("home_how_it_works_col_3")?>
							</div></a>
						</li>
					</ul>
				</div>
			</section>
		</div>
		<div >
			<div class="modal" tabindex="-1" role="dialog" id="loginReminderModal">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <span class="modal-title">Reminder: This window will be closed and redirect to CPCE Single Sign On login page in <span class="count">15</span> Seconds.</span>
			        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close" >
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	<div class="row">
			      	<div class="announce_card col-md-6">
						<h2>News</h2>
						<div class="cms" data-cms-title="login_news"><?=$cmsObj->getPageArea("login_news")?></div>
					</div>
					<div  class="announce_card col-md-6">
						<h2>Reminder</h2>
						<div class="cms" data-cms-title="login_reminders"><?=$cmsObj->getPageArea("login_reminders")?></div>
					</div>
				</div>
					
			      </div>
			      <div class="modal-footer"><button class="btn btn-default" onclick="window.location='login'"><span class="fa fa-lock"></span> Login Now</button>
			      </div>
			    </div>
			  </div>
			</div>
			
		</div>
	</main>
<?php
	include_once("footer.php");
?>
