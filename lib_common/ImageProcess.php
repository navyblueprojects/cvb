<?
// RESIZE PHOTO FUNCTION

function saveThumbnail($image_file, $dest_file, $frame_w, $frame_h, $mode)
{
	// get source image info.
	$img_data = getimagesize($image_file);	// or die ("Cannot open source");
	$src_w = $img_data[0];
	$src_h = $img_data[1];
	$src_type = $img_data[2];
	
	if ($src_type != 2) die("Image not JPEG");
	
	if ($mode == 3) {
		$src_img = ImageCreateFromJPEG($image_file);	// or die ("Cannot open source");
		$dst_img = ImageCreateTrueColor($frame_w, $frame_h);
		imagefill($dst_img, 0, 0, imagecolorallocate(imagecreatetruecolor(100, 100), 245, 245, 245));
		
		if (($src_w / $src_h) >= ($frame_w / $frame_h)) {
			$reszie_rato = ($frame_w/$src_w);
			$new_w = $frame_w;
			$new_h = $src_h * $reszie_rato;
			
			// create the thumbnail
			imageCopyResampled($dst_img,$src_img, 0, ($frame_h - $new_h)/2, 0, 0, $new_w, $new_h, $src_w, $src_h);
		} else {
			$reszie_rato = ($frame_h/$src_h);
			$new_h = $frame_h;
			$new_w = $src_w * $reszie_rato;
			
			// create the thumbnail
			imageCopyResampled($dst_img,$src_img, ($frame_w - $new_w)/2, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
		}
		
		// save image to file
		imagejpeg($dst_img, $dest_file, 100);
		
		// free up the memory.
		ImageDestroy($src_img);
		ImageDestroy($dst_img);
		
	} else if ($src_w <= $frame_w && $src_h <= $frame_h) {
		
		copy($image_file, $dest_file);
		
	} else if ($mode == 2) {
		
		$max_length = $frame_w;
		
		if ($src_w > $src_h) {
			$reszie_rato = ($max_length/$src_w);
			$new_w = $max_length;
			$new_h = $src_h * $reszie_rato;
		} else {
			$reszie_rato = ($max_length/$src_h);
			$new_w = $src_w * $reszie_rato;
			$new_h = $max_length;
		}
		
		$src_img = ImageCreateFromJPEG($image_file);	// or die ("Cannot open source");
		$dst_img = ImageCreateTrueColor($new_w, $new_h);
		
		// create the thumbnail
		imageCopyResampled($dst_img,$src_img, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
		
		// save image to file
		imagejpeg($dst_img, $dest_file, 100);
		
		// free up the memory.
		ImageDestroy($src_img);
		ImageDestroy($dst_img);
		
	} else if ($mode == 1) {
		
		$src_img = ImageCreateFromJPEG($image_file);	// or die ("Cannot open source");
		$dst_img = ImageCreateTrueColor($frame_w, $frame_h);
		
		if (($src_w / $src_h) >= ($frame_w / $frame_h)) {
			$new_h = $frame_h;
			$new_w = $src_w * $frame_h / $src_h;
			
			// create the thumbnail
			imageCopyResampled($dst_img,$src_img, -($new_w - $frame_w)/2, 0, 0, 0, $new_w, $frame_h, $src_w, $src_h);
		} else {
			$new_w = $frame_w;
			$new_h = $src_h * $frame_w / $src_w;
			
			// create the thumbnail
			imageCopyResampled($dst_img,$src_img, 0, -($new_h - $frame_h)/2, 0, 0, $frame_w, $new_h, $src_w, $src_h);
		}
		
		// save image to file
		imagejpeg($dst_img, $dest_file, 100);
		
		// free up the memory.
		ImageDestroy($src_img);
		ImageDestroy($dst_img);
	}
}
?>