var NS=(navigator.appName=='Netscape')?true:false;
var ft;
var mw;
var mh;
var xw;
var xh;
var xc;

function fitFlash(t,w,h,_w,_h,c) {
	if(t&&w&&h) {
		ft=t;
		mw=w;
		mh=h;
		xw=_w;
		xh=_h;
		xc=(c==false||c==0)?false:true;
		document.getElementById(ft).style.position='relative';
	}
	if (xw <= xh) {
		document.getElementById(ft).style.width=xw+'px';
		document.getElementById(ft).style.height=Math.floor(parseInt(mh)/parseInt(mw)*parseInt(xw))+'px';
	} else {
		document.getElementById(ft).style.width=Math.floor(parseInt(mw)/parseInt(mh)*parseInt(xh))+'px';
		document.getElementById(ft).style.height=xh+'px';
	}
}

function getFlashObj(flashId){
   if (window.document[flashId]) {
      return window.document[flashId];
   }
   if (navigator.appName.indexOf("Microsoft Internet")==-1) {
      if (document.embeds && document.embeds[flashId]) {
         return document.embeds[flashId]; 
      }
   } else {
      return document.getElementById(flashId);
   }
}

function flashAutoDimensions(flashId) { // args: (flash id, max. width, max. height)
   var flashObj = getFlashObj(flashId);
   var width = flashObj.TGetProperty("/", 8);
   var height= flashObj.TGetProperty("/", 9);
   var maxWidth = (arguments[1]) ? ((arguments[1]>0) ? arguments[1] : 0) : width;
   var maxHeight = (arguments[2]) ? ((arguments[2]>0) ? arguments[2] : 0) : height;
   fitFlash(flashId, width, height, maxWidth, maxHeight);
}