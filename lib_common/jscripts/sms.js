var CHINESE_WORD_PER_PAGE = 70;
var ENGLISH_WORD_PER_PAGE = 160;

function detectVersion(value) {	
	if(ChkChiStr(value))
		return CHINESE_WORD_PER_PAGE;
	else
		return ENGLISH_WORD_PER_PAGE;
}

function checkSMSCount(value) {
	var noOfPage = 1;
	
	noOfPage = (value.length/detectVersion(value));
	
	if (noOfPage < 1)
		noOfPage = 1;
	
	document.getElementById("smsCount").innerHTML = Math.ceil(noOfPage);
}

function countChar(value) {	
	document.getElementById("divCharCount").innerHTML = value.length;
	checkSMSCount(value);
}

function ChkChiStr(value) {

    var i=0;
    var num;
    var chiFlag = false;

    /*if (typeof document.userform.sender != "undefined") {
        if (form.sender.value.length != 0) {
            for (i=0; i<form.sender.value.length; i++) {
                letter = form.sender.value.charAt(i);
                num = escape(letter);
                if ((num.substring(1,3))>'7F') {
                    chi = 1;
                    break;
                }
            }
        }
    }*/

    if (value.length != 0) {
        for (i=0; i<value.length; i++) {
            letter = value.charAt(i);
            num = escape(letter);
            if ((num.substring(1,3))>'7F') {
                chiFlag = true;
                break;
            }
        }
    }
    return chiFlag;
}