<?php

function imgResize($Dir,$Image,$NewDir,$NewImage,$MaxWidth,$MaxHeight,$Quality) {
  list($ImageWidth,$ImageHeight,$TypeCode)=getimagesize($Dir.$Image);
  $ImageType=($TypeCode==1?"gif":($TypeCode==2?"jpeg":
             ($TypeCode==3?"png":FALSE)));
  $CreateFunction="imagecreatefrom".$ImageType;
  $OutputFunction="image".$ImageType;
  if ($ImageType) {
   $Ratio=($ImageHeight/$ImageWidth);
   $ImageSource=$CreateFunction($Dir.$Image);
   if ($ImageWidth > $MaxWidth || $ImageHeight > $MaxHeight) {
     if ($ImageWidth > $MaxWidth) {
         $ResizedWidth=$MaxWidth;
         $ResizedHeight=$ResizedWidth*$Ratio;
     }
     else {
       $ResizedWidth=$ImageWidth;
       $ResizedHeight=$ImageHeight;
     }        
     if ($ResizedHeight > $MaxHeight) {
       $ResizedHeight=$MaxHeight;
       $ResizedWidth=$ResizedHeight/$Ratio;
     }      
     $ResizedImage=imagecreatetruecolor($ResizedWidth,$ResizedHeight);
     imagecopyresampled($ResizedImage,$ImageSource,0,0,0,0,$ResizedWidth,
                         $ResizedHeight,$ImageWidth,$ImageHeight);
   } 
   else {
     $ResizedWidth=$ImageWidth;
     $ResizedHeight=$ImageHeight;      
     $ResizedImage=$ImageSource;
   }    
   $OutputFunction($ResizedImage,$NewDir.$NewImage,$Quality);
   return true;
  }    
  else
   return false;
}

?>