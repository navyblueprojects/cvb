<?
// Class for add/remove/remove all  item in cart
class Cart {	
	/**
	 * array of items info in "cart"
	 * 
	 * @var 2 dimensions array
	 * @access private
	 */
	var $items;
	
	function Cart() {
		$this->items = array();
	}
	
	function AddItem($id, $qty, $extra = array()) {
		if( !$this->_duplicate($id)) {
			$tempArr = array();

			$tempArr["itemid"] = $id;
			$tempArr["itemqty"] = $qty;
			foreach($extra as $eachExtra=>$eachValue) {
				$tempArr[$eachExtra] = $eachValue;
			}
			$this->items[] = $tempArr;
		}
	}
	
	

	function UpdateItem($id,$qty, $extra = array()) {
		$count = count($this->items);
		for($i=0;$i<$count;++$i) {
			if($this->items[$i]['itemid']== $id) {
				
				$this->items[$i]['itemqty'] = $qty;
				foreach($extra as $eachExtra=>$eachValue) {
					$this->items[$i][$eachExtra] = $eachValue;
				}
				return true;
			}
		}
		return false;
	}
	
	function RemoveItem($id) {
		// loop thru array
		$count = count($this->items);
		for($i=0;$i<$count;++$i) {
			if($this->items[$i]["itemid"]==$id) {
				// Remove the Item in Cart
				unset($this->items[$i]);
		
				// Reindexing array
				$this->items = array_values($this->items);				
				return true;
			}
		}
		return false;
	}

	function RemoveAll() {
		// delete whole array
		unset($this->items);	
	}
	
	function _duplicate($id) {
		$found = false;
		$count = count($this->items);
		for($i=0;$i<$count;++$i) {
			if($this->items[$i]["itemid"]==$id) {
				$found = true;
				break;
			}
		}

		return $found;
	}
	
	function getItem($id) {
		$count = count($this->items);
		for($i=0;$i<$count;++$i) {
			if($this->items[$i]['itemid']== $id) {
				return $this->items[$i];
			}
		}	
	}

	function getItemQty($id) {
		$count = count($this->items);
		for($i=0;$i<$count;++$i) {
			if($this->items[$i]['itemid']== $id) {
				return $this->items[$i]['itemqty'];
			}
		}	
	}
	
	
	function getItems() {
		return $this->items;
	}

	function getCount() {
		return count($this->items);
	}
	
	function getItemIdList($attr){
		$list = array();
		
		if($this->getCount() > 0 ){
			
			foreach($this->getItems() as $item ){
				if(!empty($item[$attr])){
					array_push($list,$item[$attr]);
				}
			}
		}
		
		return $list;
	}
	
	
}
?>