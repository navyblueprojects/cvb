<?php
/*
  ****************************************************************************
  * Class PageIterator                                                       *
  * Version 1.0                                                              *
  *                                                                          *
  * A PHP class for browsing through a large set of data                     *
  *                                                                          *
  * Copyright (C) 2003 by Karsten Juul Mikkelsen, kjm@arachnodata.dk         *
  *                                                                          *
  * This PHP class is free software; you can redistribute it and/or          *
  * modify it under the terms of the GNU Lesser General Public               *
  * License as published by the Free Software Foundation; either             *
  * version 2.1 of the License, or (at your option) any later version.       *
  *                                                                          *
  * This PHP class is distributed in the hope that it will be useful,        *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU         *
  * Lesser General Public License for more details.                          *
  *                                                                          *
  * You should have received a copy of the GNU Lesser General Public         *
  * License along with this PHP class; if not, write to the Free Software    *
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA  *
  *                                                                          *
  * Author:                                                                  *
  * Karsten Juul Mikkelsen, 8900 Randers, Denmark, kjm@arachnodata.dk        *
  *                                                                          *
  ****************************************************************************
*/
class PageIterator {
    var $elementCount = 0;
    var $pageCount, $perPage, $currentPage, $currentElement;
    var $firstPageElement, $lastPageElement, $nextPageElement;
    var $firstPageLink, $lastPageLink, $nextPageLink;

    function PageIterator($elementCount, $currentPage = 1, $elementsPerPage = 10, $maxLinks = '', $currentElement = 0) {
        if ($elementCount > 0) {
            $this -> elementCount = $elementCount;
            $this -> perPage = $elementsPerPage;
        }
        
        $this -> _setPageCount();
        
        // Calculate current page and element numbers
        $this -> currentElement = $currentElement;
        if ($currentElement > 0) {
            $this -> _setCurrentPage();
            $this -> _setFirstPageElement();
        }
        else {
            $this -> currentPage = $currentPage;
            if ($this -> currentPage < 1) {
                $this -> currentPage = 1;
            }
            elseif ($this -> currentPage > $this -> pageCount) {
                $this -> currentPage = $this -> pageCount;
            }
            $this -> _setFirstPageElement();
            $this -> currentElement = $this -> firstPageElement;
        }
        $this -> lastPageElement = $this -> firstPageElement + $this -> perPage - 1;
        if ($this -> lastPageElement >= $this -> elementCount) {
            $this -> lastPageElement = $this -> elementCount - 1;
        }
        $this -> nextPageElement = $this -> firstPageElement;
        
        // Calculate which pages should be linked to
        if (empty($maxLinks) or ($maxLinks >= $this -> pageCount)) {
            $this -> firstPageLink = 1;
            $this -> lastPageLink = $this -> pageCount;
        }
        else {
            if ($maxLinks < 3) {
                $maxLinks = 3;
            }
            $this -> lastPageLink = $this -> currentPage + floor($maxLinks / 2);
            if ($this -> lastPageLink > $this -> pageCount) {
                $this -> lastPageLink = $this -> pageCount;
            }
            $this -> firstPageLink = $this -> lastPageLink - ($maxLinks - 1);
            if ($this -> firstPageLink < 1) {
                $this -> firstPageLink = 1;
                $this -> lastPageLink = $maxLinks;
            }
        }
        $this -> nextPageLink = $this -> firstPageLink;
    } // constructor
    
    /*
    Public functions
    */
    
    // Page numbers
    function hasNextPage() {
        return ($this -> nextPageLink <= $this -> lastPageLink);
    }
    
    function nextPage() {
        return $this -> nextPageLink++;
    }
    
    function firstPage() {
        $this -> nextPageLink = $this -> firstPageLink;
        return $this -> nextPage();
    }
    
    function getPreviousPageNumber() {
        if ($this -> currentPage <= 1)
            return false;
        else return $this -> currentPage - 1;
    }
    
    function getNextPageNumber() {
        if ($this -> currentPage >= $this -> pageCount)
            return false;
        else return $this -> currentPage + 1;
    }
    
    function getFirstPageNumber() {
        return $this -> firstPageLink;
    }
    
    function getLastPageNumber() {
        return $this -> lastPageLink;
    }
    
    function getCurrentPageNumber() {
        return $this -> currentPage;
    }
    
    function getPageCount() {
        return $this -> pageCount;
    }

    // Element numbers
    function hasNextElement() {
        return ($this -> nextPageElement <= $this -> lastPageElement);
    }
    
    function nextElement() {
        return $this -> nextPageElement++;
    }
    
    function firstElement() {
        $this -> nextPageElement = $this -> firstPageElement;
        return $this -> nextElement();
    }
    
    function getFirstElementNumber() {
        return $this -> firstPageElement;
    }
    
    function getLastElementNumber() {
        return $this -> lastPageElement;
    }
    
    function getElementCount() {
        return $this -> elementCount;
    }
    /*
    Private methods
    */
    function _setPageCount() {
        if (!$this -> elementCount) $this -> pageCount = 0;
        else {
            $this -> pageCount = floor($this -> elementCount / $this -> perPage);
            if (($this -> elementCount % $this -> perPage) > 0) {
                $this -> pageCount++;
            }
        }
    } // _setPageCount()

    function _setCurrentPage() {
    // Calculate current page number from current element number
        if (!$this -> elementCount) {
            $this -> currentPage = 0;
        }
        else {
            $this -> currentPage = floor($this -> currentElement / $this -> perPage);
            if (($this -> currentElement % $this -> perPage) > 0) {
                $this -> currentPage++;
            }
            // Make sure currentPage is not off limits
            if ($this -> currentPage < 1) {
                $this -> currentPage = 1;
            }
            if ($this -> currentPage > $this -> pageCount) {
                $this -> currentPage = $this -> pageCount;
            }
        }
    } // _setCurrentPage()
    
    function _setFirstPageElement() {
    // Find the first element in the current page
        $this -> firstPageElement = ($this -> currentPage - 1) * $this -> perPage;
    } // _setFirstPageElement()
} // class PageIterator
?>
