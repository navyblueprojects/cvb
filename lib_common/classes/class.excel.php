<?
//Fiz essa classe pela necessidade de exportar 
//dados vindos de um banco sql server (vc pode usar BD)
//para se utilizar basta instanciar a classe
//passando como parametros os titulos das colunas
//dentro de um vetor e o segundo parametro sendo a query em si
//a classe chama por padrao o ponteiro $db do ADODB, caso n seja esse o nome
//fique a vontade para modificar a classe
//(A classe GeralExcel n eh minha)
//Creditos a DzaiaCuck - dzaiacuck@ig.com.br 
//Rubens A. Monteiro - unplu@hotmail.com 20/09/05

class  GeraExcel{

	// define parametros(init)
	function  GeraExcel(){
		$this->armazena_dados   = ""; // It stores given for imprimir(temporario)
		$this->ExcelStart();
	}// constructor end

     
	// Sum cabecario of arquivo(tipo xls)
	function ExcelStart(){
		// beginning of the cabecario of the archive
		$this->armazena_dados = pack( "vvvvvv", 0x809, 0x08, 0x00,0x10, 0x0, 0x0 );
	}

	// End of the archive excel
	function FechaArquivo(){
		$this->armazena_dados .= pack( "vv", 0x0A, 0x00);
	}

	// it mounts conteudo
	function MontaConteudo( $excel_linha, $excel_coluna, $value){
		$tamanho = strlen( $value );
		$this->armazena_dados .= pack( "v*", 0x0204, 8 + $tamanho, $excel_linha, $excel_coluna, 0x00, $tamanho );
		$this->armazena_dados .= $value;
	}// End, mounts Col/Lin

	// It generates archive(xls)
	function GeraArquivo(){
		// It closes archive(xls)
		$this->FechaArquivo();
		header("Content-type: application/zip");
		//header("Content-type: text/x-comma-separated-values");
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header("Content-disposition: inline; filename=excel.xls");
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header("Pragma: public");
		print  ( $this->armazena_dados);
	}// fecha funcao
} # End of the classroom that generates excel

class sql2excel extends GeraExcel { 
		function sql2excel($tit, $sql, $myConn = "") 
		{
			global $db;
			$db = ($myConn != "") ? $myConn : $db;
			$this->GeraExcel();
			for ($i=0; $i<count($tit); $i++) 
			{
					$this->MontaConteudo(0,$i,$tit[$i]);
	 		}
			$qr=$db->execute($sql);
			$j=1;
			while ($reg=$qr->fetchrow())
			{		
					for ($i=0; $i<count($reg)/2; $i++) 
					{
							$this->MontaConteudo($j,$i,$reg[$i]);
					}
					$j++;
			}
			$this->GeraArquivo();
		}
}
?>