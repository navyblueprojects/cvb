<?php
	require_once($pathClassLib."/function.common.php");
	function outputImageByDimension($pathToFile, $imageFileName, $width, $height, $rootPath, $className="", $id="", $other=array()){
		$newsize = imgVirtualResize ($pathToFile, $imageFileName, $width, $height, $rootPath);
		
		$otherAttr = "";
		if(!empty($other)){
		
			foreach($other as $k=>$v){
				$otherAttr.= " ".$k."=\"".$v."\"";
			}
		
		}
		
		if($newsize != 0){
			echo '<img width="'.$newsize[0].'" height="'.$newsize[1].'"  '. (!empty($id)? 'id="'.$id.'"':'') .' class="'.$className.'" src="'.$pathToFile."/".$imageFileName.'"  '.$otherAttr.'    />';
		}
	}

	function outputCropImageByDimension($pathToFile, $imageFileName, $width, $height , $className ="", $id="", $other=array()){
		echo '
				
		<div style="
			width: '.$width.'px;
			height: '.$height.'px;
			overflow: hidden;
			background-image: url('.$pathToFile."/".$imageFileName.');
			background-repeat: no-repeat;
			background-position: center center;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
		" class="'.$className.'" id="'.$id.'">	
		</div>
				
				
		';
	}
	
	function outputImageIncludePadding($pathToFile, $imageFileName, $width, $height , $rootPath, $className ="", $id="", $other=array()){
		$newsize = imgVirtualResize ($pathToFile, $imageFileName, $width, $height, $rootPath);
		
		echo '

			<div style="display:table;width:'.$width.'px;height:'.$height.'px">
				<div style="display:table-cell;vertical-align:middle;text-align:center">';
			
		if($newsize != 0){
			
			$otherAttr = "";
			if(!empty($other)){
				
				foreach($other as $k=>$v){
					$otherAttr.= " ".$k."=\"".$v."\"";
				}
				
			}
			
			
			if(!empty($id)){
				echo '<img id="'.$id.'" width="'.$newsize[0].'" height="'.$newsize[1].'"  '. (!empty($id)? 'id="'.$id.'"':'') .' class="'.$className.'" src="'.$pathToFile."/".$imageFileName.'" '.$otherAttr.'/>';
			}else{
				echo '<img  width="'.$newsize[0].'" height="'.$newsize[1].'"  '. (!empty($id)? 'id="'.$id.'"':'') .' class="'.$className.'" src="'.$pathToFile."/".$imageFileName.'" '.$otherAttr.'/>';
			}
			
		}
		
		echo '					
				</div>
			</div>
				
		';
	}
	
