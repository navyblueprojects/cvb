<?php
if (! function_exists("fixedPostData")) {
	function fixedPostData($inVal) {
		return  htmlentities(str_replace(array("\'", '\"', "\\\\"), array("'", '"', "\\"), $inVal), ENT_QUOTES, "UTF-8");
	}
}

if (! function_exists("unhtmlspecialchars")) {
	function unhtmlspecialchars( $string ) {
	  $string = str_replace ( '&amp;', '&', $string );
	  $string = str_replace ( '&#039;', '\'', $string );
  	  $string = str_replace ( '&nbsp;', ' ', $string );
	  $string = str_replace ( '&quot;', '"', $string );
	  $string = str_replace ( '&lt;', '<', $string );
	  $string = str_replace ( '&gt;', '>', $string );  
	  return $string;
	} 
}

if (!function_exists("getFileList")){
	function getFileList($directory) {
		//global $rootPath;
		$fileArr = false;
		if ($handle = opendir($directory))
		{
			$fileArr = array();
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					$fileArr[] = array((is_dir($directory.$file) ? "directory" : "file"), $file);
				}
			}
		}
		return $fileArr;
	}
}

if (! function_exists("htmlspecialchars_decode")) {
	function htmlspecialchars_decode($string,$style=ENT_COMPAT) {
        $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS,$style));
        if($style === ENT_QUOTES) { $translation['&#039;'] = '\''; }
        return strtr($string,$translation);
    }
}

// For users prior to PHP 4.3.0 you may do this:
if (! function_exists("unhtmlentities")) {
	function unhtmlentities($string) {
		// replace numeric entities
		$string = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $string);
		$string = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $string);
		// replace literal entities
		$trans_tbl = get_html_translation_table(HTML_ENTITIES);
		$trans_tbl = array_flip($trans_tbl);
		return strtr($string, $trans_tbl);
	}
}

// reform query string + ===============================================
if (! function_exists("getQueryString")) {
	function getQueryString($exception = array()) {
		$strQuery = split("&", trim($_SERVER["QUERY_STRING"]));
		$newQueryArr = array();
		$newQueryStr = "";
		
		for($i=0; $i<count($strQuery); $i++) {
			$strQuery[$i] = split("=", $strQuery[$i], 2);		
			if (!in_array($strQuery[$i][0], $exception) && $strQuery[$i][0]!="")
				$newQueryArr[$strQuery[$i][0]] = $strQuery[$i][1];
		}
		
		foreach($newQueryArr as $eachName=>$eachValue) {
			$newQueryStr .= ($newQueryStr!="") ? "&".$eachName."=".$eachValue : $eachName."=".$eachValue;
		}
		return $newQueryStr;
	}
}
// reform query string - ===============================================

if (! function_exists("getTextAreaContent")) {
	function getTextAreaContent($str) {
		return  preg_replace("/\r\n|\n|\r/", "<br>", $str);		
	}
}

if (! function_exists("getDBContent")) {
	function getDBContent($str) {
		return str_replace(array("\'", '\"'), array("'", '"'), $str);
	}
}
	
if (! function_exists("getDBContent_edit")) {
	function getDBContent_edit($str) {
		return str_replace('"', "''", str_replace(array("\'", '\"'), array("'", '"'), $str));
	}
}

// Image virtual proportional resize function + ===============================================
if (! function_exists("imgVirtualResize")) {
	function imgVirtualResize ($path, $filename, $maxwidth, $maxheight, $documentRoot="", $isMinSize=false) {
		$changeflag = "";
		$changedvalue = "";
		$width = "";
		$height = "";
		$documentRoot = ($documentRoot == "" && array_key_exists('DOCUMENT_ROOT', $_SERVER)) ? $_SERVER['DOCUMENT_ROOT'] : $documentRoot;
		
		if($filename != "" && file_exists($documentRoot.$path.$filename)) {
			$img = @getimagesize($documentRoot.$path.$filename);
			if($img[0] <= $maxwidth && $img[1] <= $maxheight) {
				$width = $img[0];
				$height = $img[1];
			} else {
				if ($isMinSize) { 
					if ($img[0] >= $img[1] && $img[1] >= $maxheight) {
						$height = $maxheight;
						$width = $img[0] * $maxheight / $img[1];
					} elseif ($img[0] < $img[1] && $img[0] >= $maxwidth) {
						$width = $maxwidth;
						$height = $img[1] * $maxwidth / $img[0];
					} else {
						$width = $img[0];
						$height = $img[1];
					}
				} else {
					if ($img[1] >= $maxheight) {
						$height = "$maxheight";
						$width = $height * ($maxwidth / $maxheight);
						$changeflag = 1;
					}
					if ( $img[0] >= $maxwidth || $width >= $maxwidth) {
						$width = $maxwidth;
						if($changeflag){
							$changedvalue = $width/$img[0]*$img[1];
							$height = $changedvalue;		
						}
						if ($height > $maxheight) {
							$width = $maxheight/$changedvalue*$maxwidth;
							$height = $maxheight;
						}	
					} elseif($img[0] != $img[1] && $img[1] <= $maxheight) {
						$width = $img[0] / ($img[1] / $maxheight);
					}
					if($height == "" && $width != "") {
						$height = $width * $img[1] / $img[0];
					}
				}
			}
			$newsize = array(round($width), round($height));
			return $newsize;
		} else return 0;
	}
}
// Image virtual proportional resize function - ===============================================

// Image actual proportional resize function + ===============================================
if (! function_exists("imgActualResize")) {
	function imgActualResize($Dir,$Image,$NewDir,$NewImage,$MaxWidth,$MaxHeight,$Quality=90) {
	  list($ImageWidth,$ImageHeight,$TypeCode)=@getimagesize($Dir."/".$Image);
	  $ImageType=($TypeCode==1?"gif":($TypeCode==2?"jpeg":
				 ($TypeCode==3?"png":FALSE)));
	  $CreateFunction="imagecreatefrom".$ImageType;
	  $OutputFunction="image".$ImageType;
	  if ($ImageType) {
	   $Ratio=($ImageHeight/$ImageWidth);
	   $ImageSource=$CreateFunction($Dir."/".$Image);
	   if ($ImageWidth > $MaxWidth || $ImageHeight > $MaxHeight) {
		 if ($ImageWidth > $MaxWidth) {
			 $ResizedWidth=$MaxWidth;
			 $ResizedHeight=$ResizedWidth*$Ratio;
		 }
		 else {
		   $ResizedWidth=$ImageWidth;
		   $ResizedHeight=$ImageHeight;
		 }        
		 if ($ResizedHeight > $MaxHeight) {
		   $ResizedHeight=$MaxHeight;
		   $ResizedWidth=$ResizedHeight/$Ratio;
		 }      
		 
		 $ResizedImage=imagecreatetruecolor($ResizedWidth,$ResizedHeight);			
		 if ($ImageType == "gif" || $ImageType == "png") {
		    imagealphablending($ResizedImage, false);
		    imagesavealpha($ResizedImage,true);
		    $transparent = imagecolorallocatealpha($ResizedImage, 255, 255, 255, 127);
		    imagefilledrectangle($ResizedImage, 0, 0, $ResizedWidth, $ResizedHeight, $transparent);
			if($ImageType == "gif")
			imagetruecolortopalette($ResizedImage, false, 255);
		 }
		 imagecopyresampled($ResizedImage,$ImageSource,0,0,0,0,$ResizedWidth,$ResizedHeight,$ImageWidth,$ImageHeight);
	   } 
	   else {
		 $ResizedWidth=$ImageWidth;
		 $ResizedHeight=$ImageHeight;      
		 $ResizedImage=$ImageSource;
		 if ($ImageType == "gif" || $ImageType == "png") {
		 $ResizedImage=imagecreatetruecolor($ResizedWidth,$ResizedHeight);			
		    imagealphablending($ResizedImage, false);
		    imagesavealpha($ResizedImage,true);
		    $transparent = imagecolorallocatealpha($ResizedImage, 255, 255, 255, 127);
		    imagefilledrectangle($ResizedImage, 0, 0, $ResizedWidth, $ResizedHeight, $transparent);
			imagecopy($ResizedImage, $ImageSource, 0, 0, 0, 0, $ImageWidth, $ImageHeight);
			if($ImageType == "gif")
			imagetruecolortopalette($ResizedImage, false, 255);
		 }
	   } 
	   if ( $ImageType == "gif" || $ImageType == "png")
		   $OutputFunction($ResizedImage,$NewDir."/".$NewImage);
	   else
		   $OutputFunction($ResizedImage,$NewDir."/".$NewImage,$Quality);
	   return true;
	  }    
	  else
	   return false;
	}
}

// Image actual proportional resize function - ===============================================


if (! function_exists("imgActualEnlarge")) {
	function imgActualEnlarge($Dir,$Image,$NewDir,$NewImage,$MaxWidth,$MaxHeight,$Quality=90) {
	  list($ImageWidth,$ImageHeight,$TypeCode)=@getimagesize($Dir."/".$Image);
	  $ImageType=($TypeCode==1?"gif":($TypeCode==2?"jpeg":
				 ($TypeCode==3?"png":FALSE)));
	  $CreateFunction="imagecreatefrom".$ImageType;
	  $OutputFunction="image".$ImageType;
	  if ($ImageType) {
	   $Ratio=($ImageHeight/$ImageWidth);
	   $ImageSource=$CreateFunction($Dir."/".$Image);
	   if ($ImageWidth < $MaxWidth || $ImageHeight < $MaxHeight) {
		 if ($ImageWidth < $MaxWidth) {
			 $ResizedWidth=$MaxWidth;
			 $ResizedHeight=$ResizedWidth*$Ratio;
		 }
		 else {
		   $ResizedWidth=$ImageWidth;
		   $ResizedHeight=$ImageHeight;
		 }        
		 if ($ResizedHeight < $MaxHeight) {
		   $ResizedHeight=$MaxHeight;
		   $ResizedWidth=$ResizedHeight/$Ratio;
		 }      
		 
		 $ResizedImage=imagecreatetruecolor($ResizedWidth,$ResizedHeight);			
		 if ($ImageType == "gif" || $ImageType == "png") {
		    imagealphablending($ResizedImage, false);
		    imagesavealpha($ResizedImage,true);
		    $transparent = imagecolorallocatealpha($ResizedImage, 255, 255, 255, 127);
		    imagefilledrectangle($ResizedImage, 0, 0, $ResizedWidth, $ResizedHeight, $transparent);
			if($ImageType == "gif")
			imagetruecolortopalette($ResizedImage, false, 255);
		 }
		 imagecopyresampled($ResizedImage,$ImageSource,0,0,0,0,$ResizedWidth,$ResizedHeight,$ImageWidth,$ImageHeight);
	   } 
	   else {
		 $ResizedWidth=$ImageWidth;
		 $ResizedHeight=$ImageHeight;      
		 $ResizedImage=$ImageSource;
		 if ($ImageType == "gif" || $ImageType == "png") {
		 $ResizedImage=imagecreatetruecolor($ResizedWidth,$ResizedHeight);			
		    imagealphablending($ResizedImage, false);
		    imagesavealpha($ResizedImage,true);
		    $transparent = imagecolorallocatealpha($ResizedImage, 255, 255, 255, 127);
		    imagefilledrectangle($ResizedImage, 0, 0, $ResizedWidth, $ResizedHeight, $transparent);
			imagecopy($ResizedImage, $ImageSource, 0, 0, 0, 0, $ImageWidth, $ImageHeight);
			if($ImageType == "gif")
			imagetruecolortopalette($ResizedImage, false, 255);
		 }
	   } 
	   if ( $ImageType == "gif" || $ImageType == "png")
		   $OutputFunction($ResizedImage,$NewDir."/".$NewImage);
	   else
		   $OutputFunction($ResizedImage,$NewDir."/".$NewImage,$Quality);
	   return true;
	  }    
	  else
	   return false;
	}
}

// Array sorting (can be sorted by any argument) + ============================================
if (! function_exists("arraySort")) {
	function arraySort ($array, $index, $order='asc', $natsort=FALSE, $case_sensitive=FALSE) {
	  if(is_array($array) && count($array)>0) {
	   foreach(array_keys($array) as $key) $temp[$key]=$array[$key][$index];
	   if(!$natsort) ($order=='asc')? asort($temp) : arsort($temp);
	   else {
		 ($case_sensitive)? natsort($temp) : natcasesort($temp);
		 if($order!='asc') $temp=array_reverse($temp,TRUE);
	   }
	   foreach(array_keys($temp) as $key) (is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
	   return $sorted;
	  }
	  return $array;
	}
}
// Array sorting (can be sorted by any argument) - ============================================

// Get index text from content + ===================================================
if (! function_exists("printIndexContent")) {
	function printIndexContent($myContent, $strLen) {
		$thisStrLen = strlen($myContent);
		if($thisStrLen > $strLen) {
			$thisContent = wordwrap($myContent, $strLen, "@@@!!@@@");
			$thisContent = split("@@@!!@@@", $thisContent);
			$myContent = $thisContent[0] . " ...";
		}
		return $myContent;
	}
}
// Get index text from content - ===================================================

// Get index text from content  + ===================================================
if (! function_exists("printIndexContent_utf8")) {
	function printIndexContent_utf8($myContent, $strLen) {
		$thisStrLen = mb_strlen($myContent, 'UTF-8');
		if($thisStrLen > $strLen) {
			$myContent = mb_substr($myContent, 0, $strLen, 'UTF-8') . " ...";
		}
		return $myContent;
	}
}
// Get index text from content - ===================================================

// Get index text from content with Fixed row and col count+ ===================================================
if (! function_exists("printIndexContent_rowed")) {
	function printIndexContent_rowed($myContent, $rowLimit, $colLen, $linebreaker="\n") {	
		$myContent = str_replace("\r","", $myContent);
		$tmpStr = explode("\n", $myContent);
		$ret = "";
		$rowCount =0;
		
		foreach($tmpStr as $tmp) 
		{
			while ($tmp != "") 
			{				
				if ($rowCount >= $rowLimit)	break;
				$tmp2 = mb_substr($tmp, 0, $colLen, 'UTF-8');
				$tmp = mb_substr($tmp, $colLen, mb_strlen($tmp,'UTF-8'),'UTF-8');
				$ret .= ($ret=="" ? "": $linebreaker) . $tmp2;
				$rowCount++;				
			}
		}
		if ($ret != $myContent)
			return "$ret ...";
		else
			return $myContent;
	}
}
// Get index text from content with Fixed row and col count- ===================================================


// text wrap in html format + ==============================================
if (! function_exists("htmlwrap")) {
	function htmlwrap($str, $width = 100, $break = "\n", $nobreak = "") {
	
	  // Split HTML content into an array delimited by < and >
	  // The flags save the delimeters and remove empty variables
	  $content = preg_split("/([<>])/", $str, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
	
	  // Transform protected element lists into arrays
	  $nobreak = explode(" ", strtolower($nobreak));
	
	  // Variable setup
	  $intag = false;
	  $innbk = array();
	  $drain = "";
	
	  // List of characters it is "safe" to insert line-breaks at
	  // It is not necessary to add < and > as they are automatically implied
	  $lbrks = "/?!%)-}]\\\"':;&";
	
	  // Is $str a UTF8 string?
	  $utf8 = (preg_match("/^([\x09\x0A\x0D\x20-\x7E]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}|\xED[\x80-\x9F][\x80-\xBF]|\xF0[\x90-\xBF][\x80-\xBF]{2}|[\xF1-\xF3][\x80-\xBF]{3}|\xF4[\x80-\x8F][\x80-\xBF]{2})*$/", $str)) ? true : false;
	
	  while (list(, $value) = each($content)) {
		switch ($value) {
	
		  // If a < is encountered, set the "in-tag" flag
		  case "<": $intag = true; break;
	
		  // If a > is encountered, remove the flag
		  case ">": $intag = false; break;
	
		  default:
	
			// If we are currently within a tag...
			if ($intag) {
	
			  // Create a lowercase copy of this tag's contents
			  $lvalue = strtolower($value);
	
			  // If the first character is not a / then this is an opening tag
			  if ($lvalue{0} != "/") {
	
				// Collect the tag name   
				preg_match("/^(\w*?)(\s|$)/", $lvalue, $t);
	
				// If this is a protected element, activate the associated protection flag
				if (in_array($t[1], $nobreak)) array_unshift($innbk, $t[1]);
	
			  // Otherwise this is a closing tag
			  } else {
	
				// If this is a closing tag for a protected element, unset the flag
				if (in_array(substr($lvalue, 1), $nobreak)) {
				  reset($innbk);
				  while (list($key, $tag) = each($innbk)) {
					if (substr($lvalue, 1) == $tag) {
					  unset($innbk[$key]);
					  break;
					}
				  }
				  $innbk = array_values($innbk);
				}
			  }
	
			// Else if we're outside any tags...
			} else if ($value) {
	
			  // If unprotected...
			  if (!count($innbk)) {
	
				// Use the ACK (006) ASCII symbol to replace all HTML entities temporarily
				$value = str_replace("\x06", "", $value);
				preg_match_all("/&([a-z\d]{2,7}|#\d{2,5});/i", $value, $ents);
				$value = preg_replace("/&([a-z\d]{2,7}|#\d{2,5});/i", "\x06", $value);
	
				// Enter the line-break loop
				do {
				  $store = $value;
	
				  // Find the first stretch of characters over the $width limit
				  if (preg_match("/^(.*?\s)?(\S{".$width."})(?!(".preg_quote($break, "/")."|\s))(.*)$/s".(($utf8) ? "u" : ""), $value, $match)) {
	
					if (strlen($match[2])) {
					  // Determine the last "safe line-break" character within this match
					  for ($x = 0, $ledge = 0; $x < strlen($lbrks); $x++) $ledge = max($ledge, strrpos($match[2], $lbrks{$x}));
					  if (!$ledge) $ledge = strlen($match[2]) - 1;
	
					  // Insert the modified string
					  $value = $match[1].substr($match[2], 0, $ledge + 1).$break.substr($match[2], $ledge + 1).$match[4];
					}
				  }
	
				// Loop while overlimit strings are still being found
				} while ($store != $value);
	
				// Put captured HTML entities back into the string
				foreach ($ents[0] as $ent) $value = preg_replace("/\x06/", $ent, $value, 1);
			  }
			}
		}
	
		// Send the modified segment down the drain
		$drain .= $value;
	  }
	
	  // Return contents of the drain
	  return $drain;
	}
}
// text wrap in html format - ==============================================

// return whether input value in array (multi-dimension array supported) + ===================================================
if (! function_exists("in_array_multi")) {
	function in_array_multi($needle, $haystack) {
		if(!is_array($haystack))
			return $needle == $haystack;
		foreach($haystack as $value)
			if(in_array_multi($needle, $value)) 
				return true;
		return false;
	}
}
// return whether input value in array (multi-dimension array supported) - ===================================================

if (! function_exists("my_file_put_contents")) {
	function my_file_put_contents($file,$content) {
		$fp = fopen($file,"w");
		fwrite($fp,$content);
		fclose($fp);
	}
}

// ======================== upload file with setting permission + ========================
if (! function_exists("uploadfile")) {
	function uploadfile($origin, $dest, $tmp_name, $removeFlag = true) { # e.g. uploadfile($upImg['name'],'../upload',$upImg['tmp_name'])
		$origin = strtolower(basename($origin));
		$fulldest = $dest.$origin;
		$filename = $origin;
		for ($i=1; file_exists($fulldest); $i++) {
			$fileext = (strpos($origin,'.')===false?'':'.'.substr(strrchr($origin, "."), 1));
			$filename = substr($origin, 0, strlen($origin)-strlen($fileext)).'('.$i.')'.$fileext;
			$fulldest = $dest.$filename;
		}
		if($removeFlag) {
			if (move_uploaded_file($tmp_name, $fulldest)) {
				@chmod($fulldest, 0777);
				return $filename;
			}
		} else {
			if (copy($tmp_name, $fulldest)) {
				@chmod($fulldest, 0777);
				return $filename;
			}
		}
		return false;
	}
}
// ======================== upload file with setting permission - ========================

// ====================================== get file extension +
if (! function_exists("getExtension")) {
	function getExtension($filename) {
		if (($pos = mb_strrpos($filename, ".")) === FALSE)
		  return false;
		else {
		  return mb_substr($filename, mb_strrpos($filename, ".", -1)+1);
		}
	}
}
// ====================================== get file extension -

// + load image file ===========================================================================
if (! function_exists("loadImageFile")) {
	function loadImageFile($imgname) {
	
		switch (strtolower(getExtension($imgname))) {
		case "jpg":	case "jpeg":
			$im = @imagecreatefromjpeg($imgname); /* Attempt to open */
			break;
		case "gif":
			$im = @imagecreatefromgif($imgname); /* Attempt to open */
			break;
		case "png":
			$im = @imagecreatefrompng($imgname); /* Attempt to open */
			break;
		}
	
		if (!$im) { /* See if it failed */
			$im  = imagecreatetruecolor(400, 300); /* Create a black image */
			$bgc = imagecolorallocate($im, 255, 255, 255);
			$tc  = imagecolorallocate($im, 0, 0, 0);
			imagefilledrectangle($im, 0, 0, 399, 299, $bgc);
			/* Output an errmsg */
			imagestring($im, 1, 5, 5, "Error loading $imgname", $tc);
		}
		return $im;
	}
}
// - load the image ===========================================================================

// + add watermark =========================================================================
if (! function_exists("addWaterMark")) {
	function addWaterMark($imgname, $waterMarkImg, $WaterMark_x=-1, $WaterMark_y=-1, $watermarkImgOpacity=30, $WM_POS="REPEAT") {
		// Transparency, 100 for none, 0 for not visible
		$WaterMark_Transparency = $watermarkImgOpacity;
		
		// Import the images
		$WaterMark = loadImageFile($waterMarkImg);
		$Main_Image = loadImageFile($imgname);
		
		// Get the width and height of each image.
		/*$Main_Image_width = imagesx($Main_Image);
		$Main_Image_height = imagesy($Main_Image);
		$WaterMark_width = imagesx($WaterMark);
		$WaterMark_height = imagesy($WaterMark);*/
		
		list($Main_Image_width, $Main_Image_height, $Main_Image_TypeCode)=@getimagesize($imgname);
		list($WaterMark_width, $WaterMark_height, $WaterMark_TypeCode)=@getimagesize($waterMarkImg);
		
		// calculate the position of the WaterMark
		if ($WM_POS == "CENTER") {
			if ($WaterMark_x <= 0)
				$currX = ($Main_Image_width - $WaterMark_width) / 2;
			else $currX = 0;
			
			if ($WaterMark_y <= 0)
				$currY = ($Main_Image_height - $WaterMark_height) / 2;
			else $currY = 0;
			@imagecopymerge_alpha($Main_Image, $WaterMark, $currX, $currY, 0, 0, $WaterMark_width, $WaterMark_height, $WaterMark_Transparency);
		} elseif ($WM_POS == "TOPLEFT") {
			if ($WaterMark_x <= 0) $WaterMark_x = 0;
			if ($WaterMark_y <= 0) $WaterMark_y = 0;
			@imagecopymerge_alpha($Main_Image, $WaterMark, $WaterMark_x, $WaterMark_y, 0, 0, $WaterMark_width, $WaterMark_height, $WaterMark_Transparency);
		} elseif ($WM_POS == "TOPRIGHT") {
			if ($WaterMark_x <= 0) $WaterMark_x = ($Main_Image_width - $WaterMark_width);
			if ($WaterMark_y <= 0) $WaterMark_y = 0;
			@imagecopymerge_alpha($Main_Image, $WaterMark, $WaterMark_x, $WaterMark_y, 0, 0, $WaterMark_width, $WaterMark_height, $WaterMark_Transparency);
		} elseif ($WM_POS == "BOTTOMLEFT") {
			if ($WaterMark_x <= 0) $WaterMark_x = 0;
			if ($WaterMark_y <= 0) $WaterMark_y = ($Main_Image_height - $WaterMark_height);
			@imagecopymerge_alpha($Main_Image, $WaterMark, $WaterMark_x, $WaterMark_y, 0, 0, $WaterMark_width, $WaterMark_height, $WaterMark_Transparency);
		} elseif ($WM_POS == "BOTTOMRIGHT") {
			if ($WaterMark_x <= 0) $WaterMark_x = ($Main_Image_width - $WaterMark_width);
			if ($WaterMark_y <= 0) $WaterMark_y = ($Main_Image_height - $WaterMark_height);				
			@imagecopymerge_alpha($Main_Image, $WaterMark, $WaterMark_x, $WaterMark_y, 0, 0, $WaterMark_width, $WaterMark_height, $WaterMark_Transparency);
		} elseif ($WM_POS == "REPEAT") {
			$currY = 0;
			while($currY <= $Main_Image_height) {
				$currX = 0;
				while($currX <= $Main_Image_width) {
					@imagecopymerge_alpha($Main_Image, $WaterMark, $currX, $currY, 0, 0, $WaterMark_width, $WaterMark_height, $WaterMark_Transparency);
					$currX += $WaterMark_width;
				}	
				$currY += $WaterMark_height;
			}
		}
		
		// show the image in png format(sharper image)	
		switch (strtolower(getExtension($imgname))) {
		case "jpg":	case "jpeg":
			imagejpeg ($Main_Image, $imgname);
			break;
		case "gif":
			imagegif ($Main_Image, $imgname);
			break;
		case "png":
			imagesavealpha($img_a, true);
			imagepng ($Main_Image, $imgname);
			break;
		}
	}
}
// - add watermark =========================================================================

if (! function_exists("imagecopymerge_alpha")) { // same to imagecopymerge but with alpha support
	function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){ 
		if(!isset($pct)){ 
			return false; 
		} 
		$pct /= 100; 
		// Get image width and height 
		$w = imagesx( $src_im ); 
		$h = imagesy( $src_im ); 
		// Turn alpha blending off 
		imagealphablending( $src_im, false ); 
		// Find the most opaque pixel in the image (the one with the smallest alpha value) 
		$minalpha = 127; 
		for( $x = 0; $x < $w; $x++ ) 
		for( $y = 0; $y < $h; $y++ ){ 
			$alpha = ( imagecolorat( $src_im, $x, $y ) >> 24 ) & 0xFF; 
			if( $alpha < $minalpha ){ 
				$minalpha = $alpha; 
			} 
		} 
		//loop through image pixels and modify alpha for each 
		for( $x = 0; $x < $w; $x++ ){ 
			for( $y = 0; $y < $h; $y++ ){ 
				//get current alpha value (represents the TANSPARENCY!) 
				$colorxy = imagecolorat( $src_im, $x, $y ); 
				$alpha = ( $colorxy >> 24 ) & 0xFF; 
				//calculate new alpha 
				if( $minalpha !== 127 ){ 
					$alpha = 127 + 127 * $pct * ( $alpha - 127 ) / ( 127 - $minalpha ); 
				} else { 
					$alpha += 127 * $pct; 
				} 
				//get the color index with new alpha 
				$alphacolorxy = imagecolorallocatealpha( $src_im, ( $colorxy >> 16 ) & 0xFF, ( $colorxy >> 8 ) & 0xFF, $colorxy & 0xFF, $alpha ); 
				//set pixel with the new color + opacity 
				if( !imagesetpixel( $src_im, $x, $y, $alphacolorxy ) ){ 
					return false; 
				} 
			} 
		} 
		// The image copy 
		imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h); 
	}
}
?>