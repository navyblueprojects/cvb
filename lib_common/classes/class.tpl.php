<?php
class classTpl {

	var $tplcontent; # template file content
	var $strip = true; # var boolen remove template {VAR} variable
	
	function classTpl($template) {
		if (!$this->tplcontent = file_get_contents($template))
			echo "Error: template file: $template Not Found.<br/>";		
	}
		
	/**
	 * function to set remove {VAR}  
	 */
	function isStrip($bool=true) {
		$this->strip = $bool;
	}
	
	function fetch($file) {
		ob_start();                    // Start output buffering
        include($file);                // Include the file
        $contents = ob_get_contents(); // Get the contents of the buffer
        ob_end_clean();
        return $contents;
	}
	
	/**
	 * Function to replace template variable 
	 */
	function setVar($tplname,$tplvalue) {
		$this->tplcontent = str_replace("{".$tplname."}",$tplvalue,$this->tplcontent);
	}
	
	/**
	 * Function to return result
	 */	
	function toHTML() {
		if($this->strip)
			$this->_strips();
		return $this->tplcontent;
	}
	
	/**
	 * function to remove not replaced TEMPLATE variable {VAR} 
	 */
	function _strips() {
		$this->tplcontent = preg_replace("/\{.+\}/","",$this->tplcontent);
		//$this->tplcontent = preg_replace("/\{([^}]+)\}/","",$this->tplcontent);
	}
}
?>
