<?
define("DGU_LCASE", 1);
define("DGU_UCASE", 2);
define("DGU_NUM", 4);
define("DGU_SPACE", 8);
define("DGU_RETURN", 16);
define("DGU_SYMBOL", 32);

class TextGenerator {
	
	var $lcaseArray = array();
	var $ucaseArray = array();
	var $numArray = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	var $symbolArray = array('~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '-', 
					'=', '{', '}', '[', ']', ':', ';', '"', '\'', '|', '\\', '<', '>', '?', ',', '.', '/');
	
	function TextGenerator() {
	
		for($i = 97 ; $i <= 122 ; $i++)	$this->lcaseArray[] = chr($i);
		for($i = 65 ; $i <= 90 ; $i++)	$this->ucaseArray[] = chr($i);
	}
	
	// third argument is toggle for options
	function generateRandomText($minLen, $maxLen) {
		$options = DGU_LCASE | DGU_UCASE | DGU_NUM | DGU_SPACE ;
		
		if (func_num_args() >= 3) {
			$options = func_get_arg(2) ^ $options;
		}
		
		$char = array();
		
	
		if ( (int)(DGU_LCASE & $options) == DGU_LCASE)	$char = array_merge($char, $this->lcaseArray); 
		if ( (int)(DGU_UCASE & $options) == DGU_UCASE)	$char = array_merge($char, $this->ucaseArray);
		if ( (int)(DGU_NUM & $options) == DGU_NUM)		$char = array_merge($char, $this->numArray);
		if ( (int)(DGU_SPACE & $options) == DGU_SPACE)	$char = array_merge($char, array(" "));
		if ( (int)(DGU_RETURN & $options) == DGU_RETURN)	$char = array_merge($char, array("\n"));
		if ( (int)(DGU_SYMBOL & $options) == DGU_SYMBOL)	$char = array_merge($char, $this->symbolArray);
		
		$str = "";
		$len = rand($minLen, $maxLen);
		for($i = 0 ; $i < $len; $i++) {
			$index = rand(0, sizeof($char)-1);
			$str .= $char[$index];
		}
		
		return $str;
	}
}

?>
