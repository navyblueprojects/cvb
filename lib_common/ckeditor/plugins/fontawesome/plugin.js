CKEDITOR.plugins.add( 'fontawesome', {
    init: function( editor ) {

        editor.addCommand( 'insertFAIcon', {
            exec: function( editor ) {
                editor.insertHtml( '<span class="fa fa-facebook"></span>' );
            }
        });
        editor.ui.addButton( 'FontAwesome', {
		    label: 'Insert Font Awesome icon',
		    command: 'insertFAIcon',
		    toolbar: 'insert,100',
            icon: this.path+'icons/fontawesome.png'
		});
        
    }
});

