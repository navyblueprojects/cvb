/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'en';
	// config.skin = 'moono';
	config.uiColor = '#EEEEEE';
	
	config.toolbar = 'Advanced';
 	config.width = 580;
	config.height = 200;
	config.toolbar_Full =
	[
		{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
			'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
	];
	
		
	config.toolbar_Standard = [
		{ name: 'document', items : [ 'Source'] },
		{ name: 'basicstyles', items : ['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyFull']},
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'insert', items : [ 'Image','Table', 'Smiley'] }
	];
	
	config.toolbar_Advanced = [
		{ name: 'document', items : [ 'Source'] },
		{ name: 'basicstyles', items : ['Bold','Italic','Underline','-','Subscript','Superscript'] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyFull']},
		{ name: 'links', items : [ 'Link','Unlink'] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'insert', items : [ 'Image','Table','Smiley'] },
	];
	
	config.toolbar_Mailing = [
		{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord' ] },
		{ name: 'editing', items : ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat' ] },
		'/',
		{ name: 'basicstyles', items : ['Bold','Italic','Underline','-','Subscript','Superscript'] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyFull']},
		{ name: 'links', items : [ 'Link','Unlink'] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'insert', items : [ 'Image','Table', 'Smiley'] }
	];
	config.toolbar_Basic =
	[
		['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About']
	];
	config.toolbar_Forum =
	[
		['Bold', 'Italic','Underline', '-','TextColor','BGColor','-','NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About'],'/',
		['Format','Font','FontSize']
	];

	config.extraPlugins = 'fontawesome';
	/*
	FCKConfig.ToolbarSets["Forum"] = [	
		['Bold','Italic','Underline','-','TextColor','BGColor', '-', 'OrderedList','UnorderedList','-','Link','Unlink','-','About'],
		['FontFormat','FontName','FontSize']
	];*/
	//config.filebrowserFlashUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';//可上傳Flash檔案
};
