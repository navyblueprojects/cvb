<?php
	require_once("config.inc.php");
	require_once("classes/student.php");

	$member = Student::retain();

	if (!$member->isLogined()){
		header("location: ./");
		exit();
	}

	include_once("header.php");
	
	$pageName = "Home";
	include_once("breadcrumb.php");
	$app_id = $member->getApplication();
?>
	<main id="home">
		<div class="wrapper">
			<div class="container">
				<h1 class="page_header">Home </h1>
				<section id="options_wrapper" class="col-sm-12" >
					<?php
						$m = $cmsObj->getPageArea("home_alert");
						if ($m) {
					?><div class="alert alert-info alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <?=$cmsObj->getPageArea("home_alert")?>
					</div><?
						}
					?>
					<div class="row" id="options_list">
						<div class="col-sm-6">
							<ul>
								<li><a href="new_cv"><img src="images/icon_01.png" alt="Create / Edit CV">Create / Edit CV </a></li>
								<li><a href="my_cv"><img src="images/icon_03.png" alt="Preview my CVs">Preview My CVs</a></li>
							</ul>
						</div>
						<?php
						if ($member->isWIE()) {
						?><div class="col-sm-6">
							<ul>
								<li><a href="joblist"><img src="images/icon_04.png" alt="WIE Application">WIE Application</a></li>
								<li><a href="app_form-<?=uniqid(sha1("application-".$app_id["id"]))?>-<?=base64_encode(sha1("application-".$app_id["id"]))?>" target="_blank"><img src="images/icon_05.png"  alt="Preview My WIE Application Form">Preview My WIE Application Form</a></li>
								<li><a href="job_app_history"><img src="images/icon_06.png" alt="WIE Application History">WIE Application History</a></li>
							</ul>
						</div><?php
						}
						?>
					</div>

				</section>
				
			</div>
		</div>
	</main>
<?php
	include_once("footer.php");
?>