<?php

	require_once("../config.inc.php");
	require_once("../classes/application.php");

	$lecturer = Lecturer::retain();
	if (!$lecturer->isLogined()) {
		header('HTTP/1.0 403 Forbidden');
		exit();
	}

	if (array_key_exists("exportExcel", $_GET)) {

		$application = new Application();
		$objPHPExcel = $application->exportXls($_POST["app"]);
		
		$session = date("Ymd_His");

		$path = __DIR__ . '/../dataexport/'.$session.'.xlsx';

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($path);
		
		$path = __DIR__ . "/../dataexport";

		$cmd = "7za a -P%s %s/%s.zip %s/*";
	    $cmd = sprintf($cmd, $_POST["password"], $path, $session,  "../dataexport");
    	
	    exec($cmd);

	    @unlink($path . '/'.$session.'.xlsx');
	    
	    $fname = $path . '/'.$session.'.zip';

	    header("Content-Type: application/zip, application/octet-stream; name=\"wie_application_data_".$session.".zip\"");
		header("Content-Disposition: inline; filename=\"wie_application_data_".$session.".zip\"");
		header('Pragma: public');
		header('Content-Length: '.filesize($fname));
		
		$fh=fopen($fname, "rb");

		fpassthru($fh);
		unlink($fname);	
		
	} else {
		
		$application = new Application();
		$fname = $application->downloadZip($_POST["app"], $_POST["password"]);		
		
		header("Content-Type: application/zip, application/octet-stream; name=\"wie_student_cv_".date("Ymd_His").".zip\"");
		header("Content-Disposition: inline; filename=\"wie_student_cv_".date("Ymd_His").".zip\"");
		header('Pragma: public');
		header('Content-Length: '.filesize($fname));
		
		$fh=fopen($fname, "rb");
		fpassthru($fh);
		unlink($fname);	
	}
?>