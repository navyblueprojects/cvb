<?php
	ob_start();
	require_once("../config.inc.php");

	include_once("header.php");
	
	if ($member->isLogined()){
		header("location: home");
		exit();
	}	
	if ($member->check_saml_login()){
		header("location: home");
		exit();
	} else {
		$member->start_login();
		exit();
	}	


	if ($member->isLogined()){
		header("location: home");
		exit();
	}

	
?>
	<main id="wie_login">
		<div class="wrapper">
			<div class="container">
				<h1 class="page_header">Lecturer Login</h1>
				<div >
					<section id="loginform_wrapper" class="col-md-6 ">
						<div  class="row">
							<data data-cms-title="tutor-login_text"><?=$cmsObj->getPageArea("tutor-login_text")?></data>
							<form id="loginform" class="form col-md-11 ">
								<div class="form-group">
									<label for="username">Username</label>
									<input type="text" id="username" name="username" placeholder="username" class="form-control" autocomplete="off" value="" />
								</div>
								<div class="form-group">
									<label for="password">Password</label>
									<input type="password" id="password" name="password" placeholder="password" class="form-control" autocomplete="off"  value="" />
								</div>
	                            <div class="form-group">
									<label for="captcha">CAPTCHA</label>
									<div class="row">
										<div class="col-md-4"><input type="text" maxlength="4" id="captcha" name="captcha" class="form-control "  autocomplete="off" /></div>
										<div class="col-md-4"><img src="./captcha" alt="captcha" style="border: 1px solid grey" id="img_captcha"/> <a id="reloadCaptcha" ><i class="fa fa-redo" ></i></a></div>

									</div>
								</div>
								<div class="form-group">
									<input class="btn btn-primary" type="submit" value="Login"/>
								</div>
							</form>
						</div>
					</section>
					<section id="announce_wrapper" class="col-md-6  ">
						<div class="announce_card">
							<h2>Message for WIE</h2>
							<data data-cms-title="message_for_WIE"><?=$cmsObj->getPageArea("message_for_WIE")?></data>
						</div>
					</section>
				</div>	
				<div class="col-md-12">
					<label>System Time: <span id="curr_time">&nbsp;</span></label>
				</div>
			</div>
            <div class="hide alert alert-danger alert-dismissible alertLogin" role="alert" >
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <span class="errorMsg"></span>
			</div>
		</div>
		<script>
			$(function(){
				setInterval(function(){
					d = new Date();
					$('#curr_time').html(d.toLocaleDateString() + " " + d.toLocaleTimeString());

				}, 1000)
			})
		</script>
	</main>
<?php
	include_once("footer.php");
	ob_end_flush();
?>