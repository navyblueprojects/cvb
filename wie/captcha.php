<?php
	session_start();
	/* Create Imagick object */
	$Imagick = new Imagick();
	/* Create the ImagickPixel object (used to set the background color on image) */
	$bg = new ImagickPixel();
	/* Set the pixel color to white */
	$bg->setColor( 'white' );
	/* Create a drawing object and set the font size */
	$ImagickDraw = new ImagickDraw();
	/* Set font and font size. You can also specify /path/to/font.ttf */
	//$ImagickDraw->setFont( 'Helvetica Regular' );
	$ImagickDraw->setFontSize( 20 );
	
	/* Create the text */
	$alphanum = 'ACDEFGHJKLMNPQRTVWX123456789';
	$string = substr( str_shuffle( $alphanum ), 2, 4 );
	$_SESSION["lctr_login_captcha"] = md5($string);
	/* Create new empty image */
	$Imagick->newImage( 85, 30, $bg ); 
	
	/* Write the text on the image */
	$x = rand(4, 20);
	$y = rand(20, 30);
	$Imagick->annotateImage( $ImagickDraw, $x, $y, 0, $string );
		
	if ($y % 4 == 0) {
		$Imagick->swirlImage( 33);
	} else if ($y % 4 == 1) {
		$Imagick->waveImage(5, 85);
		$Imagick->resizeImage(85, 30, imagick::FILTER_BOX, 1);
		
	} else if ($y % 4 == 2) {
		$Imagick->implodeImage(0.2);
	} else if ($y % 4 == 3) {
		$Imagick->implodeImage(-0.4);
	}

	if ($x % 3 == 0) {
		$Imagick->addNoiseImage(imagick::NOISE_MULTIPLICATIVEGAUSSIAN, imagick::CHANNEL_RED);
	} else if ($x % 3 == 1) {
		$Imagick->addNoiseImage(imagick::NOISE_MULTIPLICATIVEGAUSSIAN, imagick::CHANNEL_MAGENTA);
	} else if ($x % 3 == 2) {
		$Imagick->addNoiseImage(imagick::NOISE_MULTIPLICATIVEGAUSSIAN, imagick::CHANNEL_YELLOW);
	} 

	/* Draw the ImagickDraw object contents to the image. */
	$Imagick->drawImage( $ImagickDraw );
	/* Give the image a format */
	$Imagick->setImageFormat( 'png' );
	/* Send headers and output the image */
	header( "Content-Type: image/{$Imagick->getImageFormat()}" );
	echo $Imagick->getImageBlob( );

	session_write_close();
?>