<?php
	require_once("../config.inc.php");
	require_once("../classes/lecturer.php");
	
	$login = new Lecturer();

	if ($login->isLogined()) {
		header("location: home");
	} else {
		header("location: login");
	}
	exit();
?>