<?php
	require_once("../config.inc.php");
	require_once("../classes/application.php");

	session_start();

	include_once("header.php");
	if (!$member->isLogined()){
		header("location: login");
		exit();
	}
	$pageName = array("Reports"=>"");
	$application = new Application();
	$mywhere = array();
	$mywhere["where"][] = "(app.app_no <> '')";

	$search = array_key_exists("search", $_SESSION) ? $_SESSION["search"] : array();
	if (array_key_exists("search", $_POST)) {
		$search = array_key_exists("search", $_POST) ? $_POST["search"] : $search;
	}
	$_SESSION["search"] = $search;

	if ($search) {
		foreach ($search as $key => $value) {
			switch ($key) {
				case 'jobs.job_no':
					$mjob_no = array();
					foreach ($value as $nnn) {
						$mjob_no[] = "jobs.job_no like '%" . $nnn . "%'";
					}
					$mywhere["where"][] = "(".implode(" OR " , $mjob_no).")";
					break;
				case 'jobs.company':
					if ($value != "") {
						$mywhere["where"][] = "jobs.company like '%" . $value . "%'";
					}
					break;
				case 'jobs.name':
					if ($value != "") {
						$mywhere["where"][] = "jobs.name like '%" . $value . "%'";
					}
					break;
				case 'std.prog_code':
					$prog_code = array();
					foreach ($value as $vv) {
						$prog_code[] = $cmsObj->getDetail("program", $vv);
					}
					$search["std.prog_code"] = $prog_code;
					$mywhere["where"][] = "std.prog_code in (".implode("," ,$value).")";

					break;
				case 'std.yrofstudy':
					$mywhere["where"][] = "std.yrofstudy in (".implode("," ,$value).")";
					break;
				case 'jobs.application':
					list($open_date, $close_date) = explode(" - ", $value, 2);
					if ($value) {
						$mywhere["where"][] = "jobs.open_date <= '$open_date' AND jobs.close_date >= '$close_date'";
					}
					break;
				default:
					# code...
					break;
			}
		}
	}

	$sort = array_key_exists("sort", $_SESSION) ? $_SESSION["sort"] : "app.createDate";
	$sort = array_key_exists("sort", $_GET) ? $_GET["sort"] : $sort;
	$_SESSION["sort"] = $sort;

	$sd = array_key_exists("sd", $_SESSION) ? $_SESSION["sd"] : 0;
	$sd = array_key_exists("asc", $_GET) ? $_GET["asc"] : $sd;
	$_SESSION["sd"] = $sd;

	$mywhere["order"][] = $sort  . ($sd ? " ASC " : " DESC ");

	$itmlist = $application->getItemList($mywhere);
	
	$fields = array(
		"jobs.job_no"=>"WIE No.",
		//"jobs.company"=>"Company Name",
		//"jobs.name"=>"Position",
		"std.student_id"=>"Student No.",
		"app.firstname_en"=>"English Name",
		"app.lastname_zh"=>"Chinese Name",
		"app.contact"=>"Contact",
		"std.prog_code"=>"Programme Code",
		"item.status"=>"Status",
		"app.submission_date"=>"Submission Date", 
		"app.job_selected"=>"No. of Applied Job"
	);
	
	$jobs = array();
	foreach ($itmlist as $item) {
		
		foreach ($item["items"] as $job) {
			$jobs[$job["job"]["id"]] = $job["job"]["job_no"];
		}
	}
	session_write_close();

?><main id="wie-report">
		<?php include_once("breadcrumb.php")?>
		<div class="wrapper">
			<h1 class="page_header">Reports</h1>
			<section id="jobs_panel">
				<form>
					<div class="row">
						<div class="col-md-2">
							<label >Job No.</label>
						</div>
						<div class="col-md-3"><select class="select2 form-control" multiple="true"	data-name="jobs.job_no">
							<?php
							foreach ($search["jobs.job_no"] as  $value) {
								?><option selected="selected"><?=$value?></option><?
							}
							?>
						</select></div>
						<div class="col-md-1"><label >Programme</label></div>
						<div class="col-md-3"><select class="select2 form-control" multiple="true" data-field="std.prog_code" data-name="std.prog_code"><?php
						foreach ($search["std.prog_code"] as $prog_code) {
						?><option selected="selected" value="<?=$prog_code["id"]?>"><?=$prog_code["code"]?> <?=$prog_code["name"]?></option><?php
						}
						?></select></div>
						<div class="col-md-1"><label>Year</label></div>
						<div class="col-md-2"><select class="select2 form-control" multiple="true" data-name="std.yrofstudy" ><?php
						foreach ($search["std.yrofstudy"] as $prog_code) {
						?><option selected="selected" value="<?=$prog_code?>"><?="Year " . $prog_code?></option><?php
						}
						?></select></div>
					</div>
					<div class="row">
						<label class="col-md-2">Company Name</label>
						<div class="col-md-3"><input class=" form-control"  data-field="jobs.company" data-name="jobs.company" value="<?=$search["jobs.company"]?>" /></div>
						<label class="col-md-1">Position</label>
						<div class="col-md-3"><input class=" form-control"  data-field="jobs.name"  data-name="jobs.name" value="<?=$search["jobs.name"]?>"/></div>
					</div>
					<div class="row">
						<div class="col-md-2"><label>Application Period</label></div>
						<div class="col-md-7"><input type="text" class="form-control date-range" data-name="jobs.application" value="<?=$search["jobs.application"]?>"/></div>
						<div class="col-md-2"><a class="btn btn-default" id="applyfilter">Apply</a></div>
					
					</div>
				</form>
			</section>
			<section id="jobs_list">
				<div  class="row">
					<div class="col-md-12" style="padding: 1em">
						<div class="btn-group" role="group">
							<?php
								if (in_array("APPLICATION_EXPORT_EXCEL", $member->func_permission)) {
									?><a id="exportExcel" data-toggle="modal" data-target="#zipPassword" class="btn btn-default" type="button"><i class="fa fa-file-excel"></i> Export Excel</a><?
								}
							?>
							<a id="downloadCV" data-toggle="modal" data-target="#zipPassword" class="btn btn-default" type="button"><i class="fa fa-file-pdf"></i> Download Application Form</a>
						</div>
					</div>
				</div><div ><form id="download" method="post" target="_blank">
					<div class="modal fade" tabindex="-1" role="dialog" id="zipPassword" aria-labelledby="lblZipPassword">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="lblModalExActivity">Set Password:</h4>
								</div>
								<div class="modal-body">

									<div class="form-group row">
										<label for="lblZipPassword" class="col-md-4">Password: </label>
										<div class="col-md-6"><input type="password" name="password" id="password" class="form-control "/></div>
									</div>
									<div class="form-group row text-danger" id="errorModalDownload">
										<label  class="col-md-4"></label>
										<div class="col-md-6"><label>Your password must have:<ul>
											<li> 8 - 16 characters</li>
											<li> Upper and lowercase letter</li>
											<li> At least one number</li>
										</ul></label></div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-default" >Download ZIP</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><input type="checkbox" title="Select All" id="selectall"/></th>
									<?php
										foreach ($fields as $f=>$fname) { 
									?><th style="white-space: nowrap;"><a href="reports?sort=<?=$f?>&asc=<?=($sort == $f && $sd == "1") ? "0" : "1" ?>" ><?=$fname?>
											<?php
												if ($sort == $f) {
											?><i class="fa fa-arrow-<?=($sd == "1") ? "down" : "up"?>"></i><?
												}
											?></a></th><?
										}
										foreach ($jobs as $key => $value) {
											?><th><?=$value?></th><?
										}
									?>
									
								</tr>
							</thead>
							<tbody>
								<?php
									foreach ($itmlist as &$item) {
								?><tr>
									<td><input type="checkbox" value="<?=$item["application"]["id"]?>" name="app[]"/></td>
									<td><?=$item["application"]["app_no"]?></td>
									<td><?=$item["student"]["student_id"]?></td>
									<td><?=$item["application"]["firstname_en"] . " " . $item["application"]["lastname_en"]?></td>
									<td><?=$item["application"]["lastname_zh"] . $item["application"]["firstname_zh"]  ?></td>
									<td><?=$item["application"]["contact"]?></td>
									<td><?=$item["programme"]["code"] ?></td>
									<td><?=$item["application"]["status"]?></td>
									<td><?=$item["application"]["submission_date"]?></td>
									<td><?=$item["application"]["job_selected"]?></td>
									<?php
										foreach ($jobs as $key => $value) {
											?><td><?
											if (array_key_exists($key, $item["items"])) {
												echo $item["items"][$key]["priority"];
											}
											?></td><?
										}
									?>
								</tr><?php
										
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</section>
			<script src="../scripts/reports.js"></script>
			<link href="/css/select2.min.css" rel="stylesheet" />
			<script src="/js/select2.min.js"></script>
			
		</div>
	</main>
<?php
	include_once("footer.php");
?>