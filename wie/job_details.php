<?php
	require_once("../config.inc.php");
	include_once("../classes/jobs.php");

	include_once("header.php");
	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}
	$pageName = array("Job"=>"");

	$job = new Jobs();
	$job->getJobByJobNo($_GET["id"]);

	$job = $job->getDetails();

	$permission = $member->getPermission($job["id"]);
	
?><main id="jobdetails">
		<?php include_once("breadcrumb.php")?>
		<div class="wrapper">
			
			<section id="jobdetails_left" class="col-md-2">
				<data data-cms-title="jobdetails_left"><?=$cmsObj->getPageArea("jobdetails_left")?></data>
			</section>
			<section id="jobdetails_wrapper" class="col-md-10">
				<div class="row">
					<div class="text-right">
						<?php
							if ($permission["CREATE"] == 1 && in_array("JOB_DUPLICATE", $member->func_permission) ) {
						?><a  class="btn btn-default btn-duplicate" data-id="<?=$job["id"]?>">Duplicate</a> <?php
							}
							if ($permission["DELETE"] == 1 &&  in_array("JOB_DELETE", $member->func_permission) ) {
						?><a class="btn btn-default btn-delete" data-id="<?=$job["id"]?>">Delete</a> <?php
							}
							if ($permission["EDIT"] == 1 && in_array("JOB_EDIT", $member->func_permission) ) {
						?><a href="job-edit/<?=$job["job_no"]?>-<?=str_replace("-", "", urlencode($job["name"]."@".$job["dept"]."@".$job["company"])) ?>" class="btn btn-default">Edit</a> <?php
							}
						?>
						<a href="jobs.php" class="btn btn-default">Back</a>
					</div>
				</div>
				<h2><?=$job["dept"]?></h2>
					<ul id="details"><?php
						if ($job["job_no"] != "") {
							?><li class="row"><label class="col-md-2">Job No.</label> <span class="col-md-10"><?=$job["job_no"]?></span></li><?php
						}
						if ($job["name"] != "") {						
							?><li class="row"><label class="col-md-2">Position</label><span class="col-md-10"><?=$job["name"]?></span></li><?php
						}
						if ($job["company"] != "") {
						?><li class="row"><label class="col-md-2">Company</label><span class="col-md-10"><?=$job["company"]?></span></li><?php
						}
						if ($job["dept"] != "") {
						?><li class="row"><label class="col-md-2">Department </label><span class="col-md-10"><?=$job["dept"]?></span></li><?php
						}
						if ($job["vacancy"] != "") {
						?><li class="row"><label class="col-md-2">No. of Vacancy</label><span class="col-md-10"><?=$job["vacancy"]?></span></li><?php
						}
						if ($job["post"] != "") {
						?><li class="row"><label class="col-md-2">Type</label><span class="col-md-10"><?=$job["post"]?></span></li><?php
						}
						if ($job["duty"] != "") {
						?><li class="row"><label class="col-md-2">Duty</label><span class="col-md-10"><?=nl2br($job["duty"])?></span></li><?php
						}
						if ($job["period"] != "") {
						?><li class="row"><label class="col-md-2">Job Period</label><span class="col-md-10"><?=$job["period"]?></span></li><?php
						}
						if ($job["salary"] != "") {
						?><li class="row"><label class="col-md-2">Salary / Allowance</label><span class="col-md-10"><?=$job["salary"]?></span></li><?php
						}
						if ($job["work_hour"] != "") {
						?><li class="row"><label class="col-md-2">Working Hours</label><span class="col-md-10"><?=$job["work_hour"]?></span></li><?php
						}
						if ($job["work_day"] != "") {
						?><li class="row"><label class="col-md-2">Working Days</label><span class="col-md-10"><?=$job["work_day"]?></span></li><?php
						}
						if ($job["open_date"] != "") {
						?><li class="row"><label class="col-md-2">Application Start Date</label><span class="col-md-10"><?=date("Y-m-d H:i", strtotime($job["open_date"]))?></span></li><?php
						}
						if ($job["close_date"] != "") {
						?><li class="row"><label class="col-md-2">Application End Date</label><span class="col-md-10"><?=date("Y-m-d H:i", strtotime($job["close_date"]))?></span></li><?php
						}
						if ($job["remarks"] != "") {
						?><li class="row"><label class="col-md-2">Remarks</label><span class="col-md-10"><?=$job["remarks"]?></span></li><?php
						}
						if ($job["attachment"] != "") {
						?><li class="row"><label class="col-md-2">Attachment</label><span class="col-md-10"><a href="<?=UFILE_PATH . $job["attachment"] ?>" target="_blank"><?=$job["attachment"]?></a></span></li>
						<?php 
						}
						if ($job["attachment02"] != "") {
						?><li class="row"><label class="col-md-2">Attachment 2</label><span class="col-md-10"><a href="<?=UFILE_PATH . $job["attachment02"] ?>" target="_blank"><?=$job["attachment02"]?></a></span></li><?php
						}
						if ($job["attachment03"] != "") {
						
						?>
						<li class="row"><label class="col-md-2">Attachment 3</label><span class="col-md-10"><a href="<?=UFILE_PATH . $job["attachment03"] ?>" target="_blank"><?=$job["attachment03"]?></a></span></li><?php
						}
						?>
					</ul>
			</section>
		</div>
		<div class="modal fade" tabindex="-1" role="dialog" id="errorModal">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Error</h4>
		      </div>
		      <div class="modal-body">
		        <h3 id="errMsg"></h3>
		      </div>
		      <div class="modal-footer">
		        <a  class="btn btn-default" data-dismiss="modal">Close</a>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</main>
<?php
	include_once("footer.php");
?>