<?php
	ob_start();

	require_once("../config.inc.php");
	include_once("../classes/lecturer.php");

	$member = new Lecturer();

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	} else {
		$logger = new Syslog();
        $logger->log("login", "lecturer", ["member_id"=> $member->getId(), "para"=>"SUCCESS"], "SUCCESS");
        header("location: home");
        exit();
	}

?>