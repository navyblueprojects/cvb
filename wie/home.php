<?php
	require_once("../config.inc.php");

	include_once("header.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}
?>
	<main id="wie-home">
		<div >
			<?php include_once("breadcrumb.php")?>
			
			<section id="home_menu" class="col-md-12">
				
				<div class="wrapper">
					<h1 >Welcome to WIE System</h1>
					<p>Please select the operation below. </p>
					<ul>
						<li class="container col-md-4" onclick="window.location='reports.php'">
							<div><i class="fa fa-tasks fa-8x"></i></div>
							<a class="dropdown">Reports </a>
						</li>
						<li class="container col-md-4" >
							<div><i class="fa fa-book fa-8x"></i></div>
							<div class="dropdown">
								<a class="dropdown-toggle" id="jobdown" data-toggle="dropdown">Manage Jobs <span class="caret"></span></a>
								<ul class="dropdown-menu" aria-labelledby="jobdown">
									<?php
									if (in_array("JOB_CREATE", $member->func_permission)) {
									?><li><a href="job_new.php">Create New Jobs</a></li><?php
									}
									?>
									<li><a href="jobs.php">Jobs List</a></li>
								</ul>
							</div>
						</li>
						<li class="container col-md-4" onclick="window.location='applications.php'">
							<div><i class="fa fa-users fa-8x"></i></div>
							<a>Manage Applications</a>
						</li>
					</ul>
				</div>
			</section>
			
		</div>
	</main>
<?php
	include_once("footer.php");
?>