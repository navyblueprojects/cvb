<?php
	require_once("../config.inc.php");

	include_once("header.php");
	include_once("../classes/system.php");
	

	if (!$member->isLogined()){
		header("location: login");
		exit();
	}
	$pageName = array("Job"=>"");

	$keywords = array("job_no",  "company", "position", "application");
	$mywhere = array();
	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	
	foreach ($keywords as $kk) {
		$$kk = array_key_exists("job_search".$kk, $_SESSION) ? $_SESSION["job_search".$kk] : "";
		$$kk = array_key_exists($kk, $_POST) ? $_POST[$kk] : $$kk;
		$$kk = array_key_exists($kk, $_GET) ? $_GET[$kk] : $$kk;

		$_SESSION["job_search".$kk] = $$kk;

	} 

	$programme_id = array_key_exists("programme_id", $_POST) ? $_POST["programme_id"] : "";
	$programme_id = array_key_exists("programme_id", $_GET) ? $_GET["programme_id"] : $programme_id;


	if ($job_no != "") {
		$mywhere["where"][] = "job_no like '%".$job_no."%'";
	}

	if ($programme_id != "") {

		$mywhere["programme_id"] = $programme_id;
		$pp = new Program();
		$programme = $pp->getListByIds($programme_id);

	}
	if ($company != "") {
		$mywhere["where"][] = "(company like '%".$company."%' OR dept like '%".$company."%')";
	}
	if ($position != "") {
		$mywhere["where"][] = "name like '%".$position."%'";
	}
	if ($application != "") {
		list($startdate, $enddate) = explode(" - ", $application, 2);

		$startdate = date("Y-m-d 00:00:00", strtotime($startdate));
		$enddate = date("Y-m-d 23:59:59", strtotime($enddate));


		$mywhere["where"][] = " NOT(open_date >= '".$enddate."' OR close_date <= '".$startdate."')";
	}

	
	$mywhere["order"][] = " id desc";

	$limit = 10;

	$joblist = $member->getJobList(($page -1) * $limit, 9999, $mywhere);


?><main id="wie-jobs">
		<?php include_once("breadcrumb.php")?>
		<div  class="wrapper"><?php
			if (in_array("JOB_CREATE", $member->func_permission)) {
				?><a href="job_new.php" class="pull-right">Create New Jobs <i class="fa fa-arrow-right"></i></a><?php
			}
			?>
			<h1 class="page_header">Job List</h1>
			<section id="jobs_panel" class="col-md-12">
				<div>
					<form method="post" action="">
						<div class="row">
							<div class="col-md-2"><label>Job No.</label></div>
							<div class="col-md-4"><input class="form-control" name="job_no" value="<?=$job_no?>"	/></div>

							<div class="col-md-2"><label>Programme</label></div>
							<div class="col-md-4"><select class="form-control select2" multiple="multiple" name="programme_id[]" id="programme_id"><?php
							foreach ($programme as $key => $value) {
								?><option value="<?=$key?>" selected="selected"><?=$value?></option><?php
							}
							
							?></select></div>
						</div>
						<div class="row">
							<div class="col-md-2"><label>Company Name.</label></div>
							<div class="col-md-4"><input class="form-control" name="company" value="<?=$company?>" /></div>

							<div class="col-md-2"><label>Job Position</label></div>
							<div class="col-md-4"><input class="form-control" name="position" value="<?=$position?>"/></div>
						</div>
						<div class="row">
							<div class="col-md-2"><label>Application Period.</label></div>
							<div class="col-md-4"><input class="form-control date-range" name="application" value="<?=$application?>" /></div>

							<div class="col-md-6"><button class="btn btn-default">Submit</button></div>
						</div>
					</form>
				</div>
			</section>
			<section id="jobs_list" class="col-md-12">
				
				<ul id="list">
					<?php
						foreach ($joblist as $job) {
							if (++$ni > $limit) { break; }
					?><li><div class="viewbtn"><a href="./job/<?=$job["job_no"]?>-<?=str_replace("-", "", urlencode($job["name"]."@".$job["dept"]."@".$job["company"]))?>"><button class="btn btn-default">View Details</button></a></div>
						<div class="position"><?=$job["job_no"]?> - <strong><?=$job["name"]?></strong></div>
						<div class="company"><?=($job["dept"] != "") ? $job["dept"] . "<BR>" : ""?> <?=$job["company"]?></div></li><?php
						}
					?>					
				</ul>
				<?php
				if (count($joblist) > $limit || $page > 1) {
				?><nav aria-label="Page navigation">
				  <ul class="pagination">
				    <?php
				    if ($page > 1) {
 				    ?><li><a href="jobs?page=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li><?php
				    
				    }
				    $recordCount = ceil(count($joblist) / $limit) + ($page - 1); 
				    for ($i = 1; $i <= $recordCount; $i++) { 
				    ?>
				    <li><a href="jobs?page=<?=$i?>"><?=$i?></a></li>
				    
				    <?php
				    }
				    if (count($joblist) > $limit ) {
				    ?><li>
				      <a href="jobs?page=<?=$recordCount?>" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				      </a>
				    </li><?php
				    }
				    ?>
				  </ul>
				</nav><?php
				}
				?>


			</section>
			<link href="/css/select2.min.css" rel="stylesheet" />
			<script src="/js/select2.min.js"></script>
			<script type="text/javascript">
				$('#programme_id.select2').select2({
					multiple: true,
					ajax: { url: '../xhr/program',dataType: 'json',}
				});
			</script>
		</div>
	</main>
<?php
	include_once("footer.php");
?>