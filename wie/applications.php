<?php
	require_once("../config.inc.php");
	require_once("../classes/application.php");
	require_once("../classes/system.php");

	include_once("header.php");
	$pageName = array("Applications"=>"");
	
	
	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}
	

	if (array_key_exists("app_no", $_POST)) {
		$app_no = (int)$_POST["app_no"];
		$application = new Application($app_no);
		$application = $application->getDetail();
		$student = new Student($application["member_id"]);
		$student = $student->getDetail();
	}
	

	if (array_key_exists("status", $_POST)) {
		
		$lecture = new Lecturer();	
		if (!$lecture->isLogined()) {
			header("location: login.php");
			exit();
		}
		$app_id = $lecture->updateApplicationStatus($_POST["status"]);
		$msg = "Record Saved.";

		$application = new Application($app_id);

		$application = $application->getDetail();

		$student = new Student($application["member_id"]);
		$student = $student->getDetail();
		
	}
	$district = new District();

?>	<main id="wie-report">
		<?php include_once("breadcrumb.php")?>
		<div  class="wrapper"><div id="msg_wrapper"><?=$msg?> </div></div>
		<div class="wrapper"><?php
			if (in_array("APPLICATION_OFFER", $member->func_permission)) {
				?><a href="batch_offering.php" class="pull-right">Batch Offering <i class="fa fa-arrow-right"></i></a><?php
			}
			?><h1 class="page_header">Manage Applications</h1>
			<section id="jobs_panel">
				<form method="POST">
					
					<ul class="nav nav-pills">
						<li class="disabled"><a>Search By:</a></li>
						<li><a data-toggle="tab" href="#home">Student</a></li>
						<li  class="active"><a data-toggle="tab" href="#menu1">WIE No.</a></li>
					</ul>
					<br />
					<div class="tab-content">
						<div id="home" class="tab-pane  fade in ">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-2">
										<label for="student_id" >Student ID. </label>
									</div>
									<div class="col-md-4">
										<select id="student_id" name="student_id" class="select2 form-control" data-field="student_id"></select>
									</div>
									<div class="col-md-12"><ul id="appl_list" class="nav nav-pills"></ul></div>
									
								</div>
							</div>
						</div>
						<div id="menu1" class="tab-pane  fade in active">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-2">
										<label for="search_wie" >WIE No. </label>
									</div>
									<div class="col-md-4">
										<select id="search_wie" name="app_no" class="select2 form-control" data-field="wie_app_no"><?php
										if ($application) {
										?><option value="<?=$application["id"]?>" selected="selected"><?=$application["app_no"]?></option><?
										}
										?></select>
									</div>
									<div class="col-md-6">
										<button type="submit" class="col-md-2 btn btn-default" id="applyfilter">Apply</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</form>
			</section>

			<section>
				<?php
				if ($application) {
				?><div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Student Information</h3></div>
						  <div class="panel-body">
						    <div class="row">
								<div class="col-md-2 "><label>Student No:</label></div>
								<div class="col-md-2 "><?=$student["student_id"]?></div>
								
							</div>
							<div class="row">
								<div class="col-md-2 "><label>Program of Study:</label></div>
								<div class="col-md-1 "><?=($student["program"]["code"])?></div>
								<div class="col-md-2 "><label>Program Title:</label></div>
								<div class="col-md-3 "><?=($student["program"]["name"])?></div>
								<div class="col-md-2 "><label>Study Year:</label></div>
								<div class="col-md-2 "><?=($student["yrofstudy"])?></div>
							</div>
						  </div>
					    </div>
					</div>
				</div>
				<?
				}
				?>
			</section>
			<section>
				<?php
				if ($application) {
				?><div class="row">
					<div class="col-md-8">
						<div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Personal Particulars</h3></div>
						  <div class="panel-body">
						    <div class="row">
								<div class="col-sm-3 "><label>English Name:</label></div>
								<div class="col-sm-3"><?=$application["firstname_en"] . " ".$application["lastname_en"]?></div>
								<div class="col-sm-3 "><label>Chinese Name:</label></div>
								<div class="col-sm-3"><?=$application["lastname_zh"].$application["firstname_zh"]?></div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>HKID No.:</label></div>
								<div class="col-sm-3"><?=$application["hkid"]?></div>
								<div class="col-sm-3 "><label>Date of birth:</label></div>
								<div class="col-sm-3"><?=($application["dob"])?></div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>Gender:</label></div>
								<div class="col-sm-3"><?=$application["gender"]?></div>
								<div class="col-sm-3 "><label>Religion :</label></div>
								<div class="col-sm-3"><?=($application["religion"])?></div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>Address:</label></div>
								<div class="col-sm-9"><?=($application["address1"]) . "<BR />". $application["address2"]. "<BR />".$application["address3"]?></div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>Home District:</label></div>
								<div class="col-sm-3"><?=$district->getName($application["district"])?></div>
							</div>
							
							
							<div class="row">
								<div class="col-sm-12">&nbsp;</div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>College Email:</label></div>
								<div class="col-sm-3"><?=$student["c_email"]?></div>
								<div class="col-sm-3 "><label>Home Phone:</label></div>
								<div class="col-sm-3"><?=($student["h_phone"])?></div>
							</div>

							
							<div class="row">
								<div class="col-sm-3 "><label>Personal Email:</label></div>
								<div class="col-sm-3"><?=$application["email"]?></div>
								<div class="col-sm-3 "><label>Mobile Phone:</label></div>
								<div class="col-sm-3"><?=($application["contact"])?></div>
							</div>
							
							
							<div class="row">
								<div class="col-sm-12">&nbsp;</div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>Emergency Person:</label></div>
								<div class="col-sm-3"><?=$application["em_name"] . " " . $application["em_name2"]?></div>
								<div class="col-sm-3 "><label>Emergency Contact:</label></div>
								<div class="col-sm-3"><?=$application["em_contact"] ?></div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>Contact:</label></div>
								<div class="col-sm-3"><?=$application["em_phone"]?></div>
								<div class="col-sm-3 "><label>Relationship:</label></div>
								<div class="col-sm-3"><?=$application["em_relation"]?></div>
							</div>
						  </div>
					    </div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Get student Application Form(s)</h3></div>
						  <div class="panel-body">
						    <div class="row">
								<div class="col-md-4"><a href="../export_appform-<?=uniqid(sha1("application-".$application["id"]))?>-<?=base64_encode(sha1("application-".$application["id"]))?>" target="_blank"><span class="fa fa-file-pdf fa-5x"></span></a></div>
							</div>
							
						  </div>
					    </div>
					    <?php
					    if ($application["dse"].$application["hkcc"].$application["other1"].$application["other2"].$application["other3"] != "") {
					    ?><div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Students Attachments</h3></div>
						  <div class="panel-body">
						  	<?php
						  	if ($application["dse"] != "") {
						  	?>
						    <div class="row">
								<div class="col-md-3">DSE</div>
								<div class="col-md-9"><a href="../<?=$application["dse"]?>" target="_blank">Click Here</a></div>
							</div><?php
							}
							if ($application["hkcc"] != "") {
						  	?>
						    <div class="row">
								<div class="col-md-3">HKCC</div>
								<div class="col-md-9"><a href="../<?=$application["hkcc"]?>" target="_blank">Click Here</a></div>
							</div><?php
							}
							if ($application["other1"] != "") {
						  	?>
						    <div class="row">
								<div class="col-md-3">OTHER 1</div>
								<div class="col-md-9"><a href="../<?=$application["other1"]?>" target="_blank">Click Here</a></div>
								
							</div><?php
							}
							if ($application["other2"] != "") {
						  	?>
						    <div class="row">
								<div class="col-md-3">OTHER 2</div>
								<div class="col-md-9"><a href="../<?=$application["other2"]?>" target="_blank">Click Here</a></div>
								
							</div><?php
							}
							if ($application["other3"] != "") {
						  	?>
						    <div class="row">
								<div class="col-md-3">OTHER 3</div>
								<div class="col-md-9"><a href="../<?=$application["other3"]?>" target="_blank">Click Here</a></div>
							</div><?php
							}
							?>
						  </div>
					    </div><?php
					    }
					    ?>
					</div>

					<div class="col-md-8">
						<div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Application Summary </h3></div>
						  <div class="panel-body">
						    <div class="row">
								<div class="col-md-3 "><label>Status:</label></div>
								<div class="col-md-3 "><?=$application["status"]?></div>
								<div class="col-md-3 "><label>Submission Date:</label></div>
								<div class="col-md-3 "><?=$application["submission_date"]?></div>
							</div>
							
							<div class="row">
								<div class="col-sm-3 "><label>Availability:</label></div>
								<div class="col-sm-3"><?=$application["availability"]?></div>
								<div class="col-sm-3 "><label>Expected Salary:</label></div>
								<div class="col-sm-3"><?=($application["expected_salary"])?></div>
							</div>
							<div class="row">
								<div class="col-sm-3 "><label>Preferred Work Location:</label></div>
								<div class="col-sm-9"><?=$district->getName($application["work_district"])?></div>
							</div>
							<?php
							if ($application["remarks"] != "") {
							?><div class="row">
								<div class="col-sm-12 "><label>Additional Information:</label></div>
								<div class="col-sm-12"><?=($application["remarks"])?></div>
							</div><?php
							}
							if ($application["selfaccess"] != "") {
							?>
							<div class="row">
								<div class="col-sm-12 "><label>Self Assessment:</label></div>
								<div class="col-sm-12"><?=($application["selfaccess"])?></div>
							</div><?php
							}
							?>
						  </div>
					    </div>
					</div>

				</div><?php
				}
				?>
			</section>
			<section id="application_details">
				<form method="post"><?php
				if ($application) {
				?><div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Application Details</h3></div>
						  	<div class="panel-body" >
						    	<form class="form">
							    	<table border="0" cellpadding="0" cellspacing="0" class="table table-striped">
								    	<tr>
								    		<th>Priority</th>
								    		<th>Job No.</th>
								    		<th>Job Title</th>
								    		<th>Company / Dept</th>
								    		<th>Submission Date</th>
								    		<th>Offer Email Date</th>
								    		<th>Status</th>
								    		<th>Action</th>
								    	</tr>
								    	<?php
								    	foreach ($application["items"] as $job_item) {
								    		?><tr>
							    			<td><?=$job_item["priority"]?></td>
								    		<td><?=$job_item["job"]["job_no"]?></td>
								    		<td><?=$job_item["job"]["name"]?></td>
								    		<td><?=$job_item["job"]["company"]?> <?=$job_item["job"]["dept"]?></td>
								    		<td><?=$job_item["create_date"]?></td>
								    		<td><?=$job_item["offer_send_date"]?></td>
								    		<td><?php
								    		if (in_array("APPLICATION_OFFER", $member->func_permission)) {
								    		?><select name="status[<?=$job_item["id"]?>]">
								    			<option value="" >Open</option>
								    			<option value="closed" <?=($job_item["status"] == 'closed') ? 'selected="selected"' : ""?>>Closed</option>
								    			<option value="offered" <?=($job_item["status"] == 'offered') ? 'selected="selected"' : ""?>>Offered</option>
								    		</select><?php
											} else {
												echo $job_item["status"];
											}
								    		
								    		?></td>
								    		<td><button type="button" class="btnSendEmail" <?=($job_item["status"] == 'offered') ? '' : 'title="Only offered job post can send email to student" disabled="disabled"'?> data-app-id="<?=sha1("appli-job-".$job_item["id"])?>" > Send Email </button></td>
								    	</tr>
								    		<?
								    	}
								    	?>
								    </table>
								    <div class="text-center">
								    	<button type="submit" class="btn btn-default">Save</button>
								    </div>
								</form>
						  	</div>
					    </div>
					</div>
					
				</div><?php
				}
				?></form>
			</section>
			<script src="../scripts/application.js"></script>
			<link href="/css/select2.min.css" rel="stylesheet" />
			<script src="/js/select2.min.js"></script>
			<script>
				setTimeout("$('#msg_wrapper').fadeOut();", 3000);
			</script>
		</div>
	</main>
<?php
	include_once("footer.php");
?>