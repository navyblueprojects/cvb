<?php
	include_once("../config.inc.php");
	include_once("../classes/lecturer.php");

	$member = new Lecturer();
	$cmsObj = new CMS();

	$logined = $member->isLogined();
	

?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Curriculum Vitae Builder - College of Professional and Continuing Education</title>
	<meta name='robots' content='noindex,follow' />
	<base href="https://<?=$domain.$appPath?>/wie/" >

	<link rel="stylesheet" href="../css/fontawesome-all.min.css" >
	<link rel="stylesheet" href="../css/style.css?v=<?=time()?>" />
	<link rel="stylesheet" href="../css/bootstrap.min.css" />
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css"  />
	<link rel="stylesheet" href="../css/jquery.datetimepicker.css" />	
	<link rel="stylesheet" href="../js/daterangepicker.css" />
	<link rel="shortcut icon" type="image/png" href="../favicon.png"/>

	<script src="../js/jquery-2.2.5.min.js" ></script>
	<script src="../js/bootstrap.min.js" ></script>
	<script src="../scripts/wie.js" ></script>
	<script type="text/javascript" src="../js/moment.min.js" ></script>
	<script type="text/javascript" src="../js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="../js/daterangepicker.js" ></script>
	
	<script type="text/javascript" src="<?=$appPath?>/lib_common/ckeditor/ckeditor.js" ></script>
	<script type="text/javascript" src="<?=$appPath?>/lib_common/ckeditor/ckfinder/ckfinder.js"  async></script>
	<script>
		var viewCms = <?=($cmsObj->checkLogin()) ? "true" : "false"?>;
	</script>
	
</head>
<body class="font12 wie">
	<header>
		<div class=" row1">
			<div class="wrapper">
				<div class="row">
					<div class="col-xs-6 row">
						
						<div class="col-sm-12">
							<div class="dropdown">
								<a class="dropdown-toggle" id="ddFontSize" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<i class="fas fa-font"></i>
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" aria-labelledby="ddFontSize">
									<li><a data-font-size="font12">Small</a></li>
									<li><a data-font-size="font14">Medium</a></li>
									<li><a data-font-size="font16">Large</a></li>
								</ul>
							</div>
							

						</div>
					</div>
					<div class="col-lg-3 col-md-4 hidden-sm hidden-xs pull-right">
						<?php
							if (!$logined) {
							?><ul id="header_portal_nav"><li><a>Staff</a></li><li><a href="../login.php">Students</a></li></ul><?php
							}
						?>
					</div>
					<div class="visible-sm visible-xs pull-right">
						<div class="dropdown">
							<a class="dropdown-toggle" id="ddUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="fas fa-user"></i>
								<span class="caret"></span>
							</a>
							<?php
								if (!$logined) 
								{
							?><ul class="dropdown-menu dropdown-menu-right" aria-labelledby="ddUser">
								<li><a>Staff</a></li>
								<li><a href="../login.php">Students</a></li></ul><?php
								}
							?>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row row_wie">
			<div class="wrapper">
				<div class="col-md-8 col-xs-12" id="logo">
					<a href="http://www.cpce-polyu.edu.hk/"><img src="../images/logo_01.svg" /></a>
				</div>
				<div class=" visible-sm visible-xs text-right college-link" >
					<ul>
						<li><a href="http://www.hkcc-polyu.edu.hk/">HKCC <i class="fa fa-angle-right"></i></a></li>
						<li><a href="https://www.speed-polyu.edu.hk/" rel="noreferrer noopener" target="_blank" title="This link will pop up in a new window">SPEED <i class="fa fa-angle-right"></i></a></li>
					</ul>
				</div>

				<div class="col-md-4 hidden-sm hidden-xs text-right" ><a href="http://www.hkcc-polyu.edu.hk/"><img src="../images/logo_03.svg" height="50" /></a></div>
				
			</div>
		</div>
	</header>