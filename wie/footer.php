<?php
	require_once("config.inc.php");

	$footerPage = $cmsObj->getPageList();
	
?><footer>
	
	
	<div  id="top-footer">
		<div class="wrapper">
			<div class="row" >
				<div class="col-sm-5"><img src="../images/qess.png" alt="Funded by Quality Enhancement Support Scheme (QESS)"/></div>
				<div class="col-sm-6 col-sm-offset-1 text-right">
					<a href="https://sao.cpce-polyu.edu.hk/index.php" rel="noreferrer noopener" target="_blank"><img src="../images/logo_csao.png" alt="PolyU CPCE Student Affairs Office" /></a>
					<a href="http://www.sao.cpce-polyu.edu.hk/fscd/" rel="noreferrer noopener" target="_blank"><img src="../images/logo_fscc.png" alt="PolyU Further Study & Career Centre" /></a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="bottom-footer"><div class="wrapper">
		<nav>
			<ul>
				<?php
					foreach ($footerPage as $page ){
						?><li><a href="<?=$appPath . "/" . $page["tag"]?>"><?=$page["pageName"]?></a></li><?php
					}
				?>
			</ul>
		</nav>
		<div id="copyright">Copyright &copy; 2018 The Hong Kong Polytechnic University. All Rights Reserved.</div>
	</div></div>
</footer>
<script>
	var viewCms = <?=($cmsObj->checkLogin()) ? "true" : "false"?>; 
</script>
</body>
</html><?php
	session_write_close();
?>