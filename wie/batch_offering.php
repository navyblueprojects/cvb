<?php
	require_once("../config.inc.php");
	require_once("../classes/application.php");

	include_once("header.php");
	$pageName = array("Batch Offering"=>"");
	
	
	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}

	if (!in_array("APPLICATION_OFFER", $member->func_permission)) {
		header("location: home.php");
		exit();
	}

	if (array_key_exists("job_no", $_POST)) {
		


		$job = new Jobs();
		$job = $job->getDetailByJobNo($_POST["job_no"]);

		$application = new Application();
		$application = $application->findApplicationsByJobNo($_POST["job_no"]);
	}		
	

	if (array_key_exists("status", $_POST)) {
		
		$lecturer = new Lecturer();
		if (!$lecturer->isLogined()) {
			header("location: login.php");
			exit();
		}
		$jobs_id = $lecturer->batchUpdateStatus($_POST["status"]);

		$job = new Jobs($jobs_id);
		$job = $job->getDetails();

		$application = new Application();
		$application = $application->findApplicationsByJobNo($job["job_no"]);
	}

?>	<main id="wie-report">
		<?php include_once("breadcrumb.php")?>
		<div class="wrapper">
			<a href="applications.php" class="pull-right">Manage Applications <i class="fa fa-arrow-right"></i></a>
			<h1 class="page_header">Batch Offering</h1>
			<section id="jobs_panel">
				<form method="POST">
					<div class="row">
						<div class="col-md-1">
							<label for="search_job" >Job No. </label>
						</div>
						<div class="col-md-4">
							<select id="search_job" name="job_no" class="select2 form-control" data-field="jobs.job_no"><?php
							if ($job) {

							?><option value="<?=$job["id"]?>" selected="selected"><?=$job["job_no"]?></option><?
							}
							?></select>
						</div>
						<div class="col-md-6">
							<button type="submit" class="col-md-2 btn btn-default" id="applyfilter">Apply</button>
						</div>
					</div>
					<div class="row">
						
					</div>
				</form>
			</section>
			<section>
				<?php
				if ($job) {
					
				?><div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Job Details</h3></div>
						  <div class="panel-body">
						    <div class="row">
								<div class="col-md-2 "><label>Job No:</label></div>
								<div class="col-md-2 "><?=$job["job_no"]?></div>
								
							</div>
							<div class="row">
								<div class="col-md-2 "><label>Name:</label></div>
								<div class="col-md-4 "><?=($job["name"])?></div>
								<div class="col-md-2 "><label>Company / Dept:</label></div>
								<div class="col-md-4 "><?=($job["company"])?> <?=($job["dept"])?></div>
								
							</div>
							<div class="row">
								<div class="col-md-2 "><label>Vacancy:</label></div>
								<div class="col-md-4 "><?=($job["vacancy"])?></div>
								<div class="col-md-2 "><label>Application Period:</label></div>
								<div class="col-md-4 "><?=($job["open_date"])?> - <?=($job["close_date"])?></div>
								
							</div>
						  </div>
					    </div>
					</div>
				</div><?
				}
				?>
			</section>
			
			<section>
				<div class="row">
					<div class="col-md-12">

						<div class="alert alert-warning hide" id="unsave-warning" role="alert"><i class="fa fa-exclamation-triangle"></i> You have unsaved changes. Press Save to submit changes or <a onclick="revertAll()">click here to revert all changes</a></div>

						<div class="panel panel-default">
						  <div class="panel-heading">
						  	<h3 class="panel-title">Application Details </h3>
						  	
						  </div>
						  	<div class="panel-body">
						    	<?php
								if ($application) {

								?><form class="form" id="batch_offering" method="post" onsubmit="return batch_offering()">
									<div class="text-right">
									 
								</div>
							    	<table border="0" cellpadding="0" cellspacing="0" class="table table-striped">
								    	<?php
								    			if (count($application) > 1) {
								    		?><tr>
								    		<td  colspan="9" align="right"><div class="btn-group btn-group-xs" role="group" >
											  <button type="button" class="btn btn-active" onclick="offerAll()">Offer All</button>
											  <button type="button" class="btn btn-active" onclick="rejectAll()">Reject All</button>
											</div></td>
								    	</tr><?php
												}
											?>

								    	<tr>
								    		<th>WIE No</th>
								    		<th>Student No.</th>
								    		<th>English Name</th>
								    		<th>Chinese Name</th>
								    		<th>Submission Date</th>
								    		<th>Last Update</th>
								    		<th>Status</th>								    		
								    		<th>Action</th>
								    		<th>Offer Email Date</th>
								    	</tr>
								    	<?php
								    	foreach ($application as $job_item) {

								    		
								    		?><tr>

							    			<td><?=$job_item["application"]["app_no"]?></td>
								    		<td><?=$job_item["student"]["student_id"]?></td>
								    		<td><?=$job_item["application"]["firstname_en"] . " " . $job_item["application"]["lastname_en"] ?></td>
								    		<td><?=$job_item["application"]["lastname_zh"] . $job_item["application"]["firstname_zh"]?></td>
								    		<td><?=$job_item["item"]["create_date"]?></td>
								    		<td><?=$job_item["item"]["lastModDate"]?></td>
								    		<td ><?php
								    		/*
							    			if ($job_item["item"]["status"] == "" || $job_item["item"]["status"] == "OPEN") {

							    				?><a class="offerAppl" data-id="<?=$job_item["item"]["id"]?>">Offer</a><?
							    			} else {
							    				?><span><?=$job_item["item"]["status"]?></span><?
							    			}*/
								    		?><div class="btn-group" role="group" style="display: flex;">
											  <button type="button" data-status="offered" data-id="<?=$job_item["item"]["id"]?>" data-class="btn-<?=($job_item["item"]["status"] == "offered") ? "selected" : "active"?>" class="btn btn-<?=($job_item["item"]["status"] == "offered") ? "selected" : "active"?> btn-sm" data-status="OFFERED">OFFER</button>
											  <button type="button" data-status="closed" data-id="<?=$job_item["item"]["id"]?>" data-class="btn-<?=($job_item["item"]["status"] == "closed") ? "selected" : "active"?>" class="btn btn-<?=($job_item["item"]["status"] == "closed") ? "selected" : "active"?>  btn-sm" data-status="REJECTED">REJECT</button>
											</div></td>
											<td><?php
												if ($job_item["item"]["status"] == "offered") {
											?><button class="btn btn-sm btnSendEmail" type="button" data-app-id="<?=sha1("appli-job-".$job_item["item"]["id"])?>">
												<span class="visible-lg">Send Offered Email</span><span class="hidden-lg ">Email</span></button><?php
												}
											?></td>
											<td><?=($job_item["item"]["offer_send_date"]) ? $job_item["item"]["offer_send_date"] : " - "?></td>
								    	</tr>
								    		<?

								    	}
								    	?>
								    </table>
								    <div class="text-center">
								    	<button type="submit" class="btn btn-default">Save</button>
								    </div>
								</form><?php
								} else {
									?><div>No Application Yet</div><?
								}
								?>
						  	</div>
					    </div>
					</div>
					
				</div>
			</section>
			<script src="../scripts/batch_offering.js"></script>
			<link href="/css/select2.min.css" rel="stylesheet" />
			<script src="/js/select2.min.js"></script>
		</div>
	</main>
<?php
	include_once("footer.php");
?>