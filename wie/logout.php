<?php
	ob_start();	
	session_start();	
	include_once("../config.inc.php");
	require_once("../classes/cms.php");
	require_once("../classes/lecturer.php");

	$member = new Lecturer();
	$member = $member::retain();

	$_SESSION = false;
	session_write_close();
	session_destroy();

	ob_end_clean();
	$member->logout();
	
	header("location: ../");
	exit();

?>