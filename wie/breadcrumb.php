<?php

?><div class="wrapper">
	<div class="row"><div class="pull-right col-xs-6 text-right">
	<p class="member_row"><?php 
	if ($member->isLogined()) {
	?>Welcome <?=$member->getName()?>! <a href="logout">Logout</a><?php
	}
	?></p>
	</div>
	<div class="col-xs-6">
		<p class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
		<span typeof="v:Breadcrumb">
			<a rel="v:url" property="v:title" href="home">Home</a>
		</span>
		<?php
			foreach ($pageName as $key => $pName) {
				?> <span class="delimiter">&gt;</span> <?
				echo ($pName != "") ? '<a href="'.$pName.'" >'.$key.'</a>' : $key;
			}
		?>
	</p></div></div>
	
</div>