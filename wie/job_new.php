<?php
	session_start();
	require_once("../config.inc.php");
	require_once("../classes/jobs.php");
	require_once("../classes/system.php");

	include_once("header.php");
	$pageName = array("Job"=>"jobs.php");
	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}

	if (!in_array("JOB_CREATE", $member->func_permission)) {
		header("location: home.php");
		exit();
	}

	$id = array_key_exists("id", $_GET) ? $_GET["id"] : false;
	
	if (array_key_exists("name", $_POST)) {
		$jobObj = new Jobs();
		$jobObj->createByArray($_POST);
	}

	if ($id) {
		$jobObj = new Jobs();
		$jobObj->getJobByJobNo($_GET["id"]);	
	} 
	
	if ($jobObj) {
		
		$job = $jobObj->getDetails();
		$id = $job["id"];

	}
	if ($id) {
		$pageName["Edit"] = "";	
	} else {
		$pageName["Create"] = "";
	}
	if ($_SESSION[$dbDatabase . "_" . md5("SYSTEM_MSG")] != "") {
		$msg = $_SESSION[$dbDatabase . "_" . md5("SYSTEM_MSG")];
		unset($_SESSION[$dbDatabase . "_" . md5("SYSTEM_MSG")]);
	}
	session_write_close();


	$jobType = new JobType();
	$jobType = $jobType->getType();

	
?>
	<main id="wie-jobs_create">
		<?php include_once("breadcrumb.php")?>
		<div  class="wrapper">
			<div id="msg_wrapper"><?=$msg?> </div>
			 <div class="alert alert-danger alert-dismissible hide fade in" id="formError" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Error: </strong>Please fixed the following error to submit: <ul id="msg" ></ul>
			</div>	
		</div>
		
		<?php if ($id) {
			?><div  class="wrapper">
			<div class="col-md-8 col-md-offset-4">
				
				<ul class="nav nav-pills nav-justified">
					<li role="presentation" class="active"><a data-toggle="tab" href="#form-basic">Basic</a></li>
					<li role="presentation"><a data-toggle="tab" href="#form-target">Target Student</a></li>
					<li role="presentation"><a data-toggle="tab" href="#form-privilege">Staff Privileges</a></li>
					<li role="presentation"><a data-toggle="tab" href="#form-attachment">Attachment</a></li>
				</ul>
			</div>
		</div><?php
		 } 
		 ?>
		 
		<section id="jobs_form" class="col-md-12" >
			<div class="wrapper">
				<form method="post">
					<input type="hidden" name="id" value="<?=$id?>">
					<div class="tab-content">
					<div class="tab-pane fade in active" id="form-basic">
					<div class="form form-horizontal" >
						<?php
						if ($job["job_no"]) {
						?><div class="form-group">
							<div class="col-md-2"><label >Job No:</label></div>
							<div class="col-md-9"><?=$job["job_no"]?></div>
						</div><?php
						}
						?><div class="form-group">
							<div class="col-md-2"><label for="name">Position:</label></div>
							<div class="col-md-9"><input type="text" class="form-control req" id="name" name="name" autocomplete="off" value="<?=$job["name"]?>" /></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="dept">Department:</label></div>
							<div class="col-md-9"><input type="text" class="form-control" id="dept" name="dept" autocomplete="off" value="<?=$job["dept"]?>"/></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="dept">Company:</label></div>
							<div class="col-md-9"><input type="text" class="form-control" id="company" name="company" autocomplete="off" value="<?=$job["company"]?>"/></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="vacancy">No. of Vacancy: </label></div>
							<div class="col-md-9"><input type="text" class="form-control" id="vacancy" name="vacancy" autocomplete="off" value="<?=$job["vacancy"]?>"/></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="post">Type: </label></div>
							<div class="col-md-9"><?php
								$job["post"] = explode(";", $job["post"]);
							?><select class="form-control select2" id="post" name="post[]" multiple="multiple" autocomplete="off">
								<?php
								foreach ($jobType as $type) {
								?><option <?=in_array( $type["title"], $job["post"]) ? 'selected="selected"' : "";?>><?=$type["title"]?></option><?php
								}
								?>
								
							</select></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="post">Duty: </label></div>
							<div class="col-md-9"><textarea name="duty" class=" form-control" id="duty" autocomplete="off" ><?=$job["duty"]?></textarea></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="period">Job Period: </label></div>
							<div class="col-md-9"><input type="text" class="date-range form-control" id="period" name="period" autocomplete="off" value="<?=$job["period"]?>" /></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="salary">Salary / Allowance: </label></div>
							<div class="col-md-9"><input type="text" class="form-control" id="salary" name="salary" autocomplete="off" value="<?=$job["salary"]?>" /></div>
						</div>

						<div class="form-group">
							<div class="col-md-2"><label for="work_hour">Working Hours: </label></div>
							<div class="col-md-9"><input type="text" class="form-control" id="work_hour" name="work_hour" autocomplete="off" value="<?=$job["work_hour"]?>" /></div>
						</div>

						<div class="form-group">
							<div class="col-md-2"><label for="work_day">Working Days: </label></div>
							<div class="col-md-9"><input type="text" class="form-control" id="work_day" name="work_day" autocomplete="off" value="<?=$job["work_day"]?>" /></div>
						</div>

						<div class="form-group">
							<div class="col-md-2"><label for="job_no">Application Date:</label></div>
							<div class="col-md-4 col-sm-5"><input type="text" class="form-control datepicker req" name="open_date" id="open_date" autocomplete="off" value="<?=$job["open_date"]?>"/></div>
							<div class="col-md-1 col-sm-2"><label for="job_no">To</label></div>
							<div class="col-md-4 col-sm-5"><input type="text" class="form-control datepicker req" name="close_date" id="close_date" autocomplete="off" value="<?=$job["close_date"]?>"/></div>
						</div>
						<div class="form-group">
							<div class="col-md-2"><label for="remarks">Remarks:</label></div>
							<div class="col-md-9"><textarea class="form-control" id="remarks" name="remarks" autocomplete="off"><?=$job["remarks"]?></textarea></div>
						</div>
						
					</div>
					
				</div>
				<div class="tab-pane fade" id="form-target">
					<div class="form form-horizontal" >
						<div id="target_wrapper">
							<div class="target form-group hide newrow">
								<div class="col-xs-2">
									<a title="Remove" class="aRemove target hide"><span class="fa fa-times"></span> <span class="hidden-xs">Remove</span></a>
								</div>
								 <div class="col-xs-4"><select class="form-control " data-name="program_id[new][]"></select>
								 	<input type="hidden" value="" class="years" data-name="years[new][]" /></div>
								 <div class="col-xs-6 row">
									<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="years[new]" data-value="1" onclick="calcYear(this)"></a></div>
									<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="years[new]" data-value="2" onclick="calcYear(this)"></a></div>
									<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="years[new]" data-value="3" onclick="calcYear(this)"></a></div>
									<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="years[new]" data-value="4" onclick="calcYear(this)"></a></div>
								 </div>
							 </div>

						<div class="form-group row">
							<div class="col-xs-2"><span class="hidden-xs">Action</span></div>
							<div class="col-xs-4">Program</div>
							<div class="col-xs-6 row">
								<div class="col-xs-3"><span class="hidden-xs">Year 1</span><span class="visible-xs">Y1</span></div>
								<div class="col-xs-3"><span class="hidden-xs">Year 2</span><span class="visible-xs">Y2</span></div>
								<div class="col-xs-3"><span class="hidden-xs">Year 3</span><span class="visible-xs">Y3</span></div>
								<div class="col-xs-3"><span class="hidden-xs">Year 4</span><span class="visible-xs">Y4</span></div>
							</div>
						</div>
						<div class="form-group checkall-year <?=(($jobObj) && count($jobObj->getPermission()) > 0) ? '' : 'hide'?>">
							<div class="col-xs-6"></div>
							<div class="col-xs-6 row">
								<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="allyear" data-value="1" onclick="toggleAllYear(this)"></span></div>
								<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="allyear" data-value="2" onclick="toggleAllYear(this)"></span></div>
								<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="allyear" data-value="3" onclick="toggleAllYear(this)"></span></div>
								<div class="col-xs-3"><span class="fa fa-square fa-2x" data-name="allyear" data-value="4" onclick="toggleAllYear(this)"></span></div>
							 </div></div>
						<?php
							if ($jobObj) {
								foreach ($jobObj->getPermission() as $permission) {
									?><div class="target form-group">
										<div class="col-xs-2"><a title="Remove" class="aRemove target"><span class="fa fa-times"></span> <span class="hidden-xs">Remove</span></a></div>
										<div class="col-xs-4"><select class="form-control select2" name="program_id[<?=$permission["id"]?>]" ><option value="<?=$permission["program"]["id"]?>"><?=$permission["program"]["code"]?> <?=$permission["program"]["name"]?></option></select>
											<input type="hidden" class="years" name="years[<?=$permission["id"]?>]" value="<?=implode(";", $permission["years"])?>" /></div>
										<div class="col-xs-6 row"><?php

										for ($i=1; $i <= 4; $i++) { 
										?><div class="col-xs-3"><?php
											$thiss = in_array($i, $permission["years"]) ? "-check" : "";
											$thatt = in_array($i, $permission["years"]) ? "" : "-check";
											echo sprintf("<span class=\"fa fa%s-square fa-2x\" data-name=\"years[".$permission["id"]."]\" data-value=\"".$i."\" onclick=\"calcYear(this, ".$i.")\"></span>", $thiss, $thiss, $thatt);
										?></div><?
										}
								 ?></div></div><?
								}
							}

						?>
						</div>

							<div class="form-group"><div class="col-xs-2">
								<a title="Add New Row" class="aAddRow target" ><span class="fa fa-plus"></span> Add New Row</a>
							</div></div>
						</span>
					</div>
				</div>


				<div class="tab-pane fade" id="form-privilege">
					<div class="form form-horizontal" >
						<div id="privilege_wrapper"> 
							<div class="form-group"><div class="col-xs-2"><strong>Action</strong></div>
										 <div class="col-xs-5"><strong>Lecturer</strong></div>
										 <div class="col-xs-5">
											<div class="col-xs-4"><label class="hidden-xs">Read</label><label class="visible-xs">R</label></div>
											<div class="col-xs-4"><label class="hidden-xs">Update</label><label class="visible-xs">U</label></div>
											<div class="col-xs-4"><label class="hidden-xs">Delete</label><label class="visible-xs">D</label></div>
										 </div></div>
						<div class="privilege hide newrow form-group"><div class="col-xs-2"><a class="aRemove privilege"><span class="fa fa-times"></span> Remove</a></div>
							<div class="col-xs-5"><select class="form-control " data-name="lecturer[]"></select></div>
							 <div class="col-xs-5">
								<div class="col-xs-4"><span class="fa fa-square fa-2x" onclick="changePermission(this)"></span>
									<input type="hidden"  data-name="permission[READ][]" value="0"  ></div>
								<div class="col-xs-4"><span class="fa fa-square fa-2x" onclick="changePermission(this)"></span>
									<input type="hidden" data-name="permission[EDIT][]"  value="0" ></a></div>
								<div class="col-xs-4"><span class="fa fa-square fa-2x" onclick="changePermission(this)"></span>
									<input type="hidden" data-name="permission[DELETE][]"  value="0" ></a></div>
							 </div></div>
						<?php

					if ($jobObj) {
						foreach ($jobObj->getLecturerPermission() as $permission) {
							
							if ($permission["lecturer"]["username"] == $member->getName()) {
								?><div class="privilege form-group"><div class="col-xs-2"></div>
							<div class="col-xs-5">You <input type="hidden" value="<?=$permission["lecturer"]["id"]?>" name="n_lecturer[<?=$permission["id"]?>]" /></div>
							 <div class="col-xs-5">
								<div class="col-xs-4">
									<span class="fa fa-<?=($permission["permission"]["READ"]) ? 'check-' : ""?>square fa-2x"  ></span>
									<input type="hidden" name="n_permission[<?=$permission["id"]?>][READ]" value="<?=($permission["permission"]["READ"])?>" >
								</div>
								<div class="col-xs-4">
									<span class="fa fa-<?=($permission["permission"]["EDIT"]) ? 'check-' : ""?>square fa-2x" ></span>
									<input type="hidden" name="n_permission[<?=$permission["id"]?>][EDIT]" value="<?=($permission["permission"]["EDIT"])?>" >
								</div>
								<div class="col-xs-4">
									<span class="fa fa-<?=($permission["permission"]["DELETE"]) ? 'check-' : ""?>square fa-2x" ></span>
									<input type="hidden" name="n_permission[<?=$permission["id"]?>][DELETE]" value="<?=($permission["permission"]["DELETE"])?>" >
								</div>
							 </div></div><?
							} 

						}
						foreach ($jobObj->getLecturerPermission() as $permission) {
						 if ($permission["lecturer"]["username"] != $member->getName()) {

						?><div class="privilege form-group"><div class="col-xs-2"><a class="aRemove privilege"><span class="fa fa-times"></span> <span class="hidden-xs ">Remove</span></a></div>
							<div class="col-xs-5"><select class="form-control select2 " name="n_lecturer[<?=$permission["id"]?>]"><?php
							 	?><option value="<?=$permission["lecturer"]["id"]?>"><?=$permission["lecturer"]["username"]?></option><?php
							 ?></select></div>
							 <div class="col-xs-5">
								<div class="col-xs-4">
									<span class="fa fa-<?=($permission["permission"]["READ"]) ? 'check-' : ""?>square fa-2x" onclick="changePermission(this)"></span>
									<input type="hidden" name="n_permission[<?=$permission["id"]?>][READ]" value="<?=($permission["permission"]["READ"]) ?>" >
								</div>
								<div class="col-xs-4">
									<span class="fa fa-<?=($permission["permission"]["EDIT"]) ? 'check-' : ""?>square fa-2x" onclick="changePermission(this)"></span>
									<input type="hidden" name="n_permission[<?=$permission["id"]?>][EDIT]" value="<?=($permission["permission"]["EDIT"])?>">
								</div>
								<div class="col-xs-4">
									<span class="fa fa-<?=($permission["permission"]["DELETE"]) ? 'check-' : ""?>square fa-2x" onclick="changePermission(this)"></span>
									<input type="hidden" name="n_permission[<?=$permission["id"]?>][DELETE]" value="<?=($permission["permission"]["DELETE"])?>">
								</div>
							 </div></div><?php
							}
						}
					}
				?></div>	
				<div class="form-group"><div class="col-md-2">
								<a title="Add New Row" class="aAddRow privilege" ><span class="fa fa-plus"></span> Add New Row</a>
							</div></div>
					
						
					</div>
				</div>
				
				<div class="tab-pane fade" id="form-attachment">
					<div class="form form-horizontal" >
						  <div class="col-sm-12">
				            <label>Attachment (max 300KB):</label>
				            <div class="ajax-upload row " data-placement="top" title="Error, file size limited to 300KB">
				              <input type="file" class="form-control" id="attachment" />
				              <div class="col-sm-10 col-xs-8">
				                <div class="progress">
				                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
				                </div>
				              </div>
				              <div class="col-xs-1"><button type="button" class="btn btn-upload btn-default">Upload</button></div>
				            </div>
				            <div class="link-wrapper row">
				              <div class="link col-sm-9"><?php

				        
				                if ($job["attachment"] != "") {
				              ?><a class="btn" href="<?=UFILE_PATH . $job["attachment"] ?>" target="_blank"><i class="fa fa-paperclip"></i> <?=$job["attachment"]?></a>
				              <a class="btn btn-remove" data-remove-attach="attachment"><i class="fa fa-ban"></i> Delete</a><?php
				                }
				              ?></div></div>
				            </div>
				          
				          <div class="col-sm-12">
				            <label>Attachment 2 (max 300KB):</label>
				            <div class="ajax-upload row " data-placement="top" title="Error, file size limited to 300KB">
				              <input type="file" class="form-control" id="attachment02" />
				              <div class="col-sm-10  col-xs-8">
				                <div class="progress">
				                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
				                </div>
				              </div>
				              <div class="col-xs-1"><button type="button"  class="btn btn-upload btn-default">Upload</button></div>
				            </div>
				            <div class="link-wrapper row">
				              <div class="link col-sm-9"><?php

				        
				                if ($job["attachment02"] != "") {
				              ?><a class="btn" href="<?=UFILE_PATH . $job["attachment02"] ?>" target="_blank"><i class="fa fa-paperclip"></i> <?=$job["attachment02"]?></a>
				              <a  class="btn btn-remove" data-remove-attach="attachment02"><i class="fa fa-ban"></i> Delete</a><?php
				                }
				              ?></div></div>
				            </div>
				          
				          <div class="col-sm-12">
				            <label>Attachment 3 (max 300KB):</label>
				            <div class="ajax-upload row " data-placement="top" title="Error, file size limited to 300KB">
				              <input type="file" class="form-control" id="attachment03" />
				              <div class="col-sm-10 col-xs-8">
				                <div class="progress">
				                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
				                </div>
				              </div>
				              <div class="col-xs-1"><button type="button"  class="btn btn-upload btn-default">Upload</button></div>
				            </div>
				            <div class="link-wrapper row">
				              <div class="link col-sm-9"><?php

				        
				                if ($job["attachment03"] != "") {
				              ?><a class="btn" href="<?=UFILE_PATH . $job["attachment03"] ?>" target="_blank"><i class="fa fa-paperclip"></i> <?=$job["attachment03"]?></a>
				              <a class="btn btn-remove" data-remove-attach="attachment03"><i class="fa fa-ban"></i> Delete</a><?php
				                }
				              ?></div></div>
				            </div>
				          </div>
					</div>
				</div>

				<div class="action">
					<a href="jobs" class="btn btn-default" >Return to List</a>
					<button type="reset" class="btn btn-default">Reset</button>
					<button type="submit" class="btn btn-default">Submit</button>
				</div>
				</div></form>
			</div>
			</section>
		</div>
		<link href="/css/select2.min.css" rel="stylesheet" />
		<script src="/js/select2.min.js"></script>
		<script>
		var objj;
		$(function() {
			setTimeout("$('#msg_wrapper').fadeOut();", 3000);
			$('.select2').select2();
			$('#target_wrapper .select2').select2({
				ajax: { url: '../xhr/program.xhr.php',dataType: 'json'}
			});
			$('#privilege_wrapper .select2').select2({
				ajax: { url: '../xhr/lecturer.xhr.php',dataType: 'json'}
			});
		});
		</script>
	</main>
<?php
	include_once("footer.php");
?>