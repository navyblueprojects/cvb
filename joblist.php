<?php
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["Jobs"];
	
	include_once("breadcrumb.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	

	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;

	session_start(); $_SESSION[sha1($dbDatabase."_page_joblist")] = $page; session_write_close();
	
	$limit = 10;
	$offset = ($page - 1) * $limit;

	$joblist = $member->getJobList($offset, 9999);
	$detail  = $member->getDetail();

	$application = $member->getApplication();
	$application_status = $application["status"];

	$application = new Application($application["id"]);
	$itms = $application->getItems();
?>
	<main id="joblist" class="joblist">
		<script type="text/javascript" src="scripts/wie_app.js"></script>
		<div class="wrapper">
			<ul class="nav nav-step hidden-xs">
				<li class="active">Job List</li>
				<li><a href="job_preferences">Job Preference <span class="badge"><?=count($itms)?></span></a></li>
				<li>Confirmation</li>
				<li>Submit</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation" class="active"><a>Job List</a></li>
				<li role="presentation" ><a href="job_preferences">Job Preference (<?=count($itms)?>)</a></li>
				<li role="presentation" class="disabled"><a>Confirm / Submit</a></li>
			</ul>
			<div class="container">
				<h1 class="page_header">Jobs</h1>
				<div class="col-md-12"><?php
					if (!$member->isCompleteApplication()) {

						?><div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							You cannot submit your application since you have not yet complete your application form. Click <a href="new_cv#validate">here</a> to complete first.
						</div><?
					}
				?></div>
				<div >
				<section id="joblist_left" class="col-md-2"><data data-cms-title="jobs_list"><?=$cmsObj->getPageArea("jobs_list")?></data>
					
				</section>
				
				<section id="joblist_wrapper" class="col-md-10">
					<div class=" text-right">Maximum Number of Job Preferences: <?=$detail["program"]["max_job"]?></div>
					
					<?php
						if ($joblist) {
							
					?><ul id="list">
						<?php
							foreach ($joblist as $job) {
								if (++$ni > $limit) { break; }
						?><li>
							<div class="row">
								<div class="col-sm-9">
									<div ><label><?=$job["name"]?></label></div>
									<div ><?=$job["dept"]?><BR><?=$job["company"]?></div>
								</div>
								<div class="col-sm-3">
									<div class="viewbtn">
										<a href="./job/<?=$job["job_no"]?>-<?=str_replace("-", "", urlencode($job["name"]."@".$job["dept"]."@".$job["company"]))?>"><button class="btn btn-default">View Details</button></a>
										<button class="btn btn-default goAddJob" data-id="<?=$job["id"]?>"><i class="fa fa-plus"></i></button>
									</div>
							    </div>
							</div></li><?php
							}

						?>
						
					</ul><?php
					} else {
						?><div class="row">
						<div class="col-sm-12 text-center">
							<h2>No Opening Now.</h2>
						</div>
					</div><?php
					}
					?>
					<?php
				if (count($joblist) > $limit || $page > 1) {
				?><nav aria-label="Page navigation">
				  <ul class="pagination">
				    <?php
				    if ($page > 1) {
 				    ?><li><a href="joblist?page=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li><?php
				    
				    }
				    $recordCount = ceil(count($joblist) / $limit) + ($page - 1); 
				    for ($i = 1; $i <= $recordCount; $i++) { 
				    ?>
				    <li><a href="joblist?page=<?=$i?>"><?=$i?></a></li>
				    
				    <?php
				    }
				    if (count($joblist) > $limit ) {
				    ?><li>
				      <a href="joblist?page=<?=$recordCount?>" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				      </a>
				    </li><?php
				    }
				    ?>
				  </ul>
				</nav><?php
				}
				?>
				</section>

			</div>
			<div class="bottomRow">
						<div class="col-md-12">
							<a href="job_preferences" class="pull-right">Next <i class="fa fa-caret-right" ></i></a>
						</div>
					</div>
			</div>
		</div>
		<div class="modal fade" tabindex="-1" role="dialog" id="errorModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Error</h4>
	      </div>
	      <div class="modal-body">
	        <h3 id="errMsg"></h3>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	</main>
<?php
	
	include_once("footer.php");
?>