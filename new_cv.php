<?php
	ob_start();

	require_once("config.inc.php");
	include_once("classes/system.php");
	include_once("header.php");

	$pageName = ["Create CV Form"];
	
	include_once("breadcrumb.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	
	if (array_key_exists("goSave", $_GET)) {
		$member->saveCVForm($_POST["application_id"], $_POST, $_FILES);
		
		if ($_GET["goSave"] == "btn_gocv") {
			header("location: choose_content.php");
			exit();
		}
	}
	$member->getCVForm();
	
	$subject = new Subject();
	$exam_result = $subject->getAllCat();


?><main id="new_cv">
	<div class="wrapper">
		<div class="row">
			<ul class="nav nav-step hidden-xs ">
				<li class="active">Application Form</li>
				<li>Templates</li>
				<li>Preview & Save</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation" class="active"><a>Application Form</a></li>
				<li role="presentation" class="disabled"><a>Templates</a></li>
				<li role="presentation" class="disabled"><a>Preview & Save</a></li>
			</ul>
		</div>
	</div>
	<div class="container">
		<h1 class="page_header">Create CV Form </h1>	
		<div class="dropdown visible-sm pull-right " id="mobile-menu">
			<a class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				<i class="fa fa-bars"></i>
			</a>
			<div class="dropdown-menu">
				<ul class="nav nav-pills nav-stacked ">
					<li class="active" ><a data-toggle="tab" href="#basic" data-tab="basic"><span class="fa fa-user"></span> Personal Particulars</a></li>
					<li><a data-toggle="tab" href="#education" data-tid="addEducation" data-tab="education"><span class="fa fa-graduation-cap"></span> Education</a> </li>
					<li><a data-toggle="tab" href="#qualification" data-tid="addProfQuali" data-tab="qualification"><span class="fa fa-award"></span> Qualification(s)</a> </li>
					<li><a data-toggle="tab" href="#experience" data-tid="addExperience" data-tab="experience"><span class="fa fa-briefcase"></span> Experience(s)</a> </li>
					<li><a data-toggle="tab" href="#achievements" data-tid="addAchievement" data-tab="achievements"><span class="fa fa-trophy"></span> Achievement(s)</a> </li>
					<li><a data-toggle="tab" href="#skills" data-tid="addSkills" data-tab="skills"><span class="fa fa-wrench"></span> Skill(s)</a> </li>
					<li><a data-toggle="tab" href="#other" data-tid="tabOthers" data-tab="other"><span class="fa fa-star"></span> Others</a>  </li>
					<li><a data-toggle="tab" href="#attachment" data-tid="tabAttachment" data-tab="attachment"><span class="fa fa-paperclip"></span> Attachment(s)</a> </li>
				</ul>
			</div>
		</div>		
		<div class="alert alert-danger alert-dismissible hide fade in" id="formError" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Error: </strong>Please fixed the following error to submit: <ul id="msg" ></ul>
		</div>
		<form id="cv_form" method="POST" enctype="multipart/form-data">
			<section id="cv_wrapper" >
				<div >
					<ul class="nav nav-pills nav-stacked col-lg-2 col-md-3 hidden-sm hidden-xs">
						<li class="active" ><a data-toggle="tab" href="#basic" data-tab="basic"><span class="fa fa-user"></span> Personal Particulars</a></li>
						<li><a data-toggle="tab" href="#education" data-tid="addEducation" data-tab="education"><span class="fa fa-graduation-cap"></span> Education</a> </li>
						<li><a data-toggle="tab" href="#qualification" data-tid="addProfQuali" data-tab="qualification"><span class="fa fa-award"></span> Qualification(s)</a> </li>
						<li><a data-toggle="tab" href="#experience" data-tid="addExperience" data-tab="experience"><span class="fa fa-briefcase"></span> Experience(s)</a> </li>
						<li><a data-toggle="tab" href="#achievements" data-tid="addAchievement" data-tab="achievements"><span class="fa fa-trophy"></span> Achievement(s)</a> </li>
						<li><a data-toggle="tab" href="#skills" data-tid="addSkills" data-tab="skills"><span class="fa fa-wrench"></span> Skill(s)</a> </li>
						<li><a data-toggle="tab" href="#other" data-tid="tabOthers" data-tab="other"><span class="fa fa-star"></span> Others</a>  </li>
						<li><a data-toggle="tab" href="#attachment" data-tid="tabAttachment" data-tab="attachment"><span class="fa fa-paperclip"></span> Attachment(s)</a> </li>
						<li><div class="alert alert-info hide fade in" id="autosave"><i class="fa fa-spinner fa-spin"></i> Autosaving...</div><li>
					</ul>
					
					<div class="col-lg-7 col-lg-pull-3 col-md-9  col-xs-12 tab-content pull-right">
						<div id="basic" class="tab-pane fade in active"><?php include_once("cvform/basic.php")?></div>
						<div id="education" class="tab-pane fade in "><?php include_once("cvform/education.php")?></div>
						<div id="qualification" class="tab-pane fade in "><?php include_once("cvform/qualification.php")?></div>
						<div id="experience" class="tab-pane fade in "><?php include_once("cvform/experience.php")?></div>
						<div id="achievements" class="tab-pane fade in "><?php include_once("cvform/achievements.php")?></div>
						<div id="skills" class="tab-pane fade in "><?php include_once("cvform/skills.php")?></div>
						<div id="other" class="tab-pane fade in "><?php include_once("cvform/other.php")?></div>
						<div id="attachment" class="tab-pane fade in "><?php include_once("cvform/attachment.php")?></div>
					</div>
					<div class="tips col-lg-3 col-lg-push-7  col-md-3 hidden-xs hidden-sm ">
						<div >
							<h4>Tips</h4>
							<div class="tips_content"></div>
						</div>
					</div>
				</div>
				<div class="row" >
					<div class="col-xs-11 text-center" style="margin-bottom: 2em;" >
			          <button class="btn btn-default hide" id="btn_save" data-id="<?=$member->cvform["application"]["id"]?>" type="submit">Save</button>
			          <button class="btn btn-default" id="btn_gocv" data-id="<?=$member->cvform["application"]["id"]?>">Create My CV</button>
			          <?php
			          if ($member->isWIE()) {
			          ?><a href="joblist" class="btn btn-default">WIE Application</a><?php
			      	}
			          ?>
			    	</div>
			    </div>
			</section>
		</form>
	</div>
	<div class="visible-xs tips-mobile">
		<a data-toggle="modal" data-target="#modalTips"><i class="fa fa-question-circle"></i> Tips</a>
	</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="modalTips" aria-labelledby="lblModalTips">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="lblModalTips">Tips</h4>
	      </div>
	      <div class="modal-body">
	         <div class="tips_content"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<link href="css/select2.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'"/>
	<script src="js/select2.min.js" ></script>
	<script src="<?=$appPath?>/lib_common/ckeditor/ckeditor.js" ></script>
	<script src="<?=$appPath?>/lib_common/ckeditor/ckfinder/ckfinder.js" ></script>
	<script src="scripts/cvform.js?<?=time()?>" async></script>
</main>
<?php
	include_once("footer.php");
	ob_end_flush();
?>