<?php
// ============ Platform Dependent Variable ========================== +

$platForm = "mysqli";
$docTitle = "Curriculum Vitae Builder";
$dbHost = "192.168.70.115"; // locate server
$dbUser = "cvbuilder_dbusr";
$dbPassword = "jiJi&#x4DFE;";
$dbDatabase = "polyu_cvb";
$domain = "cvbuilder.cpce-polyu.edu.hk";
$contactEmail = array();
$enquiryEmail = array(); //order
$appPath = ""; 	// "/abc"

$debugMode = false;

global $remoteIPAddress;
$remoteIPAddress = $_SERVER['REMOTE_ADDR'];

$staff_directory = "192.168.95.11";
$staff_dn = array("DC=staff,DC=cpce,DC=hk");
$staff_domain = "@staff.cpce.hk";

$student_directory = "192.168.64.11";
$student_dn = array("ActiveStd - HKCC"=>"OU=HKCC,DC=student,DC=cpce,DC=hk", "ActiveStd - SPEED"=>"OU=SPEED,DC=student,DC=cpce,DC=hk");
$student_domain = "@student.cpce.hk";


$samlPath = "/var/saml/www";
$samlAuthority = "default-sp";

//$_version = array("en", "zh");

$currVersionPath = split("/", $_SERVER['SCRIPT_FILENAME']);
$currVersion = $currVersionPath[count($currVersionPath)-2];
$currPath = $currVersionPath[count($currVersionPath)-1];
	

$rootPath = $_SERVER['DOCUMENT_ROOT'];
$includePath = $rootPath.$appPath."/include";
$uploadImgDir = "/ufiles";
$uploadImgPath = $appPath . $uploadImgDir;
$pathDocRoot = $pathAppRoot = $rootPath.$appPath;
$adminPath = $appPath . "/admin";
$adoDBPath = $rootPath.$appPath."/lib_common/adodb"; # adodb
$pathClassLib = $rootPath.$appPath."/lib_common/classes";
$pathCommonLib = $rootPath.$appPath."/lib_common/jscripts";
$fckEditorPath = $appPath."/lib_common/ckeditor";
// ============ Platform Dependent Variable ========================== -

// ============ Server Settings ========================== +
//ini_set('default_charset', 'UTF-8');
//ini_set("error_reporting","E_ALL & ~E_NOTICE"); // hidden notice
date_default_timezone_set("Asia/Hong_Kong"); 
// ============ Server Settings ========================== -

// ================== upload folder setting ============================= +
$uploadedFileDir = "/ufiles";
$uploadedFilePath = $pathDocRoot . $uploadedFileDir;
$uploadedEmailFileDir = "/tmpemail";
$uploadedEmailFilePath = $pathDocRoot . $uploadedFileDir;

define("UFILE_PATH", $appPath.$uploadedFileDir."/");

$imageFilePath = $appPath . "/images";
// ================== upload folder setting ============================= -

/*
$pdo = new PDO(sprintf("mysql:dbname=%s;host=%s", $dbDatabase, $dbHost), $dbUser, $dbPassword);
$maintenance = $pdo->query("Select maintenanceFlag from `system` LIMIT 1");

foreach ($maintenance as $row) {
	if ($row["maintenanceFlag"] == "1") {
		die("The system is in maintenance. Please retry later");
		exit();
	} else {
		$pdo = $maintenance = false;
	}
}
*/
?>
