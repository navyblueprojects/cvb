<?php
	require_once("config.inc.php");
	require_once("classes/student.php");

	$member = new Student();
	$member = $member::retain();

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	

	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	$limit = 6;
	$offset = ($page - 1) * $limit;

	$pdflist = $member->getCVList($offset, $limit);

	$preload = array();
	

	include_once("header.php");
	$pageName = ["My CVs"];
	
	include_once("breadcrumb.php");


?>
	<main id="cvlist" class="cvlist">
		<div class="wrapper">
			
			<div class="container">
				<h1 class="page_header">My CVs</h1>
				
				
				<section id="cvlist_wrapper" class="col-md-12">
					<ul id="list" class="row">
						<?php
							$uniq_id = uniqid();

							foreach ($pdflist["data"] as $cv) {
								if ($cv["file01"] != "") {
							?><li class="col-sm-6 col-md-4">
								<div class="notIE">
									<!--object data="pfiles/cv-<?=$cv["file01"]?>" width="320" height="475"  type="application/pdf" >
									<embed src="pfiles/cv-<?=$cv["file01"]?>" width="320" height="475"  type="application/pdf" />
								</object-->
									<iframe src="pdf/embed<?=$appPath?>/pfiles/cv-<?=$cv["file01"]?>" width="320" height="475"></iframe>


								</div>
								<div class="forIE pdf-icon"><a href="pfiles/cv-<?=$cv["file01"]?>" target="_blank"><i class="fa fa-file-pdf"></i></a></div>
								<div class="text-center">
										<a href="pfiles/cv-<?=$cv["file01"]?>" target="_blank"><button class="btn btn-default">Download</button></a>
										<a data-id="<?=sha1($cv["file01"].$uniq_id)?>" class="btn btn-default" id="btnDelete">Delete</button></a>
									</div>
							</li><?php
								}
							}
						?>
						
					</ul>
					<div class="text-center"><nav aria-label="Page navigation">
						<ul class="pagination">
							<li><a href="my_cv?page=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
							<?php
							for ($i = 1; $i <= ceil($pdflist["total"] / $limit); $i++) {
							?><li <?=($page == $i) ? 'class="active"': ''?>><a href="my_cv?page=<?=$i?>"><?=$i?></a></li><?php
							}
							?>
							
							<li><a href="my_cv?page=<?=ceil($pdflist["total"] / $limit)?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>						
						</ul>
					</nav></div>
				</section>
				<script>

					$(function(){
						$('body').on('click','#btnDelete', function(){
							if (confirm('You cannot recover your CV once you delete. Are you sure?')){
								data = [];
								data.push({"name":"action", "value":"removeCVAttachment"});
								data.push({"name":"sess_id", "value": $(this).attr('data-id')});
								data.push({"name":"sess_key", "value": '<?=$uniq_id?>'});

								$.ajax({
									url: "xhr/cvform.xhr.php",
									data: data,
									dataType: "JSON",
									method: "POST"
								}).done(function(){
									window.location.reload();
								});
							}
						});
					});
				</script>
			</div>
		</div>
	</main>
<?php
	
	include_once("footer.php");
?>