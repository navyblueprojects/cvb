<?php
	ob_start();

	require_once("config.inc.php");
	include_once("classes/system.php");
	include_once("header.php");

	$pageName = ["Create CV Form"];
	
	include_once("breadcrumb.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	
	if (array_key_exists("goSave", $_GET)) {
		$id = $member->createCVContent($_POST["application_id"], $_POST);
		header("location: choose_template.php?id=".$id);
		exit();
		
	}
	$member->getCVForm();

	$subject = new Subject();
	$exam_result = $subject->getAllCat();


?><main id="choose_content">
	<div class="wrapper">
		<ul class="nav nav-step visible-xs">
			<li>Application Form</li>
			<li>Templates</li>
			<li class="active">Preview & Save</li>
		</ul>
		<ul class="nav nav-pills visible-xs">
			<li role="presentation" class="disabled"><a>Application Form</a></li>
			<li role="presentation" class="disabled"><a>Templates</a></li>
			<li role="presentation" class="active"><a>Preview & Save</a></li>
		</ul>
		<div class="container">
			<h1 class="page_header">Preview Application form</h1>
			<section id="cv_wrapper" class="col-md-12">
				<div class="row">
					<ul class="nav nav-pills nav-stacked col-md-2">
						<li class="active" ><a data-toggle="tab" href="#basic" data-tab="basic"><span class="fa fa-user">Personal</span></a></li>
						<li><a data-toggle="tab" href="#education" data-tab="education"><span class="fa fa-graduation-cap">Education</span></a> </li>
						<li><a data-toggle="tab" href="#experience" data-tab="experience"><span class="fa fa-briefcase">Work Experience(s)</span></a> </li>
						<li><a data-toggle="tab" href="#achievements" data-tab="achievements"><span class="fa fa-trophy">Achievements</span></a> </li>
						<li><a data-toggle="tab" href="#other" data-tab="other"><span class="fa fa-star">Other</span></a>  </li>
						
					</ul>
					<div class="col-md-10 tab-content">
						<div id="basic" class="tab-pane fade in active">
							<ul class="list">
								<li>
									<span class="cv-addon"></span>
									<div><label>Full Name</label><?=$member->cvform["personal"]["en_name"] . " " .$member->cvform["personal"]["en_lastname"]  ?></div>
								</li>
								<li><span class="cv-addon"></span><div><label>HKID</label><?=$member->cvform["personal"]["hkid"] ?></div></li> 
								<li><span class="cv-addon"></span><div><label>Gender</label><?=$member->cvform["personal"]["gender"]?></div></li> 
								<li><span class="cv-addon"></span><div><label>Age</label><?=$member->cvform["personal"]["age"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Date of Birth</label><?=$member->cvform["personal"]["dob"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Company Email</label><?=$member->cvform["personal"]["c_email"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Personal Email</label><?=$member->cvform["personal"]["p_email"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Home Phone</label><?=$member->cvform["personal"]["h_phone"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Mobile Phone</label><?=$member->cvform["personal"]["m_phone"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Address Line 1</label><?=$member->cvform["personal"]["address1"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Address Line 2</label><?=$member->cvform["personal"]["address2"]?></div></li>
								<li><span class="cv-addon"></span><div><label>Address Line 3</label><?=$member->cvform["personal"]["address3"]?></div></li>
								<li><span class="cv-addon"></span><div><label>District</label><?=$member->cvform["personal"]["district"]["name"]  ?></div></li>
   					 		</ul>
   					 	</div>
						<div id="education" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["education"]) > 0) {
							?><h4>Education</h4>
							<ul class="list"><?php
									foreach ($member->cvform["education"] as $edu) {	
										$edu["content"] = json_decode($edu["content"], true);
										
								?><li ><span class="cv-addon"></span>
									<div><span class="education-date"><?=$edu["content"]["fromdate"] . " - " . $edu["content"]["todate"]?></span>
									<?=$edu["content"]["school"]?><br /><?=$edu["content"]["class"]?></div></li><?php
									
									}
								?></ul>
							<?php
							}

							if (count($member->cvform["exam_result"]) > 0) {
							?><h4>Key Results</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["exam_result"] as $exam) {	
										$exam["content"] = json_decode($exam["content"], true);
								?><li ><span class="cv-addon"></span>
									<div><span class="education-date"><?=$exam["content"]["year"]?></span><?=$exam["content"]["exam"]?></div></li><?php
									}
								?>
							</ul><?php
							}

							if (count($member->cvform["public_exam"]) > 0) {
							?><h4>Other Public Examination Results</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["public_exam"] as $public_exam) {	
										$public_exam["content"] = json_decode($public_exam["content"], true);
								?><li ><span class="cv-addon"></span>
									<div><span class="education-date"><?=$public_exam["content"]["date"]?></span><?=$public_exam["content"]["name"]?><br />
									<?=$public_exam["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}
							?>
						</div>
						<div id="experience" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["experience"]["work"]) > 0) {
							?><h4>Work</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["experience"]["work"] as $work) {	
										$work["content"] = json_decode($work["content"], true);
								?><li ><span class="cv-addon"></span>
									<div><span class="education-date"><?=$work["content"]["fromdate"] . " - " . $work["content"]["todate"]?></span><?=$work["content"]["position"]?> (<?=$work["content"]["employment"]?>)<br />
									<?=$work["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}

							if (count($member->cvform["experience"]["internship"]) > 0) {
							?><h4>Internship</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["experience"]["internship"] as $internship) {	
										$internship["content"] = json_decode($internship["content"], true);
								?><li ><span class="cv-addon"></span>
									<div><span class="education-date"><?=$internship["content"]["fromdate"] . " - " . $internship["content"]["todate"]?></span><?=$internship["content"]["position"]?> (<?=$internship["content"]["employment"]?>)<br />
									<?=$internship["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}

							if (count($member->cvform["experience"]["reference"]) > 0) {
							?><h4>References</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["experience"]["reference"] as $reference) {	
										$reference["content"] = json_decode($reference["content"], true);
								?><li ><span class="cv-addon"></span>
									<div><?=$reference["content"]["name"]?><br />
										<?=$reference["content"]["position"]?>, <?=$reference["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}
							?>
						</div>
						<div id="achievements" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["achievement"]["achievement"]) > 0) {
							?><h4>Achievement</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["achievement"]["achievement"] as $achievement) {	
										$achievement["content"] = json_decode($achievement["content"], true);
										
								?><li ><?php
									?><span class="cv-addon"></span>
									  <div><span class="education-date"><?=$achievement["content"]["date"]?></span><?=$achievement["content"]["name"]?><BR>
									  	<?=$achievement["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}
							if (count($member->cvform["achievement"]["exactivity"]) > 0) {
							?><h4>Extra Activities</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["achievement"]["exactivity"] as $exactivity) {	
										$exactivity["content"] = json_decode($exactivity["content"], true);
								?><li ><?php
									?><span class="cv-addon"></span>
									  <div><span class="education-date"><?=$exactivity["content"]["fromdate"] . " - " . $exactivity["content"]["todate"]?></span>
									  	<?=$exactivity["content"]["position"]?><br><?=$exactivity["content"]["organization"]?>
									  	</div></li><?php
									}
								?>
							</ul><?php
							}
							if (count($member->cvform["achievement"]["skill"]) > 0) {
								foreach ($member->cvform["achievement"]["skill"] as $skill) {
									$skill["content"] = json_decode($skill["content"], true);
									
									if (count($skill["content"]["language"]) > 0){
									?><h4>Language</h4>
									<ul class="list"><?php
										foreach ($skill["content"]["language"] as $language=>$grade) {
										?><li ><span class="cv-addon"></span><div><?=$language?></div></li><?php
										}
									?></ul><?
								  	}

								  	if (count($skill["content"]["computer"]) > 0){
								?><h4>Computer Skills</h4>
									<ul class="list"><?php
										foreach ($skill["content"]["computer"] as $computer=>$grade) {
									?><li ><span class="cv-addon"></span><div><?=$computer?></div></li><?php 
										}
								  ?></ul><?php
								  	}
								}

							}
							?>

						</div>
						<div id="other" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["other"]) > 0) {
							?><h4>Other Information</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["other"] as $other) {	
										$other["content"] = json_decode($other["content"], true);
								?><li ><?php
									?><span class="cv-addon"></span>
									  <div><?=html_entity_decode($other["content"]["html"], ENT_QUOTES, "UTF-8"); ?></div></li><?php
									}
								?>
							</ul><?php
							}
							?>
						</div>
					</div>
					<div class=" text-center saveRow">
						<div class="col-md-12">
							<a href="choose_content.php"><button type="submit" class="btn btn-default" >Create Personal CV</button></a>
							<a href="joblist.php"><button type="submit" class="btn btn-default" >WIE Application</button></a>
						</div>
					</div>
				</div>
				
			</section>
		</div>
	</div>
	<link href="/css/select2.min.css" rel="stylesheet" />
	<script src="/js/select2.min.js"></script>
	<script src="scripts/cvform.js"></script>
</main>
<?php
	include_once("footer.php");
	ob_end_flush();
?>