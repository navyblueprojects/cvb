<?php
	ob_start();

	require_once("config.inc.php");
	include_once("classes/system.php");
	include_once("header.php");

	$pageName = ["Choose Templates"];
	$id = base64_decode($_GET["id"]);
	include_once("breadcrumb.php");

	$member->getCVFormByHash($id);
	
?><main id="choose_content">
	<div class="wrapper">
		<div class="row">
			<ul class="nav nav-step hidden-xs">
				<li>Application Form</li>
				<li class="active">Templates</li>
				<li>Preview & Save</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation" class="disabled"><a>Application Form</a></li>
				<li role="presentation" class="active"><a>Templates</a></li>
				<li role="presentation" class="disabled"><a>Preview & Save</a></li>
			</ul>
		</div>
	</div>
	<div class="container">
		<h1 class="page_header">Choose Template</h1>
		<section id="cv_wrapper" class="col-md-12">
			<div class="row">
				<ul class="nav  nav-stacked col-md-2">
					<li class="active" ><a data-toggle="tab" href="#template1" data-tab="template1"><i class="fas fa-caret-right"></i> Template 1</a></li>
					<li  ><a data-toggle="tab" href="#template2" data-tab="template2"><i class="fas fa-caret-right"></i> Template 2</a></li>
					<li  ><a data-toggle="tab" href="#template3" data-tab="template3"><i class="fas fa-caret-right"></i> Template 3</a></li>
					<li  ><a data-toggle="tab" href="#template4" data-tab="template4"><i class="fas fa-caret-right"></i> Template 4</a></li>
					<li  ><a data-toggle="tab" href="#template5" data-tab="template5"><i class="fas fa-caret-right"></i> Template 5</a></li>
					
				</ul>
				<div class="col-md-10 col-sm-12 tab-content">
					<div id="template1" class="tab-pane fade in active" data-tabid="1">
						<h3><i class="far fa-window-maximize"></i> Template 1</h3>
						<iframe src="cvtemplate/cvtemplate1.htm"></iframe>
					</div>
					<div id="template2" class="tab-pane fade in " data-tabid="2">
						<h3><i class="far fa-window-maximize"></i> Template 2</h3>
						<iframe src="cvtemplate/cvtemplate2.htm"></iframe>
					</div>
					<div id="template3" class="tab-pane fade in " data-tabid="3">
						<h3><i class="far fa-window-maximize"></i> Template 3</h3>
						<iframe src="cvtemplate/cvtemplate3.htm"></iframe>
					</div>
					<div id="template4" class="tab-pane fade in " data-tabid="4">
						<h3><i class="far fa-window-maximize"></i> Template 4</h3>
						<iframe src="cvtemplate/cvtemplate4.htm"></iframe>
					</div>
					<div id="template5" class="tab-pane fade in " data-tabid="5">
						<h3><i class="far fa-window-maximize"></i> Template 5</h3>
						<iframe src="cvtemplate/cvtemplate5.htm"></iframe>
					</div>
				</div>
				<div class="saveRow">
					<div class="col-md-12">
						<a href="choose_content"  class="btn btn-default pull-left" >Back</a>
						<a class="btn btn-default btn-goPreview pull-right" onclick="goPreview('<?=trim(base64_encode($id), "=")?>')" >Create My CV</a>
					</div>
				</div>
			</div>
		</section>
	</div>
	<link href="/css/select2.min.css" rel="stylesheet" />
	<script src="/js/select2.min.js"></script>
	<script src="scripts/cvform.js"></script>
	<script >
		function goPreview(id){
			window.location = '<?=$appPath?>/preview/cv'+id+'?t='+$('.tab-pane.active').attr('data-tabid');
		}
	</script>
	
</main>
<?php
	include_once("footer.php");
	ob_end_flush();
?>