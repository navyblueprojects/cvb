<?php
	
	
?><div class="wrapper text-right">
	<div class="row">
		<div class="col-xs-6"><p class="breadcrumbs" >
			<span typeof="v:Breadcrumb">
				<a rel="v:url" property="v:title" href="home">Home</a>
			</span>
			<?php
				foreach ($pageName as $url => $pName) {
					?><span class="delimiter">&gt;</span><?
					echo ($url != "") ? ' <a href="'.$url.'">'.$pName.'</a> ' : ' <span >'.$pName.'</span> '; 
				}
			?>
		</p></div>
		<div class="col-xs-6 text-right">
			<?php
			if ($member->isLogined()) {
			?><p class="member_wrapper">Hello <?=$member->getName()?>! <a href="logout">Logout</a></p><?
			}
		?></div>
	</div>

</div>
