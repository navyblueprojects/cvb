<!DOCTYPE html>
<html lang="xmlns">
<head>
<script src="/js/pdf.min.js"></script>
</head>
<body bgcolor="#808080">

<canvas id="the-canvas" width="100%"></canvas>	
<script type="text/javascript">
	// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = '<?=$_GET["f"]?>';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '/js/pdf.worker.min.js';

// Asynchronous download of PDF
var loadingTask = pdfjsLib.getDocument(url);
loadingTask.promise.then(function(pdf) {
    
  // Fetch the first page
  var pageNumber = 1;
  pdf.getPage(pageNumber).then(function(page) {
    
    var desiredWidth = window.innerWidth;
	var viewport = page.getViewport({ scale: 1.05, });
	var scale = desiredWidth / viewport.width;
	var scaledViewport = page.getViewport({ scale: scale, });



    // Prepare canvas using PDF page dimensions
    var canvas = document.getElementById('the-canvas');
    var context = canvas.getContext('2d');
    canvas.height = scaledViewport.height;
    canvas.width = scaledViewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: context,
      viewport: scaledViewport
    };
    var renderTask = page.render(renderContext);
    renderTask.promise.then(function () {
      console.log('Page rendered');
    });
  });
}, function (reason) {
  // PDF loading error
  console.error(reason);
});
</script>
</body>
</html>
