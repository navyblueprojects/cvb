<?php
	include_once("config.inc.php");
	session_start();
	if(isset($_SESSION[$dbDatabase])){
		$loginFlag = array_key_exists("loginName", $_SESSION[$dbDatabase]) ? true : false;
	}else{
		$loginFlag = false;
	}
	session_write_close();
	
	if ($loginFlag ) {
		header("location: admin/page/page_edit.php?tag=".$_GET["tag"]);
	 	exit;
	} else {
		header('HTTP/1.0 403 Forbidden');
		exit();
	}
