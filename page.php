<?php
	ob_start();
	require_once("config.inc.php");

	include_once("header.php");
	
	$page_id = $_GET["p"];
	
	$rr = explode(".", $_GET["p"]);
	if (count($rr) > 1) {
		$page_id = $rr[0];
	}


	$page = $cmsObj->getPage($page_id);

	if ($page == NULL) {
		header("location: ./");
		exit();
	}

	$pageName = array($page["pageName"]);
	include_once("breadcrumb.php");
?>
	<main id="page">
		<div >
			<div class="container">
				<h1 class="page_header"><?=$page["pageName"]?> </h1>
				<section id="options_wrapper" class="col-md-12 ">
					<div class="page_content cms"><?php 
						echo html_entity_decode($page["content"], ENT_QUOTES, "UTF-8");
					?>
				</section>
				
			</div>
		</div>
	</main>
<?php
	include_once("footer.php");
	ob_end_flush();
?>