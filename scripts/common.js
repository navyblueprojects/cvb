function displayError(obj, formatText){
	
	if (typeof(formatText) == 'undefined') {
		formatText = 'data-error-placeholder';
	}
	
	$(obj).val('').addClass('error')
		.attr('data-origin-placeholder', $(obj).attr('placeholder'))
		.attr('placeholder', $(obj).attr(formatText));
}

function resetError(obj){	
	$(obj).removeClass('error').attr('placeholder', $(obj).attr('data-origin-placeholder'));
}

var checkMail = function(email) {
	var x = email;
	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (filter.test(x)) return true;
	else return false;
}




$(function(){
	$('#frmLogin').on('submit', function(){
		var data = $(this).serializeArray();
		data.push({'name':'action', 'value':'login'});
		$.ajax({
			url: '../xhr/member',
			data: data,
			type: 'POST',
			dataType: 'JSON'
		}).success(function(data){
			window.location = "member";
		});
		return false;
	});

	
});
