$(function(){
	$(".select2[data-name='std.yrofstudy']").select2({
		multiple: true,
		data: [
			{id: 1, text: "Year 1"},
			{id: 2, text: "Year 2"},
			{id: 3, text: "Year 3"},
			{id: 4, text: "Year 4"},
		]
	});

	$(".select2[data-name='jobs.job_no']").select2({
		minimumInputLength: 1,
		tags: true,
		tokenSeparators: [",", " "],
		ajax: {
		    url: '../xhr/report',
		    dataType: 'json',
		    data: function(params){
		    	return {
		    		q: params.term,
		    		field : this.attr('data-name')
		    	};
		    }
		},
	});


	$('.select2[data-field]').select2({
		ajax: {
		    url: '../xhr/report',
		    dataType: 'json',
		    data: function(params){
		    	return {
		    		q: params.term,
		    		field : this.attr('data-field')
		    	};
		    }
		},
		multiple: true,
		minimumInputLength: 2
	});

	$('.date-range').daterangepicker({
		autoUpdateInput: false,
		locale: {
			format: 'YYYY-MM-DD HH:mm'
		}
	});
	$('.date-range').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
	}).on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});



	$('#applyfilter').on('click', function(){

		var form = $('#jobs_panel').find('form');
		formdata = [];

		$.each(form.find('[data-name]'), function(idx, obj){

			if (typeof($(obj).val()) == "string") {
				form.append('<input type="hidden" name="search['+$(obj).attr('data-name')+']" value="'+$(obj).val()+'">');
			} else {
				$.each($(obj).val(), function(i, o){
					form.append('<input type="hidden" name="search['+$(obj).attr('data-name')+'][]" value="'+o+'">');
				});
			}
		});

		form[0].method="post";
		form[0].submit();
	});

	$('#exportExcel').on('click', function(){
		if ($('input:checked').length == 0) {
			alert("Please choose a record to export");
			return false;
		} else {
			$('#download')[0].action = 'download?exportExcel';
		}
	});

	$('#downloadCV').on('click', function(){
		if ($('input:checked').length == 0) {
			alert("Please choose a record to download");
			return false;
		} else {
			$('#download')[0].action = 'download';
		}
	});

	$('#download').on('submit', function(e){
		e.preventDefault();	

		var exp = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{6,16}$/;

		if (exp.test($('#password').val())) {
		    this.submit(); 	
		    $('#password').val(''); 
		    $('#download .modal').modal('hide');
		} else {
			$('#errorModalDownload').removeClass('hide');
			$('#password').addClass('error');
		}
		return false;
	});

	$('body').on('click', '#selectall', function(){
		var checked = $('#selectall').prop('checked');
		$('input[type=checkbox]').prop('checked', checked);
	}).on('click', 'input[type=checkbox]:not(#selectall)', function(e){
		if (!e.target.checked) {
			$('#selectall').prop('checked', false);
		}
	});
});