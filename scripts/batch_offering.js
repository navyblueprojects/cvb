$(function(){
	$('.select2').select2({
		ajax: {
		    url: '../xhr/report',
		    dataType: 'json',
		    data: function(params){
		    	return {
		    		q: params.term,
		    		field : this.attr('data-field')
		    	};
		    }
		},
		multiple: true,
		minimumInputLength: 2

	});

	$('body').on('change', 'select.offerAppl', function(e){
		if (confirm(' Are you sure to continue?')) {				
			$.ajax({
				url: '../xhr/wie',
				data: {action: 'offerApplication', item_id : $(this).attr('data-id'), status: $(this).val()},
				type: 'POST',
				dataType: 'JSON'
			}).done(function(){
				window.location.reload();
			})
		}
	}).on('click', '.btn-active[data-status]', function(){
		$(this).parent().find('.btn[data-status]').removeClass('btn-selected').addClass('btn-active');
		$(this).removeClass('btn-active').addClass('btn-selected');
		$('#unsave-warning').removeClass('hide');
	});
});

function offerAll() {
	$('.btn[data-status=offered]').addClass('btn-selected').removeClass('btn-active');
	$('.btn[data-status=closed]').addClass('btn-active').removeClass('btn-selected');
	$('#unsave-warning').removeClass('hide');
}


function rejectAll() {
	$('.btn[data-status=closed]').addClass('btn-selected').removeClass('btn-active');
	$('.btn[data-status=offered]').addClass('btn-active').removeClass('btn-selected');
	$('#unsave-warning').removeClass('hide');
}

function revertAll(){
	$('.btn[data-status]').each((idx, obj)=>{
		$(obj).removeClass('btn-selected btn-active').addClass($(obj).attr('data-class'));
	});
	$('#unsave-warning').addClass('hide');
}

function batch_offering(){
	if (confirm('Are you sure to continue?')) {
		$('.btn-selected[data-class=btn-active]').each((idx, obj) =>{
			$('#batch_offering').append('<input type="hidden" name="status['+$(obj).attr('data-id')+']" value="'+$(obj).attr('data-status')+'">');
		});
	}
	return true;
}