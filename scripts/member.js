$(function(){
	$('#loginform').on('submit', function(){
		
		$('#loginform_wrapper>div').find('.alertLogin').remove();
        
        if ($('#captcha').val().length != 4) {
            var alert = $('.alertLogin').clone();
				alert.find('.errorMsg').html("Please enter the captcha code");
				alert.removeClass('hide');
				$('#loginform_wrapper>div').prepend(alert);
            return false;
        }
        
        var data = $(this).serializeArray();
		
		data.push({name: 'action', value: 'login'});
		$.ajax({
			url: 'xhr/student',
			method: 'POST',
			data: data,
			dataType: 'JSON'
		}).done(function(data){
			if (data.status == 1) {

				//window.open('saml/module.php/core/authenticate.php?as=default-sp', '_newWindow');
				window.location = 'sso_login';
			} else {
                $('#img_captcha').attr('src', 'captcha?' + new Date().getTime());
				var alert = $('.alertLogin').clone();
				alert.find('.errorMsg').html(data.msg);
				alert.removeClass('hide');
				$('#loginform_wrapper>div').prepend(alert);
			}
		});
		
		return false;
	});

	var countDownInterval = false;
	var countDownRedirect = function() {
		var countdown = parseInt($('#loginReminderModal .count').html());
		
		countdown = countdown - 1;

		if (countdown < 0) {
			window.location = 'login';
		} else {
			$('#loginReminderModal .count').html(countdown);
		}
	};

	$('#loginReminderModal').on('shown.bs.modal', ()=>{
		countDownInterval = setInterval(countDownRedirect, 1000);
	}).on('hidden.bs.modal', ()=>{
		$('#loginReminderModal .count').html("15");
		clearInterval(countDownInterval);
	});


	if (typeof(viewCms) != 'undefined') {
        if (viewCms) 
            $('[data-cms-title]').addClass('cmsView');
	}

	$('body').on('click', '.cmsView', function(e){
		var data = $(this);
		window.open('admin/page/page_edit?tag='+data.attr('data-cms-title'));
	}).on('click', '#reloadCaptcha', function(e){
        $('#img_captcha').attr('src', 'captcha?' + new Date().getTime());
    }).on('keyup', '#captcha', function(e){
        $(e.target).val($(e.target).val().toUpperCase());
    });

	$('a[data-font-size]').on('click', function(){
		$('body').removeClass().addClass($(this).attr('data-font-size'));
	});

	if (navigator.userAgent.indexOf(" MSIE ") !== -1) {
	
		$('.notIE').hide();
		$('.forIE').show();
	} else {
		
		$('.notIE').show();
		$('.forIE').hide();
	} 

	$('.cms').find('span').each(function (idx, obj) {
		if (obj.style.removeProperty) {
		    obj.style.removeProperty('font-size');
		} else {
		    obj.style.removeAttribute('font-size');
		}
	});


	keepAlive();
	setInterval("keepAlive()", 15000);
	
});

function keepAlive(){
	$.ajax({
		url: './ka-'+Date.now(),
		method: 'GET'
	});
}
