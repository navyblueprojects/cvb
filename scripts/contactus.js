$(function(){
	$('#contactus').on('submit', function(){
		var data = $(this).serializeArray();
		var valid = true;
		
		$('#contactus').find('input.req').each(function(idx, obj){
			resetError(obj);
			if ($.trim($(obj).val()) == ""){				
				displayError(obj );
				valid = false;
			}
		});
		
		if (valid) {		
			data.push({name: 'action', value: 'send'});					
			$.ajax({
				url: '../xhr/contactus',
				data: data,
				dataType: 'JSON',
				type: 'POST'
			}).done(function(data){
				window.location = 'success'
			});	
		}			
		return false;
	});
}); 