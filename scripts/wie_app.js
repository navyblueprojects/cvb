$(function(){

	
	$('#goAddJob, .goAddJob').on('click', function(){
		var jobid = $(this).attr('data-id');
		

		data = [];

		data.push({"name":"action", "value":"add"});
		data.push({"name":"id", "value": jobid});
		

		$.ajax({
			url: "xhr/job",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			if (data.status == 1) {
				window.location="job_preferences";
			} else {
				$('#errMsg').text(data.msg);
				$('#errorModal').modal('show');
			}
		});
	});

	$("#job_preference .btn-up, #job_preference .btn-down").on('click', function(){
		var  itemid = $(this).attr('data-id');
		var priority = $(this).attr('data-priority');
		
		data = [];

		data.push({"name":"action", "value":"order"});
		data.push({"name":"id", "value": itemid});
		data.push({"name":"priority", "value": priority});
		

		$.ajax({
			url: "xhr/job",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			if (data.status == 1) {
				window.location.reload();
			} else {
				$('#errMsg').text(data.msg);
				$('#errorModal').modal('show');
			}
		});
	});

	$('.btnDelete').on('click', function(){
		data = [];
		data.push({"name":"action", "value":"remove"});
		data.push({"name":"id", "value": $(this).attr('data-id')});
	
		$.ajax({
			url: "xhr/job",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			if (data.status == 1) {
				window.location.reload();
			} else {
				$('#errMsg').text(data.msg);
				$('#errorModal').modal('show');
			}
		});
	});

	$('#job_selfassess .saveRow a').on('click', function(){
		if ($('.agree:not(:checked)').length > 0) {
			alert('Please complete your self assessment form');
			return false;
		} else {
			data = [];
			data.push({"name":"action", "value":"selfassess"});
			data.push({"name":"selfassess", "value": $('#selfassess').val()});
		

			$.ajax({
				url: "xhr/job",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				if (data.status == 1) {
					window.location = 'job_preview_application';
				} else {
					$('#errorModal').modal('show');
				}				
			});
		}
	});

	$('.btn-submit-app').on('click', function(){
		$( "#dialog-confirm" ).dialog({
			resizable: false,
			height: "auto",
			width: 400,
			modal: true,
			buttons: {
				"Yes": function() {
					
					data = [];
					data.push({"name":"action", "value":"submit"});

					$.ajax({
						url: "xhr/job",
						data: data,
						dataType: "JSON",
						method: "POST"
					}).done(function(data){
						if (data.status == 1) {
							window.location = 'job_complete';
						} else {
							$('#errorModal').modal('show');
						}
						
					});
					$( this ).dialog( "close" );
					
				},
				"No": function() {
					$( this ).dialog( "close" );
				}
			}
		});

		
		
	});


	$('#job_selfassess #joblist_wrapper li').prepend('<input type="checkbox" name="agree[]" class="agree" />&nbsp;');

	
	



});