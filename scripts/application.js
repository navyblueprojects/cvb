$(function(){
	$('.select2').select2({
		ajax: {
			url: '../xhr/report',
			dataType: 'json',
			data: function(params){
				return {
					q: params.term,
					field : this.attr('data-field')
				};
			}
		},
		//multiple: true,
		minimumInputLength: 2
	});

	$('#student_id').on('select2:select', function(){
		$.ajax({
			url: '../xhr/wie',
			type: 'POST',
			dataType: 'json',
			data: {
				student_id: $('#student_id').val(),
				action: 'getApplicationByStudent'
			}
		}).done(function(data){
			$('#appl_list').empty();
			$.each(data, function(idx, obj) {
				$('#appl_list').append('<li ><a data-id="'+obj.value+'">'+obj.label+'</a></li>').removeClass('hide');
			});
		});
	});

	$('#appl_list').on('click', 'a', function(e){

	
		var newOption = new Option($(e.target).attr('data-id'), $(e.target).attr('data-id'), true, true);

		$('#search_wie').append(newOption).trigger('change');

		$('#search_wie')[0].form.submit();
	})
});