var validateObject = {
	"firstname_en" : {"msg" : "Please enter your english first name", "panel":"basic"}, 
	"lastname_en" : {"msg" : "Please enter your english last name", "panel":"basic"},
	"firstname_zh" : {"msg" : "Please enter your first name", "panel":"basic"},
	"lastname_zh" : {"msg" : "Please enter your last name", "panel":"basic"},
	"email" : {"msg" : "Please enter your email address", "invalid" : "Invalid email address format", "panel":"basic"}, 
	"contact" : {"msg":"Please enter your contact number", "invalid" : "Invalid contact number format", "panel":"basic"},
	"address1" : {"msg":"Please enter your address", "panel":"basic"},
	"age" : {"msg":"Please enter your age", "panel":"basic"},
	"hkid" : {"msg":"Please enter your hkid", "validate":"Invalid ID format, Please enter again", "panel":"basic"},
	"dob" : {"msg":"Please enter your date of birth", "panel":"basic"},
	"em_name" : {"msg":"Please enter the first name of emergency contact person", "panel":"basic"},
	"em_name2" : {"msg":"Please enter the last name of emergency contact person", "panel":"basic"},
	"em_relation" : {"msg":"Please enter your relationship with emergency contact person", "panel":"basic"},
	"em_phone" : {"msg":"Please enter the phone number of emergency contact person", "invalid" : "Invalid phone number format of emergency contact person", "panel":"basic"},
	"em_contact" : {"msg":"Please enter other contact number of emergency contact person", "invalid" : "Invalid contact number format of emergency contact person", "panel":"basic"},

	"availability" : {
		"msg":"Please enter your availability", 
		"panel":"interview"
	},
	"expected_salary" : {
		"msg":"Please enter your expected salary", 
		"panel":"interview"
	},
	"skill_computer" : {
		"msg":"Please complete the technical skills rating", 
		"panel":"skill",
		"tabpanel":"skills2"
	},
	"skill_language" : {
		"msg":"Please complete the language skills rating", 
		"panel":"skill",
		"tabpanel":"skills1"
	},    
	"photo" : {
		"msg":"Please choose your photo to upload", 
		"panel":"basic"
	},
	"propose_interview_date" : {"msg":"Please choose a proposed interview date", "panel":"interview"},
	"modalEducation_list" : {
		"msg":"Please enter at least one record of education", 
		"panel":"education", 
		"tabpanel":"education1"
	},
	"gpa_point" : {
		"msg":"Please enter your CGPA ", 
		"panel":"education", 
		"tabpanel":"education1"
	},
	"gpa_date" : {
		"msg":"Please enter the obtain date of your CGPA", 
		"panel":"education", 
		"tabpanel":"education1"
	},
	"cgpa_point" : {
		"msg":"Please enter your CGPA of your pre-associate degree or check N/A beside", 
		"panel":"education", 
		"tabpanel":"education1"
	},
	"cgpa_date" : {
		"msg":"Please enter the obtain date of your CGPA or check N/A beside", 
		"panel":"education", 
		"tabpanel":"education1"
	},
	"cgpa_college" : {
		"msg":"Please enter the obtain college of your pre-associate degree or check N/A beside", 
		"panel":"education", 
		"tabpanel":"education1"
	},
	"modalExam_list" : {
		"msg":"Please enter at least one record of DSE/HKAL/HKCE Results", 
		"panel":"education", 
		"tabpanel":"education2"
	},
	"modalPublicExam_list" : {
		"msg":"Please enter at least one record of other public exam or check N/A beside", 
		"panel":"education", 
		"tabpanel":"education2"
	},
	"modalExperience_list": {
		"msg":"Please enter at least one record of work experience or check N/A beside", 
		"panel":"experience", 
		"tabpanel":"experience1"
	},
	"modalOthExperience_list": {
		"msg":"Please enter at least one record of internship experience or check N/A beside", 
		"panel":"experience", 
		"tabpanel":"experience2"	
	},
	"modalProfQuali_list" : {
		"msg":"Please enter at least one record of professional qualification", 
		"panel":"qualification",
		"tabpanel":"qualification2"
	},
	"modalEducation" : {
		"frmedu_fromdate" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmedu_todate" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date"},
		"frmedu_school" : {"empty": "Please enter the name of school / college"},
		"frmedu_class" : {"empty": "Please enter the class name / course name of study"},
		"frmedu_year" : {"empty": "Please enter the study year"},
	},
	"modalEducation_edit" : {
		"frmedu_fromdate_edit" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmedu_todate_edit" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date"},
		"frmedu_school_edit" : {"empty": "Please enter the name of school / college"},
		"frmedu_class_edit" : {"empty": "Please enter the class name / course name of study"},
		"frmedu_year_edit" : {"empty": "Please enter the study year"},
	},
	"modalPublicExam" : {
		"frmpe_name" : {"empty": "Please enter the examination name"},
		"frmpe_organization" : {"empty": "Please enter the name of issuing organization"},
		"frmpe_date" : {"empty": "Please enter the issue date", "date-invalid": "Invalid date"},
		"frmpe_grade" : {"empty": "Please enter the grade or level obtained"},
	},
	"modalPublicExam_edit" : {
		"frmpe_name_edit" : {"empty": "Please enter the examination name"},
		"frmpe_organization_edit" : {"empty": "Please enter the name of issuing organization"},
		"frmpe_date_edit" : {"empty": "Please enter the issue date", "date-invalid": "Invalid date"},
		"frmpe_grade_edit" : {"empty": "Please enter the grade or level obtained"},
	},
	"modalProfQuali" : {
		"frmpq_name" : {"empty": "Please enter the examination / certificate name"},
		"frmpq_organization" : {"empty": "Please enter the name of issuing organization"},
		"frmpq_date" : {"empty": "Please enter the issue date", "date-invalid": "Invalid date"},
		"frmpq_grade" : {"empty": "Please enter the grade or level obtained"},
	},
	"modalProfQuali_edit" : {
		"frmpq_name_edit" : {"empty": "Please enter the examination / certificate name"},
		"frmpq_organization_edit" : {"empty": "Please enter the name of issuing organization"},
		"frmpq_date_edit" : {"empty": "Please enter the issue date", "date-invalid": "Invalid date"},
		"frmpq_grade_edit" : {"empty": "Please enter the grade or level obtained"},
	},
	"modalOthQuali" : {
		"frmoq_name" : {"empty": "Please enter the examination / certificate name"},
		"frmoq_organization" : {"empty": "Please enter the name of issuing organization"},
		"frmoq_date" : {"empty": "Please enter the issue date"},
		"frmoq_grade" : {"empty": "Please enter the grade or level obtained"},
	},
	"modalOthQuali_edit" : {
		"frmoq_name_edit" : {"empty": "Please enter the examination / certificate name"},
		"frmoq_organization_edit" : {"empty": "Please enter the name of issuing organization"},
		"frmoq_date_edit" : {"empty": "Please enter the issue date"},
		"frmoq_grade_edit" : {"empty": "Please enter the grade or level obtained"},
	},
	"modalExperience" : {
		"frmexp_fromdate" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmexp_todate" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date"},
		"frmexp_organization" : {"empty": "Please enter the name of organization"},
		"frmexp_position" : {"empty": "Please enter the position name"},
		"frmexp_duties" : {"empty": "Please describe the major duties"}
	},
	"modalExperience_edit" : {
		"frmexp_fromdate_edit" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmexp_todate_edit" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date" },
		"frmexp_organization_edit" : {"empty": "Please enter the name of organization"},
		"frmexp_position_edit" : {"empty": "Please enter the position name"},
		"frmexp_duties_edit" : {"empty": "Please describe the major duties"}
	},
	"modalOthExperience" : {
		"frmoexp_fromdate" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmoexp_todate" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date"},
		"frmoexp_organization" : {"empty": "Please enter the name of organization"},
		"frmoexp_position" : {"empty": "Please enter the position name"},
		"frmoexp_duties" : {"empty": "Please describe the major duties"}
	},
	"modalOthExperience_edit" : {
		"frmoexp_fromdate_edit" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmoexp_todate_edit" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date" },
		"frmoexp_organization_edit" : {"empty": "Please enter the name of organization"},
		"frmoexp_position_edit" : {"empty": "Please enter the position name"},
		"frmoexp_duties_edit" : {"empty": "Please describe the major duties"}
	},
	"modalExActivity" : {
		"frmexact_fromdate" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmexact_todate" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date"},
		"frmexact_organization" : {"empty": "Please enter the name of organization"},
		"frmexact_position" : {"empty": "Please enter the position name"},
		"frmexact_duties" : {"empty": "Please describe the major duties"}
	},
	"modalExActivity_edit" : {
		"frmexact_fromdate_edit" : {"empty": "Please enter the start date", "date-invalid-from": "Invalid start and end date"},
		"frmexact_todate_edit" : {"empty": "Please enter the end date", "date-invalid-to": "Invalid start and end date" },
		"frmexact_organization_edit" : {"empty": "Please enter the name of organization"},
		"frmexact_position_edit" : {"empty": "Please enter the position name"},
		"frmexact_duties_edit" : {"empty": "Please describe the major duties"}
	},
	"modalReference" : {
		"frmref_name" : {"empty": "Please enter the name of the referee"},
		"frmref_position" : {"empty": "Please enter the position of the referee"},
		"frmref_organization" : {"empty": "Please enter the organization name of the referee"},
		"frmref_contact" : {"empty": "Please enter the contact", "invalid" : "Invalid phone number format of the referee"},
		"frmref_email" : {"empty": "Please enter the email address of the referee", "invalid" : "Invalid email format of the referee"}
	},
	"modalReference_edit" : {
		"frmref_name_edit" : {"empty": "Please enter the name of the referee"},
		"frmref_position_edit" : {"empty": "Please enter the position of the referee"},
		"frmref_organization_edit" : {"empty": "Please enter the organization name of the referee"},
		"frmref_contact_edit" : {"empty": "Please enter the contact", "invalid" : "Invalid phone number format of the referee" },
		"frmref_email_edit" : {"empty": "Please enter the email address of the referee", "invalid" : "Invalid email format of the referee"}
	},
	"modalAchievement" : {
		"frmachv_name" : {"empty": "Please enter the name of scholarship or awards"},
		"frmachv_organization" : {"empty": "Please enter the name of issuing organization"},
		"frmachv_date" : {"empty": "Please enter the obtain date"}
	},
	"modalAchievement_edit" : {
		"frmachv_name_edit" : {"empty": "Please enter the name of scholarship or awards"},
		"frmachv_organization_edit" : {"empty": "Please enter the name of issuing organization"},
		"frmachv_date_edit" : {"empty": "Please enter the obtain date"}
	},
	"modalOthQuali_list" : {"msg":"Please enter at least one record of other qualification", "panel":"qualification"},
	"modalAchievement_list" : {"msg":"Please enter at least one record of achievement", "panel":"achievement"},
	"modalExActivity_list" : {"msg":"Please enter at least one record of extra-curriculum activity", "panel":"experience"},
	"frmSendCV": {
		"modalEmail_1" : {"empty": "Please enter recipient email address", "invalid": "Invalid Email Address. Please enter again."},
		"modalSubject_1" : {"empty": "Please enter the email subject"},
		"content_1" : {"empty": "Please enter the content"},
	},
	"frmSendFSCC": {
		"modalEmail_2" : {"empty": "Please enter recipient email address", "invalid": "Invalid Email Address. Please enter again."},
		"modalSubject_2" : {"empty": "Please enter the email subject"},
		"content_2" : {"empty": "Please enter the content"},
	}
};
var editControl = false;

var editFlag = false;

var monthpicker_settingobject = {
	timepicker: false,
	className: 'monthpicker_modal',
	format: 'M Y',
	minDate: '2000/01/01',
	maxDate: new Date(),
	yearEnd: new Date().getUTCFullYear(),
	allowTimes: false, 
	onClose: function(dateObj, inst){
		$(inst).removeClass('error'); //.val(moment(dateObj).format('MMM YYYY'));
		if (dateObj > new Date()) {
			$(inst).addClass('error');
		}
		$(inst).datetimepicker('destroy');
		$(inst).datetimepicker(monthpicker_settingobject);
	},
	onChangeMonth: function(dateObj, inst){
		$(inst).val(moment(dateObj).format('MMM YYYY'));
	}
};

$(function(){
	if (navigator.userAgent.indexOf("iPhone") > 0 || navigator.userAgent.match(/Android/i) ) {
		if ($('.ckeditor').next('.strip_html').length > 0) {
			var html = $('.ckeditor').next('.strip_html').html();
			$('.ckeditor').val(html);
		}
		$('.ckeditor').removeClass('ckeditor');


		if ($('#previewWindow').length > 0) {
			if ($('#previewWindow').attr('data-generate')) {
				$('#previewWindow').attr('src', $('#previewWindow').attr('data-generate'));
			}
		}
	}

	if ($('.select2.district').length > 0) {
		$('.select2.district').select2({
			ajax: {
				url: 'xhr/district',
				dataType: 'json'
				// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
			}
		});
	}
	
	$('button[data-save]').on('click', function(){
		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}
		if (typeof($(this).attr('data-validate')) != 'undefined') {
			var validate = $(this).attr('data-validate');			
			if (!eval(validate)) {
				return false;
			}
		}
		var formid = $(this).attr('data-save');
		var data = ($(formid).find('.form').serializeArray());

		data.push({"name":"action", "value":formid});
		data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
		data.push({"name":"template", "value": $(formid+'_template').html()});
		
		var tmpSkillValue = {};

		if (formid == '#modalOtherSkill' || formid == '#modalOtherSkill_edit') {
			$(formid).find('.form').serializeArray().map((datafield)=>{
				tmpSkillValue[datafield.name] = datafield.value;
			})
		}

		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			$(formid).find('form')[0].reset();

			var na_id = formid.replace('#', '#na_');
			if ($(na_id).length > 0) {
				$(na_id).prop('checked', false);
				$(na_id).parent().addClass('hide');
			}

			if (formid == '#modalExam') {
				$(formid).find(".exam_ul").addClass("hide");
				$(formid).find("li:not(.compulsory)").remove();
			}

			if (formid == '#modalOtherSkill' || formid == '#modalOtherSkill_edit') {
				var meta_id = $(data.html).attr('data-id');
				skill_other[meta_id] = tmpSkillValue;
			}
			$('#public_exam_grades').html($('#public_exam_grades').find('.list-group-item.hide').prop('outerHTML'));

			$('.modal.in').modal('hide');
			$(formid+"_list").append($(data.html));
		});
	});



	$('button[data-update]').on('click', function() {

		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}

		if (typeof($(this).attr('data-validate')) != 'undefined') {
			
			var validate = $(this).attr('data-validate');
			
			if (!eval(validate)) {
				return false;
			}
		}
		var formid = $(this).attr('data-update');
		var mid = $(this).attr('data-mid');

		var data = ($(formid+'_edit').find('.form').serializeArray());

		data.push({"name":"action", "value":formid});
		data.push({"name":"update", "value":mid});
		data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
		data.push({"name":"template", "value": $(formid+'_template').html()});

		var tmpSkillValue = {};

		if (formid == '#modalOtherSkill' || formid == '#modalOtherSkill_edit') {
			$(formid+'_edit').find('.form').serializeArray().map((datafield)=>{
				tmpSkillValue[datafield.name] = datafield.value;
			})
		}

		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			$(formid+'_edit').find('form')[0].reset();

			if (formid == '#modalExam') {
				$(formid+'_edit').find("ul").empty();
			}
			if (formid == '#modalOtherSkill' || formid == '#modalOtherSkill_edit') {
				skill_other[mid] = tmpSkillValue;
			}


			$('.modal.in').modal('hide');
			$('li[data-id='+mid+']').replaceWith(data.html);
		});
	});

	$('[data-tid]').on('focus', function(e){
		var data = new Array();
		var pid = $(e.target).attr('data-tid');

		data.push({"name":"tag", "value": pid});
		data.push({"name":"action", "value": 'tips'});

		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			$('.tips_content').attr('data-cms-title', pid).hide().html(data.html).fadeIn();
			if (data.html != "") {
				$('.tips-mobile').addClass('show');
			} else {
				$('.tips-mobile').removeClass('show');
			}
			if (viewCms) {
				$('[data-cms-title]').addClass('cmsView');
			}
		});
	});
	$('a[data-tid]').on('mouseenter', function(e){
		var data = new Array();
		var pid = $(e.target).attr('data-tid');

		data.push({"name":"tag", "value": pid});
		data.push({"name":"action", "value": 'tips'});

		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			$('.tips_content').attr('data-cms-title', pid).hide().html(data.html).fadeIn();
			if (viewCms) {
				$('[data-cms-title]').addClass('cmsView');
			}
		});
	});

	$('#district, #work_district').on('select2:opening', function(e){
		var data = new Array();
		var pid = $(e.target).attr('id');
		
		data.push({"name":"tag", "value": pid});
		data.push({"name":"action", "value": 'tips'});
		
		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			$('.tips_content').attr('data-cms-title', pid).hide().html(data.html).fadeIn();
			if (viewCms) {
				$('[data-cms-title]').addClass('cmsView');
			}
		});
	});

	$('#add_language').on('click', function(e){

		e.preventDefault();

		var obj = $('.language_skill_list')[0];
		var new_lang = $('#new_language').val();

		if (new_lang != "") {
			$('#new_language').val('');
			var clone = $(obj).clone();

			$(clone).find('h4').text(new_lang).append('<a class="btn btn-delete-language btn-xs" data-language="'+new_lang+'"> Remove <i class="fa fa-times"></i> </a>');
			

			$(clone).find('button.active').removeClass('active');
			$(clone).find('[data-language]').attr('data-language', new_lang);
			//$(clone).find('.grade>button:last-child').addClass('active');

			skill_language[new_lang] = {Written: "", Oral: ""};

			$('#language_list').append($(clone));
		}
	});
	$('#new_language, #new_computer').on('keydown', function(e){
		if (e.keyCode == 189) {
			return false;
		}
	});

	var checkHKID = false;

	$('#hkid').on('keyup', (e) => {
		$('#hkid').removeClass('error');
		if ($('#hkid').val() != '') {
			checkHKID = setTimeout(function(){
				if (!chkHKID($('#hkid'))) {
					$('#hkid').addClass('error');
				} 
			}, 200);
		}
	});



	$('#add_computer').on('click', function(e){
		e.preventDefault();
		var obj = $('.computer_skill_list')[0];
		var new_lang = $('#new_computer').val();

		if (new_lang != "") {
			$('#new_computer').val('');
			var clone = $(obj).clone();

			$(clone).find('h4').text(new_lang).append('<a class="btn btn-delete-skill btn-xs" data-language="'+new_lang+'"> Remove <i class="fa fa-times"></i> </a>');
			
			$(clone).find('button.active').removeClass('active');
			$(clone).find('.grade>button:last-child').addClass('active');

			$(clone).find('[data-computer]').attr('data-computer', new_lang);

			skill_computer[new_lang] = {};

			$('#computer_list').append($(clone));
		}
	});

	$('body').on('click', '.grade button', function(e){
		var obj = e.target;
		$(obj).parent().find('button').removeClass('active');
		skill_language[$(obj).parent().attr('data-language')][$(obj).parent().attr('data-sub')] = $(obj).addClass('active').text();


		skill = {
			"language" : skill_language,
			"computer" : skill_computer
		}

		var data = new Array();
		data.push({"name":"language", "value": JSON.stringify(skill)});
		data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
		data.push({"name":"action", "value": '#modalSkills'});
		
		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			
		});
	}).on('click', '.btn-delete-language', function(e){
		
		if (confirm('Are you sure to remove this record?')) {
			var li = $(e.target).parentsUntil('.language_skill_list').parent();
			li.remove();
			delete skill_language[$(e.target).attr('data-language')];
			
			skill = {
				"language" : skill_language,
				"computer" : skill_computer
			}

			var data = new Array();
			data.push({"name":"language", "value": JSON.stringify(skill)});
			data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
			data.push({"name":"action", "value": '#modalSkills'});
			
			$.ajax({
				url: "xhr/cvform",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				
			});
		}


	}).on('click', '.btn-delete-skill', function(e){
		
		if (confirm('Are you sure to remove this record?')) {
			var li = $(e.target).parentsUntil('.computer_skill_list').parent();
			li.remove();

			delete skill_computer[$(e.target).attr('data-language')];
			
			skill = {
				"language" : skill_language,
				"computer" : skill_computer
			}

			var data = new Array();
			data.push({"name":"language", "value": JSON.stringify(skill)});
			data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
			data.push({"name":"action", "value": '#modalSkills'});
			
			$.ajax({
				url: "xhr/cvform",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				
			});
		}
	}).on('click', '.computer button', function(e){
		var obj = e.target;
		$(obj).parent().find('button').removeClass('active');
		skill_computer[$(obj).parent().attr('data-computer')] = $(obj).addClass('active').text();


		skill = {
			"language" : skill_language,
			"computer" : skill_computer
		}

		var data = new Array();
		data.push({"name":"language", "value": JSON.stringify(skill)});
		data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
		data.push({"name":"action", "value": '#modalSkills'});
		
		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			
		});
	}).on('click', 'a[data-active]', function(e){
		var tab = $(e.target).attr('data-active');
		$('a[data-tab="'+tab+'"]').click();
	}).on('click', '.addon a.btn-edit', function(e){
		var obj =  $(e.target).parentsUntil(".list-group-item").parent();

		var item= $(obj[0]);
		var modal = $(obj[0]).parent()[0].id.replace('_list', '_edit');

		if (modal == 'modalOtherSkill_edit') {
			var dataobj = skill_other[$(item).attr('data-id')];
		} else {
			var dataobj = JSON.parse($(item).attr('data-content'));
		}
		

		$('#'+modal).find('button[data-update]').attr('data-mid', $(item).attr('data-id'));
		
		$.each(dataobj, function(idx, obj){

			$('#'+modal).find('*[name='+idx+'], *[name="'+idx+'[]"]').each(function(ii, oo){
				switch ($(oo).prop('type')) {
					case "text":
					case "textarea":
						if (typeof(obj) == 'object'){
							$(oo).val(JSON.stringify(obj));
						} else {
							if ($(oo).prop("id") in CKEDITOR.instances) {
								CKEDITOR.instances[$(oo).prop("id")].setData(obj);
							} else {
								$(oo).val(obj);
							}
						}
						break;
					case "radio":
						$('input[name='+idx+'][value='+obj+']').prop('checked', true);
				}
			});
			
			if (idx == 'todate') {
				if (obj == 'Present') {
					
					$('#'+modal).find('input[name='+idx+']').val('').prop('readonly',true).removeClass('reqModal').datetimepicker('destroy');
					$('#'+modal).find('input[name=topresent]').prop('checked', true);
				} else {
					$('#'+modal).find('input[name='+idx+']').prop('readonly',false).addClass('reqModal').datetimepicker(monthpicker_settingobject);
					$('#'+modal).find('input[name=topresent]').prop('checked', false);
				}
			}
		});
		if (modal == 'modalExam_edit' || modal == 'modalPublicExam_edit') {
			parseEditExam(modal, $(item).attr('data-id'));
		} else {
			$('#'+modal).modal('show');
		}

	}).on('click', '.addon a.btn-delete', function(e){
		var obj = $(e.target).parentsUntil(".list-group-item").parent();
		var item = $(obj[0]);

		if (confirm('Are you sure to remove this record?')) {
			
			var data = new Array();

			data.push({"name":"id", "value": $(item).attr('data-id')});
			data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
			data.push({"name":"action", "value": "removeMeta"});

			$.ajax({
				url: "xhr/cvform",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				

				if($(item).parent().find('li').length == 1	) {
					//$(obj).parentsUntil('.tab-pane').find('.panel-title').find('.hide').removeClass('hide');
					
					var na_id = '#na_'+$(item).parent()[0].id.replace('_list', '');
					$(na_id).prop('checked', false);
					$(na_id).parent().removeClass('hide');
					
				}
				$(item).remove();
			});
		} 
	}).on('click', '.addon a.btn-delete-grade', function(e){
		
		var modal = $(e.target).parentsUntil(".modal").parent();
		var textarea = $(modal).find('textarea');
		var li = $(e.target).parentsUntil("li").parent();

		var datasubject = $(e.target).attr('data-subject');
		
		if (typeof(datasubject) == 'undefined') {
			datasubject = $(e.target).parent().attr('data-subject');
		}

		if (confirm('Are you sure to remove this record?')) {
			obj = JSON.parse($(textarea).val());
			var newobj = {};
			$.each(obj, function(idx, nnn){
				if (idx !== datasubject) {
					newobj[idx] = nnn;
				}
			});

			$(textarea).val(JSON.stringify(newobj));
			$(li).remove();
		} 
	}).on('change', 'input[name=topresent]', function(e){
		var form = $(e.target).parentsUntil('.form').parent();
		if ($(e.target).prop('checked')) {
			form.find('input[name=todate]').val('').prop('readonly',true).removeClass('reqModal').datetimepicker('destroy');
		} else {
			form.find('input[name=todate]').val('').prop('readonly',false).addClass('reqModal').datetimepicker(monthpicker_settingobject);
		}
		
	}).on('keyup','[data-section]', function(){
		var obj = $(this).attr('data-section');

		if ($.trim($(this).val()) != '') {
			$('a[aria-controls='+obj+']').html($(this).val());
		} else {
			$('a[aria-controls='+obj+']').html($('a[aria-controls='+obj+']').attr('data-default-title'));
		}
	}).on('keyup', '.number', function(e){
		$(e.target).val($(e.target).val().replace(/[^0-9]+/g, ''));
	}).on('change', '[data-removereq]', function(e){
		var link = $(e.target).attr('data-removereq');
		
		if ($(e.target).prop('checked')) {
			$('#'+link).parent().removeClass('has-error');
			$('#'+link).datetimepicker('destroy');			
			$('#'+link).val('').prop('readonly', true).removeClass('req');
		} else {
			$('#'+link).val('').prop('readonly', false).addClass('req');
			$('#'+link).each((idx,obj)=> {
				if ($(obj).hasClass('monthpicker')){
					$(obj).datetimepicker(monthpicker_settingobject);
				}
			});
		}

		if ($(e.target).parents('.modal-body').length == 0) {
			if (!editFlag) {
				editControl = this;
				$('#autosave').removeClass('hide');
				setTimeout("autosave()", 1000);
				editFlag = true;
			}
		}

	}).on('change', '[data-remove-req-row]', function(e){
		var link = $(e.target).attr('data-remove-req-row');

		if ($(e.target).prop('checked')) {
			$('#'+link).removeClass('req_row');
		} else {
			$('#'+link).addClass('req_row');
		}

	}).on('click', '.btn-add-grade', function(e){
		var row = $(e.target).parent().parent();
		var grades = {};

		row.find('#subject').tooltip('destroy');
		row.find('#grade').tooltip('destroy');

		if ($('#frmpe_grade').val() != '') {
			grades = JSON.parse($('#frmpe_grade').val());
		}
		var subject = row.find('#subject').val();
		var grade = row.find('#grade').val();

		if (subject == '') {
			row.find('#subject').attr('title', 'Please enter the subject. ').attr('data-toggle','tooltip').tooltip('show');
		}

		if (grade == '') {
			row.find('#grade').attr('title', 'Please enter the grade or level obtained. ').attr('data-toggle','tooltip').tooltip('show');
		}

		if (subject != '' && grade != '' ){
			if (typeof(grades[subject]) == 'undefined') {
				grades[subject] = grade;
			}
			$('#frmpe_grade').val(JSON.stringify(grades));


			var li = $('#public_exam_grades').find('.hide').clone();
			li.find('[data-span=subject]').html(subject);
			li.find('[data-span=grade]').html(grade);
			li.find('.btn-remove-row').attr('data-subject', subject);


			li.removeClass('hide').appendTo($('#public_exam_grades'));
			row.find('#subject').val('');
			row.find('#grade').val('');
		}
		

	}).on('click', '.btn-remove-row', function(){
		var index = $(this).attr('data-subject');
		var row = $(this).parent().parent();

		if ($('#frmpe_grade').val() != '') {
			grades = JSON.parse($('#frmpe_grade').val());
		}
		delete(grades[index]);
		$('#frmpe_grade').val(JSON.stringify(grades));
		$(row).remove();
		
	}).on('click', '.btn-edit-grade', function(e){
		var row = $(e.target).parent().parent();
		var grades = {};
		
		row.find('#subject').tooltip('destroy');
		row.find('#grade').tooltip('destroy');

		if ($('#frmpe_grade_edit').val() != '') {
			grades = JSON.parse($('#frmpe_grade_edit').val());
		}		
		var subject = row.find('#subject').val();
		var grade = row.find('#grade').val();

		if (subject == '') {
			row.find('#subject').attr('title', 'Please enter the subject. ').attr('data-toggle','tooltip').tooltip('show');
		}

		if (grade == '') {
			row.find('#grade').attr('title', 'Please enter the grade or level obtained. ').attr('data-toggle','tooltip').tooltip('show');
		}


		if (subject != '' && grade != '' ){
			if (typeof(grades[subject]) == 'undefined') {
				grades[subject] = grade;
			}
		
			$('#frmpe_grade_edit').val(JSON.stringify(grades));

			var li = $('#public_exam_grades_edit').find('.hide').clone();
			li.find('[data-span=subject]').html(subject);
			li.find('[data-subject]').attr('data-subject', subject);
			li.find('[data-span=grade]').html(grade);

			li.removeClass('hide').appendTo($('#public_exam_grades_edit'));
			row.find('#subject').val('');
			row.find('#grade').val('');
		}
		

	}).on('change', '.ajax-upload input[type=file]', function(e){
		var uploadFile = $(e.target);
		var uploadObj = $(e.target).parent();

		var fileObj = (e.target.files[0]);

		if ($(uploadObj).attr('data-validfiletype').indexOf(fileObj.type) == -1 ) {
			console.log(fileObj);
			$(uploadObj).attr('title', 'Invalid Format. ').attr('data-toggle','tooltip').tooltip('show');
			return false;
		} else if ((fileObj.size / 1024) > 300 ) {
			$(uploadObj).attr('title', 'Error, file size limited to 300KB').attr('data-toggle','tooltip').tooltip('show');
			
			return false;
		} else {
			$(uploadObj).attr('data-toggle','tooltip').tooltip('destroy');
		}

		var formData = new FormData();
		formData.append($(uploadFile).prop("id"), fileObj);
		formData.append('action', 'uploadFile');
		formData.append('type', $(uploadFile).prop("id"));
		formData.append('application_id', $('#btn_save').attr('data-id'));

		$.ajax({
			url: './xhr/cvform',
			type: 'POST',
			cache: false,
			data: formData,
			dataType: 'JSON',
			processData: false,
			contentType: false,
			xhr: function(){
				var xhr = $.ajaxSettings.xhr() ;
				xhr.upload.onprogress = function(evt) {
					var progressbar = $(uploadObj).find('.progress-bar');
					var percentage = parseInt(evt.loaded / evt.total * 100);
					progressbar.attr('aria-valuenow', percentage);
					progressbar.css('width', percentage+'%');
					progressbar.html(percentage+'%');
				};
				return xhr;
			}
		}).done(function(data) {
			uploadObj.find('input').attr('data-value', data.filename);
			uploadObj.next('.link-wrapper').find('.link').html('<a href="'+data.url+'" target="_blank"><i class="fa fa-paperclip"></i> '+data.filename+'</a>');
			
			uploadObj.next('.link-wrapper').removeClass('hide');
			
			uploadObj.hide();
		});
	}).on('click', 'a[data-remove-attach]', function(e){
		if (confirm("Confirm to remove the attachment? ")) {
			var fileid = $(e.target).attr('data-remove-attach');
			var uploadObj = $('input#'+fileid).parent();
			$('input#'+fileid).attr('data-value', '');

			var data = new Array();
			data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
			data.push({"name":"type", "value": fileid});
			data.push({"name":"action", "value": 'updateFile'});

			$.ajax({
				url: "xhr/cvform",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				uploadObj.removeClass('hide').show();
				
				$(uploadObj).find('.progress-bar').css('width', '0%');
				$(uploadObj).find('.progress-bar').attr('aria-valuenow', '0');

				uploadObj.next('.link-wrapper').addClass('hide');
			});
		}
	}).on('click', '.btnSendEmail', function(e){
		var formid = this.form.id;
		var validObj = validateObject[formid];
		var valid = true;

		$(this.form).find('.req').each(function(idx, obj){
			if ($.trim($(obj).val()) == "") {
				valid = false;
				$(obj).prop('title', validObj[obj.id].empty).attr('data-toggle','tooltip').tooltip('show');
	
			} else {
				if ($(obj).next('.tooltip').length > 0) {
					$(obj).removeAttr('data-toggle').tooltip('destroy');
				}
				$(obj).parent().find('.cke').removeAttr('data-toggle').tooltip('destroy');
			}
		});
					console.log(valid);
		if (!checkMail($(this.form).find('.email').val())) {
			$("#modalEmail_1").prop('title', validObj["modalEmail_1"].invalid).attr('data-toggle','tooltip').tooltip('show');
			valid = false;
		}
	});

	$(window).scroll( function(){
		// Add sticky heree....
		var scrollTop = $(window).scrollTop();
		$('.tips>div').css('width', $('.tips>div').outerWidth());	
		if (scrollTop > 380) {
			$('.tips>div').addClass('sticky');
		} else {
			$('.tips>div').removeClass('sticky');
		}
	});	

	$('#add_exam_row').on('click', function(){
		var valid = true;
		if ($("#exam_list option:selected").text() == ""){
			valid =  false;
		}

		if ($("#exam_date").val() == ""){
			$('#exam_date').prop('title', 'Please add a year and press enter first').attr('data-toggle','tooltip').tooltip('show');
			valid =  false;
		} else {
			var amoment = $('#exam_date').val() + "-01-01";
			amoment = moment(amoment, 'YYYY-MM-DD', true);
			
			
			if (!amoment.isValid()) {
				$('#exam_date').prop('title', 'Year value not valid').attr('data-toggle','tooltip').tooltip('show');
				valid = false;
			} 
			if (!amoment.isBetween('1970-01-01', moment().format())) {
				$('#exam_date').prop('title', 'Year value not valid').attr('data-toggle','tooltip').tooltip('show');
				valid =  false;
			}
		}

		if (valid) {
			$('#exam_'+$('#exam_list').val()).removeClass("hide");
			$("#exam_list, #exam_date, #add_exam_row").hide();
			$('#lbl_exam_list').text($("#exam_list option:selected").text());
			$('#lbl_exam_date').text($("#exam_date").val());
			$('#row_add_subject').removeClass('hide');
		}
	});

	$('#modalExam').on('hidden.bs.modal', function(){
		$('#modalExam').find('form')[0].reset();
		$('#exam_'+$('#exam_list').val()).addClass("hide");
		$("#exam_list, #exam_date, #add_exam_row").val('').show();
		
		$('#lbl_exam_list').text('');
		$('#lbl_exam_date').text('');
		$('#row_add_subject').addClass('hide');
	})
	if (typeof($('body').select2) != "undefined") {
		$('#exam_subject').select2({
			ajax: {
				url: './xhr/subject',
				dataType: 'json',
				data: function(params){
					return {
						q: params.term,
						exam : $('#exam_list').val()
					};
				}
			}, 
			
		}).on('select2:select', function(e){
			var selected = $('#exam_subject').select2('data')[0];
			$('select[data-name="grade"]').addClass('hide');
			$('#exam_subject-grade'+ selected.tag).removeClass('hide');
		})

		$('#exam_subject_edit').select2({
			ajax: {
				url: './xhr/subject',
				dataType: 'json',
				data: function(params){
					return {
						q: params.term,
						exam : $('#exam_name').val()
					};
				}
			}, 
			
		}).on('select2:select', function(e){
			
			var selected = $(e.target).select2('data')[0];
			
			var id = $(e.target).prop('id');

			$('select[data-name="grade"]').addClass('hide');
			$('#'+id+'-grade'+ selected.tag).removeClass('hide');
		})
	}


	$('#add_subject_result').on('click', function(){
		var valid = true;
		var obj = $('#exam_result').val();
		if (obj == "" || obj == "null") {
			obj = "{}";
		}
		obj = JSON.parse(obj);

		if ($('#exam_subject').select2('data').length == 0){
			valid = false;
		}

		if ($("select[data-name=grade]:not(.hide)").val() == ""){
			valid = false;
		}

		if (valid) {
			var subject = $('#exam_subject').select2('data')[0];
			
			if (!obj.hasOwnProperty(subject.text)){
				var li = $('<li class="row"><div class="col-sm-8">' + subject.text +'</div>'+ 
								'<div class="col-sm-2 text-right">' + $("select[data-name=grade]:not(.hide)").val() + '</div>'+
								'<div class="addon">'+
								'<a class="btn btn-delete-grade" title="Delete" data-subject="'+subject.text+'"><span class="fa fa-times-circle"></span></a>'+
							'</div></li>');

				$('#exam_' + $('#exam_list').val()).append(li);
				obj[subject.text] = $("select[data-name=grade]:not(.hide)").val();
				$('#exam_result').val(JSON.stringify(obj));


				$('select[data-name="grade"]').addClass('hide').val('');
			}
		}
	});

	$('#edit_subject_result_add').on('click', function(){
		var valid = true;
		var obj = $('#exam_result_edit').val();
		if (obj == "" || obj == "null") {
			obj = "{}";
		}
		obj = JSON.parse(obj);

		if ($('#exam_subject_edit').select2('data').length == 0){
			valid = false;
		}

		if ($("select[data-name=grade]:not(.hide)").val() == ""){
			valid = false;
		}

		if (valid) {
			var subject = $('#exam_subject_edit').select2('data')[0];
			
			if (!obj.hasOwnProperty(subject.text)){
				var li = $('<li class="row"><div class="col-sm-8">' + subject.text +'</div>'+ 
								'<div class="col-sm-2 text-right">' + $("select[data-name=grade]:not(.hide)").val() + '</div>'+
								'<div class="addon">'+
								'<a class="btn btn-delete-grade" title="Delete" data-subject="'+subject.text+'"><span class="fa fa-times-circle"></span></a>'+
							'</div></li>');

				$('#modalExam_edit').find('.exam_ul').append(li);
				
				obj[subject.text] = $("select[data-name=grade]:not(.hide)").val();
				
				$('#exam_result_edit').val(JSON.stringify(obj));

				$('select[data-name="grade"]').addClass('hide').val('');
			}
		}
	});
	
	$('body').on('change', 'select[data-grade]', function(e) {
		$(e.target).removeClass('error');
		var modal = $(e.target).parentsUntil('.modal').parent().prop('id');
		var textarea = $('#'+modal).find('textarea');

		var obj = $(textarea).val();
		if (obj == "" || obj == "null") {
			obj = "{}";
		}
		obj = JSON.parse(obj);
		obj[$(this).attr('data-grade')] = $(this).val();
		$(textarea).val(JSON.stringify(obj));
	}).on('click', '.btn-upload', function(){
		$(this).parent().parent().find('input[type=file]').trigger('click');
	})

	$('#add_date_travel').on('click', function(){
		li = $('<li class="list-group-item">'+$('#date_travel').val()+
				'<div class="addon">'+
					'<a class="btn btn-delete" title="Delete"><span class="fa fa-times-circle"></span></a>'+
				'</div>'+
				'</li>');


		var data = [];

		data.push({"name":"action", "value":'#addDateTravel'});
		data.push({"name":"application_id", "value": $('#btn_save').attr('data-id')});
		data.push({"name":"date_travel", "value": $('#date_travel').val()});
		
		$.ajax({
			url: "xhr/cvform",
			data: data,
			dataType: "JSON",
			method: "POST"
		}).done(function(data){
			$('#modalDateTravel_list').append(li);
			$('#date_travel').val('').daterangepicker('reset');
		});
	});
	if ($('.datepicker').length > 0) {
		$('.datepicker').datetimepicker({
			format: 'Y-m-d',
			timepicker:false
		});
	}
	if ($('.date-range').length > 0) {
		$('.date-range').daterangepicker({
			minDate: new Date(),
			autoUpdateInput: false,
		});
		$('.date-range').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
	}
	if ($('.monthpicker').length > 0) {
		$('.monthpicker:not([readonly])').datetimepicker(monthpicker_settingobject);

		$('.monthpicker[readonly]').val('');
	}

	$('a[target=_print]').on('click', function(e){
		e.preventDefault();
		var w = window.open($(this).attr('href'), "_print");
		w.addEventListener('load', w.print, true); 
	});



	function validate(){
		valid = true;
		errorObj = new Array();

		$('.has-error').removeClass('has-error');
		$('#formError').addClass('hide');


		$.each($('.req'), function(idx, obj){
			if ($.trim($(obj).val()) == "") {
				$(obj).parent().addClass('has-error');
				errorObj.push(validateObject[obj.id]);
				valid = false;
			}
		});

		$.each($('.reqAttach'), function(idx, obj){
			if ($.trim($(obj).attr("data-value")) == "") {
				$(obj).parent().addClass('has-error');
				errorObj.push(validateObject[obj.id]);
				valid = false;
			}
		});

		$.each($('.phone'), function(idx, obj) {			
			if ($(obj).val().length != 8 && $(obj).val().length > 0) {
				errorObj.push({"msg": validateObject[obj.id].invalid});
				$(obj).parent().addClass('has-error');
				valid = false;
			}
		});



		$.each($('.email'), function(idx, obj) {			
			if (!checkMail($(obj).val()) && $(obj).val().length > 0) {
				errorObj.push({"msg": validateObject[obj.id].invalid});
				$(obj).parent().addClass('has-error');
				valid = false;
			}
		});

		$.each($('.req_row'), function(idx, obj){
			if ($(obj).find('li').length == 0) {

				errorObj.push(validateObject[obj.id]);
				valid = false;
			}
		});

		if ($('#hkid').hasClass('req')) {
			if (!chkHKID($('#hkid'))) {
				errorObj.push({"msg": "Incorrect HKID"});
				valid = false;
			}
		}
		
		var errObj = false;
		$.each(skill_computer, function(idx, obj){
			if (jQuery.isEmptyObject(obj)) {
				errObj = validateObject["skill_computer"];
				valid = false;
			}
		})


		if (errObj) {
			errorObj.push(errObj);
		}

		var errObj = false;
		$.each(skill_language, function(idx, langobj){
			$.each(langobj, function(idx, obj){
				if (jQuery.isEmptyObject(obj)) {

					errObj = validateObject["skill_language"];
					valid = false;
				}
			});
		});


		if (errObj) {
			errorObj.push(errObj);
		}
		

		if (errorObj.length > 0) {

			$('#formError #msg').html('');
			$.each(errorObj, function(idx, thisobj){

				$('#formError #msg').append('<li>'+thisobj.msg+'</li>');
			});
			$('a[data-tab="'+errorObj[0].panel+'"]').click();
			$('#formError').removeClass('hide');
			$('#'+errorObj[0].tabpanel).collapse("show");
			$(window).scrollTop(0);
		}

		return valid;

	}
	/*
	$('#btn_save').on('click', function(){
		//if (validate()) {
			$('#cv_wrapper').wrap('<form id="cv_form" action="new_cv?goSave='+this.id+'" method="POST" enctype="multipart/form-data"></form>');
			for (instance in CKEDITOR.instances) {
				CKEDITOR.instances[instance].updateElement();
			}
			$('#cv_form').submit();
		//}	
		return false;
	});
	*/

	if (typeof(CKEDITOR) != "undefined") {
		$('#btn_gocv').on('click', function(){
			if (validate()) {

				$('#cv_form').attr('action', 'new_cv?goSave='+this.id);

				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}
				$('#cv_form').submit();
			}	
			return false;
		});
	}
	$('#choose_content .list input[type=checkbox]').on('click', function(e){
		e.stopPropagation();
	});

	$('#choose_content .list li').on('click', function() {
		var checkbox = $(this).find('.cv-addon input[type=checkbox]'); 
		checkbox.prop('checked', !checkbox.prop('checked'));
	});

	// CKEditor fix
	if (navigator.userAgent.indexOf("iPhone") > 0 || navigator.userAgent.match(/Android/i) ) {
		if (Object.keys(CKEDITOR.instances).length > 0) {
			
			
			Object.keys(CKEDITOR.instances).map( function(obj) {
				console.log('remove ' + obj);
				setTimeout("CKEDITOR.instances['"+obj+"'].destroy(true)", 500);
			});
		}
	} else {
		$('a[data-tab=other]').on('hidden.bs.tab', function(event){
			if (typeof(CKEDITOR) != "undefined") {
				CKEDITOR.instances['other1-textarea'].updateElement();
				CKEDITOR.instances['other1-textarea'].destroy(true);
				CKEDITOR.instances['other2-textarea'].updateElement();
				CKEDITOR.instances['other2-textarea'].destroy(true);
				CKEDITOR.instances['other3-textarea'].updateElement();
				CKEDITOR.instances['other3-textarea'].destroy(true);
			}
		}).on('show.bs.tab', function(event){
			if (typeof(CKEDITOR) != "undefined") {
			
				if (!CKEDITOR.instances['other1-textarea']) {
					CKEDITOR.replace('other1-textarea');
				}
				if (!CKEDITOR.instances['other2-textarea']) {
					CKEDITOR.replace('other2-textarea');
				}
				if (!CKEDITOR.instances['other3-textarea']) {
					CKEDITOR.replace('other3-textarea');
				}
			}
		})

		$('#modalOtherSkill').on('hidden.bs.modal', function(event){
			CKEDITOR.instances['frmskill_description'].updateElement();
			CKEDITOR.instances['frmskill_description'].destroy(true);
			$('#frmskill_description').val('');
			console.log($('#frmskill_description').val());
		}).on('show.bs.modal', function(event){

			if (typeof(CKEDITOR) != "undefined") {
				if (!CKEDITOR.instances['frmskill_description']) {
					CKEDITOR.replace('frmskill_description');
				}
			}
		})

		$('#modalOtherSkill_edit').on('hidden.bs.modal', function(event){
			CKEDITOR.instances['frmskill_description_edit'].updateElement();
			CKEDITOR.instances['frmskill_description_edit'].destroy(true);
			$('#frmskill_description_edit').val('');
		}).on('show.bs.modal', function(){
			if ($('#frmskill_description_edit').hasClass('ckeditor')) {
				if (!CKEDITOR.instances['frmskill_description_edit']) {
					CKEDITOR.replace('frmskill_description_edit');
				}	
			}
		});

		if (typeof(CKEDITOR) != "undefined") {
			CKEDITOR.on('instanceReady', function(evt) {
				var editor = evt.editor;
				editor.on('focus', function(e) {
					var data = new Array();
					var pid = e.editor.id;

					data.push({"name":"tag", "value": pid});
					data.push({"name":"action", "value": 'tips'});

					$.ajax({
						url: "xhr/cvform",
						data: data,
						dataType: "JSON",
						method: "POST"
					}).done(function(data){
						$('.tips_content').attr('data-cms-title', pid).hide().html(data.html).fadeIn();
						if (viewCms) {
							$('[data-cms-title]').addClass('cmsView');
						}
					});
					return true;
				});
			});
		}
	}

	

	$('[data-toggle="tooltip"]').tooltip()
	
	//setInterval("autosave()", 30000);


	$('.form-control').on('keyup', function(){
		if ($(this).parents('.modal-body').length == 0) {
			if (!editFlag) {
				editControl = this;
				$('#autosave').removeClass('hide');
				setTimeout("autosave()", 1000);
				editFlag = true;
			}
		}
	});

	$('.monthpicker').on('change', function(){
		if ($(this).parents('.modal-body').length == 0) {
			if (!editFlag) {
				editControl = this;
				$('#autosave').removeClass('hide');
				setTimeout("autosave()", 1000);
				editFlag = true;
			}
		}
	});


	if (window.location.hash == '#validate') {
		validate();
	}
});


function autosave() {

	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	}

	data = $('#cv_form').serializeArray();

	// Fix Empty Select2 Bugs
	keyname = data.map(o=>o.name);
	$('select.select2').each((idx,obj)=>{
		if (typeof($(obj).attr('name')) != 'undefined' && keyname.indexOf($(obj).attr('name')) < 0) { 
			data.push({name: $(obj).attr('name'), value: $(obj).val()});
		}
	});

	data.push({name: "action", value: "autosave"});
	
	dummy = false;

	$.ajax({
		url: "xhr/cvform",
		data: data,
		dataType: "JSON",
		method: "POST"
	}).done(function(data){
		editFlag = false;
		$('#autosave').addClass('hide');

		Object.keys(CKEDITOR.instances).map((idx)=>{
			CKEDITOR.instances[idx].destroy(true);
			CKEDITOR.replace(idx);
		});
	});
}

function chkModalExam(){
	var valid = true;

	$('#exam_date, #modalExam .exam_ul select, #add_exam_row').prop('title', '');

	if ($.trim($('#exam_date').val()) == '') {
		$('#exam_date').prop('title', 'Please add a year and press enter first').attr('data-toggle','tooltip').tooltip('show');
		return false;
	} else {
		var amoment = $('#exam_date').val() + "-01-01";
		amoment = moment(amoment, 'YYYY-MM-DD', true);
		

		if (!amoment.isValid()) {
			$('#exam_date').prop('title', 'Year value not valid').attr('data-toggle','tooltip').tooltip('show');
			return false;
		} 
		if (!amoment.isBetween('1970-01-01', moment().format())) {
			$('#exam_date').prop('title', 'Year value not valid').attr('data-toggle','tooltip').tooltip('show');
			return false;
		}
	}

	if ($.trim($('#exam_result').val()) == '' && $('#add_exam_row').css('display') != 'none' ) {
		$('#add_exam_row').prop('title', 'Press Enter.').attr('data-toggle','tooltip').tooltip('show');
		return false;
	}

	var errObj = false;


	$('#modalExam').find('.exam_ul:not(.hide) select').each(function(idx, obj){
		if ($(obj).val() == '') {
			errObj = errObj ? errObj : obj;
			$(obj).addClass('error');
			valid = false;
		}
	});

	if (!valid) {
		$(errObj).prop('title', 'Please select your grade').attr('data-toggle','tooltip').tooltip('show');
		return false;
	}

	return valid;
}

function chkModalExam_edit(){
	var valid = true;

	$('#exam_date_edit, #modalExam_edit .exam_ul select').prop('title', '');

	if ($.trim($('#exam_date_edit').val()) == '') {
		console.log("A");
		$('#exam_date_edit').prop('title', 'Please add a year and press enter first').attr('data-toggle','tooltip').tooltip('show');
		return false;
	} else {
		var amoment = $('#exam_date_edit').val() + "-01-01";
		amoment = moment(amoment, 'YYYY-MM-DD', true);

		if (!amoment.isValid()) {
			$('#exam_date_edit').prop('title', 'Year value not valid').attr('data-toggle','tooltip').tooltip('show');
			return false;
		} 
		if (!amoment.isBetween('1970-01-01', moment().format())) {
			$('#exam_date_edit').prop('title', 'Year value not valid').attr('data-toggle','tooltip').tooltip('show');
			return false;
		}
	}


	var errObj = false;

	$('#modalExam_edit').find('.exam_ul:not(.hide) select').each(function(idx, obj){
		if ($(obj).val() == '') {
			errObj = errObj ? errObj : obj;
			$(obj).addClass('error');
			valid = false;
		}
	});

	if (!valid) {
		$(errObj).prop('title', 'Please select your grade').attr('data-toggle','tooltip').tooltip('show');
		return false;
	}

	return valid;
}

function chkHKID(input){
	var pattern = /^([A-Z]{1,2})([0-9]{6})\(([A0-9])\)$/;
	$(input).val($(input).val().toUpperCase());
	
	var hkid = $(input).val();

	if (hkid != ""){
		if (hkid[hkid.length-2] == "(" && hkid.indexOf(")") < 0) {
			$(input).val($(input).val() + ")");
		} else if (hkid.match(/\d{6}$/) != null) {
			$(input).val($(input).val() + "(");
		} 
	}

	return pattern.test($(input).val());
}

function chkModal(modal) {
	var valid = true;
	var valObj = validateObject[modal];

	var dateValid = {};

	if (typeof(valObj) != "undefined") {
		$('#'+modal+' .reqModal').each(function(idx, obj){
			

			if ($.trim($(obj).val()) == "" || $.trim($(obj).val()) == "{}" ) {

				$(obj).prop('title', valObj[obj.id].empty).attr('data-toggle','tooltip').tooltip('show');
			
				if (obj.id == 'frmpe_grade_edit') {
					$('#modalPublicExam_edit #subject').prop('title', valObj[obj.id].empty).attr('data-toggle','tooltip').tooltip('show');					
				}

				if (obj.id == 'frmpe_grade') {
					$('#modalPublicExam #subject').prop('title', valObj[obj.id].empty).attr('data-toggle','tooltip').tooltip('show');					
				}

				valid = false;
			} else {
				if ($(obj).next('.tooltip').length > 0) {
					$(obj).removeAttr('data-toggle').tooltip('destroy');
				}
			}
		});
	}
	
	$.each($('.phone'), function(idx, obj) {			
		if ($(obj).val().length != 8 && $(obj).val().length > 0) {
			$(obj).prop('title', valObj[obj.id].invalid).attr('data-toggle','tooltip').tooltip('show');
			$(obj).parent().addClass('has-error');
			valid = false;
		}
	});

	$.each($('.email'), function(idx, obj) {			
		if (!checkMail($(obj).val()) && $(obj).val().length > 0) {
			$(obj).prop('title', valObj[obj.id].invalid).attr('data-toggle','tooltip').tooltip('show');
			$(obj).parent().addClass('has-error');
			valid = false;
		}
	});

	$.each((valObj), (idx, obj)=>{
		
		$('#'+idx).removeClass('error');
		if (typeof(obj['date-invalid']) != 'undefined') {
			if (moment('1 ' + $('#'+idx).val()).unix() > moment().unix()) {
				$('#'+idx).addClass('error').prop('title', obj['date-invalid']).attr('data-toggle','tooltip').tooltip('show');
			}
		}
		if (typeof(obj['date-invalid-from']) != 'undefined') {
			dateValid['from'] = {};
			dateValid['from']['field_id'] = idx;
			dateValid['from']['field_value'] = $('#'+idx).val();
			dateValid['from']['field_msg'] = obj['date-invalid-from'];
		}
		if (typeof(obj['date-invalid-to']) != 'undefined') {
			dateValid['to'] = {};
			dateValid['to']['field_id'] = idx;
			dateValid['to']['field_value'] = $('#'+idx).val();
			dateValid['to']['field_msg'] = obj['date-invalid-to'];
		}

	});
	if (typeof(dateValid['from']) != 'undefined' && typeof(dateValid['to']) != 'undefined' &&
		dateValid['from']["field_value"] != "" && dateValid['to']["field_value"] != "") {

		if (moment('1 ' + dateValid['from'].field_value).unix() > moment('1 ' + dateValid['to'].field_value).unix()) {
			$('#'+dateValid['from']['field_id']).addClass('error').prop('title', dateValid['from']['field_msg']).attr('data-toggle','tooltip').tooltip('show');
			$('#'+dateValid['to']['field_id']).addClass('error').prop('title', dateValid['to']['field_msg']).attr('data-toggle','tooltip').tooltip('show');			
			valid = false;
		}		
	}

	return valid;
}

var checkMail = function(email) {
	var x = email;
	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (filter.test(x)) return true;
	else return false;
}

function parseEditExam(modal, pid){
	var data = new Array();
	
	data.push({"name":"pid", "value": pid});
	data.push({"name":"action", "value": 'getEditExamForm'});

	$.ajax({
		url: "xhr/cvform", 
		data: data,
		dataType: "JSON",
		method: "POST"
	}).done(function(data){
		$('#'+modal).find('.exam_ul').removeClass('hide').html(data.html);

		$('#'+modal).modal('show');
	});
}