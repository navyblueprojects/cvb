var validateObject = {
	"open_date_range_error" : {
		"msg":"Application date range error", 
		"panel":"#form-basic"
	},
	"name" : {
		"msg":"Please enter the job position name", 
		"panel":"#form-basic"
	},
	"open_date" : {
		"msg":"Please enter the application start date", 
		"panel":"#form-basic"
	},
	"close_date" : {
		"msg":"Please enter the application close date", 
		"panel":"#form-basic"
	},
}


$(function(){
	$('#loginform').on('submit', function(){
		$('#loginform_wrapper>div').find('.alertLogin').remove();
		var data = $(this).serializeArray();
        if ($('#captcha').val().length != 4) {
        	
            var alert = $('.alertLogin').clone();
            console.log(alert);
			alert.find('.errorMsg').html("Please enter the captcha code");
			alert.removeClass('hide');
			$('#loginform_wrapper>div').prepend(alert);
            return false;
        }

        data.push({name: 'action', value: 'login'});

		$.ajax({
			url: '../xhr/wie',
			method: 'POST',
			data: data,
			dataType: 'JSON'
		}).done(function(data){
			if (data.status == 1) {
				window.location = 'home';
			} else {
                 $('#img_captcha').attr('src', './captcha?' + new Date().getTime());
				var alert = $('.alertLogin').clone();
				alert.find('.errorMsg').html(data.msg);
				alert.removeClass('hide');
				$('#loginform_wrapper>div').prepend(alert);
            }
		});
        
		return false;
	});
	
	if (viewCms) {
		$('[data-cms-title]').addClass('cmsView');
	}
	

	classToggle = function(target, a, b){
		if ($(target).hasClass(a)) {
			$(target).removeClass(a).addClass(b);
		} else if ($(target).hasClass(b)){
			$(target).removeClass(b).addClass(a);
		}
	}


	calcYear = function(obj){
		classToggle(obj, 'fa-check-square', 'fa-square');
		target = $(obj).parentsUntil('.target').parent();
		val = '';
		target.find('[data-name]').each(function(idx, obj){
			if ($(obj).hasClass('fa-check-square')) {
				val += $(obj).attr('data-value') + ";";
			}
		});
		target.find('.years').val(val);
	}

	toggleAllYear = function(obj) {
		var foo;
		$.each($('.target:not(.newrow) span[data-name^=year][data-value='+$(obj).attr('data-value')+']'), function(idx, oobj){
			calcYear(oobj);
			foo = oobj
		});

		if ($(foo).hasClass('fa-square')) {
			$(obj).removeClass('fa-check-square').addClass('fa-square');
		} else {
			$(obj).removeClass('fa-square').addClass('fa-check-square');
		}
	}

	changePermission = function(obj){ 
		if ($(obj).hasClass('fa-square')) {
			$(obj).removeClass('fa-square').addClass('fa-check-square');
			$(obj).next('input[type=hidden]').val(1);
		} else {
			$(obj).removeClass('fa-check-square').addClass('fa-square');
			$(obj).next('input[type=hidden]').val(0);
		}
	} 

	$('body').on('click', '.cmsView', function(e){
		var data = $(this);
		window.open('../admin_editpage?tag='+data.attr('data-cms-title'));
	}).on('click', '#reloadCaptcha', function(e){
        $('#img_captcha').attr('src', './captcha?' + new Date().getTime());
    }).on('keyup', '#captcha', function(e){
        $(e.target).val($(e.target).val().toUpperCase());
    }).on('click', '.btn-delete', function(e){
		if (confirm("Are you sure to delete this job post?")) {
			var data = new Array();
			var pid = $(e.target).attr('data-id');

			data.push({"name":"pid", "value": pid});
			data.push({"name":"action", "value": 'deleteJob'});

			$.ajax({
				url: "../xhr/wie",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				if (data.status) {
					window.location = '/wie/jobs';
				} else {
					alert(data.msg);
				}
			});
		}
	}).on('click', '.btn-duplicate', function(e){
		if (confirm("Are you sure to duplicate this job post?")) {
			var data = new Array();
			var pid = $(e.target).attr('data-id');

			data.push({"name":"pid", "value": pid});
			data.push({"name":"action", "value": 'cloneJob'});

			$.ajax({
				url: "../xhr/wie",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				if (data.status == '1') {
					$('#errorModal').find('.modal-title').html(data.msg);
					$('#errorModal').find('a[data-dismiss]').attr({'href':'job-edit/'+data.url, 'data-dismiss':""});
				} else {
					$('#errorModal').find('.modal-title').html(data.msg);
				}
				$('#errorModal').modal('show');
			});
		}
	}).on('change', '.ajax-upload input[type=file]', function(e){
		var uploadFile = $(e.target);
		var uploadObj = $(e.target).parent();

		var fileObj = (e.target.files[0]);
		if ((fileObj.size / 1024) > 300 ) {
			$(uploadObj).attr('data-toggle','tooltip').tooltip('show');
			return false;
		} else {
			$(uploadObj).attr('data-toggle','tooltip').tooltip('destroy');
		}

		var formData = new FormData();
		formData.append($(uploadFile).prop("id"), fileObj);
		formData.append('action', 'uploadFile');
		formData.append('type', $(uploadFile).prop("id"));
		formData.append('id', $('input[name=id]').val());

		$.ajax({
			url: '../xhr/wie',
			type: 'POST',
			cache: false,
			data: formData,
			dataType: 'JSON',
			processData: false,
			contentType: false,
			xhr: function(){
				var xhr = $.ajaxSettings.xhr() ;
				xhr.upload.onprogress = function(evt) {
					var progressbar = $(uploadObj).find('.progress-bar');
					var percentage = parseInt(evt.loaded / evt.total * 100);
					progressbar.attr('aria-valuenow', percentage);
					progressbar.css('width', percentage+'%');
					progressbar.html(percentage+'%');
				};
				return xhr;
			}
		}).done(function(data) {

			uploadObj.next('.link-wrapper').find('.link').html('<a href="'+data.url+'" target="_blank"><i class="fa fa-paperclip"></i> '+data.filename+'</a>');
			

			uploadObj.next('.link-wrapper').find('.link').append('<a href="#" class="btn btn-remove" data-remove-attach="attachment"><i class="fa fa-ban"></i> Delete</a>');
			uploadObj.next('.link-wrapper').removeClass('hide');
			

			var progressbar = $(uploadObj).find('.progress-bar');
			
			progressbar.attr('aria-valuenow', 0);
			progressbar.css('width', '0%');
			progressbar.html('');


			//uploadObj.hide();
		});
	}).on('click', 'a[data-remove-attach]', function(e){
		if (confirm("Confirm to remove the attachment? ")) {
			var fileid = $(e.target).attr('data-remove-attach');
			var uploadObj = $('input#'+fileid).parent();
			
			var data = new Array();
			data.push({"name":"job_id", "value": $('input[name=id]').val()});
			data.push({"name":"type", "value": fileid});
			data.push({"name":"action", "value": 'removeFile'});

			$.ajax({
				url: "../xhr/wie",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				uploadObj.removeClass('hide').show();
				
				$(uploadObj).find('.progress-bar').css('width', '0%');
				$(uploadObj).find('.progress-bar').attr('aria-valuenow', '0');

				uploadObj.next('.link-wrapper').addClass('hide');
			});
		}
	}).on('click', '.aRemove.target', function(e){
		e.stopPropagation();
		if (confirm('Are you sure to remove this record?')) {
			$(this).parentsUntil('.target').parent().remove();
		}
	}).on('click', '.aRemove.privilege', function(e){
		e.stopPropagation();
		if (confirm('Are you sure to remove this record?')) {
			$(this).parentsUntil('.privilege').parent().remove();
		}
	}).on('click', '.aAddRow.target', function(e){
		
		var row = $('.target.newrow').clone();
		row.removeClass('hide newrow');
		row.find('.aRemove').removeClass('hide');

		$('.checkall-year').removeClass('hide');
		$('#target_wrapper').append(row);

		
		row.find('select').prop('name', row.find('select').attr('data-name'));
		row.find('.years').prop('name', row.find('.years').attr('data-name'));
		row.find('select').select2({
			ajax: { url: '../xhr/program.xhr.php',dataType: 'json'}
		});

	}).on('click', '.aAddRow.privilege', function(e){
		
		var row = $('.privilege.newrow').clone();
		row.removeClass('hide newrow');
		row.find('.aRemove').removeClass('hide');

		$('#privilege_wrapper').append(row);

		
		row.find('select').prop('name', row.find('select').attr('data-name'));
		$.each(row.find('input[data-name]'), function(idx, obj){
			$(obj).prop('name', $(obj).attr('data-name'));
		});
		row.find('select').select2({
			ajax: { url: '../xhr/lecturer.xhr.php',dataType: 'json'}
		});
	}).on('click', '#application_details .btnSendEmail, #batch_offering .btnSendEmail', (e)=>{
		
		if (confirm("Are you sure to send the offer email to the student? ")) {
			var data = new Array();
			data.push({"name":"item_id", "value": $(e.target).attr('data-app-id')});
			data.push({"name":"action", "value": 'sendOfferEmail'});

			$.ajax({
				url: "../xhr/wie",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done((data)=>{
				if (data.status == 1) {
					window.location.reload();
				} else {
					alert(data.msg);
				}
			});
		}
	}).on('submit', '#jobs_form', function(e){
		e.stopPropagation();
		var valid = true;
		var errorObj = [];

		$.each($('.req'), function(idx, obj){
			if ($.trim($(obj).val()) == "") {
				$(obj).parent().addClass('has-error');
				errorObj.push(validateObject[obj.id]);
				valid = false;
			}
		});

		if (valid) {
			if (moment($('#open_date').val()) >= moment($('#close_date').val())) {
				errorObj.push(validateObject['open_date_range_error'])
				valid = false;
			}
		}

		if (errorObj.length > 0) {
			$('#formError #msg').html('');
			$.each(errorObj, function(idx, thisobj){
				$('#formError #msg').append('<li>'+thisobj.msg+'</li>');
			});
			$('a[data-toggle][href="'+errorObj[0].panel+'"]').click();
			$('#formError').removeClass('hide');
			$(window).scrollTop(0);
		}

		return valid;
	})

	CKEDITOR.on('instanceReady', function(evt) {
	    var editor = evt.editor;
	    editor.on('focus', function(e) {
	        var data = new Array();
			var pid = e.editor.id;

			data.push({"name":"tag", "value": pid});
			data.push({"name":"action", "value": 'tips'});

			$.ajax({
				url: "../xhr/cvform",
				data: data,
				dataType: "JSON",
				method: "POST"
			}).done(function(data){
				$('.tips_content').attr('data-cms-title', pid).hide().html(data.html).fadeIn();
				if (viewCms) {
					$('[data-cms-title]').addClass('cmsView');
				}
			});
			return true;
	    });
	});

	$('.datepicker').datetimepicker({
		minDate: new Date(),
		format: 'Y-m-d H:i:s'
	});

	$('.date-range').daterangepicker({
		minDate: new Date(),
		autoUpdateInput: false,

	});

	$('.date-range').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	}).on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});

	$('a[data-font-size]').on('click', function(){
		$('body').removeClass().addClass($(this).attr('data-font-size'));
	});

	$('#wie-jobs_create .nav-pills a').on('shown.bs.tab', ()=>{
		$('button[type=reset]').addClass('hide');
	});

	$('#wie-jobs_create .nav-pills a').eq(0).on('shown.bs.tab', ()=>{
		$('button[type=reset]').removeClass('hide');
	});


	keepAlive();
	setInterval("keepAlive()", 15000);
});


function keepAlive(){
	$.ajax({
		url: './ka-'+Date.now(),
		method: 'GET'
	});
}