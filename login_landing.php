<?php
	ob_start();

	require_once("config.inc.php");
	include_once("classes/student.php");

	$member = new Student();

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	} else {
		$logger = new Syslog();
        $logger->log("login", "student", ["member_id"=> $member->getId(), "para"=>"SUCCESS"], "SUCCESS");
        header("location: privacy_statement");
        exit();
	}

?>