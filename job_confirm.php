<?php
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["Jobs"];
	
	include_once("breadcrumb.php");
	include_once("classes/application.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	

	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	$limit = 10;
	$offset = ($page - 1) * $limit;

	$application = $member->getApplication();
	
	if ($application["status"] != "NEW") {
		header("location: job_preferences");
		exit();
	}

	$application = new Application($application["id"]);
	$joblist = $application->getItems();

?><main id="job_confirm" class="joblist">
		<div class="wrapper">
			<ul class="nav nav-step hidden-xs">
				<li><a href="joblist">Job List</a></li>
				<li><a href="job_preferences">Job Preference <span class="badge"><?=count($joblist)?></span></a></li>
				<li class="active">Confirmation</li>
				<li>Submission</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation"><a href="joblist">Job List</a></li>
				<li role="presentation"><a href="job_preferences">Job Preference (<?=count($joblist)?>)</a></li>
				<li role="presentation" class="active"><a>Confirm / Submit</a></li>
			</ul>
			<div class="container">
				<h1 class="page_header">Confirm Job Application </h1>
				<div class="col-md-12"><?php
				if (!$member->isCompleteApplication()) {
					?><div class="alert alert-warning " role="alert">
					  
						You cannot submit your application since you have not yet complete your application form. Click <a href="new_cv#validate">here</a> to complete first.
					</div><?
				}
				?></div>
				<section id="joblist_wrapper" class="col-md-12">
					<div class="row hidden-xs">
						<div class="col-sm-1 text-center" ><label>Priority</label></div>
						<div class="col-sm-6" ><label>Job Post</label></div>
						<div class="col-sm-4"><label>Company</label></div>
						
						
					</div>
					<ul id="job_preference" >
						<?php
							$i = 1;
							foreach ($joblist as $job) {
                                $company = ($job["job"]["company"] == "" || $job["job"]["dept"] == "") ? "%s%s" : "%s (%s)";
                                $company = sprintf($company, $job["job"]["company"], $job["job"]["dept"]);
						?><li>
							<div class="col-xs-1 text-center" ><?=$job["priority"]?></div>
							<div class="col-xs-10 col-sm-6 "><label class="position"><?=$job["job"]["name"]?></label></div>
							<div class="col-sm-4 col-xs-offset-1 col-sm-offset-0"><div class="company"><?=$company?></div></div>
							<?php
							}
						?>
					</ul>
					<div class="bottomRow">
						<div class="col-md-12">
							<a href="job_preferences" class=" pull-left" ><i class="fa fa-caret-left" ></i> Back</a>
							<?php
					if ($member->isCompleteApplication()) {

						?><a href="job_disclaimer" class="pull-right">Next <i class="fa fa-caret-right" ></i></a><?php } 
						?></div>
					</div>
					
				</section>
			</div>
		</div>
		<script type="text/javascript" src="scripts/wie_app.js"></script>
	</main>
<?php
	include_once("footer.php");
?>