<?php
	ob_start();
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["Jobs"];
	


	include_once("breadcrumb.php");
	include_once("classes/application.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	

	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	$limit = 10;
	$offset = ($page - 1) * $limit;

	$application = $member->getApplication();
	$detail  = $member->getDetail();
	$application_status = $application["status"];
	$submission_date = $application["submission_date"];
	
	$application = new Application($application["id"]);
	$joblist = $application->getItems();

	if (count($joblist) == 0){

		header("location: joblist.php");
		exit();
	}
    
    


?><main id="joblist" class="joblist">
		<div class="wrapper">
			<ul class="nav nav-step hidden-xs">
				<li><a href="joblist.php">Job List</a></li>
				<li class="active">Job Preference <span class="badge"><?=count($joblist)?></span></li>
				<li>Confirmation</li>
				<li>Submission</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation"><a href="joblist.php">Job List</a></li>
				<li role="presentation" class="active"><a >Job Preference (<?=count($joblist)?>)</a></li>
				<li role="presentation" class="disabled"><a>Confirm / Submit</a></li>
			</ul>
			<div class="container">

				<h1 class="page_header">My Job Preference</h1>
				<div class="col-md-12"><?php
					if (!$member->isCompleteApplication()) {

						?><div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							You cannot submit your application since you have not yet complete your application form. Click <a href="new_cv#validate">here</a> to complete first.
						</div><?
					}
				?></div>
				<div class="">
				<section id="joblist_left" class="col-md-2 hidden-sm hidden-xs">
					<div ><data data-cms-title="job_app_preference"><?=$cmsObj->getPageArea("job_app_preference");?></data></div>
				</section>
				
				<section id="joblist_wrapper" class="col-md-10">
					<div class="headrow">
						<div class="col-xs-12 col-sm-6 col-sm-offset-6 text-right">Maximum Number of Job Preferences: <?=$detail["program"]["max_job"]?></div>
					</div>
					<div class=" hidden-xs">
						<div class="row">
							<div class="col-sm-1 text-center" ><label>Priority</label></div>
							<div class="col-sm-8 " ><label>Job Post</label></div>
							<div class="col-sm-3 hidden-xs text-center"><label>Action</label></div>
						</div>
					</div>
					<ul id="job_preference" >	
						<?php
							$i = 1;
							foreach ($joblist as $job) {
						
                                $company = ($job["job"]["company"] == "" || $job["job"]["dept"] == "") ? "%s%s" : "%s (%s)";
    
                                $company = sprintf($company, $job["job"]["company"], $job["job"]["dept"]);
                        ?><li >
                        	<div class="row">
								<div class="col-xs-1 text-center" ><?=$i++?></div>
								<div class="col-sm-8 col-xs-7"><div class="position"><label><?=$job["job"]["name"]?></label></div>
												<div class="company"><?=$company?></div></div>
								
									<?php
									if ($application_status == "OPEN") {
										$status	= array(""=>"Awaiting", "closed"=>"Rejected", "offered"=>"Offered");				
										?><div class="col-xs-3">
										<div class="text-center"><?=$status[$job["status"]]?> </div>
										</div><?
									} else {
									?><div class="col-sm-3 col-xs-12  text-right">
									<a class="btn btn-up btn-sm" data-id="<?=$job["id"]?>" data-priority="<?=$job["priority"] - 1?>"><i class="fa fa-arrow-up"></i></a>
									<a class="btn btn-down btn-sm"  data-id="<?=$job["id"]?>" data-priority="<?=$job["priority"] + 1?>"><i class="fa fa-arrow-down"></i></a>
									<a data-id="<?=$job["id"]?>" class="btn btnDelete btn-default btn-sm">Remove</a></div><?php
									}
									?>
							</div></li><?php
							}
						?>
					</ul>
					<div class="row">
						<div class="col-sm-12 text-left" ><?php if ($application_status == "OPEN") {?><label>Submission Date: </label> <?=$submission_date?><?php } ?></div>
					</div>
					<div class="bottomRow">
						<div class="col-md-12">
							<a href="joblist" class=" pull-left" ><i class="fa fa-caret-left" ></i> Back</a>
							<?php if ($application_status != "OPEN") {?><a href="job_confirm" class="pull-right">Next <i class="fa fa-caret-right" ></i></a><?php } ?>
						</div>
					</div>
				</section>

			</div>
		</div>
		<script type="text/javascript" src="scripts/wie_app.js"></script>
	</main>
<?php
	ob_end_flush();
	include_once("footer.php");
?>