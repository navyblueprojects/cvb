<?php
	require_once("config.inc.php");
	require_once("classes/student.php");

	$member = Student::retain();
	
	if ($member->isLogined()){
		header("location: home");
		exit();
	}	
	if ($member->check_saml_login()){
		header("location: home");
		exit();
	} else {
		$member->start_login();
		exit();
	}	



	include_once("header.php");
	$pageName = "Login";
	include_once("breadcrumb.php");

?>
	<main id="login">
		<div class="wrapper">
			<div class="container">
				<h1 class="page_header">Login</h1>
				<section id="loginform_wrapper" class="col-md-6 ">
					<div >
						<h6 class="hide">Restricted Area.</h6>
						<div class="cms" data-cms-title="login_text"><?=$cmsObj->getPageArea("login_text")?></div>
						
						<div class="row"><form id="loginform" class="form col-md-11 " method="POST">
							<div class="form-group">
								<label for="student_id">Student ID</label>
								<input type="text" id="student_id" name="student_id" value="" autocomplete="off" class="form-control" />
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" id="password" name="password" value="" class="form-control" autocomplete="off" />
							</div>
							<div class="form-group">
								<label for="captcha">CAPTCHA</label>
								<div class="row">
									<div class="col-md-4 col-xs-6"><input type="text" maxlength="4" id="captcha" name="captcha" class="form-control " autocomplete="off"  /></div>
									<div class="col-md-4 col-xs-6"><img src="captcha" alt="captcha" style="border: 1px solid grey" id="img_captcha"/> <a  id="reloadCaptcha" ><i class="fa fa-redo" ></i></a></div>

								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-primary">Login</button>
								
								<p><a href="https://ssoportal.cpce-polyu.edu.hk/passwordrecovery/" rel="noreferrer noopener" target="_blank">Forgot Password?</a></p>
							</div>
						</form></div>
					</div>
				</section>
				<section id="announce_wrapper" class="col-md-6  ">
					<div class="announce_card">
						<h2>News</h2>
						<div class="cms" data-cms-title="login_news"><?=$cmsObj->getPageArea("login_news")?></div>
					</div>
					<div  class="announce_card">
						<h2>Reminder</h2>
						<div class="cms" data-cms-title="login_reminders"><?=$cmsObj->getPageArea("login_reminders")?></div>
					</div>
				</section>
				<div class="hide alert alert-danger alert-dismissible alertLogin" role="alert" >
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <span class="errorMsg"></span>
				</div>
			</div>
		</div>
	</main>
<?php
	include_once("footer.php");
?>