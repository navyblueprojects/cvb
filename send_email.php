<?php
require($rootPath.$appPath."/config.inc.php"); # config
include_once($pathClassLib."/class.phpmailer.php");
include_once($pathClassLib."/class.smtp.php");

function sendMail($subject, $content, $email, $fromEmail = array(), $bccEmail = array(), $attach = false) {
	global $docTitle, $domain, $contactEmail, $enquiryEmail, $pathClassLib;
	
	$fromEmail = (empty($fromEmail)) ? array($docTitle, $contactEmail[0]) : $fromEmail;
	
	$mailer = new PHPMailer();
	$mailer->ClearAddresses();
	$mailer->ClearReplyTos();

	$mailer->SetLanguage("en", $pathClassLib."/language/");
	
	$mailer->isSMTP();
	$mailer->SMTPAuth = false;
	$mailer->Host = "192.168.66.129";
	$mailer->Port = "25";
	//$mailer->SMTPSecure = 'tls';
	$mailer->do_debug = 2;
	$mailer->Priority = 3;
	$mailer->Encoding = "8bit";
	$mailer->CharSet = "utf-8";
	
	$mailer->From = $fromEmail[1];
	$mailer->FromName = $fromEmail[0];
	
	$mailer->Subject = $subject;
	$mailer->IsHTML(true);
	$mailer->Body = $content;
	$mailer->Helo = "MYPC01";


	$mailer->AddReplyTo($fromEmail[1]);
	$mailer->AddAddress($email);
	foreach($bccEmail as $eachEmail) {
		$mailer->AddBCC($eachEmail);
	}

	if ($attach !== FALSE) {
		$mailer->AddAttachment($attach);
	}
	
	if ($mailer->Send()) {
		return true;
	} else {
		return $mailer->ErrorInfo;
	}



}
?>