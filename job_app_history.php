<?php
	ob_start();
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["Jobs"];
	include_once("breadcrumb.php");
	

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	

	$page = array_key_exists("page", $_GET) ? $_GET["page"] : 1;
	$limit = 10;
	$offset = ($page - 1) * $limit;

	$joblist = $member->getApplicationHistory($offset, $limit);
	
	$null = true;

	$status	= array(""=>"Awaiting", "closed"=>"Rejected", "offered"=>"Offered");				
										

?><main id="joblist" class="joblist">
		<div class="wrapper">
			
			<div class="container">
				<h1 class="page_header">WIE Application History</h1>

				<div class="row">
					<section id="" class="col-md-12">
							<?php
								$i = 1;
								if (count($joblist > 0)) {
									foreach ($joblist as $job) {
										
										if ($job["items"]) {
											$null = false;
										?><div class=" application">
							<div class="  application_header row">
								<div class="col-xs-6 col-sm-3 text-left"><strong>Application No</strong></div>
								<div class="col-xs-6 col-sm-3 text-left"><span><?=$job["application"]["app_no"]?></span></div>						
								<div class="col-xs-6 col-sm-3 text-left"><strong>Submission Date </strong></div>
								<div class="col-xs-6 col-sm-3 text-left" style="white-space: nowrap;"><span><?=$job["application"]["submission_date"]?></span></div>						
							</div>
							<div class="hidden-xs">
								<div class="col-sm-3 text-left"><label>Priority</label></div>
								<div class="col-sm-7 text-left"><label>Position</label></div>
								<div class="col-sm-2 text-center"><label>Status</label></div>
							</div><ul class=" job_preference" ><?php
									
										foreach($job["items"] as $item) {
								?><li>
									<div class="row">
										<div class="col-sm-3 col-xs-1 text-left"><span><?=$item["priority"]?></span></div>
										<div class="col-sm-7 col-xs-10 text-left"><span><label><?=$item["job"]["name"]?></label>
											<div><?=$item["job"]["company"]?></span></div></div>
										<div class="col-sm-2 col-xs-12 status"><span><?=$status[$item["status"]]?></span></div>
									</div>
								</li><?php
										}

										?></ul></div><?
										}
									}
								}
								
								if ($null) {

							?><div class=" application text-center"><h2 style="margin: 3em 0;">No application records.</h2></div><?php
								}
							?>
							
						
					</section>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="scripts/wie_app.js"></script>
	</main>
<?php
	ob_end_flush();
	include_once("footer.php");
?>