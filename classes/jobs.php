<?php    
    include_once($rootPath.$appPath."/classes/cms.php");
    include_once(__DIR__."/system.php");
    require_once(__DIR__."/application.php");
    require_once(__DIR__."/lecturer.php");

    class Jobs extends CMS{        
              
        protected $tableName = "jobs";  
        public $pageName = "Job";        // CMS Page Name
        public $permissionTable = "jobs_permission";        // CMS Page Name
        public $lecturerTable = "jobs_lecturer";        // CMS Page Name


        public $__cms_fields = array();

        function __construct($id=false){

            parent::__construct();

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            
            $this->__cms_fields[] = new CMSObj("VARCHAR", "job_no", ["label"=>"Job No", "list"=>["show", "search", "sort"], "edit"=>["show", "insert", "readonly"], "width"=>200]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "name", ["label"=>"Position", "width"=>300]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "company", ["label"=>"Company", "width"=>250]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "dept", ["label"=>"Department", "width"=>250]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "vacancy", ["label"=>"Vacancy", "list"=>[], "width"=>150]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "salary", ["label"=>"Salary/Allowance", "list"=>[], "width"=>150]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "work_hour", ["label"=>"Working Hours", "list"=>[], "width"=>150]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "work_day", ["label"=>"Working Days", "list"=>[], "width"=>150]);

            $this->__cms_fields[] = new ForeignObj( "post", ["label"=>"Type", "list"=>[],
                                                                            "options"=>["multiple"=>"multiple", "ft"=>[["table"=>"jobs_type", "value"=>"title", "label"=>"title"]]],]);
            
            $this->__cms_fields[] = new CMSObj("TEXTAREA", "duty", ["label"=>"Duty", "list"=>["search"]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "period", ["label"=>"Period", "list"=>[]]);
            //$this->__cms_fields[] = new CMSObj("TEXTAREA", "prog_code", ["label"=>"Program Code", "list"=>["search"]]);
            //$this->__cms_fields[] = new CMSObj("TEXTAREA", "subj_code", ["label"=>"Subject Code", "list"=>["search"]]);
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "open_date", ["label"=>"Open Date", "list"=>[]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "close_date", ["label"=>"Close Date", "list"=>[]]);

            $this->__cms_fields[] = new CMSObj("TEXTAREA", "remarks", ["label"=>"Remarks", "list"=>["search"]]);
            //$this->__cms_fields[] = new ForeignObj("user_id", ["label"=>"User ID", "list"=>["show", "sort"], "options"=>["ft"=>[["table"=>"account", "value"=>"id", "label"=>"username"]]], "width"=>"140" ]);
            
            //$this->__cms_fields[] = new CMSObj("VARCHAR", "max_select", ["label"=>"Max Select", "list"=>["search"], "options"=>["tips"=>"Maximum number of Job Preference"]]);
            $this->__cms_fields[] = new FileObj("VARCHAR", "attachment", ["label"=>"Attachment 1", "list"=>[]]);
            $this->__cms_fields[] = new FileObj("VARCHAR", "attachment02", ["label"=>"Attachment 2", "list"=>[]]);
            $this->__cms_fields[] = new FileObj("VARCHAR", "attachment03", ["label"=>"Attachment 3", "list"=>[]]);
            

            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "width"=>"170", "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "width"=>"170", "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);

            if ($id) {
                $this->id = $id;
            }
        }

        public function createByArray($postarr){ 
            global $id, $member, $dbDatabase;
            $data = array();

            foreach ($this->__cms_fields as $fields) {
                if (array_key_exists($fields->fieldName, $postarr)) {
                    $data[$fields->fieldName] =  $postarr[$fields->fieldName];
                }
            }
            $data["lastModDate"] = date("Y-m-d H:i:s");
            $data["post"] = implode(";", $data["post"]);
            
            if ($postarr["id"] == "") {
               
               
                $data["status"] = "E";
                $data["createDate"] = date("Y-m-d H:i:s");
                
                $this->conn->AutoExecute($this->tableName, $data, "INSERT");      
                                      
                $this->id = $this->conn->insert_Id();
                $this->conn->AutoExecute($this->tableName, ["job_no"=>"CVB".sprintf("%06s", $this->id)], "UPDATE", "id = '".$this->id."'");

                $permission = array();
                $permission["lecturer_id"] = $member->getId();
                $permission["job_id"] = $this->id;
                $pp["READ"] = 1;
                $pp["EDIT"] = 1;
                $pp["DELETE"] = 1;
                $permission["permission"] = json_encode($pp, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);

                $this->conn->AutoExecute($this->lecturerTable, $permission, "INSERT");

                session_start(); $_SESSION[$dbDatabase . "_" . md5("SYSTEM_MSG")] = "Record saved. Please add the target students."; session_write_close();
                
            } else {
                
                $this->id = $postarr["id"];
                $this->conn->AutoExecute($this->tableName, $data, "UPDATE", "id = '".$this->id."'");

                $this->conn->Execute("DELETE from ".$this->permissionTable." where id NOT IN (".implode(",", array_keys($postarr["years"])).") AND job_id = '".$this->id."'");
                $this->conn->Execute("DELETE from ".$this->lecturerTable." where id NOT IN (".implode(",", array_keys($postarr["n_lecturer"])).") AND job_id = '".$this->id."'");
                
                // Target Student
                foreach ($postarr["years"] as $idx=>$years) {

                    if ($idx !== "new") {
                        $jobs_permission = array();
                        $jobs_permission["years"] = $years;
                        $jobs_permission["program_id"] = $postarr["program_id"][$idx];
                        
                        $this->conn->AutoExecute($this->permissionTable, $jobs_permission, "UPDATE", "id = '$idx'");    
                    

                    } else {

                        foreach ($years as $key => $value) {
                            if ($value != "") {
                                $jobs_permission = array();
                                $jobs_permission["years"] = $value;
                                $jobs_permission["job_id"] = $this->id;
                                $jobs_permission["program_id"] = $postarr["program_id"]["new"][$key];

                                $this->conn->AutoExecute($this->permissionTable, $jobs_permission, "INSERT");
                                
                            }
                        }
                    }
                }

                foreach ($postarr["n_lecturer"] as $idx=>$value) {
                    $jobs_lecturer = array();
                    $jobs_lecturer["lecturer_id"] = $value;



                    $pp["READ"] = (int)$postarr["n_permission"][$idx]["READ"];
                    $pp["EDIT"] = (int)$postarr["n_permission"][$idx]["EDIT"];
                    $pp["DELETE"] = (int)$postarr["n_permission"][$idx]["DELETE"];
                    $jobs_lecturer["permission"] = json_encode($pp, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);

                    $this->conn->AutoExecute($this->lecturerTable, $jobs_lecturer, "UPDATE", "id = '$idx'");
                }

                $jobs_lecturer = array();
                $jobs_lecturer["job_id"] = $this->id;

                foreach ($postarr["lecturer"] as $idx=>$value) {
                    
                    $jobs_lecturer["lecturer_id"] = $value;

                    $pp["READ"] = (int)$postarr["permission"]["READ"][$idx];
                    $pp["EDIT"] = (int)$postarr["permission"]["EDIT"][$idx];
                    $pp["DELETE"] = (int)$postarr["permission"]["DELETE"][$idx];
                    $jobs_lecturer["permission"] = json_encode($pp, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);

                    $this->conn->AutoExecute($this->lecturerTable, $jobs_lecturer, "INSERT");
                   
                }
                session_start(); $_SESSION[$dbDatabase . "_" . md5("SYSTEM_MSG")] = "Record saved."; session_write_close();

            }
        }

        public function getList($mywhere=array()){

            return $this->get($this->tableName, $mywhere);

        }

        public function getPermission() {

            $data =  $this->get($this->permissionTable, ["where"=>["job_id = '".$this->id."'"]]);
            foreach ($data as &$row) {
                $program = new Program($row["program_id"]);
                $row["program"] =  $program->getDetail();
                $row["years"] = explode(";", $row["years"]);
            }

            return $data;
        }

        public function getLecturerPermission($lecturer_id = FALSE){
            $mywhere = array();
            $mywhere["where"][] = "job_id = '".$this->id."'";
            
            if ($lecturer_id !== FALSE) {
                $mywhere["where"][] = "lecturer_id = '" . (int)$lecturer_id . "'";
            }

            $data =  $this->get($this->lecturerTable, $mywhere);
            
            foreach ($data as &$row) {
                $row["lecturer"] =  $this->conn->GetRow("Select * from lecturer where id = '".$row["lecturer_id"]."'");
                $row["permission"] = json_decode($row["permission"], true);
            }

            return $data;
        }

        public function getDetailByJobNo($job_no){
            $data = $this->getOne($this->tableName, ["where"=>["job_no = '".$job_no."'"]]);

            return $data;   
        }

        public function getJobByJobNo($job_no){
            $data = $this->getOne($this->tableName, ["where"=>["job_no = '".$job_no."'"]]);
            $this->id = $data["id"];
            
        }

        public function exportXls () {
            global $pathClassLib;
            require_once(__DIR__."/../lib_common/phpexcel/PHPExcel.php");
            

            $thisRow = 1; $thisCol = 0;

            $objPhpExcel = new PHPExcel();

            // Records

            $worksheet = $objPhpExcel->setActiveSheetIndex(0);
           
            foreach ($this->__cms_fields as $field) {
                if ($field->type != "CUSTOM" && $field->fieldName != "id") {
                    if ($field->fieldName != 'node_id' || $this->level > 1) {
                        if (!in_array($field->fieldName, ['createDate', 'lastModDate'])) {
                            
                            $label = $field->label;

                            if (in_array($field->type, ["RADIO", "SELECT", "CHECKBOX"])) {
                                $options = array_map("strip_tags", array_keys($field->options));

                                $label = $field->label ." (" . implode(" / ", $options).")";
                            } 

                            $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  $label);
                        }
                    }
                }
            }

            $records = $this->conn->GetAll($_POST["thissql"]);
            

            foreach ($records as $row) {
                $thisRow++;
                $thisCol = 0;

                
                foreach ($this->__cms_fields as $field) {
                     

                    if ($field->type != "CUSTOM" && $field->fieldName != "id") {
                        if ($field->fieldName != 'node_id' || $this->level > 1) {
                            
                            if (!in_array($field->fieldName, ['createDate', 'lastModDate'])) {
                                

                                if (in_array($field->type, ["RADIO", "SELECT", "CHECKBOX"])) {




                                    $label = $field->label ." (" . implode(" / ", $options).")";
                                    $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  
                                         strip_tags(array_search($row[$field->fieldName], $field->options))
                                    );
                                } else {
                                    $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  $row[$field->fieldName]);
                                }
                           
                                
                            }
                        }
                    }
                }

            }
            //exit();



            return $objPhpExcel;
        }

        public function getId(){
            return $this->id;
        }
        
        public function uploadFile($field) {
            $filesArray = array();
            $return = array();
            
            $fileObj = new FileObj("FILE", $field);
            $fileObj->setUpload($filesArray);

            foreach ($filesArray as $idx=>$value) {
                $return["type"] = $idx;
                $return["filename"] = $value;
                $return["url"] = UFILE_PATH . $value;

                $this->conn->AutoExecute($this->tableName, [$idx=> $value], "UPDATE", "id = '".$this->id."'");
            }
            return $return;
        }


        public function removeFile($field) {

            $this->conn->AutoExecute($this->tableName, [$field=> ""], "UPDATE", "id = '".$this->id."'");
            return array("status"=>1, "msg"=>"ok");
        }


        public function getXhrList($field, $q=""){
            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "job_no"=>"job_no",  "company"=>"company",  "dept"=>"dept", "name"=>"name",  );
            $mywhere["limit"] = 999;
            

            $flds = array(
                'jobs.job_no'=>"job_no",
                'jobs.company'=>"company",
                'jobs.dept'=>"dept",
                'jobs.name'=>"name",
            );

            if ($q != "") {
                $mywhere["where"][] = $flds[$field] . " LIKE '%".$q."%'";
                $mywhere["order"][] = $flds[$field];
            }
            
            $list = $this->get($this->tableName, $mywhere);
            $result = array();
            foreach ($list as $li) {
                $result["results"][] = array(
                    "id" => $li[$flds[$field]], 
                    "text" => $li[$flds[$field]],
                );
            }

            return $result;
        }
        
        public function getDetails(){                   
            $details = $this->getOne($this->tableName, ["where"=>["id = '".$this->id."'"]]);
            
            return $details;
        }

        public function delete($member){

            $appl = new Application();
            
            $details = $this->getLecturerPermission($member->getId());

            if ($details[0]["permission"]["DELETE"] == 1) {
                $list = $appl->findApplicationsByJobId($this->id);
                if (count($list) > 0) {
                    return array("status"=>0, "msg"=>"Deletion failed. The job post has already been applied.");
                } else {
                    $this->conn->Execute("UPDATE jobs SET status = 'X' where id = '".$this->id."'");
                    $this->conn->Execute("DELETE FROM jobs_lecturer where job_id = '".$this->id."'");
                    $this->conn->Execute("OPTIMIZE TABLE jobs_lecturer");
                    return array("status"=>1, "msg"=>"OK");
                }

            } else {
                return array("status"=>0, "msg"=>"You do not have permission to delete this post");
            }

            
        }

        public function cloneJob($member){ 

            $org_id = $this->id;

            // 1. Clone Job
            $data = $this->getOne($this->tableName, ["where"=>["id = '".$org_id."'"]]);
            unset($data["id"]);
            $this->conn->AutoExecute($this->tableName, $data, "INSERT");
            $this->id = $this->conn->insert_Id();

            $this->conn->AutoExecute($this->tableName, ["job_no"=>"CVB".sprintf("%04s", $this->id)], "UPDATE", "id = '".$this->id."'");

            // 2. Clone Lecturer Permission
            $permission = array();
            $permission["lecturer_id"] = $member->getId();
            $permission["job_id"] = $this->id;
            $pp["READ"] = 1;
            $pp["EDIT"] = 1;
            $pp["DELETE"] = 1;
            $permission["permission"] = json_encode($pp, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);

            $this->conn->AutoExecute($this->lecturerTable, $permission, "INSERT");

            $mywhere = array();
            $mywhere["fields"] = array("lecturer_id", "permission");
            $mywhere["where"] = array("job_id = '".$org_id."'", "lecturer_id <> '".$member->getId()."'");
            $lecturerRows = $this->get($this->lecturerTable, $mywhere);

            foreach ($lecturerRows as $row) {
                $row["job_id"] = $this->id;
                $this->conn->AutoExecute($this->lecturerTable, $row, "INSERT");
            }

            $mywhere = array();
            $mywhere["fields"] = array("program_id", "years");
            $mywhere["where"] = array("job_id = '".$org_id."'");
            $permissionRows = $this->get($this->permissionTable, $mywhere);

            foreach ($permissionRows as $row) {
                $row["job_id"] = $this->id;
                $this->conn->AutoExecute($this->permissionTable, $row, "INSERT");
            }

            $url = "CVB".sprintf("%04s", $this->id)."-".urlencode($data["name"]."@".$data["dept"]."@".$data["company"]);

            return array("status"=>1, "id"=>$this->id, "url"=>$url, "msg"=>"Record Duplicate. Press Close to go to edit");


        }
    }
?>