<?php
    require_once($adoDBPath.'/adodb.inc.php'); # load code common to ADOdb   
    require_once($pathClassLib.'/function.common.php');
    require_once(__DIR__.'/cmsobj.php');


    class CMS {
        protected $id;
        protected $conn;
        protected $tableName = "page";  
        public $pageName = "Web CMS";        // CMS Page Name
        public $admin_js; 
        public $__cms_fields;

        function __construct($tableName="", $pageName=""){
            global $dbHost,$dbUser,$dbPassword,$dbDatabase,$debugMode;

            if (!isset($GLOBALS["conn"])) {
                $conn = &ADONewConnection("mysqli");  # create a connection
                $conn->PConnect($dbHost,$dbUser,$dbPassword,$dbDatabase);# connect to MySQL 
                $conn->debug = $debugMode;
                $conn->Execute("SET NAMES 'utf8'"); 
                
                $GLOBALS["conn"] = $conn;
            }

            $this->conn = $GLOBALS["conn"];              
            
            
            if ($pageName != "") {
                $this->pageName = $pageName;
            }
            if ($tableName != "") {
                $this->tableName = $tableName;
            }
            $this->admin_js = [];
            
            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            
            $this->__cms_fields[] = new SelectObj("RADIO", "type", ["label"=>"Type", "list"=>["show", "sort"], "edit"=>["show", "insert", "readonly"], 
                                                                    "options"=>["Page"=>"page",
                                                                                "Area"=>"area",
                                                                                "Form Control"=>"tips"],
                                                                    "width"=>200]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "tag", ["label"=>"Tag", "width"=>300, "readonly"=>true, "options"=>[]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "pageName", ["label"=>"Page Name", "list"=>[],  "width"=>300, "options"=>["tips"=>"Page Title for page, Placeholder for form control"]]);
            $this->__cms_fields[] = new CMSObj("TEXT", "content", ["label"=>"Content ", "width"=>250, "list"=>["search"], "options"=>["tips"=>"Content for page, Tips text for from control"]]);
             
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "width"=>"170", "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "width"=>"170", "edit"=>["insert", "update"]]);
            
        }



        public function getPageArea($tag){
            $html = $this->getOne($this->tableName, ["where"=>["tag = ?", "type = ?"], "where_parameters"=>[$tag, 'area']]);
            return html_entity_decode($html["content"], ENT_QUOTES, "UTF-8");
        }

        public function getPage($tag){
            $html = $this->getOne($this->tableName, ["where"=>["tag = ?", "type = ?"], "where_parameters"=>[$tag, 'page']]);
            return $html;
        }

        public function getPageList(){
            $html = $this->get($this->tableName, ["where"=>["status = ?", "type = ?"], "where_parameters"=>['E', 'page']]);
            return $html;
        }

        public function getTips($tag){
            $html = $this->getOne("page", ["where"=>["tag = ?", "type = ?"], "where_parameters"=>[$tag, 'tips']]); 

            return html_entity_decode($html["content"], ENT_QUOTES, "UTF-8");
        }
        
        protected function reconnect(){
            // reconnect to mysql after the object is unserialized
            $this->conn = $GLOBALS["conn"];
        }
        
        public function get($tableName, $para=array(), $debug=false){
            $param = array("fields"=>array(0=>"*"), 
                               "offset"=>0,
                               "limit"=>9999,
                               "where"=>array(),
                               "where_parameters"=>array(),
                               "order"=>array());
			if ($para) {
				foreach ($para as $fields=>$value) {
					$param[strtolower($fields)] = $value;
				}
			}
            
            if ($param["fields"][0] == "*") {
                $fields = "*";
            } else {
                $fields = array();
                foreach ($param["fields"] as $idx=>$val){
                    if (is_string($idx)){ // ALIAS
                        $fields[] = $val . " AS " . $idx;
                    } else {
                        $fields[] = $val;
                    }
                }
                $fields = implode("," , $fields);

            }

            $thissql = "SELECT ".$fields." FROM ".$tableName;
            
            if ($param["where"]) {
                $thissql .= " WHERE " . implode(" AND ", $param["where"]);
            }
            if ($param["order"]) {
                $thissql .= " ORDER BY ". implode(", ", $param["order"]);                
            }
            $thissql .= " LIMIT ".$param["offset"].", ".$param["limit"];
            if ($debug) {
				echo $thissql;
			}
            if (array_key_exists("where_parameters", $param)) {
            	
                $query = $this->conn->prepare($thissql);
                return   $this->conn->getAll($query, $param["where_parameters"]);
            } else {
                return $this->conn->getAll($thissql);
            }
        }

        public function importXLS($fileobject) {
            global $pathClassLib;
            require_once(__DIR__."/../lib_common/phpexcel/PHPExcel.php");

            
            try {
                $tmp_name = $fileobject["tmp_name"];

                $objPHPExcel = PHPExcel_IOFactory::load($fileobject["tmp_name"]);
                $sheet = $objPHPExcel->getSheet(0); 

                $highestRow = $sheet->getHighestRow(); 
                $highestColumn = $sheet->getHighestColumn();

                for ($row = 1; $row <= $highestRow; $row++) {
                    $rows = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                }

                foreach ($rows as $row) {
                    # code...
                    $resultRows = $rrow = array();
                    $i = 0;
                    foreach ($this->__cms_fields as $field) {
                        if ($field->type != "CUSTOM" && $field->fieldName != "id") {
                            if ($field->fieldName != 'node_id' || $this->level > 1) {
                                if (!in_array($field->fieldName, ['createDate', 'lastModDate'])) {
                                    $rrow[$field->fieldName] = $row[$i++];
                                }
                            }
                        }
                    }
                    $this->createRowByArray($rrow);
                }

                return $i;
            } catch (Exception $err) {
                var_dump($err);
                return false;
            }
        }


        public function createRowByArray($row) {
            require_once(__DIR__."/../lib_common/phpexcel/PHPExcel.php");

            $dataRow = array();
            foreach ($this->__cms_fields as $fieldObj) {
                
                $skipFields = array("id", "createDate", "lastModDate");

                if ($this->level > 1) {
                    $skipFields[] = "node_id";
                }

                if (!in_array($fieldObj->fieldName, $skipFields)) {
                    switch($fieldObj->type) {
                        case "CUSTOM" :
                        case "PHOTO" :
                            break;
                        case "INT" :
                            $dataRow[$fieldObj->fieldName] = (int)$row[$fieldObj->fieldName];
                            break;
                        case "DATE":
                            $dataRow[$fieldObj->fieldName] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($row[$fieldObj->fieldName]));
                            break;
                        case "FOREIGN" :
                            $ft = $fieldObj->options["ft"][0];
                            $sql = "Select " . $ft["value"] . " from " . $ft["table"] . " where " . $ft["label"] . " like concat('%".$row[$fieldObj->fieldName]."%') order by id desc";
                            $dataRow[$fieldObj->fieldName] = $this->conn->GetOne($sql);
                            break;
                        case "RADIO":
                            foreach ($fieldObj->options as $key => $value) {
                                if ($row[$fieldObj->fieldName] == strip_tags($key)) {
                                    $dataRow[$fieldObj->fieldName] = $value;
                                    break;
                                }
                            }
                            break;
                        case "PASSWORD" :
                            $dataRow[$fieldObj->fieldName] = sha1($row[$fieldObj->fieldName]);
                            break;
                        default:
                            $dataRow[$fieldObj->fieldName] = $row[$fieldObj->fieldName];
                    }
                }     
            }
            $dataRow["createDate"] = $dataRow["lastModDate"] = date("Y-m-d H:i:s");
            $this->conn->AutoExecute($this->tableName, $dataRow, "INSERT");
            return $this->conn->insert_Id();
        }

        public function exportXlsTemplate () {
            global $pathClassLib;
            require_once(__DIR__."/../lib_common/phpexcel/PHPExcel.php");
            

            $thisRow = 1; $thisCol = 0;

            $objPhpExcel = new PHPExcel();

            $worksheet = $objPhpExcel->setActiveSheetIndex(0);
           
            // Records 
            foreach ($this->__cms_fields as $field) {
                if ($field->type != "CUSTOM" && $field->fieldName != "id") {
                    if ($field->fieldName != 'node_id' || $this->level > 1) {
                        if (!in_array($field->fieldName, ['createDate', 'lastModDate'])) {
                            
                            $label = $field->label;

                            if (in_array($field->type, ["RADIO", "SELECT", "CHECKBOX"])) {
                                $options = array_map("strip_tags", array_keys($field->options));

                                $label = $field->label ." (" . implode(" / ", $options).")";
                            } 

                            $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  $label);
                        }
                    }
                }
            }
            $records = $this->conn->GetRows($_POST["thissql"]);


            return $objPhpExcel;
        }
      


        public function getOne($tableName, $para=array()) {
            $para["limit"] = 1;
            $data = $this->get($tableName, $para);
            
            return $data[0];
        }
        
        public function getCat($tableName, $node_id){
            $thissql = "SELECT * FROM ".$tableName."_node ";
            $thissql .= "WHERE node_id = '$node_id'";
            
            return $this->conn->getRow($thissql);
        }
        public function getId(){
            return $this->id;
        }

        public function getCate($tableName, $node_head=0, $node_level=0){
            $thissql = "SELECT * FROM ".$tableName."_node ";
            $mywhere = array("1");
            if ($node_head){
                $mywhere[] = "node_parent = '$node_head'";
            }
            if ($node_level){
                $mywhere[] = "node_level = '$node_level'";
            }            
            $thissql .= "WHERE " . implode(" AND ", $mywhere) ." ORDER BY node_left";
            return $this->conn->getAll($thissql);
        }
        
        public function getDetail($tableName, $id){
            $thissql = "SELECT * FROM ".$tableName." WHERE id = '$id' "; 
            $detail = $this->conn->getRow($thissql); 
            $detail["cat"][0] =  $this->conn->getRow("Select * from ".$tableName."_node where node_id = '".$detail["node_id"]."'"); 
            while ($detail["cat"][0]["node_parent"] > 1){
                $thissql = "Select * from ".$tableName."Node where node_id = '".$detail["cat"][0]["node_parent"]."'";
                $detail["cat"][] = $detail["cat"][0];
                $detail["cat"][0] = $this->conn->getRow($thissql); 
                
            }
            return $detail;
        }

        public function checkLogin(){
            global $dbDatabase;
            if (array_key_exists($dbDatabase, $_SESSION)) {
                
                if (array_key_exists("PAGE_EDIT", $_SESSION[$dbDatabase])) {
                    return true;
                }

            }

             return false;
        }

        public function adminLog($id, $account_id, $action, $remarks="") {
            $log = array();
            $log["account_id"] = $account_id;
            $log["cms"] = $this->tableName;
            $log["action"] = $action;
            $log["object_id"] = $id;
            $log["ipaddr"] = $_SERVER["REMOTE_ADDR"];
            $log["remarks"] = $remarks;
            $log["status"] = 1;

            $this->conn->AutoExecute("admin_log", $log, "INSERT");
        }

        public function getAdminLogs($id, $limit=0) {

            $mywhere = array();
            $mywhere["where"] = ["cms = ?", "object_id = ?"];
            $mywhere["where_parameters"] = [$this->tableName, $id];
            $mywhere["order"][] = "createDate desc";

            $mywhere["limit"] = ($limit == 0) ? 5 : $limit;


            $rows = $this->get("admin_log", $mywhere);
            $logs = array();
            
            if ($rows) {
	            foreach ($rows as $log) {
	                $logline = "%s %s the record at %s";

	                $action = array("create"=>"created", "update"=>"updated", "trash"=>"trashed", "restore"=>"restore");

	                $account = $this->conn->GetOne("Select username from account where id = '".$log["account_id"]."'");

	                $logs[] = sprintf($logline, $account, $action[$log["action"]], $log["createDate"]);
	            }
            }
            return $logs;
        }
    }