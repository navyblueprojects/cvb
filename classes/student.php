<?php    
    include_once($rootPath.$appPath."/classes/cms.php");
    require_once(__DIR__."/application.php");
    require_once(__DIR__."/syslog.php");
    require_once(__DIR__."/nqfileobj.php");

    require_once($samlPath."/_include.php"); 

    class Student extends CMS{        

        protected $tableName = "student";  

        private $logined = false;
        public $id;
        public $name;
        public $pageName = "Students";        // CMS Page Name
        public $__cms_fields = array();
        const SESSION_KEY = "polyuhkcc:cvb::student";
        public $cvform;

        function __construct($id=false){

            parent::__construct();

        	$this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            
            $this->__cms_fields[] = new CMSObj("VARCHAR", "student_id", ["label"=>"Student Number", "list"=>["show", "search", "sort"], "edit"=>["show", "insert", "readonly"], "width"=>150]);
            //$this->__cms_fields[] = new CMSObj("PASSWORD", "password", ["label"=>"Password", "list"=>[ ], "edit"=>["show", "insert", "readonly"], "width"=>150]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "wie_no", ["label"=>"WIE No", "list"=>["show", "search", "sort"], "edit"=>["show", "insert", "readonly"], "width"=>150]);

            $this->__cms_fields[] = new SelectObj("RADIO", "is_wie", ["label"=>"Is WIE? ", "list"=>["show"], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Yes</span>"=>"1", "<span style=\"color: #CC0000\">No</span>"=>"0")]);    

            $this->__cms_fields[] = new CustomObj("CUSTOM", "", ["list"=>[], "options"=>["custom"=>["style"=>"color: #5897fb;font-style:italic; padding-top:1em; border-bottom: 1px solid grey", "label"=>"Study Information"]]]);
            
            $this->__cms_fields[] = new ForeignObj( "prog_code", ["label"=>"Programme of Study", "list"=>[],
                                                                            "options"=>["ft"=>[["table"=>"program", "value"=>"id", "label"=>"CONCAT(code, ' ',  name) "]]],]);
            //$this->__cms_fields[] = new CMSObj("VARCHAR", "prog_title", ["label"=>"Programme Title", "list"=>[]]);
            //$this->__cms_fields[] = new CMSObj("VARCHAR", "subj_code", ["label"=>"Subject Code", "list"=>[]]);
            // $this->__cms_fields[] = new CMSObj("VARCHAR", "subj_title", ["label"=>"Subject Title", "list"=>[]]);
            $this->__cms_fields[] = new SelectObj("RADIO", "yrofstudy", ["label"=>"Study Year:", "list"=>[],"options"=>array("Year 1"=>"1", "Year 2"=>"2", "Year 3"=>"3", "Year 4"=>"4" )]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "en_name", ["label"=>"First Name", "list"=>[ "search", "sort"], "width"=>250]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "en_lastname", ["label"=>"Last Name", "list"=>[ "search", "sort"], "width"=>250]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "ch_name", ["label"=>"First Name (中)", "list"=>["search", "sort"],  "width"=>350]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "ch_lastname", ["label"=>"Last Name (中)", "list"=>["search", "sort"],  "width"=>350]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "hkid", ["label"=>"HKID", "list"=>["search"]]);
            $this->__cms_fields[] = new CMSObj("DATE", "dob", ["label"=>"DOB", "list"=>[]]);
            $this->__cms_fields[] = new CMSObj("INT", "age", ["label"=>"Age", "list"=>[]]);
            $this->__cms_fields[] = new SelectObj("RADIO", "gender", ["label"=>"Gender", "list"=>[], "options"=>array("Male"=>"M", "Female"=>"F")]);
            $this->__cms_fields[] = new FileObj("PHOTO", "photo", ["label"=>"Photo", "list"=>[]]);

            $this->__cms_fields[] = new CustomObj("CUSTOM", "", [ "list"=>[],"options"=>["custom"=>["style"=>"color: #5897fb; font-style:italic; padding-top:1em; border-bottom: 1px solid grey",  "list"=>[],  "label"=>"Contact Information"]]]);



            $this->__cms_fields[] = new CMSObj("TEXTAREA", "address1", ["label"=>"Address", "list"=>["search"]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "c_email", ["label"=>"College Email", "list"=>["search"]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "p_email", ["label"=>"Personal Email", "list"=>["search"]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "h_phone", ["label"=>"Phone (Home)", "list"=>["search"]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "m_phone", ["label"=>"Phone (Mobile)", "list"=>["search"]]);
            
            $this->__cms_fields[] = new ForeignObj("district", ["label"=>"Home District", "list"=>["sort"], "options"=>["ft"=>[["table"=>"district", "value"=>"id", "label"=>"name"]]], "width"=>"140" ]);
            $this->__cms_fields[] = new ForeignObj("work_district", ["label"=>"Preferred Work District", "list"=>["sort"], 
                                                    "options"=>["ft"=>[["table"=>"district", "value"=>"id", "label"=>"name"]], "multiple"=>true], "width"=>"140" ]);

            
            $this->__cms_fields[] = new CustomObj("CUSTOM", "", [ "list"=>[],"options"=>["custom"=>["style"=>"color: #5897fb; font-style:italic; padding-top:1em; border-bottom: 1px solid grey", "list"=>[],  "label"=>"Emergency Contact"]]]);


            $this->__cms_fields[] = new CMSObj("VARCHAR", "em_name", ["label"=>"Emergency First Name", "list"=>[]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "em_name2", ["label"=>"Emergency Last Name", "list"=>[]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "em_contact", ["label"=>"Emergency Contact", "list"=>[]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "em_relation", ["label"=>"Relation", "list"=>[]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "em_phone", ["label"=>"Phone No.", "list"=>[]]);

            //$this->__cms_fields[] = new ForeignObj("user_id", ["label"=>"User ID", "list"=>["show", "sort"], "options"=>["ft"=>[["table"=>"account", "value"=>"id", "label"=>"username"]]], "width"=>"140" ]);
            

            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);
            
            if (is_numeric($id)) {
                $this->id = $id;
            } else if ($id) {
                $info = $this->dehashId($id);
                if ($info) {
                    $this->logined = true;  
                    $this->id = $info["id"];
                    $this->name = $info["en_name"];
                    $this->wie = ($info["is_wie"] == 1);
                    $this->save();
                }
            }
        }

        private function save(){
            if (!$_SESSION) {
                session_start();
            }
            $_SESSION[base64_encode(self::SESSION_KEY)] = sha1( self::SESSION_KEY . ($this->getId()));
            session_write_close();
            
        }

        public static function retain() {
            if (!$_SESSION) {
                session_start();
            }


            if (array_key_exists(base64_encode(self::SESSION_KEY), $_SESSION)) {
                $key = $_SESSION[base64_encode(self::SESSION_KEY)];
                session_write_close();
                return new Student($key);
            } else {
                session_write_close();
                return new Student();
            }

        }

        public static function logout() {
            global $domain;

            if (!$_SESSION) {
                session_start();
            }
            
            $_SESSION[base64_encode(self::SESSION_KEY)] = false;
            unset($_SESSION[base64_encode(self::SESSION_KEY)]);
            session_destroy();
            session_write_close();
                
            $authObj = new \SimpleSAML\Auth\Simple("default-sp");   
            $authObj->logout([
                "ReturnTo"=>"https://".$domain."/logout",
                "ReturnStateStage"=>"MyLogoutState",
                "ReturnStateParam"=>"LogoutState"
            ]);
            \SimpleSAML\Session::getSessionFromRequest()->cleanup(); 

        }

        public function getXhrList($q=""){
            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "student_id"=>"student_id");
            $mywhere["limit"] = 999;
            $mywhere["where"][] = "status = 'E'";
            if ($q != "") {
                $mywhere["where"][] = "student_id LIKE '%".$q."%'";
            }
           
            $mywhere["order"][] = "student_id";

            $list = $this->get($this->tableName, $mywhere);
            $result = array();
            foreach ($list as $li) {
                $result["results"][] = array(
                    "id" => (int)$li["id"], 
                    "text" => $li["student_id"]
                );
            }

            return $result;
        }

        public function dehashId($hash) {
            
            $mywhere["where"][] = "sha1(concat('".self::SESSION_KEY."', id)) = ?";
            $mywhere["where_parameters"][] = $hash;

            $row = $this->getOne("student", $mywhere);
            
            return $row;
        }

        public function login($username, $password, $captcha) {
            $this->logined = false;
            $mywhere = array();
            $mywhere[] = "status = ?";
            $mywhere[] = "student_id = ?";
            
            $where_para = array('E', $username);

            $data = $this->get($this->tableName, array("offset"=>0, "limit"=>1, "where"=>$mywhere, "where_parameters"=>$where_para));

            $logger = new Syslog();
            session_start();
            if (md5($captcha) <> $_SESSION["stdt_login_captcha"]) {
                $logger->log("login", "student", ["member_id"=> 0, "para"=>json_encode(["username"=>$username, "result"=>"INCORRECT CAPTCHA"])], "FAILED");
           
                return json_encode(array("status"=>0, "msg"=>"incorrect captcha"));
            } 
            session_write_close();

            if ($data && $this->ldap_login($username, $password)) {
                $this->logined = true;  
                $this->id = $data[0]["id"];
                $this->name = $data[0]["en_name"];
                $this->wie = ($data[0]["is_wie"] == 1);
                $this->save();

                $logger->log("login", "student", ["member_id"=> $this->id, "para"=>"SUCCESS"], "SUCCESS");

                $msg = "SUCCESS";
            } else {

                $logger->log("login", "student", ["member_id"=> 0, "para"=>json_encode(["username"=>$username, "result"=>"FAILED"])], "FAILED");
                $msg = "Incorrect username or password";
            }
            return json_encode(array("status"=>(int)$this->logined, "msg"=>$msg));
        }

        public function getId(){
            return $this->id;
        }

        public function getDetail($mywhere=false){
            if ($mywhere) {
                return $this->getOne($this->tableName, $mywhere);
            } else {
                $data =  parent::getDetail($this->tableName, $this->id);

                $pp = array();
                $pp["where"][] = "id = ?";
                $pp["where_parameters"][] = $data["prog_code"];

                $data["program"] = $this->getOne("program", $pp);
                return $data;
            } 
        }


        public function ldap_login($username, $password) {

            global $student_directory, $student_dn, $student_domain;


            $dsStudent = ldap_connect($student_directory);
            ldap_set_option($dsStudent, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($dsStudent, LDAP_OPT_REFERRALS, 0);

            $return = false;

            if (@ldap_bind($dsStudent, $username . $student_domain, $password)) {
            	foreach ($student_dn as $grp=>$dn) {
					$filter = sprintf("(&(sAMAccountname=%s)(memberOf=CN=%s,OU=Groups,%s))",
								$username,
								$grp,
								$dn);

					$attr = array("cn", "displayName", "description", "mail", "userPrincipalName", "info", "title", "department", "division",
								  "extensionAttribute1", "extensionAttribute2", "extensionAttribute3", "extensionAttribute4",
								  "extensionAttribute5", "extensionAttribute6", "extensionAttribute7", "extensionAttribute8",
								  "extensionAttribute9", "extensionAttribute10", "extensionAttribute11", "extensionAttribute12", "extensionAttribute13");

	                $result = @ldap_search($dsStudent, "OU=Students,".$dn, $filter, $attr);
	                if ($result) {
	                	$return = ldap_get_entries($dsStudent, $result);
	                	if ($return["count"] > 0) {
	                		break;	
	                	}
	                }
	            }
            }

            return $return;
        }

        public function getLastApplication(){
            $mywhere = array();

            $mywhere["offset"] = 0;
            $mywhere["limit"] = 1;
            $mywhere["where"][] = "status = ?";
            $mywhere["where"][] = "member_id = ?";
            $mywhere["where_parameters"][] = "OPEN";
            $mywhere["where_parameters"][] = $this->id;
            $mywhere["order"][] = "id desc";

            $application = $this->getOne("application", $mywhere);
            return $application;
        }

        public function getApplication(){
            $mywhere = array();

            $mywhere["offset"] = 0;
            $mywhere["limit"] = 1;

            $mywhere["where"][] = "status <> ?";
            $mywhere["where_parameters"][] = "CLOSED";
            
            $mywhere["where"][] = "member_id = ?";
            $mywhere["where_parameters"][] = $this->id;
            
            $application = $this->getOne("application", $mywhere);
            
            if (!$application) {
                $application = array();

                $mywhere["where"] = array("member_id = ?");
            	$mywhere["where_parameters"] = array($this->id);

            	$mywhere["order"] = array("createDate desc");
            	
                $application = $this->getOne("application", $mywhere, true);
                
                $last_app_id = $application["id"];
               
                unset($application["id"],$application["app_no"], $application["submi"]);

                $application["member_id"] = $this->id;
                $application["no_attempt"] = $this->conn->GetOne("Select count(*) cc from application where member_id = '".$this->id."'");
                $application["job_selected"] = 0;
                $application["status"] = "NEW";
                $application["createDate"] = date("Y-m-d H:i:s");
                $application["lastModDate"] = date("Y-m-d H:i:s");

                $this->conn->AutoExecute("application", $application, "INSERT");

                $application["id"] = $this->conn->insert_Id();

                $where = array();
                $where["where"][] = "application_id = ?";
                $where["where_parameters"] = array($last_app_id);

                $last_app_meta = $this->get("student_meta", $where);
                
            	if ($last_app_meta) {
                    foreach ($last_app_meta as $meta) {
                    	unset($meta["id"]);
                    	$meta["application_id"] = $application["id"];

                    	$this->conn->AutoExecute("student_meta", $meta, "INSERT");
                    }
                } else {
                    $this->saveMeta($application["id"], "other", array("title"=>"", "html"=>""), "");
                    $this->saveMeta($application["id"], "other", array("title"=>"", "html"=>""), "");
                    $this->saveMeta($application["id"], "other", array("title"=>"", "html"=>""), "");
                }
                $application["is_new"] = true;

            }
            return $application;
        }

        public function getCVFormByHash($hash) {

            $id = $this->getOne("personal_cv", array("where"=>["sha1(concat('app-', id)) = ?", "student_id = ?"], "where_parameters"=>[$hash, $this->id]));
            
            $data =  $this->getCVForm($id["application_id"]);

            $data["personal_cv"]["id"] = $id["id"];

            return $data;
        }

        public function getCVForm($id=false) {

            if ($id) {
                
                $mywhere[] = "member_id = '" . $this->id . "'";
                $application = $this->getOne("application", array("offset"=>0, "limit"=>1, "where"=>["id = '".(int)$id."'"] ));
                if (!$application) {
                    return false;
                }
            } else {
                $application = $this->getApplication();
            }

            $mywhere = array("member_id = '".$this->id."'", "application_id = '" . $application["id"] . "'");

            $data = array();
            $data["personal"] = $this->getOne($this->tableName, array("where"=>array("id = '". $this->id . "'")));
            if ($application["firstname_en"] != "") {
                
                $data["personal"]["en_name"] = $application["firstname_en"];
                $data["personal"]["en_lastname"] = $application["lastname_en"];
                $data["personal"]["ch_name"] = $application["firstname_zh"];
                $data["personal"]["ch_lastname"] = $application["lastname_zh"];
                $data["personal"]["address1"] = $application["address1"];
                $data["personal"]["address2"] = $application["address2"];
                $data["personal"]["address3"] = $application["address3"];
                $data["personal"]["district"] = $application["district"];
                $data["personal"]["work_district"] = $application["work_district"];
                
                $data["personal"]["h_phone"] = $application["h_phone"];
                $data["personal"]["m_phone"] = $application["contact"];
                $data["personal"]["contact"] = $application["contact"];
                $data["personal"]["hkid"] = $application["hkid"];
                $data["personal"]["dob"] = ($application["dob"] == "1970-01-01") ? "" : $application["dob"];
                $data["personal"]["gender"] = $application["gender"];
                $data["personal"]["age"] = $application["age"];
                $data["personal"]["p_email"] = $application["email"];
                $data["personal"]["religion"] = $application["religion"];

                $data["personal"]["em_name"] = $application["em_name"];
                $data["personal"]["em_name2"] = $application["em_name2"];
                $data["personal"]["em_relation"] = $application["em_relation"];
                $data["personal"]["em_phone"] = $application["em_phone"];
                $data["personal"]["em_contact"] = $application["em_contact"];

                $application["gpa"] = json_decode($application["gpa"], true);
                $application["cgpa"] = json_decode($application["cgpa"], true);

            }

            $data["personal"]["district"] = $this->getOne("district", array("where"=>array("id = '". $data["personal"]["district"] . "'")));

            $ids = explode(";", $data["personal"]["work_district"]);

            $data["personal"]["work_district"] = $this->get("district", array("where"=>array("id in ('" . implode("','", $ids) . "')")));
            $data["personal"]["program"] = $this->getOne("program", array("where"=>array("id = '".$data["personal"]["prog_code"]."'")));

            
            $sortMeta = array(
                "education"=>"education",
                "exam_result"=>"exam_result",
                "public_exam"=>"public_exam",
                "prof_qualification"=>array("qualification", "professional"),
                "oth_qualification"=>array("qualification", "other"),
                "work_experience"=>array("experience", "work"),
                "internship_experience"=>array("experience", "internship"),
                "achievement"=>array("achievement", "achievement"),
                "exactivity"=>array("achievement", "exactivity")
            );


            foreach ($sortMeta as $idx=>$val) {
                if (is_array($val)) {
                    $data[$val[0]][$val[1]] = $this->sortMetaByDate($this->get("student_meta", ["where"=>array_merge($mywhere, ["meta_type = '".$idx."'"]) ]), $idx);
                } else {
                    $data[$val] = $this->sortMetaByDate($this->get("student_meta", ["where"=>array_merge($mywhere, ["meta_type = '".$idx."'"]) ]), $idx);
                }
            }


            $data["experience"]["reference"] = $this->get("student_meta", array("where"=>array_merge($mywhere, array("meta_type = 'reference'"))));
            $data["achievement"]["skill"] = $this->get("student_meta", array("where"=>array_merge($mywhere, array("meta_type = 'skill'"))));
            $data["achievement"]["skill_other"] = $this->get("student_meta", array("where"=>array_merge($mywhere, array("meta_type = 'other_skill'"))));
            $data["other"] = $this->get("student_meta", array("where"=>array_merge($mywhere, array("meta_type = 'other'"))));

            $data["application"] = $application;
            $data["require_data"] = $this->getRequireFields();
            $this->cvform = $data;

            return $data;
        }

        public function sortMetaByDate($rows, $type){

            $retRows = array();

            foreach ($rows as  $row) {
                
                $contentObj = json_decode($row["content"], true);
                
                switch ($type) {
                    case "exam_result": $index = strtotime($contentObj["year"]."-01-01"); break; 
                    case "public_exam":                     
                    case "achievement": 
                    case "prof_qualification": 
                    case "oth_qualification": $index = strtotime($contentObj["date"]); break; 

                    case "education": 
                    case "work_experience": 
                    case "internship_experience":
                    case "exactivity": $index = strtotime($contentObj["fromdate"]); break; 
                }
                
                while (array_key_exists($index, $retRows)) {
                    $index++;
                }                
                
                $retRows[$index] = $row;
            }
            krsort($retRows);

            return array_values($retRows);
        }


        public function isCompleteApplication(){
            $cvform = $this->getCVForm();
            $validFlag = true;

            foreach ($cvform["require_data"] as $idx=>$obj) {
            	switch ($idx) {
                    case 'firstname_en': 
                    case 'lastname_en': 
                    case 'firstname_zh':
                    case 'lastname_zh': 
                    case 'email':
                    case 'contact':
                    case 'address1':
                    case 'age':
                    case 'hkid':
                    case 'dob':
                    case 'photo':
                    case 'em_name':
                    case 'em_name2':
                    case 'em_relation':
                    case 'em_phone':
                    case 'em_contact':                    	
                    	if ($cvform["application"][$idx] == "") return false; 
                    	break;
                    case 'modalEducation_list':
                    	if (count($cvform["education"]) == 0) return false; 
                    	break;
                    case 'modalExam_list':
                    	if (count($cvform["exam_result"]) == 0) {
                    		return false; 
                    	}
                    	break;                    	
                    /*case 'modalAchievement_list':
                    	if (count($cvform["achievement"]["achievement"]) == 0) {
                    		return false; 
                    	}
                    	break;                    	
                    case 'modalExActivity_list':
                    	if (count($cvform["achievement"]["exactivity"]) == 0) {
                    		return false; 
                    	}*/
                    case 'skills':
                    	if (count($cvform["achievement"]["skill"]) == 0) {
                    		return false;
                    	}
                        break;
                }
            }
            return $validFlag;
 
        }

        public function getCVList($offset, $limit){
            $mywhere = array();
            $mywhere["where"][] = "student_id = ?";
            $mywhere["where_parameters"][] = $this->id;

            $mywhere["where"][] = "status = ?";
            $mywhere["where_parameters"][] = "E";
            
            $mywhere["where"][] = "file01 <> ''";
            
            $mywhere["order"][] = "createDate desc";

            $data["total"] = count($this->get("personal_cv", $mywhere));

            $mywhere["offset"] = $offset;
            $mywhere["limit"] = $limit;

            $data["data"] = $this->get("personal_cv", $mywhere);

            return $data;
        }

        public function removeCVAttachment($cv_id, $cv_key){
            $this_cv = $this->getOne("personal_cv", array("where"=>array("student_id = '".$this->id."'", "sha1(concat(file01, '".$cv_key."')) = '".$cv_id."'")));
            if (!$this_cv) {
                return array("status"=>0, "msg"=>"CV not found");
            } else {
                $data["status"] = 'X';
                $this->conn->AutoExecute("personal_cv", $data, "UPDATE", "id = '".$this_cv["id"]."'");
                return array("status"=>1, "msg"=>"OK");
            }
        } 


        public function getRequireFields(){
            $data =  array(
                        "firstname_en"=>"req", "lastname_en"=>"req", "firstname_zh"=>"req", "lastname_zh"=>"req", "email"=>"req", "contact"=>"req",
                        "modalEducation_list"=>"req_row",
                     );
            
            if ($this->isWIE()){
                $data["address1"] = "req";
                $data["age"] = "req";
                $data["hkid"] = "req";
                $data["dob"] = "req";
                $data["availability"] = "req";
                $data["expected_salary"] = "req";
                $data["photo"] = "reqAttach";
                $data["gpa"] = "req";

                $data["em_name"] = "req";
                $data["em_name2"] = "req";
                $data["em_relation"] = "req";
                $data["em_phone"] = "req";
                $data["em_contact"] = "req";
                

                $data["modalExam_list"] = "req_row";
                $data["modalProfQuali_list"] = "req_row";
                $data["modalOthQuali_list"] = "req_row";
                //$data["modalAchievement_list"] = "req_row";
                //$data["modalExActivity_list"] = "req_row";
            }


            return $data; 
        }

        public function getExamType($id) {
            return $this->getOne("subject_node", array("where"=> array("node_id = '".(int)$id."' OR node_name = '".$id."'")) );
        }

        public function getExamForm($meta_id){

            $meta = $this->getOne("student_meta", array("where"=>array("id = '".(int)$meta_id."' and member_id = '".(int)$this->id."'")));

            if ($meta["meta_type"] == 'exam_result') {
                $meta = json_decode($meta["content"], true);
                $exam = $this->getOne("subject_node", array("where"=>array("node_name = '".$meta["exam"]."'")));
                $html = ' <li class="row"><div class="col-sm-8"><label>Subject</label></div><div class="col-sm-2 text-right"><label>Grade</label></div></li>';

                $grades = array(
                    "1"=>["5**","5*","5","4","3","2","1"],
                    "2"=>["A","B","C","D","E","F","U"],
                    "3"=>["A","B","C","D","E","U"]
                );

                foreach ($meta["exam_result"] as $key => $value) {
                    $subject = $this->getOne("subject", array("where"=>array("node_id = '".$exam["node_id"]."' AND name = '".$key."'")));


                    $options = array();
                    foreach ($grades[$subject["grade"]] as $opt) {
                        $options[] = sprintf('<option value="%s" %s>%s</option>', $opt, ($value == $opt) ? 'selected="selected"' : "", $opt);
                                            
                    }
                    $options = implode("", $options);
                    
                    $deletebtn = ($subject["mandatory"]) ? "" : '<a class="btn btn-delete-grade" title="Delete" data-subject="'.$key.'"><span class="fa fa-times-circle"></span></a>';

                    $html .= sprintf('<li class="row"><div class="col-sm-8">%s</div><div class="col-sm-2 text-right">'.
                                        '<select  data-grade="%s"><option value="">- Select -</option>%s</select>'.
                                        '</div><div class="addon">%s</div></li>', $key, $key, $options, $deletebtn);    
                }
                
                return array("status"=>1, "exam_id"=>$exam["node_id"], "exam"=>$exam["node_name"], "html"=>$html);
            } else {

                $meta = json_decode($meta["content"], true);
                $meta["grade"] = json_decode($meta["grade"], true);

                $html = '<li class="list-group-item hide">
                <div class="row"> <div class="col-md-5"><span data-span="subject"></span></div>'.
                                 '<div class="col-md-5 text-right"><span data-span="grade"></span></div></div>
                <div class="addon"><a class="btn btn-delete-grade" data-subject="subject" title="Delete"><span class="fa fa-times-circle"></span></a></div>
              </li>';

                foreach ($meta["grade"] as $key => $value) {

                    $html .= sprintf('<li class="list-group-item">'.
                                        '<div class="row">'.
                                            '<div class="col-md-5"><span data-span="subject">'.$key.'</span></div>'.
                                            '<div data-span="grade" class="col-md-5 text-right">'.$value.'</div>'.
                                        '</div>'.
                                        '<div class="addon"><a class="btn btn-delete-grade" title="Delete" data-subject="'.$key.'"><span class="fa fa-times-circle"></span></a></div>'.
                                      '</li>');    

                }



                return array("status"=>1, "html"=>$html);
            }

            
        }
        
        public function getApplicationHistory(){
            $mywhere = array();
            $mywhere["where"][] = "member_id = '".$this->id."'";
            $mywhere["where"][] = "app_no <> ''";
            $mywhere["order"][] = "submission_date desc";

            $joblist = $this->get("application", $mywhere);

            $jobs = array();
            foreach ($joblist as $job){
                $tmp = array();
                $tmp["application"] = $job;

                $appl = new Application($job["id"]);
                $tmp["items"] = $appl->getItems();
                $jobs[] = $tmp;
            }
            return $jobs;
        }

        public function saveMeta($application_id, $type, $content, $template){
            $data = array();
            $data["application_id"] = $application_id;
            $data["member_id"] = $this->id;
            $data["meta_type"] = $type;
            $data["content"] = json_encode($content, JSON_UNESCAPED_UNICODE);

            foreach ($content as $key=>$con) {
                if ($type == "public_exam" && $key == "grade") {
                    $grade = json_decode($content["grade"], true);
                    $tmp = array();
                    foreach ($grade as $idx=>$obj) {
                        $tmp[] = "$idx($obj)";
                    }
                    $template = str_replace("[".$key."]", implode(", ", $tmp), $template);
                } else {
                    $template = str_replace("[".$key."]", $con, $template);
                }
            }

            $this->conn->AutoExecute("student_meta", $data, "INSERT");

            $template = str_replace("data-content=\"[content]\"", "data-content='".str_replace("'", "&#39;", $data["content"]) ."'", $template);
            $template = str_replace("[id]", $this->conn->insert_Id(), $template);

            return $template;
            
        }

        public static function getFilePath($student_id) {
            
            global $uploadedFilePath, $uploadedFileDir, $appPath;

            $abs_path = $uploadedFilePath . "/" . md5("stud".$student_id."ent");

            if (!file_exists($abs_path)) {
                mkdir($abs_path, 0777, true);
            }

            return array(
                "absolute_path"=>$uploadedFilePath . "/" . md5("stud".$student_id."ent"),
                "path"=> $appPath.$uploadedFileDir."/". md5("stud".$student_id."ent"),
                "dbpath"=>md5("stud".$student_id."ent")
            );
        }

        public function getFileById($type, $id, $str=""){
            $fileObj = new NqFileObj($this->id);
            if ($str == "") {
                return $fileObj->download($id);
            } else {
                return $fileObj->generatefile($id);
            }
        }

        public function importWIE($fileobject) {
            global $pathClassLib;
            require_once(__DIR__."/../lib_common/phpexcel/PHPExcel.php");
            $i = 0;
            try {
                $tmp_name = $fileobject["tmp_name"];

                $objPHPExcel = PHPExcel_IOFactory::load($fileobject["tmp_name"]);
                $sheet = $objPHPExcel->getSheet(0); 

                $highestRow = $sheet->getHighestRow(); 
                $highestColumn = $sheet->getHighestColumn();

                for ($row = 1; $row <= $highestRow; $row++) {
                    //$rows = $sheet->rangeToArray('A' . $row . ':' . '' . $row, NULL, TRUE, FALSE);
                    $student_id = ($sheet->getCellByColumnAndRow(0, $row)->getValue());

                    $this->conn->execute("Update student set is_wie = 1 where student_id = '".$student_id."'");
                    $i++;
                }

                return $i;
            } catch (Exception $err) {
                var_dump($err);
                return false;
            }
        }

        public function updateMeta($application_id, $type, $content, $template, $id=""){
            $data = array();
            $data["content"] = json_encode($content, JSON_UNESCAPED_UNICODE);

            $mywhere = array();
            $mywhere["where"][] = "member_id = '". $this->id."'";
            $mywhere["where"][] = "application_id = '".$application_id."'";
            $mywhere["where"][] = "meta_type = '".$type."'";

            if ($id != "") {
                $mywhere["where"][] = "id = '".$id."'";
            }

            $dataOne = $this->getOne("student_meta", $mywhere);

            if ($dataOne["id"]) {
                foreach ($content as $key=>$con) {
                    if ($type == "public_exam" && $key == "grade") {
                        $grade = json_decode($content["grade"], true);
                        $tmp = array();
                        foreach ($grade as $idx=>$obj) {
                            $tmp[] = "$idx($obj)";
                        }
                        $template = str_replace("[".$key."]", implode(", ", $tmp), $template);


                    } else {
                        $template = str_replace("[".$key."]", $con, $template);
                    }
                }
                $template = str_replace("[id]", $id, $template);
                $template = str_replace("data-content=\"[content]\"", "data-content='".str_replace("'", "&#39;", $data["content"])."'", $template);

                $this->conn->AutoExecute("student_meta", $data, "UPDATE", implode(" AND ", $mywhere["where"]));
            } else {
                foreach ($content as $key=>$con) {
                    $template = str_replace("[".$key."]", $con, $template);
                }
                $data["application_id"] = $application_id;
                $data["member_id"] = $this->id;
                $data["meta_type"] = $type;
                $this->conn->AutoExecute("student_meta", $data, "INSERT");
            }
            return $template;
        }
        public function removeMeta($application_id, $id){

            $mywhere = array();
            $mywhere["where"][] = "member_id = '". $this->id."'";
            $mywhere["where"][] = "application_id = '".$application_id."'";
            $mywhere["where"][] = "id = '".$id."'";


            $data = $this->getOne("student_meta", $mywhere);
            if ($data) {
                $this->conn->Execute("DELETE from student_meta where id='".$id."'");
            }

            return array("msg"=>"ok");
        }


        public function saveCVForm($application_id, $updatedata) {

            foreach ($updatedata["other"]["content"]["id"] as $idx=>$other) {
                $data = array();
                $data["title"] = $updatedata["other"]["title"]["id"][$idx];
                $data["html"] = htmlentities($updatedata["other"]["content"]["id"][$idx], ENT_QUOTES, "UTF-8");

                $this->updateMeta($application_id, "other", $data, "", $idx);
            }
            foreach ($updatedata["other"]["content"]["new"] as $idx=>$other) {
                if (trim(strip_tags($other)) != "") {
                    $data = array();
                    $data["title"] = $updatedata["other"]["title"]["new"][$idx];
                    $data["html"] = htmlentities($updatedata["other"]["content"]["new"][$idx], ENT_QUOTES, "UTF-8");

                    $this->saveMeta($application_id, "other", $data) ;
                }
            }
  
            $application = new Application($application_id);
            $data = $application->getDetail();
            if ($data["member_id"] == $this->id) {
                $application->saveCVForm($updatedata);
                return array("status"=>'1', "msg"=>"Ok");
            } else {
                return array("status"=>'0', "msg"=>"Access Denied");
            }
        }

        public function uploadFile($application_id, $field) {
            $filesArray = array();
            $return = array();
            
            $nqFileObj = new NqFileObj($this->id);

            $file_id = $nqFileObj->upload($_FILES[$field]);



            $this->conn->AutoExecute("application", [$field=> "pfiles/attach-". $file_id], "UPDATE", "id = '".$application_id."'"); 


            $return["type"] = $field;
            $return["filename"] = "attach-" . $file_id;
            $return["url"] = "pfiles/attach-" . $file_id;

            /*

            foreach ($filesArray as $idx=>$value) {
                $return["type"] = $idx;
                $return["filename"] = $value;
                $return["url"] = $path["path"] ."/". $value;

                $this->conn->AutoExecute("application", [$idx=> $path["dbpath"] ."/". $value], "UPDATE", "id = '".$application_id."'");
            }*/
            return $return;
        }

        public function updateFile($application_id, $field) {
            $this->conn->AutoExecute("application", [$field=> ""], "UPDATE", "id = '".$application_id."'");
            return array("status"=>1, "msg"=>"ok");
        }


        public function createCVContent($application_id, $meta_ids) {

            foreach ($meta_ids as $idx=>&$vv) {
                foreach ($vv as $ii=>$v) {
                    if ($v == "") {
                        unset($vv[$ii]);
                    }
                }
            }

            $data = array();
            $data["student_id"] = $this->id;
            $data["application_id"] = $application_id;

            $data["basic"]    = ($meta_ids["basic"])   ? implode(",", $meta_ids["basic"])    : "";
            $data["meta"]     = ($meta_ids["meta"])     ? implode(",", $meta_ids["meta"])     : "";
            $data["language"] = ($meta_ids["language"]) ? implode(",", $meta_ids["language"]) : "";
            $data["computer"] = ($meta_ids["computer"]) ? implode(",", $meta_ids["computer"]) : "";
            
            $data["status"] = "E";
            $data["createDate"] = date("Y-m-d H:i:s");
            
            
            $this->conn->AutoExecute("personal_cv", $data, "INSERT");

          
            $id = $this->conn->insert_Id();
            
            $this->conn->AutoExecute("application", ["cv_attached"=>$id], "UPDATE", "id = '$application_id'");

            return $id;
        }

        public function getName(){
            return $this->name;
        }

        public function getJobList($offset, $limit){
            require_once(__DIR__. "/jobs.php");
            $job = new Jobs();
            $me = $this->getDetail();

            $mywhere = array();
            $mywhere["fields"] = array("job_ids"=>"GROUP_CONCAT(job_id)");
            $mywhere["where"][] = "program_id = '".$me["prog_code"]."'";
            $mywhere["where"][] = "years like ('%".$me["yrofstudy"].";%')";
            
            $jobs_id = $this->getOne("jobs_permission", $mywhere);

            $mywhere = array();
            $mywhere["offset"] = $offset;
            $mywhere["limit"] = $limit;
            $mywhere["where"][] = "id in (".$jobs_id["job_ids"].")";
            $mywhere["where"][] = "open_date <= NOW() AND close_date > NOW()";
            $mywhere["where"][] = "status = 'E'";

            return $job->getList($mywhere);
        }

        public function check_saml_login() {
            global $samlAuthority, $conn;

            $token = sha1("student_saml:sp:NameID_token");

            session_start();
            if (array_key_exists($token, $_SESSION)) {
                $tmp = unserialize(base64_decode($_SESSION[$token]));
                if ($tmp) {
                    $authData = $tmp;
                }
            } else {
                $auth = new \SimpleSAML\Auth\Simple("default-sp");
                $authData = $auth->getAuthData("saml:sp:NameID");
            }
            session_write_close();
             
            if ($authData) {
                session_start();
                $_SESSION[$token] = base64_encode(serialize($authData));
                session_write_close();

                if (strpos($authData->value, "@cpce-polyu.edu.hk") === FALSE) {
                    list($username, $domain) = explode("@", $authData->value);
                    
                    $mywhere = array();
                    $mywhere[] = "status = ?";
                    $mywhere[] = "student_id = ?";
                    
                    $where_para = array('E', $username);

                    $data = $this->get($this->tableName, array("offset"=>0, "limit"=>1, "where"=>$mywhere, "where_parameters"=>$where_para));

                    if ($data) {
                        
                        $this->logined = true;  
                        $this->id = $data[0]["id"];
                        $this->name = $data[0]["en_name"];
                        $this->wie = ($data[0]["is_wie"] == 1);
                        $this->save();
                        
                        return true;
                    }
                } else {
                    header("location: wie/");
                    exit();
                }
            }
        

            return false;
        }

        public function start_login() {
            global $domain, $conn;
            $auth = new \SimpleSAML\Auth\Simple('default-sp');

            $auth->login([
                "ReturnTo"=>"https://".$domain."/login_landing",
                "KeepPost"=>TRUE,
            ]);
        }

        public function isLogined(){
            if (!$this->check_saml_login()){
                return false;
            }
            return $this->logined;
        }

        public function isWIE(){
            
            return $this->wie;
        }
    }
?>