<?php
    require_once($pathClassLib.'/dbtree.class.php');
    require_once($pathClassLib."/class.uploadfile.php");
    require_once($pathClassLib."/function.common.php");
    require_once($pathClassLib."/class.file.php");

    class CMSObj {
        # array( Name, DB Field Name, type, array(list status, width, sortable, searchable), array(edit form status, update, insert, default selection/value))

        public $label = "";
        public $fieldName = "";
        public $type = "";
        public $list = array();
        public $edit = array();
        public $options = array();
        public $lang = array();

        public $fileUpload = false;
        protected $value = false;

        private $defaultOps = array(
            "list" => array("show", "sort", "search"),
            "edit" => array("show", "update", "insert"),
            "width" => 250,
            "options" => array(),
            "lang" => array(),
            "prefix" => ""
        );

        function __construct($type, $fieldName, $ops=array()){
            global $dbDatabase;

            $opps = $this->defaultOps;

            foreach ($ops as $key => $value) {
                if (array_key_exists($key, $this->defaultOps)) {
                    $opps[$key] = $value;
                }
            }

            $this->type = $type;
            $this->label = $this->fieldName = $fieldName;

            if (array_key_exists("label", $ops)) {
                $this->label = $ops["label"];
            }
            $this->list["show"] = (bool)in_array("show", $opps["list"]);
            $this->list["sort"] = (bool)in_array("sort", $opps["list"]);
            $this->list["search"] = (bool)in_array("search", $opps["list"]);
            $this->list["width"] = (int)$opps["width"];

            $this->edit["show"] = (bool)in_array("show", $opps["edit"]);
         

            $this->edit["update"] = (bool)in_array("update", $opps["edit"]);
            $this->edit["insert"] = (bool)in_array("insert", $opps["edit"]);
            $this->edit["readonly"] = (bool)in_array("readonly", $opps["edit"]);

            $this->edit["prefix"] = $opps["prefix"];


            $this->options = $opps["options"];   
            
            
            $this->lang = $opps["lang"];

            $this->field =  $this->fieldName . (!(empty($this->lang)) ? '_'.$this->lang[0] : "");
            if (array_key_exists("field_name", $this->options)) {
                $this->field = $this->options["field_name"];
            }
            

        }



        public function render() {
            global $rs, $defaultVersionPath;

            $ret = array_key_exists($this->field, $rs->fields) ? (str_replace(array("\n", "\r"), "", $rs->fields[$this->field])) : "";

            switch ($this->type) {
                case 'INT':       return number_format($ret, 0);
                case 'DOUBLE':    return number_format($ret, 2);
                case 'HYPERLINK': return ($ret != "") ? '<a href="' . $ret . '" target="_blank">Link</a>' : '';
                case 'COLOR':     return '<div style="background:' . $ret . ';width:10px; height:10px; border:1px solid #000; padding:1px; margin:3px auto;"></div>';
                default:          return $ret;
            }
        }

        public function renderControls($value=false) {
            global ${$this->field};
            global $cms, $adminPath, $id;

            $thisValue = ($value !== false) ? $value : ${$this->field};

            switch ($this->type) {
                
                case "DATE":
                case "DATE_PICKER":
                case "DATETIME":
                case "DATETIME_PICKER":
                    $json = array();
                    $json["showsTime"] = in_array($this->type, ["DATETIME", "DATETIME_PICKER"]);
                    $json["inputField"] = $this->field;
                    $json["button"] = "btn".$this->field;
                    $json["ifFormat"] = ($json["showsTime"]) ? "%Y-%m-%d %H:%M:00" : "%Y-%m-%d";
                    
                    if ($thisValue == "") {
                        $thisValue = ($json["showsTime"]) ? date("Y-m-d H:i:00") : date("Y-m-d");
                    } 
                   

                    $cms->admin_js["calendar"][] = 'Calendar.setup('.json_encode($json).');';

                    return '<input id="'.$this->field.'" type="text"  class="date" value="'.$thisValue.'" name="'.$this->field.'" style="width: 115px;"/> 
                            <i class="fa fa-calendar fa-lg" id="btn'.$this->field.'" title="Date Selector" src="'.$adminPath.'/images/calendar.gif" style="cursor:pointer;vertical-align:text-bottom;" />';
                    
                case "TEXT":
                    $cms->admin_js["ckeditor"][] = $this->field;
                    return vsprintf('<textarea id="%s" name="%s" style="width:350px; height:200px">%s</textarea>', [$this->field, $this->field, $thisValue]);

                case "TEXTAREA":
                    $thisValue = str_replace(array("\'", '\"'), array("'", '"'), $thisValue);
                    return vsprintf('<textarea id="%s" name="%s" style="width:350px; height:200px">%s</textarea>', [$this->field, $this->field, $thisValue]);
    
                default: 
                    $v = str_replace('"', "''", html_entity_decode($thisValue));
                    $input =  '%s <input type="%s" id="%s" name="%s" value="%s"" onkeydown="if(event.keyCode==13)event.keyCode=9" %s/>';

                    if ($this->type == 'PASSWORD'){
                        return vsprintf($input, array($this->edit["prefix"], 'password', $this->field, $this->field, '', ""));
                    } else {
                        return vsprintf($input, array($this->edit["prefix"], 'text', $this->field, $this->field, $v, (($this->edit["readonly"] && $id ) ? "readonly" : "")));
                    }  

            }
        }

        public function save($update){
            global $fields;

            $this->getpost();

            if ($update) {
                if ($this->edit["update"]) {
                    $this->update($fields);
                }
            } else {
                $fields["createDate"] = date("Y-m-d H:i:s");
                if ($this->edit["insert"]) {
                    $this->insert($fields);
                }
            }
        }


        protected function getpost(){ 
          
            if (array_key_exists($this->field, $_POST) ) {
                
                if (in_array("multiple", $this->options)){
                    $this->value = array();
                    foreach ($_POST[$this->field] as $it) {
                        $it = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $it);   
                        $this->value[] = htmlspecialchars($it, ENT_QUOTES,'UTF-8');
                    }
                } else {
                    $this->value = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $_POST[$this->field]);   
                    $this->value = htmlspecialchars($this->value, ENT_QUOTES,'UTF-8');
                }

                
            }
            
        }

        protected function insert(&$fields) {
            $this->update($fields);

            if (!empty($this->lang)) {
                foreach ($this->lang as $ff) {
                    if ($ff != '') {
                        $fields[$this->fieldName.'_'.$ff] = $this->value;
                    }
                }
            }
        }   

        protected function update(&$fields) {
            switch ($this->type) {
                case "PASSWORD_MD5":
                    if(!empty($this->value)){
                        $salt = substr(time(), -3, 3);
                        $fields[$this->field] = sha1(sha1($this->value).$salt);
                        $fields[$this->field."_salt"] = $salt;
                    } 
                    break;
                case "PASSWORD":
                    if(!empty($this->value)){
                        $fields[$this->field] = sha1($this->value);
                    }
                    break;
                default:
                    $fields[$this->field] = $this->value;
                    break;
            }
        }
    }

    class CategoryObj extends CMSObj {

        public $node_table = "";
        public $dbtree = false;

        function __construct($fieldName, $ops=array()) {
            global $conn;
            parent::__construct("CATEGORY", $fieldName, $ops);
            $this->node_table = $ops["tableName"]."_node";
            $this->dbtree = new dbtree($ops["tableName"]."_node", 'node', $conn);
        }

        public function render() {
            global $rs;
            $ret = array();

            if (in_array("multiple", $this->options)) {
                $tmpArr = explode(";", $rs->fields[$this->fieldName]);
                if ($tmpArr) {
                    foreach($tmpArr as $eachId) {
                        if($eachId != "") {
                            $data1 = $this->dbtree->GetNodeInfo($eachId, false, "node_name");
                            $ret[] = html_entity_decode($data1[3]);
                        }
                    }
                }
                $ret = implode(", ", $ret) ;

            } else {
                if ($rs->fields["node_level"] > 1) {
                    $thisNodeId = $rs->fields["node_id"];
                    do {
                        list($thisNodeLevel,$thisNodeName)  = array_slice($this->dbtree->GetNodeInfo($thisNodeId, false, "node_name"), 2, 2);
                        $thisNodeId = reset($this->dbtree->GetParentInfo($rs->fields["node_id"]));
                        array_unshift($ret, html_entity_decode($thisNodeName));
                    } while ($thisNodeLevel > $rs->fields["node_level"]-1);

                    $ret = implode(" -> ", $ret);
                } else {
                    $ret = html_entity_decode($rs->fields["node_name"]);     
                }
            }
            return str_replace(array("\n", "\r"), "", $ret);
            
        }

        public function renderControls($value=false) {
            global ${$this->field};

            $thisValue = ($value === false) ? ${$this->field} : $value;

            $v = explode(";", $thisValue);
            $this->dbtree->Full(array('node_id', 'node_level', 'node_name'));
            $printForm = array('<option value=""></option>');
            $end = '';

            while ($item = $this->dbtree->NextRow()) {

                if ($item['node_level'] > 0) {
                    $selected = in_array($item['node_id'], $v) ? "selected" : "";
                    
                    $li = html_entity_decode($item['node_name']);

                    if ($this->hasChild($item['node_id']) && !PARENT_NODE_SELECT) {
                        $printForm[] = $end.'<optgroup LABEL="'.html_entity_decode($item['node_name'])."  \"> ".$li.'\n';
                        $end = '</optgroup>';
                    } else
                        $printForm[] = '<option value="'.$item['node_id'].'" style="text-indent:'.$item["node_level"].'em" '.$selected."> ".$li."</option>\n";
                }

            }
            
            $class = (array_key_exists("select2", $this->options) && $this->options["select2"] === false) ? "" : "select2";
         
            return  '<select id="'.$this->field.'" class="'.$class.'" ' . (in_array("multiple", $this->options) ? 'name="'.$this->field.'[]" multiple'  : 'name="'.$this->field.'"') . '>'.
                        implode('', $printForm).
                    '</select>';  
        }

        private function hasChild($node_id) {
            global $conn;
            $thissql = "SELECT n2.* FROM `". $this->node_table ."` n1, `". $this->node_table ."` n2 
                        WHERE n1.`node_id`='".$node_id."' AND n1.`node_left`<n2.`node_left` AND n1.`node_right`>n2.`node_right`";
            $rs = $conn->Execute($thissql);
            return ($rs->RecordCount() > 0);
        }

        protected function getpost(){
            $this->value = $_POST[$this->field];
        }

        protected function insert(&$fields){
            $this->update($fields);
        }    

        protected function update(&$fields){
            if (in_array("multiple", $this->options)) {
                if (!empty($this->value)){
                    $fields[$this->field] = implode(";", $this->value);
                } else {
                    $fields[$this->field] = '1;';
                }
            } else {
                $fields[$this->field] = $this->value;
            }
        }
    }


    class ForeignObj extends CMSObj {

        public $node_table = "";
        public $dbtree = false;
        private $conn = false;

        function __construct($fieldName, $ops=array()) {
            global $conn;
            $this->conn = $conn;
            parent::__construct("FOREIGN", $fieldName, $ops);
        }

        public function render() {
            global $rs;

            $rss = trim(str_replace(";", ",", $rs->fields[$this->fieldName]), ",");

            // recursively get the end of the chain

            foreach ($this->options["ft"] as $ft) {
                $sql = preg_replace("/\{([^\{]{1,5}?)\}/e", '$ft["$1"]', "SELECT {label} FROM {table} WHERE {value} in ('$rss')");
                $rss = implode("','", $this->conn->GetCol($sql));
            }

            return str_replace("'", "", $rss);
        }

        public function renderControls(){

            global ${$this->field};
            
            $lis = array('<option value=""></option>');
            $value = explode(";" , ${$this->field});

            foreach ($this->options["ft"] as $ft) {
                $sql = preg_replace("/\{([^\{]{1,5}?)\}/e", '$ft["$1"]', "SELECT {value}, {label} FROM {table} order by {label} ");
                if (array_key_exists("where", $ft)) {
                    $sql .= " WHERE ". implode(" AND ", $ft["where"]);
                }

                $rss = implode("','", $this->conn->GetAssoc($sql)); 
            }
            
            $rss = $this->conn->GetAssoc($sql);
            
            if ($rss) {
                foreach ($rss as $k=>$rs) {
                    $lis[] = vsprintf('<option value="%s" %s>%s</option>', [$k, (in_array($k, $value)) ? 'selected="selected"' : "" ,$rs]);
                }
            }

            $select = '<select class="select2" id="%s" name="%s" %s>%s</select>';

            $args = array();
            $args["id"] = $this->field;
            $args["name"] = in_array("multiple", $this->options) ? $this->field. "[]" : $this->field;
            $args["multiple"] = in_array("multiple", $this->options) ? "multiple" : "";
            $args["list"] = implode("", $lis);

            return vsprintf($select, $args); 
        }

        protected function insert(&$fields){
            $this->update($fields);
        } 

        protected function update(&$fields){
            
            if (in_array("multiple", $this->options)) {
                if (!empty($this->value)){
                    $fields[$this->field] = implode(";", $this->value);
                } else {
                    $fields[$this->field] = '';
                }
            } else {
                $fields[$this->field] = $this->value;
            }
        }
    }

    class SelectObj extends CMSObj {

        function __construct($type, $fieldName, $ops=array()) {
            parent::__construct($type, $fieldName, $ops);
        }

        public function render() {
            global $rs;
            $ret = array();

            $strStatus = array();
            foreach( $this->options as $eachName=>$eachValue) {
                $strStatus[trim($eachValue)] = trim($eachName); 
            }

            $tmpArr = explode(";", $rs->fields[$this->fieldName]);

            foreach($tmpArr as $eachId) {
                if ($eachId != "") {
                    $ret[] = ($strStatus[$eachId] != "") ? $strStatus[$eachId] : $eachId;
                }
            }
            return implode(",", $ret);
        }

        public function renderControls() {
            global ${$this->field};

            $value = explode(";",${$this->field});
            $strStatus = array();
            $lis = array();

            $multiple = ($this->type == "CHECKBOX") ? true : false;
            
            if ($this->type == "RADIO") {
                $selected = (${$this->field} == "") ? 'checked' : '';
            }
            
            foreach( $this->options as $eachName=>$eachValue) {
                $i++;
                if ($this->type == "SELECT")  { 
                    $selected = in_array($eachValue, $value) ? 'selected="selected"' : $selected;
                    $lis[] = vsprintf('<option value="%s" %s>%s</option>', [$eachValue, $selected, $eachName]);
                
                } else {
                    $id = ($this->field."_" .$eachValue);
                    $label = '<label for="%s"><input id="%s" name="%s" type="%s" value="%s" %s /> %s</label>&nbsp;';
                    $attr = array();
                    $attr["for"] = $id;
                    $attr["id"] = $id;
                    $attr["name"] = ($multiple) ? $this->field."[]" : $this->field;
                    $attr["type"] = ($multiple) ? "checkbox" : "radio";
                    $attr["value"] = $eachValue;
                    $attr["checked"] = in_array($eachValue, $value) ? "checked" : $selected;
                    if (${$this->field} == "" && $i == 1) {
                        $attr["checked"] = "checked";
                    }
                    $attr["label"] = $eachName;
                    $lis[] = vsprintf($label,$attr);
                }

                $selected = "";
            }


            switch ($this->type) {
                case "SELECT" : 
                    $select = '<select class="select2" id="%s" name="%s%s" %s>%s</select>';
                    $attr = array($this->field, $this->field);
                    $attr[] = in_array("multiple", $this->options) ? "[]" : "";
                    $attr[] = in_array("multiple", $this->options) ? "multiple" : "";
                    $attr[] = implode("", $lis);

                    return vsprintf($select, $attr);

                default :
                    return implode("", $lis);
            }
        
        }
        protected function getpost() { 
            $multiple = ($this->type == "CHECKBOX") ? true : false;          
            if (array_key_exists($this->field, $_POST) ) {    
                if ($multiple){
                    $this->value = array();
                    foreach ($_POST[$this->field] as $it) {
                        $it = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $it);   
                        $this->value[] = htmlspecialchars($it, ENT_QUOTES,'UTF-8');
                    }

                } else {
                    $this->value = str_replace(array("\\'", "\\\"", '\\\\'), array("'", "\"", '\\'), $_POST[$this->field]);   
                    $this->value = htmlspecialchars($this->value, ENT_QUOTES,'UTF-8');
                }
            }
        }

        protected function insert(&$fields) {
            $this->update($fields);
        }   

        protected function update(&$fields) {
            $multiple = ($this->type == "CHECKBOX") ? true : false;
            
            if ($multiple){
                $fields[$this->field] = implode(";", $this->value);
            } else {
                $fields[$this->field] = $this->value;
            }

        }
    }

    class FileObj extends CMSObj {

        private $path;

        function __construct($type, $fieldName, $ops=array()) {
            global $uploadedFilePath;
            global $pathDocRoot;

            parent::__construct($type, $fieldName, $ops);
            
            $this->path = $uploadedFilePath;
            $this->fileUpload = new file_upload;
            $this->fileUpload->upload_dir = $uploadedFilePath . "/"; 
            
            
            if (array_key_exists("upload_dir",$ops)) {
                $this->path = $ops["upload_dir"];
                $this->fileUpload->upload_dir = $ops["upload_dir"]."/";
            }
            
            $this->fileUpload->max_length_filename = 50;
            $this->fileUpload->rename_file = true;
        }

        public function render() {  
            global $rs, $adminPath;

            $control = '<a href="%s" target="_blank"><img src="%s" width="%s" height="%s" alt="%s" border="0" /></a>';
            $newsize = 0;
            $filename = $rs->fields[$this->fieldName];

            if ($filename != "")  {   
                $path = $url = UFILE_PATH.$filename;

                if($this->type == "THUMBNAIL") {
                    $newsize = imgVirtualResize(UFILE_PATH, $filename, 30, 30);    
                } else {
                    $newsize = [18,15];
                    $path = $adminPath."/images/icondownload.gif";
                }
            }
            return ($newsize != 0) ? vsprintf($control, [$url, $path, $newsize[0], $newsize[1], $this->fieldName]) : "";
        }

        public function renderControls() {            
            global $rootPath, ${$this->field};
            $filename = ${$this->field};
            $printimg = "";


            $rows = array();
            $rows[] = '<br /><input type="file" id="'.$this->field.'" name="'.$this->field.'" />';
            

            if ($filename != "") {
                $printimg = UFILE_PATH.$filename;
                
                if (in_array($this->type, ["PHOTO", "THUMBNAIL"])){
                    $newsize = imgVirtualResize(UFILE_PATH, $filename, 80, 60, $rootPath); 
                    $control = '<a href="%s" target="_blank"><img src="%s" border="0" width="%s" height="%s" align="left" alt="%s" hspace="5" /></a>';
                    $args = array(UFILE_PATH.urlencode($filename), $printimg, $newsize[0], $newsize[1], $filename);

                    if ($newsize != 0) {
                        $rows[] =   vsprintf($control, $args);
                    }
                } else {
                    array_unshift($rows, vsprintf('<a href="%s" target="_blank">%s</a>', [UFILE_PATH.urlencode($filename), $printimg]));
                }
            }

            $rows[] = "<input type=\"hidden\" name=\"filename_".$this->field."\" value=\"".$filename."\" />";
            
            if ($filename != "") {
                $rows[] = "<label><input type=\"checkbox\" name=\"fileDel_".$this->field."\" value=\"".$filename."\" /> Delete</label>";
            }

            return  implode("", $rows);
        
        }

        protected function getpost(){
            $this->value = (array_key_exists($this->field, $_FILES)) ? $_FILES[$this->field] : "";
            $this->value["delete"] = (array_key_exists("fileDel_".$this->field, $_POST)) ? $_POST["fileDel_".$this->field] : "";
            $this->value["filename"] = (array_key_exists("filename_".$this->field, $_POST)) ? $_POST["filename_".$this->field] : "";
        }

        protected function insert(&$fields){

            global $rootPath, $appPath;


            if ($this->value["name"] != "") {
            
                $this->fileUpload->the_temp_file = $this->value["tmp_name"];
                $this->fileUpload->the_file = $this->value["name"];
                $this->fileUpload->http_error = $this->value["error"];
                $this->fileUpload->file_copy = "";

                $this->fileUpload->replace = "n";
                $this->fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
                $this->fileUpload->upload();
                
                $this->value = $this->fileUpload->file_copy;


                if (in_array($this->type, ["PHOTO", "THUMBNAIL"])) {

                    if (!empty($this->options["size"])) {
                        imgActualResize($this->path, $fields[$this->path], 
                                        $this->path, $fields[$this->path],
                                        $this->options["size"][0], $this->options["size"][1]);
                    }


                    if (!empty($this->options["watermark"])) {
                        $imgPath = $this->path."/".$this->value;
                        $watermarkPath = $rootPath.$appPath.trim($this->options["watermark"]["img"]);
                        
                        if (array_key_exists("repeat", $this->options["watermark"])) {
                            $this->addWaterMark($imgPath, $watermarkPath, 25, $this->options["watermark"]["repeat"]);
                        } else {
                            $this->addWaterMark($imgPath, $watermarkPath, 25);
                        }
                    }
                    
                    if (!empty($this->options["thumbnail"]) and $this->value != "") {
                        foreach ($this->options["thumbnail"] as $idx=>$sizeImg)
                        {
                            // $sizeImg = [field name, width, height];
                            $extension = end(explode(".", $this->value));
                            $filename = trim($this->value, ".".$extension);
                            $thumbnail = $filename."_t".$idx.".".$extension;

                            $file = new File();
                            $file->copy_file($this->path."/".$this->value, $this->path."/".$thumbnail);
                            imgActualResize($this->path, $thumbnail, $this->path, $thumbnail,  $sizeImg[1], $sizeImg[2], 100);
                            $fields[$sizeImg[0]] = $thumbnail;
                        }
                    }
                }
                $fields[$this->field] = $this->value;
            }
        }

        public function setUpload(&$fields) {
            $this->getpost();

            $this->insert($fields);
        }

        protected function update(&$fields) {
            global ${$this->field};
            $filename = ${$this->field};

            if ($this->value["delete"] != ""){
                @unlink($this->path."/".$filename);
                $fields[$this->field] = "";   
            } else {
               $this->insert($fields);
            }
        }
        

        private function addWaterMark($imgname, $watermarkImg, $watermarkImgOpacity=30, $WM_POS="REPEAT") {
            // Get the width and height of each image.
            /*
            $imgWidth = imagesx($Main_Image);
            $imgHeight = imagesy($Main_Image);
            $watermarkWidth = imagesx($watermark);
            $watermarkHeight = imagesy($watermark);
            */
            $mainImage = loadImageFile($imgname);
            $watermark = loadImageFile($watermarkImg);
            
            list($imgWidth, $imgHeight, $imgTypeCode) = @getimagesize($imgname);
            list($watermarkWidth, $watermarkHeight, $imgTypeCode) = @getimagesize($watermarkImg);

            $x = $y = 0;
            if ($WM_POS == "REPEAT") {
                while($y <= $imgHeight) {
                    while($x <= $imgWidth) {
                        @imagecopymerge_alpha($mainImage, $watermark, $x, $y, 0, 0, $watermarkWidth, $watermarkHeight, $watermarkImgOpacity);
                        $x += $watermarkWidth;
                    }   
                    $x = 0; 
                    $y += $watermarkHeight;
                }
            } else {
                // calculate the position of the WaterMark
                if ($WM_POS == "CENTER") {
                    $x = ($imgWidth - $watermarkWidth) / 2;
                    $y = ($imgHeight - $watermarkHeight) / 2;
                } else {
                    $x = in_array($WM_POS, ["TOPLEFT", "BOTTOMLEFT"]) ? 0 : ($imgWidth - $watermarkWidth);
                    $y = in_array($WM_POS, ["TOPLEFT", "TOPRIGHT"]) ? 0 : ($imgHeight - $watermarkHeight);
                }
                @imagecopymerge_alpha($mainImage, $watermark, $x, $y, 0, 0, $watermarkWidth, $watermarkHeight, $watermarkImgOpacity);
            }
            
            // show the image in png format(sharper image)  
            switch (strtolower(end(explode(".",  $imgname)))) {
                case "jpg": case "jpeg":
                    imagejpeg($mainImage, $imgname);
                    break;
                case "gif":
                    imagegif($mainImage, $imgname);
                    break;
                case "png":
                    imagesavealpha($mainImage, true);
                    imagepng($mainImage, $imgname);
                    break;  
            }
        }
    }


    class CustomObj extends CMSObj {
        function __construct($type, $fieldName, $ops=array()) {
            parent::__construct($type, $fieldName, $ops);
        }

        function renderControls(){
            global $cms, ${$this->field};
            $thisValue = ${$this->field};
            $printimg = "";
            
            switch ($this->type) {

                case "COLOR":    
                    $isChecked = true;
                    if ($thisValue == "" || is_null($thisValue)){
                        $isChecked = false;
                    }

                    $args = array_fill(0, 7, $this->field);
                    $args[0] = "";
                    $args[2] = "disabled";
                    $args[5] = str_replace('"', "''", html_entity_decode($thisValue));
                    $args[6] = "visibility:hidden";

                    if ($isChecked) {
                        $args[0] = "checked";
                        $args[2] = $args[6] = "";
                    }
                        
                    $printForm = '<input type="checkbox" %s onclick="toggleCheckBox(\'%s\')" />'.
                    '<input type="color" %s id="%s" name="%s" value="%s" style="width:60px; %s" onkeydown="if(event.keyCode==13)event.keyCode=9" />';
                    
                    return vsprintf($printForm, $args);

                case "JSON":
                    $para = array();
                    $tmptbl ='';

                    foreach($this->options["fields"]  as $eachShowName =>$eachFieldName)
                    { 
                        $para[] = $this->field.'_'.$eachFieldName->field.'[]';
                    }

                    $para = "'#JSON_".$this->field."', ['".implode("','", $para)."'], 'float:left; width:127px;'";
                    $btn = '<a style="cursor:pointer; text-weight:bold; width:24px;" onclick="add2('.$para.')" ><i class="fa fa-plus" ></i></a>';          
                    $myInv = json_decode($thisValue, true);      

                    $fw = (int)(12 / count($this->options["fields"])); // bootstrap grid field width;

                    $tmptbl .= '<div style="width:800px;"><div class="col-xs-10 " align="center">';
                    foreach($this->options["fields"]  as $eachShowName =>$eachFieldName)
                    {
                        $tmptbl .= '<div class="col-xs-'.$fw.'">'.$eachFieldName->label.'</div>';
                    }           
                    $tmptbl .= '</div><div class="col-xs-1" >'.$btn.'</div></div>';
                    $tmptbl .= '<div id="JSON_'.$this->field.'">';

                    
                    //var_dump($myInv);
                    
                    if (is_array($myInv)){     
                        foreach($myInv as $eachValue) { 

                            $tmprow = '';
                            foreach($this->options["fields"]  as $eachShowName=>$eachFieldName) {
                                $tmprow .= '<div class="col-xs-'.$fw.'">'.$eachFieldName->renderControls($eachValue[$eachShowName]).'</div>';
                                // $tmprow .= '<input type="text" size="10" id="'.$this->field.'_'.$eachFieldName.'[]" name="'.$this->field.'_'.$eachFieldName.'[]" value="" style="float:left" />';
                            }
                            $tmptbl .= '<div class="json_row" style="width:800px;"><div class="col-xs-10 " align="center">' .
                                            $tmprow .
                                        '</div><div class="col-xs-1"><div style="float:left; width:22px; padding-top:12px; cursor:pointer" onclick="removeThis($(this))" align="center"><i class="fa fa-times"></i></div></div></div>';
                        } 
                    } else     
                    {
                        $tmprow = '';
                        foreach($this->options["fields"]  as $eachShowName=>$eachFieldName) {
                            $tmprow .= '<div class="col-xs-'.$fw.'">'.$eachFieldName->renderControls().'</div>';
                        }
                        $tmptbl .= '<div class="json_row" style="width:800px;"><div class="col-xs-10 " align="center">' .
                                        $tmprow .
                                    '</div><div class="col-xs-1"><div style="float:left; width:22px; padding-top:12px; cursor:pointer" onclick="removeThis($(this))" align="center"><i class="fa fa-times"></i></div></div></div>';
                    }
                
                    $tmptbl .= '</div>';
                    return $tmptbl.'<br clear="all" />';
                    

                /* case "GOOGLEMAP": To-do

                    $thisValue = str_replace('"', "''", html_entity_decode($thisValue));
            
                    $cms->admin_js["gmap"][] = '<script src="http://maps.google.com/maps/api/js?sensor=false"></script>';          
                    $cms->admin_js["gmap"][] = '<script type="text/javascript" src="'.$adminPath.'/js/jquery.tinyMap.js"></script>';          
                    $cms->admin_js["gmap"][] = '<script type="text/javascript" src="'.$adminPath.'/js/googlemap.js"></script>';          
                            
                    return '<div style="padding-bottom:10px;">'.
                                 '<input type="text" style="margin-right: 10px;" id="'.$this->field.'" class="GOOGLEMAP" name="'.$this->field.'" value="'.$thisValue.'" onkeydown="if(event.keyCode==13)event.keyCode=9" />
                                  <button type="button" id="clear">Clear</button></div><div style=\"width:100%; height:400px;\" id=\"map\"></div>'; */
                
                default : 
                    return "";
            }


                
        }

        protected function getpost(){
            if ($this->type == "JSON") {
                $this->value = array(); 

                $data = array();

                foreach ($_POST[$this->field] as $fieldName=>$fields){
                    foreach ($fields as $idx=>$field){
                        $data[$idx][$fieldName] = $field;
                    }
                }
                $this->value = str_replace(array("\'", '\"'), array("'", '"'), json_encode($data, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK));
                
            } else {
                parent::getpost();
            }
        }

        protected function update(&$fields) {
            if ($this->type == "JSON") {
                $fields[$this->field] = $this->value;
            } else {
                parent::update($fields);
            }
        }
    }

    class PermissionObj extends CMSObj {
        private $conn = false;

        function __construct($fieldName, $ops=array()) {
            global $conn;
            $this->conn = $conn;
            parent::__construct("PERMISSION", $fieldName, $ops);
        }

        function renderControls(){
            global $id;

            $permission_table = PERMISSION_TABLE;
            $account_role_table = ACCOUNT_ROLE_TABLE;

            $permRs = $this->conn->prepare("SELECT `pm`.`id`,`type`,`perm_name`,`perm_value`, ar.role_id ".
                                           "FROM    ".$permission_table." pm LEFT JOIN ".$account_role_table." ar ".
                                           "ON      pm.id = ar.perm_id AND ar.role_id = ? ".
                                          "ORDER BY pm.type asc, pm.display_priority desc");

            
            $result = $this->conn->GetAll($permRs, [$id]);
            $permList = array();

            foreach($result as $k=>$v){
                $permissionId = $v["id"];
                $permissionType = $v["type"];
                $permissionName = $v["perm_name"];
                $permissionValue =  $v["perm_value"];
                $permissionRoleId =  $v["role_id"];
        
                if(empty($permList[$permissionType])){
                    $permList[$permissionType] = array();
                }
                $permList[$permissionType][$permissionId] = array("id"=>$permissionId, "name"=>$permissionName,"val" => $permissionValue);
                $permList[$permissionType][$permissionId]["isChecked"] = (!empty($permissionRoleId));                    
            }

            $rows = array();

            foreach($permList as $k=>$v){
                $list = "";

                foreach($v as $role){
                    $checkbox = '<div class="checkboxContainer"><label><input type="checkbox" name="role[]" value="%s" %s>%s</label></div>';
                    $checked = ($role["isChecked"])? "checked='checked'" : "";
                    $list .= vsprintf($checkbox, array($role["id"], $checked, $role["name"]));
                }
                $rows[] = '<fieldset class=""><legend><label><input type="checkbox" class="checkAllToggle" >'.$k.'</label></legend>'.$list.'</fieldset>';
            
            }
            
            return implode("\r\n", $rows);
        }

    }
?>