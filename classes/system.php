<?php    
    include_once($rootPath.$appPath."/classes/cms.php");


    class District extends CMS{        

        protected $tableName = "district";  
        public $pageName = "Districts";        // CMS Page Name


        public $__cms_fields = array();

        protected $dimension = array();

        function __construct(){

            parent::__construct();

        	$this->dimension["thumb"] = array(200, 200);
        	$this->dimension["detail"] = array(808, 535);

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            $this->__cms_fields[] = new CMSObj("INT", "rank", ["label"=>"Sort Order", "list"=>["show", "sort"], "edit"=>["show", "insert", "readonly"], "width"=>150]);

             $this->__cms_fields[] = new ForeignObj("parent_id", ["label"=>"Parent District", "list"=>["show", "sort"],
                                                                            "options"=>["ft"=>[["table"=>"district", "value"=>"id", "label"=>"name"]]],]);
           
            $this->__cms_fields[] = new CMSObj("VARCHAR", "name", ["label"=>"Name", "width"=>350]);
            
            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);
        }

        public function getXhrList($q=""){
            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "name"=>"name");
            $mywhere["limit"] = 999;
            $mywhere["where"][] = "status = 'E' ";
            $mywhere["where"][] = "parent_id = 0 ";
            $mywhere["order"][] = "rank";

            $parentlist = array();
            foreach ( $this->get($this->tableName, $mywhere) as $obj) {
                $parentlist[$obj["id"]] = $obj["name"];
            }

            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "parent_id"=>"parent_id", "name"=>"name");
            $mywhere["limit"] = 999;
            $mywhere["where"][] = "status = 'E' ";
            $mywhere["where"][] = "parent_id <> 0 ";
            $mywhere["where"][] = "name LIKE '%".$q."%'";
            $mywhere["order"][] = "rank";

            $list = $this->get($this->tableName, $mywhere);
            
            $result = array();
            foreach ($list as $li) {
                $result[$li["parent_id"]][] = array(
                    "id" => (int)$li["id"], 
                    "text" => $li["name"]
                );
            }

            $return = array();
            foreach ($result as $parent_id=>$children) {
                $return["results"][] = array(
                    "id"=>$parent_id,
                    "text"=>$parentlist[$parent_id],
                    "children"=>$children
                );
            }

            return $return;
        }

        public function getName($ids) {
            $ids = explode(",", $ids);
            $row = $this->getOne($this->tableName, array("where"=>array("id in (".implode(",", $ids).")")));
            return $row["name"];
        }
    }
    
    class Subject extends CMS{        

        protected $tableName = "subject";  
        public $pageName = "Exam Subject";        // CMS Page Name

        public $__cms_fields = array();

        protected $dimension = array();

        function __construct(){

            parent::__construct();

            $this->dimension["thumb"] = array(200, 200);
            $this->dimension["detail"] = array(808, 535);

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            
            $this->__cms_fields[] = new CategoryObj("node_id", ["label"=>"Category", "tableName"=>$this->tableName, "list"=>["show", "sort", "search"], "width"=>120]);   
            
            
            $this->__cms_fields[] = new CMSObj("VARCHAR", "name", ["label"=>"Name", "width"=>350]);
            
            $this->__cms_fields[] = new SelectObj("SELECT", "mandatory", ["label"=>"Mandatory", "list"=>["show", "sort"], "width"=>100,  "options"=>array("No"=>"0", "Yes"=>"1")]);

            $this->__cms_fields[] = new SelectObj("SELECT", "grade", ["label"=>"Grade", "list"=>[], "width"=>100,  "options"=>array("1/2/3/4/5/5*/5**"=>1,
                                                                                                                                    "A/B/C/D/E/F/U"=>2, 
                                                                                                                                    "A/B/C/D/E/U"=>3)]);

            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);
        }

        function getAllCat(){
            $data = parent::getCate($this->tableName,0,1);
            foreach ($data as &$ddd) {
                $ddd["compulsory"] = $this->get($this->tableName, ["where"=>["node_id = '".$ddd["node_id"]."'", "mandatory = 1", "status = 'E'"], "order"=>["rank"]]);
            }
            return $data;
        }


        public function getXhrList($q="", $exam=""){
            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "name"=>"name", "grade"=>"grade");
            $mywhere["limit"] = 999;
            $mywhere["where"][] = "status = 'E' AND mandatory = 0";
            if ($q != "") {
                $mywhere["where"][] = "name LIKE '%".$q."%'";
            }
            if ($exam != "") {
                $exam = $this->getOne($this->tableName."_node", array("where"=>array("node_id = '".$exam."' OR node_name = '".$exam."'")));
                
                $mywhere["where"][] = "node_id = '".$exam["node_id"]."'";
            }
            $mywhere["order"][] = "rank";

            $list = $this->get($this->tableName, $mywhere);
            $result = array();
            foreach ($list as $li) {
                $result["results"][] = array(
                    "id" => (int)$li["id"], 
                    "text" => $li["name"],
                    "tag" => $li["grade"]
                );
            }

            return $result;
        }
    } 

    class Program extends CMS{        

        protected $tableName = "program";  
        public $pageName = "Programme";        // CMS Page Name
        public $id;
        public $__cms_fields = array();

        protected $dimension = array();

        function __construct($id = false){

            parent::__construct();

            $this->dimension["thumb"] = array(200, 200);
            $this->dimension["detail"] = array(808, 535);

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            
            //$this->__cms_fields[] = new CategoryObj("node_id", ["label"=>"Category", "tableName"=>$this->tableName, "list"=>["show", "sort", "search"]]);   
            
            // $this->__cms_fields[] = new CMSObj("INT", "rank", ["label"=>"Sort Order", "list"=>["show", "sort"], "edit"=>["show", "insert", "readonly"], "width"=>150]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "code", ["label"=>"Programme Code", "width"=>120]);
            
            $this->__cms_fields[] = new CMSObj("VARCHAR", "name", ["label"=>"Programme Name", "width"=>400]);
            
             $this->__cms_fields[] = new CMSObj("INT", "max_job", ["label"=>"Max job", "list"=>["show"], "width"=>100,  "options"=>array("tips"=>"Maximum number of Job Preference per Application")]);


            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);

            if ($id) { $this->id = $id; }
        }

        public function getListByIds($ids) {
            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "name"=>"CONCAT(code,' ', name)" );
            $mywhere["limit"] = 999;
            $mywhere["where"][] = "id in (".implode(",", $ids).")";
            $mywhere["order"][] = "code";

            $list = $this->get($this->tableName, $mywhere);
            $result = array();
            foreach ($list as $li) {
                $result[(int)$li["id"]] =  $li["name"];
            }
            return $result;

        }

        public function getXhrList($q=""){
            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "name"=>"CONCAT(code,' ', name)" );
            $mywhere["limit"] = 999;
            $mywhere["where"][] = "status = 'E'";
            if ($q != "") {
                $mywhere["where"][] = "(name LIKE '%".$q."%' OR code like '%".$q."%')";
            }
            $mywhere["order"][] = "code";

            $list = $this->get($this->tableName, $mywhere);
            $result = array();
            foreach ($list as $li) {
                $result["results"][] = array(
                    "id" => (int)$li["id"], 
                    "text" => $li["name"],
                );
            }

            return $result;
        }



        public function getDetail(){
            return $this->getOne($this->tableName, ["where"=>["id = '".$this->id."'"]]);
        }

    } 

    class EmailTemplate extends CMS{        

        protected $tableName = "emailtemplate";  
        public $pageName = "Email Template";        // CMS Page Name


        public $__cms_fields = array();

        protected $dimension = array();

        function __construct(){

            parent::__construct();

            $this->dimension["thumb"] = array(200, 200);
            $this->dimension["detail"] = array(808, 535);

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "name", ["label"=>"Email function", "edit"=>["show", "insert"], "width"=>350]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "subject", ["label"=>"Email Subject", "width"=>350]);
            $this->__cms_fields[] = new CMSObj("TEXT", "content", ["label"=>"Content", "list"=>[], "width"=>350]);
            
            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);
        }

        public function getTemplate($action, $content) {
            $template = $this->getOne($this->tableName, array("where"=>array("name = '".$action."'")));
        
            foreach ($content as $idx => $value) {
                $template["content"] = str_replace("{".$idx."}", $value, $template["content"]);
            }
            return $template;
        }
    }

    class JobType extends CMS{        

        protected $tableName = "jobs_type";  
        public $pageName = "Job Type";        // CMS Page Name

        public $__cms_fields = array();

        protected $dimension = array();

        function __construct(){

            parent::__construct();


            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            $this->__cms_fields[] = new CMSObj("Rank", "rank", ["label"=>"Rank", "list"=>["show", "sort"]]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "title", ["label"=>"Title", "edit"=>["show", "insert", "update"], "width"=>350]);
            
            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);
        }

        public function getType() {
            return $this->get("jobs_type", ["where"=>["status = 'E'"]]);
        }
    }


    class DataManipulator extends CMS {

        //protected $tableName = "program";  
        public $pageName = "Data Manipulate";        // CMS Page Name
        public $id;
        public $__cms_fields = array();

        function __construct($id = false){

            parent::__construct();
            $this->__cms_fields = array();

            $this->__cms_fields["Student"] = array("student"=>"Master Records", "student_meta"=>"Student Metadata", "personal_cv"=>"Student CV");
            
            $this->__cms_fields["Lecturer"] = array("lecturer"=>"Master Records");

            $this->__cms_fields["Job"] = array("jobs"=>"Master Records", "jobs_lecturer"=>"Lecturer Permission", "jobs_permission"=>"Target students");

            $this->__cms_fields["Job Application"] = array("application"=>"Master Records", "application_item"=>"Application Jobs items");
            
            $this->__cms_fields["Web CMS"] = array("page"=>"Master Records");

            $this->__cms_fields["Static data"] = array("district"=>"District", "program"=>"Program", "subject_node"=>"Exam", "subject"=>"Exam Subject");
            
            $this->__cms_fields["System users"] = array("account"=>"Master Records", "account_role"=>"Account Role", "role"=>"Role Records", "role_permission"=>"Role Permission");
            
            $this->__cms_fields["System record"] = array("file_obj"=>"File Object", "syslog"=>"System Log");
        }

        public function getStudent() {
            return $this->conn->GetAll("Select * from student where status = 'E' and id in (Select member_id from file_obj) order by student_id ");
        }

        public function export($tables, $password, $where = array(), $nocreate=false) {

            global $dbHost, $dbUser, $dbPassword, $dbDatabase;
            global $rootPath, $appPath;

            $session = time();

            set_time_limit(300);

            $destPath = $rootPath.$appPath."/dataexport";

            mkdir($destPath, 0777);

            $this->conn->Execute("Update `system` set maintenanceFlag = 1");

            foreach ($tables as $ttt) {

                $mywhere = ($where[$ttt] != "") ?  '--where="'.$where[$ttt].'"' : "";
                $nocreate = ($nocreate) ? "--no-create-info" : "";

                $destFile = $destPath."/".$ttt.".sql";
                $cmd = "mysqldump -h %s -u %s --password='%s' %s %s %s %s > %s";
                $cmd = sprintf($cmd, $dbHost, $dbUser, $dbPassword, $dbDatabase, $ttt, $mywhere, $nocreate, $destFile);   
                exec($cmd);
            }
            
            $cmd = "7za a -P%s %s/cvb-data-%s.zip %s/*.sql ";

            $cmd = sprintf($cmd, $password, $destPath, $session, "../../dataexport");
            exec($cmd);

            $this->conn->Execute("Update `system` set maintenanceFlag = 0");
            
            foreach ($tables as $ttt) {
                @unlink($destPath."/".$ttt.".sql");
            }
            return $session;
        }

        public function removeFiles($student_ids) {
            $this->conn->Execute("DELETE from `file_obj` where member_id in ('".implode("','", $student_ids)."')");
        }

        public function upload ($importzip) {
            global $uploadedFilePath;

            $session = time();
            
            if ($importzip["error"] != 0) {
                return array("status"=>0, "msg"=> $importzip["error"]);
            }
            @mkdir($uploadedFilePath."/dataimport", 0700);
            move_uploaded_file($importzip["tmp_name"], $uploadedFilePath."/dataimport/".$session.".zip");
            return array("status"=>1, "session"=>$session);
        }

        public function process ($session_id){
            global $uploadedFilePath;
            $zip_name = $uploadedFilePath. "/dataimport/".$session_id.".zip";
            $dir_name = $uploadedFilePath. "/dataimport/".$session_id."/";

            if (!file_exists($zip_name)) {
                return array("status"=>1, "msg"=>"Done!", "zip_name"=>$zip_name);
            } else {
                if (file_exists($dir_name)) {
                    return array("status"=>0, "msg"=>"Waiting....");
                } else {
                    @mkdir($uploadedFilePath."/dataimport/".$session, 0700);
                    $zip = new ZipArchive();
                    if ($zip->open($zip_name) == TRUE) {
                        if ($zip->extractTo($dir_name)) {
                            $msg = "Extracting Zip file with ..." . $zip->getStatusString(). "\n";
                            $zip->close();
                            if ($dh = @opendir($dir_name)) {
                                while (($file = readdir($dh)) !== false) {
                                    if ($file != "." && $file != "..") {
                                        $validSQL = false;
                                        foreach ($this->__cms_fields as $section) {
                                            if (array_key_exists(str_replace(".sql", "", $file), $section)) {
                                                $validSQL = true;
                                                break;
                                            }
                                        }
                                        if ($validSQL) {
                                            $count++;
                                        } else {
                                            if (!is_dir($dir_name.$file)) {
                                                @unlink($dir_name.$file);
                                            } else {
                                                $files = array_diff(scandir($file), array('.','..')); 
                                                foreach ($files as $file) { 
                                                    (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
                                                } 
                                                rmdir($dir);
                                            }
                                        }
                                    }
                                }

                                $msg .= $count ." valid SQL file(s) found. \n";
                            }

                            return array("status"=>0, "msg"=>$msg);
                        } else {
                            return array("status"=>0, "msg"=>"");
                        } 

                        
                    }
                    

                }
                
            }
        }
    }
?>