<?php
    include_once($rootPath.$appPath."/classes/cms.php");
    include_once(__DIR__."/student.php");
    include_once(__DIR__."/jobs.php");
    include_once(__DIR__."/system.php");
    require_once(__DIR__."/jpdf.php");

    include_once($rootPath.$appPath."/send_email.php"); 

    class Application extends CMS{

        protected $tableName = "application";  
        protected $itemTable = "application_item";  

        protected $id;
        public $pageName = "WIE Application";        // CMS Page Name
        public $__cms_fields = array();
        const SESSION_KEY = "polyuhkcc:cvb::application";
        public $cvform;

        function __construct($id=false){

            parent::__construct();

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
                
            $this->__cms_fields[] = new ForeignObj( "member_id", ["label"=>"Student", "list"=>["show"],
                                                                            "options"=>["ft"=>[["table"=>"student", "value"=>"id", "label"=>"student_id"]]],]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "app_no", ["label"=>"Application No."]);
            
            $this->__cms_fields[] = new CMSObj("INT", "no_attempt", ["label"=>"No. Attempt", "list"=>["show", "sort"], "width"=>150,  "edit"=>[]]);
            $this->__cms_fields[] = new CMSObj("INT", "job_selected", ["label"=>"Job Selected", "list"=>["show", "sort"], "width"=>150, "edit"=>[]]);
            //$this->__cms_fields[] = new CMSObj("VARCHAR", "subj_code", ["label"=>"Subject Code", "list"=>[]]);
            // $this->__cms_fields[] = new CMSObj("VARCHAR", "subj_title", ["label"=>"Subject Title", "list"=>[]]);
    
            $this->__cms_fields[] = new SelectObj("", "status", ["label"=>"Status", "width"=>200,  "options"=>array("<span style=\"color: #00FF00\">OPEN</span>"=>"OPEN", "<span style=\"color: #808080\">PENDING</span>"=>"PENDING", "<span style=\"color: #800000\">CLOSED</span>"=>"CLOSED","<span style=\"color: #808080\">NOT YET SUBMITTED</span>"=>"NEW",)]);            
            
            //$this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "submission_date", ["label"=>"Submission Date", "width"=>200, "list"=>["show", "sort"], "edit"=>[]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);

            if ($id)  $this->id = $id;

        }


        public function add($job_id, $member_id){
            $job = new Jobs((int)$job_id);
            $job = $job->getDetails();

            if (!$job){
                return array("status"=>0, "msg"=>"Job not found");
            }

            $student = new Student($member_id);
            $me = $student->getDetail();
            
            $details = $this->getDetail();

            if ($details["status"] != "NEW") {
                return array("status"=>0, "msg"=>"Your application has already submitted at ".$details["submission_date"].". Please wait for the result.");
            }

            $mywhere = array();
            $mywhere["where"][] = "job_id = ?";
            $mywhere["where"][] = "program_id = ?";
            $mywhere["where"][] = "years like ?";
            
            $mywhere["where_parameters"] = array($job_id, $me["prog_code"], '%'.$me["yrofstudy"].';%');

            $allow = $this->getOne("jobs_permission", $mywhere);

            if (!$allow) {
                return array("status"=>0, "msg"=>"Jobs not for this student");
            }

            $exist = array();
            $exist["where"] = array("jobs_id = ?", "application_id = ?");
            $exist["where_parameters"] = array($job_id, $this->id);

            $exist = $this->get($this->itemTable, $exist);
            
            if (!$exist) {
                $data = array();
                $data["application_id"] = $this->id;
                $data["jobs_id"] = $job_id;
                $data["priority"] = $this->conn->GetOne("Select count(*) from application_item where application_id = '".$this->id."'") + 1; 
                $data["status"] = "";
                $data["create_date"] = date("Y-m-d H:i:s");

                if ($me["program"]["max_job"] < $data["priority"]) {
                    return array("status"=>0, "msg"=>"You can only select ".$me["program"]["max_job"]." posts. ");
                } else {
                    $this->conn->AutoExecute($this->itemTable, $data, "INSERT");
                    $this->conn->Execute("Update application set job_selected = job_selected + 1 where id = '".$this->id."'");

                }
                
            }
            return array("status"=>1, "msg"=>"ok");

        }

        public function remove($job_id, $member_id){

            $details = $this->getDetailByArray();

            if ($details["member_id"] != $member_id){
                return array("status"=>0, "msg"=>"Job not found");
            }
            
            $exist = array();
            $exist["where"] = array("id = ?", "application_id = ?");
            $exist["where_parameters"] = array($job_id, $this->id);
            
            $exist = $this->get($this->itemTable, $exist);

            if ($exist) {
                $this->conn->Execute("DELETE from ".$this->itemTable." where id = '".$exist[0]["id"]."'");

                $items = $this->getItems();

                foreach ($items as $item) {
                    $priority++;
                    $this->conn->Execute("UPDATE application_item set priority = '".$priority."' where id = '".$item["id"]."'");
                }

                $this->conn->Execute("UPDATE application 
                                        SET job_selected = ".count($items)."
                                        WHERE id = '".$this->id."'");

                return array("status"=>1, "msg"=>"ok");
            } else {
                return array("status"=>0, "msg"=>"Job not found");
            }

        }

        private function save(){
            $this->conn = false;
            session_start();
            $_SESSION[base64_encode(self::SESSION_KEY)] = serialize($this);
            session_write_close();
        }

        public function getItems(){
            $mywhere = array();
            $mywhere["where"][] = "application_id = ?"; 
            $mywhere["where_parameters"][] = $this->id;
            $mywhere["order"][] = "priority";

            $data = $this->get($this->itemTable, $mywhere);
            
            foreach ($data as &$job) {
                $nn = array();
                $nn["where"][] = "id = ?";
                $nn["where_parameters"][] = $job["jobs_id"];

                $job["job"] = $this->getOne("jobs", $nn);
            }

            return $data;
        }

        public static function retain() {
            
            $obj = new Application();
            session_start();
            if (array_key_exists(base64_encode(self::SESSION_KEY), $_SESSION)) {
                $existObj = unserialize($_SESSION[base64_encode(self::SESSION_KEY)]);
                $existObj->conn = $obj->conn;
                return $existObj;
            }
            session_write_close();
            return $obj;
        }

        public function submit($selfaccess){
            $details = $this->getDetail();

            $student = new Student($details["member_id"]);
            $student = $student->getDetail();

            $student_row = array();

            $data = array();
            $data["status"] = "OPEN";
            $data["submission_date"] = date("Y-m-d H:i:s");
            $data["lastModDate"] = date("Y-m-d H:i:s");

            $recordCount = $this->conn->GetOne("Select count(*) as recordCount from application where member_id = '".$details["member_id"]."'");

            $student_row["wie_no"] = $data["app_no"] = $student["student_id"] . sprintf("%03s", $recordCount);

            while ($this->conn->GetOne("Select count(*) as recordCount from application where app_no = '".$data["app_no"]."'")) {
                $student_row["wie_no"] = $data["app_no"] = $student["student_id"] . sprintf("%03s", $recordCount++);               
            }

            $this->conn->AutoExecute($this->tableName, $data, "UPDATE", "id='".$this->id."' AND member_id = '".$details["member_id"]."'");

            $this->conn->AutoExecute("student", $student_row, "UPDATE", "id='".$details["member_id"]."'");

            return array("status"=>1, "msg"=>"OK");
        }

        public function update($data_arr){
            $details = $this->getDetail();
            
            $data = array();
            $data["lastModDate"] = date("Y-m-d H:i:s");
            $data["selfaccess"] = $data_arr["selfassess"];
            
            $this->conn->AutoExecute($this->tableName, $data, "UPDATE", "id='".$this->id."' AND member_id = '".$details["member_id"]."'");
            
            return array("status"=>1, "msg"=>"OK");
        }

        public function getApplicationNumber(){
            $data = $this->getDetail();

            return $data["app_no"];
        }

        public function getFromHashId($hash){
            $mywhere = array();
            $mywhere["where"][] = "sha1(concat('application-', id)) = ?";
            $mywhere["where_parameters"][] = $hash;

            $id = $this->getOne($this->tableName, $mywhere);
            if ($id) {
                $this->id = $id["id"];
            }
            return $this;
        }

        public function getDetail(){
            $data = parent::getDetail($this->tableName, $this->id);

            $mywhere = array();
            $mywhere["where"][] = "application_id = ?";
            $mywhere["where_parameters"][] = $this->id;
            $mywhere["order"][] = "priority ";
            
            $data["items"] = $this->get($this->itemTable, $mywhere);

            foreach ($data["items"] as &$item) {
                $job = array();
                $job["where"][] = "id = ?";
                $job["where_parameters"][] = $item["jobs_id"];

                $item["job"] = $this->getOne("jobs", $job);
            }

            return $data;
        }

        public function getCVDetail() {
            $data = parent::getDetail($this->tableName, $this->id);
            $return = array();
            $return["ENAME"] = $data["firstname_en"] . " " . $data["lastname_en"];
            $return["CNAME"] = $data["firstname_zh"] . " " . $data["lastname_zh"];
            $return["ADDRESS"] = $data["address1"] . "<br />" . $data["address2"] . "<br />" . $data["address3"];
            $return["CONTACT"] = $data["contact"];
            $return["EMAIL"] = $data["email"];
            $return["GENDER"] = $data["gender"];
            $return["AGE"] = $data["age"] ;
            $return["PHOTO"] = "http://" . $_SERVER["HTTP_HOST"] . UFILE_PATH. $data["photo"];

            $return["CGPA"] = $data["cgpa"];
            return $return;
        }

        public function getDetailByArray($mywhere) {

            $mywhere["where"][] = "id = ?";
            $mywhere["where_parameters"][] = $this->id;
            
            return $this->getOne($this->tableName, $mywhere);
        }

        public function getXhrList( $q=""){
            $mywhere = array();
            $mywhere["limit"] = 999;

            if ($q != "") {

                //$mywhere["where"][] = "`status` = 'OPEN'";
                $mywhere["where"][] = "`app_no` LIKE concat('%', ?, '%')";
                $mywhere["where_parameters"][] = $q;
                $mywhere["order"][] = "app_no";
            }
            
            $list = $this->get($this->tableName, $mywhere);
            $result = array();
            foreach ($list as $li) {
                $result["results"][] = array(
                    "id" => $li["id"], 
                    "text" => $li["app_no"],
                );
            }

            return $result;
        }

        public function getItemList($mywhere){


            $jointable = "application_item item join application app on item.application_id = app.id
                                                join student std on app.member_id = std.id
                                                join jobs on item.jobs_id = jobs.id";
            $mywhere["fields"] = array("item.*");
            $data =  $this->get($jointable, $mywhere);


            $applications = array();

            foreach ($data as $item) {
                if (!array_key_exists($item["application_id"], $applications)) {
                    $app = array();

                    $appObj = new Application($item["application_id"]);
                    $app["application"] = $appObj->getDetail();

                    $student = new Student($app["application"]["member_id"]);
                    $app["student"] = $student->getDetail(); 

                    $program = new Program($app["student"]["prog_code"]);
                    $app["programme"] = $program->getDetail();

                    $applications[$item["application_id"]] = $app;
                }
            }

            foreach ($applications as $idx=>&$app) {

                $items = array();
                $items["where"][] = "application_id = ?";
                $items["where_parameters"][] = $idx;
                $items["order"][] = "priority";

                $items = $this->get($this->itemTable, $items);

                foreach ($items as $item) {
                    $job = new Jobs($item["jobs_id"]);
                    $item["job"] = $job->getDetails(); 
                    $app["items"][$item["jobs_id"]] = $item;
                }
            }
            
            return $applications;
        }

        public function findApplicationsByJobNo($job_no) {

            $job = $this->getOne("jobs", array("where"=>array("job_no = '$job_no'")));

            return $this->findApplicationsByJobId($job["id"]);
        }

        public function findApplicationsByJobId($job_id) {

            $applications = $this->get($this->itemTable, array("where"=>array("jobs_id = '".$job_id."'")));
       
            $return = array();

            foreach ($applications as $app) {
                
                
                if (!array_key_exists($app["application_id"], $return)) {
                    $item = array();
                    $item["item"] = $app;

                    $applObj = new Application($app["application_id"]);
                    $item["application"] = $applObj->getDetail(); 
                     
                    $student = new Student($item["application"]["member_id"]);
                    $item["student"] = $student->getDetail(); 

                    $program = new Program($item["student"]["prog_code"]);
                    $item["programme"] = $program->getDetail();

                    
                    $return[$app["application_id"]] = $item;
                }
            }

            return $return;
        }

        public function getApplicationByItemId($item_id) {
            $applications = $this->get($this->itemTable, array("where"=>array("id = '".$item_id."'")));
            

            return new Application($applications[0]["application_id"]);
        }

        public function getApplicationByItemSha1Id($item_id) {
            $applications = $this->get($this->itemTable, array("where"=>array("SHA1(CONCAT('appli-job-', id)) = '".$item_id."'")));
            

            $this->id = $applications[0]["application_id"];
            return $applications[0];
        }

        public function updateStatus($status){
            $application = array();
            $offer = array();


            foreach ($status as $key => $value) {
                $item = $this->getOne($this->itemTable, array("where"=>array("id = '".$key."'")));

                $job_id = $item["jobs_id"];


                if (!in_array($item["application_id"], $application)) {
                    $application[] = $item["application_id"];
                }
                if ($value == "offered") {
                    $offer[] = $item;
                }
                $this->conn->AutoExecute($this->itemTable, array("status"=>$value, "lastModDate"=>date("Y-m-d H:i:s")), "UPDATE", "id = '".$item["id"]."'");
            }

            //$this->sendOfferEmail($offer);            

            foreach ($application as $app_id) {
                $this->id = $app_id;

                $not_all_closed = $this->get($this->itemTable, array("where"=>array("status <> 'closed'", "application_id = '".$app_id."'")));

                if (count($not_all_closed) == 0) {
                    $this->conn->AutoExecute($this->tableName, array("status"=>"CLOSED"), "UPDATE", "id = '".$app_id."'");
                }
            }

            return $job_id;
        }


        public function sendOfferEmail($offeredItem=FALSE) {

            if ($offeredItem === FALSE) {
                $mywhere = array();
                $mywhere["where"][] = "application_id = '".$this->id."'";
                $mywhere["where"][] = "status = 'offered'";
                $offeredItem = $this->get($this->itemTable, $mywhere);

            }

            foreach ($offeredItem as $item) {

                $application = new Application($item["application_id"]);
                $app = $application->getDetail();

                $student = new Student($app["member_id"]);
                $stud = $student->getDetail();
                
                $job = new Jobs($item["jobs_id"]);
                $job = $job->getDetails();
                
                $email = $stud["c_email"];

                $emailtemplate = new EmailTemplate();

                $content = array(
                            "ENAME"=>$app["firstname_en"] . " " . $app["lastname_en"],
                            "JOB_NO"=>$job["job_no"],
                            "JOB_NAME"=>$job["name"],
                            "COMPANY"=>$job["company"],
                            "DEPARTMENT"=>$job["dept"] . (($job["company"] != "") ? "," : "")
                        );

                $emailtemplate = $emailtemplate->getTemplate("JOB_OFFER", $content);

                $this->conn->AutoExecute($this->itemTable, array("offer_send_date"=>date("Y-m-d H:i:s")), "UPDATE", "id = '".$item["id"]."'");
            
                sendMail($emailtemplate["subject"], html_entity_decode($emailtemplate["content"], ENT_QUOTES,"UTF-8"), $email, array("WIE Application", "ccwie@hkcc-polyu.edu.hk"));
            }
            return array("status"=>1, "msg"=>"ok");
        }


        public function offerApplication($item_id){
            $this->conn->AutoExecute($this->itemTable, array("status"=>"OFFERED"), "UPDATE", "id = '$item_id'");
            return array("status"=>1, "msg"=>ok);
        }

        public function exportXls($ids, $password=""){
            global $pathClassLib;
            require_once(__DIR__."/../lib_common/phpexcel/PHPExcel.php");
            

            $mywhere = array();
            $mywhere["where"][] = "app.id in ('".implode("','", $ids)."')";

            $data = $this->getItemList($mywhere);
          

            $fields = array(
                "jobs.job_no" => array("title"=> "WIE No", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>15),
                "std.student_id"=> array("title"=> "Student No", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>10),
                "app.en_name"=> array("title"=> "English Name", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>11),
                "app.ch_name"=> array("title"=> "Chinese Name", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>11),
                "app.contact"=> array("title"=> "Mobile Phone", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>11),
                "std.prog_code"=> array("title"=> "Programme Code", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>50),
                "app.status"=> array("title"=> "Status", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>7),
                "app.submission_date"=> array("title"=> "Submission Date", "dataType" => PHPExcel_Cell_DataType::TYPE_STRING, "width"=>17),
                "app.job_selected"=> array("title"=> "No. of Applied Job", "dataType" => PHPExcel_Cell_DataType::TYPE_NUMERIC, "width"=>14),
            );


            $thisRow = 1; $thisCol = 0;

            $objPhpExcel = new PHPExcel();

            $worksheet = $objPhpExcel->setActiveSheetIndex(0);
           
            foreach ($fields as $field) {
                $worksheet->getColumnDimensionByColumn($thisCol)->setWidth($field["width"]);
                $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow, $field["title"]);

            }
            $thisRow++;
            $thisCol = 0;

            $jobs = array();

            foreach ($data as $row) {
                $thisCol = 0;

                $value = array(
                    "jobs.job_no"    => $row["application"]["app_no"],
                    "std.student_id" => $row["student"]["student_id"],
                    "app.en_name"    => $row["application"]["firstname_en"] . " " . $row["application"]["lastname_en"],
                    "app.ch_name"    => $row["application"]["lastname_zh"] . $row["application"]["firstname_zh"], 
                    "app.contact"    => $row["application"]["contact"],
                    "std.prog_code"  => $row["programme"]["code"] . " " . $row["programme"]["name"],
                    "app.status"     => $row["application"]["status"], 

                    "app.submission_date" => $row["application"]["submission_date"],
                    "app.job_selected" => $row["application"]["job_selected"]
                );

                foreach ($value as $idx=>$thisvalue) {
                    $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow, $thisvalue, true)->setDataType($fields[$idx]["dataType"]);
                }

                foreach ($row["items"] as $idx=>$item) {
                
                    $title = sprintf("%s - %s (%s)", $item["job"]["job_no"], $item["job"]["name"], $item["job"]["company"]);
                    $jobs[$title][$thisRow] = $item["priority"];
                    
                }
                $thisRow++;
            }


            foreach ($jobs as $title=>$priority) {
                $worksheet->setCellValueByColumnAndRow($thisCol, 1, $title, true)->setDataType(PHPExcel_Cell_DataType::TYPE_STRING);
                foreach ($priority as $rowNo => $value) {
                    $worksheet->setCellValueByColumnAndRow($thisCol, $rowNo, $value, true)->setDataType(PHPExcel_Cell_DataType::TYPE_NUMERIC);
                }

                $thisCol++;
            }
            
            
            return $objPhpExcel;

        }

        public function generateApplicationPDF($lecturer){
            global $domain, $appPath;
            $application = $this->getDetail();

            $student = new Student($application["member_id"]);
            $data = $student->getCVForm($this->id);
            $student = $student->getDetail();
            
            $html2pdf = new JayPDF('P', 'A4', 'en', true, 'UTF-8');
            $test2pdf = new JayPDF('P', 'A4', 'en', true, 'UTF-8');
            $html2pdf->setTestIsImage(0);
            $test2pdf->setTestIsImage(0);
            $pageHeight = 0;

            $html2pdf->writeHTML('<style>
                th, td {font-size: 12px; }
                table {width: 100%; table-layout:fixed; }
                table tr:first-child {background-color: #CCCCCC; text-align: center;}
                h3 {font-size: 14px;  }
                table table tr:first-child {background-color: transparent;}
            </style>');

            $html2pdf->addFont('cid0ct');

            // BASIC
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center" >'.
                    '<tr><th colspan="5" bgcolor="#CCCCCC" align="center" ><h3>Personal Particulars</h3></th></tr>'.
                    '<tr><th style="width: 100px; padding: 8px;">English Name:</th><td style="width: 100px; padding: 8px;">%s</td>'.
                        '<th style="width: 100px; padding: 8px;">Chinese Name:</th><td style="width: 90px; padding: 8px; font-family:cid0ct;">%s</td>'.
                        '<td style="width: 200px;" rowspan="4">%s</td></tr>'.
                    '<tr><th style="padding: 8px;">Home District:</th><td style="padding: 8px;">%s</td>'.
                        '<th style="padding: 8px;">Gender: </th><td style="padding: 8px;">%s</td></tr>'.                
                    '<tr><th style="padding: 8px;">College Email:</th><td style="padding: 8px;" colspan="3">%s</td></tr>'.
                    '<tr><th style="padding: 8px;">Contact Number:</th><td style="padding: 8px;" colspan="3">%s</td></tr>'.
                '</table><p></p>';

            $ENAME = $data["personal"]["en_name"].' ' .$data["personal"]["en_lastname"];
            $CNAME = $data["personal"]["ch_lastname"].$data["personal"]["ch_name"];
            $PHOTO = '';

            
            if ($application["photo"] != "") {

                $nqFileObj = new NqFileObj($student["id"]);

                $application["photo"] = end(explode("-", $application["photo"]));                
                $tmpfile = $nqFileObj->generatefile($application["photo"], ".jpg");
                
                $url = "https://".$_SERVER["HTTP_HOST"].$appPath."/". $tmpfile;

                $PHOTO = '<img src="'.$url.'" width="200"  />';
            }
            
            $HOME = $data["personal"]["district"]["name"];
            $GENDER = $data["personal"]["gender"];
            $EMAIL = $student["c_email"];
            $CONTACT = $data["personal"]["contact"];

            $this->writeHTML(sprintf($html, $ENAME, $CNAME, $PHOTO, $HOME, $GENDER, $EMAIL, $CONTACT), $html2pdf, $test2pdf, $pageHeight);

            // Education
            $html = '<table  border="1" cellspacing="0" cellpadding="3pt" align="center">
                        <tr><th colspan="4" bgcolor="#CCCCCC" align="center"><h3>Education</h3></th></tr>
                        <tr><th style="width: 40pt; padding: 8pt;">From<br>(MM/YY):</th>
                            <th style="width: 40pt; padding: 8pt;">To<br>(MM/YY):</th>
                            <th style="vertical-align: top; width: 150pt; padding: 8pt; ">School/College:</th>
                            <th style="vertical-align: top; width: 150pt; padding: 8pt;">Class/Course of Study (Study Year)</th></tr>%s
                    </table><p></p>';

            foreach ($data["education"] as $edu) {
                $content = json_decode($edu["content"], true);
                $rows .= sprintf('<tr><td style="padding: 4pt 8pt;"><div style="width: 40pt; ">%s</div></td>'.
                                      '<td style="padding: 4pt 8pt;"><div style="width: 40pt; ">%s</div></td>'.
                                        '<td style="padding: 4pt 8pt;"><div style="width: 184pt; ">%s</div></td>'.
                                        '<td style="padding: 4pt 8pt;"><div style="width: 190pt; ">%s (%s)</div></td></tr>', 

                                $content["fromdate"], $content["todate"], $content["school"], $content["class"], $content["year"]);
             }

            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);
                    
            // Academic Attainment 
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                    <tr><th colspan="3" bgcolor="#CCCCCC" align="center"><h3>Academic Attainment</h3></th></tr>
                    <tr><th style="width: 50px; padding: 8px;">Date Obtained</th>
                        <th style="width: 286px; padding: 8px;">School/College</th>
                        <th style="width: 300px; padding: 8px;">GPA/ Public Exam Results</th></tr>%s
                    </table><p></p>';

            $rows = "";

            if ($data["application"]["gpa"]["point"] > 0) {
                $rows .= sprintf("<tr><td style='padding: 8px;'>%s</td><td style='padding: 8px;'>%s</td><td style='padding: 8px;'>%s</td></tr>", 
                                $data["application"]["gpa"]["date"], $data["application"]["gpa"]["school"], "CGPA : " . $data["application"]["gpa"]["point"]);
            }
            if ($data["application"]["cgpa"]["point"] > 0) {
                $rows .= sprintf("<tr><td style='padding: 8px;'>%s</td><td style='padding: 8px;'>%s</td><td style='padding: 8px;'>%s</td></tr>",
                                $data["application"]["cgpa"]["date"], $data["application"]["cgpa"]["college"], "CGPA of Pre-Associate Degree : " . $data["application"]["cgpa"]["point"]);
            } else {
                $rows .= "<tr><td style='padding: 8px;'></td><td style='padding: 8px;'></td><td style='padding: 8px;'>CGPA of Pre-Associate Degree : N/A</td></tr>";
            }

            foreach ($data["exam_result"] as $edu) {
                $content = json_decode($edu["content"], true);
                $result = "";
                foreach ($content["exam_result"] as $key=>$value) {
                    $result .= "$key ($value)<BR>";
                }
                $rows .= sprintf('<tr><td style="padding: 8px;" valign="top">%s</td>
                                      <td style="padding: 8px;" valign="top">%s</td>
                                      <td style="padding: 8px;" valign="top">%s</td></tr>', $content["year"], $content["exam"], $result);
            }

            foreach ($data["public_exam"] as $edu) {
                $content = json_decode($edu["content"], true);
                
                $content["grade"] = json_decode($content["grade"], true);
                $result = "";
                foreach ($content["grade"] as $nnn=>$obj) {
                    $result .= "$nnn ($obj) <BR> ";
                }
                $rows .= sprintf('<tr><td style="padding: 8px;" valign="top">%s</td>
                                      <td style="padding: 8px;" valign="top">%s (%s)</td>
                                      <td style="padding: 8px;" valign="top">%s</td></tr>', $content["date"], $content["name"], $content["organization"], $result);
            }
            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);
                
            // Work Experience
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                    <tr><th colspan="6" bgcolor="#CCCCCC" align="center"><h3>Work Experience</h3></th></tr>
                    <tr><th style="width: 40px; padding: 8px;">From<br>(MM/YY):</th>
                        <th style="width: 40px; padding: 8px;">To<br>(MM/YY):</th>
                        <th style="width: 150px; padding: 8px;">Name of Organization</th>
                        <th style="width: 60px; padding: 8px;">Type of Employment</th>
                        <th style="width: 120px; padding: 8px;">Position </th>
                        <th style="width: 120px; padding: 8px;">Major Duties</th></tr>
                    %s
                </table><p></p>'; 

            $rows = "";

            if (count($data["experience"]["work"]) > 0) {
                foreach ($data["experience"]["work"] as $work) {
                    $work["content"] = json_decode($work["content"], true);



                    $rows .= sprintf('<tr><td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 130pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 60pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td></tr>', 
                                      $work["content"]["fromdate"], $work["content"]["todate"], 
                                      $work["content"]["organization"], $work["content"]["employment"],
                                      $work["content"]["position"], nl2br($work["content"]["duties"])
                                    );

                }       
            } else {
                $rows = sprintf('<tr><td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 130pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 60pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td></tr>', 
                                      "N/A", "N/A", "N/A", "N/A", "N/A", "N/A"
                                    );
            }
            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);
            

            // Internship Experience
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                    <tr><th colspan="6" bgcolor="#CCCCCC" align="center"><h3>Internship Experience</h3></th></tr>
                    <tr><th style="width: 40px; padding: 8px;">From<br>(MM/YY):</th>
                        <th style="width: 40px; padding: 8px;">To<br>(MM/YY):</th>
                        <th style="width: 150px; padding: 8px;">Name of Organization</th>
                        <th style="width: 60px; padding: 8px;">Type of Employment</th>
                        <th style="width: 120px; padding: 8px;">Position </th>
                        <th style="width: 120px; padding: 8px;">Major Duties</th></tr>%s            
                    </table><p></p>';


            if (count($data["experience"]["internship"]) > 0) {
                $rows = "";
                foreach ($data["experience"]["internship"] as $work) {
                    $work["content"] = json_decode($work["content"], true);

                    $rows .= sprintf('<tr><td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 130pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 60pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td></tr>', 
                                      $work["content"]["fromdate"], $work["content"]["todate"], 
                                      $work["content"]["organization"], $work["content"]["employment"],
                                      $work["content"]["position"], nl2br($work["content"]["duties"])
                                    );
                }       
            } else {
                 $rows = sprintf('<tr><td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 130pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 60pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 105pt; ">%s</div></td></tr>', 
                                       "N/A", "N/A", "N/A", "N/A", "N/A", "N/A"
                                    );
            }    
            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);
            

            // Extra-Curricular Activit(ies)
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                    <tr><th colspan="5" bgcolor="#CCCCCC" align="center"><h3>Extra-Curricular Activit(ies)</h3></th></tr>
                    <tr><th style="width: 40pt; padding: 8pt;">From<br>(MM/YY):</th>
                        <th style="width: 40pt; padding: 8pt;">To<br>(MM/YY):</th>
                        <th style="width: 100pt; padding: 8pt;">Name of Organization</th>
                        <th style="width: 80pt; padding: 8pt;">Position </th>
                        <th style="width: 130pt; padding: 8pt;">Major Duties</th></tr>%s            
                    </table><p></p>';

            if (count($data["achievement"]["exactivity"]) > 0) {
                $rows = "";
                foreach ($data["achievement"]["exactivity"] as $work) {
                    $work["content"] = json_decode($work["content"], true);

                    $rows .= sprintf('<tr><td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 120pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 100pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 150pt; ">%s</div></td></tr>', 
                                      $work["content"]["fromdate"], $work["content"]["todate"], 
                                      $work["content"]["organization"], $work["content"]["position"], nl2br($work["content"]["duties"])
                                    );
                }
            } else {
                $rows = sprintf('<tr><td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 40pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 120pt; ">%s</div></td>'.
                                         '<td style="padding: 8px;" valign="top"><div style="width: 100pt; ">%s</div></td>
                                          <td style="padding: 8px;" valign="top"><div style="width: 150pt; ">%s</div></td></tr>', 
                                     "N/A", "N/A", "N/A", "N/A", "N/A"
                                );
            }
            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);
            
            

            // Professional Qualification(s)
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                    <tr><th colspan="4" bgcolor="#CCCCCC" align="center"><h3>Professional Qualification(s)</h3></th></tr>
                    <tr><th style="width: 80px; padding: 8px;">Date Issued<br>(MM/YY):</th>
                        <th style="width: 195px; padding: 8px;">Examination/ Certification</th>
                        <th style="width: 190px; padding: 8px;">Name of Issuing Organization</th>
                        <th style="width: 140px; padding: 8px;">Grade/ Level obtained</th></tr>%s           
                    </table><p></p>';

            if (count($data["qualification"]["professional"]) > 0) {
                $rows = "";
                foreach ($data["qualification"]["professional"] as $work) {
                    $work["content"] = json_decode($work["content"], true);

                    $rows .= sprintf('<tr><td style="padding: 8px;" valign="top">%s</td>'.
                                        '<td style="padding: 8px;" valign="top">%s</td>'.
                                        '<td style="padding: 8px;" valign="top">%s</td>'.
                                        '<td style="padding: 8px;" valign="top">%s</td></tr>', 
                                      $work["content"]["date"],  $work["content"]["name"], $work["content"]["organization"], nl2br($work["content"]["grade"])
                                    );
                }                
            } else {
                $rows = sprintf('<tr><td style="padding: 8px;" valign="top">%s</td>'.
                                    '<td style="padding: 8px;" valign="top">%s</td>'.
                                    '<td style="padding: 8px;" valign="top">%s</td>'.
                                    '<td style="padding: 8px;" valign="top">%s</td></tr>', 
                                 "N/A", "N/A", "N/A", "N/A"
                            );
            }
            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);

            // Other Qualification(s)
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                    <tr><th colspan="4" bgcolor="#CCCCCC" align="center"><h3>Other Qualification(s)</h3></th></tr>
                    <tr><th style="width: 80px; padding: 8px;">Date Issued<br>(MM/YY):</th>
                        <th style="width: 195px; padding: 8px;">Examination/ Certification</th>
                        <th style="width: 190px; padding: 8px;">Name of Issuing Organization</th>
                        <th style="width: 140px; padding: 8px;">Grade/ Level obtained</th></tr>%s           
                    </table><p></p>';

            if (count($data["qualification"]["other"]) > 0) {
                $rows = "";
                foreach ($data["qualification"]["other"] as $work) {
                    $work["content"] = json_decode($work["content"], true);

                    $rows .= sprintf('<tr><td style="padding: 8px;" valign="top">%s</td><td style="padding: 8px;" valign="top">%s</td>
                                          <td style="padding: 8px;" valign="top">%s</td><td style="padding: 8px;" valign="top">%s</td></tr>', 
                                      $work["content"]["date"],  $work["content"]["name"], $work["content"]["organization"], nl2br($work["content"]["grade"])
                                    );
                }
            } else {
                $rows = sprintf('<tr><td style="padding: 8px;" valign="top">%s</td><td style="padding: 8px;" valign="top">%s</td>
                                    <td style="padding: 8px;" valign="top">%s</td><td style="padding: 8px;" valign="top">%s</td></tr>', 
                                    "N/A", "N/A", "N/A", "N/A"
                                );
            }   
            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);
            
            // Achievement(s)
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                        <tr><th colspan="3" bgcolor="#CCCCCC" align="center"><h3>Achievement(s)</h3></th></tr>
                        <tr><th style="width: 80px; padding: 8px; vertical-align: top">Date Issued:</th>
                            <th style="width: 420px; padding: 8px; vertical-align: top">Name of Issuing Organization</th>
                            <th style="width: 140px; padding: 8px; vertical-align: top">Grade/ Level obtained</th></tr>%s
                    </table><p></p>';


            if (count($data["achievement"]["achievement"]) > 0) {
                $rows = "";
                foreach ($data["achievement"]["achievement"] as $work) {
                    $work["content"] = json_decode($work["content"], true);
                    $rows .= sprintf('<tr><td style="padding: 8px;" valign="top">%s</td><td style="padding: 8px;" valign="top">%s</td>'.
                                         '<td style="padding: 8px;" valign="top">%s</td></tr>',
                                     $work["content"]["date"], $work["content"]["organization"], $work["content"]["name"]);
                }
                
            } else {
                 $rows = sprintf('<tr><td style="padding: 8px;" valign="top">%s</td><td style="padding: 8px;" valign="top">%s</td>'.
                                         '<td style="padding: 8px;" valign="top">%s</td></tr>',
                                     "N/A","N/A","N/A");
            }
            $this->writeHTML(sprintf($html, $rows), $html2pdf, $test2pdf, $pageHeight);

            // Skills 
            $html = '<table border="1" cellspacing="0" cellpadding="3px" align="center">
                        <tr><th style=" padding: 0px 8px; background-color:#CCCCCC; width: 640px; vertical-align: top; text-align: center" colspan="6"><h3>Skill(s)</h3></th></tr>
                        %s %s %s
                    </table><p></p>';

            $skill = json_decode($data["achievement"]["skill"][0]["content"], true);
            
            $rows = "";
            $tech = "";
            $i = 0;
            foreach ($skill["language"] as $language=>$kkk) {
                $lll = '';
                foreach ($kkk as $key => $value) {
                    $lll .= '<td style=" padding: 8px; vertical-align: top"><STRONG>'.$key .':</STRONG></td>'.
                            '<td style=" padding: 8px; vertical-align: top">'.$value .'</td>';
                }

                $rows .= '<tr><td style="width: 80px; padding: 8px; vertical-align: top">'.ltrim($language, "_").'</td>'.
                        '<td valign="top" colspan="5" style="width: 600px;  vertical-align: top "><table border="0" align="left"><tr>' . $lll .
                        '</tr></table></td></tr>';      
            }

            $languagehtml = '<tr><td colspan="6" style=" padding: 8px;"><strong>Language Proficiency</strong></td></tr> '.$rows;

            
            $count = count($skill["computer"]);
            foreach ($skill["computer"] as $language=>$kkk) {
                $tech .= '<td style=" padding: 8px;width:80px"><strong>'.ltrim($language, "_").'</strong></td>'.

                $colspan = "";
                if ($count - 1 == $i) {
                    $colspan = 'colspan="'. (5 - (($i % 3) * 2)) .'"';
                } 
                $tech .= sprintf('<td style=" padding: 8px;width:96px" %s>%s</td>', $colspan, $kkk);

                if ($i++ % 3 == 2) {
                    $tech .= '</tr><tr>';
                }
            }

            $technicalhtml = '<tr><td colspan="6" style=" padding: 8px;"><strong>Technical Skills</strong></td></tr><tr>'.$tech.'</tr>';
            
            $other_skill = "";

            if (count($data["achievement"]["skill_other"]) > 0) {
                $other_skill = '<tr><td colspan="6" style=" padding: 8px;"><strong>Other Skills</strong></td></tr>';

                foreach ($data["achievement"]["skill_other"] as $ooo) {
                    $ooo = json_decode($ooo["content"], true);
                    $other_skill .= '<tr><td style=" padding: 8px;"><strong>'.$ooo["skill"].'</strong></td>'.
                                 '<td colspan="5" valign="top" style=" padding: 8px;">'.html_entity_decode($ooo["description"], ENT_QUOTES, "UTF-8").'</td></tr>';
                }
            }

            $this->writeHTML(sprintf($html, $languagehtml, $technicalhtml, $other_skill), $html2pdf, $test2pdf, $pageHeight);


            $emergency = '<table border="1" cellspacing="0" cellpadding="3px" align="center">'.
                         '<tr><th colspan="3" bgcolor="#CCCCCC" align="center"><h3>Emergency Contact</h3></th></tr>'.
                         '<tr><th style="width:210px; padding: 8px;">Name:</th><th style="width:210px; padding: 8px;">Contact: </th><th style="width:215px; padding: 8px;">Relationship:</th></tr>'.
                         '<tr><td style=" padding: 8px;">%s</td><td style=" padding: 8px;">%s </td><td style=" padding: 8px;">%s</td></tr>'.
                         '</table>';


            $this->writeHTML(sprintf($emergency, $data["personal"]["em_name"] . " " . $data["personal"]["em_name2"], $data["personal"]["em_contact"], $data["personal"]["em_relation"]), $html2pdf, $test2pdf, $pageHeight);

            
            $test2pdf = false;
            $filename = sha1(base64_encode(self::SESSION_KEY).$application["id"]) . ".pdf";

            $path = Student::getFilePath($application["member_id"]);
            
            $html2pdf->output($path["absolute_path"]."/".$filename, "F");
            $html2pdf->clean();

            $html2pdf = false;

            return $path["absolute_path"]."/".$filename;
        }

        private function writeHTML($html, $html2pdf, $test2pdf, &$pageHeight) {

            $test2pdf->newPage();
            $height = $test2pdf->pdf->getY();
            $test2pdf->writeHTML($html);
            $height = $test2pdf->pdf->getY() - $height;

            if ($pageHeight + $height > 285) {
                $html2pdf->newPage();
                $pageHeight = 0;
            }
            $pageHeight += $height;
            
            $html = preg_replace("/(\p{Han})/u", '<span style="font-family: cid0ct">\1</span>', $html);

            $html2pdf->writeHTML($html);
            $test2pdf->clean();
        }

        public static function downloadZip($ids, $password){
            
            global $rootPath, $appPath, $lecturer;

            /*
            $zip = new ZipArchive();

            list($usec, $sec) = explode(' ', microtime());
            
            srand((float)$sec + ((float) $usec * 100000));
            $randval1 = rand(0, 9898989) . rand(0, 9898989);
            $fname = "../tmp/".$randval1.".zip";

            if ($zip->open($fname, ZipArchive::CREATE) !== TRUE) {
                exit("cannot open <$fname>, please retry later.\n");
            }
            */
            $path = rtrim($rootPath, "/") . $appPath . "/application_form";

            @mkdir($path, 0700);
            // clear all file
            array_map('unlink', glob($path."/*", GLOB_MARK));
            $session = date("YmdHis");
            
            $files = array();
            $fname = $path."/appform-".$session.".zip";

            foreach ($ids as $id) {
                if ($id != "") {
                    $app = new Application($id);
                    $fff = $app->generateApplicationPDF($lecturer);
                    copy($fff, $path."/".$app->getApplicationNumber() . ".pdf");
                    $files[] = $path."/".$app->getApplicationNumber() . ".pdf";
                    unlink($fff);                   
                }
            }
            
            $cmd = "7za a -P%s %s/appform-%s.zip %s/* ";
            $cmd = sprintf($cmd, $password, $path, $session, "../application_form");
            exec($cmd);
            
            foreach($files as $f) {
                @unlink($f);
            }

            return $fname;
        }


        public function reorder($itemid, $priority){
            $data = $this->getOne($this->itemTable, ["where"=>["id = '$itemid'"]]);

            if ($data["application_id"] != $this->id) {
                return array("status"=>0, "msg"=>"Error occured");
            }

            $this->conn->Execute("UPDATE ".$this->itemTable." SET priority = '".$data["priority"]."' where application_id = '".$this->id."' AND priority = '".$priority."'");

            $this->conn->Execute("UPDATE ".$this->itemTable." SET priority = '".$priority."' where id = '".$itemid."'");

            return array("status"=>1, "msg"=>"OK");
        }

        public function saveCVForm($data){

            $data["basic"]["gpa"] = json_encode($data["basic"]["gpa"], JSON_UNESCAPED_UNICODE);
            $data["basic"]["cgpa"] = json_encode($data["basic"]["cgpa"], JSON_UNESCAPED_UNICODE);


            $this->conn->AutoExecute($this->tableName, $data["basic"], "UPDATE", "id='".$this->id."'");

            $this->conn->AutoExecute($this->tableName, $data["interview"], "UPDATE", "id='".$this->id."'");

        }

                
    }
?>