<?php
    require_once($adoDBPath.'/adodb.inc.php'); # load code common to ADOdb   
    require_once($pathClassLib.'/function.common.php');
    require_once(__DIR__.'/cms.php');


    class NqFileObj extends CMS {
    	private $member_id;
    	private $key;
    	private $isLecturer = false;
        public $tableName = "file_obj";  

        function __construct($member_id=false) {
        	parent::__construct();

        	if ($member_id === true) {
        		$this->isLecturer = true;
        	} else if ($member_id) {
	        	$this->member_id = $member_id;
	        	$this->key = md5("stud".$member_id."ent");
	        	$this->cipher_name = "AES-128-CBC";
        	} 
        }

        public function upload($fileobj) {

        	$type = pathinfo($fileobj["tmp_name"]);
			$data = file_get_contents($fileobj["tmp_name"]);
			$plaintext = base64_encode($data);
			
			$ivlen = openssl_cipher_iv_length($this->cipher_name);
			$iv = openssl_random_pseudo_bytes($ivlen);


			$ciphertext_raw = openssl_encrypt($plaintext, $this->cipher_name, $this->key, $options=OPENSSL_RAW_DATA, $iv);
			$hmac = hash_hmac('sha256', $ciphertext_raw, $this->key, $as_binary=true);
			$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

			$data = array();
			$data["member_id"] = $this->member_id;
			$data["name"] = $fileobj["name"];
			$data["mime"] = $fileobj["type"];
			$data["data"] = $ciphertext;
			$data["status"] = "E";
			$data["createDate"] = $data["lastModDate"] = date("Y-m-d H:i:s");

			$this->conn->AutoExecute("file_obj", $data, "INSERT"); 

			return $this->conn->insert_Id();
        }

        public function readfile($filename, $filetype="") {
        	$type = pathinfo($filename);
			$data = file_get_contents($filename);
			$plaintext = base64_encode($data);
			
			$ivlen = openssl_cipher_iv_length($this->cipher_name);
			$iv = openssl_random_pseudo_bytes($ivlen);


			$ciphertext_raw = openssl_encrypt($plaintext, $this->cipher_name, $this->key, $options=OPENSSL_RAW_DATA, $iv);
			$hmac = hash_hmac('sha256', $ciphertext_raw, $this->key, $as_binary=true);
			$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

			$data = array();
			$data["member_id"] = $this->member_id;
			$data["name"] = $type["basename"];
			$data["mime"] = $this->mime_type($type["basename"]);
			$data["data"] = $ciphertext;
			$data["status"] = "E";
			$data["createDate"] = $data["lastModDate"] = date("Y-m-d H:i:s");

			$this->conn->AutoExecute("file_obj", $data, "INSERT"); 

			return $this->conn->insert_Id();
        }

        private function mime_type($base_name) {
        	$extension = explode(".", $base_name);
        	$extension = $extension[count($extension) - 1];

        	switch ($extension) {
        		case 'docx':
        			return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        		case 'xls':
        			return "application/vnd.ms-excel";
        		case 'xlsx':
        			return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        		case 'jpg':
        			return "image/jpeg";
    			case 'zip':
        			return "application/zip";
        		case 'pdf':
        			return "application/pdf";
        		default:
        			return mime_content_type($base_name);
        	}
        }


        public function download($id) {

        	$file = $this->conn->GetRow("Select member_id, name, mime, data from file_obj where id = '". ((int)$id) ."'");

        	if ($this->member_id) {
        		if ($file["member_id"] != $this->member_id) {
	        		header('HTTP/1.0 403 Forbidden');
					exit();
				}
        	} else if ($this->isLecturer){
        		$this->member_id = $file["member_id"];
        		$this->key = md5("stud".$this->member_id."ent");
	        	$this->cipher_name = "AES-128-CBC";
        	} else {
        		header('HTTP/1.0 403 Forbidden');
				exit();
        	}

        	$c = base64_decode($file["data"]);
			$ivlen = openssl_cipher_iv_length($this->cipher_name);	
			$iv = substr($c, 0, $ivlen);
			$hmac = substr($c, $ivlen, $sha2len=32);


			$ciphertext_raw = substr($c, $ivlen+$sha2len);
			$original_plaintext = openssl_decrypt($ciphertext_raw, $this->cipher_name, $this->key, $options=OPENSSL_RAW_DATA, $iv);
			$calcmac = hash_hmac('sha256', $ciphertext_raw, $this->key, $as_binary=true);

			if (hash_equals($hmac, $calcmac))	
			{
				header("Content-type:".$file["mime"]);
				header("Content-Disposition:inline;filename=".$file["name"]);
				header("Content-length: ".strlen(base64_decode($original_plaintext)));
			    echo base64_decode($original_plaintext)."\n";
			} else {
				header('HTTP/1.0 404 Not Found');
				exit();
			}
        }


        public function generatefile($id, $extension="") {
        	global $appPath;

        	$file = $this->conn->GetRow("Select member_id, name, mime, data from file_obj where id = '". ((int)$id) ."'");


        	if ($file["member_id"] != $this->member_id) {
        		header('HTTP/1.0 403 Forbidden');
        		exit();
        	}

        	$c = base64_decode($file["data"]);
			$ivlen = openssl_cipher_iv_length($this->cipher_name);	
			$iv = substr($c, 0, $ivlen);
			$hmac = substr($c, $ivlen, $sha2len=32);


			$ciphertext_raw = substr($c, $ivlen+$sha2len);
			$original_plaintext = openssl_decrypt($ciphertext_raw, $this->cipher_name, $this->key, $options=OPENSSL_RAW_DATA, $iv);
			$calcmac = hash_hmac('sha256', $ciphertext_raw, $this->key, $as_binary=true);

			if (hash_equals($hmac, $calcmac))	
			{        	
				$filename = "tmp/".microtime(1).$extension;

				$filename_handle = $_SERVER['DOCUMENT_ROOT'].$appPath."/".$filename;

				file_put_contents($filename_handle, base64_decode($original_plaintext));
			}
			return $filename;
        }
    }

?>