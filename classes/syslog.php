<?php    
    include_once($rootPath.$appPath."/classes/cms.php");
    require_once($pathClassLib."/class.uploadfile.php");
    require_once($pathClassLib."/function.common.php");
    require_once($pathClassLib."/class.file.php");


    class Syslog extends CMS{        
        public $pageName = "System Log"; 
        protected $tableName = "syslog";
        protected $page = 1;


        function __construct(){

            parent::__construct();

            $this->memberType = array("student"=>1, "lecturer"=>2);

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            //$this->__cms_fields[] = new CategoryObj("node_id", ["label"=>"Category", "tableName"=>"plan", "list"=>["show", "sort", "search"], "width"=>"150"]);   
                
        
             $this->__cms_fields[] = new ForeignObj("member_id", ["label"=>"Student", 
                                                                            "options"=>["ft"=>[["table"=>"student", "value"=>"id", "label"=>"student_id"]]],]);
            
             $this->__cms_fields[] = new ForeignObj("member_id", ["label"=>"Lecturer", 
                                                                            "options"=>["ft"=>[["table"=>"lecturer", "value"=>"id", "label"=>"username"]]],]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "action", ["label"=>"Action",  "width"=>100, ]);
            $this->__cms_fields[] = new CMSObj("TEXTAREA", "para", ["label"=>"Parameter", "list"=>[], "width"=>100, ]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "result", ["label"=>"Result", "width"=>100, ]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "ip", ["label"=>"IP Address", "width"=>150, ]);

                
            //$this->__cms_fields[] = new CMSObj("TEXT", "content", ["label"=>"Content", "list"=>["search"], "lang"=>["en", "zh", "cn"]]);
            //$this->__cms_fields[] = new CMSObj("TEXTAREA", "youtube", ["label"=>"Video", "list"=>[], "width"=>350, "prefix"=>"http://",  "lang"=>["en", "zh", "cn"]]);

            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);
        }

        
        public function log($action, $type, $data, $result="") {
            $data["action"] = $action;
            $data["type"] = $this->memberType[$type];

            $data["member_id"] = array_key_exists("member_id", $data) ? $data["member_id"] : "";
            $data["ip"] = (array_key_exists("HTTP_X_FORWARDED_FOR", $_SERVER)) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER['REMOTE_ADDR'];
            $data["para"] = array_key_exists("para", $data) ? $data["para"] : "";
            $data["result"] = $result;
            $data["createDate"] = date("Y-m-d H:i:s");

            $this->conn->AutoExecute($this->tableName, $data, "INSERT");
            
            return $this->conn->insert_Id();
        }

        public function exportXls ($memberType) {
            global $pathClassLib;
            require_once(__DIR__."/../lib_common/phpexcel/PHPExcel.php");
            

            $thisRow = 1; $thisCol = 0;
            $objPhpExcel = new PHPExcel();

            // Records
            $_cms_fields = $this->__cms_fields;
            if ($memberType == 1) {
                unset($_cms_fields[2]);
            } else {
                unset($_cms_fields[1]);
            }

            $worksheet = $objPhpExcel->setActiveSheetIndex(0);
           
            foreach ($_cms_fields as $field) {
                if ($field->type != "CUSTOM" && $field->fieldName != "id") {
                    if ($field->fieldName != 'node_id' || $this->level > 1) {
                        $label = $field->label;

                        if (in_array($field->type, ["RADIO", "SELECT", "CHECKBOX"])) {
                            $options = array_map("strip_tags", array_keys($field->options));

                            $label = $field->label ." (" . implode(" / ", $options).")";
                        } 

                        $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  $label);
                    }
                }
            }

            //$records = $this->conn->GetAll($_POST["thissql"]);
            
            $rs = $this->conn->Execute($_POST["thissql"]);

            while (!$rs->EOF) { 
            //foreach ($records as $row) {
                $thisRow++;
                $thisCol = 0;
                foreach ($_cms_fields as $field) {
                    if ($field->type != "CUSTOM" && $field->fieldName != "id") {
                        if ($field->fieldName != 'node_id' || $this->level > 1) {
                            
                            if (in_array($field->type, ["RADIO", "SELECT", "CHECKBOX"])) {
                                $label = $field->label ." (" . implode(" / ", $options).")";
                                $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  
                                     strip_tags(array_search($rs->fields[$field->fieldName], $field->options))
                                );
                            } else if (get_class($field) == "ForeignObj") {
                                if ($memberType == 1) { // student
                                    $valuex = $this->conn->GetOne("Select student_id from student where id = '".$rs->fields[$field->fieldName] ."'");
                                } else {
                                    $valuex = $this->conn->GetOne("Select username from lecturer where id = '".$rs->fields[$field->fieldName] ."'");
                                }
                                $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  $valuex);
                            } else {
                                $worksheet->setCellValueByColumnAndRow($thisCol++, $thisRow,  $rs->fields[$field->fieldName]);
                            }
                           
                        }
                    }
                }
                $rs->MoveNext();

            }
            //exit();



            return $objPhpExcel;
        }
    
    }

?>