<?php    
    include_once(__DIR__."/cms.php");
    include_once(__DIR__."/student.php");
    include_once(__DIR__."/application.php");
    require_once(__DIR__."/jpdf.php");
    require_once(__DIR__."/syslog.php");
    require_once(__DIR__."/nqfileobj.php");

    include_once("lib_common/phpdocx/classes/CreateDocx.php");
  

    class CVForm extends CMS{        
        public $pageName = "personal_cv"; 
        protected $tableName = "personal_cv";
        protected $page = 1;
        public $id;


        function __construct($id=false){

            parent::__construct();

            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            //$this->__cms_fields[] = new CategoryObj("node_id", ["label"=>"Category", "tableName"=>"plan", "list"=>["show", "sort", "search"], "width"=>"150"]);   
                
            $this->__cms_fields[] = new CMSObj("VARCHAR", "name", ["label"=>"Name",  "width"=>350, ]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "points", ["label"=>"Required Points", "list"=>[], "width"=>0, ]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "bus_vol", ["label"=>"Required BV", "list"=>[], "width"=>0, ]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "rate", ["label"=>"Total Direct Sale(%)", "list"=>[], "width"=>0, ]);
            $this->__cms_fields[] = new CMSObj("TEXTAREA", "remark", ["label"=>"Remark", "list"=>[], "width"=>0, ]);

            $json_fields = array();
            $json_fields["grade"] = new CategoryObj("node_id", ["label"=>"Member Grade","tableName"=>"member", "options"=>["field_name"=>"level_calc[grade][]", "select2"=>false]]); 
            $json_fields["referral"] = new CMSObj("VARCHAR", "referral", ["label"=>"Referral Count", "options"=>["field_name"=>"level_calc[referral][]"]]);
            $json_fields["level"] = new CMSObj("VARCHAR", "level", ["label"=>"Level", "options"=>["field_name"=>"level_calc[level][]"]]); 
            $json_fields["rate"] = new CMSObj("VARCHAR", "rate", ["label"=>" % ", "options"=>["field_name"=>"level_calc[rate][]"]]);

            $this->__cms_fields[] = new CustomObj("JSON", "level_calc", ["label"=>"Level Calculation", "list"=>[], "width"=>0,"options"=>["fields"=>$json_fields]]);
            
            //$this->__cms_fields[] = new CMSObj("TEXT", "content", ["label"=>"Content", "list"=>["search"], "lang"=>["en", "zh", "cn"]]);
            //$this->__cms_fields[] = new CMSObj("TEXTAREA", "youtube", ["label"=>"Video", "list"=>[], "width"=>350, "prefix"=>"http://",  "lang"=>["en", "zh", "cn"]]);

            $this->__cms_fields[] = new SelectObj("CHECKBOX", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"]]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"]]);
            
            //$this->__cms_fields[] = new PermissionObj("lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["show","insert", "update"]]);

            if ($id) {
                $this->id = $id;
            }
        }

        public function getDetail() {
            $data = parent::getDetail($this->tableName, $this->id);
            $data["language"] = explode(",", $data["language"]);
            $data["computer"] = explode(",", $data["computer"]);
            $data["basicinfo"] = explode(",", $data["basic"]);

            $mywhere = array();
            $mywhere["where"][] = "meta_type <> 'skill'";
            $mywhere["where"][] = "id in (".trim($data["meta"], ",").")";


            $metalist = $this->get("student_meta", $mywhere);
                        
            foreach ($metalist as $meta) {
                $data["metalist"][$meta["meta_type"]][] = json_decode($meta["content"], true);
            }
            $skillmeta = $this->get("student_meta", array("where" =>array("meta_type = 'skill'", "application_id = '".$data["application_id"]."'")));
            
            foreach ($skillmeta as $ski) {
               $data["metalist"]["skill"][] = json_decode($ski["content"], true);
            }
            
            // sort the meta
            foreach ($data["metalist"] as $metatype=>&$meta) {
                $tmp = array();

                $meta2sort = ["education", "exam_result", "public_exam", "prof_qualification", "oth_qualification",                              
                              "work_experience", "internship_experience", "exactivity", "achievement"];

                if (in_array($metatype, $meta2sort)) {
                    foreach ($meta as $row) {
                    
                        switch ($metatype) {
                            case "exam_result": 
                                $index = strtotime($row["year"]."-01-01"); break; 

                            case "public_exam": 
                            case "prof_qualification": 
                            case "oth_qualification":
                            case "achievement": 
                                $index = strtotime($row["date"]); break; 
                            
                            case "education": 
                            case "work_experience": 
                            case "internship_experience":
                            case "exactivity": 
                                $index = strtotime($row["fromdate"]); break; 
                        }
                        while (array_key_exists($index, $tmp)) {
                            $index++;
                        } 
                        $tmp[$index] = $row;
                    }
                    krsort($tmp);
                    $meta = array_values($tmp);                    
                }
                
            }

            return $data;
        }

        public function getCVByHash($hash, $student){
            $cv = $this->getOne("personal_cv", array("where"=>["sha1(concat('app-', id)) = ?", "student_id = ?"], "where_parameters"=>[base64_decode($hash), $student->getId()]));
            
            if ($cv) {

                $this->id = $cv["id"];

                return $this;
            } else { 
                return false;
            }
        }

        public function getObjByHashId($hash, $student) {
            $cv = $this->getOne("personal_cv", array("where"=>["sha1(concat('cv-', id)) = ?", "student_id = ?"], "where_parameters"=>[base64_decode($hash), $student->getId()]));
            
            if ($cv) {
                $this->id = $cv["id"];
                return $this;
            } else { 
                return false;
            }
        }

        public function getEmail() {
        	$cvformObj = $this->getDetail();

        	$application = new Application($cvformObj["application_id"]);

        	$email = $application->getDetailByArray(["fields"=>[
        		"ENAME"=>"CONCAT(firstname_en, ' ' , lastname_en)",
        		"EMAIL"=>"email",
        	]]);

        	return array( $email["ENAME"], $email["EMAIL"]);
        }

        public function genpdf($template, $tno, $action) {
            global $rootPath, $appPath;
            $PDF_HEIGHT = 285;

            $cvformObj = $this->getDetail();
            
            $student = new Student($cvformObj["student_id"]);
            $application = new Application($cvformObj["application_id"]);




            //$application = $application->getDetail();
            
            $basic = array();
            $basic["fields"] = array(
                "ENAME" => "CONCAT(firstname_en, ' ' , lastname_en)",
                "CNAME" => "CONCAT(lastname_zh,  firstname_zh)",
                "ADDRESS1" => "address1",
                "ADDRESS2" => "address2",
                "ADDRESS3" => "address3",
                "CONTACT" => "contact",
                "EMAIL" => "email",
                "GENDER" => "gender",
                "AGE" => "age",
                "DOB" => "dob",
                "CGPA" => "cgpa",
                "GPA" => "gpa",
                "RELIGION" => "religion",
                "AVAILABILITY" => "availability",
                "EXPECTED_SALARY" => "expected_salary",
            );
            $studentDetails = ($student->getDetail());
            
            // BASIC
            $template["BASIC"]["html"] = $template["BASIC"]["TEMPLATE"];
            
            $rowsPart = array(
                "EDUCATION"=>"education",
                "RESULT"=>"exam_result",
                "PRESULT"=>"public_exam",
                "WORK"=>"work_experience",
                "INTERNSHIP"=>"internship_experience",
                "EXACTIVITIES"=>"exactivity",
                "PROFESSIONAL"=>"prof_qualification",
                "OTHER_QUALI"=>"oth_qualification",
                "ACHIEVEMENTS"=>"achievement",
                "SKILLS"=>"other_skill",
                "REFERENCE"=>"reference",
                "OTHERS"=>"other",
                "ETC"=>"etc"
            );

            foreach ($rowsPart as $part=>$mtype) {
                if (count($cvformObj["metalist"][$mtype]) > 0) {

                    $rows = $this->renderRows($part, $template[$part]["ROW"], $cvformObj["metalist"][$mtype]);
                    $thtml = explode("{".$part."-ROW}", $template[$part]["TEMPLATE"], 3);
                    $template[$part]["html"] = $thtml[0] . $rows . $thtml[2];
                } else {
                    if ($part == "SKILLS") {
                        $thtml = explode("{".$part."-ROW}", $template[$part]["TEMPLATE"], 3);
                        $template[$part]["html"] = $thtml[0] . $thtml[2];
                    } else if ($part == "ETC") {
                        $template[$part]["html"] = $template[$part]["TEMPLATE"];
                    } else {
                        $template[$part]["html"] = "";
                    }
                } 
            }
            $basicArray = $application->getDetailByArray($basic);
            $basicArray["ADDRESS"] = $basicArray["ADDRESS1"];
            $basicArray["ADDRESS"] .=  (trim($basicArray["ADDRESS2"]) != "") ? "<BR>".$basicArray["ADDRESS2"] : "";
            $basicArray["ADDRESS"] .=  (trim($basicArray["ADDRESS3"]) != "") ? "<BR>".$basicArray["ADDRESS3"] : "";

            
            unset($basicArray["ADDRESS1"], $basicArray["ADDRESS2"], $basicArray["ADDRESS3"]);

            foreach ($basicArray as $idx=>$value) {

                $addfields =  explode(",", strtoupper($cvformObj["basic"]));
                
                foreach ($template as $iidx=>&$hhtml) {
                    if (in_array($idx, $addfields)){
                        if ($idx == "CGPA" || $idx == "GPA") {
                            $mahtml = explode("<!-- [$idx] -->", $hhtml["html"]);
                            if (count($mahtml)> 1) {
                                $value = json_decode($value, true);
                                foreach ($value as $ii=>&$vv) {
                                    if (strtoupper($ii) == "SCHOOL" && in_array($vv, array("HKCC", "SPEED"))) {
                                        $vv = ($vv == "HKCC") ? "Hong Kong Community College" : "SPEED, The Hong Kong Polytechnic University";
                                    }
                                    $mahtml[1] = str_replace("[".strtoupper($ii)."]", $vv, $mahtml[1]);     
                                }                            
                                $hhtml["html"] = implode("", $mahtml);
                            }
                        } else  {
                            $hhtml["html"] = str_replace("[$idx]", wordwrap($value, 35, "<br />"), $hhtml["html"]);       
                        }

                    } else {
                        $mahtml = explode("<!-- [$idx] -->", $hhtml["html"]);
                        if (count($mahtml) == 3){
                            $hhtml["html"] = $mahtml[0] . $mahtml[2];
                        } else {
                            $hhtml["html"] = implode("", $mahtml);
                        }

                    } 
                }
            }



            if (implode("", $cvformObj["language"]) != "") {
                $template["SKILLS"]["token"] = explode("{LANGUAGE-ROW}", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["LANGUAGE-ROW"] = $template["SKILLS"]["token"][1];
                $rows = "";

                foreach ($cvformObj["metalist"]["skill"] as $skillMeta){

                    foreach ($skillMeta["language"] as $key => $value) {

                        if (in_array($key, $cvformObj["language"])){
                            $langRow = $template["SKILLS"]["LANGUAGE-ROW"];
                            

                            $rr = array();
                            foreach ($value as $idx=>$result){
                                $rr[] = "$idx : $result";
                            }

                            $langRow = str_replace("[LANGUAGE]", ltrim( $key, "_"), $langRow);
                            $langRow = str_replace("[RESULTS]", implode(", ",$rr), $langRow);
                            $rows .= $langRow;


                        }
                    }
                    $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . $rows . $template["SKILLS"]["token"][2];
                }
            } else {
                $template["SKILLS"]["token"] = explode("<!-- [LANGUAGE] -->", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . "" . $template["SKILLS"]["token"][2];
            }
            

            if (implode("", $cvformObj["computer"]) != "") {
                $template["SKILLS"]["token"] = explode("{COMPUTER-ROW}", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["COMPUTER-ROW"] = $template["SKILLS"]["token"][1];
                $rows = "";

                foreach ($cvformObj["metalist"]["skill"] as $skillMeta){
                    
                        foreach ($skillMeta["computer"] as $key => $value) {
                            if (in_array($key, $cvformObj["computer"])){
                                $langRow = $template["SKILLS"]["COMPUTER-ROW"];
                                $langRow = str_replace("[COMPUTER]", ltrim( $key, "_"), $langRow);
                                $langRow = str_replace("[RESULTS]", $value, $langRow);
                                $rows .= $langRow;
                            }
                        }
                        $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . $rows . $template["SKILLS"]["token"][2];
                   
                }
            } else {
                $template["SKILLS"]["token"] = explode("<!-- [COMPUTER] -->", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . "" . $template["SKILLS"]["token"][2];
            }
            
            $app = $application->getDetail();


            $path = (Student::getFilePath($student->getId()));

            if (!is_dir($path["absolute_path"])){
                mkdir($path["absolute_path"], 0744);
            }
            if ($action == "docx") {
                $docx = new CreateDocx();
                
                $docx->modifyPageLayout("A4", array("marginTop"=>567, "marginBottom"=>567, "marginLeft"=>567, "marginRight"=>567));

                $i = 1;
                $embedHtml = "";
                
                foreach ($template as $idx=>&$value) {
                    if ($app["photo"] != "" && in_array("PHOTO", $addfields)) {
                        $app["photo"] = end(explode("-", $app["photo"]));
                        $nqFileObj = new NqFileObj($student->getId());
                        $tmpfile = $nqFileObj->generatefile($app["photo"]);
                        $value["html"] = str_replace("[PHOTO]", "https://".$_SERVER["HTTP_HOST"].$appPath."/". $tmpfile, $value["html"]);
                    }
                    $embedHtml .= $value["html"];

                }

                $docx->embedHTML($embedHtml);

                $filename = time().".docx";

                $path = (Student::getFilePath($student->getId()));

                $docx->createDocx($path["absolute_path"]."/".$filename);

                $nqFileObj = new NqFileObj($student->getId());
                $file_objid = $nqFileObj->readfile($path["absolute_path"]."/".$filename);

                $student->getFileById($_GET["t"], $file_objid);
                exit();

                //unlink($path["absolute_path"]."/".$filename);

            } else {


                $html2pdf = new JayPDF('P', 'A4', 'en', true, 'UTF-8', array(8,8,8,8));

                //$html2pdf->setdefaultfont("Times");
                $html2pdf->addFont("cid0ct");
                $html2pdf->addFont("dejavusans");
                $html2pdf->writeHTML('<link type="text/css" href="css/template'.$tno.'.css" rel="stylesheet" />');


                $testpdf = new JayPDF('P', 'A4', 'en', true, 'UTF-8', array(8,8,8,8));
                $testpdf->addFont("cid0ct");
                $testpdf->addFont("dejavusans");
                $testpdf->writeHTML('<link type="text/css" href="css/template'.$tno.'.css" rel="stylesheet" />');

                //$html2pdf->writeHTML(preg_replace("/(\p{Han})/u", '<span style="font-family: cid0ct">\1</span>',$html));
                $pageHeight = 0;        
                
                // Split the skill table

                $tmpTable = explode("</table>", $template["SKILLS"]["html"]);

                $template["SKILLS"]["html"] = array();

                for ($ii = 0; $ii <= count($tmpTable) - 1; $ii) {
                    if ($tmpTable[$ii+1] ) {
                        $template["SKILLS"]["html"][] = $tmpTable[$ii++]."</table>";
                    } else {
                        $ii++;
                    }
                }
                $template["SKILLS"]["html"][] = $tmpTable[$ii];
       

                foreach ($template as $idx=>&$value) {


                    if ($idx == "SKILLS") {

                        foreach ($value["html"] as $srow) {
                        	$testpdf->newPage();
                            $height = $testpdf->pdf->getY();
                            $testpdf->writeHTML($srow);
                            $height = $testpdf->pdf->getY() - $height;

                            if ($pageHeight + $height > $PDF_HEIGHT) {
                                $html2pdf->newPage();
                                $pageHeight = 0;
                            }
                            $pageHeight += $height;
                        }
                        $value["html"] = implode("", $value["html"]);

                    } else {
                    
                        if (strpos($value["html"], "<!-- [PHOTO] -->") !== FALSE ) {    
                                                    
                            if ($app["photo"] != "" && in_array("PHOTO", $addfields)) {
                            	$app["photo"] = end(explode("-", $app["photo"]));
                            	$nqFileObj = new NqFileObj($student->getId());
                                $tmpfile = $nqFileObj->generatefile($app["photo"]);
                                $value["html"] = str_replace("[PHOTO]", "https://".$_SERVER["HTTP_HOST"].$appPath."/". $tmpfile, $value["html"]);
                            } else {
                                $value["html"] = explode("<!-- [PHOTO] -->", $value["html"], 3);
                                $value["html"] = $value["html"][0] . "" . $value["html"][2];
                            }
                        }


                        $testpdf->newPage();
                        $height = $testpdf->pdf->getY();
                        $testpdf->writeHTML($value["html"]);
                        $height = $testpdf->pdf->getY() - $height;
                        
                        if ($pageHeight + $height > $PDF_HEIGHT) {
                        	
                            $html2pdf->newPage();
                            $pageHeight = 0;
                        }
                        $pageHeight += $height;
                    }

                    $value["html"] = preg_replace("/(\p{Han})/u", '<span style="font-family: cid0ct">\1</span>', $value["html"] );
                   
                    $html2pdf->writeHTML($value["html"]);
                    
                }

                $filename = time().".pdf";
                $path = (Student::getFilePath($student->getId()));
                $html2pdf->output($path["absolute_path"]."/".$filename, $action);
                
                unlink($rootPath.$appPath."/".$tmpfile);

                $nqFileObj = new NqFileObj($student->getId());
                $file_objid = $nqFileObj->readfile($path["absolute_path"]."/".$filename);

                unlink($path["absolute_path"]."/".$filename);

                $data["file01"] = $file_objid;
            }

            $logger = new SysLog();
            $logger->log("cvform", "student", ["member_id"=> $student->getId(), "para"=>$cvformObj["id"]], "SUCCESS");
           
            $this->conn->AutoExecute($this->tableName, $data, "UPDATE", "id = '".$cvformObj["id"]."'");
            return "pfiles/cv-".$file_objid;
        }




        public function preview($template, $html){
            global $rootPath, $appPath;
            $cvformObj = $this->getDetail();
            
            $student = new Student($cvformObj["student_id"]);
            $application = new Application($cvformObj["application_id"]);

            //$application = $application->getDetail();
            
            $basic = array();
            $basic["fields"] = array(
                "ENAME" => "CONCAT(firstname_en, ' ' , lastname_en)",
                "CNAME" => "CONCAT(lastname_zh, firstname_zh)",
                "ADDRESS" => "CONCAT(address1, '<BR>' , address2, '<BR>' , address3)",
                "CONTACT" => "contact",
                "EMAIL" => "email",
                "GENDER" => "gender",
                "AGE" => "age",
                "DOB" => "dob",
                "RELIGION" => "religion",
                "GPA" => "gpa",
                "CGPA" => "cgpa",
                "AVAILABILITY" => "availability",
                "EXPECTED_SALARY" => "expected_salary"
            );

            // BASIC
            $template["BASIC"]["html"] = $template["BASIC"]["TEMPLATE"];
            
          
            $rowsPart = array(
                "EDUCATION"=>"education",
                "RESULT"=>"exam_result",
                "PRESULT"=>"public_exam",
                "WORK"=>"work_experience",
                "INTERNSHIP"=>"internship_experience",
                "EXACTIVITIES"=>"exactivity",
                "PROFESSIONAL"=>"prof_qualification",
                "OTHER_QUALI"=>"oth_qualification",
                "ACHIEVEMENTS"=>"achievement",
                "SKILLS"=>"other_skill",
                "REFERENCE"=>"reference",
                "OTHERS"=>"other",
                "ETC"=>"etc"
            );

            foreach ($rowsPart as $part=>$mtype) {

                $rows = "";
                if (count($cvformObj["metalist"][$mtype]) > 0) {

                    $rows = $this->renderRows($part, $template[$part]["ROW"], $cvformObj["metalist"][$mtype]);
                    $thtml = explode("{".$part."-ROW}", $template[$part]["TEMPLATE"], 3);
                    $template[$part]["html"] = $thtml[0] . $rows . $thtml[2];
                } else {
                    $template[$part]["html"] = "";

                    if ($part == "SKILLS") {
                        $thtml = explode("{".$part."-ROW}", $template[$part]["TEMPLATE"], 3);
                        $template[$part]["html"] = $thtml[0] . $rows . $thtml[2];
                    } elseif ($part == "ETC") {
                        $template[$part]["html"] = $template[$part]["TEMPLATE"];
                    }
                } 
            }
            

            foreach ($application->getDetailByArray($basic) as $idx=>&$value) {

                $addfields =  explode(",", strtoupper($cvformObj["basic"]));

                foreach ($template as $iidx=>&$hhtml) {
                    if (in_array($idx, $addfields)){                        
                        if ($idx == "CGPA" || $idx == "GPA") {
                            $mahtml = explode("<!-- [$idx] -->", $hhtml["html"]);
                            if (count($mahtml)> 1) {
                                $value = json_decode($value, true);
                                foreach ($value as $ii=>&$vv) {
                                    if (strtoupper($ii) == "SCHOOL" && in_array($vv, array("HKCC", "SPEED"))) {
                                        $vv = ($vv == "HKCC") ? "Hong Kong Community College" : "SPEED, The Hong Kong Polytechnic University";
                                    }
                                    $mahtml[1] = str_replace("[".strtoupper($ii)."]", $vv, $mahtml[1]);     
                                }                            
                                $hhtml["html"] = implode("", $mahtml);
                            }
                        } else  {
                            $hhtml["html"] = str_replace("[$idx]", $value, $hhtml["html"]);       
                        }
                                         
                    } else {

                        $mahtml = explode("<!-- [$idx] -->", $hhtml["html"]);
                        if (count($mahtml) == 3){
                            $hhtml["html"] = $mahtml[0] . $mahtml[2];
                        } else {
                            $hhtml["html"] = implode("", $mahtml);
                        }
                    } 
                }
            }

            if (implode("", $cvformObj["language"]) != "") {
                $template["SKILLS"]["token"] = explode("{LANGUAGE-ROW}", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["LANGUAGE-ROW"] = $template["SKILLS"]["token"][1];
                $rows = "";

                foreach ($cvformObj["metalist"]["skill"] as $skillMeta){
                    
                    foreach ($skillMeta["language"] as $key => $value) {
                        if (in_array($key, $cvformObj["language"])){
                            $langRow = $template["SKILLS"]["LANGUAGE-ROW"];
                            $rr = array();
                            foreach ($value as $idx=>$result){
                                $rr[] = "$idx : $result";
                            }
                            $langRow = str_replace("[LANGUAGE]", ltrim( $key, "_"), $langRow);
                            $langRow = str_replace("[RESULTS]", implode(", ",$rr), $langRow);
                            $rows .= $langRow;
                        }
                    }
                    $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . $rows . $template["SKILLS"]["token"][2];
                }
                
            } else {
                $template["SKILLS"]["token"] = explode("<!-- [LANGUAGE] -->", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . "" . $template["SKILLS"]["token"][2];
            }


            if (implode("", $cvformObj["computer"]) != "") {
                $template["SKILLS"]["token"] = explode("{COMPUTER-ROW}", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["COMPUTER-ROW"] = $template["SKILLS"]["token"][1];
                $rows = "";

                foreach ($cvformObj["metalist"]["skill"] as $skillMeta){
                    
                        foreach ($skillMeta["computer"] as $key => $value) {
                            if (in_array($key, $cvformObj["computer"])){
                                $langRow = $template["SKILLS"]["COMPUTER-ROW"];
                                $langRow = str_replace("[COMPUTER]", ltrim( $key, "_"), $langRow);
                                $langRow = str_replace("[RESULTS]", $value, $langRow);
                                $rows .= $langRow;
                            }
                        }
                        $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . $rows . $template["SKILLS"]["token"][2];
                   
                }
            } else {
                $template["SKILLS"]["token"] = explode("<!-- [COMPUTER] -->", $template["SKILLS"]["html"], 3);
                $template["SKILLS"]["html"] = $template["SKILLS"]["token"][0] . "" . $template["SKILLS"]["token"][2];
            }

            $app = $application->getDetail();
            
            foreach ($template as $part=>$content){
                $html = explode("<!-- [".$part."] -->", $html, 3);

                if (strpos( $content["html"], "<!-- [PHOTO] -->") !== FALSE) {    
                    if ( in_array("PHOTO", $addfields)) {
                        
                        $nhtml = str_replace("[PHOTO]", "https://".$_SERVER["HTTP_HOST"].$appPath."/". $app["photo"], $content["html"]);

                    } else {
                        $html[1] = explode("<!-- [PHOTO] -->", $content["html"], 3);
                        $nhtml = $html[1][0] . "" . $html[1][2];
                    }
                } else {
                    $nhtml = $content["html"];
                }
                $html = $html[0] . $nhtml . $html[2];
            }

             
            foreach ($application->getDetailByArray($basic) as $idx=>&$value) {
                
                $html = explode("<!-- [".$idx."] -->", $html, 3);
                
                if (count($html) > 1) {
                    

                    $nhtml = str_replace("[".$idx."]", $value, $html[1]);
                    $html = $html[0] . $nhtml . $html[2];
                } else {
                    $html = $html[0] . "" . $html[2];
                }
            }
            
            return $html;

         
        }

        public function renderRows($type, $rowtemplate, $rows){
            $html = "";
            $nn = array();
            
            foreach ($rows as $row) {
        
                $rt = $rowtemplate;
                switch($type) {
                    case "EDUCATION":
                        $nn["DATE"] = $row["fromdate"] . " to " . $row["todate"];
                        $nn["SCHOOL"] = $row["school"];
                        $nn["CLASS"] = $row["class"];
                        $nn["YEAROFSTUDY"] = $row["year"];
                        break; 

                    case "RESULT":
                        $nn["DATE"] = $row["year"];
                        $nn["EXAM"] = $row["exam"];
                        $rr = "";
                        foreach ($row["exam_result"] as $idx=>$result){
                            if ($idx != "") {
                                $rr .= "$idx : $result <br />";
                            }
                        }
                        $nn["RESULT"] = $rr;

                        break; 
                    case "PRESULT":
                        
                        $nn["DATE"] = $row["date"];
                        $nn["EXAM"] = $row["name"] . ", " . $row["organization"];
                        $rr = "";
                        $row["grade"] = json_decode($row["grade"], true);
                        foreach ($row["grade"] as $idx=>$result){
                            if ($idx != "") {
                                $rr .= "$idx : $result <br />";
                            }
                        }
                        $nn["RESULT"] = $rr;

                        break; 

                    case "WORK":
                    case "INTERNSHIP":
                        $nn["EMPLOYMENT"] = $row["employment"];
                    case "EXACTIVITIES": 
                        $nn["DATE"] = $row["fromdate"]  . " to " . $row["todate"];
                        $nn["ORGANIZATION"] = $row["organization"];
                        $nn["POSITION"] = $row["position"];
                        $nn["DUTIES"] = nl2br($row["duties"]);
                        break;
                    case "PROFESSIONAL":  
                    case "OTHER_QUALI":  
                        $nn["DATE"] = $row["date"];
                        $nn["NAME"] = $row["name"];
                        $nn["ORGANIZATION"] = $row["organization"];
                        $nn["GRADE"] = $row["grade"];
                        break;
                    case "ACHIEVEMENTS":  
                        $nn["DATE"] = $row["date"];
                        $nn["NAME"] = $row["name"];
                        $nn["ORGANIZATION"] = $row["organization"];
                        break;
                    case "REFERENCE":  
                        $nn["NAME"] = $row["name"];
                        $nn["POSITION"] = $row["position"];
                        $nn["ORGANIZATION"] = $row["organization"];
                        $nn["CONTACT"] = $row["contact"];
                        $nn["EMAIL"] = $row["email"];
                        
                        break;
                    case "SKILLS": 
                        $nn["SKILL"] = $row["skill"];
                        $nn["DESCRIPTION"] = html_entity_decode($row["description"], ENT_QUOTES, "UTF-8");
                        break;
                    case "OTHERS":  
                        $nn["TITLE"] = $row["title"];
                        $nn["CONTENT"] = html_entity_decode($row["html"], ENT_QUOTES, "UTF-8");
                        break;
                }
                foreach ($nn as $idx => $value) {

                    $rt = str_replace("[$idx]", $value, $rt);
                }

                $html .= $rt;
             
            }
            return $html;
        }

    
    }

?>