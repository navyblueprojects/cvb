<?php    
    include_once($rootPath.$appPath."/classes/cms.php");
    require_once(__DIR__."/syslog.php");
    require_once(__DIR__."/nqfileobj.php");

    require_once($samlPath."/_include.php"); 

    class Lecturer extends CMS{        
        protected $tableName = "lecturer";        
        public $pageName = "Lecturer";

        protected $permissionTable = "jobs_lecturer";
        protected $funcTable = "lecturer_permission";
        protected $type = "";
        protected $logined = false;
        protected $id = false;
        protected $username = false;
        public $prog_list;
        public $func_permission;
        const _session_key = "polyuhkcc:cvb::lecturer";
        
        function __construct($memObj=false){
            
            parent::__construct();

            $this->dimension["thumb"] = array(200, 200);
            $this->dimension["detail"] = array(808, 535);
            $this->__cms_fields = array();
            $this->__cms_fields[] = new CMSObj("INT", "id", ["label"=>"ID", "list"=>[], "edit"=>[]]);
            $this->__cms_fields[] = new CMSObj("HIDDEN", "node_id", ["label"=>"Category",  "list"=>[], "edit"=>["insert"]]);   
            //$this->__cms_fields[] = new CMSObj("INT", "rank", ["label"=>"Display Priority", "list"=>["show", "sort"], "edit"=>["show", "insert", "readonly"], "width"=>150]);

            
            $this->__cms_fields[] = new CMSObj("VARCHAR", "username", ["label"=>"Username",  "list"=>["show", "sort", "search"],  "width"=>200]);
            //$this->__cms_fields[] = new CMSObj("PASSWORD", "password", ["label"=>"Password",  "list"=>[], "options"=>["tips"=>"For updating new password only. Case Sensitive"]]);

            $this->__cms_fields[] = new CMSObj("VARCHAR", "name", ["label"=>"Name" , "width"=>200]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "email", ["label"=>"Email", "width"=>200]);
            $this->__cms_fields[] = new CMSObj("VARCHAR", "phone", ["label"=>"Phone", "width"=>120]);
            

            $this->__cms_fields[] = new ForeignObj("program", ["label"=>"Program", "list"=>[], "options"=>["multiple"=>"multiple","ft"=>[["table"=>"program", "value"=>"id", "label"=>"CONCAT(code, ' ', name) codename"]]], "width"=>"140" ]);

            $this->__cms_fields[] = new ForeignObj("access_control", ["label"=>"Access Right", "list"=>[], "options"=>["multiple"=>"multiple","ft"=>[["table"=>"lecturer_permission", "value"=>"id", "label"=>"name"]]], "width"=>"140" ]);
         
            $this->__cms_fields[] = new SelectObj("RADIO", "status", ["label"=>"Status", "list"=>[], "width"=>100,  "options"=>array("<span style=\"color: #00FF00\">Enable</span>"=>"E", "<span style=\"color: #CC0000\">Disable</span>"=>"D")  ]);            
            
            $this->__cms_fields[] = new CMSObj("DATETIME", "createDate", ["label"=>"Create Date", "list"=>["show", "sort"], "edit"=>["insert"], "width"=>160]);
            $this->__cms_fields[] = new CMSObj("DATETIME", "lastModDate", ["label"=>"Last Updated", "list"=>["show", "sort"], "edit"=>["insert", "update"], "width"=>160]);
            
            session_start();
            if (array_key_exists( base64_encode(self::_session_key), $_SESSION)) {
                $memObj = unserialize($_SESSION[base64_encode(self::_session_key)]);
            }
            session_write_close();

            if ($memObj) {

                $this->logined = $memObj->logined;  
                $this->type = $memObj->type;
                $this->id = $memObj->id;
                $this->prog_list = $memObj->prog_list;
                $this->func_permission = $memObj->func_permission;
                $this->username = $memObj->username;
                $this->save();   
            }
        }
        
        public function check_saml_login() {
            global $samlAuthority;
            if (!$this->conn) $this->reconnect(); 

            $token = sha1("lecturer:sp:NameID_token");

            session_start();
            if (array_key_exists($token, $_SESSION)) {
                $tmp = unserialize(base64_decode($_SESSION[$token]));
                if ($tmp) {
                    $authData = $tmp;
                }
            } else {
                $auth = new \SimpleSAML\Auth\Simple("default-sp");
                $authData = $auth->getAuthData("saml:sp:NameID");
            }
            session_write_close();

            if ($authData) {
                session_start();
                $_SESSION[$token] = base64_encode(serialize($authData));
                session_write_close();

                if (strpos($authData->value, "student") === FALSE) {
                    list($username, $domain) = explode("@", $authData->value);

                    $mywhere = array("status='E'");
                    $mywhere[] = "username = ?";

                    $data = $this->get($this->tableName, array("offset"=>0, "limit"=>1, "where"=>$mywhere, "where_parameters"=>[$username]));

                    if ($data) {
                        $this->logined = true;  
                        $this->type = $data[0]["node_id"];
                        $this->id = $data[0]["id"];
                        $this->prog_list = $data[0]["program"];

                        $this->func_permission = $this->getStaffPermission($data[0]["access_control"]);
                        
                        $this->username = $data[0]["username"];
                        $this->save();
                        
                        return true;

                    } else {
                        // create default user
                        $this->logined = true;

                        $lecturer = array();
                        $lecturer["username"] = $username;
                        $lecturer["name"] = $username;
                        $lecturer["email"] = $authData->value;
                        $lecturer["phone"] = "";
                        $lecturer["program"] = "";
                        $lecturer["access_control"] = "";
                        $lecturer["status"] = "E";
                        $lecturer["createDate"] = date("Y-m-d H:i:s");
                        $lecturer["lastModDate"] = date("Y-m-d H:i:s");

                        $this->conn->AutoExecute($this->tableName, $lecturer, "INSERT");

                        $this->type = $data[0]["node_id"];
                        $this->id = $this->conn->insert_Id();
                        
                        $this->username = $lecturer["username"];
                        $this->save();
                        
                        return true;
                    }
                } else {
                    return false;
                }
            } 
            return false;
    
        }

        public function start_login() {
            global $domain, $conn;
            $auth = new \SimpleSAML\Auth\Simple('default-sp');

            $auth->login([
                "ReturnTo"=>"https://".$domain."/wie/login_landing",
                "KeepPost"=>TRUE,
            ]);
        }

        public function isLogined() {
            if (!$this->check_saml_login()){
                return false;
            }
            $this->reconnect();
            return $this->logined;
        }

        public static function retain(){        
            global $dbDatabase;
            session_start();
            $oo = (array_key_exists( base64_encode(self::_session_key), $_SESSION)) ? $_SESSION[base64_encode(self::_session_key)] : false;            
            session_write_close();

            if (!is_object($oo)){
                $oo = unserialize($oo);        
            }

            if ($oo) {
                $obj = new Lecturer($oo);
            } else {
                $obj = new Lecturer();
            }

            $obj->reconnect();
            return $obj;
        }

       
        public function getName(){
            return $this->username;
        }
        
        public function getDetails(){
            
            if ($this->logined) {

                $details = $this->getDetail($this->tableName, $this->id);           
            }
            return $details;
        }
  

        public function update($arr){
            if ($this->logined) {
                if ($arr["password"] == '') {
                    unset($arr["password"]);
                }
                $this->conn->AutoExecute($this->tableName, $arr, "UPDATE", "id = '" .$this->id . "'");
                $msg = 'SUCCESS';
                $status = 1;
            } else {
                $msg = 'UPDATE FAILED. NOT YET LOGGED IN';
                $status = 0;
            }
            
            return json_encode(array("status"=>$status, "msg"=>$msg)); 
        }

        public function getFileById($type, $id, $str=""){
            
            $fileObj = new NqFileObj(true);
            if ($str == "") {
                return $fileObj->download($id);
            } else {
                return $fileObj->generatefile($id);
            }
        }
        
        public function login($username, $password, $captcha) {
            
            $this->logined = false;
            
            $logger = new Syslog();

            $ldap = $this->ldap_login($username,  $password);

            if ($ldap < 0) {
                $logger->log("login", "lecturer", ["member_id"=> 0, "para"=>json_encode(["username"=>$username, "result"=>"FAILED"])], "FAILED");
           
                return json_encode(array("status"=>0,
                                         "msg"=>"login failed"
                                ));
            } 

            session_start();
            if (md5($captcha) <> $_SESSION["lctr_login_captcha"]) {
                $logger->log("login", "lecturer", ["member_id"=> 0, "para"=>json_encode(["username"=>$username, "result"=>"INCORRECT CAPTCHA"])], "FAILED");
           
                return json_encode(array("status"=>0,
                                    "msg"=>"incorrect captcha"
                                ));
            } 
            session_write_close();
            
            $mywhere = array("status='E'");
            $mywhere[] = "username = ?";

            $data = $this->get($this->tableName, array("offset"=>0, "limit"=>1, "where"=>$mywhere, "where_parameters"=>[$username]));

            if ($data) {
                $this->logined = true;  
                $this->type = $data[0]["node_id"];
                $this->id = $data[0]["id"];
                $this->prog_list = $data[0]["program"];

                $this->func_permission = $this->getStaffPermission($data[0]["access_control"]);
                
                $this->username = $data[0]["username"];
                $this->save();

                $logger->log("login", "lecturer", ["member_id"=> $this->id, "para"=>"SUCCESS"], "SUCCESS");

                $msg = "SUCCESS";
            } else {
                
                // create default user
                $this->logined = true;

                $lecturer = array();
                $lecturer["username"] = $username;
                $lecturer["name"] = $ldap[0]["cn"][0];
                $lecturer["email"] = $ldap[0]["userprincipalname"][0];
                $lecturer["phone"] = "";
                $lecturer["program"] = "";
                $lecturer["access_control"] = "";
                $lecturer["status"] = "E";
                $lecturer["createDate"] = date("Y-m-d H:i:s");
                $lecturer["lastModDate"] = date("Y-m-d H:i:s");

                $this->conn->AutoExecute($this->tableName, $lecturer, "INSERT");

                $this->type = $data[0]["node_id"];
                $this->id = $this->conn->insert_Id();
                
                $this->username = $lecturer["username"];
                $this->save();

                $logger->log("login", "lecturer", ["member_id"=> $this->id, "para"=>"SUCCESS"], "SUCCESS");


                $msg = "SUCCESS";

            }
            return json_encode(array("status"=>(int)$this->logined, "msg"=>$msg));
        }
        
        public static function logout(){
            global $dbDatabase, $domain;    

            if (!$_SESSION) {
                session_start();
            }
            $_SESSION[base64_encode(self::_session_key)] = false;   
            session_destroy();
            session_write_close();

            $authObj = new \SimpleSAML\Auth\Simple("default-sp");   
            $authObj->logout([
                "ReturnTo"=> "https://".$domain."/",                           
                "ReturnStateParam" => "LogoutState",                            
                "ReturnStateStage" => "MyLogoutState"
            ]);
            \SimpleSAML\Session::getSessionFromRequest()->cleanup();

        }
 
        public function save(){
            global $dbDatabase; 
            $this->conn = false; 
            if (!$_SESSION) {
                session_start();
            }
            $_SESSION[base64_encode(self::_session_key)] = serialize($this);
            session_write_close();
        }
        
        public function getJobList($offset, $limit, $where=false){
            $mywhere = array();
            $mywhere["where"][] = "lecturer_id = '".$this->id."'";
            $mywhere["where"][] = "permission LIKE '%\"READ\":1,%'";
            
            $permission = $this->get($this->permissionTable, $mywhere);
            
            $data = array();
            $data2 = array();
            foreach ($permission as $job) {
                $data[] = $job["job_id"];
            }

            if ($where["programme_id"] !== FALSE) {
                $jobs_permission = $this->get("jobs_permission", array("where"=>array("program_id in ('".implode("','", $where["programme_id"])."')")));
                foreach ($jobs_permission as $jp) {
                    $data2[] = $jp["job_id"];
                }
            }


            if ($where === FALSE) {
                $where = array();
            }
            $where["offset"] = $offset;
            $where["limit"] = $limit;
            $where["where"][] = "status = 'E'";
            $where["where"][] = "id in ('".implode("','", $data)."')";
            if (count($data2) > 0) {
                $where["where"][] = "id in ('".implode("','", $data2)."')";
            }
            
            
            return $this->get("jobs", $where);
        }

        private function getStaffPermission($access_control=""){
        	if ($access_control == "") {
        		$me = $this->getOne($this->tableName, ["where"=>["id = '$this->id' "]]);
        		$access_control =  $me["access_control"];

        	}        	
        	$permission = $this->get($this->funcTable, ["where"=>["id in ('".implode("','",explode(";", $access_control)). "')"]]);


        	foreach ($permission as $pp) {
        		$this->func_permission[] = $pp["name"];
        	}

        	return $this->func_permission; 
        }
        
        
        public function offerApplication($item_id){
            $application = new Application();
            $log = new Syslog();

            $log->log("approval", "lecturer", array("member_id"=>$this->id, "para"=>$item_id), "SUCCESS");
            
            return $application->offerApplication($item_id);
        }

        public function sendOfferEmail($item_id) {
            $application = new Application();
            $item_row = $application->getApplicationByItemSha1Id($item_id);
            
            $log = new Syslog();
            $log->log("sendOfferEmail", "lecturer", array("member_id"=>$this->id, "para"=>$item_row), "SUCCESS");
            
            return $application->sendOfferEmail(array($item_row));
        }

        public function batchUpdateStatus($status) {
            $application = new Application();
            $log = new Syslog();

            foreach ($status as $key => $value) {

                $para = array();
                $para["item_id"] = $item_id;
                $para["status"] = $value;

                $log->log("approval", "lecturer", array("member_id"=>$this->id, "para"=>json_encode($para)), $value);
            }
            return $application->updateStatus($status);
        }

        public function updateApplicationStatus($status) {
            $application = new Application();
            $log = new Syslog();

            foreach ($status as $key => $value) {
                $para = array();
                $para["item_id"] = $key;
                $para["status"] = $value;

                $log->log("approval", "lecturer", array("member_id"=>$this->id, "para"=>json_encode($para)), $value);
            }
            $application->updateStatus($status);
            return $application->getId();
        }

        
        public function getXhrList($q=""){
            if ($this->conn == false) {
                $this->reconnect();
            }

            $mywhere = array();
            $mywhere["fields"] = array("id"=>"id", "username"=>"username" );
            $mywhere["limit"] = 999;
            $mywhere["where"][] = "status = 'E'";

            if ($q != "") {
                $mywhere["where"][] = "username LIKE '%".$q."%'";
            }

            $mywhere["order"][] = "name";
            
            $list = $this->get($this->tableName, $mywhere);
            
            $result = array();
            foreach ($list as $li) {
                $result["results"][] = array(
                    "id" => (int)$li["id"], 
                    "text" => $li["username"],
                );
            }
            
            return $result;
        }

        public function ldap_login($username, $password) {
            global $staff_directory, $staff_dn, $staff_domain;

            $dsStaff = ldap_connect($staff_directory);
            ldap_set_option($dsStaff, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($dsStaff, LDAP_OPT_REFERRALS, 0);


            if (@ldap_bind($dsStaff, $username . $staff_domain, $password)) {
                foreach ($staff_dn as $dn) {
                    $r = @ldap_search($dsStaff, $dn, '(sAMAccountname='.$username.")");
                    if ($r) {
                        break;
                    }
                }
            }
            if (!$r) {
                return -1;
            }
            
            $result = @ldap_get_entries( $dsStaff, $r);
            if (!$result[0]) {
                return -1;
            }
            if (!@ldap_bind( $dsStaff, $result[0]['dn'], $password) ) {
                return false;
            } else {
                return $result[0];
            }
        }

        public function getToken() {
            return md5($this->id . base64_encode(self::_session_key));
        }

        public function validateToken($token) {
            return $this->conn->getRow("Select id from lecturer where  md5(concat(id, '". base64_encode(self::_session_key)."')) = '".$token."'");

            //return md5($this->id . );
        }

        public function getPermission($job_id){
            $data =  $this->getOne($this->permissionTable, array("where"=>array("lecturer_id = '".$this->id."'", "job_id = '".$job_id."'")));

            return json_decode($data["permission"], true);
        }

    }



?>