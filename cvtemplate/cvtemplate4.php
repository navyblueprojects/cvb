<page>
	<page_header>
		<base href="https://cvbuilder.cpce-polyu.edu.hk/" >
		<link rel="stylesheet" href="css/style.css" />
	</page_header>
	<body id="template4" class="template">
		<main>
		<!-- [BASIC] -->
		<style>
			ul { list-style: none; padding: 0;  }
			p { margin: 0; }
			li { margin: 0; padding: 0; padding-left: 10px; }
			hr { border: 1px solid #202020; }
			img { width: 150px }
		</style>
		<table style="width: 100%;" width="100%">
			<tr><td style="width: 440px; vertical-align: top; " ><!-- [ENAME] --><h1>[ENAME] <!-- [CNAME] -->([CNAME])<!-- [CNAME] --></h1><!-- [ENAME] --></td>
			<td style="width: 300px; vertical-align: top;" ><table >
				<!-- [ADDRESS] --><tr><td style="width: 50%; vertical-align: top;">Address:</td><td style="width: 50%; vertical-align: top;">[ADDRESS]</td></tr><!-- [ADDRESS] -->
				<!-- [CONTACT] --><tr><td style=" vertical-align: top;">Contact:</td><td >[CONTACT]</td></tr><!-- [CONTACT] -->
				<!-- [EMAIL] --><tr><td style=" vertical-align: top;">Email:</td><td >[EMAIL]</td></tr><!-- [EMAIL] -->
				<!-- [GENDER] --><tr><td style="vertical-align: top;">Gender:</td><td >[GENDER]</td></tr><!-- [GENDER] -->
				<!-- [AGE] --><tr><td style="vertical-align: top;">Age:</td><td >[AGE]</td></tr><!-- [AGE] -->
				<!-- [DOB] --><tr><td style="vertical-align: top; " >Date of Birth:</td><td >[DOB]</td></tr><!-- [DOB] -->
				<!-- [RELIGION] --><tr><td style="vertical-align: top; height:26px;">Religion:</td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
			</table></td>
		</tr></table><hr />
		<!-- [BASIC] -->
		<!-- [EDUCATION] -->
		<table border="0" cellspacing="0px" style="width: 100%;" width="100%">
			<tr>
				<td style="width: 592px; vertical-align: top;">
					<div class="title" style="width:592px; vertical-align: top;"><h2>EDUCATION</h2></div>
					<table border="0" cellspacing="0px" >
						{EDUCATION-ROW}<tr><td style="width:236px; vertical-align: top;" >[DATE]</td>
										   <td style="width:356px; vertical-align: top;"><strong>[SCHOOL]</strong><BR />
													<i>[CLASS]</i><br >[YEAROFSTUDY]</td></tr>{EDUCATION-ROW}
					</table><br /><br />
				</td>
				<td style="width: 148px; vertical-align: top; text-align: right;" align="right"><!-- [PHOTO] --><img src="[PHOTO]"  /><!-- [PHOTO] --></td>
			</tr>
		</table>
		<!-- [EDUCATION] -->
		<!-- [RESULT] -->
		<div class="title" style="width:592px; vertical-align: top;"><h2>ACADEMIC ATTAINMENT</h2></div>
		<table border="0" cellspacing="0px" style="width:740px; vertical-align: top;">
			<!-- [GPA] --><tr><td style="width:236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
							  <td style="width:504px; padding-bottom: 14px; vertical-align: top;"><strong>[SCHOOL]</strong><BR>CGPA: [POINT]</td></tr><!-- [GPA] -->
			<!-- [CGPA] --><tr><td style="width:236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
								<td style="width:504px; padding-bottom: 14px; vertical-align: top;"><strong>[COLLEGE]</strong><BR>CGPA: [POINT]</td></tr><!-- [CGPA] -->
			{RESULT-ROW}<tr><td style="width:236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
								<td style="width:504px; vertical-align: top;"><strong>[EXAM]</strong>
										<ul><li>[RESULT]</li></ul></td></tr>{RESULT-ROW}
		</table>
		<br />
		<!-- [RESULT] -->
		<!-- [PRESULT] -->
		<table  border="0" cellspacing="0px" style="width:740px vertical-align: top;">
			{PRESULT-ROW}<tr ><td style="width:236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
								<td style="width:504px; padding-bottom: 14px; vertical-align: top;"><strong>[EXAM]</strong><BR><ul><li>[RESULT]</li></ul></td></tr>{PRESULT-ROW}
		</table>
		<!-- [PRESULT] -->
		<!-- [WORK] -->
		<div class="title" style="width: 592px;  vertical-align: top;"><h2>WORK EXPERIENCE</h2></div>
		<table border="0" cellspacing="0px">
			{WORK-ROW}<tr><td style="width: 236px;   padding-bottom: 14px; vertical-align: top;">[DATE]</td>
						 <td style="width:504px;  vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
										  [POSITION]
										  <ul><li>[DUTIES]</li></ul>
										</td></tr>{WORK-ROW}
		</table>
		<br /><br />
		<!-- [WORK] -->
		<!-- [INTERNSHIP] -->
		<div class="title" style="  vertical-align: top;"><h2>INTERNSHIP EXPERIENCE</h2></div>
		<table  border="0" cellspacing="0px">
			{INTERNSHIP-ROW}<tr><td style="width:236px;  padding-bottom: 14px; vertical-align: top;">[DATE]</td>
								<td style="width:504px; vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
										  [POSITION]
										  <ul><li>[DUTIES]</li></ul>
										</td></tr>{INTERNSHIP-ROW}
		</table>
		<br /><br />
		<!-- [INTERNSHIP] -->
		<!-- [EXACTIVITIES] -->
		<div class="title" style=" vertical-align: top;"><h2>EXTRA-CURRICULAR ACTIVITIES</h2></div>
		<table border="0" cellspacing="0px">
			{EXACTIVITIES-ROW}<tr><td style="width: 236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
									<td style="width:504px; padding-bottom: 14px; vertical-align: top;"><strong>[ORGANIZATION]</strong><br />
										  [POSITION]
										  <ul><li>[DUTIES]</li></ul>
										</td></tr>{EXACTIVITIES-ROW}
		</table>
		<br /><br />
		<!-- [EXACTIVITIES] -->
		<!-- [PROFESSIONAL] -->
		<div class="title" style="vertical-align: top;"><h2>PROFESSIONAL QUALIFICATIONS</h2></div>
		<table border="0" cellspacing="0px">
			{PROFESSIONAL-ROW}<tr><td style="width: 236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
								  <td style="width: 504px; padding-bottom: 14px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
										  <ul><li>[GRADE]</li></ul>
										</td></tr>{PROFESSIONAL-ROW}
		</table>
		<br /><br />
	
		<!-- [PROFESSIONAL] -->
		<!-- [OTHER_QUALI] -->
		<div class="title" style=" vertical-align: top;"><h2>OTHER QUALIFICATIONS</h2></div>
		<table  border="0" cellspacing="0px">
			{OTHER_QUALI-ROW}<tr><td style="width: 236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
								 <td style="width: 504px; padding-bottom: 14px;  vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
										  <ul><li>[GRADE]</li></ul>
										</td></tr>{OTHER_QUALI-ROW}
			
		</table>
		<br /><br />
	
		<!-- [OTHER_QUALI] -->
		<!-- [ACHIEVEMENTS] -->
		<div class="title" style="vertical-align: top;"><h2>ACHIEVEMENTS</h2></div>
		<table border="0" cellspacing="0px">
			{ACHIEVEMENTS-ROW}<tr><td style="width: 236px; padding-bottom: 14px; vertical-align: top;">[DATE]</td>
								  <td style="width: 504px; padding-bottom: 14px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong></td></tr>{ACHIEVEMENTS-ROW}
			
		</table>
		<br /><br />
		
		<!-- [ACHIEVEMENTS] -->
		<!-- [SKILLS] -->
		<div class="title" style=" vertical-align: top;"><h2>SKILLS</h2></div>
		<table border="0" cellspacing="0px">
			<tr><td style="width: 236px; padding-bottom: 14px; vertical-align: top;">Language Proficiency:</td>
				<td style="width: 504px; padding-bottom: 14px; vertical-align: top;">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br />[RESULTS]<br />{LANGUAGE-ROW}</td></tr>
			<tr><td style="width: 236px; padding-bottom: 14px; vertical-align: top;">Technical Skills:</td>
				<td style="width: 504px; padding-bottom: 14px; vertical-align: top;">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td></tr>
			{SKILLS-ROW}<tr><td style="width: 236px; padding-bottom: 14px; vertical-align: top;">[SKILL]:</td>
						<td style="width: 504px; padding-bottom: 14px; vertical-align: top;">[DESCRIPTION]</td></tr>{SKILLS-ROW}
		</table>
		<!-- [SKILLS] -->
		<!-- [OTHERS] -->
		{OTHERS-ROW}
		<div class="title" style=" vertical-align: top; text-transform: uppercase"><h2>[TITLE]</h2></div>
		<div  style=" padding-bottom: 14px; vertical-align: top; font-family: Times; font-size: 16px">[CONTENT]</div>
		{OTHERS-ROW}
		<!-- [OTHERS] -->
		<!-- [REFERENCE] -->
		<div class="title" style="vertical-align: top;"><h2>REFERENCE</h2></div>
		
		<table border="0" cellspacing="0px">
			{REFERENCE-ROW}
			<tr><td style="width:236px vertical-align: top;">Name:</td>
				<td style="width:504px; vertical-align: top;">[NAME]</td></tr>
			<tr><td style="width:236px; vertical-align: top;">Position:</td>
				<td style="width:504px; vertical-align: top;">[POSITION]</td></tr>
			<tr><td style="width:236px; vertical-align: top;">Organization:</td>
				<td style="width:504px; vertical-align: top;">[ORGANIZATION]</td></tr>
			<tr><td style="width:236px; vertical-align: top;">Contact Number:</td>
				<td style="width:504px; vertical-align: top;">[CONTACT]</td></tr>
			<tr><td style="width:236px; vertical-align: top;">Email:</td>
				<td style="width:504px; vertical-align: top;">[EMAIL]</td></tr>
			<tr><td style="width:236px; vertical-align: top;">&nbsp;</td>
				<td style="width:504px; vertical-align: top;">&nbsp;</td></tr>
			{REFERENCE-ROW}
		</table>
		
		<!-- [REFERENCE] -->
		<!-- [ETC] -->
		<!-- [EXPECTED_SALARY] --><div class="title" style=" vertical-align: top; "><h2>EXPECTED SALARY:</h2></div><div style="font-family: Times; font-size: 16px">[EXPECTED_SALARY]</div><BR><!-- [EXPECTED_SALARY] -->
		<!-- [AVAILABILITY] --><div class="title" style="  vertical-align: top;"><h2>AVAILABILITY:</h2></div><div style="font-family: Times; font-size: 16px">[AVAILABILITY]</div><BR><!-- [AVAILABILITY] -->
		<!-- [ETC] -->
		</main>
	</body>
</page>