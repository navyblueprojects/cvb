<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/style.css" />
</head>
<body id="template1" class="template">
<main>
	<!-- [BASIC] -->
	<style>
		h2 {style="border-bottom:1px solid #000000}
	</style>
	<h1><!-- [ENAME] -->[ENAME]<!-- [ENAME] --><span style="font-family: Times"><!-- [CNAME] -->([CNAME])<!-- [CNAME] --></span></h1>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		<tr><td><table width="100%" border="0" cellspacing="0px">
		<!-- [ADDRESS] --><tr><td valign="top" style="width:200px; vertical-align: top;" >Address:</td><td style="width:300px; vertical-align: top;">[ADDRESS]</td></tr><!-- [ADDRESS] -->
		<!-- [CONTACT] --><tr><td valign="top" >Contact Number:</td><td valign="top">[CONTACT]</td></tr><!-- [CONTACT] -->
		<!-- [EMAIL] --><tr><td valign="top">Email:</td><td valign="top">[EMAIL]</td></tr><!-- [EMAIL] -->
		<!-- [GENDER] --><tr><td valign="top">Gender:</td><td valign="top">[GENDER]</td></tr><!-- [GENDER] -->
		<!-- [AGE] --><tr><td valign="top">Age:</td><td valign="top">[AGE]</td></tr><!-- [AGE] -->
		<!-- [DOB] --><tr><td valign="top">Date of Birth:</td><td valign="top">[DOB]</td></tr><!-- [DOB] -->
		<!-- [RELIGION] --><tr><td valign="top">Religion:</td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
		</table></td><td  style="text-align: right; width:180px; vertical-align: top; "><!-- [PHOTO] --><img src="[PHOTO]" width="180" /><!-- [PHOTO] --></td></tr>
	</table>
	<br />
	<!-- [BASIC] -->
	<!-- [EDUCATION] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">EDUCATION</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		{EDUCATION-ROW}<tr style="padding: 0px;"><td valign="top" style="width:200px; padding: 0px; margin:0px;">[DATE]</td><td valign="top" style="width:600px;" ><b>[SCHOOL]</b><BR><i>[CLASS]</i><br >[YEAROFSTUDY]</td></tr>{EDUCATION-ROW}
	</table>
	<br />
	<!-- [EDUCATION] -->
	<!-- [RESULT] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">ACADEMIC ATTAINMENT</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		<!-- [GPA] --><tr ><td valign="top" style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:600px;"><strong>[SCHOOL]</strong><br>CGPA: [POINT]</td></tr><!-- [GPA] -->
		<!-- [CGPA] --><tr ><td valign="top" style="width:200px; vertical-align: top;">[DATE]</td><td valign="top"  style="width:600px;"><strong>[COLLEGE]</strong><br>CGPA: [POINT]<br></td></tr><!-- [CGPA] -->
		{RESULT-ROW}<tr ><td valign="top" style="width:200px; vertical-align: top;" >[DATE]</td><td valign="top"  style="width:600px;"><strong>[EXAM]</strong><ul style="list-style: none;"><li style="padding-left: 40px;">[RESULT]</li></ul></td></tr>{RESULT-ROW}
	</table>
	<!-- [RESULT] -->
	<!-- [PRESULT] -->
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		{PRESULT-ROW}<tr ><td valign="top" style="width:200px; vertical-align: top;" >[DATE]</td><td valign="top" style="width:600px;"><strong>[EXAM]</strong><ul style="list-style: none;"><li style="padding-left: 40px;">[RESULT]</li></ul></td></tr>{PRESULT-ROW}
	</table>
	<!-- [PRESULT] -->
	<!-- [WORK] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">WORK EXPERIENCE</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">		
		{WORK-ROW}<tr ><td style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:600px;"><strong>[ORGANIZATION]  ([EMPLOYMENT])</strong><br />[POSITION] <ul style="list-style: none;"><li style="padding-left: 40px;">[DUTIES] </li></ul></td></tr>{WORK-ROW}
	</table>
	<!-- [WORK] -->
	<!-- [INTERNSHIP] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">INTERNSHIP EXPERIENCE</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		{INTERNSHIP-ROW}<tr ><td style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:600px;"><strong>[ORGANIZATION]  ([EMPLOYMENT])</strong><br />[POSITION] <ul style="list-style: none;"><li style="padding-left: 40px;">[DUTIES] </li></ul></td></tr>{INTERNSHIP-ROW}
	</table>
	<br />
	<!-- [INTERNSHIP] -->
	<!-- [EXACTIVITIES] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">EXTRA-CURRICULAR ACTIVITIES</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		{EXACTIVITIES-ROW}
		<tr ><td  style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:600px;"><strong>[ORGANIZATION]</strong><br />[POSITION] <ul style="list-style: none;"><li style="padding-left: 40px;">[DUTIES] </li></ul></td></tr>
		{EXACTIVITIES-ROW}
	</table>
	<br /> 
	<!-- [EXACTIVITIES] -->
	<!-- [PROFESSIONAL] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">PROFESSIONAL QUALIFICATIONS</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		{PROFESSIONAL-ROW}
		<tr ><td  style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:600px;"><strong>[NAME], [ORGANIZATION]</strong><br /> <ul style="list-style: none;"><li style="padding-left: 40px;">[GRADE]</li></ul></td></tr>
		{PROFESSIONAL-ROW}
	</table>
	<br /> 
	<!-- [PROFESSIONAL] -->
	<!-- [OTHER_QUALI] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">OTHER QUALIFICATIONS</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		{OTHER_QUALI-ROW}<tr ><td  style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:600px;"><strong>[NAME], [ORGANIZATION]</strong><ul style="list-style: none;"><li style="padding-left: 40px;">[GRADE]</li></ul></td></tr>{OTHER_QUALI-ROW}
	</table>
	<br /> 
	<!-- [OTHER_QUALI] -->
	<!-- [ACHIEVEMENTS] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">ACHIEVEMENTS</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">		
		{ACHIEVEMENTS-ROW}<tr ><td style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:600px;">[NAME], [ORGANIZATION]</td></tr>{ACHIEVEMENTS-ROW}
	</table>
	<br />
	<!-- [ACHIEVEMENTS] -->
	<!-- [SKILLS] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">SKILLS</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">
		<tr ><td style="width:200px; vertical-align: top;">Language Proficiency:</td><td valign="top" style="width:600px;">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr>
		<tr ><td style="width:200px; vertical-align: top;">Technical Skills:</td><td valign="top" style="width:600px;">{COMPUTER-ROW}<u>[COMPUTER]:</u> [RESULTS]<br />{COMPUTER-ROW}</td></tr>
		{SKILLS-ROW}<tr><td style="width:200px; vertical-align: top;">[SKILL]:</td><td valign="top" style="width:600px;">[DESCRIPTION]</td></tr>{SKILLS-ROW}
	</table>
	<br /> 
	<!-- [SKILLS] -->
	<!-- [OTHERS] -->
		{OTHERS-ROW}<div class="title"><h2 style="border-bottom:1px solid #000000; vertical-align: top; text-transform: uppercase">[TITLE]</h2></div><div style="font-family: Times;  font-size: 14px;">[CONTENT] &nbsp;</div>{OTHERS-ROW}
	<!-- [OTHERS] -->
	<!-- [REFERENCE] -->
	<div class="title"><h2 style="border-bottom:1px solid #000000">REFERENCE</h2></div>
	<table width="100%" border="0" cellspacing="0px" style="font-family: Times;">{REFERENCE-ROW}
		<tr ><td style="width:200px; vertical-align: top;">Name:</td><td valign="top" style="width:600px;">[NAME]</td></tr>
		<tr ><td style="width:200px; vertical-align: top;">Position:</td><td valign="top" style="width:600px;">[POSITION]</td></tr>
		<tr ><td style="width:200px; vertical-align: top;">Organization:</td><td valign="top" style="width:600px;">[ORGANIZATION]</td></tr>
		<tr ><td style="width:200px; vertical-align: top;">Contact Number:</td><td valign="top" style="width:600px;">[CONTACT]</td></tr>
		<tr ><td style="width:200px; vertical-align: top;">Email:</td><td valign="top" style="width:600px;">[EMAIL]</td></tr>
	{REFERENCE-ROW}</table>	
	<br />		
	<!-- [REFERENCE] -->
	<!-- [ETC] -->
		<!-- [EXPECTED_SALARY] --><h3 style="font-size: 22px">EXPECTED SALARY: <span style="font-family: Times; font-size:16px">[EXPECTED_SALARY]</span></h3><!-- [EXPECTED_SALARY] -->
		<!-- [AVAILABILITY] --><h3 style="font-size: 22px">AVAILABILITY: <span style="font-family: Times; font-size:16px">[AVAILABILITY]</span></h3><!-- [AVAILABILITY] -->
	<!-- [ETC] -->
</main>
</body>
</html>