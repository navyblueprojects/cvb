<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body id="template2" class="template">
		<main>
			<!-- [BASIC] -->
			<div class="title"><h2 style="background-color: #CCCCCC">PERSONAL PARTICULARS</h2></div>
			<table  width="100%" border="0" cellspacing="0px">
				<tr><td style="width:535px; vertical-align: top;">
				<table width="100%" border="0" cellspacing="0px">
				<!-- [ENAME] --><tr><td valign="top" style="width:210px; vertical-align: top;" >Name:</td><td valign="top" style="width:325px; vertical-align: top;" >[ENAME] <!-- [CNAME] --><span style="font-family: cid0ct;">([CNAME])</span><!-- [CNAME] --></td></tr><!-- [ENAME] -->
				<!-- [ADDRESS] --><tr><td valign="top">Address:</td><td valign="top">[ADDRESS]</td></tr><!-- [ADDRESS] -->
				<!-- [CONTACT] --><tr><td valign="top">Contact Number:</td><td valign="top">[CONTACT]</td></tr><!-- [CONTACT] -->
				<!-- [EMAIL] --><tr><td valign="top">Email:</td><td valign="top">[EMAIL]</td></tr><!-- [EMAIL] -->
				<!-- [GENDER] --><tr><td valign="top">Gender:</td><td valign="top">[GENDER]</td></tr><!-- [GENDER] -->
				<!-- [AGE] --><tr><td valign="top">Age:</td><td valign="top">[AGE]</td></tr><!-- [AGE] -->
				<!-- [DOB] --><tr><td valign="top" >Date of Birth:</td><td valign="top">[DOB]</td></tr><!-- [DOB] -->
				<!-- [RELIGION] --><tr><td valign="top">Religion:</td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
				<!-- [AVAILABILITY] --><tr><td valign="top">Availability:</td><td valign="top">[AVAILABILITY]</td></tr><!-- [AVAILABILITY] -->
				<!-- [EXPECTED_SALARY] --><tr><td valign="top">Expected Salary:</td><td valign="top">[EXPECTED_SALARY]</td></tr><!-- [EXPECTED_SALARY] -->
			</table></td>
				<td valign="top" style="width:250px; vertical-align: top;"><img src="[PHOTO]"  width="250"  /></td>
			</tr>
			</table>

			<br />
			<!-- [BASIC] -->
			<!-- [EDUCATION] -->
			<div class="title"><h2 style="background-color: #CCCCCC">EDUCATION</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{EDUCATION-ROW}<tr ><td valign="top" style="width:210px; vertical-align: top;">[DATE]</td><td valign="top" style="width:535px; vertical-align: top;"><b>[SCHOOL]</b><BR><i>[CLASS]</i><br >[YEAROFSTUDY]</td></tr>{EDUCATION-ROW}
			</table>
			<br />
			<!-- [EDUCATION] -->
			<!-- [RESULT] -->
			<div class="title"><h2 style="background-color: #CCCCCC">ACADEMIC ATTAINMENT</h2></div>
			<table  width="100%" border="0" cellspacing="0px">
				<!-- [GPA] --><tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[SCHOOL]</strong><br>CGPA: [POINT]</td></tr><!-- [GPA] -->
				<!-- [CGPA] --><tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[COLLEGE]</strong><br>CGPA: [POINT]</td></tr><!-- [CGPA] -->
				{RESULT-ROW}<tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[EXAM]</strong>
											<ul style="list-style: none; "><li style="padding-left: 40px;">[RESULT]</li></ul></td></tr>{RESULT-ROW}
			</table>
			<!-- [RESULT] -->
			<!-- [PRESULT] -->
			<table width="100%" border="0" cellspacing="0px">
				{PRESULT-ROW}<tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[EXAM]</strong>
											<ul style="list-style: none; "><li style="padding-left: 40px;">[RESULT]</li></ul></td></tr>{PRESULT-ROW}
			</table>
			<!-- [PRESULT] -->
			<!-- [WORK] -->
			<div class="title"><h2 style="background-color: #CCCCCC">WORK EXPERIENCE</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{WORK-ROW}<tr  ><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  <i>[POSITION]</i>
											  <ul style="list-style: none;"><li style="padding-left: 40px;">[DUTIES] </li></ul>
											</td></tr>{WORK-ROW}
			</table>
			<br />
			<!-- [WORK] -->
			<!-- [INTERNSHIP] -->
			<div class="title"><h2 style="background-color: #CCCCCC">INTERNSHIP EXPERIENCE</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{INTERNSHIP-ROW}<tr ><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  <i>[POSITION]</i>
											  <ul style="list-style: none;"><li style="padding-left: 40px;">[DUTIES]</li></ul>
											</td></tr>{INTERNSHIP-ROW}
			</table>
			<br />
			<!-- [INTERNSHIP] -->
			<!-- [EXACTIVITIES] -->
			<div class="title"><h2 style="background-color: #CCCCCC">EXTRA-CURRICULAR ACTIVITIES</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{EXACTIVITIES-ROW}<tr  ><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[ORGANIZATION]</strong><br />
											  <i>[POSITION]</i>
											  <ul style="list-style: none;"><li style="padding-left: 40px;">[DUTIES]</li></ul>
											</td></tr>{EXACTIVITIES-ROW}
			</table>
			<br />
			<!-- [EXACTIVITIES] -->
			<!-- [PROFESSIONAL] -->
			<div class="title"><h2 style="background-color: #CCCCCC">PROFESSIONAL QUALIFICATIONS</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{PROFESSIONAL-ROW}
				<tr ><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
											  <ul style="list-style: none;"><li style="padding-left: 40px;">[GRADE]</li></ul>
											</td></tr>
				{PROFESSIONAL-ROW}
			</table>
			<br />
			<!-- [PROFESSIONAL] -->
			<!-- [OTHER_QUALI] -->
			<div class="title"><h2 style="background-color: #CCCCCC">OTHER QUALIFICATIONS</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{OTHER_QUALI-ROW}
				<tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:535px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
											  <ul style="list-style: none;"><li style="padding-left: 40px;">[GRADE]</li></ul>
											</td></tr>
				{OTHER_QUALI-ROW}

			</table>
			<br />
			<!-- [OTHER_QUALI] -->
			<!-- [ACHIEVEMENTS] -->
			<div class="title"><h2 style="background-color: #CCCCCC">ACHIEVEMENTS</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{ACHIEVEMENTS-ROW}<tr ><td style="width:210px; vertical-align: top; padding-bottom: 24px">[DATE]</td><td style="width:535px; vertical-align: top;">[NAME], [ORGANIZATION]</td></tr>{ACHIEVEMENTS-ROW}
			</table>
			<br />
			<!-- [ACHIEVEMENTS] -->
			<!-- [SKILLS] -->
			<div class="title"><h2 style="background-color: #CCCCCC">SKILLS</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				<!-- [LANGUAGE] --><tr ><td style="width:210px; vertical-align: top;">Language Proficiency:</td><td style="width:535px; padding-bottom: 20px; vertical-align: top;">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr><!-- [LANGUAGE] -->
				<!-- [COMPUTER] --><tr>
					<td style="width:210px; vertical-align: top;">Technical Skills:</td>
					<td style="width:535px; vertical-align: top;">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td>
				</tr><!-- [COMPUTER] -->
				{SKILLS-ROW}<tr>
					<td style="width:210px; vertical-align: top;"><p>[SKILL]:</p></td>
					<td style="width:535px; vertical-align: top;">[DESCRIPTION]</td>
				</tr>{SKILLS-ROW}
			</table>
			<br />
			<!-- [SKILLS] -->
			<!-- [OTHERS] -->
			{OTHERS-ROW}<div style="  font-size: 16px;"><div class="title"><h2 style="background-color: #CCCCCC; text-transform: uppercase;">[TITLE]</h2></div></div>
			[CONTENT]
			{OTHERS-ROW}
			<br /><br />
			<!-- [OTHERS] -->
			<!-- [REFERENCE] -->
			<div class="title"><h2 style="background-color: #CCCCCC">REFERENCE</h2></div>
			<table  width="100%" border="0" cellspacing="0px">{REFERENCE-ROW}
					<!-- [NAME] --><tr><td style="width:210px; vertical-align: top;">Name:</td><td style="width:535px; vertical-align: top;">[NAME]</td></tr><!-- [NAME] -->
					<!-- [POSITION] --><tr><td style="width:210px; vertical-align: top;" >Position:</td><td style="width:535px; vertical-align: top;">[POSITION]</td></tr><!-- [POSITION] -->
					<!-- [ORGANIZATION] --><tr><td style="width:210px; vertical-align: top;">Organization:</td><td style="width:535px; vertical-align: top;">[ORGANIZATION]</td></tr><!-- [ORGANIZATION] -->
					<!-- [CONTACT] --><tr><td style="width:210px; vertical-align: top;" >Contact Number:</td><td style="width:535px; vertical-align: top;">[CONTACT]</td></tr><!-- [CONTACT] -->
					<!-- [EMAIL] --><tr><td style="width:210px; vertical-align: top;" >Email:</td><td style="width:535px; vertical-align: top;">[EMAIL]</td></tr><!-- [EMAIL] -->
				{REFERENCE-ROW}
			</table>
			<br />
			<!-- [REFERENCE] -->			
		</main>
	</body>
</html>