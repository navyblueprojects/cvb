<!DOCTYPE html>
<html>
	<head>
		<base href="https://cvbuilder.cpce-polyu.edu.hk/" >
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body id="template3" class="template">
		<main>
			<!-- [BASIC] -->
			<div class="title"><h2>Personal Particulars</h2></div>
			<table >
				<tr><td valign="top"><table>
						  <tr><td style="width:210px; height:20px; vertical-align: top;">Name:</td>
					  	      <td style="width:300px; height:20px; vertical-align: top;"  ><!-- [ENAME] -->[ENAME]<!-- [ENAME] --> <!-- [CNAME] -->([CNAME])<!-- [CNAME] --></td></tr>
			  	<!-- [ADDRESS] --><tr><td style="height:20px; vertical-align: top;">Address:</td><td >[ADDRESS]</td></tr><!-- [ADDRESS] -->
			  	<!-- [CONTACT] --><tr><td style="height:20px; vertical-align: top;">Contact Number:</td><td>[CONTACT]</td></tr><!-- [CONTACT] -->
				<!-- [EMAIL] --><tr><td style="height:20px; vertical-align: top;">Email:</td><td>[EMAIL]</td></tr><!-- [EMAIL] -->
				<!-- [GENDER] --><tr><td style="height:20px; vertical-align: top;">Gender:</td><td>[GENDER]</td></tr><!-- [GENDER] -->
				<!-- [AGE] --><tr><td style="height:20px; vertical-align: top;">Age:</td><td>[AGE]</td></tr><!-- [AGE] -->
				<!-- [DOB] --><tr><td style="vertical-align: top; height:20px;" >Date of Birth:</td><td valign="top">[DOB]</td></tr><!-- [DOB] -->
				<!-- [RELIGION] --><tr><td style="vertical-align: top; height:20px;">Religion:</td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
					  </table></td>
					  <td rowspan="7" align="right" valign="top"><!-- [PHOTO] --><img src="[PHOTO]" width="200"  /><!-- [PHOTO] --></td></tr>
				<tr><td>&nbsp;	</td></tr>
			</table>
			<!-- [BASIC] -->
			<!-- [EDUCATION] -->
			<div class="title"><h2>Education</h2></div>
			<table>
				{EDUCATION-ROW}<tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px;  vertical-align: top;" ><strong>[SCHOOL]</strong><BR>
											<i>[CLASS]</i><br >[YEAROFSTUDY]</td></tr>{EDUCATION-ROW}
			</table>
			<br /><br />
			<!-- [EDUCATION] -->
			<!-- [RESULT] -->
			<div class="title"><h2>Academic Attainment</h2></div>
			<table>
				<!-- [GPA] --><tr><td style="width:210px; padding-bottom: 14px; vertical-align: top;">[DATE]</td><td valign="top" style="width:510px; vertical-align: top; padding-bottom: 12px"><strong>[SCHOOL]</strong><BR>CGPA: [POINT]</td></tr><!-- [GPA] -->
				<!-- [CGPA] --><tr><td style="width:210px; padding-bottom: 14px; vertical-align: top;">[DATE]</td><td valign="top" style="width:510px; vertical-align: top; padding-bottom: 12px"><strong>[COLLEGE]</strong><BR>CGPA: [POINT]</td></tr><!-- [CGPA] -->
				{RESULT-ROW}<tr><td style="width:210px; padding-bottom: 14px; vertical-align: top;">[DATE]</td><td valign="top" style="width:510px; vertical-align: top;"><strong>[EXAM]</strong>
											<ul style="list-style: none"><li >[RESULT]</li></ul></td></tr>{RESULT-ROW}
			</table>
			<!-- [RESULT] -->
			<!-- [PRESULT] -->
			<table>
				{PRESULT-ROW}<tr><td style="width:210px; padding-bottom: 14px; vertical-align: top;">[DATE]</td><td valign="top" style="width:510px; vertical-align: top;"><strong>[EXAM]</strong><BR>
											<ul style="list-style: none"><li >[RESULT]</li></ul></td></tr>{PRESULT-ROW}
			</table>
			<br />
			<!-- [PRESULT] -->
			<!-- [WORK] -->
			<div class="title"><h2>Work Experience</h2></div>
			<table>
				{WORK-ROW}<tr><td style="width:210px; vertical-align: top;">[DATE]</td>
							  <td style="width:510px; vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  [POSITION]
											  <ul style="list-style: none"><li>[DUTIES] </li></ul>
											</td></tr>{WORK-ROW}
			</table>
			<br />
			<!-- [WORK] -->
			<!-- [INTERNSHIP] -->
			<div class="title"><h2>Internship Experience</h2></div>
			<table>
				{INTERNSHIP-ROW}<tr><td style="width:210px; vertical-align: top;">[DATE]</td>
									<td style="width:510px; vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  [POSITION]
											  <ul style="list-style: none"><li>[DUTIES] </li></ul>
											</td></tr>{INTERNSHIP-ROW}
			</table>
			<br />
			<!-- [INTERNSHIP] -->
			<!-- [EXACTIVITIES] -->
			<div class="title"><h2>Extra-Curricular Activities</h2></div>
			<table>
				{EXACTIVITIES-ROW}<tr><td style="width:210px; vertical-align: top;">[DATE]</td>
										<td style="width:510px; vertical-align: top;"><strong>[ORGANIZATION]</strong><br />
											  <i>[POSITION]</i>
											  <ul style="list-style: none"><li>[DUTIES]</li></ul>
											</td></tr>{EXACTIVITIES-ROW}
			</table>
			<br /><br />
			<!-- [EXACTIVITIES] -->
			<!-- [PROFESSIONAL] -->
			<div class="title"><h2>Professional Qualifications</h2></div>
			<table>
				{PROFESSIONAL-ROW}
				<tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
											  <ul style="list-style: none"><li>[GRADE]</li></ul>
											</td></tr> 
				{PROFESSIONAL-ROW}
			</table>
			<!-- [PROFESSIONAL] -->
			<!-- [OTHER_QUALI] -->
			<div class="title"><h2>Other Qualifications</h2></div>
			<table>
				{OTHER_QUALI-ROW}
				<tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
											  <ul style="list-style: none"><li>[GRADE]</li></ul>
											</td></tr>
				{OTHER_QUALI-ROW}
			</table>
			<!-- [OTHER_QUALI] -->
			<!-- [ACHIEVEMENTS] -->
			<div class="title"><h2>Achievements</h2></div>
			<table>
				{ACHIEVEMENTS-ROW}<tr><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;">[NAME], [ORGANIZATION]</td></tr>{ACHIEVEMENTS-ROW}
			</table>
			<br />
			<!-- [ACHIEVEMENTS] -->
			<!-- [SKILLS] -->
			<div class="title"><h2>Skills</h2></div>
			<table>
				<!-- [LANGUAGE] --><tr><td valign="top" style="width:210px; vertical-align: top;">Language Proficiency:</td><td style="width:510px; vertical-align: top;">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr><!-- [LANGUAGE] -->
				<!-- [COMPUTER] --><tr><td valign="top" style="width:210px; vertical-align: top;">Technical Skills:</td><td style="width:510px; vertical-align: top;">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td></tr><!-- [COMPUTER] -->
				{SKILLS-ROW}<tr ><td style="width:210px; padding-top:12px; vertical-align: top;">[SKILL]:</td><td style="width:510px;  padding-top:12px; vertical-align: top;">[DESCRIPTION]</td></tr>{SKILLS-ROW}
			</table>
			<br />
			<!-- [SKILLS] -->
			<!-- [OTHERS] -->
			{OTHERS-ROW}<div style=" font-family: dejavusans, Arial; font-size: 16px"><div class="title"><h2>[TITLE]</h2></div>[CONTENT]</div>{OTHERS-ROW}<br />
			<!-- [OTHERS] -->
			<!-- [REFERENCE] -->
			<div class="title"><h2>Reference</h2></div>
			<table>
				{REFERENCE-ROW}
					<tr><td style="width:210px; vertical-align: top;">Name:</td><td style="width:510px; vertical-align: top;">[NAME]</td></tr>
					<tr><td style="width:210px; vertical-align: top;">Position:</td><td style=" vertical-align: top;">[POSITION]</td></tr>
					<tr><td style="width:210px; vertical-align: top;">Organization:</td><td style=" vertical-align: top;">[ORGANIZATION]</td></tr>
					<tr><td style="width:210px; vertical-align: top;">Contact Number:</td><td style=" vertical-align: top;">[CONTACT]</td></tr>
					<tr><td style="width:210px; vertical-align: top;">Email:</td><td style=" vertical-align: top;">[EMAIL]</td></tr>
				{REFERENCE-ROW}
			</table><br />
			<br /><!-- [REFERENCE] -->
			<!-- [ETC] -->
			<!-- [EXPECTED_SALARY] --><h3>Expected Salary: <span>[EXPECTED_SALARY]</span></h3><!-- [EXPECTED_SALARY] -->
			<!-- [AVAILABILITY] --><h3>Availability: <span>[AVAILABILITY]</span></h3><!-- [AVAILABILITY] -->
			<!-- [ETC] -->
			
		</main>
	</body>
</html>