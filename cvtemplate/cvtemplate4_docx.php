<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body id="template4" class="template">
		<main>
		<!-- [BASIC] -->
		<style>
			h1 {font-family: Times; font-size: 26px; line-height: 32px; margin:0; padding: 0; }
			h2 {font-family: Times; font-size: 20px; margin:0; padding:  0; }
			p { margin: 0; }
			td { font-size: 16px; font-family: Times;  line-height: 20px; vertical-align: top; }
			ul { margin: -10px -30px; padding: 0; list-style: none  }
			li { margin: 0; padding: 0; padding-left: 20px; }
			.title h2 { border-bottom: 1px solid #000000; padding-bottom: 5px; margin-bottom: 5px; padding-top: 5px; margin-top: 5px;}
			hr { border: 1px solid #202020; }
		</style>
		<table width="100%" border="0" cellspacing="0px">
			<tr><td style="width:440px; " ><h1><!-- [ENAME] -->[ENAME]<!-- [ENAME] --> <!-- [CNAME] -->([CNAME])<!-- [CNAME] --></h1></td>
			<td style="width:300px; " ><table width="100%" border="0" cellspacing="0px">
				<!-- [ADDRESS] --><tr><td style="width:100px; ">Address:</td><td style="width:200px; ">[ADDRESS]</td></tr><!-- [ADDRESS] -->
				<!-- [CONTACT] --><tr><td style="width:100px; ">Contact:</td><td style="width:200px; ">[CONTACT]</td></tr><!-- [CONTACT] -->
				<!-- [EMAIL] --><tr><td style="width:100px; ">Email:</td><td style="width:200px; ">[EMAIL]</td></tr><!-- [EMAIL] -->
				<!-- [GENDER] --><tr><td style="width:100px; ">Gender:</td><td style="width:200px; ">[GENDER]</td></tr><!-- [GENDER] -->
				<!-- [AGE] --><tr><td style="width:100px; ">Age:</td><td style="width:200px; ">[AGE]</td></tr><!-- [AGE] -->
				<!-- [DOB] --><tr><td style="width:100px;" >Date of Birth:</td><td style="width:200px; ">[DOB]</td></tr><!-- [DOB] -->
				<!-- [RELIGION] --><tr><td style="width:100px;">Religion:</td><td style="width:200px; ">[RELIGION]</td></tr><!-- [RELIGION] -->
			</table></td>
		</tr></table><hr>
		<!-- [BASIC] -->
		<!-- [EDUCATION] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:600px; ">
					<div class="title"><h2>EDUCATION</h2></div>
					<table width="100%" border="0" cellspacing="0px">
						{EDUCATION-ROW}<tr><td style="width:180px; " >[DATE]</td>
										   <td style="width:420px; "><strong>[SCHOOL]</strong><BR>
													<i>[CLASS]</i><br >[YEAROFSTUDY]</td></tr>{EDUCATION-ROW}
					</table><br />
				</td>
				<td style="width:140px; " align="right"><img src="[PHOTO]" width="200" /></td>
			</tr>
		</table>
		<!-- [EDUCATION] -->
		<!-- [RESULT] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:540px; ">
					<div class="title" ><h2>ACADEMIC ATTAINMENT</h2></div>
				</td>
				<td style="width:200px; " align="right"></td>
			</tr>
			<tr><td colspan="2" style="width:740px;"><table style="width: 100%;" border="0" cellspacing="0px">
				<!-- [GPA] --><tr><td style="width:180px;  ">[DATE]</td>
								  <td style="width:560px; "><strong>[SCHOOL]</strong><br>CGPA: [POINT]</td></tr><!-- [GPA] -->
				<!-- [CGPA] --><tr><td style="width:180px; ">[DATE]</td>
								  <td style="width:560px; "><strong>[COLLEGE]</strong><br>CGPA: [POINT]</td></tr><!-- [CGPA] -->
				{RESULT-ROW}<tr><td style="width:180px; ">[DATE]</td>
								<td style="width:560px; "><strong>[EXAM]</strong>
								<ul><li>[RESULT]</li></ul></td></tr>{RESULT-ROW}
			</table></td></tr>
		</table>
		<!-- [RESULT] -->
		<!-- [PRESULT] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr><td style="width:740px; "><table style="width: 100%;" border="0" cellspacing="0px">
				{PRESULT-ROW}<tr><td style="width:180px; ">[DATE]</td>
								<td style="width:560px; "><strong>[EXAM]</strong>
										<ul><li>[RESULT]</li></ul></td></tr>{PRESULT-ROW}
				</table></td>
			</tr>
		</table>
		<!-- [PRESULT] -->
		<!-- [WORK] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:540px; ">
					<div class="title" ><h2>WORK EXPERIENCE</h2></div>
				</td>
				<td style="width:200px; " align="right"></td>
			</tr>
			<tr><td colspan="2" style="width:740px; "><table width="100%" border="0" cellspacing="0px">
					{WORK-ROW}<tr><td style="width:180px; padding-bottom: 14px; ">[DATE]</td>
								  <td style="width:560px; "><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
									  [POSITION]
									  <ul><li>[DUTIES]</li></ul>
									</td></tr>{WORK-ROW}
				</table></td></tr>
		</table>
		<!-- [WORK] -->
		<!-- [INTERNSHIP] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:740px; "><div class="title"><h2>INTERNSHIP EXPERIENCE</h2></div></td>
			</tr>
			<tr><td colspan="2"><table width="100%" border="0" cellspacing="0px">
				{INTERNSHIP-ROW}<tr><td style="width:180px; padding-bottom: 14px; ">[DATE]</td>
									<td style="width:560px; "><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  [POSITION]
											  <ul><li>[DUTIES]</li></ul>
											</td></tr>{INTERNSHIP-ROW}
			</table></td></tr>
		</table>
		<!-- [INTERNSHIP] -->
		<!-- [EXACTIVITIES] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:740px; ">
					<div class="title" ><h2>EXTRA-CURRICULAR ACTIVITIES</h2></div>
				</td>
			</tr>
			<tr><td colspan="2"><table width="100%" border="0" cellspacing="0px">
				{EXACTIVITIES-ROW}<tr><td style="width:180px; padding-bottom: 14px; ">[DATE]</td>
										<td style="width:560px; padding-bottom: 14px; "><strong>[ORGANIZATION]</strong><br />
											  [POSITION]
											  <ul><li>[DUTIES]</li></ul>
											</td></tr>{EXACTIVITIES-ROW}
			</table></td></tr>
		</table>
		<!-- [EXACTIVITIES] -->
		<!-- [PROFESSIONAL] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:740px; ">
					<div class="title" ><h2>PROFESSIONAL QUALIFICATIONS</h2></div>
				</td>
			</tr>
			<tr><td colspan="2">
				<table width="100%" border="0" cellspacing="0px">
						{PROFESSIONAL-ROW}<tr><td style="width:180px; padding-bottom: 14px; ">[DATE]</td>
											  <td style="width:560px; padding-bottom: 14px; "><strong>[NAME], [ORGANIZATION]</strong><br />
													  <ul><li>[GRADE]</li></ul>
													</td></tr>{PROFESSIONAL-ROW}
				</table>
			</td></tr>
		</table>
		<!-- [PROFESSIONAL] -->
		<!-- [OTHER_QUALI] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:740px; ">
					<div class="title" ><h2>OTHER QUALIFICATIONS</h2></div>
					</td>
			</tr>
			<tr><td colspan="2"><table width="100%" border="0" cellspacing="0px">
				{OTHER_QUALI-ROW}<tr><td style="width:180px; padding-bottom: 14px; ">[DATE]</td>
					 <td style="width:560px; padding-bottom: 14px; "><strong>[NAME], [ORGANIZATION]</strong><br />
							  <ul><li>[GRADE]</li></ul>
							</td></tr>{OTHER_QUALI-ROW}
				
			</table></td></tr>
		</table>
		<!-- [OTHER_QUALI] -->
		<!-- [ACHIEVEMENTS] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:740px; "> <div class="title" ><h2>ACHIEVEMENTS</h2></div> </td>
			</tr>
			<tr><td colspan="2"><table width="100%" border="0" cellspacing="0px">
				{ACHIEVEMENTS-ROW}<tr><td style="width:180px; padding-bottom: 14px; ">[DATE]</td>
									  <td style="width:560px; padding-bottom: 14px; "><strong>[NAME], [ORGANIZATION]</strong></td></tr>{ACHIEVEMENTS-ROW}
				
			</table></td></tr>
		</table>
		<!-- [ACHIEVEMENTS] -->
		<!-- [SKILLS] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:740px; "><div class="title" ><h2>SKILLS</h2></div></td>	
			</tr>
			<tr>
				<td colspan="2"><table width="100%" border="0" cellspacing="0px">
					<tr><td style="width:180px; padding-bottom: 14px; ">Language Proficiency:</td>
						<td style="width:560px; padding-bottom: 14px; ">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr>
					<tr><td style="width:180px; padding-bottom: 14px; ">Technical Skills:</td>
						<td style="width:560px; padding-bottom: 14px; ">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td></tr>
					{SKILLS-ROW}<tr ><td style="width:180px; padding-bottom: 14px; "><p>[SKILL]:</p></td>
								<td style="width:560px; padding-bottom: 14px; ">[DESCRIPTION]</td></tr>{SKILLS-ROW}
				</table></td>
			</tr>
		</table>
		<!-- [SKILLS] -->
		<!-- [OTHERS] -->
		<table width="100%" border="0" cellspacing="0px">
			{OTHERS-ROW}<tr>
				<td style="width:740px; font-family: Times; font-size: 16px;"><div class="title" ><h2>[TITLE]</h2></div>
					[CONTENT]</td>	
			</tr>
			{OTHERS-ROW}
		</table>
		<!-- [OTHERS] -->
		<!-- [REFERENCE] -->
		<table width="100%" border="0" cellspacing="0px">
			<tr>
				<td style="width:740px; ">
			<div class="title" ><h2>REFERENCE</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{REFERENCE-ROW}
				<tr><td style="width:180px; ">Name:</td><td style="width:560px;">[NAME]</td></tr>
				<tr><td style="width:180px; ">Position:</td><td style="width:560px;">[POSITION]</td></tr>
				<tr><td style="width:180px; ">Organization:</td><td style="width:560px;">[ORGANIZATION]</td></tr>
				<tr><td style="width:180px; ">Contact Number:</td><td style="width:560px;">[CONTACT]</td></tr>
				<tr><td style="width:180px; ">Email:</td><td style="width:560px;">[EMAIL]</td></tr>
				<tr><td style="width:180px; ">&nbsp;</td><td style="width:560px; ">&nbsp;</td></tr>
				{REFERENCE-ROW}
			</table></td>
			</tr>
		</table>
		<!-- [REFERENCE] -->
		<!-- [ETC] -->
		<!-- [EXPECTED_SALARY] --><div class="title" style="width:740px; vertical-align: top; "><h2>EXPECTED SALARY:</h2></div><div style="font-family: Times; font-size: 16px">[EXPECTED_SALARY]</div><BR><!-- [EXPECTED_SALARY] -->
		<!-- [AVAILABILITY] --><div class="title" style="width:740px; vertical-align: top;"><h2>AVAILABILITY:</h2></div><div style="font-family: Times; font-size: 16px">[AVAILABILITY]</div><BR><!-- [AVAILABILITY] -->
		<!-- [ETC] -->
		</main>
	</body>
</html>