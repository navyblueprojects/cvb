<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body id="template3" class="template">
		<main>
			<div >
			<!-- [BASIC] -->
			<style>
				td { font-size: 14px;  line-height: 18px; vertical-align: top; }
				ul { margin: -10px -30px; padding: 0; list-style: none; }
				p { margin: 0; }
				li { margin: 0; padding: 0; padding-left: 10px; list-style: none; }
				.title h2 { font-size: 16px; margin:0; padding:  0; border-bottom: 1px solid #406696; padding-bottom: 5px; margin-bottom: 5px; padding-top: 5px; margin-top: 5px; color: #406696; }
				h3 {  font-size: 16px; color: #406696; }
				h3 span {  font-size: 14px; color: #000000; }
			</style>
			<div class="title"><h2>Personal Particulars</h2></div>
			<table width="100%" border="0" cellspacing="0px" >
				<tr><td style="width:210px;">Name:</td><td style="width:300px; "  ><!-- [ENAME] -->[ENAME]<!-- [ENAME] --> <!-- [CNAME] -->([CNAME])<!-- [CNAME] --></td>
					<td style="width:200px; " rowspan="6" align="right" valign="top"><!-- [PHOTO] --><img src="[PHOTO]" width="200"  /><!-- [PHOTO] --></td></tr>
				<!-- [ADDRESS] --><tr><td style="width:210px;">Address:</td><td style="width:300px; ">[ADDRESS]</td></tr><!-- [ADDRESS] -->
				<!-- [CONTACT] --><tr><td style="width:210px;">Contact Number:</td><td style="width:300px; ">[CONTACT]</td></tr><!-- [CONTACT] -->
				<!-- [EMAIL] --><tr><td style="width:210px;">Email:</td><td style="width:300px; ">[EMAIL]</td></tr><!-- [EMAIL] -->
				<!-- [GENDER] --><tr><td style="width:210px;">Gender:</td><td style="width:300px; ">[GENDER]</td></tr><!-- [GENDER] -->
				<!-- [AGE] --><tr><td style="width:210px;">Age:</td><td style="width:300px; ">[AGE]</td></tr><!-- [AGE] -->
				<!-- [DOB] --><tr><td style="width:210px;" >Date of Birth:</td><td valign="top">[DOB]</td></tr><!-- [DOB] -->
				<!-- [RELIGION] --><tr><td style="width:210px;">Religion:</td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
			</table>

			<br />
			<!-- [BASIC] -->
			<!-- [EDUCATION] -->
			<div class="title"><h2>Education</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{EDUCATION-ROW}<tr><td style="width:210px; ">[DATE]</td><td style="width:500px; " ><strong>[SCHOOL]</strong><BR>
											<i>[CLASS]</i><br >[YEAROFSTUDY]</td></tr>{EDUCATION-ROW}
			</table>
			<!-- [EDUCATION] -->
			<!-- [RESULT] -->
			<div class="title"><h2 >Academic Attainment</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				<!-- [GPA] --><tr><td style="width:210px; ">[DATE]</td><td style="width:500px; padding-bottom: 14px; "><strong>[SCHOOL]</strong><br>CGPA: [POINT]</td></tr><!-- [GPA] -->
				<!-- [CGPA] --><tr><td style="width:210px; ">[DATE]</td><td style="width:500px; padding-bottom: 14px; "><strong>[COLLEGE]</strong><br>CGPA: [POINT]</td></tr><!-- [CGPA] -->
				{RESULT-ROW}<tr><td style="width:210px; "  valign="top">[DATE]</td><td style="width:500px; "><strong>[EXAM]</strong>
											<ul><li>[RESULT]</li></ul></td></tr>{RESULT-ROW}
			</table>
			<!-- [RESULT] -->
			<!-- [PRESULT] -->
			<table width="100%" border="0" cellspacing="0px">
				{PRESULT-ROW}<tr><td style="width:210px; "  valign="top">[DATE]</td><td style="width:500px; "><strong>[EXAM]</strong>
											<ul><li>[RESULT]</li></ul></td></tr>{PRESULT-ROW}
			</table>
			<!-- [PRESULT] -->
			<!-- [WORK] -->
			<div class="title"><h2 >Work Experience</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{WORK-ROW}<tr><td style="width:210px;">[DATE]</td>
							  <td style="width:500px;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  [POSITION]
											  <ul><li>[DUTIES] </li></ul>
											</td></tr>{WORK-ROW}
			</table>
			<br />
			<!-- [WORK] -->
			<!-- [INTERNSHIP] -->
			<div class="title"><h2 >Internship Experience</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{INTERNSHIP-ROW}<tr><td style="width:210px;">[DATE]</td>
									<td style="width:500px;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  [POSITION]
											  <ul><li>[DUTIES] </li></ul>
											</td></tr>{INTERNSHIP-ROW}
			</table>
			<br />
			<!-- [INTERNSHIP] -->
			<!-- [EXACTIVITIES] -->
			<div class="title"><h2 >Extra-Curricular Activities</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{EXACTIVITIES-ROW}<tr><td style="width:210px;">[DATE]</td>
										<td style="width:500px;"><strong>[ORGANIZATION]</strong><br />
											  <i>[POSITION]</i>
											  <ul><li>[DUTIES]</li></ul>
											</td></tr>{EXACTIVITIES-ROW}
			</table>
			<br />
			<!-- [EXACTIVITIES] -->
			<!-- [PROFESSIONAL] -->
			<div class="title"><h2 >Professional Qualifications</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{PROFESSIONAL-ROW}
				<tr><td style="width:210px;">[DATE]</td><td style="width:500px;"><strong>[NAME], [ORGANIZATION]</strong>
											  <ul><li>[GRADE]</li></ul>
											</td></tr>
				{PROFESSIONAL-ROW}
			</table>
			<br />
			<!-- [PROFESSIONAL] -->
			<!-- [OTHER_QUALI] -->
			<div class="title"><h2 >Other Qualifications</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{OTHER_QUALI-ROW}
				<tr><td style="width:210px;">[DATE]</td><td style="width:500px;"><strong>[NAME], [ORGANIZATION]</strong>
											  <ul><li>[GRADE]</li></ul>
											</td></tr>
				{OTHER_QUALI-ROW}
			</table>
			<br />
			<!-- [OTHER_QUALI] -->
			<!-- [ACHIEVEMENTS] -->
			<div class="title"><h2 >Achievements</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{ACHIEVEMENTS-ROW}<tr><td style="width:210px;">[DATE]</td><td style="width:500px; vertical-align: top;">[NAME], [ORGANIZATION]</td></tr>{ACHIEVEMENTS-ROW}
			</table>
			<br />
			<!-- [ACHIEVEMENTS] -->
			<!-- [SKILLS] -->
			<div class="title"><h2 >Skills</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				<tr><td valign="top" style="width:210px; ">Language Proficiency:</td><td style="width:500px; ">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr>
				<tr><td valign="top" style="width:210px; ">Technical Skills:</td><td style="width:500px;">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td></tr>
				{SKILLS-ROW}<tr ><td style="width:210px; "><p>[SKILL]:</p></td><td style="width:500px; padding-top:12px; ">[DESCRIPTION]</td></tr>{SKILLS-ROW}
			</table>
			<!-- [SKILLS] -->
			<!-- [OTHERS] -->			
			{OTHERS-ROW}<div style="font-size: 16px"><div class="title"><h2>[TITLE]</h2></div>
			<p>[CONTENT]</p></div>
			{OTHERS-ROW}
			<!-- [OTHERS] -->
			<!-- [REFERENCE] -->
			<div class="title"><h2 >Reference</h2></div>
			<table width="100%" border="0" cellspacing="0px">
				{REFERENCE-ROW}
					<tr><td style="width:210px;">Name:</td><td style="width:500px; ">[NAME]</td></tr>
					<tr><td style="width:210px;">Position:</td><td style="width:500px;">[POSITION]</td></tr>
					<tr><td style="width:210px;">Organization:</td><td style="width:500px;">[ORGANIZATION]</td></tr>
					<tr><td style="width:210px;">Contact Number:</td><td style="width:500px;">[CONTACT]</td></tr>
					<tr><td style="width:210px;">Email:</td><td style="width:500px;">[EMAIL]</td></tr>
				{REFERENCE-ROW}
			</table>
			<br />
			<!-- [REFERENCE] -->
			<!-- [ETC] -->
			<!-- [EXPECTED_SALARY] --><h3>Expected Salary: <span>[EXPECTED_SALARY]</span></h3><!-- [EXPECTED_SALARY] -->
			<!-- [AVAILABILITY] --><h3>Availability: <span>[AVAILABILITY]</span></h3><!-- [AVAILABILITY] -->
			<!-- [ETC] -->
		</div>
		</main>
	</body>
</html>