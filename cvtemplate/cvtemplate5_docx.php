<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body id="template5" class="template">
		<main>
			<!-- [BASIC] -->
			<style>
				h1 {text-align: center;}
				h1, h2, table, td {font-family: Arial;}
				td { font-size: 16px; vertical-align: top;}
				li { margin: 0; padding: 0; padding-left: 20px; }
				ul { list-style: none  }
				hr { border: 1px solid #202020; }
				p {margin: 0;}
			</style>
			<h1><!-- [ENAME] -->[ENAME]<!-- [ENAME] --> <!-- [CNAME] -->([CNAME])<!-- [CNAME] --></h1>
			<table width="100%" border="0" cellspacing="0px">
				<!-- [ADDRESS] --><tr><td valign="top" align="center" colspan="2"><strong>Address</strong> [ADDRESS]</td></tr><!-- [ADDRESS] -->
				<tr><!-- [CONTACT] --><td valign="top" align="right" width="50%"><strong>Contact Number</strong> [CONTACT] </td><!-- [CONTACT] -->
					<!-- [EMAIL] --><td align="left" width="50%"> <strong>Email</strong> [EMAIL]</td><!-- [EMAIL] --></tr>
				<tr><td valign="top" align="center" colspan="2"><!-- [GENDER] --><strong>Gender</strong> [GENDER]<!-- [GENDER] -->
					 <!-- [AGE] --> <strong>Age</strong> [AGE]<!-- [AGE] --></td></tr>
				<!-- [DOB] --><tr><td align="right" style="vertical-align: top; " > <strong>Date of Birth: </strong></td><td valign="top">[DOB]</td></tr><!-- [DOB] -->
				<!-- [RELIGION] --><tr><td align="right" style="vertical-align: top; "> <strong>Religion: </strong></td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
			</table>
			<hr />
			<!-- [BASIC] -->
			<!-- [EDUCATION] -->
			<h2>EDUCATION</h2>
			<table width="100%" border="0" cellspacing="0px">
				{EDUCATION-ROW}<tr><td valign="top" style="width:500px;"><b>[SCHOOL]</b><BR><i>[CLASS]</i><br >[YEAROFSTUDY]<BR></td><td valign="top" style="width:200px; "  align="right">[DATE]</td></tr>{EDUCATION-ROW}
			</table>
			<p></p>
			<!-- [EDUCATION] -->
			<!-- [RESULT] -->
			<h2>ACADEMIC ATTAINMENT</h2>
			<table width="100%" border="0" cellspacing="0px">
				<!-- [GPA] --><tr><td style="width:500px; "><strong>[SCHOOL]</strong><br>CGPA: [POINT]</td>
								  <td style="width:200px; padding-bottom: 14px; " align="right">[DATE]</td></tr><!-- [GPA] -->
				<!-- [CGPA] --><tr><td style="width:500px; "><strong>[COLLEGE]</strong><br>CGPA: [POINT]</td>
									<td style="width:200px; padding-bottom: 14px; " align="right">[DATE]</td></tr><!-- [CGPA] -->
				{RESULT-ROW}<tr><td valign="top"><strong>[EXAM]</strong>
						<ul><li>[RESULT]</li></ul></td><td valign="top"  align="right">[DATE]</td></tr>{RESULT-ROW}
			</table>
			<!-- [RESULT] -->
			<!-- [PRESULT] -->
			<table width="100%" border="0" cellspacing="0px">
				{PRESULT-ROW}<tr><td valign="top"><strong>[EXAM]</strong>
						<ul><li>[RESULT]</li></ul></td><td valign="top"  align="right">[DATE]</td></tr>{PRESULT-ROW}
			</table>
			<!-- [PRESULT] -->
			<!-- [WORK] -->
			<h2>WORK EXPERIENCE</h2>
			<table width="100%" border="0" cellspacing="0px">
				{WORK-ROW}<tr><td valign="top" width="470"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
						  <i>[POSITION]</i>
						  <ul><li>[DUTIES]</li></ul>
						</td><td valign="top" width="270" align="right">[DATE]</td></tr>{WORK-ROW}
				
			</table>
			<!-- [WORK] -->
			<!-- [INTERNSHIP] -->
			<h2>INTERNSHIP EXPERIENCE</h2>
			<table width="100%" border="0" cellspacing="0px">
				{INTERNSHIP-ROW}<tr><td valign="top" width="470"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
						  <i>[POSITION]</i>
						  <ul><li>[DUTIES]</li></ul>
						</td><td valign="top" width="270" align="right">[DATE]</td></tr>{INTERNSHIP-ROW}
			</table>
			<!-- [INTERNSHIP] -->
			<!-- [EXACTIVITIES] -->
			<h2>EXTRA-CURRICULAR ACTIVITIES</h2>
			<table  width="100%" border="0" cellspacing="0px">{EXACTIVITIES-ROW}
				<tr><td valign="top" width="470"><strong>[ORGANIZATION] </strong><br />
						  [POSITION]
						  <ul><li>[DUTIES]</li></ul>
						</td><td valign="top" width="270"  align="right">[DATE]</td></tr>
				{EXACTIVITIES-ROW}
			</table>
			<!-- [EXACTIVITIES] -->
			<!-- [PROFESSIONAL] -->
			<h2>PROFESSIONAL QUALIFICATIONS</h2>
			<table  width="100%" border="0" cellspacing="0px">
				{PROFESSIONAL-ROW}
				<tr><td valign="top"  width="470"><strong>[NAME], [ORGANIZATION]</strong><br />
					  <ul><li>[GRADE]</li></ul>
					</td><td valign="top"  width="270" align="right">[DATE]</td></tr>
				{PROFESSIONAL-ROW}
			</table>
			<!-- [PROFESSIONAL] -->
			<!-- [OTHER_QUALI] -->
			<h2>OTHER QUALIFICATIONS</h2>
			<table width="100%" border="0" cellspacing="0px">
				{OTHER_QUALI-ROW}
				<tr><td valign="top"><strong>[NAME], [ORGANIZATION]</strong><br />
					  <ul><li>[GRADE]</li></ul>
					</td><td valign="top" align="right">[DATE]</td></tr>
				{OTHER_QUALI-ROW}
			</table>
			<!-- [OTHER_QUALI] -->
			<!-- [ACHIEVEMENTS] -->
			<h2>ACHIEVEMENTS</h2>
			<table width="100%" border="0" cellspacing="0px">
				{ACHIEVEMENTS-ROW}<tr><td valign="top" width="470"><b>[NAME], [ORGANIZATION]</b></td><td valign="top" width="270" align="right">[DATE]</td></tr>{ACHIEVEMENTS-ROW}
			</table>
			<!-- [ACHIEVEMENTS] -->
			<!-- [SKILLS] -->
			<h2>SKILLS</h2>
			<table width="100%" border="0" cellspacing="0px">
				<tr><td valign="top" width="185">Language Proficiency:</td><td valign="top" width="555">{LANGUAGE-ROW}<u><b>[LANGUAGE]:</b></u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr>
				<tr><td valign="top" width="185">Technical Skills:</td><td valign="top" width="555">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td></tr>
				{SKILLS-ROW}<tr ><td style="width:30%; vertical-align: top;"><br>[SKILL]:<br/></td><td valign="top"  width="555"><br>[DESCRIPTION]<br></td></tr>{SKILLS-ROW}
			</table>
			<!-- [SKILLS] -->
			<!-- [OTHERS] -->
			{OTHERS-ROW}<div style="font-family: Arial; font-size: 16px;"><h2 style="text-transform: uppercase">[TITLE]</h2> 
			<table>
				<tr><td valign="top">[CONTENT]</td></tr>
			</table></div>
			{OTHERS-ROW}
			<!-- [OTHERS] -->
			<!-- [REFERENCE] -->
			<h2>REFERENCE</h2>
			<table width="100%" border="0" cellspacing="0px">{REFERENCE-ROW}
				<tr><td valign="top" width="185">Name:</td><td valign="top"  width="555">[NAME]</td></tr>
				<tr><td valign="top" width="185">Position:</td><td valign="top"  width="555">[POSITION]</td></tr>
				<tr><td valign="top" width="185">Organization:</td><td valign="top"  width="555">[ORGANIZATION]</td></tr>
				<tr><td valign="top" width="185">Contact Number:</td><td valign="top"  width="555">[CONTACT]</td></tr>
				<tr><td valign="top" width="185">Email:</td><td valign="top" width="555">[EMAIL]</td></tr>
			{REFERENCE-ROW}</table>
			<!-- [REFERENCE] -->
			<!-- [ETC] -->
			<!-- [EXPECTED_SALARY] --><h2>EXPECTED SALARY:</h2><span style="font-family: Arial">[EXPECTED_SALARY]</span><!-- [EXPECTED_SALARY] -->
			<!-- [AVAILABILITY] --><h2>AVAILABILITY:</h2><span style="font-family: Arial">[AVAILABILITY]</span><!-- [AVAILABILITY] -->
			<!-- [ETC] -->
			
		</main>
	</body>
</html>