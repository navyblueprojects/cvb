<page>
<page_header>
	<base href="https://cvbuilder.cpce-polyu.edu.hk/" >
	<link rel="stylesheet" href="css/style.css" />
</page_header>
<body id="template1" class="template">
<main>
	<!-- [BASIC] -->
	<style>
		ul { list-style: none; padding: 0;  }
		p { margin: 0; }
		li { margin: 0; padding: 0; padding-left: 20px; }
	</style>
	<h1><!-- [ENAME] -->[ENAME] <!-- [ENAME] --><span style="font-family: cid0ct"><!-- [CNAME] -->([CNAME])<!-- [CNAME] --></span></h1>
	<table style=" font-family: Times">
		<tr><td valign="top" style="width:530px;  vertical-align: top;"><table >
		<!-- [ADDRESS] --><tr><td valign="top" style="width:200px; vertical-align: top;" >Address:</td><td style="width:330px; vertical-align: top;">[ADDRESS]</td></tr><!-- [ADDRESS] -->
		<!-- [CONTACT] --><tr><td valign="top" >Contact Number:</td><td valign="top">[CONTACT]</td></tr><!-- [CONTACT] -->
		<!-- [EMAIL] --><tr><td valign="top">Email:</td><td valign="top">[EMAIL]</td></tr><!-- [EMAIL] -->
		<!-- [GENDER] --><tr><td valign="top">Gender:</td><td valign="top">[GENDER]</td></tr><!-- [GENDER] -->
		<!-- [AGE] --><tr><td valign="top">Age:</td><td valign="top">[AGE]</td></tr><!-- [AGE] -->
		<!-- [DOB] --><tr><td valign="top">Date of Birth:</td><td valign="top">[DOB]</td></tr><!-- [DOB] -->
		<!-- [RELIGION] --><tr><td valign="top">Religion:</td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
		</table></td><td  style="text-align: right; width:180px;   vertical-align: top; "><!-- [PHOTO] --><img src="[PHOTO]" width="180" /><!-- [PHOTO] --></td></tr>
	</table>
	<br /> <br /> <br />
	<!-- [BASIC] -->
	<!-- [EDUCATION] -->
	<div class="title"><h2 >EDUCATION</h2></div>
	<table style="width: 100%; ">
		{EDUCATION-ROW}<tr style="font-family:  Times">
			<td valign="top" style="width:200px; vertical-align: top;">[DATE]</td>
			<td valign="top" style="width:520px;"><b>[SCHOOL]</b><BR><i>[CLASS]</i><br >[YEAROFSTUDY]</td>
		</tr>{EDUCATION-ROW}
	</table>
	<br /> <br /> <br />
	<!-- [EDUCATION] -->
	<!-- [RESULT] -->
	<div class="title"><h2 >ACADEMIC ATTAINMENT</h2></div>
	<table>
		<!-- [GPA] --><tr style=" font-family: Times;">
			<td valign="top" style="width:200px; vertical-align: top;">[DATE]</td>
			<td valign="top" style="width:520px;"><b>[SCHOOL]</b><BR> CGPA: [POINT]<BR></td>
		</tr><!-- [GPA] -->
		<!-- [CGPA] --><tr style=" font-family: Times;">
			<td valign="top" style="width:200px; vertical-align: top;">[DATE]</td>
			<td valign="top" style="width:520px; vertical-align: top;"><b>[COLLEGE]</b><BR>CGPA: [POINT]<br><br></td></tr><!-- [CGPA] -->
		{RESULT-ROW}<tr style="font-family: Times">
			<td valign="top" style="width:200px; vertical-align: top;">[DATE]</td>
			<td valign="top" style="width:520px; vertical-align: top;"><strong>[EXAM]</strong><BR><ul><li>[RESULT]</li></ul></td>
		</tr>{RESULT-ROW}
	</table>		
	<!-- [RESULT] -->
	<!-- [PRESULT] -->
	<table>
		{PRESULT-ROW}<tr style="font-family: Times">
			<td valign="top" style="width:200px; vertical-align: top;" >[DATE]</td>
			<td valign="top" style="width:520px; vertical-align: top;"><strong>[EXAM]</strong><BR><ul><li>[RESULT]</li></ul></td>
		</tr>{PRESULT-ROW}
	</table>
	<br /> <br />
	<!-- [PRESULT] -->
	<!-- [WORK] -->
	<div class="title"><h2 >WORK EXPERIENCE</h2></div>
	<table>		
		{WORK-ROW}<tr style="font-family: Times"><td style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:520px;"><strong>[ORGANIZATION]  ([EMPLOYMENT])</strong><br />[POSITION] <ul><li>[DUTIES] </li></ul></td></tr>{WORK-ROW}
	</table>
	<br /> <br />
	<!-- [WORK] -->
	<!-- [INTERNSHIP] -->
	<div class="title"><h2 >INTERNSHIP EXPERIENCE</h2></div>
	<table>
		{INTERNSHIP-ROW}<tr style=" font-family: Times"><td style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:520px;"><strong>[ORGANIZATION]  ([EMPLOYMENT])</strong><br />[POSITION] <ul><li>[DUTIES] </li></ul></td></tr>{INTERNSHIP-ROW}
	</table>
	<br /> <br /> <br />
	<!-- [INTERNSHIP] -->
	<!-- [EXACTIVITIES] -->
	<div class="title"><h2 >EXTRA-CURRICULAR ACTIVITIES</h2></div>
	<table>
		{EXACTIVITIES-ROW}
		<tr style=" font-family: Times"><td  style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:520px;"><strong>[ORGANIZATION]</strong><br />[POSITION] <ul><li>[DUTIES] </li></ul></td></tr>
		{EXACTIVITIES-ROW}
	</table>
	<br /> <br /> <br />
	<!-- [EXACTIVITIES] -->
	<!-- [PROFESSIONAL] -->
	<div class="title"><h2 >PROFESSIONAL QUALIFICATIONS</h2></div>
	<table>
		{PROFESSIONAL-ROW}
		<tr style=" font-family: Times;"><td  style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:520px;"><strong>[NAME], [ORGANIZATION]</strong><br /> <ul><li>[GRADE]</li></ul></td></tr>
		{PROFESSIONAL-ROW}
	</table>
	<br /> <br /> <br />
	<!-- [PROFESSIONAL] -->
	<!-- [OTHER_QUALI] -->
	<div class="title"><h2 >OTHER QUALIFICATIONS</h2></div>
	<table>
		{OTHER_QUALI-ROW}<tr style=" font-family: Times;"><td  style="width:200px; vertical-align: top;">[DATE]</td><td valign="top" style="width:520px;"><strong>[NAME], [ORGANIZATION]</strong><br /><ul><li>[GRADE]</li></ul></td></tr>{OTHER_QUALI-ROW}
	</table>
	<br /> <br /> <br />
	<!-- [OTHER_QUALI] -->
	<!-- [ACHIEVEMENTS] -->
	<div class="title"><h2 >ACHIEVEMENTS</h2></div>
	<table>		
		{ACHIEVEMENTS-ROW}<tr style=" font-family: Times;"><td style="width:200px; vertical-align: top;">[DATE]</td><td valign="top">[NAME], [ORGANIZATION]</td></tr>{ACHIEVEMENTS-ROW}
	</table>
	<br /> <br /> <br />
	<!-- [ACHIEVEMENTS] -->
	<!-- [SKILLS] -->
	<div class="title"><h2 >SKILLS</h2></div>
	<table>
		<tr style=" font-family: Times"><td style="width:200px; vertical-align: top;">Language Proficiency:</td><td valign="top">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr>
	</table>
	<table>
		<tr style=" font-family: Times"><td style="width:200px; vertical-align: top;">Technical Skills:</td><td valign="top">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td></tr>
	</table>
	<table>
		{SKILLS-ROW}<tr style=" font-family: Times; "><td style="width:200px; vertical-align: top;"><br>[SKILL]:<br/></td><td valign="top"><br>[DESCRIPTION]<br></td></tr>{SKILLS-ROW}
	</table>
	<br /> <br /> <br />
	<!-- [SKILLS] -->
	<!-- [OTHERS] -->
		{OTHERS-ROW}<div class="title"><h2 style=" vertical-align: top; text-transform: uppercase">[TITLE]</h2></div><div style="font-family: Times; font-size: 16px;">[CONTENT]</div><br />{OTHERS-ROW}
	<br />
	<!-- [OTHERS] -->
	<!-- [REFERENCE] -->
	<div class="title"><h2 >REFERENCE</h2></div>
	<table>{REFERENCE-ROW}
		<tr style=" font-family: Times"><td style="width:200px; vertical-align: top;">Name:</td><td valign="top">[NAME]</td></tr>
		<tr style=" font-family: Times"><td >Position:</td><td valign="top">[POSITION]</td></tr>
		<tr style=" font-family: Times"><td >Organization:</td><td valign="top">[ORGANIZATION]</td></tr>
		<tr style=" font-family: Times"><td >Contact Number:</td><td valign="top">[CONTACT]</td></tr>
		<tr style=" font-family: Times"><td >Email:</td><td valign="top">[EMAIL]</td></tr>
		<tr style=" font-family: Times"><td >&nbsp;</td><td valign="top">&nbsp;</td></tr>
	{REFERENCE-ROW}</table>	
	<br /> <br /> 
	<!-- [REFERENCE] -->
	<!-- [ETC] -->
	<!-- [EXPECTED_SALARY] --><h3 >EXPECTED SALARY: <span  style="font-size: 16px; font-family: Times;">[EXPECTED_SALARY]</span></h3><!-- [EXPECTED_SALARY] -->
	<!-- [AVAILABILITY] --><h3 >AVAILABILITY: <span style="font-size: 16px; font-family: Times;">[AVAILABILITY]</span></h3><!-- [AVAILABILITY] -->
	<!-- [ETC] -->
	<br />
	
</main>
</body>
</page>