<page>
<page_header>
	<base href="https://cvbuilder.cpce-polyu.edu.hk/" >
	<link rel="stylesheet" href="css/style.css" />
</page_header>
<body id="template2" class="template">
		<main>
			<!-- [BASIC] -->
			<style>
				ul { list-style: none; padding: 0;  }
				p { margin: 0; }
				li { margin: 0; padding: 0; padding-left: 20px; }
			</style>
			<div class="title"><h2>PERSONAL PARTICULARS</h2></div>
			<table  >
				<tr><td style="width:510px; vertical-align: top;">
				<table style=" font-family: dejavusans, Tahoma;">
				<!-- [ENAME] --><tr><td  style="width:210px; height:18px; vertical-align: top;" >Name:</td><td valign="top" style="width:300px; vertical-align: top;" >[ENAME] <!-- [CNAME] --><span style="font-family: cid0ct;">([CNAME])</span><!-- [CNAME] --></td></tr><!-- [ENAME] -->
				<!-- [ADDRESS] --><tr><td style="vertical-align: top; height:18px;">Address:</td><td valign="top">[ADDRESS]</td></tr><!-- [ADDRESS] -->
				<!-- [CONTACT] --><tr><td style="vertical-align: top; height:18px;">Contact Number:</td><td valign="top">[CONTACT]</td></tr><!-- [CONTACT] -->
				<!-- [EMAIL] --><tr><td style="vertical-align: top; height:18px;">Email:</td><td valign="top">[EMAIL]</td></tr><!-- [EMAIL] -->
				<!-- [GENDER] --><tr><td style="vertical-align: top; height:18px;">Gender:</td><td valign="top">[GENDER]</td></tr><!-- [GENDER] -->
				<!-- [AGE] --><tr><td style="vertical-align: top; height:18px;">Age:</td><td valign="top">[AGE]</td></tr><!-- [AGE] -->
				<!-- [DOB] --><tr><td style="vertical-align: top; height:18px;" >Date of Birth:</td><td valign="top">[DOB]</td></tr><!-- [DOB] -->
				<!-- [RELIGION] --><tr><td style="vertical-align: top; height:18px;">Religion:</td><td valign="top">[RELIGION]</td></tr><!-- [RELIGION] -->
				<!-- [AVAILABILITY] --><tr><td style="vertical-align: top; height:18px;">Availability:</td><td valign="top">[AVAILABILITY]</td></tr><!-- [AVAILABILITY] -->
				<!-- [EXPECTED_SALARY] --><tr><td style="vertical-align: top; height:18px;">Expected Salary:</td><td valign="top">[EXPECTED_SALARY]</td></tr><!-- [EXPECTED_SALARY] -->
			</table></td>
				<td valign="top" style="width:200px; vertical-align: top;"><!-- [PHOTO] --><img src="[PHOTO]"  width="200"  /><!-- [PHOTO] --></td>
			</tr>
			</table>
			<br />
			<!-- [BASIC] -->
			<!-- [EDUCATION] -->
			<div class="title"><h2>EDUCATION</h2></div>
			<table>
				{EDUCATION-ROW}<tr style=" font-family: dejavusans, Tahoma"><td valign="top" style="width:210px; vertical-align: top;">[DATE]</td><td valign="top" style="width:510px; vertical-align: top; "><b>[SCHOOL]</b><BR><i>[CLASS]</i><br >[YEAROFSTUDY]</td></tr>{EDUCATION-ROW}
			</table>
			<br />
			<!-- [EDUCATION] -->
			<!-- [RESULT] -->
			<div class="title"><h2>ACADEMIC ATTAINMENT</h2></div>
			<table style=" font-family: dejavusans, Tahoma">
				<!-- [GPA] --><tr><td style="width:210px;   padding-bottom: 14px; vertical-align: top;">[DATE]</td><td valign="top" style="width:510px; vertical-align: top; padding-bottom: 12px;"><strong>[SCHOOL]</strong><BR>CGPA: [POINT]</td></tr><!-- [GPA] -->
				<!-- [CGPA] --><tr><td style="padding-bottom: 14px;  vertical-align: top;">[DATE]</td><td valign="top" style="width:510px; vertical-align: top; padding-bottom: 12px;"><strong>[COLLEGE]</strong><br>CGPA: [POINT]</td></tr><!-- [CGPA] -->
				{RESULT-ROW}<tr><td style=" vertical-align: top;">[DATE]</td><td ><strong>[EXAM]</strong><br>
											<ul style="list-style: none;"><li>[RESULT]</li></ul></td></tr>{RESULT-ROW}
			</table>
			<!-- [RESULT] -->
			<!-- [PRESULT] -->
			<table>
				{PRESULT-ROW}<tr style="font-family: dejavusans, Tahoma"><td valign="top" style="width:210px; vertical-align: top;" >[DATE]</td><td valign="top" style="width:510px;"><strong>[EXAM]</strong><br><ul style="list-style: none;"><li>[RESULT]</li></ul></td></tr>{PRESULT-ROW}
			</table>
			 <br />
			<!-- [PRESULT] -->
			<!-- [WORK] -->
			<div class="title"><h2>WORK EXPERIENCE</h2></div>
			<table>
				{WORK-ROW}<tr style=" font-family: dejavusans, Tahoma;"><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  <i>[POSITION]</i>
											  <ul style="list-style: none;"><li>[DUTIES] </li></ul>
											</td></tr>{WORK-ROW}
			</table>
			<br />
			<!-- [WORK] -->
			<!-- [INTERNSHIP] -->
			<div class="title"><h2>INTERNSHIP EXPERIENCE</h2></div>
			<table>
				{INTERNSHIP-ROW}<tr style=" font-family: dejavusans, Tahoma;"><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;"><strong>[ORGANIZATION] ([EMPLOYMENT])</strong><br />
											  <i>[POSITION]</i>
											  <ul style="list-style: none;"><li>[DUTIES]</li></ul>
											</td></tr>{INTERNSHIP-ROW}
			</table>
			<br />
			<!-- [INTERNSHIP] -->
			<!-- [EXACTIVITIES] -->
			<div class="title"><h2>EXTRA-CURRICULAR ACTIVITIES</h2></div>
			<table>
				{EXACTIVITIES-ROW}<tr  style=" font-family: dejavusans, Tahoma;"><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;"><strong>[ORGANIZATION]</strong><br />
											  <i>[POSITION]</i>
											  <ul style="list-style: none;"><li>[DUTIES]</li></ul>
											</td></tr>{EXACTIVITIES-ROW}
			</table>
			<br />
			<!-- [EXACTIVITIES] -->
			<!-- [PROFESSIONAL] -->
			<div class="title"><h2>PROFESSIONAL QUALIFICATIONS</h2></div>
			<table>
				{PROFESSIONAL-ROW}
				<tr  style=" font-family: dejavusans, Tahoma;"><td style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
					  <ul style="list-style: none;"><li>[GRADE]</li></ul>
					</td></tr>
				{PROFESSIONAL-ROW}
			</table>
			<br />
			<!-- [PROFESSIONAL] -->
			<!-- [OTHER_QUALI] -->
			<div class="title"><h2>OTHER QUALIFICATIONS</h2></div>
			<table>
				{OTHER_QUALI-ROW}
				<tr style=" font-family: dejavusans, Tahoma;"><td  style="width:210px; vertical-align: top;">[DATE]</td><td style="width:510px; vertical-align: top;"><strong>[NAME], [ORGANIZATION]</strong><br />
					<ul style="list-style: none;"><li>[GRADE]</li></ul>
				</td></tr>
				{OTHER_QUALI-ROW}

			</table>
			<br />
			<!-- [OTHER_QUALI] -->
			<!-- [ACHIEVEMENTS] -->
			<div class="title"><h2>ACHIEVEMENTS</h2></div>
			<table>
				{ACHIEVEMENTS-ROW}<tr style=" font-family: dejavusans, Tahoma;"><td style="width:210px; padding-bottom: 14px; vertical-align: top;">[DATE]</td><td style="width:510px; padding-bottom: 14px; vertical-align: top;">[NAME], [ORGANIZATION]</td></tr>{ACHIEVEMENTS-ROW}
			</table>
			<br />
			<!-- [ACHIEVEMENTS] -->
			<!-- [SKILLS] -->
			<div class="title"><h2>SKILLS</h2></div>
			<table>
				<!-- [LANGUAGE] --><tr style=" font-family: dejavusans, Tahoma;"><td style="width:210px; vertical-align: top;">Language Proficiency:</td><td style="width:510px; padding-bottom: 20px; vertical-align: top;">{LANGUAGE-ROW}<u>[LANGUAGE]:</u><br>[RESULTS]<br />{LANGUAGE-ROW}</td></tr><!-- [LANGUAGE] -->

				<!-- [COMPUTER] --><tr style=" font-family: dejavusans, Tahoma;">
					<td style="width:210px; vertical-align: top;">Technical Skills:</td>
					<td style="width:510px; padding-bottom: 20px; vertical-align: top;">{COMPUTER-ROW}[COMPUTER]: [RESULTS]<br />{COMPUTER-ROW}</td>
				</tr><!-- [COMPUTER] -->

				{SKILLS-ROW}<tr style=" font-family: dejavusans, Tahoma; ">
					<td style="width:210px; vertical-align: top;">[SKILL]:</td>
					<td style="width:510px; padding-bottom: 20px; vertical-align: top;">[DESCRIPTION]</td>
				</tr>{SKILLS-ROW}
			</table>
			<br />
			<!-- [SKILLS] -->
			<!-- [OTHERS] -->
			{OTHERS-ROW}<div style=" font-family: dejavusans, Tahoma; font-size: 16px;"><div class="title"><h2 style=" vertical-align: top; text-transform: uppercase">[TITLE]</h2></div>
			[CONTENT]<br /></div>
			{OTHERS-ROW}
			<br />
			<!-- [OTHERS] -->
			<!-- [REFERENCE] -->
			<div class="title"><h2>REFERENCE</h2></div>
			<table style=" font-family: dejavusans, Tahoma;">{REFERENCE-ROW}
					<!-- [NAME] --><tr><td style="width:210px; vertical-align: top;">Name:</td><td style="width:510px; vertical-align: top;">[NAME]</td></tr><!-- [NAME] -->
					<!-- [POSITION] --><tr><td style="width:210px; vertical-align: top;" >Position:</td><td style="width:510px; vertical-align: top;">[POSITION]</td></tr><!-- [POSITION] -->
					<!-- [ORGANIZATION] --><tr><td style="width:210px; vertical-align: top;">Organization:</td><td style="width:510px; vertical-align: top;">[ORGANIZATION]</td></tr><!-- [ORGANIZATION] -->
					<!-- [CONTACT] --><tr><td style="width:210px; vertical-align: top;" >Contact Number:</td><td style="width:510px; vertical-align: top;">[CONTACT]</td></tr><!-- [CONTACT] -->
					<!-- [EMAIL] --><tr><td style="width:210px; vertical-align: top;" >Email:</td><td style="width:510px; vertical-align: top;">[EMAIL]</td></tr><!-- [EMAIL] -->
					<tr><td style="width:210px; vertical-align: top;" >&nbsp;</td><td style="width:510px; vertical-align: top;">&nbsp;</td></tr>
				{REFERENCE-ROW}
			</table>
			<br /><br />
			<!-- [REFERENCE] -->
		</main>
	</body>
</page>