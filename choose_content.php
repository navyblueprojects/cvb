<?php
	ob_start();

	require_once("config.inc.php");
	include_once("classes/system.php");
	include_once("header.php");

	$pageName = ["Create CV Form"];
	
	include_once("breadcrumb.php");

	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	
	if (array_key_exists("goSave", $_GET)) {
		$id = $member->createCVContent($_POST["application_id"], $_POST);
		header("location: choose-template/".base64_encode(sha1("app-".$id)));
		exit();
		
	}
	$member->getCVForm();

	$subject = new Subject();
	$exam_result = $subject->getAllCat();


?><main id="choose_content">
	<div class="wrapper">
		<div class="row">
			<ul class="nav nav-step hidden-xs ">
				<li class="active">Application Form</li>
				<li>Templates</li>
				<li>Preview & Save</li>
			</ul>
			<ul class="nav nav-pills visible-xs">
				<li role="presentation" class="active"><a>Application Form</a></li>
				<li role="presentation" class="disabled"><a>Templates</a></li>
				<li role="presentation" class="disabled"><a>Preview & Save</a></li>
			</ul>
		</div>
		
	</div>
	<div class="container">
			
			<div class="dropdown visible-xs pull-right " id="mobile-menu">
				<a class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<i class="fa fa-bars"></i>
				</a>
				<div class="dropdown-menu">
					<ul class="nav nav-pills nav-stacked ">
						<li class="active" ><a data-toggle="tab" href="#basic" data-tab="basic"><span class="fa fa-user"> Personal Particulars</span> </a></li>
						<?php
						if (count($member->cvform["education"]) > 0 || count($member->cvform["exam_result"]) > 0 || count($member->cvform["public_exam"]) > 0) { 
						?><li><a data-toggle="tab" href="#education" data-tab="education"><span class="fa fa-graduation-cap">Education</span></a> </li><?php
						}
						if (count($member->cvform["qualification"]["professional"]) > 0 || count($member->cvform["qualification"]["other"]) > 0 ) { 
						?><li><a data-toggle="tab" href="#qualification" data-tab="qualification"><span class="fa fa-award"> Qualification(s)</span></a> </li><?php
						}
						if (count($member->cvform["experience"]["work"]) > 0 || count($member->cvform["experience"]["internship"]) > 0 || count($member->cvform["experience"]["reference"]) > 0 || count($member->cvform["achievement"]["exactivity"]) > 0  ) { 
						?><li><a data-toggle="tab" href="#experience" data-tab="experience"><span class="fa fa-briefcase"> Experience(s)</span></a> </li><?php
						}
						if (count($member->cvform["achievement"]["achievement"]) > 0 ) { 
						?><li><a data-toggle="tab" href="#achievements" data-tab="achievements"><span class="fa fa-trophy"> Achievement(s)</span></a> </li><?php
						}
						if (count($member->cvform["achievement"]["skill"]) > 0) {
						?><li><a data-toggle="tab" href="#skills" data-tab="skills"><span class="fa fa-wrench">Skill(s)</span></a> </li><?php
						}
						if (count($member->cvform["other"]) > 0 || $member->cvform["application"]["availability"] || $member->cvform["application"]["expected_salary"] ) { 
						?><li><a data-toggle="tab" href="#other" data-tab="other"><span class="fa fa-star">Others</span></a>  </li><?php
						}
					?>
					</ul>
				</div>
			</div>	
			<h1 class="page_header">Select Your Content</h1>

			<section id="cv_wrapper" class="col-md-12">
				<div class="row">
					<ul class="nav nav-pills nav-stacked col-md-2 hidden-xs">
						<li class="active" ><a data-toggle="tab" href="#basic" data-tab="basic"><span class="fa fa-user"> Personal Particulars</span></a></li>
						<?php
						if (count($member->cvform["education"]) > 0 || count($member->cvform["exam_result"]) > 0 || count($member->cvform["public_exam"]) > 0) { 
						?><li><a data-toggle="tab" href="#education" data-tab="education"><span class="fa fa-graduation-cap"> Education</span></a> </li><?php
						}
						if (count($member->cvform["qualification"]["professional"]) > 0 || count($member->cvform["qualification"]["other"]) > 0 ) { 
						?><li><a data-toggle="tab" href="#qualification" data-tab="qualification"><span class="fa fa-award"> Qualification(s)</span></a> </li><?php
						}
						if (count($member->cvform["experience"]["work"]) > 0 || count($member->cvform["experience"]["internship"]) > 0 || count($member->cvform["experience"]["reference"]) > 0 || count($member->cvform["achievement"]["exactivity"]) > 0  ) { 
						?><li><a data-toggle="tab" href="#experience" data-tab="experience"><span class="fa fa-briefcase"> Experience(s)</span></a> </li><?php
						}
						if (count($member->cvform["achievement"]["achievement"]) > 0 ) { 
						?><li><a data-toggle="tab" href="#achievements" data-tab="achievements"><span class="fa fa-trophy"> Achievement(s)</span></a> </li><?php
						}
						if (count($member->cvform["achievement"]["skill"]) > 0) {
						?><li><a data-toggle="tab" href="#skills" data-tab="skills"><span class="fa fa-wrench"> Skill(s)</span></a> </li><?php
						}
						if (count($member->cvform["other"]) > 0 || $member->cvform["application"]["availability"] || $member->cvform["application"]["expected_salary"] ) { 
						?><li><a data-toggle="tab" href="#other" data-tab="other"><span class="fa fa-star"> Others</span></a>  </li><?php
						}
					?>
					</ul><?php
					?>
					<form method="post" action="choose_content?goSave">
					<div class="col-md-10 tab-content">
						<div id="basic" class="tab-pane fade in active">
							<ul class="list">
								<li>
									<span class="cv-addon"><input type="checkbox" name="basic[]" value="ename" checked="checked"/></span>
									<div><label>Full Name</label><?=$member->cvform["application"]["lastname_en"] . " " .$member->cvform["application"]["firstname_en"]  ?></div>
								</li>
								<li>
									<span class="cv-addon"><input type="checkbox" name="basic[]" value="cname" checked="checked"/></span>
									<div><label>Full Name (中)</label><?=$member->cvform["application"]["lastname_zh"] . $member->cvform["application"]["firstname_zh"] ?></div>
								</li>
								<li><span class="cv-addon"><input type="checkbox" name="basic[]" value="gender" checked="checked" /></span><div><label>Gender</label><?=$member->cvform["application"]["gender"]?></div></li> 
								<li><span class="cv-addon"><input type="checkbox" name="basic[]" value="age" checked="checked" /></span><div><label>Age</label><?=$member->cvform["personal"]["age"]?></div></li>
								<li><span class="cv-addon"><input type="checkbox" name="basic[]" value="dob" checked="checked" /></span><div><label>Date of Birth</label><?=$member->cvform["application"]["dob"]?></div></li>
								<li><span class="cv-addon"><input type="checkbox" name="basic[]" value="email" checked="checked" /></span><div><label>Email</label><?=$member->cvform["personal"]["p_email"]?></div></li>
								<li><span class="cv-addon"><input type="checkbox" name="basic[]" value="contact" checked="checked" /></span><div><label>Contact</label><?=$member->cvform["personal"]["contact"]?></div></li>
								
								<li><span class="cv-addon"><input type="checkbox" name="basic[]" value="address" checked="checked" /></span>
									<div><label>Address </label><?=$member->cvform["personal"]["address1"]?><BR>
									<?=$member->cvform["personal"]["address2"]?><BR>
									<?=$member->cvform["personal"]["address3"]?></div></li>

								<?php
									if ($member->cvform["personal"]["religion"] != "") { 
								?><li><span class="cv-addon"><input type="checkbox" name="basic[]" value="religion" checked="checked" /></span>
									<div><label>Religion </label><?=$member->cvform["personal"]["religion"]?></div></li><?php
									}
									if ($member->cvform["application"]["photo"] != "") { 
								?><li><span class="cv-addon"><input type="checkbox" name="basic[]" value="photo" checked="checked" /></span>
									<div><label>Photo </label><a href="<?=$appPath . "/". $member->cvform["application"]["photo"]?>" rel="noreferrer noopener"  target="_blank">Photo</a></div></li><?
								}
									?>
   					 		</ul>
   					 	</div>
						<div id="education" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["education"]) > 0) {
							?><h4>Education</h4>
							<ul class="list"><?php
									foreach ($member->cvform["education"] as $edu) {	
										$edu["content"] = json_decode($edu["content"], true);
										
								?><li ><span class="cv-addon"><input type="checkbox" name="meta[]" value="<?=$edu["id"]?>" checked="checked" /></span>
									<div><span class="education-date"><?=$edu["content"]["fromdate"] . " - " . $edu["content"]["todate"]?></span>
									<?=$edu["content"]["school"]?><br /><?=$edu["content"]["class"]?></div></li><?php
									
									}
								?></ul>
							<?php
							}

							
							?><h4>Academic Attainment</h4>
							<ul class="list">
								<?php
								if ($member->cvform["application"]["cgpa"]["point"] > 0) {
								?><li ><span class="cv-addon"><input type="checkbox" name="basic[]" value="cgpa" checked="checked" /></span>
									<div><span class="education-date"><?=$member->cvform["application"]["cgpa"]["date"]?></span>  CGPA: <?=$member->cvform["application"]["cgpa"]["point"]?><BR>School: <?=$member->cvform["application"]["gpa"]["school"]?></div></li><?
								}
								if ($member->cvform["application"]["gpa"]) {
								?><li ><span class="cv-addon"><input type="checkbox" name="basic[]" value="gpa" checked="checked" /></span>
									<div><span class="education-date"><?=$member->cvform["application"]["gpa"]["date"]?></span> <?=$member->cvform["application"]["cgpa"]["school"]?> GPA: <?=$member->cvform["application"]["gpa"]["point"]?></div></li><?
								}
								?>
								<?php
								if (count($member->cvform["exam_result"]) > 0) {
									foreach ($member->cvform["exam_result"] as $exam) {	
										$exam["content"] = json_decode($exam["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox" name="meta[]" value="<?=$exam["id"]?>" checked="checked" /></span>
									<div><span class="education-date"><?=$exam["content"]["year"]?></span><?=$exam["content"]["exam"]?></div></li><?php
									}
								}
								?>
							</ul><?php
							

							if (count($member->cvform["public_exam"]) > 0) {
							?><h4>Other Public Results</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["public_exam"] as $public_exam) {	
										$public_exam["content"] = json_decode($public_exam["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$public_exam["id"]?>" checked="checked"/></span>
									<div><span class="education-date"><?=$public_exam["content"]["date"]?></span><?=$public_exam["content"]["name"]?><br />
									<strong>Organization:</strong> <?=$public_exam["content"]["organization"]?>
									<strong>Result:</strong> <?php

									$h = array();

									foreach(json_decode($public_exam["content"]["grade"], true) as $idx => $grade){
										$h[] =  $idx . ": " . $grade;
									}
									echo " (".implode(" / ", $h).") ";
									?></div></li><?php
									}
								?>
							</ul><?php
							}
							?>
						</div>
						<div id="experience" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["experience"]["work"]) > 0) {
							?><h4>Work Experience(s)</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["experience"]["work"] as $work) {	
										$work["content"] = json_decode($work["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$work["id"]?>" checked="checked"/></span>
									<div><span class="education-date"><?=$work["content"]["fromdate"] . " - " . $work["content"]["todate"]?></span><?=$work["content"]["position"]?> (<?=$work["content"]["employment"]?>)<br />
									<?=$work["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}

							if (count($member->cvform["experience"]["internship"]) > 0) {
							?><h4>Internship Experience(s)</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["experience"]["internship"] as $internship) {	
										$internship["content"] = json_decode($internship["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$internship["id"]?>" checked="checked"/></span>
									<div><span class="education-date"><?=$internship["content"]["fromdate"] . " - " . $internship["content"]["todate"]?></span><?=$internship["content"]["position"]?> (<?=$internship["content"]["employment"]?>)<br />
									<?=$internship["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}

							if (count($member->cvform["achievement"]["exactivity"]) > 0) {
							?><h4>Extra-Curricular Activity(ies)</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["achievement"]["exactivity"] as $exactivity) {	
										$exactivity["content"] = json_decode($exactivity["content"], true);
								?><li ><?php
									?><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$exactivity["id"]?>" checked="checked"/></span>
									  <div><span class="education-date"><?=$exactivity["content"]["fromdate"] . " - " . $exactivity["content"]["todate"]?></span>
									  	<?=$exactivity["content"]["position"]?><br><?=$exactivity["content"]["organization"]?>
									  	</div></li><?php
									}
								?>
							</ul><?php
							}

							if (count($member->cvform["experience"]["reference"]) > 0) {
							?><h4>Reference(s)</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["experience"]["reference"] as $reference) {	
										$reference["content"] = json_decode($reference["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$reference["id"]?>" checked="checked"/></span>
									<div><?=$reference["content"]["name"]?><br />
										<?=$reference["content"]["position"]?>, <?=$reference["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}
							?>
						</div>
						<div id="qualification" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["qualification"]["professional"]) > 0) {
							?><h4>Professional Qualification(s)</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["qualification"]["professional"] as $professional) {	
										$professional["content"] = json_decode($professional["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$professional["id"]?>" checked="checked"/></span>
									<div><span class="education-date"><?=$professional["content"]["date"]?></span><?=$professional["content"]["name"]?> <br />
									<?=$professional["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}

							if (count($member->cvform["qualification"]["other"]) > 0) {
							?><h4>Other Qualification(s)</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["qualification"]["other"] as $other) {	
										$other["content"] = json_decode($other["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$other["id"]?>" checked="checked"/></span>
									<div><span class="education-date"><?=$other["content"]["date"] ?></span><?=$other["content"]["name"]?><br />
									<?=$other["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}

							
							?>
						</div>
						<div id="achievements" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["achievement"]["achievement"]) > 0) {
							?><h4>Achievement(s)</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["achievement"]["achievement"] as $achievement) {	
										$achievement["content"] = json_decode($achievement["content"], true);
										
								?><li ><?php
									?><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$achievement["id"]?>" checked="checked"/></span>
									  <div><span class="education-date"><?=$achievement["content"]["date"]?></span><?=$achievement["content"]["name"]?><BR>
									  	<?=$achievement["content"]["organization"]?></div></li><?php
									}
								?>
							</ul><?php
							}
							
							
							?>
							
						</div>
						<div id="skills" class="tab-pane fade in "><?php
							
							if (count($member->cvform["achievement"]["skill"]) > 0) {


								foreach ($member->cvform["achievement"]["skill"] as $skill) {
									$skill["content"] = json_decode($skill["content"], true);
									
									if (count($skill["content"]["language"]) > 0){
									?><h4>Language Proficiency</h4>
									<ul class="list"><?php
										foreach ($skill["content"]["language"] as $language=>$grade) {
										?><li ><span class="cv-addon"><input type="checkbox"  name="language[]" value="<?=$language?>" checked="checked"/></span>
											<div><?=ltrim($language, "_")?>: <?
											foreach ($grade as $item=>$g) {
												echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;".$item . ": " . $g. " ";
											}?></div></li><?php
										}
									?></ul><?
								  	}

								  	if (count($skill["content"]["computer"]) > 0){
								?><h4>Technical Skills</h4>
									<ul class="list"><?php
										foreach ($skill["content"]["computer"] as $computer=>$grade) {
									?><li ><span class="cv-addon"><input type="checkbox"  name="computer[]" value="<?=$computer?>" checked="checked"/></span><div><?=ltrim($computer, "_")?>: <?=$grade?></div></li><?php 
										}
								  ?></ul><?php
								  	}
								}

							}
							?><input type="hidden" value="<?=$member->cvform["achievement"]["skill"][0]["id"]?>" name="meta[]" /><?
							if (count($member->cvform["achievement"]["skill_other"]) > 0) {
								?><h4>Other Skill</h4>
								<ul class="list"><?
								foreach ($member->cvform["achievement"]["skill_other"] as $skillOther) {
									$skillOther["content"] = json_decode($skillOther["content"], true);
								?><li ><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$skillOther["id"]?>" checked="checked"/></span>
									  <div><?=$skillOther["content"]["skill"]?></div>
									  <div><?=html_entity_decode($skillOther["content"]["description"], ENT_QUOTES, "UTF-8"); ?></div></li><?php
								}
								?></ul><?
							}
							
						?></div>
						<div id="other" class="tab-pane fade in ">
							<?php
							if (count($member->cvform["other"]) > 0) {
							?><h4>Other Information</h4>
							<ul class="list">
								<?php
									foreach ($member->cvform["other"] as $other) {
										$other["content"] = json_decode($other["content"], true);

										if (trim($other["content"]["title"]) != "") {
								?><li ><?php
									?><span class="cv-addon"><input type="checkbox"  name="meta[]" value="<?=$other["id"]?>" checked="checked"/></span>
									  <p><strong><?=$other["content"]["title"]?></strong></p>
									  <div><?=html_entity_decode($other["content"]["html"], ENT_QUOTES, "UTF-8"); ?></div></li><?php
										}
									}
								?>
							</ul><?php
							}
							if ($member->cvform["application"]["availability"]) {
							?><h4>Availability</h4>
							<ul class="list">
								<li ><span class="cv-addon"><input type="checkbox"  name="basic[]" value="availability" checked="checked"/></span>
									  <div>Availability: <?=($member->cvform["application"]["availability"])?></div></li>
							</ul><?php
							}
							if ($member->cvform["application"]["expected_salary"]) {
							?>
							<h4>Expected Salary</h4>
							<ul class="list">
								<li ><span class="cv-addon"><input type="checkbox"  name="basic[]" value="expected_salary" checked="checked"/></span>
									  <div>Expected Salary: <?=($member->cvform["application"]["expected_salary"])?></div></li>
							</ul><?php
							}
							?>
						</div>
					</div>
					<div class=" text-center saveRow">
						<input type="hidden" name="application_id" value="<?=$member->cvform["application"]["id"]?>" />
						<div class="col-md-12"><a href="new_cv" class="btn btn-default" >Back to Edit</a> <button type="submit" class="btn btn-default" >Create CV</button></div>
					</div>
					</form>
				</div>
				
			</section>
		</div>
	<link href="/css/select2.min.css" rel="stylesheet" />
	<script src="/js/select2.min.js"></script>
	<script src="scripts/cvform.js"></script>
</main>
<?php
	include_once("footer.php");
	ob_end_flush();
?>