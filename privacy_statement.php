<?php
	require_once("config.inc.php");

	include_once("header.php");
	$pageName = ["Privacy Statement"];
	//include_once("breadcrumb.php");
	
	if (!$member->isLogined()){
		header("location: login.php");
		exit();
	}	
	
?>
	<main id="disclaimer">
		<div class="wrapper">
			<div class="container">
				<h1 class="page_header">Privacy Statement</h1>
				<section id="disclaimer_wrapper" class="col-md-12">
					<data data-cms-title="privacy_statement"><?=$cmsObj->getPageArea("privacy_statement")?></data>
				</section>
				<div id="disclaimer_action">
					<a href="#" data-tid="modalDisagree" data-toggle="modal" data-target="#modalDisagree"><button class="btn btn-default">Disagree</button></a>
					<a href="home.php"><button class="btn btn-default">I Agree</button></a>
				</div>
			</div>
		</div>
				<div class="modal fade" tabindex="-1" role="dialog" id="modalDisagree" aria-labelledby="lblModalDisagree">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="lblModalDisagree">Disagree </h4>
		      </div>
		      <div class="modal-body">
		         <data data-cms-title="ps_disagree_msg">
					<?=$cmsObj->getPageArea("ps_disagree_msg")?>
				</data>
		      </div>
		      <div class="modal-footer">
		        <a href="logout"><button type="button" class="btn btn-default" >Back to main page</button></a>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

	</main>
<?php
	include_once("footer.php");
?>